<?php
/**
* Provides a 'Publisso Gold Slider' Block
*
* @Block(
*   id = "publisso_gold_slider_block",
*   admin_label = @Translation("Publisso Gold Slider Block"),
* )
*/

namespace Drupal\publisso_gold\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
        
        class PublissoGoldSlider extends BlockBase implements BlockPluginInterface{

        /**
        * {@inheritdoc}
        */
        public function build() {
                
                $theme = \Drupal::service('theme.manager')->getActiveTheme()->getName();
                $template = new \Drupal\publisso_gold\Controller\Template();
                
                $template->get($theme.'_slider');
                if($template->isLoaded() === false) $template->get('slider');
                
                $t = \Drupal::service('publisso_gold.texts');
                $imagePath = \Drupal::service('module_handler')->getModule('publisso_gold')->getPath().'/images';
                
                $partner = ['datacite', 'totalequality', 'landderideen', 'openaccess'];
                
                foreach($partner as $_){
                        
                        $template->setVar($_.'_logo' , "/$imagePath/".$t->get('slider.'.$_.'.logo'));
                        $template->setVar($_.'_title', $t->get('slider.'.$_.'.title', 'afc'));
                        $template->setVar($_.'_alt'  , $t->get('slider.'.$_.'.alt'  , 'afc'));
                        $template->setVar($_.'_url'  , $t->get('slider.'.$_.'.url'  , 'afc'));
                }
                
                return array(
                        '#type' => 'inline_template',
                        '#template' => $template->parse(),
                        '#attached' => [
                                'library' => [
                                        'publisso_gold/default'
                                ]
                        ],
                        '#cache' => [
                                'max-age' => 0
                        ]
                );
        }

        /**
        * {@inheritdoc}
        */
        public function blockForm($form, FormStateInterface $form_state) {

//                 $form = parent::blockForm($form, $form_state);
// 
//                 $config = $this->getConfiguration();
// 
//                 $form['publisso_gold_block_title'] = array (
//                         
//                         '#type' => 'textfield',
//                         '#title' => $this->t('Title'),
//                         '#description' => $this->t('Module Title?'),
//                         '#default_value' => isset($config['title']) ? $config['title'] : ''
//                 );

                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function blockSubmit($form, FormStateInterface $form_state) {
                
//                 $this->setConfigurationValue('title', $form_state->getValue('publisso_gold_block_title'));
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function defaultConfiguration() {
                
//                 $default_config = \Drupal::config('publisso_gold.settings');
//                 
//                 return array(
//                         'title' => $default_config->get('publisso_gold.title')
//                 );
                return [];
        }

}
