<?php
/**
 * Provides a 'Publisso Gold Preview Book Content' Block
 *
 * @Block(
 *   id = "publisso_gold_preview_book_content_block",
 *   admin_label = @Translation("Publisso Gold Preview Book Content Block"),
 * )
 */

namespace Drupal\publisso_gold\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\menu_link_content;
        
        
        class PublissoGoldPreviewBookContentBlock extends BlockBase implements BlockPluginInterface{
    
    private $modpath;
    private $userrole = 'guest';
    private $user = 'guest';
    private $userweight = 10;
    private $user_id = NULL;
    private $tmpl = '';
    private $tmpl_vars = [];
    private $medium_id;
    private $modaccessweights = [];

  /**
   * {@inheritdoc}
   */
  public function build() {
    
        //echo '<pre>'.print_r('Beginning block content....', 1).'</pre>';
        
        $config = $this->getConfiguration();
        $this->getModuleAccessPermissions();
        $this->modpath = drupal_get_path('module', 'publisso_gold');
          
        $ary['route_name'] = \Drupal::routeMatch()->getRouteName();
        $ary['route_param'] = \Drupal::routeMatch()->getRawParameters()->all();
        
        $showBlockOnRoutes = [
                'publisso_gold.books_book.preview',
                'publisso_gold.book.chapter.preview',
        ];
        
        if(!in_array($ary['route_name'], $showBlockOnRoutes)) return ['#markup' => ''];
        
        //get request
        $request = \Drupal::service('request_stack')->getCurrentRequest();
        list($form, $data) = explode('|', $request->request->get('data'));
                
        if(!$form) $form = (object)[]; else $form = json_decode(base64_decode($form));
        if(!$data) $data = (object)[]; else $data = json_decode(base64_decode($data));

        //assign values
        $values = [
                'title',
                'abstract',
                'keywords',
                'chapter_text',
                'references'
        ];
        
        foreach($values as $_) if($form->$_) $data->$_ = $form->$_;
        //echo '<pre>'.print_r($data, 1).'</pre>';
          
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
    
        if(!array_key_exists('action', $_SESSION))
                $_SESSION['action'] = [];
        
        $book = new \Drupal\publisso_gold\Controller\Book($ary['route_param']['bk_id']);
        
        //echo '<pre>'.print_r($book, 1).'</pre>';
    //if(array_key_exists('previewbook', $_SESSION['action'])){
        
        //$book = new \Drupal\publisso_gold\Controller\Book($_SESSION['action']['previewbook']);
		$this->medium_id = $book->getElement('id');
        $this->tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_book_content.tmpl.inc.php');
        
        $this->tmpl_vars['::txt_about::'] = (string)t('About this Book');
        $this->tmpl_vars['::txt_media::'] = (string)t('Media');
        $this->tmpl_vars['::txt_editorial_board::'] = (string)t('Editorial Board');
        $this->tmpl_vars['::txt_authors::'] = (string)t('Authors');
        $this->tmpl_vars['::txt_manuscript_guidelines::'] = (string)t('Manuscript Guidelines');
        $this->tmpl_vars['::txt_imprint::'] = (string)t('Imprint');
        $this->tmpl_vars['::txt_overview_chapters::'] = (string)t('Overview Chapters');
        
        $this->tmpl_vars['::book_about_sidebar_blocks::'] = '';
        $this->tmpl_vars['::book_chapter_content_sidebar_blocks::'] = '';
		
		$_ = base64_decode($book->getElement('about'));
		$this->tmpl_vars['::book_about::'] = $_;
		
		$_ = base64_decode($book->getElement('manuscript_guidelines'));
		$this->tmpl_vars['::book_manuscript_guidelines::'] = $_;
		
		$_ = base64_decode($book->getElement('imprint'));
		$this->tmpl_vars['::book_imprint::'] = $_;
		
		$this->tmpl_vars['::book_funding_text::'] = $book->getElement('funding_text');
		//echo '<pre>'.print_r('Reading TOC....', 1).'</pre>';
        $toc = new \Drupal\publisso_gold\Controller\TableOfContent(false, ['medium' => 'book', 'id' => $book->getElement('id')], true);
        //echo '<pre>'.print_r('Beginning parsing structure....', 1).'</pre>';
        $this->tmpl_vars['::book_chapter_list::'] = $this->parseStructure($toc->readStructure(), 1);
        //echo '<pre>'.print_r('Finishing parsing structure....', 1).'</pre>';
        /*
		foreach($book->readChapters() as $chapter){
			$this->tmpl_vars['::book_chapter_list::'] .= '<li><a href="/publisso_gold/book/'.$book->getElement('id').'/chapter/'.$chapter->getElement('id').'">'.$chapter->getElement('title').'</a></li>';
            
            if($_SESSION['user']['id'] == 1){
                
                $toc = new \Drupal\publisso_gold\Controller\TableOfContent(false, ['medium' => 'book', 'id' => $book->getElement('id')]);
                $this->tmpl_vars['::book_chapter_list::'] = $this->parseStructure($toc->readStructure(), 1);
                
            }
		}
		*/
        if(count($book->readSustainingMembers())){
            
            $_tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_sidebar_block.tmpl.inc.php');
            $_vars = [];
            $_vars['::sbb_title::'] = (string)t('Sustaining members');
            $_vars['::sbb_content::'] = '';
            $_vars['::view_name::'] = 'partner-list';
            
            foreach($book->readSustainingMembers() as $bsm_id){
                
                $_vars['::sbb_content::'] .= '<img src="/system/getPicture/bsm/'.$bsm_id.'"><br>';
            }
            
            $this->tmpl_vars['::book_about_sidebar_blocks::'] .= $this->renderVars($_tmpl, $_vars);
        }
		
        
/* create template for authors
 *
 * use template book_content_author
 */
        $tmpl_author = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_author.tmpl.inc.php');
        $content_authors = '';
        $c = 0;
        
		if(count($book->readAuthors()) == 0){
			$content_authors = '<p>Here the authors of the chapters will be listed.</p>';
		}
		else{
			foreach($book->readAuthors() as $user){
                                
                          if(!$user->profile->getElement('show_data_in_boards')) continue;

			  $_tmpl = $tmpl_author;
			  $_vars = [];
			  
			  $_vars['::txt_more::'] = (string)t('more');
			  $_vars['::txt_address::'] = (string)t('Address');
			  $_vars['::txt_contact::'] = (string)t('Contact');
			  
			  $_vars['::modal_id::'] = 'm_authors_'.($c + 1);
			  
			  $_vars['::author_name::'] = $user->profile->getElement('graduation').' '.$user->profile->getElement('firstname').' '.$user->profile->getElement('lastname').' '.$user->profile->getElement('graduation_suffix');
			  $_vars['::author_institute::'] = $user->profile->getElement('institute');
			  $_vars['::author_department::'] = $user->profile->getElement('department');
			  $_vars['::author_street::'] = $user->profile->getElement('street');
			  $_vars['::author_postal_code::'] = $user->profile->getElement('postal_code');
			  $_vars['::author_city::'] = $user->profile->getElement('city');
			  $_vars['::author_country::'] = getCountry($user->profile->getElement('country'));
			  $_vars['::author_telephone::'] = $user->profile->getElement('telephone');
			  $_vars['::author_email::'] = $user->profile->getElement('email');
			  $_vars['::author_pic_link::'] = '/system/getPicture/up/'.$user->getId();
			  $_vars['::author_address::'] = implode(', ', [implode(' ', [$_vars['::author_postal_code::'], $_vars['::author_city::']]), $_vars['::author_country::']]);
			  
			  if($c % 2 == 0){
				$_tmpl = '<div class="row flex-row-sm">'.$_tmpl;
			  }
			  
			  if($c % 2 != 0){
				$_tmpl .= '</div><hr>';
			  }
			  
			  $content_authors .= $this->renderVars($_tmpl, $_vars);
			  $c++;
			}
        }
		
		if($c > 0 && $c % 2 != 0){
			$content_authors .= '</div>';
		}
		
        $this->tmpl_vars['::content_authors::'] = $content_authors;
/* =========================== */

/* create template for editorial board
 *
 * use template book_content_author
 * (like authors, though same var-names are used)
 */
        $tmpl_eb = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_author.tmpl.inc.php');
        $content_eb = '';
        $c = 0;
        
        foreach($book->readEditorialBoard() as $uid => $user){
          
          $_tmpl = $tmpl_eb;
          $_vars = [];
          
          $_vars['::txt_more::'] = (string)t('more');
          $_vars['::txt_address::'] = (string)t('Address');
          $_vars['::txt_contact::'] = (string)t('Contact');
          
          $_vars['::modal_id::'] = 'm_editors_'.($c + 1);
          
          $_vars['::author_name::'] = implode(' ', [$user->profile->getElement('graduation'), $user->profile->getElement('firstname'), $user->profile->getElement('lastname'), $user->profile->getElement('graduation_suffix')]);
          $_vars['::author_institute::'] = $user->profile->getElement('institute');
          $_vars['::author_department::'] = $user->profile->getElement('department');
          $_vars['::author_street::'] = $user->profile->getElement('street');
		  $_vars['::author_notes::'] = $user->tempInfo['eb_notes'].'<br><br>';
          $_vars['::author_postal_code::'] = $user->profile->getElement('postal_code');
          $_vars['::author_city::'] = $user->profile->getElement('city');
          $_vars['::author_country::'] = getCountry($user->profile->getElement('country'));
          $_vars['::author_telephone::'] = $user->profile->getElement('telephone');
          $_vars['::author_email::'] = $user->profile->getElement('email');
          $_vars['::author_pic_link::'] = '/system/getPicture/up/'.$user->getId();
		  $_vars['::author_address::'] = implode(', ', [implode(' ', [$_vars['::author_postal_code::'], $_vars['::author_city::']]), $_vars['::author_country::']]);
          
          if($c % 2 == 0){
            $_tmpl = '<div class="row flex-row-sm">'.$_tmpl;
          }
          
          if($c % 2 != 0){
            $_tmpl .= '</div><hr>';
          }
          
          $content_eb .= $this->renderVars($_tmpl, $_vars);
          $c++;
        }
		
		if($c > 0 && $c % 2 != 0){
			$content_eb .= '</div>';
		}
		
        $this->tmpl_vars['::content_eb::'] = $content_eb;
		
/* =========================== */
        if($ary['route_name'] == 'publisso_gold.book.chapter.preview'){
		//if(array_key_exists('previewchapter', $_SESSION['action'])){
			
			$tempstore = \Drupal::service('user.private_tempstore')->get('preview');
			$chapter = new \Drupal\publisso_gold\Controller\BookchapterTemp((array)$data);
			
			//preview pdf
                        $tempstore->set('chapter', serialize($chapter));
                        $tempstore->set('medium', serialize($book));
                        
                        if(\Drupal::service('publisso_gold.setup')->getValue('book.chapter.preview.pdf') != 0){
                                $url = Url::fromRoute('publisso_gold.export.chapterTemp', ['type' => 'pdf'], ['attributes' => ['target' => '_blank']]);
                                $link = Link::fromTextAndUrl((string)t('PDF Preview'), $url);
                                $_vars['::PDFPreview::'] = $link->toString();
                        }
			
			//echo '<pre>'.print_r($chapter, 1).'</pre>';
			$_tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_tab_chapter_content.tmpl.inc.php');
			$_vars['::txt_chapter_content::'] = (string)t('Chapter content');
			$_vars['::tab_chapter_content_class_active::'    ] = 'active';
			$this->tmpl_vars['::li_item_chapter_content::'] 		= $this->renderVars($_tmpl, $_vars);
			
			$_tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_content_chapter_content.tmpl.inc.php');
			$_abstract = base64_decode($chapter->getElement('abstract'));
			$_fulltext = base64_decode($chapter->getElement('chapter_text'));
			
			list($_fulltext, $_references) = \Drupal::service('publisso_gold.tools')->getReferencesFromText(base64_decode($chapter->getElement('chapter_text')));
			$inactiveReferences = \Drupal::service('publisso_gold.tools')->getInactiveReferences($_references, $chapter->getElement('references'));
			$_references = implode('<br>', array_filter([nl2br($_references), nl2br($inactiveReferences)]));
			$_vars['::book_chapter_content::'] = '
				<div>
                    <h1>
					'.$chapter->getElement('title').'
                    </h1>
				</div>
				::authors::
				<div>
                                        <div>'.(!empty($_abstract) ? '<h3>'.t('Abstract').'</h3>'.$_abstract . '<hr>' : '<br><br>').'</div>
					<div>'.$_fulltext.'</div>
					<div>'.(!empty($_references) ? '<hr><h2>'.((string)t('References')).'</h2>'.$_references : '').'</div>
				</div>
			';
            /*
            $authors = $chapter->readAuthors();
            
            $authors_names = '';
            $authors_meta = '';
            $c = 0;
            
            foreach($authors as $author){
                
                $authors_names .= (!empty($authors_names) ? '<br>' : '').$author['firstname'].' '.$author['lastname'].' <sup>'.($c + 1).'</sup>';
                $authors_meta .= (!empty($authors_meta) ? '<br>' : '').'<sup>'.($c + 1).'</sup>'.$author['department'].(!empty($author['department']) ? ', ' : '').$author['affiliation'];
                
                $c++;
            }
            
            $_vars['::book_chapter_content::'] = $this->renderVars($_vars['::book_chapter_content::'], ['::authors::' => $authors_names.'<br><br>'.$authors_meta]);
            */
            
            
            
            $authors         = [];
                $affiliations    = [];
                $strAuthors      = '';
                $strAffiliations = '';
                
                $chapter_authors = json_decode($chapter->getElement('authors'));
                
                foreach($chapter_authors as $_) $authors[$_->weight] = $_;
                ksort($authors);
                
                foreach($authors as $_) $affiliations[] = $_->affiliation;
                $affiliations = array_keys(array_flip($affiliations)); //make affs unique
                
                for($c = 1; $c <= count($affiliations); $c++){
                        if($affiliations[$c - 1]) $strAffiliations .= (!empty($strAffiliations) ? '<br>' : '') .'<sup>'.$c.'</sup>&nbsp;'.$affiliations[$c - 1];
                }
                
                foreach($authors as $_){
                        
                        $affKey = array_search($_->affiliation, $affiliations) + 1;
                        $strAuthors .= (!empty($strAuthors) ? '<br>' : '').implode(' ', [$_->firstname, $_->lastname]).(!empty($affKey) ? " <sup>$affKey</sup>" : "");
                }

            $_vars['::book_chapter_content::'] = $this->renderVars($_vars['::book_chapter_content::'], ['::authors::' => $strAuthors.'<br>'.implode('<br>', array_filter(json_decode($chapter->getElement('corporation'), 1))).'<br><br>'.$strAffiliations]);
            
            
            
			$_vars['::content_chapter_content_class_active::'] = 'active';
			$_vars['::book_citation_note::'] = $book->getElement('citation_note');
            
            //citation note
                
            $__tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_sidebar_block.tmpl.inc.php');
            $__vars = [];
            $__vars['::sbb_title::'] = (string)t('Citation note');
            $__vars['::sbb_content::'] = '';
            $__vars['::view_name::'] = 'citation-note';
            
            $cnstring = '~AUTHORLIST~. ~TITLE~. ~VERSION~In: ~CNSTRING~. ~PUBLICATIONPLACE~: ~PUBLISHER~; ~BEGINPUBLICATION~-.';
               
            //set title
            $cnstring = str_replace('~TITLE~', $chapter->getElement('title'), $cnstring);
            
            //set version
            $version = $chapter->getElement('version');
            
            if(!empty($version))
                $cnstring = str_replace('~VERSION~', $version.'. ', $cnstring);
            else
                $cnstring = str_replace('~VERSION~', '', $cnstring);
            
            //set cnstring
            $cnstring = str_replace('~CNSTRING~', $book->getElement('citation_note'), $cnstring);
            
            //set begin publication
            $_ = !empty($chapter->getElement('publication_year')) ? $chapter->getElement('publication_year') : $book->getElement('begin_publication');
            $cnstring = str_replace('~BEGINPUBLICATION~', $book->getElement('begin_publication'), $cnstring);
            
            //set publication place
            $_ = !empty($chapter->getElement('publication_place')) ? $chapter->getElement('publication_place') : $book->getElement('publication_place');
            $cnstring = str_replace('~PUBLICATIONPLACE~', $_, $cnstring);
            
            //set publisher
            $_ = !empty($chapter->getElement('publisher')) ? $chapter->getElement('publisher') : $book->getElement('publisher');
            $cnstring = str_replace('~PUBLISHER~', $_, $cnstring);
            
            //set authors
            $authors = [];
            $authors_for_cn = [];
            
            $chapter_authors = json_decode($chapter->getElement('authors'));
            
            if(!is_array($chapter_authors)){
                
                //first author who's written publication
                #$authors[] = $chapter->getElement('author_uid');
                
                //second authors which additional selected
                if(!empty($chapter->getElement('authors'))){
                    foreach(explode(',', $chapter->getElement('authors')) as $_){
                        if(!in_array($_, $authors)){
                            $authors[] = $_;
                        }
                    }
                }
                
                //generate author string
                foreach($authors as $uid){
                    $_user = new \Drupal\publisso_gold\Controller\User($uid);
                    $authors_for_cn[] = $_user->profile->getElement('lastname').' '.substr($_user->profile->getElement('firstname'), 0, 1);
                }
                
                //third more authors
                if(!empty($chapter->getElement('more_authors'))){
                    foreach(json_decode($chapter->getElement('more_authors'), true) as $_){
                        $authors_for_cn[] = $_['more_author_lastname'].' '.substr($_['more_author_name'], 0, 1);
                    }
                }
            }
            else{
                $corporations_for_cn = [];
                $authors = [];
                foreach($chapter_authors as $_) $authors[$_->weight] = $_;
                ksort($authors);
                
                foreach($authors as $_){
                    $initials = '';
                    $firstnames = preg_split('/([\\s-]+)/', $_->firstname, -1);
                    
                    $i = 0;
                    foreach($firstnames as $name){
                        if(!empty($name)) $initials .= strtoupper(substr($name, 0, 1));
                        $i++;
                        if($i == 2) break;
                    }
                    
                    $authors_for_cn[] = $_->lastname.' '.$initials;
                }
                
                //fourth corporations
                if(!empty($chapter->getElement('corporation'))){
                    foreach(json_decode($chapter->getElement('corporation'), true) as $_){
                        $corporations_for_cn[] = $_;
                    }
                }
				
				$authors_for_cn = array_filter($authors_for_cn);
				$corporations_for_cn = array_filter($corporations_for_cn);
            }
            
			if(!empty($chapter->getELement('doi')))
				$cnstring .= '<br>DOI: <a href="http://dx.doi.org/'.$chapter->getElement('doi').'">'.$chapter->getElement('doi').'</a>';
            
            $author_string_for_cn  = '';
            $author_string_for_cn .= implode(', ', $authors_for_cn);
            $author_string_for_cn .= (!empty($author_string_for_cn) ? (count($corporations_for_cn) ? '; ' : '') : '').implode('; ', $corporations_for_cn);
            
            $cnstring = str_replace('~AUTHORLIST~', $author_string_for_cn, $cnstring);
            $__vars['::sbb_content::'] = $cnstring;
            
            $_vars['::citation_note::'] = $this->renderVars($__tmpl, $__vars);
            
            //sustaining_members
            if(count($book->readSustainingMembers())){
                
                $__tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_sidebar_block.tmpl.inc.php');
                $__vars = [];
                $__vars['::sbb_title::'] = (string)t('Sustaining members');
                $__vars['::sbb_content::'] = '';
                $__vars['::view_name::'] = 'partner-list';
                
                foreach($book->readSustainingMembers() as $bsm_id){
                    
                    $__vars['::sbb_content::'] .= '<img src="/system/getPicture/bsm/'.$bsm_id.'"><br>';
                }
                
                $_vars['::sustaining_members::'] = $this->renderVars($__tmpl, $__vars);
            }
			
            //license
            if(!empty($chapter->getElement('license'))){
                
                $__tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_sidebar_block.tmpl.inc.php');
                $__vars = [];
                $__vars['::sbb_title::'] = (string)t('License');
                $__vars['::sbb_content::'] = $chapter->getElement('license');
                $__vars['::view_name::'] = 'license';
                
                $_vars['::chapter_license::'] = $this->renderVars($__tmpl, $__vars);
            }
            /*
            //version
            if(!empty($chapter->getElement('version'))){
                
                $__tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_sidebar_block.tmpl.inc.php');
                $__vars = [];
                $__vars['::sbb_title::'] = (string)t('Version');
                $__vars['::sbb_content::'] = $chapter->getElement('version');
                $__vars['::view_name::'] = 'version';
                
                $_vars['::chapter_version::'] = $this->renderVars($__tmpl, $__vars);
            }
            
            //keywords
            $keywords = json_decode($chapter->getElement('keywords'), true);
            
            if(count($keywords)){
                
                $__tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_sidebar_block.tmpl.inc.php');
                $__vars = [];
                $__vars['::sbb_title::'] = (string)t('Keywords');
                $__vars['::sbb_content::'] = '<ul class="list-unstyled">';
                $__vars['::view_name::'] = 'keywords';
                
                foreach($keywords as $_){
                    $__vars['::sbb_content::'] .= '<li><a href="#" class="intern">'.$_.'</a></li>';
                }
                
                $__vars['::sbb_content::'] .= '</ul>';
                $_vars['::chapter_keywords::'] = $this->renderVars($__tmpl, $__vars);
            }
            */
            $this->tmpl_vars['::content_item_chapter_content::'] = $this->renderVars($_tmpl, $_vars);
			unset($_SESSION['action']['previewchapter']);
		}
		else{
			$this->tmpl_vars['::tab_about_class_active::'    ] = 'active';
			$this->tmpl_vars['::content_about_class_active::'] = 'active';
		}
		
        $this->tmpl = $this->renderVars($this->tmpl, $this->tmpl_vars);
		unset($_SESSION['action']['previewchapter']);
        unset($_SESSION['action']['previewbook']);
		unset($_SESSION['data']['previewchapter']);
    //}
    return array(
            '#type' => 'inline_template',
            '#template' => $this->tmpl,
            'content' => [
              #$this->tmpl
            ],
            '#attached' => [
                'library' => [
                    'publisso_gold/default',
                    'core/jquery.ui.tabs'
                ]
            ],
            '#cache' => [
                'max-age' => 0
            ]
        );
  }
        
        /**
         * @param $structure
         * @param $level
         * @return string
         */
        private function parseStructure(&$structure, $level){
           
        $markup = '';
        $class_levels = [
            '1' => 'nav',
            '2' => 'dropdown-menu sub-menu',
            '3' => 'sub-menu deep-level',
            '4' => 'expanded',
            '5' => 'expanded last'
        ];
        
        if(count($structure) > 0){
            
            $markup .= '<ul class="'.$class_levels[$level + 1000].'">';
            
            foreach($structure as $item){
                
                $childs = $item['childs'];
                $item   = $item['item'  ];
                
                $class = count($childs) > 0 ? ' class="dropdown on"' : '';
                
                $markup .= '<li>';
                
                if(!empty($item->getElement('link'))){
                    
                    $link = $item->getElement('link');
                    
                    if(substr($link, 0, 9) == 'intern://'){
                        $link = str_replace('intern://', '', $link);
                        list($sub_medium, $sub_medium_id) = explode('/', $link);
                        
                        switch($sub_medium){
                            case 'chapter':
                                $link = '/publisso_gold/book/'.$this->medium_id.'/'.$sub_medium.'/'.$sub_medium_id;
                                break;
                        }
                    }
                }
                
                if(!(empty($link) || empty($item->getElement('link'))))
                    $markup .= '<a href="'.$link.'">'.$item->getElement('title').'</a>';
                else
                    $markup .= $item->getElement('title');
                
                if($item->getElement('description') != ''){
                    $markup .= '<div class="description">'.$item->getElement('description').'</div>';
                }
                
                if($item->getElement('authors') != ''){
                    $markup .= '<div class="authors">'.$item->getElement('authors').'</div>';
                }
                
                if(count($childs) > 0){
                    $markup .= $this->parseStructure($childs, $level + 1);
                }
                
                $markup .= '</li>';
            }
            
            $markup .= '</ul>';
        }
        
        return $markup;
    }
  
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    
    return $form;
  }
  
   /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    
  }
  
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
   
    return array(
   
    );
  }
        
        /**
         * @param $tmpl
         * @param array $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl, array $vars){
      
      //set Site-Vars
      
      $tmpl = str_replace(array_keys($vars), array_values($vars), $tmpl);
      
      //remove unused vars
      return preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $tmpl);

    }
    
    private function getModuleAccessPermissions(){
        
        $sql = "SELECT * FROM {rwPubgoldModuleaccess}";
        $result = db_query($sql);
        $result = $result->fetchAll();
        
        foreach($result as $res){
            $this->modaccessweights[$res->mod_name] = $res->mod_minaccessweight;
        }
    }
        
        /**
         * @param $modname
         * @return bool
         */
        private function userHasAccessRight($modname){
        
        if(!array_key_exists($modname, $this->modaccessweights)){
            return false;
        }
        return $this->modaccessweights[$modname] <= $this->userweight;
    }
}
