<?php
        /**
         * Provides a 'Publisso Gold Book Table of Contents' Block
         *
         * @Block(
         *   id = "publisso_gold_book_table_of_contents_block",
         *   admin_label = @Translation("Publisso Gold Book Table of Contents Block"),
         * )
         */
        namespace Drupal\publisso_gold\Plugin\Block;
        
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Block\BlockBase;
        use Drupal\Core\Block\BlockPluginInterface;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Url;
        use Drupal\Core\Link;
        use Drupal\menu_link_content;
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\TemplateManager;
        use Drupal\publisso_gold\Controller\TableOfContent;
        
        class PublissoGoldBookTableOfContentsBlock extends BlockBase implements BlockPluginInterface {
                
                private $modpath;
                private $userrole   = 'guest';
                private $user       = 'guest';
                private $userweight = 10;
                private $user_id    = null;
                private $tmpl       = '';
                private $tmpl_vars  = [];
                private $medium_id;
                
                /**
                 * {@inheritdoc}
                 */
                public function build () {
        
                        $config = $this->getConfiguration();
                        $rm = \Drupal::routeMatch();
                        $routeName = $rm->getRouteName();
                        
                        if(array_key_exists('show_on_routes', $config) && !empty($config['show_on_routes'])){
                                
                                $show_on_routes = unserialize($config['show_on_routes']) ?? [];
                                if(!in_array($routeName, $show_on_routes)) return ['#cache' => ['max-age' => 0]];
                        }
        
                        if(null === ($bk_id = $rm->getParameter('bk_id'))) return ['#cache' => ['max-age' => 0]];

                        $book = BookManager::getBook($bk_id, false);
                        $template = TemplateManager::load(strtolower($book->getElement('theme')).'_book_chapter_list');
                        
                        if(!$template->isLoaded()) return ['#cache' => ['max-age' => 0]];
                        
                        $template->setVar('book_title', $book->getElement('title'));
                        $template->setVar('txt_table_of_contents', (string)$this->t('Table of Content'));
                        
                        $toc = new TableOfContent(false, ['medium' => 'book', 'id' => $bk_id]);
                        $template->setVar('chapter_list', '<div id="nav">'.$this->parseStructure($toc->readStructure() ?? [], 1).'</div>');
                        
                        return [
                                '#type' => 'inline_template',
                                '#template' => $template->parse(),
                                '#cache' => ['max-age' => 0]
                        ];
                }
        
                /**
                 * @param array $structure
                 * @param $level
                 * @param null $parentID
                 * @return string
                 */
                private function parseStructure (array $structure, $level, $parentID = null ) {
                        
                        $markup1 = '';
                        $childOf = $parentID ? 'child-of="'.$parentID.'"' : '';
                        $addClass = $parentID ? 'collapse ' : '';
                        if ( count( $structure ) > 0 ) {
                                
                                $markup1 .= '<ul class="'.$addClass.'lvl' . $level . '" '.$childOf.'>';
                                
                                foreach ( $structure as $item ) {
                                        
                                        $childs = $item[ 'childs' ];
                                        $item = $item[ 'item' ];
                                        $dataToggle = count( $childs ) > 0 && $level < 4 ? ' data-toggle="collapse"' : '';
                                        $dataTarget = count( $childs ) > 0 && $level < 4 ? ' data-target="ul[child-of=\''.$item->getElement('id').'\']"' : '';
        
                                        $class = count( $childs ) > 0 && $level < 4 ? ' class="icon-arrow-up" '.$dataToggle.' '.$dataTarget : '';
                                        
                                        $LNK = $URL = null;
                                        
                                        $markup1 .= '<li><div class="li-wrapper"><div class="t"><div class="tr"><div class="td td-icon"><span id="icon" '.$class.'></span></div><div class="td td-text">';
                                        
                                        $link = false;
                                        
                                        if ( !empty( $item->getElement( 'link' ) ) ) {
                                                
                                                $link = $item->getElement( 'link' );
                                                
                                                if ( substr( $link, 0, 9 ) == 'intern://' ) {
                                                        
                                                        $link = str_replace( 'intern://', '', $link );
                                                        list( $sub_medium, $sub_medium_id ) = explode( '/', $link );
                                                        
                                                        switch ( $sub_medium ) {
                                                                case 'chapter':
                                                                        $chapter = new \Drupal\publisso_gold\Controller\Bookchapter( $sub_medium_id, true );
                                                                        $URL = Url::fromRoute( 'publisso_gold.books_book.chapter.chapter_content', [ 'bk_id' => $this->medium_id, 'cp_id' => $sub_medium_id ] );
                                                                        $LNK = Link::fromTextAndUrl( $item->getElement( 'title' ), $URL );
                                                                        $url = $chapter->getChapterLink( 'url' );
                                                                        $link = Link::fromTextAndUrl( $item->getElement( 'title' ), $url );
                                                                        break;
                                                        }
                                                }
                                                else{
                                                        if(UrlHelper::isValid($link)){
                                                                $url = Url::fromUri($link);
                                                                $url->setOptions(['attributes' => ['target' => '_blank']]);
                                                                $link = Link::fromTextAndUrl($link, $url);
                                                        }
                                                        else{
                                                                $link = null;
                                                        }
                                                }
                                        }
                                        
                                        $markup1 .= '<span '.($dataToggle ? 'class="fakeLink"' : '').'>'.( $link ? $link->toString() : $item->getElement( 'title' ) ) . '</span></div></div></div></div>';
                                        
                                        if ( count( $childs ) > 0 ) {
                                                $markup1 .= $this->parseStructure( $childs, $level + 1, $item->getElement('id') );
                                        }
                                        
                                        $markup1 .= '</li>';
                                }
                                
                                $markup1 .= '</ul>';
                        }
                        
                        return $markup1;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function blockForm ( $form, FormStateInterface $form_state ) {
        
                        $config = $this->getConfiguration();
                        
                        $show_on_routes = [];
                        if(!empty($config['show_on_routes'])){
                                $show_on_routes = unserialize($config['show_on_routes']);
                        }
                        
                        $form['show_on_routes'] = [
                                
                                '#type' => 'textarea',
                                '#title' => (string)$this->t('Show on routes:'),
                                '#description' => (string)$this->t('Insert routes (as defined in [module].routing.yml) one per line.'),
                                '#default_value' => implode("\n", $show_on_routes)
                        ];
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function blockSubmit ( $form, FormStateInterface $form_state ) {
        
                        $config = $this->getConfiguration();
                        $config['show_on_routes'] = serialize(array_map('trim', explode("\n", $form_state->getValue('show_on_routes'))));
                        $this->setConfiguration($config);
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function defaultConfiguration () {
                        
                        return [];
                }
        }
