<?php
/**
 * Provides a 'Publisso Gold Preview Book Header' Block
 *
 * @Block(
 *   id = "publisso_gold_preview_book_header_block",
 *   admin_label = @Translation("Publisso Gold Preview Book Header Block"),
 * )
 */

namespace Drupal\publisso_gold\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\menu_link_content;
        
        
        class PublissoGoldPreviewBookHeaderBlock extends BlockBase implements BlockPluginInterface{
    
    private $modpath;
    private $userrole = 'guest';
    private $user = 'guest';
    private $userweight = 10;
    private $user_id = NULL;
    private $tmpl = '';
    private $tmpl_vars = [];
    private $modaccessweights = array();

  /**
   * {@inheritdoc}
   */
  public function build() {
     
    $config = $this->getConfiguration();
    $session = \Drupal::service('session');
    $this->getModuleAccessPermissions();
    $this->modpath = drupal_get_path('module', 'publisso_gold');
    
    //get user, role and weight - both stored after login in default session
    if(isset($_SESSION['user']['name'])		) $this->user          = $_SESSION['user']['name'];
    if(isset($_SESSION['user']['role'])		) $this->userrole      = $_SESSION['user']['role'];
    if(isset($_SESSION['user']['weight'])	) $this->userweight    = $_SESSION['user']['weight'];
    if(isset($_SESSION['user']['id'])		) $this->user_id           = $_SESSION['user']['id'];
    if(isset($_SESSION['user']['role_id'])	) $this->role_id      = $_SESSION['user']['role_id'];
    if(! isset($_SESSION['logged_in'])		) $_SESSION['logged_in']  = false;
    
    require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
    
    if(!array_key_exists('action', $_SESSION))
      $_SESSION['action'] = [];
    
    if(array_key_exists('previewbook', $_SESSION['action'])){
        
        $book = new \Drupal\publisso_gold\Controller\Book($_SESSION['action']['previewbook']);
        $this->tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_book_header.tmpl.inc.php');
        switch(strtolower($book->getElement('theme'))){
                
                case 'gms':
                        $linkClasses = 'btn btn-warning';
                        break;
                        
                default:
                        $linkClasses = 'link--calltoaction inline';
        }
        
        if($book->hasAuthor($_SESSION['user']['id']) || ($book->countAuthors() == 0 && $_SESSION['logged_in'] === true) || $_SESSION['user']['weight'] >= 70){
            $this->tmpl_vars['::lnk_publish_chapter::'] = '
                <a href="/publisso_gold/book/'.$book->getElement('id').'/chapter/add" class="'.$linkClasses.'">
                    <span>'.((string)t('Submit')).'</span>
                </a>
            ';
        }
        
        if($this->userHasAccessRight('bookmanagement')){
            $this->tmpl_vars['::lnk_edit_book::'] = '
                <a href="/publisso_gold/bookmanagement/book/'.$book->getElement('id').'/edit/" class="'.$linkClasses.'">
                    <span>'.((string)t('Edit book')).'</span>
                </a>
            ';
            
            $this->tmpl_vars['::lnk_edit_book_eb::'] = '
                <a href="/publisso_gold/bookmanagement/set_editorialboard/'.$book->getElement('id').'" class="'.$linkClasses.'">
                    <span>'.((string)t('Edit Edit. Board')).'</span>
                </a>
            ';
        }
        
        $this->tmpl_vars['::bk_id::'] = $book->getElement('id');
        $this->tmpl_vars['::book_title::'] = $book->getElement('title');
		
		$this->tmpl_vars['::lnk_cover::'] = !empty($book->getElement('cover_link')) ? $book->getElement('cover_link') : '#';
        
        $this->tmpl_vars['::book_editors::'] = [];
        $this->tmpl_vars['::book_editors::'] = $book->getElement('public_editors');
        
        $this->tmpl_vars['::book_authors::'] = '';
        
        foreach($book->readAuthors() as $author){
            
            $this->tmpl_vars['::book_authors::'] .= (!empty($this->tmpl_vars['::book_authors::']) ? ', ' : '')
                                                 .  $author->profile->getElement('firstname')
                                                 .  ' '
                                                 .  $author->profile->getElement('lastname');
        }
        
        if(!empty($book->getElement('isbn'))){
            $this->tmpl_vars['::li_item_book_isbn::'] = '<li>ISBN: '.$book->getElement('isbn').'</li>';
        }
        
        if(!empty($book->getElement('issn'))){
            $this->tmpl_vars['::li_item_book_issn::'] = '<li>ISBN: '.$book->getElement('issn').'</li>';
        }
        
        if(!empty($book->getElement('funding_name'))){
            $this->tmpl_vars['::li_item_book_funding_name::'] = '<li>'.((string)t('Funding name')).': '.$book->getElement('funding_name').'</li>';
        }
        
        if(!empty($book->getElement('funding_number'))){
            $this->tmpl_vars['::li_item_book_funding_number::'] = '<li>'.((string)t('Funding number')).': '.$book->getElement('funding_number').'</li>';
        }
        
		$_ = [];
		foreach($book->readEditorialBoard() as $uid => $user){
			$_[] = implode(' ', [$user->profile->getElement('firstname'), $user->profile->getElement('lastname')]);
		}
        $this->tmpl_vars['::li_item_book_editorial_board::']  = '<li>'.implode('</li><li>', $_).'</li>';
		
		$this->tmpl_vars['::li_item_funding_text::'] = $book->getElement('funding_text');
		
        $this->tmpl = $this->renderVars($this->tmpl, $this->tmpl_vars);
    }
    
    $session->set('action', []);
        
    return array(
            '#type' => 'markup',
            '#markup' => $this->tmpl,
            'content' => [
              #$this->tmpl
            ],
            '#attached' => [
                'library' => [
                    'publisso_gold/default',
                    'core/jquery.ui.tabs'
                ]
            ],
            '#cache' => [
                'max-age' => 0
            ]
        );
  }
  
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    
    return $form;
  }
  
   /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    
  }
  
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
   
    return array(
   
    );
  }
        
        /**
         * @param $tmpl
         * @param array $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl, array $vars){
      
      //set Site-Vars
      
      $tmpl = str_replace(array_keys($vars), array_values($vars), $tmpl);
      
      //remove unused vars
      return preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $tmpl);

    }
    
    private function getModuleAccessPermissions(){
        
        $sql = "SELECT * FROM {rwPubgoldModuleaccess}";
        $result = db_query($sql);
        $result = $result->fetchAll();
        
        foreach($result as $res){
            $this->modaccessweights[$res->mod_name] = $res->mod_minaccessweight;
        }
    }
        
        /**
         * @param $modname
         * @return bool
         */
        private function userHasAccessRight($modname){
        
        if(!array_key_exists($modname, $this->modaccessweights)){
            return false;
        }
        return $this->modaccessweights[$modname] <= $this->userweight;
    }
}
