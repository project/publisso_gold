<?php
        /**
         * Provides a 'Publisso Gold NavbarMeta' Block
         *
         * @Block(
         *   id = "publisso_gold_navbarmeta_block",
         *   admin_label = @Translation("Publisso Gold NavbarMeta Block"),
         * )
         */
        
        namespace Drupal\publisso_gold\Plugin\Block;
        
        use Drupal\Core\Block\BlockBase;
        use Drupal\Core\Block\BlockPluginInterface;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Controller\Template;
        use Drupal\Core\Url;
        use Drupal\Core\Link;
        use Drupal\Core\Render\Markup;
        
        
        class PublissoGoldNavbarMetaBlock extends BlockBase implements BlockPluginInterface {
                
                /**
                 * {@inheritdoc}
                 **/
                public function build () {
                        
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        $theme =  \Drupal::service( 'theme.manager' )->getActiveTheme()->getName();
                        $template = new Template();
                        $template->get( $theme . '_navbarmeta' );
                        if ( !$template->isLoaded() ) $template->get( 'publisso_gold_navbarmeta' );
                        
                        $session = \Drupal::service( 'session' );
                        
                        $action = !( $session->has( 'logged_in' ) && $session->get( 'logged_in' ) === true ) ? 'login' : 'logout';
                        
                        $route[ 'route_name' ] = \Drupal::routeMatch()->getRouteName();
                        $route[ 'route_param' ] = \Drupal::routeMatch()->getRawParameters()->all();
                        
                        $lm = \Drupal::service( 'language_manager' );
                        
                        $languages = $lm->getLanguages();
                        $currLang = $lm->getCurrentLanguage()->getId();
                        
                        $langSwitcherHTML = '<ul class="language-switcher language-switcher-locale-url">';
                        
                        $c = 0;
                        foreach ( $languages as $langCode => $language ) {
                                
                                $classes = [];
                                
                                if ( $c == 0 ) {
                                        $classes[] = 'first';
                                }
                                elseif ( $c == count( $languages ) - 1 ) {
                                        $classes[] = 'last';
                                }
                                
                                if ( $currLang == $langCode ) {
                                        $classes[] = 'active';
                                }
                                
                                
                                $link = Link::fromTextAndUrl( strtoupper( $langCode ), Url::fromRoute( $route[ 'route_name' ], $route[ 'route_param' ], [ 'language' => $languages[ strtolower( $langCode ) ], 'attributes' => [ 'class' => $classes ] ] ) )->toString();
                                $langSwitcherHTML .= '<li class="">' . $link . '</li>';
                        }
                        
                        $langSwitcherHTML .= '</ul>';
                        
                        $template->setVar( 'langSwitcher', $langSwitcherHTML );
                        
                        $urlLogin = Url::fromRoute( "publisso_gold.$action", [], [
                                'attributes' =>
                                        [
                                                'id' =>
                                                        'login',
                                        ],
                        ]
                        );
                        $lnkLogin = Link::fromTextAndUrl( (string)t( $texts->get( "global.$action", 'fc' ) ), $urlLogin);
                        
                        $lnkLogin = $lnkLogin->toString();
                        $lnkLogin = preg_replace('/^(\<a.*?\>)(.+)(\<\/a\>)$/', '$1<span aria-hidden="true" class="icon-'.($action == 'login' ? 'user' : 'cancel-circle').'"></span><span id="loginText"> $2$3</span>', $lnkLogin);
                        $template->setVar( 'lnk_login', $lnkLogin );
                        
                        return [
                                '#type'     => 'inline_template',
                                '#template' => $template->parse(),
                                'content'   => [
                                ],
                                '#attached' => [
                                        'library' => [
                                                'publisso_gold/default',
                                        ],
                                ],
                                '#cache'    => [
                                        'max-age' => 0,
                                ],
                        ];
                }
        }

