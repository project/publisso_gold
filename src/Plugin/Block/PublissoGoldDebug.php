<?php
/**
* Provides a 'Publisso Gold Debug' Block
*
* @Block(
*   id = "publisso_gold_debug_block",
*   admin_label = @Translation("Publisso Gold Debug Block"),
* )
*/

namespace Drupal\publisso_gold\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\menu_link_content;
       
        class PublissoGoldDebug extends BlockBase implements BlockPluginInterface{

        /**
        * {@inheritdoc}
        */
        public function build() {

                $session = \Drupal::service('session');
                
                if(!$session->has('user')){
                        
                        return array(
                                '#type' => 'markup',
                                '#markup' => '',
                                '#attached' => [
                                        'library' => [
                                                'publisso_gold/default'
                                        ]
                                ],
                                '#cache' => [
                                        'max-age' => 0
                                ]
                        );
                }
                
                $user = $session->get('user');
                
                $orig_user = null;
                
                if($session->has('orig_user')){
                        $orig_user = $session->get('orig_user');
                }
                
                if(($user['weight'] ?? 0) < 80 && ($orig_user === null || ($orig_user['weight'] ?? 0) < 80)){ //only
                        // for
                        // administrators
                        
                        return array(
                                '#type' => 'markup',
                                '#markup' => '',
                                '#attached' => [
                                        'library' => [
                                                'publisso_gold/default'
                                        ]
                                ],
                                '#cache' => [
                                        'max-age' => 0
                                ]
                        );
                }
                
                $template = new \Drupal\publisso_gold\Controller\Template();
                $template->get('debug');
                
                /**
                 * User Information
                 */
                $userinfo = '';
                
                if(is_array($user)){
                        foreach($user as $k => $v){
                                $userinfo .= sprintf("%-20s", $k)."\t=>\t$v\n";
                        }
                }
                if($orig_user){
                        $userinfo .= "\tOriginal User\n\t=============\n";
                        
                        if(is_array($orig_user)){
                                foreach($orig_user as $k => $v){
                                        $userinfo .= "\t".sprintf("%-20s", $k)."\t=>\t$v\n";
                                }
                        }
                }
                
                $template->setVar('user_information', $userinfo);
                
                /**
                 * Database Information
                 */
                $options = \Drupal::database()->getConnectionOptions();
                $databaseinfo = '';
                
                if(is_array($options)){
                        foreach($options as $k => $v){
                                if(is_array($v)) $v = json_encode($v);
                                $databaseinfo .= sprintf("%-20s", $k)."\t=>\t$v\n";
                        }
                }
                $template->setVar('database_information', $databaseinfo);
                
                /**
                 * Setup Information
                 */
                $setup = \Drupal::service('publisso_gold.setup');
                $configinfo = '';
                
                $keys = $setup->getKeys();
                
                if(is_array($keys)){
                        foreach($keys as $k){
                                $configinfo .= sprintf("%-40s", $k)."\t=>\t".$setup->getValue($k)."\n";
                        }
                }
                
                $template->setVar('config_information', $configinfo);
                
                $routeInfo = 'Route-name: '.\Drupal::routeMatch()->getRouteName()."\n";
                foreach(\Drupal::routeMatch()->getParameters() as $k => $v){
                        if(is_scalar($v))
                                $routeInfo .= sprintf("%-20s", $k)."\t=>\t$v\n";
                }
                
                $template->setVar('route_info', $routeInfo);
                
                return array(
                        '#type' => 'inline_template',
                        '#template' => $template->parse(),
                        '#attached' => [
                                'library' => [
                                        'publisso_gold/default'
                                ]
                        ],
                        '#cache' => [
                                'max-age' => 0
                        ]
                );
        }

        /**
        * {@inheritdoc}
        */
        public function blockForm($form, FormStateInterface $form_state) {

//                 $form = parent::blockForm($form, $form_state);
// 
//                 $config = $this->getConfiguration();
// 
//                 $form['publisso_gold_block_title'] = array (
//                         
//                         '#type' => 'textfield',
//                         '#title' => $this->t('Title'),
//                         '#description' => $this->t('Module Title?'),
//                         '#default_value' => isset($config['title']) ? $config['title'] : ''
//                 );

                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function blockSubmit($form, FormStateInterface $form_state) {
                
//                 $this->setConfigurationValue('title', $form_state->getValue('publisso_gold_block_title'));
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function defaultConfiguration() {
                
//                 $default_config = \Drupal::config('publisso_gold.settings');
//                 
//                 return array(
//                         'title' => $default_config->get('publisso_gold.title')
//
                return [];
        }

}
