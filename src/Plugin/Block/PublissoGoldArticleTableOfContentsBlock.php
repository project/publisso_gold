<?php
        /**
         * Provides
         * a
         * 'Publisso
         * Gold
         * Article
         * Table
         * of
         * Contents'
         * Block
         *
         * @Block(
         *   id
         *   =
         *   "publisso_gold_article_table_of_contents_block",
         *   admin_label
         *   =
         *   @Translation("Publisso Gold
         *                          Article
         *                          Table
         *                          of
         *                          Contents
         *                          Block"),
         * )
         */
        namespace Drupal\publisso_gold\Plugin\Block;
        
        use Drupal;
        use Drupal\Core\Block\BlockBase;
        use Drupal\Core\Block\BlockPluginInterface;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Url;
        use Drupal\Core\Link;
        use Drupal\publisso_gold\Controller\Journalarticle;
        use Drupal\publisso_gold\Controller\TableOfContent;
        use Drupal\publisso_gold\Controller\Volume;
        
        class PublissoGoldArticleTableOfContentsBlock extends BlockBase implements BlockPluginInterface {
                
                private $modpath;
                private $tmpl = '';
                private $medium_id;
                private $lang = null;
                
                /**
                 * {@inheritdoc}
                 */
                public function build() {
                        
                        $this->modpath = drupal_get_path('module', 'publisso_gold');
                        
                        $route['route_name'] = Drupal::routeMatch()->getRouteName();
                        $route['route_param'] = Drupal::routeMatch()->getRawParameters()->all();
                        $route['route_namefrags'] = explode('.', $route['route_name']);
                        
                        $allowedRoutes = [
                                'publisso_gold.journal.volume.article', 'publisso_gold.journal.volume.issue.article', 'publisso_gold.journals_journal.volume.article'
                        ];
                        
                        if (in_array($route['route_name'], $allowedRoutes)) {
                                
                                $journal = Drupal\publisso_gold\Controller\Manager\JournalManager::getJournal($route['route_param']['jrn_id']);
                                $this->medium_id = $journal->getElement('id');
                                
                                $article = new Journalarticle($route['route_param']['jrna_id']);
                                
                                $lang = Drupal::languageManager()->getCurrentLanguage()->getId();
                                
                                if (($rq = Drupal::service('request_stack')->getCurrentRequest()) !== null) {
                                        if ($rq->query->has('lang')) $lang = $rq->query->get('lang');
                                }
                                
                                if ($lang == $article->getOrigLanguage()) $lang = null;
                                
                                $this->lang = $lang;
                                
                                $this->tmpl = Drupal\publisso_gold\Controller\Manager\TemplateManager::load('publisso_gold_article_list');
                                
                                $this->tmpl->setVar('article_title', $article->getElement('title', $lang, false));
                                $this->tmpl->setVar('txt_table_of_contents', (string)t('Table of Content'));
                                $this->tmpl->setVar('headers_list', '');
                                
                                $toc = new TableOfContent(false, ['medium' => 'article', 'id' => $article->getElement('id'), 'lang' => $lang]);
                                
                                if (!$toc->getElement('id')) {
                                        $article->createTOCStructure($lang);
                                        $toc = new TableOfContent(false, ['medium' => 'article', 'id' => $article->getElement('id'), 'lang' => $lang]);
                                        $toc->recreateStructure();
                                }
                                
                                $this->tmpl->setVar('headers_list', $this->parseStructure($toc->readStructure(), 1));
                        }
                        
                        return [
                                '#type'        => 'inline_template', '#template' => is_object($this->tmpl) ? $this->tmpl->parse() : '', 'content' => [//$this->tmpl
                                ], '#attached' => [
                                        'library' => [
                                                'publisso_gold/default', 'core/jquery.ui.tabs',
                                        ],
                                ], '#cache'    => [
                                        'max-age' => 0,
                                ],
                        ];
                }
        
                /**
                 * @param $structure
                 * @param $level
                 * @return string
                 */
                private function parseStructure($structure, $level) {
                        
                        $route['route_name'] = Drupal::routeMatch()->getRouteName();
                        $route['route_param'] = Drupal::routeMatch()->getRawParameters()->all();
                        
                        $markup = '';
                        $class_levels = [
                                '1' => 'nav', '2' => 'dropdown-menu sub-menu', '3' => 'sub-menu deep-level', '4' => 'expanded', '5' => 'expanded last',
                        ];
                        
                        if (is_array($structure) && count($structure) > 0) {
                                
                                $addParam = [];
                                
                                if ($this->lang) $addParam['lang'] = $this->lang;
                                
                                $style = $level > 1 && $level < 4 ? ' style="display: none;"' : '';
                                
                                $markup .= '<ul class="' . $class_levels[$level] . '"' . $style . '>';
                                
                                foreach ($structure as $item) {
                                        
                                        $childs = $item['childs'];
                                        $item = $item['item'];
                                        
                                        $class = count($childs) > 0 && $level < 4 ? ' class="dropdown"' : '';
                                        
                                        $markup .= '<li' . $class . '>';
                                        
                                        if (count($childs) > 0 && $level < 4) $markup .= '<button class="dropdown-toggle" type="button" data-toggle="dropdown"><span class="icon-arrow-down"></span></button>';
                                        
                                        if (!empty($item->getElement('link'))) {
                                                
                                                $link = $item->getElement('link');
                                                
                                                if (substr($link, 0, 9) == 'intern://') {
                                                        $link = str_replace('intern://', '', $link);
                                                        list($sub_medium, $sub_medium_id) = explode('/', $link);
                                                        
                                                        switch ($sub_medium) {
                                                                case 'chapter':
                                                                        $link = '/publisso_gold/book/' . $this->medium_id . '/' . $sub_medium . '/' . $sub_medium_id;
                                                                        break;
                                                                
                                                                case 'volume':
                                                                        $volume = new Volume($sub_medium_id);
                                                                        
                                                                        $link = LINK::fromTextAndUrl($item->getElement('title'), Url::fromRoute('publisso_gold.journal.volume', array_merge([
                                                                                        'jrn_id' => $volume->getElement('jrn_id'), 'vol_id' => $volume->getElement('id'),
                                                                                ], $addParam)
                                                                        )
                                                                        )->getUrl()->toString();
                                                                        break;
                                                                
                                                                case 'anchor':
                                                                        $link = LINK::createFromRoute($item->getElement('title'), $route['route_name'], array_merge($route['route_param'], $addParam), ['fragment' => $sub_medium_id])->getUrl()->toString();
                                                                        break;
                                                        }
                                                }
                                        }
                                        
                                        //if no target for item, hide link
                                        $markup .= '<a href="' . $link . '" ' . (empty($link) ? 'onclick="return false;" style="text-decoration: none; cursor: default;"' : '') . '>' . $item->getElement('title') . '</a>';
                                        /*
                                        if($item->getElement('description') != ''){
                                            $markup .= '<br>'.$item->getElement('description');
                                        }
        
                                        if($item->getElement('authors') != ''){
                                            $markup .= '<br>'.$item->getElement('authors');
                                        }
                                        */
                                        if (count($childs) > 0) {
                                                $markup .= $this->parseStructure($childs, $level + 1);
                                        }
                                        
                                        $markup .= '</li>';
                                }
                                
                                $markup .= '</ul>';
                        }
                        
                        return $markup;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function blockForm($form, FormStateInterface $form_state) {
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function blockSubmit($form, FormStateInterface $form_state) {
                
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function defaultConfiguration() {
                        
                        return [
                        
                        ];
                }
        }


