<?php
        
        /**
         * Provides a 'Publisso Gold Footer Right' Block
         * * @Block(
         *   id = "publisso_gold_footer_right",
         *   admin_label = @Translation("Publisso Gold Footer Right"),
         * )
         */
        
        namespace Drupal\publisso_gold\Plugin\Block;
        
        
        use Drupal\Core\Block\BlockBase;
        use Drupal\Core\Block\BlockPluginInterface;
        use Drupal\Core\Menu\MenuTreeParameters;

        class PublissoGoldFooterRight extends BlockBase implements BlockPluginInterface {
        
                /**
                 * @inheritDoc
                 */
                public function build() {
                        
                        $imgPath = \Drupal::moduleHandler()->getModule('publisso_gold')->getPath().'/images';
                        $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
                        
                        $str = '<span class="nobr">
                                <a class="zbmed logo-publisso" href="http://www.publisso.de/'.$lang.'" target="_blank" title="Home PUBLISSO">
                                    <img class="img-responsive" src="/'.$imgPath.'/LOGO_PUBLISSO_'.strtoupper($lang).'.png" alt="ZB MED: Logo">
                                </a>
                                <a class="zbmed logo-zbmed" href="http://www.zbmed.de/'.$lang.'" target="_blank" title="Home ZBMED">
                                    <img class="img-responsive" src="/'.$imgPath.'/LOGO_ZBMED_'.strtoupper($lang).'.png" alt="ZB MED: Logo" style="font-size: 16px;">
                                </a>
                                </span>';
                        
                        $mt = \Drupal::menuTree();
                        $menuTree = $mt->load('publisso_gold', new MenuTreeParameters());
                        $manipulators = [
                                ['callable' => 'menu.default_tree_manipulators:checkAccess'         ],
                                ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort']
                        ];
                        $menuTree = $mt->build($mt->transform($menuTree, $manipulators));
                        
                        return ['#markup' => $str];
                }
        }