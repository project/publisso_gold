<?php
        /**
         * Provides a 'Publisso Gold Journal Table of Contents' Block
         *
         * @Block(
         *   id = "publisso_gold_journal_table_of_contents_block",
         *   admin_label = @Translation("Publisso Gold Journal Table of Contents Block"),
         * )
         */
        namespace Drupal\publisso_gold\Plugin\Block;
        
        use Drupal\Core\Block\BlockBase;
        use Drupal\Core\Block\BlockPluginInterface;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Url;
        use Drupal\Core\Link;
        use Drupal\menu_link_content;
        use Drupal\publisso_gold\Controller\Manager\JournalManager;
        
        class PublissoGoldJournalTableOfContentsBlock extends BlockBase implements BlockPluginInterface {
                
                private $modpath;
                private $userrole   = 'guest';
                private $user       = 'guest';
                private $userweight = 10;
                private $user_id    = null;
                private $tmpl       = '';
                private $tmpl_vars  = [];
                private $medium_id;
                private $modaccessweights = [];
                
                /**
                 * {@inheritdoc}
                 */
                public function build () {
                        
                        $config = $this->getConfiguration();
                        $this->getModuleAccessPermissions();
                        $this->modpath = drupal_get_path( 'module', 'publisso_gold' );
                        
                        $route[ 'route_name' ] = \Drupal::routeMatch()->getRouteName();
                        $route[ 'route_param' ] = \Drupal::routeMatch()->getRawParameters()->all();
                        
                        $allowedRoutes = [
                                'publisso_gold.journals_journal',
                                'publisso_gold.journals_journal.volume',
                                #'publisso_gold.journals_journal.volume.article',
                                'publisso_gold.journals_journal.volume.article.current_volume',
                                'publisso_gold.journals_journal.volume.article.about',
                                'publisso_gold.journals_journal.volume.article.contact',
                                'publisso_gold.journals_journal.volume.article.editorial_board',
                                'publisso_gold.journals_journal.volume.article.manuscript_guidelines',
                                'publisso_gold.journals_journal.volume.article.imprint',
                                'publisso_gold.journals_journal.volume.article.archive',
                                'publisso_gold.journals_journal.volume.article.volume',
                                'publisso_gold.journals_journal.volume.current_volume',
                                'publisso_gold.journals_journal.volume.about',
                                'publisso_gold.journals_journal.volume.contact',
                                'publisso_gold.journals_journal.volume.manuscript_guidelines',
                                'publisso_gold.journals_journal.volume.editorial_board',
                                'publisso_gold.journals_journal.volume.imprint',
                                'publisso_gold.journals_journal.volume.archive',
                                'publisso_gold.journals_journal.current_volume',
                                'publisso_gold.journals_journal.about',
                                'publisso_gold.journals_journal.contact',
                                'publisso_gold.journals_journal.imprint',
                                'publisso_gold.journals_journal.archive',
                                'publisso_gold.journals_journal.manuscript_guidelines',
                                'publisso_gold.journals_journal.editorial_board'
                        ];
                        
                        require_once( $this->modpath . '/inc/publisso_gold.lib.inc.php' );
                        
                        if ( in_array( $route[ 'route_name' ], $allowedRoutes ) ) {
                                
                                $journal = JournalManager::getJournal($route['route_param']['jrn_id']);
                                $this->medium_id = $journal->getElement( 'id' );
                                
                                $this->tmpl = new \Drupal\publisso_gold\Controller\Template();
                                $this->tmpl->get( 'publisso_gold_journal_article_list' );
                                
                                $this->tmpl->setVar( 'journal_title', $journal->getElement( 'title' ) );
                                $this->tmpl->setVar( 'txt_table_of_contents', (string)t( 'Table of Content' ) );
                                $this->tmpl->setVar( 'article_list', '' );
                                
                                $toc = new \Drupal\publisso_gold\Controller\TableOfContent( false, [ 'medium' => 'journal', 'id' => $journal->getElement( 'id' ) ] );
                                //echo '<pre>'.(print_r($toc->readStructure(), 1)).'</pre>';
                                $this->tmpl->setVar( 'article_list', $this->parseStructure( $toc->readStructure(), 1 ) );
                        }
                        
                        return [
                                '#type'        => 'inline_template', '#template' => is_object( $this->tmpl ) ? $this->tmpl->parse() : '', 'content' => [//$this->tmpl
                                ], '#attached' => [
                                        'library' => [
                                                'publisso_gold/default', 'core/jquery.ui.tabs',
                                        ],
                                ], '#cache'    => [
                                        'max-age' => 0,
                                ],
                        ];
                }
        
                /**
                 * @param $structure
                 * @param $level
                 * @return string
                 */
                private function parseStructure ($structure, $level ) {
                        
                        $markup = '';
                        $class_levels = [
                                '1' => 'nav', '2' => 'dropdown-menu sub-menu', '3' => 'sub-menu deep-level', '4' => 'expanded', '5' => 'expanded last',
                        ];
                        
                        if ( count( $structure ) > 0 ) {
                                
                                $style = $level > 1 && $level < 4 ? ' style="display: none;"' : '';
                                
                                $markup .= '<ul class="' . $class_levels[ $level ] . '"' . $style . '>';
                                
                                if ( is_array( $structure ) || is_object( $structure ) ) {
                                        
                                        foreach ( $structure as $item ) {
                                                
                                                $childs = $item[ 'childs' ];
                                                $item = $item[ 'item' ];
                                                
                                                $class = count( $childs ) > 0 && $level < 4 ? ' class="dropdown"' : '';
                                                
                                                $markup .= '<li' . $class . '>';
                                                
                                                if ( count( $childs ) > 0 && $level < 4 ) $markup .= '<button class="dropdown-toggle" type="button" data-toggle="dropdown"><span class="icon-arrow-down"></span></button>';
                                                
                                                if ( !empty( $item->getElement( 'link' ) ) ) {
                                                        
                                                        $link = $item->getElement( 'link' );
                                                        
                                                        if ( substr( $link, 0, 9 ) == 'intern://' ) {
                                                                $link = str_replace( 'intern://', '', $link );
                                                                list( $sub_medium, $sub_medium_id ) = explode( '/', $link );
                                                                
                                                                switch ( $sub_medium ) {
                                                                        case 'chapter':
                                                                                $link = '/publisso_gold/book/' . $this->medium_id . '/' . $sub_medium . '/' . $sub_medium_id;
                                                                                break;
                                                                        case 'volume':
                                                                                $volume = new \Drupal\publisso_gold\Controller\Volume( $sub_medium_id );
                                                                                $link = LINK::fromTextAndUrl( $item->getElement( 'title' ), Url::fromRoute( 'publisso_gold.journal.volume', [
                                                                                                                                                                                                  'jrn_id' => $volume->getElement( 'jrn_id' ), 'vol_id' => $volume->getElement( 'id' ),
                                                                                                                                                                                          ]
                                                                                )
                                                                                )->getUrl()->toString()
                                                                                ;
                                                                }
                                                        }
                                                }
                                                
                                                //if no target for item, hide link
                                                $markup .= '<a href="' . ( $link ?? '' ) . '" ' . ( empty( $link ) ? 'onclick="return false;" style="text-decoration: none; cursor: default;"' : '' ) . '>' . $item->getElement( 'title' ) . '</a>';
                                                /*
                                                if($item->getElement('description') != ''){
                                                    $markup .= '<br>'.$item->getElement('description');
                                                }
                                                
                                                if($item->getElement('authors') != ''){
                                                    $markup .= '<br>'.$item->getElement('authors');
                                                }
                                                */
                                                if ( count( $childs ) > 0 ) {
                                                        $markup .= $this->parseStructure( $childs, $level + 1 );
                                                }
                                                
                                                $markup .= '</li>';
                                        }
                                }
                                
                                $markup .= '</ul>';
                        }
                        
                        return $markup;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function blockForm ( $form, FormStateInterface $form_state ) {
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function blockSubmit ( $form, FormStateInterface $form_state ) {
                
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function defaultConfiguration () {
                        
                        return [
                        
                        ];
                }
        
                /**
                 * @param $tmpl
                 * @param array $vars
                 * @return string|string[]|null
                 */
                private function renderVars ($tmpl, array $vars ) {
                        
                        //set Site-Vars
                        
                        $tmpl = str_replace( array_keys( $vars ), array_values( $vars ), $tmpl );
                        
                        //remove unused vars
                        return preg_replace( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
                        
                }
                
                private function getModuleAccessPermissions () {
                        
                        $sql = "SELECT * FROM {rwPubgoldModuleaccess}";
                        $result = db_query( $sql );
                        $result = $result->fetchAll();
                        
                        foreach ( $result as $res ) {
                                $this->modaccessweights[ $res->mod_name ] = $res->mod_minaccessweight;
                        }
                }
        
                /**
                 * @param $modname
                 * @return bool
                 */
                private function userHasAccessRight ($modname ) {
                        
                        if ( !array_key_exists( $modname, $this->modaccessweights ) ) {
                                return false;
                        }
                        return $this->modaccessweights[ $modname ] <= $this->userweight;
                }
        }

