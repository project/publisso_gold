<?php
/**
 * Provides a 'Publisso Gold Search' Block
 *
 * @Block(
 *   id = "publisso_gold_search_block",
 *   admin_label = @Translation("Publisso Gold Search Block"),
 * )
 */

namespace Drupal\publisso_gold\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\menu_link_content;
use Drupal\publisso_gold\Form\publisso_goldSearch;

class PublissoGoldSearchBlock extends BlockBase implements BlockPluginInterface{
    
    private $modpath;
    private $search_form;

  /**
   * {@inheritdoc}
   */
  public function build() {
    
    $config = $this->getConfiguration();
    
    $this->search_form = \Drupal::formBuilder()->getForm('Drupal\publisso_gold\Form\publisso_goldSearch');
    
    return array(
            '#type' => 'markup',
            '#markup' => '',
            'content' => array(
                $this->search_form
            ),
            '#attached' => [
                'library' => [
                    'publisso_gold/default'
                ]
            ],
            '#cache' => [
                'max-age' => 0
            ]
        );
  }
  
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    
    return $form;
  }
  
   /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
  }
  
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
   
    return array(
   
    );
  }
        
        /**
         * @param null $tmpl
         * @param null $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl = NULL, $vars = NULL){
        
        if($tmpl == NULL || $vars == NULL){
            //set Site-Vars
            $this->tmpl = str_replace(array_keys($this->tmpl_vars), array_values($this->tmpl_vars), $this->tmpl);
            
            //remove unused vars
            $this->tmpl = preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl);
        }
        else{
            //set Site-Vars
            $tmpl = str_replace(array_keys($vars), array_values($vars), $tmpl);
            
            //remove unused vars
            return preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $tmpl);
        }
    }
    
    private function getModuleAccessPermissions(){
        
        $sql = "SELECT * FROM {rwPubgoldModuleaccess}";
        $result = db_query($sql);
        $result = $result->fetchAll();
        
        foreach($result as $res){
            $this->modaccessweights[$res->mod_name] = $res->mod_minaccessweight;
        }
    }
        
        /**
         * @param $modname
         * @return bool
         */
        private function userHasAccessRight($modname){
        
        if(!array_key_exists($modname, $this->modaccessweights)){
            return false;
        }
        return $this->modaccessweights[$modname] <= $this->userweight;
    }
}
