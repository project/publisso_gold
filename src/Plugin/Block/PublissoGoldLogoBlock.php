<?php
/**
 * Provides a 'Publisso Gold Logo' Block
 *
 * @Block(
 *   id = "publisso_gold_logo_block",
 *   admin_label = @Translation("Publisso Gold Logo Block"),
 * )
 */

namespace Drupal\publisso_gold\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\menu_link_content;
use \Drupal\publisso_gold\Controller\Template;
        
        
        class PublissoGoldLogoBlock extends BlockBase implements BlockPluginInterface{

        /**
        * {@inheritdoc}
        */
        public function build() {
                
                $template = new Template();
                $template->get('logo');
                
                $texts          = \Drupal::service('publisso_gold.texts');
                $imagePath      = \Drupal::service('module_handler')->getModule('publisso_gold')->getPath().'/images';
                $lang           = \Drupal::languageManager()->getCurrentLanguage()->getId();
                
                $template->setVar('logo.lnk.publisso'   , $texts->get("logo.lnk.$lang.publisso"                 )) ;
                $template->setVar('logo.lnk.title'      , (string)$this->t($texts->get('logo.lnk.title', 'fc'   )));
                $template->setVar('logo.img.src'        , "/$imagePath/".$texts->get("logo.img.$lang.src"       )) ;
                $template->setVar('logo.img.alt'        , (string)$this->t($texts->get("logo.img.alt", 'ac'     )));
                $template->setVar('gms.logo.img.alt'    , (string)$this->t($texts->get('gms.logo.img.alt', 'ac' )));
                $template->setVar('gms.logo.img.src'    , "/$imagePath/".$texts->get("gms.logo.img.src"         )) ;
                
                return array(
                        
                        '#type' => 'inline_template',
                        '#template' => $template->parse(),
                        '#attached' => [
                                'library' => [
                                        'publisso_gold/default'
                                ]
                        ],
                        '#cache' => [
                                'max-age' => 0
                        ]
                );
        }
  
        /**
        * {@inheritdoc}
        */
        public function blockForm($form, FormStateInterface $form_state) {
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function blockSubmit($form, FormStateInterface $form_state) {
        }

        /**
        * {@inheritdoc}
        */
        public function defaultConfiguration() {
                return [];
        }
}
