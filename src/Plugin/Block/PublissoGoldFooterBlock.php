<?php
/**
 * Provides a 'Publisso Gold Footer' Block
 *
 * @Block(
 *   id = "publisso_gold_footer_block",
 *   admin_label = @Translation("Publisso Gold Footer Block"),
 * )
 */

namespace Drupal\publisso_gold\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\menu_link_content;
use \Drupal\publisso_gold\Controller\Template;
        
        
        class PublissoGoldFooterBlock extends BlockBase implements BlockPluginInterface{

        private $userrole = 'guest';
        private $user = 'guest';
        private $userweight = 10;
        private $user_id = NULL;

        /**
        * {@inheritdoc}
        */
        public function build() {
                
                $tools     = \Drupal::service('publisso_gold.tools');
                $texts     = \Drupal::service('publisso_gold.texts');
                $imagePath = \Drupal::service('module_handler')->getModule('publisso_gold')->getPath().'/images';
                $lang      = \Drupal::languageManager()->getCurrentLanguage()->getId();
                $template  = new Template();
                
                $theme =  (\Drupal::service('theme.manager')->getActiveTheme()->getName());
                $template->get($theme.'_footer');
                if(!$template->isLoaded()) $template->get('publisso_gold_footer');
                
                $links = ['twitter', 'youtube', 'flickr', 'googleplus'];
                foreach($links as $_)
                        $template->setVar("lnk_$_", $texts->get("footer.lnk.$_"));
                
                
                $content = ['legal_notice' => 'afc', 'contact' => 'afc', 'sitemap' => 'afc', 'title' => '', 'totop' => 'al'];
                foreach($content as $_ => $__)
                        $template->setVar("txt_$_", $texts->get("footer.$_", $__));
                
                
                $links = ['legal_notice', 'contact', 'sitemap'];
                foreach($links as $_)
                        $template->setVar("lnk_$_", $texts->get("footer.lnk.$lang.$_"));
                
                $target = \Drupal::service('session')->get('logged_in') ? 'logout' : 'login';
                $urlLogin = Url::fromRoute('publisso_gold.'.$target);
                $lnkLogin = Link::fromTextAndUrl((string)t($texts->get('global.'.$target, 'fc')), $urlLogin);
                $template->setVar('login', $lnkLogin->toString());
                
                $urlPP = Url::fromRoute('publisso_gold.privacy_protection', ['action' => 'view']);
                $lnkPP = Link::fromTextAndUrl($texts->get('footer.privacy_protection', 'fc'), $urlPP);
                $template->setVar('lnk_privacy_protection', $lnkPP->toString());
                
                $template->setVar('txt_cookie_hint', (string)$this->t(
                        $texts->get('footer.txt.cookie_hint'),
                        [
                                '@lnk_privacy_protection' => Link::fromTextAndUrl(
                                        (string)$this->t(
                                                $texts->get('footer.txt.cookie_hint.lnk_privacy_protection', 'fc')
                                        ),
                                        $urlPP
                                )->toString()
                        ]
                ));
                $template->setVar('txt_close', (string)$this->t($texts->get('footer.txt.cookie_hint.btn_agree', 'afc')));
                
                
                $template->setVar('logo.publisso.lnk'            , $texts->get("logo.lnk.$lang.publisso"                  	 )) ;
                $template->setVar('logo.publisso.lnk.title'      , (string)$this->t($texts->get('logo.lnk.'.$lang.'.title', 'fco')));
                $template->setVar('logo.publisso.img.src'        , "/$imagePath/".$texts->get("logo.img.$lang.src"        	 )) ;
                $template->setVar('logo.publisso.img.alt'        , (string)$this->t($texts->get("logo.img.alt", 'ac'             )));
                
                $template->setVar('logo.zbmed.lnk'            , $texts->get("logo.lnk.$lang.zbmed"                           	    )) ;
                $template->setVar('logo.zbmed.lnk.title'      , (string)$this->t($texts->get('logo.lnk.zbmed.'.$lang.'.title', 'fco')));
                $template->setVar('logo.zbmed.img.src'        , "/$imagePath/".$texts->get("logo.zbmed.img.$lang.src"        	    )) ;
                $template->setVar('logo.zbmed.img.alt'        , (string)$this->t($texts->get("logo.img.alt", 'ac'                   )));
                
                $template->setVar('logo.gms.img.src', "/$imagePath/".$texts->get('gms.logo.img.src'		)) ;
                $template->setVar('logo.gms.img.alt', (string)$this->t($texts->get('gms.logo.img.alt', 'fco'	)));
                
                return array(
                        'site' => [
                                '#type' => 'inline_template',
                                '#template' => $template->parse(),
                                '#attached' => [
                                        'library' => [
                                                'publisso_gold/default'
                                        ]
                                ]
                        ],
                        'matomo' => [
                                '#type' => 'inline_template',
                                '#template' => '<img src="https://books.publisso.de/matomo/piwik.php?idsite='.\Drupal::service('publisso_gold.setup')->getValue('matomo.site.id').'&amp;rec=1" style="border:0" alt="" />'
                        ],
                        '#cache' => [
                                'max-age' => 0
                        ]
                );
        }
  
        /**
        * {@inheritdoc}
        */
        public function blockForm($form, FormStateInterface $form_state) {
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function blockSubmit($form, FormStateInterface $form_state) {
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function defaultConfiguration() {
                return [];
        }
}
