<?php
        /**
         * Provides a 'Publisso Gold Social Links Block' Block
         * * @Block(
         *   id = "publisso_gold_social_links",
         *   admin_label = @Translation("Publisso Gold Social Links"),
         * )
         */
        
        namespace Drupal\publisso_gold\Plugin\Block;
        
        
        use Drupal\Core\Block\BlockBase;
        use Drupal\Core\Block\BlockPluginInterface;
        use Drupal\Core\Link;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Publisso;

        class PublissoGoldSocialLinks extends BlockBase implements BlockPluginInterface {
        
                /**
                 * @inheritDoc
                 */
                public function build() {
                        
                        $content = '';
                        
                        if(null !== ($twitterSuffix = Publisso::setup()->getValue('twitter_suffix'))){
                                $content .= '<li><a href="https://twitter.com/'.$twitterSuffix.'" target="_blank"><span class="icon-twitter">Twitter</span></a></li>';
                        }
        
                        if(null !== ($youtubeSuffix = Publisso::setup()->getValue('youtube_suffix'))){
                                $content .= '<li><a href="https://youtube.com/user/'.$youtubeSuffix.'" target="_blank"><span class="icon-youtube">Youtube</span></a></li>';
                        }
                        
                        if($content) $content = "<ul class=\"nav social\">$content</ul>";
                        
                        return ['#markup' => $content];
                }
        }