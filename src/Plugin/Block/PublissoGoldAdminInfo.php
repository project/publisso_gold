<?php
/**
* Provides a 'Publisso Gold AdminInfo' Block
 * * @Block(
 *   id = "publisso_gold_admin_info",
 *   admin_label = @Translation("Publisso Gold Admin Info"),
 * )
*/

namespace Drupal\publisso_gold\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\menu_link_content;
        
        class PublissoGoldAdminInfo extends BlockBase implements BlockPluginInterface{

        /**
        * {@inheritdoc}
        */
        public function build() {

                $session = \Drupal::service('session');
                $texts = \Drupal::service('publisso_gold.texts');
                
                if(!$session->has('user')){
                        
                        return array(
                                '#type' => 'markup',
                                '#markup' => '',
                                '#attached' => [
                                        'library' => [
                                                'publisso_gold/default'
                                        ]
                                ],
                                '#cache' => [
                                        'max-age' => 0
                                ]
                        );
                }
                
                $user = $session->get('user');
                
                $guestInfo = null;
                if($session->has('orig_user')){
                        $orig_user = $session->get('orig_user');
                        
                        $user = new \Drupal\publisso_gold\Controller\User($user['id']);
                        $vars = ['@user.id' => $user->getElement('id')];
                        foreach($user->getKeys() as $_) $vars["@user.$_"] = $user->getElement($_);
                        foreach($user->profile->getKeys() as $_) $vars["@user.profile.$_"] = $user->profile->getElement($_);
                        
                        $guestInfo = [
                                '#markup' => (string)$this->t(
                                        $texts->get('block.admininfo.msg.guestview'),
                                        $vars
                                ),
                                '#prefix' => '<div class="alert alert-success fade in admininfo">',
                                '#suffix' => '</div>'
                        ];
                }
                
                return array(
                        $guestInfo,
                        '#cache' => [
                                'max-age' => 0
                        ]
                );
        }

        /**
        * {@inheritdoc}
        */
        public function blockForm($form, FormStateInterface $form_state) {
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function blockSubmit($form, FormStateInterface $form_state) {
                
//                 $this->setConfigurationValue('title', $form_state->getValue('publisso_gold_block_title'));
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function defaultConfiguration() {
                
//                 $default_config = \Drupal::config('publisso_gold.settings');
//                 
//                 return array(
//                         'title' => $default_config->get('publisso_gold.title')
//                 );
                return [];
        }

}

