<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\SSO.
 */
namespace Drupal\publisso_gold\Controller;

use Drupal\Core\Database\Connection;
        
        /**
         * Class SSO
         * @package Drupal\publisso_gold\Controller
         */
        class SSO {

        private $dbh;
        
        /**
         * @return string
         */
        public function __toString() {
                return 'SSO';
        }
        
        /**
         * @param Connection $database
         */
        public function construct(Connection $database) {
                $this->dbh = $database;
        }
        
        /**
         * @return bool
         */
        public function active() {
                $active = Publisso::setup()->getValue( 'system.sso.active' );
                return ! ! count( array_filter( [
                        'on',
                        '1',
                        'yes'
                ], function ($v) use ($active) {
                        return preg_match( "/\b$active\b/i", $v );
                } ) );
        }
        
        /**
         * @param User $user
         * @return bool
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        public function resetPassword(\Drupal\publisso_gold\Controller\User &$user) {
                if ( ! preg_match( '/^K\d{5,}/', $user->getElement( 'zbmed_kennung' ) ) ) {
                        error_log( 'Password reset unsuccessful! User (ID ' . $user->getElement( 'id' ) . ') has not a valid ZBMED number!' );
                        return false;
                }

                $data = [
                        'method' => 'send_password',
                        'parameters' => [
                                'knr' => $user->getElement( 'zbmed_kennung' ),
                                'sys_id' => 'ZBMED',
                                'portal' => 'PUBLISSO'
                        ],
                        'values' => []
                ];

                $response = \Drupal::httpClient()->request( 'POST', 'https://lvps46-163-114-198.dedicated.hosteurope.de/zbmedauth', [
                        'headers' => [
                                'Content-Type' => 'application/json'
                        ],
                        'http_errors' => false,
                        'verify' => false,
                        'query' => [
                                'data' => base64_encode( json_encode( $data ) )
                        ]
                ] );

                if ( $response->getStatusCode() == 200 ) {

                        $ret = json_decode( $response->getBody()->getContents() );

                        if ( $ret->code == 200 ) {

                                return true;
                        } else {
                                error_log( 'Password reset unsuccessful!: ' . $ret->code . ': ' . $ret->msg );
                                return false;
                        }
                } else {
                        error_log( 'Password reset unsuccessful! SOAP response-code: ' . $response->getStatusCode() );
                        return false;
                }
        }
}
?>
