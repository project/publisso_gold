<?php

namespace Drupal\publisso_gold\Controller\Interfaces;

/**
 * Interface MediumInterface
 * @package Drupal\publisso_gold\Controller\Interfaces
 */
interface MediumInterface {
        const DBTBL_PREFIX = "rwPubgold";
        
        /**
         * MediumInterface constructor.
         * @param null $id
         * @param false $noChilds
         */
        public function __construct($id = null, $noChilds = false);
        
        /**
         * @param $id
         * @param false $loadNoChilds
         * @return mixed
         */
        public function load($id, $loadNoChilds = false);

        public function save();
        
        /**
         * @param string $elemName
         * @return mixed
         */
        public function getElement(string $elemName);

        public function getId();
}
