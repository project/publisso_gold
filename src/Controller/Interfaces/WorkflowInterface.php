<?php
        
        namespace Drupal\publisso_gold\Controller\Interfaces;

        /**
         * Interface WorkflowInterface
         * @package Drupal\publisso_gold\Controller\Interfaces
         */
        interface WorkflowInterface {
                
                const DBTBL_PREFIX = "rwPubgold";
        
                /**
                 * WorkflowInterface constructor.
                 * @param null $id
                 * @param string $tbl_column_prefix
                 * @param bool $loadMedium
                 */
                public function __construct($id = null, $tbl_column_prefix = 'wf_', $loadMedium = true);
        
                /**
                 * @param bool $sendInfoMail
                 * @param false $preliminary
                 * @return mixed
                 */
                public function publish($sendInfoMail = true, $preliminary = false);
        }
