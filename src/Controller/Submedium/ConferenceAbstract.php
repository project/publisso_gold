<?php

        namespace Drupal\publisso_gold\Controller\Submedium;
        use Drupal\Core\Link;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\License;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\SubmissionFundingManager;
        use Drupal\publisso_gold\Controller\Medium\Conference;
        use Drupal\publisso_gold\Controller\Publisso;
        use Exception;

        /**
         * Class ConferenceAbstract
         * @package Drupal\publisso_gold\Controller\Submedium
         */
        class ConferenceAbstract {

                private $id;
                private $elements;
                private $abstractExists;
                private $messenger;
                private $tm;
                private $tbl_column_prefix;
                private $dbh;
                private $fundings;
                private $parentMedium;
                private $sessionWeight;

                public function __construct(int $id){

                        $this->id                = $id;
                        $this->elements          = [];
                        $this->abstractExists    = false;
                        $this->messenger         = \Drupal::messenger();
                        $this->tm                = \Drupal::translation();
                        $this->dbh               = \Drupal::database();
                        $this->tools             = Publisso::tools();
                        $this->tbl_column_prefix = 'ca_';
                        $this->fundings          = [];
                        $this->parentMedium      = null;
                        $this->sessionWeight     = null;

                        return $this->load();
                }

                /**
                 * @return int
                 */
                public function getId() : int{
                        return $this->id;
                }

                /**
                 * @return int
                 */
                public function ID() : int{
                        return $this->getId();
                }

                /**
                 * @return bool
                 */
                public function exists() : bool{
                        return $this->abstractExists;
                }

                /**
                 * @return Conference
                 */
                public function getParentMedium() : Conference{
                        if(!$this->parentMedium)
                                $this->parentMedium = ConferenceManager::getConference($this->elements['cfid']);

                        return $this->parentMedium;
                }

                /**
                 * @param string $ElementName
                 */
                public function getElement(string $ElementName){
                        if($ElementName == 'id') return $this->getId();
                        $data = $this->elements[$ElementName] ?? null;
                        return ($this->tools->isSerialized($data) ? unserialize($data) : $data) ?? null;
                }

                /**
                 * @param string $element
                 * @param $value
                 */
                public function setElement(string $element, $value){
                        if(!(is_scalar($value) || is_null($value))) $value = serialize($value);
                        if($this->hasElement($element)) $this->elements[$element] = $value;
                }

                /**
                 * @param string $ElementName
                 * @param string|null $language
                 * @return bool
                 */
                public function deleteElementTranslations(string $ElementName, string $language = null) : bool{

                        $error = null;

                        try{
                                $qry = $this->dbh->delete('rwPubgoldConferenceAbstractsTranslations');
                                $qry->condition('field', $ElementName, '=')->condition('ca_id', $this->id, '=');
                                if($language) $qry->condition('language', $language, '=');

                                $qry->execute();

                        }catch(Exception $e){
                                $error = $e;
                        }
                        finally{

                                if($error){
                                        Publisso::log('Error in file '.$error->getFile().' line '.$error->getLine().': '.$error->getMessage());
                                        $this->messenger->addError((string)$this->tm->translate('Cant store translation for field "@field" for abstract #@id. See error-log for details.', ['@field' => $ElementName, '@id' => $this->id]));
                                        return false;
                                }

                                return true;
                        }
                }

                public function getFundings() : array{
                        return $this->fundings;
                }

                /**
                 * @param string $ElementName
                 * @param $ElementValue
                 * @param string $language
                 * @return bool
                 */
                public function setElementTranslation(string $ElementName, $ElementValue, string $language) : bool{

                        $error = null;

                        try{
                                $this->dbh->upsert('rwPubgoldConferenceAbstractsTranslations')->fields([
                                        'ca_id' => $this->id,
                                        'language' => $language,
                                        'field' => $ElementName,
                                        'value' => $ElementValue
                                ])->key('ca_id')->key('field')->key('language')->execute();
                        }catch(Exception $e){
                                $error = $e;
                        }
                        finally{

                                if($error){
                                        Publisso::log('Error in file '.$error->getFile().' line '.$error->getLine().': '.$error->getMessage());
                                        $this->messenger->addError((string)$this->tm->translate('Cant store translation for field "@field" for abstract #@id. See error-log for details.', ['@field' => $ElementName, '@id' => $this->id]));
                                        return false;
                                }

                                return true;
                        }
                }

                /**
                 * @return bool
                 */
                public function save() :bool{

                        $fields = [];

                        $this->elements['modified'] = date("y-m-d H:i_s");
                        foreach($this->elements as $k => $v){
                                $fields[$this->tbl_column_prefix.$k] = $v;
                        }

                        $error = null;

                        try {
                                $this->dbh->update( 'rwPubgoldConferenceAbstracts' )->fields( $fields )->condition( $this->tbl_column_prefix . 'id', $this->id, '=' )->execute();
                                $this->dbh->update('rwPubgoldConferenceSessions')->fields(['title' => $this->getElement('title')])->condition('ca_id', $this->getId(), '=')->execute();
                        }
                        catch(Exception $e){
                                $error = $e;
                        }
                        finally{

                                if($error){
                                        Publisso::log('Error in file '.$error->getFile().' line '.$error->getLine().': '.$error->getMessage());
                                        $this->messenger->addError((string)$this->tm->translate('Cant store abstract #@id. See error-log for details.', ['@id' => $this->id]));
                                        return false;
                                }

                                return true;
                        }
                }

                /**
                 * @param string $element
                 * @return bool
                 */
                public function hasElement(string $element) : bool{
                        return array_key_exists($element, $this->elements);
                }

                private function load() :bool{

                        $qry = $this->dbh->select('rwPubgoldConferenceAbstracts', 'a')
                                            ->fields('a', [])
                                            ->condition('ca_id', $this->id, '=');

                        $this->abstractExists = !! $qry->countQuery()->execute()->fetchField();

                        if(!$this->abstractExists){
                                $this->messenger->addError($this->tm->translate('The requested item (abstract #@id) could not be found!', ['@id' => $this->id]));
                                return false;
                        }

                        try {
                                foreach ( $qry->execute()->fetchAssoc() as $k => $v ) {
                                        if ( $k != $this->tbl_column_prefix . 'id' ) // store abstract except id
                                                $this->elements [ str_replace( $this->tbl_column_prefix, '', $k ) ] = $v;
                                }
                                $this->sessionWeight = $this->dbh->select('rwPubgoldConferenceSessions', 't')->fields('t', ['weight'])->condition('ca_id', $this->ID(), '=')->execute()->fetchField();
                        }
                        catch(Exception $e){
                                Publisso::log('Error in file '.$e->getFile().' line '.$e->getLine().': '.$e->getMessage());
                                $this->messenger->addError((string)$this->tm->translate('Cant load abstract #@id. See error-log for details.', ['@id' => $this->id]));
                        }
                        finally{

                                if($this->getElement('ctrlFunding')) $this->fundings = SubmissionFundingManager::getFundings($this->id, 'abstract');
                        }
                        return true;
                }

                /**
                 * @param int $sessionID
                 * @param int|null $weight
                 * @return bool
                 * @throws Exception
                 */
                public function assignSession(int $sessionID, int $weight = null){

                        if(!$weight) {
                                $qry = \Drupal::database()->select( 'rwPubgoldConferenceSessions', 't' );
                                $qry->addExpression( 'case when isnull(MAX(weight)) then 0 else max(weight) end', 'max_weight' );
                                $qry->condition( 'cf_id', $this->elements[ 'cfid' ] );
                                $weight = $qry->execute()->fetchField() + 1;
                        }
                        $qry = \Drupal::database()->insert('rwPubgoldConferenceSessions');
                        $qry->fields(['cf_id' => $this->elements['cfid'], 'type' => 'abstract', 'title' => $this->elements['title'], 'ca_id' => $this->id, 'parent' => $sessionID, 'weight' => $weight + 1]);

                        $qry->execute();

                        $this->setElement('session', $sessionID);
                        return $this->save();
                }

                /**
                 * @param int $sessionID
                 * @param int|null $weight
                 * @return bool
                 */
                public function reassignSession(int $sessionID, int $weight = null) : bool{

                        $fields = ['parent' => $sessionID];
                        if($weight) $fields['weight'] = $weight;

                        \Drupal::database()->update('rwPubgoldConferenceSessions')->fields($fields)->condition('ca_id', $this->id, '=')->execute();

                        $this->setElement('session', $sessionID);
                        return $this->save();
                }

                /**
                 * @param int $sessionID
                 * @return bool
                 */
                public function unassignSession(int $sessionID) : bool{

                        \Drupal::database()->delete('rwPubgoldConferenceSessions')->condition('ca_id', $this->id, '=')->execute();
                        $this->setElement('session', null);
                        return $this->save();
                }

                /**
                 * @return |null
                 */
                public function getSessionParent() :? array{
                        return $this->dbh->select('rwPubgoldConferenceSessions', 't')->fields('t', ['parent'])->condition('ca_id', $this->id, '')->execute()->fetchField() ?? null;
                }

                /**
                 * @return array
                 */
                public function getElementKeys() : array{
                        return array_keys($this->elements);
                }

                public function getCitationNote(bool $forceGen = false) : string{


                        if($this->getElement('citation_note') && !$forceGen) return $this->getElement('citation_note');

                        $date = [];
                        $dateStart = explode('-', $this->getParentMedium()->getElement('date_start'));
                        $dateEnd = explode('-', $this->getParentMedium()->getElement('date_end'));

                        for($i = 0; $i < 3; $i++){
                                if($dateStart[$i] != $dateEnd[$i]) $date['start'][] = $dateStart[$i];
                                $date['end'][] = $dateEnd[$i];
                        }

                        $date = implode('.', array_reverse($date['start'])).(count($date['start']) <= 2 ? '.' : '').'-'.implode('.', array_reverse($date['end']));

                        $data = [
                                $this->getParentMedium()->getElement('inviting_society'),
                                $this->getParentMedium()->getElement('title'),
                                $this->getParentMedium()->getElement('city'),
                                $date ?? null,
                                implode('; ', array_filter([implode(': ', array_filter([$this->getParentMedium()->getElement('publisher_city'), $this->getParentMedium()->getElement('publisher_name')])), $this->getParentMedium()->getElement('publishing_year')])),
                                $publishingInfo ?? null,
                                implode('Doc', (['', $this->getElement('doc_no')]))
                        ];

                        $cn =  implode('. ', array_filter($data));

                        if($this->getElement('published')) $this->setElement('citation_note', $cn);

                        return $cn;
                }

                /**
                 * @param string $type
                 * @param bool $toString
                 * @param array $attributes
                 * @return \Drupal\Core\GeneratedLink|\Drupal\Core\GeneratedUrl|Link|Url|string|null
                 */
                public function getDOILink(string $type = 'url', bool $toString = false, array $attributes = []){

                        if(!$this->getElement('doi')) return null;

                        $url = 'https://dx.doi.org/'.$this->getElement('doi');
                        $url = Url::fromUri($url, ['attributes' => $attributes]);

                        if($type == 'link'){
                                $url = Link::fromTextAndUrl($this->getElement('doi'), $url);
                                if($toString) $url = $url->toString(); return $url;
                        }

                        if($toString) $url = $url->toString(); return $url;
                }

                public function getLicense() :? License{

                        if($this->getElement('license')){
                                return new License($this->getElement('license'));
                        }

                        return null;
                }

                public function getNext() :? Url{

                        $qry = \Drupal::database()->select('rwPubgoldConferenceSessions', 't');
                        $qry->join('rwPubgoldConferenceAbstracts', 'a', 't.ca_id = a.ca_id');
                        $qry->addField('t', 'ca_id');
                        $qry->condition('t.cf_id', $this->getElement('cfid'), '=');
                        $qry->condition('t.weight', $this->sessionWeight, '>');
                        $qry->condition('t.ca_id', $this->ID(), '!=');
                        $qry->isNotNull('a.ca_published');
                        $qry->orderBy('weight', 'DESC');
                        $id = $qry->execute()->fetchField();

                        if($id){
                                return Url::fromRoute('publisso_gold.conferences_conference.abstract', ['cf_id' => $this->getParentMedium()->getId(), 'ca_id' => $id]);
                        }

                        return null;
                }

                public function getPrevious() :? Url{

                        $qry = \Drupal::database()->select('rwPubgoldConferenceSessions', 't');
                        $qry->join('rwPubgoldConferenceAbstracts', 'a', 't.ca_id = a.ca_id');
                        $qry->addField('t', 'ca_id');
                        $qry->condition('t.cf_id', $this->getElement('cfid'), '=');
                        $qry->condition('t.weight', $this->sessionWeight, '<');
                        $qry->condition('t.ca_id', $this->ID(), '!=');
                        $qry->isNotNull('a.ca_published');
                        $qry->orderBy('weight', 'DESC');
                        $id = $qry->execute()->fetchField();

                        if($id){
                                return Url::fromRoute('publisso_gold.conferences_conference.abstract', ['cf_id' => $this->getParentMedium()->getId(), 'ca_id' => $id]);
                        }

                        return null;
                }
        }
