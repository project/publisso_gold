<?php

	/**
	 * @file
	 * Contains \Drupal\publisso_gold\Controller\Setup.
	 */

	namespace Drupal\publisso_gold\Controller;
        
        /**
         * Class Setup
         * @package Drupal\publisso_gold\Controller
         */
        class Setup{
		
		private $table = 'rwPubgoldSetup';
		private $data  = []				 ;
                
                /**
                 * @return string
                 */
                public function __toString(){
			return 'Setup';
		}
		
		public function __construct(){
			
			$result = \Drupal::database()->select($this->table, 't')->fields('t', [])->execute()->fetchAll();
			
			foreach($result as $res){
				$this->data[] = (array)$res;
			}
		}
                
                /**
                 * @param $key
                 * @param false $date
                 * @return mixed
                 */
                public function getValue($key, $date = false ){
                        return $this->get($key, $date, 'stpValue');
		}
                
                /**
                 * @param $key
                 * @param false $date
                 * @return mixed
                 */
                public function getDescription($key, $date = false ){
                        return $this->get($key, $date, 'stpDescription');
		}
                
                /**
                 * @param $key
                 * @param $date
                 * @param $var
                 * @return mixed
                 */
                private function get($key, $date, $var){
                        
                        $$var = $lastdate = false;
			
			foreach( $this->data as $setup ){
				
				if( array_key_exists('stpKey', $setup) ){
					
					if( $setup[ 'stpKey' ] == $key ){
						
						if( $lastdate === false || strtotime($lastdate) < strtotime($setup[ 'stpValidFrom' ]) ){
							
							if( ( $date !== false && strtotime($date) >= strtotime($setup[ 'stpValidFrom' ]) ) || $date === false ){
								
								$$var = $setup[ $var ];
								$lastdate = $setup[ 'stpValidFrom' ];
								continue;
							}
						}
					}
				}
			}
			
			return $$var;
		}
                
                /**
                 * @param $key
                 * @param $value
                 * @param null $description
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 * @throws \Exception
                 */
                public function createEntryForKey($key, $value, $description = null){
                        
                        return \Drupal::database()->insert('rwPubgoldSetup')->fields([
                                'stpKey'         => $key,
                                'stpDescription' => $description,
                                'stpValue'       => $value,
                                'stpValidFrom'   => date('Y-m-d H:i:s')
                        ])->execute();
		}
                
                /**
                 * @param $key
                 * @param null $description
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 */
                public function updateDescriptionForKey($key, $description = null){
                        
                        error_log('Update DESC for "'.$this->getCurrentKeyID($key).'"');
                        
                        return \Drupal::database()->update('rwPubgoldSetup')->fields([
                                'stpDescription' => $description
                        ])->condition('id', $this->getCurrentKeyID($key), '=')->execute();
		}
                
                /**
                 * @return array
                 */
                public function getKeys(){
			
			$keys = [];
			
			foreach( $this->data as $setup ){
				
				if( array_key_exists('stpKey', $setup) && !in_array($setup['stpKey'], $keys)){
					$keys[] = $setup['stpKey'];
				}
			}
			
			return $keys;
		}
                
                /**
                 * @param $key
                 * @return mixed
                 */
                private function getCurrentKeyID($key){
                        
                        return \Drupal::database()->select('rwPubgoldSetup', 't')->fields('t', ['id'])
                                ->condition('stpKey', $key, '=')
                                ->range(0, 1)->orderBy('stpValidFrom', 'DESC')->execute()->fetchField();
		}
	}
?>
