<?php
        
        /**
         * @file
         * Contains \Drupal\publisso_gold\Controller\Journal.
         */
        
        namespace Drupal\publisso_gold\Controller;
        
        /**
         * Class Journal
         * @package Drupal\publisso_gold\Controller
         */
        class Journal
        {
                
                private $id;
                private $table             = 'rwPubgoldJournals';
                private $tbl_column_prefix = 'jrn_';
                private $elements          = [];
                private $editors           = [];
                private $editorsinchief    = [];
                private $authors           = [];
                private $sustainingmembers = [];
                private $reviewers         = [];
                private $editorialoffice   = [];
                private $chapters          = [];
                private $editorialboard    = [];
                private $advisoryboard     = [];
                private $eb_ab_annotations = [];
                private $tableofcontent    = false;
                private $processes         = [];
                private $cover_link        = '';
                private $volumes           = [];
                private $noChilds          = false;
        
                /**
                 * @return string
                 */
                public function __toString ()
                {
                        return "class journal";
                }
        
                /**
                 * Journal constructor.
                 * @param null $id
                 * @param false $noChilds
                 */
                public function __construct ($id = null, $noChilds = false )
                {
                        
                        $this->noChilds = $noChilds;
                        $this->id = $id;
                        $this->load( $this->id );
                }
        
                /**
                 * @param $title
                 * @throws \Exception
                 */
                public function create ($title )
                {
                        $this->id = \Drupal::database()->insert( $this->table )->fields( [ $this->tbl_column_prefix . 'title' => $title ] )->execute();
                        $this->load();
                }
        
                /**
                 * @param $elemName
                 * @param null $elemValue
                 * @param bool $storeOnDB
                 * @return bool
                 */
                public function setElement ($elemName, $elemValue = null, $storeOnDB = true )
                {
                        
                        if ( array_key_exists( $elemName, $this->elements ) ) {
                                
                                $this->elements[ $elemName ] = $elemValue;
                                
                                if ( $storeOnDB === false ) return true;
                                
                                $this->save();
                                $this->load();
                        }
                        else {
                                return false;
                        }
                }
        
                /**
                 * @param $elemName
                 * @return mixed|null
                 */
                public function getElement ($elemName )
                {
                        if ( $elemName == 'id' ) return $this->id;
                        return $this->elements[ $elemName ] ?? null;
                }
        
                /**
                 * @param null $elemName
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 */
                public function save ($elemName = null )
                {
                        
                        $fields = $this->elements;
                        
                        if ( $elemName !== null ) { //if specific element selected
                                $fields = [ $elemName => $fields[ $elemName ] ];
                        }
                        
                        foreach ( $fields as $k => $var ) {
                                $fields[ $this->tbl_column_prefix . $k ] = $var;
                                unset( $fields[ $k ] );
                        }
                        
                        //serialize unserialized data
                        $keys = [ 'logo', 'cover', 'control' ];
                        foreach ( $keys as $key ) $fields[ $this->tbl_column_prefix . $key ] = serialize( $fields[ $this->tbl_column_prefix . $key ] );
                        
                        //echo '<pre>'.print_r($fields, 1).'</pre>'; exit();
                        return \Drupal::database()->update( $this->table )->fields( $fields )->condition( $this->tbl_column_prefix . 'id', $this->id )->execute();
                }
        
                /**
                 * @return mixed|string|null
                 */
                public function getCoverLink ()
                {
                        return empty( $this->elements[ 'cover_link' ] ) ? $this->cover_link : $this->getElement( 'cover_link' );
                }
        
                /**
                 * @param false $noChilds
                 * @return false
                 */
                private function load ($noChilds = false )
                {
                        
                        if ( $this->id ) {
                                
                                $qry = \Drupal::database()->select( $this->table, 'b' )->fields( 'b', [] )->condition( $this->tbl_column_prefix . 'id', $this->id, '=' );
                                
                                if ( $qry->countQuery()->execute()->fetchField() == 0 ) {
                                        $this->id = null;
                                        return false;
                                }
                                
                                $journal = $qry->execute()->fetchAssoc();
                                
                                foreach ( $journal as $k => $v ) {
                                        
                                        if ( $k != $this->tbl_column_prefix . 'id' ) //jrn book except id
                                                $this->elements[ str_replace( $this->tbl_column_prefix, '', $k ) ] = $v;
                                }
                                
                                if ( empty( $this->getElement( 'cover_link' ) ) ) {
                                        $this->cover_link = \Drupal\Core\Url::fromRoute( 'publisso_gold.journals_journal', [ 'jrn_id' => $this->getElement( 'id' ) ] )->toString();
                                }
                                
                                //unserialize serialized data
                                $keys = [ 'logo', 'cover', 'control' ];
                                foreach ( $keys as $key ) $this->elements[ $key ] = unserialize( $this->elements[ $key ] );
                                
                                if ( !$this->noChilds ) {
                                        $this->getEditors();
                                        $this->getEditorialOffice();
                                        $this->getEditorsInChief();
                                        $this->getReviewers();
                                        //$this->getSustainingMembers();
                                        //$this->getAuthors();
                                        //$this->getChapters();
                                        $this->parseEditorialBoard();
                                        //$this->loadProcesses();
                                        //$this->loadMailtemplates();
                                        
                                        //$this->tableofcontent = new TableOfContent(null, ['medium' => 'book', 'id' => $this->id]);
                                        $this->loadVolumes();
                                }
                                
                        }
                }
                
                private function loadVolumes ()
                {
                        $this->volumes = [];
                        $this->volumes = \Drupal::database()->select( 'rwPubgoldJournalVolume', 't' )->fields( 't', [ 'id' ] )->condition( 'jrn_id', $this->id, '=' )->orderBy( 'year', 'ASC' )->orderBy( 'number', 'ASC' )->execute()->fetchCol();
                }
        
                /**
                 * @return array
                 */
                public function getVolumes ()
                {
                        return $this->volumes;
                }
        
                /**
                 * @param $id
                 * @return bool
                 */
                public function hasVolume($id){
                        return in_array($id, $this->volumes);
                }
        
                /**
                 * @param $vol_id
                 * @return Volume|false
                 */
                public function getVolume ($vol_id )
                {
                        if ( !in_array( $vol_id, $this->volumes ) ) return false;
                        return new \Drupal\publisso_gold\Controller\Volume( $vol_id );
                }
        
                /**
                 * @return bool
                 */
                public function hasVolumes ()
                {
                        return !!count( $this->volumes );
                }
        
                /**
                 * @return mixed
                 */
                public function getCurrentVolumeID ()
                {
                        return \Drupal::database()->select( 'rwPubgoldJournalVolume', 't' )->fields( 't', [ 'id' ] )->condition( 'jrn_id', $this->id, '=' )->orderBy( 'year', 'DESC' )->orderBy( 'number', 'DESC' )->execute()->fetchField();
                }
        
                /**
                 * @return Volume
                 */
                public function getCurrentVolume ()
                {
                        return new \Drupal\publisso_gold\Controller\Volume( $this->getCurrentVolumeID()
                        );
                }
                
                private function parseEditorialBoard ()
                {
                        
                        $eb = json_decode( $this->getElement( 'editorial_board' ) );
                        $eb = (array)$eb;
                        
                        if ( !array_key_exists( 'main', $eb ) ) {
                                
                                uasort( $eb, [ 'self', 'sortEditorialBoard' ] );
                                
                                foreach ( $eb as $uid => $data ) {
                                        
                                        $user = new User( $uid );
                                        if ( !$user->profile->getElement( 'show_data_in_boards' ) ) continue;
                                        
                                        if ( $data->show_in_editorial_board == 1 ) {
                                                
                                                $this->editorialboard[ $uid ] = $user;
                                                
                                                foreach ( $data as $key => $show ) {
                                                        
                                                        if ( $show == 0 ) {
                                                                //disable data not shown, but don't store in database
                                                                $this->editorialboard[ $uid ]->profile->setElement( str_replace( 'profile_', '', $key ), '', false );
                                                        }
                                                }
                                        }
                                }
                        }
                        else {
                                
                                $ab = (array)$eb[ 'advisory' ];
                                $eb[ 'main' ] = (array)$eb[ 'main' ];
                                $this->eb_ab_annotations[ 'main' ] = $eb[ 'annotation' ];
                                uasort( $eb[ 'main' ], [ 'self', 'sortEditorialBoard' ] );
                                
                                foreach ( $eb[ 'main' ] as $uid => $data ) {
                                        
                                        $user = new User( $uid );
                                        if ( !$user->profile->getElement( 'show_data_in_boards' ) ) continue;
                                        
                                        if ( $data->show_in_editorial_board == 1 ) {
                                                
                                                $this->editorialboard[ $uid ] = $user;
                                                $this->editorialboard[ $uid ]->tempInfo[ 'eb_notes' ] = $data->notes;
                                                
                                                foreach ( $data as $key => $show ) {
                                                        
                                                        if ( $show == 0 ) {
                                                                //disable data not shown, but don't store in database
                                                                $this->editorialboard[ $uid ]->profile->setElement( str_replace( 'profile_', '', $key ), '', false );
                                                        }
                                                }
                                        }
                                }
                                
                                //advisory boards
                                foreach ( $ab as $advisoryBoard ) {
                                        
                                        $advisoryBoard = (array)$advisoryBoard;
                                        
                                        $user = $advisoryBoard[ 'user' ];
                                        $name = $advisoryBoard[ 'name' ];
                                        $this->eb_ab_annotations[ $name ] = $advisoryBoard[ 'annotation' ];
                                        //echo '<pre>'.print_r($user, 1).'</pre>';
                                        uasort( $user, [ 'self', 'sortEditorialBoard' ] );
                                        //echo '<pre>'.print_r($user, 1).'</pre>';
                                        $this->advisoryboard[ $name ] = [];
                                        
                                        foreach ( $user as $uid => $data ) {
                                                
                                                $user = new User( $uid );
                                                if ( !$user->profile->getElement( 'show_data_in_boards' ) ) continue;
                                                
                                                if ( $data->show_in_editorial_board == 1 ) {
                                                        
                                                        $this->advisoryboard[ $name ][ $uid ] = $user;
                                                        $this->advisoryboard[ $name ][ $uid ]->tempInfo[ 'eb_notes' ] = $data->notes;
                                                        
                                                        foreach ( $data as $key => $show ) {
                                                                
                                                                if ( $show == 0 ) {
                                                                        //disable data not shown, but don't store in database
                                                                        $this->advisoryboard[ $name ][ $uid ]->profile->setElement( str_replace( 'profile_', '', $key ), '', false );
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                }
        
                /**
                 * @return array
                 */
                public function readEditorialBoard ()
                {
                        return $this->editorialboard;
                }
        
                /**
                 * @return array
                 */
                public function readAdvisoryBoard ()
                {
                        return $this->advisoryboard;
                }
        
                /**
                 * @param string $name
                 * @return mixed|null
                 */
                public function getEbAbAnnotation ($name = '' )
                {
                        return $this->eb_ab_annotations[ $name ] ?? null;
                }
        
                /**
                 * @param $uid
                 * @throws \Exception
                 */
                public function addEditor ($uid )
                {
                        
                        if ( $uid ) {
                                if ( !\Drupal::database()->select( 'rwPubgoldJournalEditors', 't' )->fields( 't', [] )->condition( 'je_uid', $uid, '=' )->condition( 'je_journalid', $this->getElement( 'id' ), '=' )->countQuery()->execute()->fetchField() ) {
                                        \Drupal::database()->insert( 'rwPubgoldJournalEditors' )->fields( [ 'je_uid' => $uid, 'je_journalid' => $this->getElement( 'id' ) ] )->execute();
                                }
                                
                                $this->getEditors();
                        }
                }
        
                /**
                 * @param $uid
                 * @return bool
                 */
                public function isUserEditor ($uid )
                {
                        
                        foreach ( $this->editors as $user ) {
                                
                                if ( $user->getElement( 'id' ) == $uid ) return true;
                        }
                }
        
                /**
                 * @param array $editor_uids
                 * @throws \Exception
                 */
                public function setEditors (array $editor_uids )
                {
                        
                        if ( is_array( $editor_uids ) ) {
                                
                                \Drupal::database()->delete( 'rwPubgoldJournalEditors' )->condition( 'je_journalid', $this->id, '=' )->execute();
                                
                                foreach ( $editor_uids as $uid ) \Drupal::database()->insert( 'rwPubgoldJournalEditors' )->fields( [ 'je_journalid' => $this->id, 'je_uid' => $uid ] )->execute();
                                
                                $this->getEditors();
                        }
                }
                
                private function getEditors ()
                {
                        
                        $editors = \Drupal::database()->select( 'rwPubgoldJournalEditors', 'be' )->fields( 'be', [] )->condition( 'je_journalid', $this->id, '=' )->execute()->fetchAll();
                        
                        $this->editors = [];
                        
                        foreach ( $editors as $editor ) {
                                
                                $this->editors[] = new User( $editor->je_uid );
                        }
                }
        
                /**
                 * @return array
                 */
                public function readEditors ()
                {
                        return $this->editors;
                }
        
                /**
                 * @param $uid
                 * @return bool
                 */
                public function isUserEditorialOffice ($uid )
                {
                        
                        foreach ( $this->editorialoffice as $user ) {
                                
                                if ( $user->getElement( 'id' ) == $uid ) return true;
                        }
                }
        
                /**
                 * @param $uid
                 * @throws \Exception
                 */
                public function addEditorialOffice ($uid )
                {
                        
                        if ( $uid ) {
                                if ( !\Drupal::database()->select( 'rwPubgoldJournalsEditorialOffice', 't' )->fields( 't', [] )->condition( 'jeo_uid', $uid, '=' )->condition( 'jeo_jrnid', $this->getElement( 'id' ), '=' )->countQuery()->execute()->fetchField() ) {
                                        \Drupal::database()->insert( 'rwPubgoldJournalsEditorialOffice' )->fields( [ 'jeo_uid' => $uid, 'jeo_jrnid' => $this->getElement( 'id' ) ] )->execute();
                                }
                                
                                $this->getEditorialOffice();
                        }
                }
                
                private function getEditorialOffice ()
                {
                        
                        $editors = \Drupal::database()->select( 'rwPubgoldJournalsEditorialOffice', 'beo' )->fields( 'beo', [] )->condition( 'jeo_jrnid', $this->id, '=' )->execute()->fetchAll();
                        
                        $this->editorialoffice = [];
                        
                        foreach ( $editors as $editor ) {
                                $this->editorialoffice[] = new User( $editor->jeo_uid );
                        }
                }
        
                /**
                 * @param array $eo_uids
                 * @throws \Exception
                 */
                public function setEditorialOffice (array $eo_uids )
                {
                        
                        if ( is_array( $eo_uids ) ) {
                                
                                \Drupal::database()->delete( 'rwPubgoldJournalsEditorialOffice' )->condition( 'jeo_jrnid', $this->id, '=' )->execute();
                                
                                foreach ( $eo_uids as $uid ) \Drupal::database()->insert( 'rwPubgoldJournalsEditorialOffice' )->fields( [ 'jeo_jrnid' => $this->id, 'jeo_uid' => $uid ] )->execute();
                                
                                $this->getEditorialOffice();
                        }
                }
        
                /**
                 * @return array
                 */
                public function readEditorialOffice ()
                {
                        return $this->editorialoffice;
                }
        
                /**
                 * @param $uid
                 * @return bool
                 */
                public function isUserEditorInChief ($uid )
                {
                        
                        foreach ( $this->editorsinchief as $user ) {
                                
                                if ( $user->getElement( 'id' ) == $uid ) return true;
                        }
                }
        
                /**
                 * @param $uid
                 * @throws \Exception
                 */
                public function addEditorsInChief ($uid )
                {
                        
                        if ( $uid ) {
                                if ( !\Drupal::database()->select( 'rwPubgoldJournalsEditorsInChief', 't' )->fields( 't', [] )->condition( 'jeic_uid', $uid, '=' )->condition( 'jeic_jrnid', $this->getElement( 'id' ), '=' )->countQuery()->execute()->fetchField() ) {
                                        \Drupal::database()->insert( 'rwPubgoldJournalsEditorsInChief' )->fields( [ 'jeic_uid' => $uid, 'jeic_jrnid' => $this->getElement( 'id' ) ] )->execute();
                                }
                                
                                $this->getEditorsInChief();
                        }
                }
                
                private function getEditorsInChief ()
                {
                        
                        $editors = \Drupal::database()->select( 'rwPubgoldJournalsEditorsInChief', 'beic' )->fields( 'beic', [] )->condition( 'jeic_jrnid', $this->id, '=' )->execute()->fetchAll();
                        
                        $this->editorsinchief = [];
                        
                        foreach ( $editors as $editor ) {
                                
                                $this->editorsinchief[] = new User( $editor->jeic_uid );
                        }
                }
        
                /**
                 * @param array $eic_uids
                 * @throws \Exception
                 */
                public function setEditorsInChief (array $eic_uids )
                {
                        
                        if ( is_array( $eic_uids ) ) {
                                
                                \Drupal::database()->delete( 'rwPubgoldJournalsEditorsInChief' )->condition( 'jeic_jrnid', $this->id, '=' )->execute();
                                
                                foreach ( $eic_uids as $uid ) \Drupal::database()->insert( 'rwPubgoldJournalsEditorsInChief' )->fields( [ 'jeic_jrnid' => $this->id, 'jeic_uid' => $uid ] )->execute();
                                
                                $this->getEditorsInChief();
                        }
                }
        
                /**
                 * @return array
                 */
                public function readEditorsInChief ()
                {
                        return $this->editorsinchief;
                }
        
                /**
                 * @param $uid
                 * @throws \Exception
                 */
                public function addReviewer ($uid )
                {
                        
                        if ( $uid ) {
                                if ( !\Drupal::database()->select( 'rwPubgoldJournalReviewers', 't' )->fields( 't', [] )->condition( 'jr_uid', $uid, '=' )->condition( 'jr_journalid', $this->getElement( 'id' ), '=' )->countQuery()->execute()->fetchField() ) {
                                        \Drupal::database()->insert( 'rwPubgoldJournalReviewers' )->fields( [ 'jr_uid' => $uid, 'jr_journalid' => $this->getElement( 'id' ) ] )->execute();
                                }
                                
                                $this->getReviewers();
                        }
                }
        
                /**
                 * @param $uid
                 * @return bool
                 */
                public function isUserReviewer ($uid )
                {
                        
                        foreach ( $this->reviewers as $user ) {
                                
                                if ( $user->getElement( 'id' ) == $uid ) return true;
                        }
                }
                
                private function getReviewers ()
                {
                        
                        $editors = \Drupal::database()->select( 'rwPubgoldJournalReviewers', 'jr' )->fields( 'jr', [] )->condition( 'jr_journalid', $this->id, '=' )->execute()->fetchAll();
                        
                        $this->reviewers = [];
                        
                        foreach ( $editors as $editor ) {
                                
                                $this->reviewers[] = new User( $editor->jr_uid );
                        }
                }
        
                /**
                 * @param array $reviewer_uids
                 * @throws \Exception
                 */
                public function setReviewers (array $reviewer_uids )
                {
                        
                        if ( is_array( $reviewer_uids ) ) {
                                
                                \Drupal::database()->delete( 'rwPubgoldJournalReviewers' )->condition( 'jr_journalid', $this->id, '=' )->execute();
                                
                                foreach ( $reviewer_uids as $uid ) \Drupal::database()->insert( 'rwPubgoldJournalReviewers' )->fields( [ 'jr_journalid' => $this->id, 'jr_uid' => $uid ] )->execute();
                                
                                $this->getReviewers();
                        }
                }
        
                /**
                 * @return array
                 */
                public function readReviewers ()
                {
                        return $this->reviewers;
                }
        
                /**
                 * @param $uid
                 * @return bool
                 */
                public function isUserAuthor ($uid )
                {
                        
                        foreach ( $this->authors as $user ) {
                                
                                if ( $user->getElement( 'id' ) == $uid ) return true;
                        }
                }
        
                /**
                 * @param $uid
                 * @throws \Exception
                 */
                public function addAuthor ($uid )
                {
                        
                        if ( $uid ) {
                                if ( !\Drupal::database()->select( 'rwPubgoldBookAuthors', 't' )->fields( 't', [] )->condition( 'ba_uid', $uid, '=' )->condition( 'ba_bookid', $this->getElement( 'id' ), '=' )->countQuery()->execute()->fetchField() ) {
                                        \Drupal::database()->insert( 'rwPubgoldBookAuthors' )->fields( [ 'ba_uid' => $uid, 'ba_bookid' => $this->getElement( 'id' ) ] )->execute();
                                }
                                
                                $this->getAuthors();
                        }
                }
                
                private function getAuthors ()
                {
                        
                        $result = \Drupal::database()->select( 'rwPubgoldBookAuthors', 'ba' )->fields( 'ba', [] )->condition( 'ba_bookid', $this->id, '=' )->execute()->fetchAll();
                        
                        $this->authors = [];
                        
                        foreach ( $result as $_ ) {
                                $this->authors[] = new User( $_->ba_uid );
                        }
                }
        
                /**
                 * @param array $author_uids
                 * @throws \Exception
                 */
                public function setAuthors (array $author_uids )
                {
                        
                        if ( is_array( $author_uids ) ) {
                                
                                \Drupal::database()->delete( 'rwPubgoldBookAuthors' )->condition( 'ba_bookid', $this->id, '=' )->execute();
                                
                                foreach ( $author_uids as $uid ) \Drupal::database()->insert( 'rwPubgoldBookAuthors' )->fields( [ 'ba_bookid' => $this->id, 'ba_uid' => $uid ] )->execute();
                                
                                $this->getReviewers();
                        }
                }
        
                /**
                 * @return array
                 */
                public function readAuthors ()
                {
                        return $this->authors;
                }
                
                private function getSustainingMembers ()
                {
                        
                        $result = \Drupal::database()->select( 'rwPubgoldBooksSustainingMembers', 'bsm' )->fields( 'bsm', [] )->condition( 'bsm_bkid', $this->id, '=' )->execute()->fetchAll();
                        
                        $this->sustainingmembers = [];
                        
                        foreach ( $result as $_ ) $this->sustainingmembers[] = $_->bsm_id;
                }
        
                /**
                 * @param null[] $data
                 * @throws \Exception
                 */
                private function setSustainingMember ($data = ['image' => null, 'type' => null ] )
                {
                        
                        if ( array_key_exists( 'image', $data ) && $data[ 'image' ] != null && array_key_exists( 'type', $data ) && $data[ 'type' ] != null ) {
                                
                                \Drupal::database()->insert( 'rwPubgoldBooksSustainingMembers' )->fields( [ 'bsm_bkid' => $this->id, 'bsm_image' => $data[ 'image' ], 'type' => $data[ 'type' ] ] )->execute();
                                
                                $this->getSustainingMembers();
                        }
                }
        
                /**
                 * @return array
                 */
                public function readSustainingMembers ()
                {
                        return $this->sustainingmembers;
                }
        
                /**
                 * @return mixed
                 */
                public function readPublicEditors ()
                {
                        return $this->public_editors;
                }
                
                private function getChapters ()
                {
                        
                        $result = \Drupal::database()->select( 'rwPubgoldBookChapters', 'bc' )->fields( 'bc', [ 'cp_id' ] )->condition( 'cp_bkid', $this->id, '=' )->execute()->fetchAll();
                        
                        $this->chapters = [];
                        
                        foreach ( $result as $_ ) $this->chapters[] = new Bookchapter( $_->cp_id );
                }
        
                /**
                 * @return array
                 */
                public function readChapters ()
                {
                        return $this->chapters;
                }
        
                /**
                 * @param $cp_id
                 * @return false|mixed
                 */
                public function readChapter ($cp_id )
                {
                        
                        if ( $cp_id ) {
                                foreach ( $this->chapters as $chapter ) {
                                        if ( $chapter->getElement( 'id' ) == $cp_id ) {
                                                return $chapter;
                                        }
                                }
                        }
                        
                        return false;
                }
        
                /**
                 * @param $author_uid
                 * @return bool
                 */
                public function hasAuthor ($author_uid )
                {
                        
                        foreach ( $this->authors as $author ) {
                                if ( $author->getElement( 'id' ) == $author_uid ) return true;
                        }
                        
                        return false;
                }
        
                /**
                 * @return int|void
                 */
                public function countAuthors ()
                {
                        return count( $this->authors );
                }
                
                private function loadProcesses ()
                {
                        
                        $result = \Drupal::database()->select( 'rwPubgoldProcesses', 't' )->fields( 't', [] )->execute()->fetchAll();
                        
                        foreach ( $result as $res ) {
                                $this->processes[ $res->pc_id ][ 'name' ] = $res->pc_name;
                        }
                }
                
                private function loadMailtemplates ()
                {
                        
                        foreach ( $this->processes as $pc_id => $process ) {
                                
                                $result = \Drupal::database()->select( 'rwPubgoldMailassignement', 't' )->fields( 't', [ 'ma_mailtemplate_id', 'ma_attachment' ] )->condition( 'ma_medium_type', 'journal', '=' )->condition( 'ma_medium_id', $this->id, '=' )->condition( 'ma_process_id', $pc_id, '=' )->execute()->fetchAssoc();
                                
                                $attachment = [];
                                
                                if ( !empty( $result[ 'ma_mailtemplate_id' ] ) ) {
                                        
                                        if ( !empty( $result[ 'ma_attachment' ] ) ) $attachment = array_filter( json_decode( $result[ 'ma_attachment' ], true ) );
                                        
                                        $result = \Drupal::database()->select( 'rwPubgoldMailtemplates', 't' )->fields( 't', [] )->condition( 'id', $result[ 'ma_mailtemplate_id' ], '=' )->execute()->fetchAssoc();
                                        
                                        $this->processes[ $pc_id ][ 'mailout' ] = $result;
                                        $this->processes[ $pc_id ][ 'mailout' ][ 'attachment' ] = $attachment;
                                }
                                else {
                                        
                                        $result = \Drupal::database()->select( 'rwPubgoldMailassignement', 't' )->fields( 't', [ 'ma_mailtemplate_id', 'ma_attachment' ] )->condition( 'ma_medium_type', 'default', '=' )->condition( 'ma_process_id', $pc_id, '=' )->execute()->fetchAssoc();
                                        
                                        if ( !empty( $result[ 'ma_attachment' ] ) ) $attachment = array_filter( json_decode( $result[ 'ma_attachment' ], true ) );
                                        
                                        if ( !empty( $result[ 'ma_mailtemplate_id' ] ) ) {
                                                
                                                $result = \Drupal::database()->select( 'rwPubgoldMailtemplates', 't' )->fields( 't', [] )->condition( 'id', $result[ 'ma_mailtemplate_id' ], '=' )->execute()->fetchAssoc();
                                                
                                                $this->processes[ $pc_id ][ 'mailout' ] = $result;
                                                $this->processes[ $pc_id ][ 'mailout' ][ 'attachment' ] = $attachment;
                                        }
                                }
                        }
                }
        
                /**
                 * @param $process_name
                 * @return false|mixed
                 */
                public function getMailout ($process_name )
                {
                        
                        foreach ( $this->processes as $pid => $process ) {
                                
                                if ( $process[ 'name' ] == $process_name ) {
                                        
                                        if ( array_key_exists( 'mailout', $process ) ) {
                                                return $process[ 'mailout' ];
                                        }
                                        else {
                                                return false;
                                        }
                                }
                        }
                        
                        return false;
                }
        
                /**
                 * @param $a
                 * @param $b
                 * @return int
                 */
                private static function sortEditorialBoard ($a, $b )
                {
                        $aw = $a->weight;
                        $bw = $b->weight;
                        //echo '<pre>'.print_r($aw, 1).'</pre>';
                        //echo '<pre>'.print_r($bw, 1).'</pre>';
                        if ( $aw == $bw ) {
                                return 0;
                        }
                        
                        return ( $aw > $bw ) ? +1 : -1;
                }
        
                /**
                 * @param $element_name
                 * @return mixed
                 */
                public function getControlElement ($element_name )
                {
                        
                        $control = ( $this->elements[ 'control' ] );
                        return $control[ $element_name ];
                }
        
                /**
                 * @param $element_name
                 * @param $element_value
                 */
                public function setControlElement ($element_name, $element_value )
                {
                        $control = json_decode( $this->elements[ 'control' ], true );
                        $control[ $element_name ] = $element_value;
                        $this->setElement( 'control', json_encode( $control ) );
                }
        
                /**
                 * @return array
                 */
                public function getControlKeys ()
                {
                        return array_keys( json_decode( $this->getElement( 'control' ), true ) );
                }
        
                /**
                 * @return array
                 */
                public function getDataKeys ()
                {
                        return array_keys( $this->elements );
                }
        
                /**
                 * @return array
                 */
                public function getKeys ()
                {
                        return array_keys( $this->elements );
                }
        
                /**
                 * @param $name
                 */
                public static function test ($name )
                {
                        print '[[' . $name . ']]';
                }
        }
        
        


?>
