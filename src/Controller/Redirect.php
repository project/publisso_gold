<?php
        
        
        namespace Drupal\publisso_gold\Controller;
        
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Routing\TrustedRedirectResponse;

        /**
         * Class Redirect
         * @package Drupal\publisso_gold\Controller
         */
        class Redirect extends ControllerBase {
                
                private $texts;
                
                public function __construct(){
                        $this->texts = \Drupal::service('publisso_gold.texts');
                }
        
                /**
                 * @return TrustedRedirectResponse
                 */
                public function home(){
                        return new TrustedRedirectResponse('publisso_gold.books', 301);
                }
        
                /**
                 * @param $name
                 * @return TrustedRedirectResponse
                 */
                public function send($name){
                        $lang = \drupal::service('language_manager')->getCurrentLanguage()->getId();
                        return new TrustedRedirectResponse($this->texts->get("route.extern.$name.$lang"), 301);
                }
        
                /**
                 * @param $bk_id
                 * @return \Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function bookOld2New($bk_id){
                        return $this->redirect('publisso_gold.books_book', ['bk_id' => $bk_id], [], 301);
                }
        
                /**
                 * @param $bk_id
                 * @param $cp_id
                 * @return \Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function chapter_old($bk_id, $cp_id){
                        return $this->redirect('publisso_gold.book.chapter', ['bk_id' => $bk_id, 'cp_id' => $cp_id], [], 301);
                }
        }
