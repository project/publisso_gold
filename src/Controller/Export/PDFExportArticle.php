<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Controller\Export.
         */

        namespace Drupal\publisso_gold\Controller\Export;

        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\ArticleManager;
        use Drupal\publisso_gold\Controller\Manager\LicenseManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Symfony\Component\HttpFoundation\RequestStack;
        use \Symfony\Component\HttpFoundation\Response;
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Database\Connection;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Symfony\Component\HttpFoundation\BinaryFileResponse;
        use Symfony\Component\HttpFoundation\Request;
        use Symfony\Component\HttpFoundation\ResponseHeaderBag;

        use \Drupal\publisso_gold\Controller\Blob;


        /**
         * Class PDFExportArticle
         * @package Drupal\publisso_gold\Controller\Export
         */
        class PDFExportArticle extends ControllerBase {

                protected $session;
                protected $pdftk;
                protected $license;
                protected $item;
                protected $outParams;
                protected $volatile;
                protected $force_new;
                protected $request;

                /**
                 * @param ContainerInterface $container
                 * @return PDFExportArticle|static
                 */
                public static function create (ContainerInterface $container ) {
                        \Drupal::service( 'page_cache_kill_switch' )->trigger();
                        return new static( $container->get( 'database' ), $container->get( 'request_stack' ) );
                }

                public function __construct ( Connection $database, RequestStack $request ) {

                        \Drupal::service( 'publisso_gold.tools' )->forceUserAction();
                        $this->session = \Drupal::service( 'session' );

                        if ( !$this->session->isStarted() ) {
                                $this->session->migrate();
                        }

                        $this->session->set( 'lastaccess', time() );

                        if ( empty( $this->pdftk = exec( 'which pdftk' ) ) ) {
                                $this->pdftk = false;
                        }

                        $this->outParams = [];
                        $this->volatile = false;
                        $this->force_new = false;
                        $this->request = $request->getCurrentRequest();
                }

                /**
                 * @param Request $request
                 * @param $type
                 * @return Response
                 */
                public function raw (Request $request, $type = 'pdf' ) {

                        $tempstore = \Drupal::service( 'user.private_tempstore' )->get( 'pg_pdf_bin' );

                        $response = new Response();
                        $response->headers->set( 'Content-Type', 'application/pdf' );
                        $response->headers->set( 'Content-Disposition', 'attachment; filename="' . $tempstore->get( 'name' ) . '"' );
                        $response->setContent( $tempstore->get( 'data' ) );

                        return $response;
                }

                /**
                 * @return Response
                 */
                public function temp () {

                        $tempstore = \Drupal::service( 'user.private_tempstore' )->get( 'pg_pdf_bin' );

                        $response = new Response();
                        $response->headers->set( 'Content-Type', $tempstore->get( 'type' ) );
                        $response->headers->set( 'Content-Disposition', 'attachment; filename="' . $tempstore->get( 'name' ) . '"' );
                        $response->setContent( $tempstore->get( 'data' ) );

                        return $response;
                }

                /**
                 * @param $type
                 * @return BinaryFileResponse|Response
                 * @throws \PhpOffice\PhpSpreadsheet\Exception
                 * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
                 * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
                 */
                public function articleTemp ($type = 'pdf') {

                        $tempstore = \Drupal::service( 'user.private_tempstore' )->get( 'preview' );
                        $article = unserialize( $tempstore->get( 'article' ) );

                        if ( ( $workflow = $tempstore->get( 'workflow' ) ) === null )
                                $wfState = 'in submission';
                        else
                                $wfState = $workflow->getElement( 'state' );

                        if ( !$article ) {
                                return \Drupal::service( 'publisso_gold.tools' )->accessDenied( (string)t( 'Oops, something went wrong. We didn\'t found anything to show here...' ) );
                        }

                        $params = '--header-center "' . $wfState . '"';

                        return $this->HTML2PDF( '<h1>' . $article->getElement( 'title' ) . '</h1>' . base64_decode( $article->getElement( 'article_text' ) ), null, $params );
                }

                /**
                 * @param $jrna_id
                 * @param $type
                 * @return mixed
                 */
                public function article_volatile ($jrna_id, $type = 'pdf' ) {

                        $this->volatile = true;
                        return $this->article( $jrna_id, $type );
                }

                /**
                 * @param $jrna_id
                 * @param $type
                 * @return mixed
                 */
                public function article_force_new ($jrna_id, $type = 'pdf' ) {
                        if ( \Drupal::service( 'session' )->get( 'user' )[ 'weight' ] < 70 ) {
                                return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        }
                        $this->force_new = true;
                        return $this->article( $jrna_id, $type );
                }

                /**
                 * @param $jrna_id
                 * @param $type
                 * @return BinaryFileResponse|Response
                 * @throws \PhpOffice\PhpSpreadsheet\Exception
                 * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
                 * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
                 */
                public function article ($jrna_id, $type = 'pdf' ) {

                        $article = ArticleManager::getArticle($jrna_id);

                        if ( $this->force_new ) {

                                if ( $type == 'pdf' && $article->getElement( 'pdf_blob_id' ) ) {

                                        $blob = new Blob( $article->getElement( 'pdf_blob_id' ) );
                                        $blob->delete( $blob->getId() );
                                        $article->setElement( 'pdf_blob_id' );
                                }
                        }

                        if ( $article->getElement( 'doi' ) ) {

                                $doi = explode( '/', $article->getElement( 'doi' ) );

                                if ( $doi[ 1 ] ) {
                                        $this->outParams[ 'name' ] = $doi[ 1 ] . '.pdf';
                                }
                        }

                        if ( $type == 'pdf' && $article->getElement( 'pdf_blob_id' ) && !$this->volatile && !( $this->request->query->has( 'override_output' ) && $this->request->query->get( 'override_output' ) == 'html' ) ) {
                                return $this->sendFromBlob( $article->getElement( 'pdf_blob_id' ) );
                                exit();
                        }

                        $this->item = $article;
                        $journal = $article->getMedium();

                        $this->license = LicenseManager::get($article->getElement( 'license' ));

                        $xslB64 = 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHhzbDpzdHlsZXNoZWV0IHZlcnNpb249IjEuMCIgeG1sbnM6eHNsPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L1hTTC9UcmFuc2Zvcm0iPgogICAgPHhzbDp0ZW1wbGF0ZSBtYXRjaD0iL3JlY29yZCI+CiAgICAgICAgPGh0bWw+CiAgICAgICAgPGhlYWQ+CiAgICAgICAgICAgIDxzY3JpcHQgdHlwZT0idGV4dC9qYXZhc2NyaXB0IiBzcmM9Imh0dHBzOi8vY2RuanMuY2xvdWRmbGFyZS5jb20vYWpheC9saWJzL21hdGhqYXgvMi43LjcvTWF0aEpheC5qcz9jb25maWc9VGVYLU1NTC1BTV9DSFRNTCI+PC9zY3JpcHQ+CiAgICAgICAgICAgIDxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+CiAgICAgICAgICAgICAgICBodG1sLCBib2R5ewogICAgICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBhcmlhbCAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBwdDsKICAgICAgICAgICAgICAgICAgICBsaW5lLWhpZ2h0OiAxLjE1ZW07CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgaHRtbCwgYm9keXsKICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDA7CiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMDsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBib2R5ewogICAgICAgICAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxcHg7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgc3VwewogICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogOHB0OwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGFbaHJlZl17CiAgICAgICAgICAgICAgICAgICAgY29sb3I6IHJnYig1NSw5MSwxNTQpOwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIHA6bm90KHRhYmxlKXsKICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEwcHQ7CiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMnB0OwogICAgICAgICAgICAgICAgICAgIG9ycGhhbmVzOiAzOwogICAgICAgICAgICAgICAgICAgIHdpZG93czogMzsKICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5OwogICAgICAgICAgICAgICAgICAgIC8qZGlzcGxheTogaW5saW5lLWJsb2NrOyovCiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgaDEuY2hhcHRlclRpdGxlewogICAgICAgICAgICAgICAgICAgIGNvbG9yOiByZ2IoNTUsOTEsMTU0KSAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMThwdDsKICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZCAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgICAgIHBhZ2UtYnJlYWstYWZ0ZXI6IGF2b2lkICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7CiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMTVwdDsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBoMS5jaGFwdGVyVGl0bGVUcmFuc2xhdGVkewogICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMThwdDsKICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsOwogICAgICAgICAgICAgICAgICAgIHBhZ2UtYnJlYWstYWZ0ZXI6IGF2b2lkICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgaDJ7CiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB0ICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQgIWltcG9ydGFudDsKICAgICAgICAgICAgICAgICAgICBwYWdlLWJyZWFrLWFmdGVyOiBhdm9pZCAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrOwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGgzewogICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRwdCAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgICAgIGZvbnQtc3R5bGU6IGl0YWxpYyAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgICAgIHBhZ2UtYnJlYWstYWZ0ZXI6IGF2b2lkICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgaDR7CiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB0ICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQgIWltcG9ydGFudDsKICAgICAgICAgICAgICAgICAgICBwYWdlLWJyZWFrLWFmdGVyOiBhdm9pZCAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrOwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGg1ewogICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJwdCAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgICAgIGZvbnQtc3R5bGU6IGl0YWxpYyAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgICAgIHBhZ2UtYnJlYWstYWZ0ZXI6IGF2b2lkICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgaDZ7CiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB0ICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICAgICAgcGFnZS1icmVhay1hZnRlcjogYXZvaWQgIWltcG9ydGFudDsKICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jazsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICB0YWJsZSB7CiAgICAgICAgICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMmVtOwogICAgICAgICAgICAgICAgICAgIGZsb2F0OiBub25lICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMDsKICAgICAgICAgICAgICAgICAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlOwogICAgICAgICAgICAgICAgICAgIGJvcmRlci1zcGFjaW5nOiAwOwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIHRhYmxlIHRkLCB0YWJsZSB0aHsKICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiA0cHg7CiAgICAgICAgICAgICAgICAgICAgLypib3JkZXI6IDFweCBzb2xpZCBibGFjazsqLwogICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBwdDsKICAgICAgICAgICAgICAgICAgICB3aWR0aDogYXV0byAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgICAgIHdvcmQtd3JhcDogYnJlYWstd29yZCAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgdGFibGUgdGQgcCwgdGFibGUgdGggcHsKICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0ICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgdGFibGUuYmlnICp7CiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiA1cHQgIWltcG9ydGFudDsKICAgICAgICAgICAgICAgICAgICB3b3JkLXdyYXA6IGJyZWFrLXdvcmQgIWltcG9ydGFudDsKICAgICAgICAgICAgICAgIH0KCgogICAgICAgICAgICAgICAgdGFibGUgY2FwdGlvbnsKICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0OwogICAgICAgICAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAzcHQ7CiAgICAgICAgICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHQ7CiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiA5cHQgIWltcG9ydGFudDsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICB0YWJsZSN0YmxBZmZpbGlhdGlvbnMgdGR7CiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMDsKICAgICAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDsKICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgIHRhYmxlLmJvcmRlckJvdHRvbUJsYWNrewogICAgICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIHRhYmxlLmJvcmRlckJvdHRvbUJsYWNrIHRyICp7CiAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGJsYWNrICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgdGFibGUuYm9yZGVyRnVsbEJsYWNrewogICAgICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZSAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIHRhYmxlLmJvcmRlckZ1bGxCbGFjayB0ZCwgdGFibGUuYm9yZGVyRnVsbEJsYWNrIHRoewogICAgICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICBmaWd1cmV7CiAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAwOwogICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDIwcHg7CiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDsKICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jazsKICAgICAgICAgICAgICAgICAgICBwYWdlLWJyZWFrLWluc2lkZTogYXZvaWQgIWltcG9ydGFudDsKICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBmaWd1cmUgZmlnY2FwdGlvbnsKICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAzcHQ7CiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiA5cHQgIWltcG9ydGFudDsKICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jazsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBpbWd7CiAgICAgICAgICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlOwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGRpdi5maXJzdFNwYWNlewoKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBzcGFuLmNoYXB0ZXJBdXRob3J7CiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7CiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICAubm8tYnJlYWt7CiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7CiAgICAgICAgICAgICAgICAgICAgcGFnZS1icmVhay1pbnNpZGU6IGF2b2lkICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgLnBhZ2UtYnJlYWstYmVmb3JlewogICAgICAgICAgICAgICAgICAgIHBhZ2UtYnJlYWstYmVmb3JlOiBhbHdheXMgIWltcG9ydGFudDsKICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jazsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICAucGFnZS1icmVhay1hZnRlcnsKICAgICAgICAgICAgICAgICAgICBwYWdlLWJyZWFrLWFmdGVyOiBhbHdheXMgIWltcG9ydGFudDsKICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jazsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBhLmxua1JlbW92ZWRDb250ZW50ewogICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiAxMDA7CiAgICAgICAgICAgICAgICAgICAgZm9udC1zdHlsZTogaXRhbGljOwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIHVsLCBvbCB7CiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB0OwogICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBwdDsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBsaXsKICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAycHQ7CiAgICAgICAgICAgICAgICAgICAgLypwYWdlLWJyZWFrLWluc2lkZTogYXZvaWQgIWltcG9ydGFudDsqLwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGRpdltzZWN0aW9uXj0ia2V5d29yZHMiXXsKICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB0OwogICAgICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHQ7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgZGl2W3NlY3Rpb249ImF1dGhvcnMiXXsKICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB0OwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGRpdi5hdXRob3J7CiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiA5cHQ7CiAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDFlbTsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICB0YWJsZSN0YmxBZmZpbGlhdGlvbnMgdHIgdGR7CiAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDsKICAgICAgICAgICAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wOwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGRpdi5hZmZpbGlhdGlvbnsKICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA1cHQ7CiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiA5cHQgIWltcG9ydGFudDsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBkaXZbc2VjdGlvbj0iYWZmaWxpYXRpb25zIl17CiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVwdDsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBkaXZbc2VjdGlvbj0iYWJzdHJhY3QiXXsKICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxOXB0OwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGRpdltzZWN0aW9uPSJhYnN0cmFjdCJdIGgyewogICAgICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVwdDsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBkaXZbc2VjdGlvbj0iYWJzdHJhY3QiXSB0ZXh0ewogICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTIuNnB0ICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgZGl2W3NlY3Rpb249ImFic3RyYWN0X3RyYW5zbGF0ZWQiXXsKICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxOXB0OwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGRpdltzZWN0aW9uPSJhYnN0cmFjdF90cmFuc2xhdGVkIl0gaDJ7CiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB0OwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGRpdltzZWN0aW9uPSJhYnN0cmFjdF90cmFuc2xhdGVkIl0gdGV4dHsKICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEyLjZwdCAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGRpdltzZWN0aW9uPSJyZWZlcmVuY2VzIl0gewogICAgICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxOwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGRpdltzZWN0aW9uPSJyZWZlcmVuY2VzIl0gb2x7CiAgICAgICAgICAgICAgICAgICAgLypwYWRkaW5nLWxlZnQ6IDJlbTsqLwogICAgICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxLjJlbTsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBkaXZbc2VjdGlvbj0icmVmZXJlbmNlcyJdIG9sIGxpewogICAgICAgICAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMS43NXB0OwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGRpdltzZWN0aW9uPSJjaGFwdGVyX3RleHQiXSBwIHsKCiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgZGl2W3NlY3Rpb249ImNoYXB0ZXJfdGV4dCJdIHRhYmxlIHsKICAgICAgICAgICAgICAgICAgICB0YWJsZS1sYXlvdXQ6IGZpeGVkOwogICAgICAgICAgICAgICAgICAgIHdvcmQtd3JhcDogYnJlYWstd29yZDsKICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgZGl2W3NlY3Rpb249InJlZmVyZW5jZXMiXSBoMnsKICAgICAgICAgICAgICAgICAgICAvKmZvbnQtc2l6ZTogOXB0OyovCiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgZGl2W3NlY3Rpb249InJlZmVyZW5jZXMiXSBsaXsKICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAycHQ7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgZGl2W3NlY3Rpb249InJlZmVyZW5jZXMiXSBsaSAqewogICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogOXB0ICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDFlbTsKICAgICAgICAgICAgICAgIH0KCgogICAgICAgICAgICAgICAgZGl2W3NlY3Rpb249ImNpdGF0aW9uX25vdGUiXXsKICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA3cHQ7CiAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDFlbTsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBkaXZbc2VjdGlvbj0iY29ycmVzcG9uZGluZ19hdXRob3IiXXsKICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA3cHQ7CiAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDFlbTsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBkaXZbc2VjdGlvbj0iY29weXJpZ2h0Il17CiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogN3B0OwogICAgICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxZW07CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgZGl2W3NlY3Rpb249InJlZmVyZW5jZXMiXSAqewogICAgICAgICAgICAgICAgICAgIHBhZ2UtYnJlYWstaW5zaWRlOiBhdXRvICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICAgICAgb3JwaGFuZXM6IDI7CiAgICAgICAgICAgICAgICAgICAgd2lkb3dzOiAyOwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGxpLnBhZ2UtYnJlYWstYWxsb3dlZHsKICAgICAgICAgICAgICAgICAgICBwYWdlLWJyZWFrLWluc2lkZTogYXV0byAhaW1wb3J0YW50OwogICAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICAgIGxpLnBhZ2UtYnJlYWstYXZvaWR7CiAgICAgICAgICAgICAgICAgICAgcGFnZS1icmVhay1pbnNpZGU6IGF2b2lkICFpbXBvcnRhbnQ7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgZGl2LmFsZXJ0ewogICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDE1cHg7CiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDsKICAgICAgICAgICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCB0cmFuc3BhcmVudDsKICAgICAgICAgICAgICAgICAgICBib3JkZXItdG9wLWNvbG9yOiB0cmFuc3BhcmVudDsKICAgICAgICAgICAgICAgICAgICBib3JkZXItcmlnaHQtY29sb3I6IHRyYW5zcGFyZW50OwogICAgICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b20tY29sb3I6IHRyYW5zcGFyZW50OwogICAgICAgICAgICAgICAgICAgIGJvcmRlci1sZWZ0LWNvbG9yOiB0cmFuc3BhcmVudDsKICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgZGl2LmFsZXJ0LWluZm97CiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICMzMTcwOGY7CiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2Q5ZWRmNzsKICAgICAgICAgICAgICAgICAgICBib3JkZXItY29sb3I6ICNiY2U4ZjE7CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgICAgZGl2LmFsZXJ0LWluZm8gKnsKICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzMxNzA4ZjsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBkaXYuYWxlcnQtaW5mbyB0YWJsZSwgZGl2LmFsZXJ0LWluZm8gdGR7CiAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgIzMxNzA4ZjsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICBhOmVtcHR5eyBkaXNwbGF5OiBibG9jazsgcG9zaXRpb246IGFic29sdXRlOyB9CgoKICAgICAgICAgICAgICAgIC50ZXh0Um90YXRlOTBkZWd7CiAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpOwotd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKC05MGRlZyk7CiAgICAgICAgICAgICAgICB9CgoKLnRleHRSb3RhdGUtOTBkZWd7CiAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoLTkwZGVnKTsKICAgICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTsKfQogICAgICAgICAgICA8L3N0eWxlPgogICAgICAgIDwvaGVhZD4KICAgICAgICA8Ym9keT4KICAgICAgICA8aDEgY2xhc3M9ImNoYXB0ZXJUaXRsZSI+CiAgICAgICAgICAgIDx4c2w6dmFsdWUtb2Ygc2VsZWN0PSJ0aXRsZSIgLz4KICAgICAgICA8L2gxPgogICAgICAgIDx4c2w6aWYgdGVzdD0idGl0bGUtdHJhbnNsYXRlZCAhPSAnJyI+CiAgICAgICAgICAgIDxoMSBjbGFzcz0iY2hhcHRlclRpdGxlVHJhbnNsYXRlZCI+PHhzbDp2YWx1ZS1vZiBzZWxlY3Q9InRpdGxlLXRyYW5zbGF0ZWQiIC8+PC9oMT4KICAgICAgICA8L3hzbDppZj4KICAgICAgICA8eHNsOmlmIHRlc3Q9ImF1dGhvcnNbY291bnQoKikgPiAwXSI+CiAgICAgICAgICAgIDxkaXYgc2VjdGlvbj0iYXV0aG9ycyI+CiAgICAgICAgICAgICAgICA8eHNsOmZvci1lYWNoIHNlbGVjdD0iYXV0aG9ycy9hdXRob3IiPgogICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9ImF1dGhvciI+CiAgICAgICAgICAgICAgICAgICAgICAgIDxzdHJvbmc+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8eHNsOnZhbHVlLW9mIHNlbGVjdD0iZmlyc3RuYW1lIiAvPiYjMTYwOzx4c2w6dmFsdWUtb2Ygc2VsZWN0PSJsYXN0bmFtZSIgLz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4c2w6aWYgdGVzdD0iYWZmaWxpYXRpb24tbnVtYmVyIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3VwPjx4c2w6dmFsdWUtb2Ygc2VsZWN0PSJhZmZpbGlhdGlvbi1udW1iZXIiIC8+PC9zdXA+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3hzbDppZj4KICAgICAgICAgICAgICAgICAgICAgICAgPC9zdHJvbmc+CiAgICAgICAgICAgICAgICAgICAgPC9kaXY+CiAgICAgICAgICAgICAgICA8L3hzbDpmb3ItZWFjaD4KICAgICAgICAgICAgPC9kaXY+CiAgICAgICAgPC94c2w6aWY+CiAgICAgICAgPHhzbDppZiB0ZXN0PSJhZmZpbGlhdGlvbnNbY291bnQoKikgPiAwXSI+CiAgICAgICAgICAgIDxkaXYgc2VjdGlvbj0iYWZmaWxpYXRpb25zIj4KICAgICAgICAgICAgICAgIDx0YWJsZSBpZD0idGJsQWZmaWxpYXRpb25zIj4KICAgICAgICAgICAgICAgICAgICA8eHNsOmZvci1lYWNoIHNlbGVjdD0iYWZmaWxpYXRpb25zL2FmZmlsaWF0aW9uIj4KICAgICAgICAgICAgICAgICAgICAgICAgPHRyPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkIGlkPSJ0YmxBZmZpbGlhdGlvbnMiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzdXA+PHhzbDp2YWx1ZS1vZiBzZWxlY3Q9IkBudW1iZXIiIC8+PC9zdXA+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RkPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkIGlkPSJ0YmxBZmZpbGlhdGlvbnMiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9ImFmZmlsaWF0aW9uIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhzbDp2YWx1ZS1vZiBzZWxlY3Q9ImN1cnJlbnQoKSIgLz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGQ+CiAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+CiAgICAgICAgICAgICAgICAgICAgPC94c2w6Zm9yLWVhY2g+CiAgICAgICAgICAgICAgICA8L3RhYmxlPgogICAgICAgICAgICA8L2Rpdj4KICAgICAgICA8L3hzbDppZj4KICAgICAgICA8eHNsOmlmIHRlc3Q9ImFic3RyYWN0ICE9ICcnIj4KICAgICAgICAgICAgPGRpdiBzZWN0aW9uPSJhYnN0cmFjdCI+CiAgICAgICAgICAgICAgICA8aDI+PHhzbDp2YWx1ZS1vZiBzZWxlY3Q9ImFic3RyYWN0L0B0aXRsZSIgLz48L2gyPgogICAgICAgICAgICAgICAgPHRleHQ+CiAgICAgICAgICAgICAgICAgICAgPHhzbDp2YWx1ZS1vZiBzZWxlY3Q9ImFic3RyYWN0IiAvPgogICAgICAgICAgICAgICAgPC90ZXh0PgogICAgICAgICAgICA8L2Rpdj4KICAgICAgICA8L3hzbDppZj4KICAgICAgICA8eHNsOmlmIHRlc3Q9ImtleXdvcmRzW2NvdW50KCopID4gMF0iPgogICAgICAgICAgICA8ZGl2IHNlY3Rpb249ImtleXdvcmRzIj4KICAgICAgICAgICAgICAgIDxzdHJvbmc+PHhzbDp2YWx1ZS1vZiBzZWxlY3Q9ImtleXdvcmRzL0B0aXRsZSIgLz46IDwvc3Ryb25nPgogICAgICAgICAgICAgICAgPHhzbDpmb3ItZWFjaCBzZWxlY3Q9ImtleXdvcmRzL2tleXdvcmQiPgogICAgICAgICAgICAgICAgICAgIDx4c2w6dmFsdWUtb2Ygc2VsZWN0PSJjdXJyZW50KCkiIC8+CiAgICAgICAgICAgICAgICAgICAgPHhzbDppZiB0ZXN0PSJwb3NpdGlvbigpICE9IGxhc3QoKSI+LCA8L3hzbDppZj4KICAgICAgICAgICAgICAgIDwveHNsOmZvci1lYWNoPgogICAgICAgICAgICA8L2Rpdj4KICAgICAgICA8L3hzbDppZj4KICAgICAgICA8eHNsOmlmIHRlc3Q9ImFic3RyYWN0LXRyYW5zbGF0ZWQgIT0gJyciPgogICAgICAgICAgICA8ZGl2IHNlY3Rpb249ImFic3RyYWN0X3RyYW5zbGF0ZWQiPgogICAgICAgICAgICAgICAgPGgyPjx4c2w6dmFsdWUtb2Ygc2VsZWN0PSJhYnN0cmFjdC10cmFuc2xhdGVkL0B0aXRsZSIgLz48L2gyPgogICAgICAgICAgICAgICAgPHRleHQ+CiAgICAgICAgICAgICAgICAgICAgPHhzbDp2YWx1ZS1vZiBzZWxlY3Q9ImFic3RyYWN0LXRyYW5zbGF0ZWQiIC8+CiAgICAgICAgICAgICAgICA8L3RleHQ+CiAgICAgICAgICAgIDwvZGl2PgogICAgICAgIDwveHNsOmlmPgogICAgICAgIDx4c2w6aWYgdGVzdD0iL2NoYXB0ZXIva2V5d29yZHMtdHJhbnNsYXRlZFtjb3VudCgqKSA+IDBdIj4KICAgICAgICAgICAgPGRpdiBzZWN0aW9uPSJrZXl3b3Jkc190cmFuc2xhdGVkIj4KICAgICAgICAgICAgICAgIDxzdHJvbmc+PHhzbDp2YWx1ZS1vZiBzZWxlY3Q9ImtleXdvcmRzLXRyYW5zbGF0ZWQvQHRpdGxlIiAvPjogPC9zdHJvbmc+CiAgICAgICAgICAgICAgICA8eHNsOmZvci1lYWNoIHNlbGVjdD0ia2V5d29yZHMtdHJhbnNsYXRlZC9rZXl3b3JkIj4KICAgICAgICAgICAgICAgICAgICA8eHNsOnZhbHVlLW9mIHNlbGVjdD0iY3VycmVudCgpIiAvPgogICAgICAgICAgICAgICAgICAgIDx4c2w6aWYgdGVzdD0icG9zaXRpb24oKSAhPSBsYXN0KCkiPiwgPC94c2w6aWY+CiAgICAgICAgICAgICAgICA8L3hzbDpmb3ItZWFjaD4KICAgICAgICAgICAgPC9kaXY+CiAgICAgICAgPC94c2w6aWY+CiAgICAgICAgPGRpdiBzZWN0aW9uPSJjaGFwdGVyX3RleHQiPgogICAgICAgICAgICA8eHNsOmNvcHktb2Ygc2VsZWN0PSJmdWxsdGV4dC90ZXh0KCkiIC8+CiAgICAgICAgPC9kaXY+CiAgICAgICAgPHhzbDppZiB0ZXN0PSJyZWZlcmVuY2VzW2NvdW50KCopID4gMF0iPgogICAgICAgICAgICA8ZGl2IHNlY3Rpb249InJlZmVyZW5jZXMiPgogICAgICAgICAgICAgICAgPGgyPjx4c2w6dmFsdWUtb2Ygc2VsZWN0PSJyZWZlcmVuY2VzL0B0aXRsZSIgLz48L2gyPgogICAgICAgICAgICAgICAgPG9sIGlkPSJsaXN0UmVmZXJlbmNlcyI+CiAgICAgICAgICAgICAgICAgICAgPHhzbDpmb3ItZWFjaCBzZWxlY3Q9InJlZmVyZW5jZXMvcmVmZXJlbmNlIj4KICAgICAgICAgICAgICAgICAgICAgICAgPGxpPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhzbDppZiB0ZXN0PSJzdHJpbmctbGVuZ3RoKCkgJmx0OyAyMDAiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4c2w6YXR0cmlidXRlIG5hbWU9ImNsYXNzIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFnZS1icmVhay1hdm9pZAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwveHNsOmF0dHJpYnV0ZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwveHNsOmlmPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhzbDpjb3B5LW9mIHNlbGVjdD0iY3VycmVudCgpL25vZGUoKSIgLz4KICAgICAgICAgICAgICAgICAgICAgICAgPC9saT4KICAgICAgICAgICAgICAgICAgICA8L3hzbDpmb3ItZWFjaD4KICAgICAgICAgICAgICAgIDwvb2w+CiAgICAgICAgICAgIDwvZGl2PgogICAgICAgIDwveHNsOmlmPgogICAgICAgIDx4c2w6aWYgdGVzdD0iZGF0YXNbY291bnQoKikgPiAwXSI+CiAgICAgICAgICAgIDxkaXYgc2VjdGlvbj0iZGF0YXMiPgogICAgICAgICAgICAgICAgPGgyPjx4c2w6dmFsdWUtb2Ygc2VsZWN0PSJkYXRhcy9AdGl0bGUiIC8+PC9oMj4KICAgICAgICAgICAgICAgIDx4c2w6Zm9yLWVhY2ggc2VsZWN0PSJkYXRhcy9kYXRhIj4KICAgICAgICAgICAgICAgICAgICA8eHNsOmNvcHktb2Ygc2VsZWN0PSJjdXJyZW50KCkvbm9kZSgpIiAvPgogICAgICAgICAgICAgICAgICAgIDx4c2w6aWYgdGVzdD0icG9zaXRpb24oKSAhPSBsYXN0KCkiPgogICAgICAgICAgICAgICAgICAgICAgICA8YnIgLz4KICAgICAgICAgICAgICAgICAgICA8L3hzbDppZj4KICAgICAgICAgICAgICAgIDwveHNsOmZvci1lYWNoPgogICAgICAgICAgICA8L2Rpdj4KICAgICAgICA8L3hzbDppZj4KICAgICAgICA8ZGl2IHNlY3Rpb249InZpZGVvcyI+CiAgICAgICAgICAgIDxoMj5WaWRlb3M8L2gyPgogICAgICAgIDwvZGl2PgogICAgICAgIDx4c2w6aWYgdGVzdD0iYXR0YWNobWVudHNbY291bnQoKikgPiAwXSI+CiAgICAgICAgICAgIDxkaXYgc2VjdGlvbj0iYXR0YWNobWVudHMiPgogICAgICAgICAgICAgICAgPGgyPjx4c2w6dmFsdWUtb2Ygc2VsZWN0PSJhdHRhY2htZW50cy9AdGl0bGUiIC8+PC9oMj4KICAgICAgICAgICAgICAgIDx4c2w6Zm9yLWVhY2ggc2VsZWN0PSJhdHRhY2htZW50cy9hdHRhY2htZW50Ij4KICAgICAgICAgICAgICAgICAgICA8YT4KICAgICAgICAgICAgICAgICAgICAgICAgPHhzbDphdHRyaWJ1dGUgbmFtZT0iaHJlZiI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8eHNsOnZhbHVlLW9mIHNlbGVjdD0iLi91cmwiLz4KICAgICAgICAgICAgICAgICAgICAgICAgPC94c2w6YXR0cmlidXRlPgogICAgICAgICAgICAgICAgICAgICAgICA8eHNsOnZhbHVlLW9mIHNlbGVjdD0iLi91cmwiLz4KICAgICAgICAgICAgICAgICAgICA8L2E+CiAgICAgICAgICAgICAgICAgICAgPHhzbDppZiB0ZXN0PSJwb3NpdGlvbigpICE9IGxhc3QoKSI+CiAgICAgICAgICAgICAgICAgICAgICAgIDxiciAvPgogICAgICAgICAgICAgICAgICAgIDwveHNsOmlmPgogICAgICAgICAgICAgICAgPC94c2w6Zm9yLWVhY2g+CiAgICAgICAgICAgIDwvZGl2PgogICAgICAgIDwveHNsOmlmPgogICAgICAgIDx4c2w6aWYgdGVzdD0iZXJyYXR1bSAhPSAnJyI+CiAgICAgICAgICAgIDxkaXYgc2VjdGlvbj0iZXJyYXR1bSIgY2xhc3M9Im5vLWJyZWFrIj4KICAgICAgICAgICAgICAgIDxoMj48eHNsOnZhbHVlLW9mIHNlbGVjdD0iZXJyYXR1bS9AdGl0bGUiIC8+PC9oMj4KICAgICAgICAgICAgICAgIDx4c2w6Y29weS1vZiBzZWxlY3Q9ImVycmF0dW0vbm9kZSgpIiAvPgogICAgICAgICAgICA8L2Rpdj4KICAgICAgICA8L3hzbDppZj4KICAgICAgICA8eHNsOmlmIHRlc3Q9ImNvcnJlc3BvbmRpbmctYXV0aG9yc1tjb3VudCgqKSA+IDBdIj4KICAgICAgICAgICAgPGRpdiBzZWN0aW9uPSJjb3JyZXNwb25kaW5nX2F1dGhvciIgY2xhc3M9Im5vLWJyZWFrIj4KICAgICAgICAgICAgICAgIDxici8+CiAgICAgICAgICAgICAgICA8c3Ryb25nPjx4c2w6dmFsdWUtb2Ygc2VsZWN0PSJjb3JyZXNwb25kaW5nLWF1dGhvcnMvQHRpdGxlIiAvPjogPC9zdHJvbmc+CiAgICAgICAgICAgICAgICA8eHNsOmZvci1lYWNoIHNlbGVjdD0iY29ycmVzcG9uZGluZy1hdXRob3JzL2F1dGhvciI+CiAgICAgICAgICAgICAgICAgICAgPHhzbDppZiB0ZXN0PSIuL2dyYWR1YXRpb24gIT0gJyciPjx4c2w6dmFsdWUtb2Ygc2VsZWN0PSIuL2dyYWR1YXRpb24iLz4mIzE2MDs8L3hzbDppZj4KICAgICAgICAgICAgICAgICAgICA8eHNsOnZhbHVlLW9mIHNlbGVjdD0iLi9maXJzdG5hbWUiLz4mIzE2MDs8eHNsOnZhbHVlLW9mIHNlbGVjdD0iLi9sYXN0bmFtZSIvPgogICAgICAgICAgICAgICAgICAgIDx4c2w6aWYgdGVzdD0iLi9ncmFkdWF0aW9uLXN1ZmZpeCAhPSAnJyI+JiMxNjA7PHhzbDp2YWx1ZS1vZiBzZWxlY3Q9Ii4vZ3JhZHVhdGlvbi1zdWZmaXgiLz48L3hzbDppZj4KICAgICAgICAgICAgICAgICAgICA8eHNsOmlmIHRlc3Q9Ii4vaW5zdGl0dXRlICE9ICcnIj4sJiMzMjs8eHNsOnZhbHVlLW9mIHNlbGVjdD0iLi9pbnN0aXR1dGUiLz4gPC94c2w6aWY+CiAgICAgICAgICAgICAgICAgICAgPHhzbDppZiB0ZXN0PSIuL2RlcGFydG1lbnQgIT0gJyciPiwmIzMyOzx4c2w6dmFsdWUtb2Ygc2VsZWN0PSIuL2RlcGFydG1lbnQiLz4gPC94c2w6aWY+CiAgICAgICAgICAgICAgICAgICAgPHhzbDppZiB0ZXN0PSIuL3N0cmVldCAhPSAnJyI+LCYjMzI7PHhzbDp2YWx1ZS1vZiBzZWxlY3Q9Ii4vc3RyZWV0Ii8+IDwveHNsOmlmPgogICAgICAgICAgICAgICAgICAgIDx4c2w6aWYgdGVzdD0iLi9wb3N0YWwtY29kZSAhPSAnJyI+LCYjMzI7PHhzbDp2YWx1ZS1vZiBzZWxlY3Q9Ii4vcG9zdGFsLWNvZGUiLz48L3hzbDppZj4KICAgICAgICAgICAgICAgICAgICA8eHNsOmlmIHRlc3Q9Ii4vY2l0eSAhPSAnJyI+JiMxNjA7PHhzbDp2YWx1ZS1vZiBzZWxlY3Q9Ii4vY2l0eSIvPiA8L3hzbDppZj4KICAgICAgICAgICAgICAgICAgICA8eHNsOmlmIHRlc3Q9Ii4vY291bnRyeSAhPSAnJyI+LCYjMzI7PHhzbDp2YWx1ZS1vZiBzZWxlY3Q9Ii4vY291bnRyeSIvPiA8L3hzbDppZj4KICAgICAgICAgICAgICAgICAgICA8eHNsOmlmIHRlc3Q9Ii4vdGVsZXBob25lICE9ICcnIj4sJiMzMjtQaG9uZTomIzE2MDs8eHNsOnZhbHVlLW9mIHNlbGVjdD0iLi90ZWxlcGhvbmUiLz4gPC94c2w6aWY+CiAgICAgICAgICAgICAgICAgICAgPHhzbDppZiB0ZXN0PSIuL2VtYWlsICE9ICcnIj4sJiMzMjtFLW1haWw6JiMxNjA7PHhzbDp2YWx1ZS1vZiBzZWxlY3Q9Ii4vZW1haWwiLz4gPC94c2w6aWY+CiAgICAgICAgICAgICAgICAgICAgPHhzbDppZiB0ZXN0PSJwb3NpdGlvbigpICE9IGxhc3QoKSI+PGJyLz48L3hzbDppZj4KICAgICAgICAgICAgICAgIDwveHNsOmZvci1lYWNoPgogICAgICAgICAgICA8L2Rpdj4KICAgICAgICA8L3hzbDppZj4KICAgICAgICA8eHNsOmlmIHRlc3Q9ImNpdGF0aW9uLW5vdGUgIT0gJyciPgogICAgICAgICAgICA8ZGl2IHNlY3Rpb249ImNpdGF0aW9uX25vdGUiIGNsYXNzPSJuby1icmVhayI+CiAgICAgICAgICAgICAgICA8YnIgLz4KICAgICAgICAgICAgICAgIDxzdHJvbmc+PHhzbDp2YWx1ZS1vZiBzZWxlY3Q9ImNpdGF0aW9uLW5vdGUvQHRpdGxlIiAvPjogPC9zdHJvbmc+CiAgICAgICAgICAgICAgICA8c3BhbiBpZD0iY25fdGV4dCI+PHhzbDpjb3B5LW9mIHNlbGVjdD0iY2l0YXRpb24tbm90ZS9ub2RlKCkiIC8+PC9zcGFuPgogICAgICAgICAgICA8L2Rpdj4KICAgICAgICA8L3hzbDppZj4KICAgICAgICA8eHNsOmlmIHRlc3Q9ImNvcHlyaWdodCAhPSAnJyI+CiAgICAgICAgICAgIDxkaXYgc2VjdGlvbj0iY29weXJpZ2h0IiBjbGFzcz0ibm8tYnJlYWsiPgogICAgICAgICAgICAgICAgPGJyIC8+CiAgICAgICAgICAgICAgICA8c3Ryb25nPjx4c2w6dmFsdWUtb2Ygc2VsZWN0PSJjb3B5cmlnaHQvQHRpdGxlIiAvPjogPC9zdHJvbmc+CiAgICAgICAgICAgICAgICA8eHNsOmNvcHktb2Ygc2VsZWN0PSJjb3B5cmlnaHQvbmFtZS9ub2RlKCkiIC8+CiAgICAgICAgICAgICAgICA8YnIgLz4KICAgICAgICAgICAgICAgIDx4c2w6Y29weS1vZiBzZWxlY3Q9ImNvcHlyaWdodC9kZXNjcmlwdGlvbi9ub2RlKCkiIC8+CiAgICAgICAgICAgIDwvZGl2PgogICAgICAgIDwveHNsOmlmPgogICAgICAgIDwvYm9keT4KICAgICAgICA8L2h0bWw+CiAgICA8L3hzbDp0ZW1wbGF0ZT4KPC94c2w6c3R5bGVzaGVldD4K';

                        $doc = $article->getXML4PDF();

                        $xsl = new \DOMDocument();
                        $xsl->loadXML( base64_decode( $xslB64 ) );
                        $proc = new \XSLTProcessor();
                        $proc->importStylesheet( $xsl );
                        $doc = $proc->transformToDoc( $doc );

                        $htmlGenText = html_entity_decode( $doc->saveHTML() );

                        //make sure, utf-8 and latin1 are correctly displayed
                        $htmlGenText = mb_convert_encoding( $htmlGenText, 'HTML-ENTITIES', 'UTF-8' );
                        #echo $htmlGenText; exit;
                        $html = new \DOMDocument();
                        $html->loadHTML( $htmlGenText, LIBXML_NOWARNING | LIBXML_NOERROR);

                        $xpath = new \DOMXPath( $html );
                        $body = $xpath->query( '//body' )->item( 0 );

                        //search for Videos and remove them
                        $videos = [];
                        foreach ( $xpath->evaluate( '//iframe[contains(@src, "youtube")]|//video' ) as $node ) {
                                $videos[] = $node;
                        }

                        $c = 0;
                        foreach ( $videos as $k => $node ) {

                                $src = $node->getAttribute( 'src' );
                                if(preg_match('/^\/[^\/]/', $src)){
                                        $url = Url::fromUserInput($src);
                                        $url->setAbsolute();
                                        $src = $url->toString();
                                }
                                $c++;
                                $newNode = $html->createElement( 'a', (string)t( 'Removed video @nr', [ '@nr' => $c ] ) );
                                $newNode->setAttribute( 'href', '#rwRemovedContent~' . md5( $src ) );
                                $newNode->setAttribute( 'class', 'lnkRemovedContent' );
                                $node->parentNode->replaceChild( $newNode, $node );
                                $videos[ $k ] = $src;
                        }

                        $divVideos = $xpath->query( '//div[@section="videos"]' );
                        if ( $divVideos->length ) {

                                $divVideos = $divVideos->item( 0 );
                                if ( !count( $videos ) ) $divVideos->parentNode->removeChild( $divVideos );

                                for ( $c = 0; $c < count( $videos ); $c++ ) {
                                        $anchor = md5( $videos[ $c ] );
                                        $video = preg_replace( '/(\?.+)?$/', '', $videos[ $c ] );
                                        if ( substr( $video, 0, 2 ) == '//' ) $video = "http:$video";
                                        $divVideos->appendChild( $html->createElement( 'a', 'Video ' . ( $c + 1 ) . ': ' ) )->setAttribute( 'name', 'rwRemovedContent~' . $anchor );
                                        $divVideos->appendChild( $html->createElement( 'a', $video ) )->setAttribute( 'href', $video );
                                        $divVideos->appendChild( $html->createElement( 'br' ) );
                                }
                        }

                        //remove html-border from tables
                        foreach ( $xpath->evaluate( '//table' ) as $node ) {
                                $node->removeAttribute( 'border' );
                        }

                        //clean table-styles
                        foreach ( [ 'table', 'tr', 'th', 'td' ] as $_ ) {

                                foreach ( $xpath->evaluate( "//$_" ) as $_node ) {
                                        $_node->removeAttribute( 'width' );
                                }
                        }

                        //disable inline-styles on tables ad table-cells
                        foreach ( $xpath->evaluate( '//table|//td|//th' ) as $table ) {
                                //$table->removeAttribute('style');
                                $table->setAttribute( 'style', preg_replace( '/(width|height|border)\s*:\s*.+?;/', '', $table->getAttribute( 'style' ) ) );
                        }

                        //all in a figure - don't break
                        $figures = [];
                        foreach ( $xpath->evaluate( '//figure' ) as $node ) {
                                $figures[] = $node;
                        }

                        foreach ( $figures as $node ) {

                                $uid = uniqid();
                                $parent = $node->parentNode;

                                $span = $html->createElement( 'span' );
                                $span->setAttribute( 'class', 'no-break' );
                                $span->appendChild( $node->cloneNode( true ) );
                                $html->importNode( $span );
                                $parent->replaceChild( $span, $node );

                        }

                        $brs = [];
                        foreach ( $xpath->evaluate( '//figcaption//br' ) as $_ ) $brs[] = $_;

                        foreach ( $brs as $node ) {
                                $node->parentNode->removeChild( $node );
                        }

                        if ( $this->pdftk !== false ) {

                                $varPath = Publisso::tools()->getVarDirAbsolute();

                                //mark big tables
                                $tableNr = 0;

                                foreach ( $xpath->evaluate( "//table[not(@id = 'tblAffiliations')]" ) as $table ) {

                                        $tableNr++;
                                        $licInfo = "&copy; " . $this->license->getElement( 'name' ) . ': ' . $this->license->getElement( 'description' );

                                        if ( isset( $big ) ) unset( $big );

                                        if ( preg_match( '/pdf-keep/', $table->getAttribute( 'class' ) ) ) continue;
                                        if ( preg_match( '/pdf-force-extract/', $table->getAttribute( 'class' ) ) ) {
                                                $big = true;
                                        }

                                        if ( !isset( $big ) ) {

                                                $big = false;
                                                $nlTbody = $xpath->evaluate( 'tbody', $table );

                                                if ( $nlTbody->length > 0 ) {

                                                        $tr = $xpath->evaluate( 'tr[.//td|.//th]', $nlTbody[ 0 ] );

                                                        $maxTd = 0;

                                                        foreach ( $tr as $_ ) { //alle Zeilen absuchen, um colspans auszuschließen

                                                                $nlTd = $xpath->evaluate( 'td|th', $_ );
                                                                if ( $nlTd->length > $maxTd ) $maxTd = $nlTd->length;

                                                        }

                                                        if ( $maxTd > 6 ) $big = true;
                                                }
                                        }

                                        if ( $big ) {
                                                $table->setAttribute( 'nr', $tableNr );
                                                $table->setAttribute( 'big', '1' );
                                                $table->setAttribute( 'cols', $maxTd );
                                        }
                                }

                                //remove big tables, store them for attach to pdf
                                $nodes = [];
                                foreach ( $xpath->evaluate( '//table[@big]' ) as $tableNode ) {
                                        $nodes[] = $tableNode;
                                }

                                foreach ( $nodes as $node ) {

                                        $c = $node->getAttribute( 'nr' );
                                        $_clone = $node->cloneNode( true );
                                        $_clone->setAttribute( 'border', '1' );


                                        $_doc = new \DOMDocument();
                                        $_doc->appendChild( $_doc->importNode( $_clone, true ) );
                                        $_xpath = new \DOMXpath( $_doc );


                                        if ( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] == 1 ) {

                                                //echo $_doc->saveHTML(); exit();
                                        }

                                        $name = "table_" . time() . uniqid() . '.html';

                                        $attachments[ "Table $c" ][ 'path' ] = "$varPath/$name";
                                        $attachments[ "Table $c" ][ 'desc' ] = "";

                                        $tr = Publisso::tools()->DOMAppendChild( 'tr', '', [], $_doc, $_xpath->evaluate( '/table/tbody' )[ 0 ] );
                                        Publisso::tools()->DOMAppendChild( 'td', ' ', [ 'colspan' => $_clone->getAttribute( 'cols' ) ], $_doc, $tr );

                                        if ( $xpath->evaluate( '//caption', $node )->length ) {
                                                $attachments[ "Table $c" ][ 'desc' ] = trim( $xpath->evaluate( '//caption', $node )[ 0 ]->nodeValue );

                                                //Beschreibung
                                                Publisso::tools()->DOMAppendChild( 'tr', '', [], $_doc, $_xpath->evaluate( '/table/tbody' )[ 0 ] );
                                                Publisso::tools()->DOMAppendChild( 'td', 'Beschreibung: ' . $attachments[ "Table $c" ][ 'desc' ], [ 'colspan' => $_clone->getAttribute( 'cols' ) ], $_doc, $_xpath->evaluate( '/table/tbody/tr[last()]' )[ 0 ] );
                                                Publisso::tools()->DOMAppendChild( 'tr', '', [], $_doc, $_xpath->evaluate( '/table/tbody' )[ 0 ] );
                                                Publisso::tools()->DOMAppendChild( 'td', ' ', [ 'colspan' => $_clone->getAttribute( 'cols' ) ], $_doc, $_xpath->evaluate( '/table/tbody/tr[last()]' )[ 0 ] );
                                        }

                                        //Lizenzinfo
                                        Publisso::tools()->DOMAppendChild( 'tr', '', [], $_doc, $_xpath->evaluate( '/table/tbody' )[ 0 ] );
                                        Publisso::tools()->DOMAppendChild( 'td', $licInfo, [ 'colspan' => $_clone->getAttribute( 'cols' ) ], $_doc, $_xpath->evaluate( '/table/tbody/tr[last()]' )[ 0 ] );

                                        file_put_contents( "$varPath/$name", $_doc->saveHTML() );

                                        //echo $_doc->saveHTML(); exit();
                                        $newNode = $html->createElement( 'p', (string)$this->t( 'Extracted Table: Table @nr', [ '@nr' => $c ] ) );
                                        //REPLACE!!
                                        $node->parentNode->replaceChild( $newNode, $node );
                                        //$c++;
                                }
                        }

                        #echo $html->saveHTML(); exit();

                        $port = \Drupal::request()->getPort();
                        $host = \Drupal::request()->getHttpHost();
                        $host = "http" . ( $port == 443 ? 's' : '' ) . "://$host";

                        $vars = [
                                'journalTitle'      => urlencode( $journal->getElement( 'title' ) ),
                                'articleTitleShort' => urlencode( $article->getElement( !empty( $article->getElement( 'title_short' ) ) ? 'title_short' : 'title' ) ),
                                'journalID'         => $journal->getElement( 'id' ),
                                'articleID'         => $article->getElement( 'id' ),
                                'articleDOI'        => $article->getElement( 'doi' ),
                                'license'           => $article->getElement( 'license' ),
                                'theme'             => strtolower( $journal->getElement( 'theme' ) ),
                                'language'          => strtoupper( empty( $journal->getElement( 'language' ) ) ? 'en' : $journal->getElement( 'language' ) ),
                        ];

                        if ( count( $article->readAuthors() ) ) {
                                $vars[ 'author' ] = urlencode( $article->readAuthors()[ 0 ]->profile->getElement( 'lastname' ) . ', ' . $article->readAuthors()[ 0 ]->profile->getFirstnameInitials() . ( count( $article->readAuthors() ) > 1 ? ' (et al.)' : '' ) );
                        }

                        $replace = '';

                        foreach ( $vars as $k => $v ) $replace .= " --replace '$k' '" . str_replace( "'", "'\''", $v ) . "'";

                        if ( $this->request->query->has( 'override_output' ) && $this->request->query->get( 'override_output' ) == 'html' ) {
                                echo $html->saveHTML();
                                exit();
                        }

                        return $this->HTML2PDF(
                                $html->saveHTML(),
                                $article->getElement( 'title' ),
                                "-B 20mm -T 25mm -L 25mm -R 25mm --dpi 96 --run-script 'MathJax.Hub.Config({\"SVG\": {scale: 400}}); MathJax.Hub.Queue([\"Rerender\", MathJax.Hub], function () {window.status=\"finished\"})' --window-status finished --no-stop-slow-scripts --image-quality 100 --image-dpi 600 --page-size A4 -O Portrait --header-html '$host/system/gethtml/JournalHeadPDF' -q --footer-html '$host/system/gethtml/JournalFootPDF' $replace --debug-javascript --encoding utf-8 --enable-javascript  --no-stop-slow-scripts --disable-smart-shrinking",
                                $attachments
                        );
                }

                /**
                 * @param $html
                 * @param null $fileName
                 * @param null $params
                 * @param null $attachments
                 * @return BinaryFileResponse|Response
                 * @throws \PhpOffice\PhpSpreadsheet\Exception
                 * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
                 * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
                 */
                private function HTML2PDF ($html, $fileName = null, $params = null, $attachments = null ) {

                        if ( $fileName === null ) $fileName = 'file';

                        $fileName = str_replace( '/', '', $fileName );
                        $fileName = preg_replace('/[[:^print:]]/', '', $fileName);

                        $filePath = "/tmp";
                        $mh = \Drupal::service( 'module_handler' );
                        $mp = $mh->getModule( 'publisso_gold' )->getPath();

                        $filePath = DRUPAL_ROOT . '/' . $mp . "/var/tmp";
                        $bin = DRUPAL_ROOT . '/' . $mp . "/vendor/wkhtmltox_0.12.5-1/bin/wkhtmltopdf";

                        if(!is_executable($bin)){
                                chmod($bin, 0755);
                        }

                        $tmpFileName = uniqid();

                        $sourceFile = "$filePath/$tmpFileName.html";
                        $targetFile = "$filePath/$tmpFileName.pdf";

                        $doc = new \DOMDocument();

                        $doc->loadHTML( mb_convert_encoding( $html, 'HTML-ENTITIES', 'UTF-8' ) );

                        $images = $doc->getElementsByTagName( 'img' );

                        for ( $i = 0; $i < $images->length; $i++ ) {
                                $src = $doc->getElementsByTagName( 'img' )[ $i ]->getAttribute( 'src' );

                                if ( substr( $src, 0, 1 ) == '/' ) {
                                        $doc->getElementsByTagName( 'img' )[ $i ]->setAttribute( 'src', DRUPAL_ROOT . $src );
                                }
                        }

                        $doc->saveHTMLFile( $sourceFile );
                        $call = "$bin $params file://$sourceFile $targetFile";
                        #echo $call;

                        system( $call );
                        unlink( $sourceFile );


                        if ( is_array( $attachments ) ) {

                                $mh = \Drupal::service( 'module_handler' )->getModule( 'publisso_gold' )->getPath();
                                $varPath = DRUPAL_ROOT . "/$mh/var";

                                $cell_st = [
                                        'font'      => [
                                                'size'  => 9,
                                                'align' => 'right',

                                        ],
                                        'alignment' => [
                                                'wrapText'   => true,
                                                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                                        ],
                                        'borders'   => [
                                                'inside'  => [
                                                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                                ],
                                                'outline' => [
                                                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                                ],
                                        ],
                                ];

                                foreach ( $attachments as $name => $attachment ) {

                                        $desc = $attachment[ 'desc' ];
                                        $attachment = $attachment[ 'path' ];

                                        $xls = \PhpOffice\PhpSpreadsheet\IOFactory::load( $attachment );

                                        /**
                                         * Ausgabe als Excel
                                         */
                                        $out = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx( $xls );
                                        $sheet = $out->getSpreadsheet();
                                        $maxCols = $sheet->getActiveSheet()->getHighestColumn();
                                        $maxRows = $sheet->getActiveSheet()->getHighestRow();

                                        foreach ( range( 'A', $sheet->getActiveSheet()->getHighestDataColumn() ) as $col ) {
                                                $sheet->getActiveSheet()->getColumnDimension( $col )->setAutoSize( true );
                                        }

                                        foreach ( range( '1', $sheet->getActiveSheet()->getHighestDataRow() ) as $row ) {
                                                $sheet->getActiveSheet()->getRowDimension( $row )->setRowHeight( -1 );
                                        }

                                        $sheet->getActiveSheet()->getStyle( "A1:" . $maxCols . $maxRows )->applyFromArray( $cell_st );

                                        $sheet->getProperties()->setDescription( $desc );
                                        $sheet->getProperties()->setTitle( $name );
                                        $sheet->getProperties()->setCustomProperty( (string)$this->t( 'License' ), $this->license->getElement( 'description' ), 'text' );

                                        $out = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx( $sheet );

                                        $outDir = uniqid();
                                        $outName = $name . '.xlsx';
                                        mkdir( "$varPath/tmp/$outDir", 0755, true );
                                        $out->save( "$varPath/tmp/$outDir/$outName" );

                                        exec( $this->pdftk . " $targetFile attach_file \"$varPath/tmp/$outDir/$outName\" output $targetFile" . '.attached' );
                                        copy( $targetFile . '.attached', $targetFile );
                                        unlink( $targetFile . '.attached' );


                                        unlink( "$attachment.xlsx" );
                                        unlink( "$varPath/tmp/$outDir/$outName" );
                                        rmdir( "$varPath/tmp/$outDir" );


                                        /**
                                         * Ausgabe als Opendocument
                                         */
                                        $out = new \PhpOffice\PhpSpreadsheet\Writer\Ods( $xls );
                                        $sheet = $out->getSpreadsheet();
                                        $maxCols = $sheet->getActiveSheet()->getHighestColumn();
                                        $maxRows = $sheet->getActiveSheet()->getHighestRow();

                                        foreach ( range( 'A', $sheet->getActiveSheet()->getHighestDataColumn() ) as $col ) {
                                                $sheet->getActiveSheet()->getColumnDimension( $col )->setAutoSize( true );
                                        }

                                        foreach ( range( '1', $sheet->getActiveSheet()->getHighestDataRow() ) as $row ) {
                                                $sheet->getActiveSheet()->getRowDimension( $row )->setRowHeight( -1 );
                                        }

                                        $sheet->getActiveSheet()->getStyle( "A1:" . $maxCols . $maxRows )->applyFromArray( $cell_st );

                                        $sheet->getProperties()->setDescription( $desc );
                                        $sheet->getProperties()->setTitle( $name );
                                        $sheet->getProperties()->setCustomProperty( (string)$this->t( 'License' ), $this->license->getElement( 'description' ), 'text' );

                                        $out = new \PhpOffice\PhpSpreadsheet\Writer\Ods( $sheet );

                                        $outDir = uniqid();
                                        $outName = $name . '.ods';
                                        mkdir( "$varPath/tmp/$outDir", 0755, true );
                                        $out->save( "$varPath/tmp/$outDir/$outName" );

                                        exec( $this->pdftk . " $targetFile attach_file \"$varPath/tmp/$outDir/$outName\" output $targetFile" . '.attached' );
                                        copy( $targetFile . '.attached', $targetFile );
                                        unlink( $targetFile . '.attached' );


                                        unlink( "$attachment.ods" );
                                        unlink( "$varPath/tmp/$outDir/$outName" );
                                        rmdir( "$varPath/tmp/$outDir" );


                                        /**
                                         * Enfernen nicht benötigter Dinge
                                         */
                                        unlink( $attachment );
                                }
                        }

                        $pdfOut = false;

                        if ( !$this->volatile ) {
                                $blob = new Blob();
                                $blob->create( file_get_contents( $targetFile ), 'application/pdf', serialize( [ 'name' => array_key_exists( 'name', $this->outParams ) ? $this->outParams[ 'name' ] : $tmpFileName . '.pdf' ] ) );

                                if ( $blob->getId() ) {
                                        unlink( $targetFile );
                                        $this->item->setElement( 'pdf_blob_id', $blob->getId() );
                                        $pdfOut = true;
                                        return $this->sendFromBlob( $blob->getId() );
                                }
                        }

                        if ( !$pdfOut ) {

                                //store BIN-PDF for several Reasons
                                $tempstore = \Drupal::service( 'user.private_tempstore' )->get( 'pg_pdf_bin' );
                                $tempstore->set( 'name', $fileName );
                                $tempstore->set( 'type', 'application/pdf' );
                                $tempstore->set( 'data', file_get_contents( $targetFile ) );

                                $response = new BinaryFileResponse( $targetFile );
                                $response->deleteFileAfterSend( true );
                                $response->setContentDisposition(
                                        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                                        $fileName . '.pdf',
                                        iconv( 'UTF-8', 'ASCII//TRANSLIT', $fileName . '.pdf' )
                                );

                                return $response;
                        }
                }

                /**
                 * @param int $id
                 * @return Response
                 */
                private function sendFromBlob (int $id ) {

                        $blob = new Blob( $id );


                        $response = new Response();
                        $response->headers->set( 'Content-Type', $blob->type );
                        $response->headers->set( 'Content-Disposition', 'attachment; filename="' . $blob->meta[ 'name' ] . '"' );
                        $response->setContent( $blob->data );

                        return $response;
                }
        }
