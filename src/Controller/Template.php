<?php

	/**
	 * @file
	 * Contains \Drupal\publisso_gold\Controller\Template.
	 */

	namespace Drupal\publisso_gold\Controller;
        
        /**
         * Class Template
         * @package Drupal\publisso_gold\Controller
         */
        class Template{
		
		private $dbh;
		private $var;
		private $tmp;
		private $loaded;
		private $name;
		private $pathAbsolute = false;
		
		public function __construct(){
		        $this->var = [];
		        $this->loaded = false;
                }
                
                /**
                 * @return string
                 */
                public function __toString(){
			return 'Template';
		}
                
                /**
                 * @param null $name
                 * @param false $pathAbsolute
                 * @return $this
                 */
                public function get($name = null, $pathAbsolute = false){
			
		        $this->pathAbsolute = $pathAbsolute;
		        
		        if($name) {
		                
		                if(!$pathAbsolute) {//in diesem Zweig ist alter Kram
                                        $path = __DIR__ . "/../../inc/$name.tmpl.inc.php";
                                        
                                        if ( !( file_exists( $path ) && is_readable( $path ) ) ) {
                                                $this->tmp = (string)t( "Can't read template @name", [ '@name' => $name ] );
                                                error_log( 'ERROR: ' . $this->tmp );
                                                $this->loaded = false;
                                        }
                                        else {
                                                $this->tmp = file_get_contents( $path );
                                                $this->loaded = true;
                                                $this->name = $name;
                                        }
                                }
		                else{
		                        
                                        $this->tmp = file_get_contents( $name );
                                        $this->loaded = true;
                                        $this->name = $name;
                                }
                        }
		        
                        return $this;
		}
                
                /**
                 * @param $name
                 * @param $value
                 */
                public function setVar($name, $value){
                        $this->var[$name] = $value;
		}
                
                /**
                 * @param $name
                 * @param $value
                 */
                public function appendToVar($name, $value){
		        if(!array_key_exists($name, $this->var)) $this->var[$name] = '';
                        $this->var[$name] .= $value;
		}
                
                /**
                 * @param $name
                 * @param $value
                 */
                public function prependToVar($name, $value){
                        $this->var[$name] = $value.$this->var[$name];
		}
                
                /**
                 * @param $name
                 * @return mixed|null
                 */
                public function getVar($name){
                        return $this->var[$name] ?? null;
		}
                
                /**
                 * @param string $delimiter
                 * @param bool $unknownVars
                 * @return string|string[]|null
                 */
                public function parse(string $delimiter = '::', $unknownVars = true){
                        
                        $tmpl = $this->tmp;
                        $vars =[];
                        
                        foreach($this->var as $key => $val){
                                $vars[$delimiter.$key.$delimiter] = $val;
                        }
                        
                        $tmpl = @str_replace(array_keys($vars), array_values($vars), $tmpl);
                        
                        if($unknownVars)
                                $tmpl = preg_replace("/$delimiter([a-zA-Z-_1-9]+)$delimiter?/", '', $tmpl);
                        
                        return $tmpl;
		}
                
                /**
                 * @param $HTMLString
                 */
                public function prependHTML($HTMLString){
                        $this->tmp = $HTMLString.$this->tmp;
		}
                
                /**
                 * @param $HTMLString
                 */
                public function appendHTML($HTMLString){
                        $this->tmp = $this->tmp.$HTMLString;
		}
		
		public function print(){
                        return $this->tmp;
		}
                
                /**
                 * @return false
                 */
                public function isLoaded(){
                        return $this->loaded;
		}
		
		public function clear(){
                        $this->get($this->name, $this->pathAbsolute);
                        $this->var = [];
		}
		
		public function getName(){
		        return $this->name;
                }
	}
?>
