<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\TableOfContentItem.
 */

namespace Drupal\publisso_gold\Controller;

/**
 * Class TableOfContentItem
 * @package Drupal\publisso_gold\Controller
 */
class TableOfContentItem {
    
    private $id;
    private $elements           = [];
    private $tbl_column_prefix  = 'toci_';
    var $level                  = 0;
        
        /**
         * @return string
         */
        public function __toString(){
        return "class TableOfContentItem";
    }
        
        /**
         * TableOfContentItem constructor.
         * @param $id
         */
        public function __construct($id){
        
        $this->id = $id;
        $this->load($this->id);
    }
        
        /**
         * @param $elemName
         * @param null $elemValue
         * @param bool $storeOnDB
         * @return bool
         */
        public function setElement($elemName, $elemValue = null, $storeOnDB = true){
        
        if(array_key_exists($elemName, $this->elements)){
            
            $oldValue = $this->elements[$elemName];
            $this->elements[$elemName] = $elemValue;
            
            if($storeOnDB === false)
                return true;
            
            if($this->save($elemName) === false){
                
                $this->elements[$elemName] = $oldValue;
                return false;
            }
            else{
                return true;
            }
        }
        else{
            return false;
        }
    }
        
        /**
         * @param $elemName
         * @return mixed
         */
        public function getElement($elemName){
		if($elemName == 'id') return $this->id;
        return $this->elements[$elemName];
    }
        
        /**
         * @param null $elemName
         * @return \Drupal\Core\Database\StatementInterface|int|string|null
         */
        public function save($elemName = null){
        
        $fields = $this->elements;
        
        if($elemName !== null){ //if specific element selected
            $fields = [$elemName => $fields[$elemName]];
        }
        
        foreach($fields as $k => $v){
            $fields[$this->tbl_column_prefix.$k] = $v;
            unset($fields[$k]);
        }
        
        return \Drupal::database()->update('rwPubgoldTableOfContentItem')
        ->fields($fields)
        ->condition($this->tbl_column_prefix.'id', $this->id)
        ->execute();
    }
    
    private function load(){
        
        if($this->id){
            
            $result = \Drupal::database()->select('rwPubgoldTableOfContentItem', 'toci')
                     ->fields('toci', [])
                     ->condition($this->tbl_column_prefix.'id', $this->id, '=')
                     ->execute()
                     ->fetchAssoc();
            
            foreach($result as $k => $v){
                
                if($k != $this->tbl_column_prefix.'id') //store except id
                  $this->elements[str_replace($this->tbl_column_prefix, '', $k)] = $v;
            }
        }
    }
}

?>