<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\Cron.
 */

namespace Drupal\publisso_gold\Controller;

use Drupal;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\Unicode;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns autosave responses.
 */
class Cron extends ControllerBase{
	
        protected $modname = 'publisso_gold';
        protected $modpath;
        protected $cronKey;
        
        public function __construct(){
                
                $this->modpath = drupal_get_path('module', $this->modname);
                $this->cronKey = \Drupal::service('publisso_gold.setup')->getValue('system.cron.key');
        }
        
        /**
         * @param Request $request
         * @return JsonResponse
         */
        public function reminderDeadline(Request $request){
                
                $response = new JsonResponse();
                $response->setContent('');
                
                if($request->get('cronKey') != $this->cronKey || $request->get('cronKey') == null){
                        $response->setContent('500: '.((string)t('Forbidden!')));
                        return $response;
                }
                
                $ary['route_name'] = \Drupal::routeMatch()->getRouteName();
                $ary['route_param'] = \Drupal::routeMatch()->getRawParameters()->all();
                
                $mailer = \Drupal::service('plugin.manager.mail');
                
                //get workflows not published
                $wf_ids = \Drupal::service('publisso_gold.tools')->getUnpublishedWorkflowIDs();
                
                $content = '';
                foreach($wf_ids as $wf_id){
                        
                        //get workflow
                        $workflow = new Workflow($wf_id);
                        
                        //get medium
                        $medium = \Drupal::service('publisso_gold.tools')->getWorkflowMedium($workflow);
                        
                        if($medium){
                                
                                list($_, $mediumType) = explode(' ', $medium->__toString());
                                $content .= $workflow->getElement('id').': '.$mediumType."\n";
                        }
                }
                $response->setContent($content);
                return $response;
        }
}
?>
