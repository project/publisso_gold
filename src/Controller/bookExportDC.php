<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\bookExportDC.
 */

namespace Drupal\publisso_gold\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class bookExportDC
 * @package Drupal\publisso_gold\Controller
 */
class bookExportDC extends ControllerBase{
        
        private $session;
        
        /**
         * @param ContainerInterface $container
         * @return bookExportDC|static
         */
        public static function create(ContainerInterface $container){
                \Drupal::service('page_cache_kill_switch')->trigger();
                return new static($container->get('database'));
        }
    
        public function __construct(Connection $database){
                
                \Drupal::service('publisso_gold.tools')->forceUserAction();
                $this->session = \Drupal::service('session');
                
                if (!$this->session->isStarted()) {
                        $this->session->migrate();
                }
                
                $this->session->set('lastaccess', time());
        }
}
