<?php
        
        
        namespace Drupal\publisso_gold\Controller;


        /**
         * Class Publisso
         * @package Drupal\publisso_gold\Controller
         */
        class Publisso {
        
                /**
                 * @return User
                 */
                public static function currentUser(){
                        
                        $session = \Drupal::service('session');
                        if($session->has('logged_in') && $session->get('logged_in')) return new User($session->get('user')['id']);
                        else return new User();
                }
        
                /**
                 * @param $userID
                 * @return User
                 */
                public static function User($userID){
                        return new User($userID);
                }
        
                /**
                 * @return Setup
                 */
                public static function setup(){
                        return new Setup();
                }
        
                /**
                 * @return Tools
                 */
                public static function tools(){
                        return new Tools();
                }
        
                /**
                 * @return Texts
                 */
                public static function text(){
                        return new Texts();
                }
        
                /**
                 * @return LoginProvider
                 */
                public static function LoginProvider(){
                        return new LoginProvider();
                }
        
                /**
                 * @param string $msg
                 */
                public static function log(string $msg){
                        $logPath = self::tools()->getVarDirAbsolute().'/log';
                        if(!is_dir($logPath)) @mkdir($logPath, 0755, true);
        
                        $lastLogsStanding = [
                                date('Y-m-d', time()).'.log',
                                date('Y-m-d', time()-86400).'.log',
                                date('Y-m-d', time()-86400 * 2).'.log',
                                date('Y-m-d', time()-86400 * 3).'.log',
                                date('Y-m-d', time()-86400 * 4).'.log',
                        ];
        
                        foreach(glob($logPath.'/*.log') as $file){
                                if(!in_array(basename($file), $lastLogsStanding)) unlink($file);
                        }
        
                        $logFile = $logPath.'/'.date('Y-m-d', time()).'.log';
        
                        if($fh = fopen($logFile, 'a+')){
                                fwrite($fh, '=========='."\n".date('H:i:s')."\n".$msg."\n==========\n\n");
                                fclose($fh);
                        }
                }
        }
