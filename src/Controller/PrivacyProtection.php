<?php

	/**
	 * @file
	 * Contains \Drupal\publisso_gold\Controller\PrivacyProtection.
	 */

	namespace Drupal\publisso_gold\Controller;
	use Drupal\Core\Controller\ControllerBase;
	use Symfony\Component\DependencyInjection\ContainerInterface; 
        use Drupal\Core\Database\Connection;
	use Symfony\Component\HttpFoundation\Session\Session;
	use Drupal\Core\Url;
	use Drupal\Core\Link;
	use Symfony\Component\HttpFoundation\Request;
	
	use \Drupal\publisso_gold\Controller\Texts;
	use \Drupal\publisso_gold\Controller\Tools;
        
        /**
         * Class PrivacyProtection
         * @package Drupal\publisso_gold\Controller
         */
        class PrivacyProtection extends ControllerBase{
		
                private $modname;
                private $session;
                private $route;
                private $texts;
		private $tools;
		private $ReflectionClass;
		private $raw;
                
                /**
                 * @return string
                 */
                public function __toString(){
			return $this->ReflectionClass->getName();
		}
		
		/**
		 * {@inheritdoc}
		 */
		public static function create(ContainerInterface $container){
                        return new static(
                                $container->get('session'),
                                $container->get('publisso_gold.texts'),
                                $container->get('publisso_gold.tools')
                        );
		}
		
		public function __construct(Session $session, Texts $texts, Tools $tools){
                        
                        $this->ReflectionClass = new \ReflectionClass($this);
                        
                        \Drupal::service('publisso_gold.tools')->forceUserAction();
                        
                        $this->modname = strtolower($this->ReflectionClass->getShortName());
                        $this->session = $session;
                        $this->texts = $texts;
                        $this->tools = $tools;
                        
                        $this->route['name'] = \Drupal::routeMatch()->getRouteName();
                        $this->route['param'] = \Drupal::routeMatch()->getRawParameters()->all();
                        
                        $this->raw = null;
		}
                
                /**
                 * @param $action
                 * @param $date
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function main($action, $date){
                        
                        if(preg_match('/^(.+)\|(.+)/', $action, $matches)){
                                $action = $matches[1];
                                $date = $matches[2];
                        }
		        
                        //Zugriffsrechte prüfen
                        if(!$this->tools->userHasAccessRight($this->modname.'.'.$action, $this->session->get('user')['weight'])){
                                return $this->redirect('publisso_gold.privacy_protection', ['action' => 'view']);
                        }
                        
                        //will der User bestätigen und hat der letzten bereits zugestimmt? Dann nur ansehen....
                        $user = new \Drupal\publisso_gold\Controller\User($this->session->get('user')['id']);
                        
                        if($action == 'agree' && $user->profile->agreedLatestPrivacyProtection() === true){
                                return $this->redirect(
                                        'publisso_gold.privacy_protection', 
                                        [
                                                'action' => 'view', 
                                                'date' => base64_encode($this->getCurrent())
                                        ]
                                );
                        }
                        
                        try{
                                return array_merge($this->$action(base64_decode($date)), ['#cache' => ['max-age' =>
                                                                                                               0]]);
                        }
                        catch(\Error $e){
                                
                                $m = \Drupal::service('messenger');
                                error_log("Error in file >>".$e->getFile()."<< Line ".$e->getLine().": ".$e->getMessage());
                                $m->addError((string)t(\Drupal::service('publisso_gold.texts')->get('global.message.system.error', 'fco')), 'error');
                                return [];
                        }
		}
                
                /**
                 * @return array
                 */
                private function agree(){
                        
                        return [
                                'headline' => [
                                        '#prefix' => '<h1>',
                                        '#suffix' => '</h1><hr>',
                                        '#markup' => (string)$this->t($this->texts->get('privacy_protection.agree.headline', 'fc'))
                                ],
                                'text' => [
                                        '#type' => 'inline_template',
                                        '#template' => $this->getHTMLFromDate($this->getCurrent())
                                ],
                                'form' => \Drupal::formBuilder()->getForm("Drupal\publisso_gold\Form\agreePrivacyProtection")
                        ];
		}
                
                /**
                 * @param $action
                 * @return string
                 */
                public function _title($action){
          
		        if(preg_match('/^(.+)\|(.+)/', $action, $matches)){
                                $action = $matches[1];
                        }
		        
                        return (string)$this->t($this->texts->get('privacy_protection.page.title.'.$action, 'fc'));
		}
                
                /**
                 * @param null $date
                 * @return array[]
                 */
                private function view($date = null){
                        
                        if(!$date) $date = $this->getCurrent();
                        
                        $request = Request::createFromGlobals();
                        $referer = $request->server->get('HTTP_REFERER');
                        
                        $overrideLang = null;
                        if($request->query->has('lang')) $overrideLang = $request->query->get('lang');
                        
                        if($referer){
                                $url = Url::fromUri($referer);
                                $back = [
                                        '#prefix' => '<hr>',
                                        '#suffix' => '<br><br>',
                                        '#type' => 'link',
                                        '#title' => $this->texts->get('global.back', 'fc'),
                                        '#url' => $url,
                                        '#attributes' => [
                                                'class' => [
                                                        'btn',
                                                        'btn-warning'
                                                ]
                                        ]
                                ];
                        }
                        
                        $currLang = $overrideLang ? $overrideLang : \Drupal::service('language_manager')->getCurrentLanguage()->getId();
                        
                        if(!$this->languageAvailable($currLang, $date)) $currLang = $this->getDefaultLanguage($date);
                        
                        $translations = [];
                        $languages = $this->getLanguages($date);
                        
                        if(false !== ($index = array_search($currLang, $languages))){
                                unset($languages[$index]);
                        }
                        
                        if(count($languages)){
                                
                                $langs = [
                                        '#type' => 'fieldset',
                                        'headline' => [
                                                '#markup' => (string)$this->t($this->texts->get('privacy_protection.view.translation_hint', 'fc')),
                                                '#prefix' => '<h6><strong>',
                                                '#suffix' => '</strong></h6>'
                                        ],
                                        'content' => [],
                                        '#prefix' => '<hr>',
                                        '#suffix' => !$back ? '<hr><br>' : ''
                                ];
                                
                                foreach($languages as $_){
                                        
                                        $langs['content'][] = [
                                                '#type' => 'link',
                                                '#url' => Url::fromRoute(
                                                        'publisso_gold.privacy_protection', 
                                                        [
                                                                'action' => 'view', 
                                                                'date' => base64_encode($date),
                                                                'lang' => $_
                                                        ]
                                                ),
                                                '#title' => (string)$this->t(\Drupal::service('language_manager')->getLanguageName($_)),
                                                '#suffix' => '&nbsp;&nbsp;&nbsp;'
                                        ];
                                }
                        }
                        
                        $text = $this->getHTMLFromDate($date);
                        if($this->languageAvailable($currLang, $date)){
                                if($currLang != $this->getDefaultLanguage($date))
                                        $text = $this->getTranslatedHTMLFromDate($currLang, $date);
                        }
                        
                        return [
                                'translations' => $translations,
                                'text' => [
                                        '#type' => 'inline_template',
                                        '#template' => $text
                                ],
                                'langs' => $langs,
                                'back' => $back ? $back : []
                        ];
		}
                
                /**
                 * @param null $date
                 * @return array
                 */
                private function translate($date = null){
                        
                        if(!$date) $date = $this->getCurrent();
                        
                        return [
                                'headline' => [
                                        '#prefix' => '<h1>',
                                        '#suffix' => '</h1><hr>',
                                        '#markup' => (string)$this->t($this->texts->get('privacy_protection.translate.headline', 'fc'), ['@date' => $date])
                                ],
                                'form' => \Drupal::formBuilder()->getForm("Drupal\publisso_gold\Form\\translatePrivacyProtection", ['date' => $date])
                        ];
		}
		
		private function list(){
                        
                        $qry = \Drupal::database()->select('rwPubgoldPrivacyProtection', 't')->fields('t', []);
                        
                        try{
                                $res = $qry->execute()->fetchAll();
                        }
                        catch(\Drupal\Core\Database\DatabaseExceptionWrapper $e){
                                error_log("Error in file >>".$e->getFile()."<< Line ".$e->getLine().": ".$e->getMessage());
                                \Drupal::service('messenger')->addError((string)t(\Drupal::service
                                ('publisso_gold.texts')
                                                                                    ->get('global.message.system.error', 'fco')));
                                return [];
                        }
                        
                        $rows = [];
                        
                        foreach($res as $_){
                                
                                $date = $_->id;
                                $page = new \DOMDocument();
                                $page->loadXML($_->page);
                                $xpath = new \DOMXpath($page);
                                
                                $desc = '';
                                $descList = $xpath->evaluate('/root/description');
                                
                                if($descList->length){
                                        $desc = $descList[0]->nodeValue;
                                }
                                
                                $urlView = Url::fromRoute(
                                        'publisso_gold.privacy_protection', 
                                        [
                                                'action' => 'view', 
                                                'date' => base64_encode($date),
                                                'lang' => $this->getDefaultLanguage($date)
                                        ]
                                );
                                
                                if(count(array_diff(array_keys(\Drupal::service('language_manager')->getLanguages()), $this->getLanguages($date)))){
                                        
                                        $translate = [
                                                '#type' => 'link',
                                                '#title' => (string)$this->t($this->texts->get('global.Translate', 'fc')),
                                                '#url' => Url::fromRoute(
                                                        'publisso_gold.privacy_protection', 
                                                        [
                                                                'action' => 'translate', 
                                                                'date' => base64_encode($date)
                                                        ]
                                                ),
                                                '#suffix' => '&nbsp;&nbsp;&nbsp;'
                                        ];
                                }
                                
                                $translations = [];
                                foreach($this->getTranslations($date) as $lang){
                                        
                                        $urlViewTranslated = Url::fromRoute(
                                                'publisso_gold.privacy_protection', 
                                                [
                                                        'action' => 'view|'.base64_encode($date),
                                                        'lang' => $lang
                                                ]
                                        );
                                        $translations[] = [
                                                '#type' => 'link',
                                                '#url' => $urlViewTranslated,
                                                '#title' => (string)$this->t(\Drupal::service('language_manager')->getLanguageName($lang))
                                        ];
                                }
                                
                                for($i = 0; $i < count($translations); $i++){
                                        if($i < count($translations) - 1) $translations[$i]['#suffix'] = '&nbsp;&nbsp;&nbsp;';
                                }
                                
                                $rows[] = [
                                        [
                                                'data' => [
                                                        '#markup' => $date
                                                ],
                                                'field' => 'date'
                                        ],
                                        [
                                                'data' => [
                                                        '#markup' => $desc
                                                ],
                                                'field' => 'desc'
                                        ],
                                        [
                                                'data' => [
                                                        '#type'  => 'link',
                                                        '#title' => \Drupal::service('language_manager')->getLanguageName($this->getDefaultLanguage($date)),
                                                        '#url'   => $urlView
                                                ],
                                                'field' => 'language'
                                        ],
                                        [
                                                'data' => [
                                                        '#type' => 'container',
                                                        'content' => $translations
                                                ],
                                                'field' => 'translations'
                                        ],
                                        [
                                                'data' => [
                                                        '#type' => 'container',
                                                        'content' => [
                                                                $translate ?? []
                                                        ]
                                                ],
                                                'field' => 'action'
                                        ],
                                ];
                        }
                        
                        $urlAdd = Url::fromRoute('publisso_gold.privacy_protection', ['action' => 'add']);
                        
                        return [
                                'headline' => [
                                        '#prefix' => '<h1>',
                                        '#suffix' => '</h1>',
                                        '#markup' => (string)$this->t($this->texts->get('privacy_protection.list.headline', 'fc'))
                                ],
                                'table' => [
                                        '#type' => 'table',
                                        '#header' => [
                                                [
                                                        'data' => (string)$this->t($this->texts->get('privacy_protection.list.column.date', 'fc')),
                                                        'field' => 'date' 
                                                ],
                                                [
                                                        'data' => (string)$this->t($this->texts->get('privacy_protection.list.column.desc', 'fc')),
                                                        'field' => 'desc'
                                                ],
                                                [
                                                        'data' => (string)$this->t($this->texts->get('privacy_protection.list.column.language', 'fc')),
                                                        'field' => 'language'
                                                ],
                                                [
                                                        'data' => (string)$this->t($this->texts->get('privacy_protection.list.column.transltions', 'fc')),
                                                        'field' => 'translations'
                                                ],
                                                [
                                                        'data' => '',
                                                        'field' => 'action'
                                                ]
                                        ],
                                        '#empty' => (string)$this->t($this->texts->get('privacy_protection.list.empty', 'fc')),
                                        /*'#rows' => $rows,*/
                                        '#cache' => [
                                                'max-age' => 0
                                        ],
                                        '#rows' => $rows
                                ],
                                'add' => [
                                        '#type' => 'link',
                                        '#url'  => $urlAdd,
                                        '#title' => (string)$this->t($this->texts->get('privacy_protection.list.add', 'fc')),
                                        '#attributes' => [
                                                'class' => [
                                                        'btn',
                                                        'btn-warning'
                                                ]
                                        ],
                                        '#suffix' => '<br><br>'
                                ]
                        ];
		}
                
                /**
                 * @return array
                 */
                private function add(){
                        
                        return [
                                'headline' => [
                                        '#prefix' => '<h1>',
                                        '#suffix' => '</h1><hr>',
                                        '#markup' => (string)$this->t($this->texts->get('privacy_protection.add.headline', 'fc'))
                                ],
                                'form' => \Drupal::formBuilder()->getForm("Drupal\publisso_gold\Form\addPrivacyProtection")
                        ];
		}
                
                /**
                 * @param null $date
                 */
                private function getRaw($date = null){
                        
                        if(!$date) $date = $this->getCurrent();
                        
                        $this->raw = \Drupal::database()->select('rwPubgoldPrivacyProtection', 't')
                                ->fields('t', [])->condition('id', $date, '=')->execute()->fetchAll()[0];
		}
                
                /**
                 * @param $valid_from
                 * @param $description
                 * @param $page
                 * @param $language
                 * @return bool
                 * @throws \Exception
                 */
                public function createNew($valid_from, $description, $page, $language){
                        
                        $doc = new \DOMDocument('1.0', 'UTF-8');
                        $xpath = new \DOMXpath($doc);
                        \Drupal::service('publisso_gold.tools')->DOMAppendChild('root', '', [], $doc, $xpath->evaluate('/')[0]);
                        \Drupal::service('publisso_gold.tools')->DOMAppendChild('description', $description, [], $doc, $xpath->evaluate('/root')[0]);
                        \Drupal::service('publisso_gold.tools')->DOMAppendChild('text', base64_encode($page), [], $doc, $xpath->evaluate('/root')[0]);
                        \Drupal::service('publisso_gold.tools')->DOMAppendChild('created', '', ['timestamp' => $valid_from, 'uid' => $this->session->get('user')['id']],
                        $doc, $xpath->evaluate('/root')[0]);
                        $xpath->evaluate('/root/text')[0]->setAttribute('lang', $language);
                        $doc->formatOutput = true;
                        
                        $qry = \Drupal::database()->insert('rwPubgoldPrivacyProtection')->fields(['id' => $valid_from, 'page' => $doc->saveXML()]);
                        
                        try{
                                $res = $qry->execute();
                                return true;
                        }
                        catch(\Drupal\Core\Database\DatabaseExceptionWrapper $e){
                                error_log("Error in file >>".$e->getFile()."<< Line ".$e->getLine().": ".$e->getMessage());
                                drupal_set_message((string)t(\Drupal::service('publisso_gold.texts')->get('global.message.system.error', 'fco')), 'error');
                                return false;
                        }
		}
                
                /**
                 * @param null $date
                 * @return |null
                 */
                public function getDefaultLanguage($date = null){
                        
                        if(!$date) $date = $this->getCurrent();
                        if(!$this->raw) $this->getRaw($date);
                        
                        $doc = new \DOMDocument();
                        $doc->loadXML($this->raw->page);
                        $xpath = new \DOMXpath($doc);
                        $nodeList = $xpath->evaluate('/root/text');
                        
                        return $nodeList->length ? ($nodeList->item(0)->hasAttribute('lang') ? $nodeList->item(0)->getAttribute('lang') : null) : null;
		}
                
                /**
                 * @param null $date
                 * @return false|string
                 */
                public function getHTMLFromDate($date = null){
                        
                        if(!$date) $date = $this->getCurrent();
                        if(!$this->raw) $this->getRaw($date);
                        
                        $doc = new \DOMDocument();
                        $doc->loadXML($this->raw->page);
                        $xpath = new \DOMXpath($doc);
                        
                        return base64_decode($xpath->evaluate('/root/text')[0]->nodeValue);
		}
                
                /**
                 * @param $lang
                 * @param null $date
                 * @return false|string
                 */
                public function getTranslatedHTMLFromDate($lang, $date = null){
                        
                        if(!$date) $date = $this->getCurrent();
                        if(!$this->raw) $this->getRaw($date);
                        
                        $doc = new \DOMDocument();
                        $doc->loadXML($this->raw->page);
                        $xpath = new \DOMXpath($doc);
                        
                        $nodeList = $xpath->evaluate('/root/translations/translation[@lang="'.$lang.'"]');
                        
                        if($nodeList->length)
                                return base64_decode($xpath->evaluate('/root/translations/translation[@lang="'.$lang.'"]')[0]->nodeValue);
                        else
                                return base64_decode($xpath->evaluate('/root/text')[0]->nodeValue);
		}
                
                /**
                 * @param null $date
                 * @return mixed
                 */
                private function getXML($date = null){
                        
                        if(!$date) $date = $this->getCurrent();
                        if(!$this->raw) $this->getRaw($date);
                        return $this->raw->page;
		}
                
                /**
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 */
                private function save(){
                        
                        return \Drupal::database()->update('rwPubgoldPrivacyProtection')
                                ->fields(['page' => $this->raw->page])->condition('id', $this->raw->id, '=')
                                ->execute();
		}
                
                /**
                 * @return mixed
                 */
                public function getCurrent(){
                        return \Drupal::database()->select('rwPubgoldPrivacyProtection', 't')->fields('t', ['id'])->orderBy('id', 'DESC')->range(0, 1)->execute()->fetchField();
		}
                
                /**
                 * @param $lang
                 * @param $text
                 * @param null $date
                 */
                public function setTranslation($lang, $text, $date = null){
                        
                        if(!$date) $date = $this->getCurrent();
                        if(!$this->raw) $this->getRaw($date);
                        
                        $doc = new \DOMDocument();
                        $doc->loadXML($this->raw->page);
                        $xpath = new \DOMXpath($doc);
                        
                        if(!$xpath->evaluate('/root/translations')->length){
                                \Drupal::service('publisso_gold.tools')->DOMAppendChild('translations', '', [], $doc, $xpath->evaluate('/root')[0]);
                        }
                        
                        if(!$xpath->evaluate('/root/translations/translation[@lang="'.$lang.'"]')->length){
                                \Drupal::service('publisso_gold.tools')->DOMAppendChild(
                                        'translation', 
                                        base64_encode($text), 
                                        ['lang' => $lang], 
                                        $doc, 
                                        $xpath->evaluate('/root/translations')[0]
                                );
                        }
                        
                        $doc->formatOutput = true;
                        $this->raw->page = $doc->saveXML();
                        $this->save();
		}
                
                /**
                 * @param null $date
                 * @return array
                 */
                public function getTranslations($date = null){
                        
                        if(!$date) $date = $this->getCurrent();
                        if(!$this->raw) $this->getRaw($date);
                        
                        $doc = new \DOMDocument();
                        $doc->loadXML($this->raw->page);
                        $xpath = new \DOMXpath($doc);
                        
                        $langs = [];
                        $translatedList = $xpath->evaluate('/root/translations/translation');
                        
                        if($translatedList->length){
                                
                                foreach($translatedList as $node){
                                        if($node->hasAttribute('lang')) $langs[] = $node->getAttribute('lang');
                                }
                        }
                        
                        return $langs;
		}
                
                /**
                 * @param $lang
                 * @return bool
                 */
                public function hasTranslations($lang){
                        if(!$date) $date = $this->getCurrent();
                        if(!$this->raw) $this->getRaw($date);
                        return !!count($this->getTranslations($date));
		}
                
                /**
                 * @param $lang
                 * @param null $date
                 * @return bool
                 */
                public function hasTranslation($lang, $date = null){
                        
                        if(!$date) $date = $this->getCurrent();
                        if(!$this->raw) $this->getRaw($date);
                        return in_array($this->getTranslations($date));
		}
                
                /**
                 * @param $date
                 * @return array
                 */
                public function getLanguages($date){
                        return array_merge([$this->getDefaultLanguage($date)], $this->getTranslations($date));
		}
                
                /**
                 * @param $lang
                 * @param null $date
                 * @return bool
                 */
                public function languageAvailable($lang, $date = null){
                        
                        if(!$date) $date = $this->getCurrent();
                        if(!$this->raw) $this->getRaw($date);
                        return in_array($lang, $this->getLanguages($date));
		}
        }
?>

