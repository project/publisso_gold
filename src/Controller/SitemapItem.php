<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\SitemapItem.
 */

namespace Drupal\publisso_gold\Controller;

/**
 * Class SitemapItem
 * @package Drupal\publisso_gold\Controller
 */
class SitemapItem {
    
    private $id;
    private $elements           = [];
    private $tbl_column_prefix  = 'sm_';
    private $childs             = [];
    var $level                  = 0;
        
        /**
         * @return string
         */
        public function __toString(){
        return "class SitemapItem";
    }
        
        /**
         * SitemapItem constructor.
         * @param $item
         */
        public function __construct($item){
        
        $this->id = $item->sm_id;
        
        foreach($item as $k => $v){
            if($k != $this->tbl_column_prefix.'id')
                $this->elements[str_replace($this->tbl_column_prefix, '', $k)] = $v;
        }
    }
        
        /**
         * @param $elemName
         * @param null $elemValue
         * @param bool $storeOnDB
         * @return bool
         */
        public function setElement($elemName, $elemValue = null, $storeOnDB = true){
        
        if(array_key_exists($elemName, $this->elements)){
            
            $oldValue = $this->elements[$elemName];
            $this->elements[$elemName] = $elemValue;
            
            if($storeOnDB === false)
                return true;
            
            if($this->save($elemName) === false){
                
                $this->elements[$elemName] = $oldValue;
                return false;
            }
            else{
                return true;
            }
        }
        else{
            return false;
        }
    }
        
        /**
         * @param $elemName
         * @return mixed
         */
        public function getElement($elemName){
		if($elemName == 'id') return $this->id;
        return $this->elements[$elemName];
    }
        
        /**
         * @param null $elemName
         * @return \Drupal\Core\Database\StatementInterface|int|string|null
         */
        public function save($elemName = null){
        
        $fields = $this->elements;
        
        if($elemName !== null){ //if specific element selected
            $fields = [$elemName => $fields[$elemName]];
        }
        
        foreach($fields as $k => $v){
            $fields[$this->tbl_column_prefix.$k] = $v;
            unset($fields[$k]);
        }
        
        return \Drupal::database()->update('rwPubgoldSitemap')
        ->fields($fields)
        ->condition($this->tbl_column_prefix.'id', $this->id)
        ->execute();
    }
}

?>