<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\Autosave.
 */

namespace Drupal\publisso_gold\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Unicode;
use Drupal;

/**
 * Returns autosave responses.
 */
class Autosave {

	/**
	* Returns response for the country name autocompletion.
	*
	* @param \Symfony\Component\HttpFoundation\Request $request
	*   The current request object containing the search string.
	*
	* @return \Symfony\Component\HttpFoundation\JsonResponse
	*   A JSON response containing the autocomplete suggestions for countries.
	*/
	
	public function content(Request $request){
		
		$string = $request->request;
		$param = $string->all();
                
                $response = new Response();
                
		if($param['p1']['ctrl_action'] == 'autosave'){
			
			switch($param['p1']['ctrl_action']){
				
				case 'autosave':
					
					switch($param['p1']['ctrl_type']){
						
						case 'bookchapter':
                                                        
                                                        $param['p2'] = unserialize(base64_decode($param['p2']));
							list($err, $tmp_id) = $this->autosaveBookchapter($param);
                                                        
                                                        $r = [
                                                                'error' => $err,
                                                                'data' => $tmp_id,
                                                                'debug' => '<pre>'.print_r($param, 1).'</pre>'
                                                        ];
                                                        $response->setContent(json_encode($r));
							break;
                                                
                                                case 'journalarticle':
                                                        $param['p2'] = unserialize(base64_decode($param['p2']));
							list($err, $tmp_id) = $this->autosaveJournalarticle($param);
                                                        
                                                        $r = [
                                                                'error' => $err,
                                                                'data' => $tmp_id,
                                                                'debug' => '<pre>'.print_r($param, 1).'</pre>'
                                                        ];
                                                        $response->setContent(json_encode($r));
                                                        break;
					}
					break;
			}
		}
                
                return $response;
	}
        
        /**
         * @param $param
         * @return array
         * @throws \Exception
         */
        private function autosaveBookchapter($param){
        
		$dbo = \Drupal::database();
		
                foreach($param as $k => $v) $$k = $v;
                
                //p1 has values from actually shown form
                //p2 has values from formerly processed forms
                //let's only use p2
                //maybe, p1 (actual form) has more actually values - assign them to p2
                
                //so let's start with single values
                $sv = [
                        'title', 'keywords', 'abstract', 'chapter_text', 'conflict_of_interest', 'conflict_of_interest_text',
                        'funding', 'funding_name', 'funding_id', 'add_files', 'accept_policy', 'references'
                ];
                
                foreach($sv as $key)
                        if(array_key_exists($key, $p1)) $p2[$key] = $p1[$key];
                
                //now, the array of corporations
                $key = 'corporations[content';
                if(array_key_exists($key, $p1) && is_array($p1[$key])){
                        
                        $p2['corporation'] = [];
                        
                        foreach($p1[$key] as $v){
                                $p2['corporation'][] = $v;
                        }
                }
                
                //and now all the authors
                $key = 'fs_authors[content';
                if(array_key_exists($key, $p1) && is_array($p1[$key])){
                        
                        $p2['author_add'] = [];
                        $p2['author_db' ] = [];
                        
                        foreach($p1[$key]['author-db']['content'] as $author){
                                
                                if(!empty($author['author'])){
                                        
                                        $_ = [
                                                'author' => $author['author'],
                                                'weight' => $author['weight']
                                        ];

                                        if(array_key_exists('is-corresponding', $author))
                                                $_['is-corresponding'] = $author['is-corresponding'];

                                        $p2['author_db'][] = $_;
                                }
                        }
                        
                        foreach($p1[$key]['author-add']['content'] as $author){
                                
                                if(!(
                                        empty($author['content']['firstname'  ]) ||
                                        empty($author['content']['lastname'   ]) ||
                                        empty($author['content']['affiliation'])
                                )){
                                        $p2['author_add'][] = $author['content'];
                                }
                        }
                }
                
                if(empty($p1['tmp_id'])){
                        
                        $tmp_wf = new \Drupal\publisso_gold\Controller\RunningSubmission();
                        $tmp_wf->create($_SESSION['user']['id']);
			$tmp_wf->setElement('bk_id', $p1['bk_id']);
                }
                else{
                        $tmp_wf = new \Drupal\publisso_gold\Controller\RunningSubmission($p1['tmp_id']);
                }
                
                foreach($p2 as $k => $v)
			$tmp_wf->setDataElement($k, $v);
        
                return [0, $tmp_wf->getElement('id')];
                
                
                
		if(!$param['tmp_id']){ //create new dataset
			
			$tmp_wf = new \Drupal\publisso_gold\Controller\RunningSubmission();
			$tmp_wf->create($_SESSION['user']['id']);
			$tmp_wf->setElement('bk_id', $param['bk_id']);
		}
		else{ //load existing dataset and update/insert data
			$tmp_wf = new \Drupal\publisso_gold\Controller\RunningSubmission($param['tmp_id']);
		}
		
		$authors = [];
		$weight = 0;
		
		foreach($param['authors'] as $_){
			
            if(!is_array($_)){
                
                list($id, $author) = explode('|', $_);
                $id = base_convert(trim($id), 36, 10);
                
                if($id){
                    
                    $author = new \Drupal\publisso_gold\Controller\User($id);
                    
                    $authors[] = [
                        'firstname'         => $author->profile->getElement('firstname'),
                        'lastname'          => $author->profile->getElement('lastname'),
                        'affiliation'       => $author->profile->getElement('institute'),
                        'uid'               => $author->getElement('id'),
                        'is_corresponding'  => false,
                        'weight'            => ++$weight
                    ];
                }
            }
            else{
                
                $authors = $_;
            }
		}
		
		foreach($param['more_authors'] as $_){
			
			if(!(empty($_['content']['more_author_name']) || empty($_['content']['more_author_lastname']) || empty($_['content']['more_author_affiliation']))){
			
				$authors[] = [
					'firstname'         => $_['content']['more_author_name'],
					'lastname'          => $_['content']['more_author_lastname'],
					'affiliation'       => $_['content']['more_author_affiliation'],
					'uid'               => null,
					'is_corresponding'  => false,
					'weight'            => ++$weight
				];
			}
		}
		$data['authors'] = $authors;
		
		$corporations = [];
		
		foreach($param['corporations'] as $k => $v){
			if(preg_match('/^\d+$/', $k) && !empty($v)) $corporations[] = $v;
		}
		
		$data['corporations'] = $corporations;
        
		foreach($data as $k => $v)
			$tmp_wf->setDataElement($k, $v);
        
		echo $tmp_wf->getElement('id');
		
		exit();
	}
        
        /**
         * @param $param
         * @return array
         * @throws \Exception
         */
        private function autosaveJournalarticle($param){

                $dbo = \Drupal::database();

                foreach($param as $k => $v) $$k = $v;

                //p1 has values from actually shown form
                //p2 has values from formerly processed forms
                //let's only use p2
                //maybe, p1 (actual form) has more actually values - assign them to p2

                //so let's start with single values
                $sv = [
                        'title', 'keywords', 'abstract', 'article_text', 'conflict_of_interest', 'conflict_of_interest_text', 'article_type',
                        'funding', 'funding_name', 'funding_id', 'add_files', 'accept_policy', 'references'
                ];

                foreach($sv as $key)
                        if(array_key_exists($key, $p1)) $p2[$key] = $p1[$key];

                //now, the array of corporations
                $key = 'corporations[content';
                if(array_key_exists($key, $p1) && is_array($p1[$key])){

                        $p2['corporation'] = [];

                        foreach($p1[$key] as $v){
                                $p2['corporation'][] = $v;
                        }
                }

                //and now all the authors
                $key = 'fs_authors[content';
                if(array_key_exists($key, $p1) && is_array($p1[$key])){

                        $p2['author_add'] = [];
                        $p2['author_db' ] = [];

                        foreach($p1[$key]['author-db']['content'] as $author){

                                if(!empty($author['author'])){

                                        $_ = [
                                                'author' => $author['author'],
                                                'weight' => $author['weight']
                                        ];

                                        if(array_key_exists('is-corresponding', $author))
                                                $_['is-corresponding'] = $author['is-corresponding'];

                                        $p2['author_db'][] = $_;
                                }
                        }

                        foreach($p1[$key]['author-add']['content'] as $author){

                                if(!(
                                empty($author['content']['firstname'  ]) ||
                                empty($author['content']['lastname'   ]) ||
                                empty($author['content']['affiliation'])
                                )){
                                        $p2['author_add'][] = $author['content'];
                                }
                        }
                }

                if(empty($p1['tmp_id'])){

                        $tmp_wf = new \Drupal\publisso_gold\Controller\RunningSubmission(null, $p1['ctrl_type']);
                        $tmp_wf->create(\Drupal::service('session')->get('user')['id'], $p1['ctrl_type']);
                        $tmp_wf->setElement('medium_id', $p1['jrn_id']);
                }
                else{
                        $tmp_wf = new \Drupal\publisso_gold\Controller\RunningSubmission($p1['tmp_id']);
                }

                foreach($p2 as $k => $v)
                        $tmp_wf->setDataElement($k, $v);

                return [0, $tmp_wf->getElement('id')];



                if(!$param['tmp_id']){ //create new dataset

                        $tmp_wf = new \Drupal\publisso_gold\Controller\RunningSubmission(null, $p1['ctrl_type']);
                        $tmp_wf->create(\Drupal::service('session')->get('user')['id'], $p1['ctrl_type']);
                        $tmp_wf->setElement('medium_id', $param['jrn_id']);
                }
                else{ //load existing dataset and update/insert data
                        $tmp_wf = new \Drupal\publisso_gold\Controller\RunningSubmission($param['tmp_id']);
                }

                $authors = [];
                $weight = 0;

                foreach($param['authors'] as $_){

                        if(!is_array($_)){

                                list($id, $author) = explode('|', $_);
                                $id = base_convert(trim($id), 36, 10);

                                if($id){

                                        $author = new \Drupal\publisso_gold\Controller\User($id);

                                        $authors[] = [
                                                'firstname'         => $author->profile->getElement('firstname'),
                                                'lastname'          => $author->profile->getElement('lastname'),
                                                'affiliation'       => $author->profile->getElement('institute'),
                                                'uid'               => $author->getElement('id'),
                                                'is_corresponding'  => false,
                                                'weight'            => ++$weight
                                        ];
                                }
                        }
                        else{

                        $authors = $_;
                        }
                }

                foreach($param['more_authors'] as $_){

                        if(!(empty($_['content']['more_author_name']) || empty($_['content']['more_author_lastname']) || empty($_['content']['more_author_affiliation']))){

                                $authors[] = [
                                        'firstname'         => $_['content']['more_author_name'],
                                        'lastname'          => $_['content']['more_author_lastname'],
                                        'affiliation'       => $_['content']['more_author_affiliation'],
                                        'uid'               => null,
                                        'is_corresponding'  => false,
                                        'weight'            => ++$weight
                                ];
                        }
                }
                $data['authors'] = $authors;

                $corporations = [];

                foreach($param['corporations'] as $k => $v){
                        if(preg_match('/^\d+$/', $k) && !empty($v)) $corporations[] = $v;
                }

                $data['corporations'] = $corporations;

                foreach($data as $k => $v)
                        $tmp_wf->setDataElement($k, $v);

                echo $tmp_wf->getElement('id');

                exit();
        }
}
?>
