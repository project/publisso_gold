<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Controller\PublissoGold.
         */
        
        namespace Drupal\publisso_gold\Controller;
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Form\FormInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Form\publisso_goldLogin;
        use Drupal\Core\Url;
        use Drupal\Core\Link;
        use Drupal\Core\Routing;
        use Drupal\Core\Database\Connection;
        use Drupal\Core\Entity\Entity;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use \Symfony\Component\HttpFoundation\Response;
        use Drupal\menu_link_content;
        use Drupal\Core\Menu\MenuLinktree;
        use Symfony\Component\HttpFoundation\Request;
        use Drupal\Component\Render\FormattableMarkup;
        use \Drupal\publisso_gold\Controller\Medium\Book;
        use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
        
        /**
         * Class PublissoGold
         * @package Drupal\publisso_gold\Controller
         */
        class PublissoGold extends ControllerBase {
                
                //define string-constants - deprecated
                const __LNK_BOOKS__                = 'Books';
                const __LNK_JOURNALS__             = 'Journals';
                const __LNK_CONFERENCES__          = 'Conferences';
                const __LNK_USERPROFILE__          = 'Userprofile';
                const __LNK_USERMANAGEMENT__       = 'Usermanagement';
                const __LNK_BOOKMANAGEMENT__       = 'Bookmanagement';
                const __LNK_JOURNALMANAGEMENT__    = 'Journalmanagement';
                const __LNK_CONFERENCEMANAGEMENT__ = 'Conferencemanagement';
                const __LNK_DASHBOARD__            = 'Dashboard';
                const __LNK_ADDBOOK__              = 'add book';
                const __LNK_ADDJOURNAL__           = 'add journal';
                const __LNK_ADDCONFERENCE__        = 'add conference';
                const __ACCESS_DENIED__            = 'You can not access this page. You may need to log in first.';
                const __DEFAULT_ROLE_ID__          = 7;
                const __DEFAULT_ROLE_NAME__        = 'Author';
                const __DEFAULT_ROLE_WEIGHT__      = 20;
                const __TXT_LICENSE__              = 'License';
                const __TXT_PUBLICATION_YEAR__     = 'Publication Year';
                const __TXT_PUBLICATION_PLACE__    = 'Publication Place';
                const __TXT_PUBLISHER__            = 'Publisher';
                const __TXT_DOI__                  = 'DOI';
                const __TXT_DDC__                  = 'DDC';
                const __TXT_CORRESPONDING_AUTHOR__ = 'Corresponding Author';
                const __TXT_URN__                  = 'URN';
                const __TXT_KEYWORDS__             = 'Keywords';
                const __TXT_FUNDING_NUMBER__       = 'Funding Number';
                const __TXT_FUNDING_ID__           = 'Funding ID';
                const __TXT_VERSION__              = 'Version';
                const __TXT_LNK_BACKTOBOOK__       = 'back to Book';
                const __TXT_LNK_BACKTOJOURNAL__    = 'back to Journal';
                const __TXT_LNK_BACKTOCONFERENCE__ = 'back to Conference';
                const __NO_BOOK_GIVEN__            = 'No book given';
                const __NO_JOURNAL_GIVEN__         = 'No journal given';
                const __NO_CONFERENCE_GIVEN__      = 'No conference given';
                const __NO_CHAPTER_GIVEN__         = 'Couldn\'t find chapter';
                const __NO_ARTICLE_GIVEN__         = 'Couldn\'t find article';
                const __NO_PAPER_GIVEN__           = 'Couldn\'t find paper';
                const __TXT_ARTICLE_TYPE__         = 'Article-type';
                const __TXT_VOLUME__               = 'Volume';
                const __TXT_VOLUME_TITLE__         = 'Volume-title';
                const __TXT_DOC_NUMBER__           = 'docNumber';
                const __TXT_TITLE_TRANSLATED__     = 'Title translated';
                const __TXT_FUNDING_NAME__         = 'Funding name';
                const __TXT_ISSUE__                = 'Issue';
                const __TXT_ISSUE_TITLE__          = 'Issue-title';
                const __TXT_SESSION__              = 'Session';
                const __TXT_PRESENTING_AUTHOR__    = 'Presenting author';
                
                
                private   $tmpl                = '';
                private   $tmpl_vars           = [];
                private   $modpath;
                private   $userrole            = 'guest';
                private   $user                = 'guest';
                private   $user_id             = null;
                private   $userweight          = 10;
                private   $login_form;
                private   $modaccessweights    = [];
                private   $route_name;
                private   $modname             = 'publisso_gold';
                private   $database;
                private   $force_redirect      = null;
                private   $force_redirect_data = [];
                private   $page_title;
                protected $currentUser         = null;
                private   $texts;
                private   $tools;
                private   $setup;
        
                /**
                 * @param ContainerInterface $container
                 * @return PublissoGold|static
                 */
                public static function create (ContainerInterface $container ) {
                        \Drupal::service( 'page_cache_kill_switch' )->trigger();
                        return new static( $container->get( 'database' ) );
                }
                
                public function __construct ( Connection $database ) {
                        
                        $_SESSION[ 'setup' ] = new Setup(); //deprecated
                        
                        $this->session = \Drupal::service( 'session' );
                        $session = $this->session;
                        $this->texts = \Drupal::service( 'publisso_gold.texts' );
                        $this->tools = \Drupal::service( 'publisso_gold.tools' );
                        $this->setup = \Drupal::service( 'publisso_gold.setup' );
                        
                        if ( !$session->isStarted() ) {
                                $session->migrate();
                        }
                        
                        //set various parameters
                        $this->route_name = \Drupal::routeMatch()->getRouteName();
                        $this->modpath = \Drupal::service( 'module_handler' )->getModule( 'publisso_gold' )->getPath();
                        $this->getModuleAccessPermissions(); //deprecated
                        $this->database = $database; //deprecated
                        
                        require_once( $this->modpath . '/inc/publisso_gold.lib.inc.php' ); //deprecated
                }
        
                /**
                 * @return \Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function content () {
                        //default route
                        return $this->redirect( 'publisso_gold.books' );
                }
        
                /**
                 * @return array
                 */
                public function books () {
                        
                        #$block = \Drupal\block\Entity\Block::load('publissogoldjournalcontentblock');
                        
                        
                        #echo '<pre>';
                        #print_r($block);
                        #echo '</pre>';
                        /*
                        $host = explode('.', \Drupal::request()->getHost())[0];

                        switch($host){

                                case 'zeitschriften':
                                        return $this->redirect('publisso_gold.journals');
                                        break;
                        }
                        */
                        $template = new Template();
                        $template->get( 'publisso_gold_books_booklist' );
                        
                        $templateBook = new Template();
                        $templateBook->get( 'publisso_gold_books_booklist_booklink' );
                        
                        $bookIDs = \Drupal::database()->select( 'rwPubgoldBooks', 't' )->fields( 't', [ 'bk_id' ] )->condition('bk_public', 1)->orderBy( 'bk_title', 'ASC' )->execute()->fetchCol();
                        $template->setVar( 'books_booklist', '' );
                        
                        foreach ( $bookIDs as $bk_id ) {
                                
                                #$book = new Book( $bk_id, true );
                                $book = BookManager::getBook($bk_id);
                                
                                $templateBook->clear();
                                $templateBook->setVar( 'bk_id', $bk_id );
                                $templateBook->setVar( 'books_booklist_link', $book->getElement( 'title' ) );
                                $templateBook->setVar( 'books_booklist_title', $book->getElement( 'title' ) );
                                $templateBook->setVar( 'books_booklist_description', base64_decode( $book->getElement( 'description' ) ) );
                                $templateBook->setVar( 'txt_lnk_view_book', (string)$this->t( 'View book' ) );
                                
                                $urlCover = Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'bc', 'id' => $bk_id ] );
                                $urlLogo = Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'bl', 'id' => $bk_id ] );
                                
                                if ( !empty( $link = $book->getCoverLink() ) ) {
                                        
                                        $templateBook->setVar( 'books_booklist_cover', '<a href="' . $link . '"><img class="media-object" src="' . $urlCover->toString() . '"></a>' );
                                        $templateBook->setVar( 'lnk_view_book', $link );
                                }
                                else {
                                        
                                        $templateBook->setVar( 'books_booklist_cover', '<img class="media-object" src="' . $urlCover->toString() . '"' );
                                        $urlBook = Url::fromRoute( 'publisso_gold.books_book', [ 'bk_id' => $bk_id ] );
                                        $templateBook->setVar( 'lnk_view_book', $urlBook->toString() );
                                }
                                
                                $templateBook->setVar( 'books_booklist_logo', $urlLogo->toString() );
                                $templateBook->setVar('books_booklist_logo_alt', $book->hasLogo() ? 'LOGO: '
                                                                                                     .$book->getElement('publisher') : '');
                                
                                $template->appendToVar( 'books_booklist', $templateBook->parse() );
                        }
                        
                        return [ '#type' => 'markup', '#markup' => $template->parse(), '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function DirectSubmit () {
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !( \Drupal::service( 'session' )->has( 'logged_in' ) && \Drupal::service( 'session' )->get( 'logged_in' ) === true ) ) {
                        #        return [ '#markup' => (string)$this->t( self::__ACCESS_DENIED__ ), '#cache' => [ 'max-age' => 0 ] ];
                        #}
                        
                        $this->renderVars();
                        return [ '#markup' => $this->tmpl, 'content' => \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\DirectSubmit" ), '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param null $bk_id
                 * @return array
                 */
                public function books_book_preview ($bk_id = null ) {
                        
                        $_SESSION[ 'action' ][ 'previewbook' ] = $bk_id; //deprecated
                        $_SESSION[ 'breadcrumb_info' ][ 'bk_id' ] = $bk_id; //deprecated
                        
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'bk_id' => $bk_id ] );
                        
                        $this->renderVars(); //deprecated
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $jrn_id
                 * @return array
                 */
                public function journalManagementRolesAssign ($jrn_id ) {
                        
                        $journal = new Journal( $jrn_id );
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'journalmanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'jrn_id' ] = $jrn_id; //deprecated
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'jrn_id' => $jrn_id ] );
                        
                        $form = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\JournalAssignRoles", [ 'jrn_id' => $jrn_id ] );
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $bk_id
                 * @param Request $request
                 * @return array
                 */
                public function book_addchapter ($bk_id, Request $request ) {
                        
                        $request = \Drupal::requestStack()->getCurrentRequest();
                        
                        $book = new Book( $bk_id );
                        $tempstore = \Drupal::service( 'user.private_tempstore' )->get( 'publisso_gold' );
                        
                        #if ( !( count( $book->readAuthors() ) == 0 || $book->isUserAuthor() || $book->isUserEditorialOffice() || $this->session->get( 'user' )[ 'weight' ] >= $this->setup->getValue( 'default.admin.role.weight' ) ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'bk_id' => $bk_id ] );
                        
                        $wf_id = isset( $_REQUEST[ 'wf_id' ] ) ? $_REQUEST[ 'wf_id' ] : null;
                        
                        $tmp_id = $request->query->get( 'continue' );
                        
                        if ( $tmp_id ) {
                                
                                if ( !\Drupal::database()->select( 'rwPubgoldBookchapter_temp', 't' )->fields( 't', [] )->condition( 'id', $tmp_id, '=' )->countQuery()->execute()->fetchField() ) {
                                        return $this->tools->accessDenied( (string)$this->t( 'Running submission not found' ) );
                                }
                                
                                if ( !\Drupal::database()->select( 'rwPubgoldBookchapter_temp', 't' )->fields( 't', [] )->condition( 'id', $tmp_id, '=' )->condition( 'uid', $this->session->get( 'user' )[ 'id' ] )->countQuery()->execute()->fetchField() ) {
                                        return $this->tools->accessDenied();
                                }
                                
                                $tempstore->set( 'bookchapter_tmpid', $tmp_id );
                        }
                        else {
                                //$tempstore->delete('bookchapter_tmpid');
                        }
                        
                        $form_addchapter = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldBookAddChapter", [ 'bk_id' => $bk_id, 'wf_id' => $wf_id, 'tmp_id' => $tmp_id ] );
                        
                        if ( !$wf_id ) //new submission
                                $form_addchapter_new = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\SubmitBookChapter", [ 'bk_id' => $bk_id, 'tmp_id' => $tmp_id ] );
                        else
                                $form_addchapter_new = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\RevisionBookChapter", [ 'bk_id' => $bk_id, 'wf_id' => $wf_id, 'tmp_id' => $tmp_id ] );
                        
                        $this->renderVars();
                        
                        return $form_addchapter_new;
                }
        
                /**
                 * @param $jrn_id
                 * @param $vol_id
                 * @param $iss_id
                 * @param $jrna_id
                 * @return string
                 */
                public function journalarticle_page_title ($jrn_id, $vol_id, $iss_id, $jrna_id ) {
                        
                        $article = new Journalarticle( $jrna_id );
                        $volume = new Volume( $vol_id );
                        $issue = new Issue( $iss_id );
                        
                        $volIss = 'Vol.' . $volume->getElement( 'number' );
                        if ( $issue->getElement( 'id' ) ) $volIss .= '(' . $issue->getElement( 'number' ) . ')';
                        
                        $title = (string)$this->t( '@article in @journal (@volIss)', [ '@article' => html_entity_decode( $article->getElement( 'title' ) ), '@journal' => html_entity_decode( $article->getParentElement( 'title' ) ), '@volIss' => $volIss ] );
                        return $title;
                }
        
                /**
                 * @param $jrn_id
                 * @param $vol_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function journal_volume ($jrn_id, $vol_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }

                        $journal = new Journal($jrn_id);

                        if(!$journal->hasVolume($vol_id)){
                                throw new NotFoundHttpException();
                        }

                        $_SESSION[ 'breadcrumb_info' ][ 'jrn_id' ] = $jrn_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'vol_id' ] = $vol_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'jrn_id' => $jrn_id, 'vol_id' => $vol_id ] );
                        
                        return [ '#markup' => '', '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $jrn_id
                 * @param $vol_id
                 * @param $iss_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function journal_volume_issue ($jrn_id, $vol_id, $iss_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }

                        $journal = new Journal($jrn_id);

                        $notFound = false;
                        if(!$journal->hasVolume($vol_id)) $notFound = true;
                        else{
                                $volume = new Volume($vol_id);
                                if(!$volume->hasIssue($iss_id)) $notFound = true;
                        }

                        if($notFound) throw new NotFoundHttpException();

                        $_SESSION[ 'breadcrumb_info' ][ 'jrn_id' ] = $jrn_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'vol_id' ] = $vol_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'iss_id' ] = $iss_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'jrn_id' => $jrn_id, 'vol_id' => $vol_id, 'iss_id' => $iss_id ] );
                        
                        return [ '#markup' => '', '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $jrn_id
                 * @param $vol_id
                 * @param $jrna_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function journal_volume_article ($jrn_id, $vol_id, $jrna_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }

                        $journal = new Journal($jrn_id);
                        $notFound = false;
                        if(!$journal->hasVolume($vol_id)) $notFound = true;
                        else{
                                $volume = new Volume($vol_id);
                                if(!$volume->hasArticle($jrna_id)) $notFound = true;
                        }

                        if($notFound) throw new NotFoundHttpException();

                        $_SESSION[ 'breadcrumb_info' ][ 'jrn_id' ] = $jrn_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'vol_id' ] = $vol_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'jrna_id' ] = $jrna_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'jrn_id' => $jrn_id, 'vol_id' => $vol_id, 'jrna_id' => $jrna_id ] );
                        
                        return [ '#markup' => '', '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $jrn_id
                 * @param $vol_id
                 * @param $iss_id
                 * @param $jrna_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function journal_volume_issue_article ($jrn_id, $vol_id, $iss_id, $jrna_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }

                        $journal = new Journal($jrn_id);
                        $notFound = false;
                        if(!$journal->hasVolume($vol_id)) $notFound = true;
                        else{
                                $volume = new Volume($vol_id);
                                if(!$volume->hasIssue($iss_id)) $notFound = true;
                                else{
                                        $issue = new Issue($iss_id);
                                        if(!$issue->hasArticle($jrna_id)) $notFound = true;
                                }
                        }

                        if($notFound) throw new NotFoundHttpException();

                        $_SESSION[ 'breadcrumb_info' ][ 'jrn_id' ] = $jrn_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'vol_id' ] = $vol_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'iss_id' ] = $iss_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'jrna_id' ] = $jrna_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'jrn_id' => $jrn_id, 'vol_id' => $vol_id, 'jrna_id' => $jrna_id, 'iss_id' => $iss_id ] );
                        
                        return [ '#markup' => '', '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param null $bk_id
                 * @param null $cp_id
                 * @param string $action
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function book_readchapter ($bk_id = null, $cp_id = null, $action = 'content' ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        if ( $cp_id === null ) {
                                return [ '#type' => 'markup', '#markup' => t( self::__NO_CHAPTER_GIVEN__ ), '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        }
                        
                        $this->tmpl = file_get_contents( $this->modpath . '/inc/publisso_gold_book_chapter.tmpl.inc.php' );
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'bk_id' ] = $bk_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'cp_id' ] = $cp_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'action' ] = $action;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'bk_id' => $bk_id, 'cp_id' => $cp_id, 'action' => $action ] );
                        
                        $book = new Book( $bk_id );
                        $chapter = new \Drupal\publisso_gold\Controller\Bookchapter( $cp_id );
                        
                        if ( ( $chapter = $book->readChapter( $chapter->getElement( 'id' ) ) ) === false ) {
                                
                                return [ '#type' => 'markup', '#markup' => '<div class="rwError">' . ( (string)t( 'Chapter not found or the selected chapter is not part of this book!' ) ) . '</div>', '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        }
                        
                        $_SESSION[ 'action' ][ 'viewbook' ] = $bk_id;
                        $_SESSION[ 'action' ][ 'viewchapter' ] = $cp_id;
                        $_SESSION[ 'action' ][ 'action' ] = $action;
                        
                        $this->tmpl = $this->renderVars( $this->tmpl, $vars );
                        
                        $this->renderVars();
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param null $bk_id
                 * @param string $action
                 * @param Request $request
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function book_readchapter_preview ($bk_id = null, $action = 'content', Request $request ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'bk_id' ] = $bk_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'action' ] = $action;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'bk_id' => $bk_id, 'action' => $action ] );
                        
                        $book = new Book( $bk_id );
                        
                        #$_SESSION['action']['previewbook'] = $bk_id;
                        #$_SESSION['action']['previewchapter'] = 0;
                        #$_SESSION['action']['action'] = $action;
                        
                        //$_SESSION['data']['previewchapter'] = $_REQUEST;
                        
                        $this->renderVars();
                        return [ '#type' => 'markup', '#markup' => '', '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function journals () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $tmpl = new Template();
                        $tmpl->get( 'publisso_gold_journals_journallist' );
                        $journals = \Drupal::service( 'publisso_gold.tools' )->getJournalIDs(['jrn_public' => 1], ['jrn_title' => 'ASC']);
                        
                        $journallist = '';
                        
                        foreach ( $journals as $jrn_id ) {
                                
                                $tmpl_link = new Template();
                                $tmpl_link->get( 'publisso_gold_journals_journallist_journallink' );
                                $journal = new Journal( $jrn_id );
                                
                                $tmpl_link->setVar( 'journals_journallist_link', $journal->getElement( 'title' ) );
                                $tmpl_link->setVar( 'journals_journallist_cover', '<img class="media-object" src="/system/getPicture/ul/0" alt="">' );
                                $tmpl_link->setVar( 'journals_journallist_logo', '<img class="media-object" src="/system/getPicture/ul/0" alt="">' );
                                
                                if ( is_array( $journal->getElement( 'cover' ) ) && count( $journal->getElement( 'cover' ) ) ) {
                                        
                                        $tmpl_link->setVar( 'journals_journallist_cover', '' );
                                        
                                        foreach ( $journal->getElement( 'cover' ) as $_ ) {
                                                if ( !empty( $tmpl_link->getVar( 'journals_journallist_cover' ) ) ) $tmpl_link->setVar( 'journals_journallist_cover', '<br>' . $tmpl_link->getVar( 'journals_journallist_cover' ) );
                                                
                                                $tmpl_link->setVar( 'journals_journallist_cover', '<img class="media-object" src="/system/getPicture/ul/' . $_ . '" alt="">' . $tmpl_link->getVar( 'journals_journallist_cover' ) );
                                        }
                                }
                                
                                if ( is_array( $journal->getElement( 'logo' ) ) && count( $journal->getElement( 'logo' ) ) ) {
                                        
                                        $tmpl_link->setVar( 'journals_journallist_logo', '' );
                                        
                                        foreach ( $journal->getElement( 'logo' ) as $_ ) {
                                                
                                                if ( !empty( $tmpl_link->getVar( 'journals_journallist_logo' ) ) ) $tmpl_link->setVar( 'journals_journallist_logo', '<br>' . $tmpl_link->getVar( 'journals_journallist_logo' ) );
                                                $url = Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'ul', 'id' => $_ ] );
                                                
                                                $tmpl_link->setVar( 'journals_journallist_logo', '<img class="media-object" src="/system/getPicture/ul/' . $_ . '" alt="">' . $tmpl_link->getVar( 'journals_journallist_logo' ) );
                                        }
                                }
                                
                                
                                $tmpl_link->setVar( 'lnk_view_journal', Link::fromTextAndUrl( (string)t( 'View journal' ), Url::fromRoute( 'publisso_gold.journals_journal', [ 'jrn_id' => $journal->getElement( 'id' ) ] ) )->toString()
                                );
                                
                                $journallist .= $tmpl_link->parse();
                        }
                        //echo $journallist; exit();
                        $tmpl->setVar( 'journals_journallist', $journallist );
                        
                        return [
                                '#type'     => 'markup', '#markup' => $tmpl->parse(), /*'content' => array(
                $this->login_form
            )*/
                                '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                        
                        return [
                                '#type'     => 'markup', '#markup' => $this->tmpl, /*'content' => array(
                $this->login_form
            )*/
                                '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        
                /**
                 * @param null $jrn_id
                 * @param Request $request
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function journal_addarticle ($jrn_id = null, Request $request ) {
                        
                        $session = \Drupal::service( 'session' );
                        if ( !( $session->has( 'logged_in' ) && $session->get( 'logged_in' ) === true ) ) {
                                return [ '#markup' => (string)$this->t( self::__ACCESS_DENIED__ ), '#cache' => [ 'max-age' => 0 ] ];
                        }
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        if ( $jrn_id === null ) {
                                return [ '#type' => 'markup', '#markup' => t( self::__NO_JOURNAL_GIVEN__ ), '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        }
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'jrn_id' ] = $jrn_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'jrn_id' => $jrn_id ] );
                        $_SESSION[ 'action' ][ 'viewjournal' ] = $jrn_id;
                        $wf_id = isset( $_REQUEST[ 'wf_id' ] ) ? $_REQUEST[ 'wf_id' ] : null;
                        $tmp_id = $request->query->get( 'continue' );
                        
                        if ( !$wf_id ) //new submission
                                $form_addarticle_new = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\SubmitJournalArticle", [ 'jrn_id' => $jrn_id, 'tmp_id' => $tmp_id ] );
                        else
                                $form_addarticle_new = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\RevisionJournalArticle", [ 'jrn_id' => $jrn_id, 'wf_id' => $wf_id, 'tmp_id' => $tmp_id ] );
                        
                        $this->renderVars();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form_addarticle_new ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param null $jrna_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function journal_readarticle ($jrna_id = null ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        if ( $jrna_id === null ) {
                                return [ '#type' => 'markup', '#markup' => t( self::__NO_ARTICLE_GIVEN__ ), '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        }
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'jrna_id' ] = $jrna_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'jrna_id' => $jrna_id ] );
                        
                        $this->tmpl = file_get_contents( $this->modpath . '/inc/publisso_gold_journal_article.tmpl.inc.php' );
                        $article = \Drupal::database()->select( 'rwPubgoldJournalArticles', 'ja' )->fields( 'ja', [ 'jrna_jrnid', 'jrna_title', 'jrna_article_text' ] )->condition( 'jrna_id', $jrna_id, '=' )->execute()->fetchAssoc();
                        
                        $_SESSION[ 'action' ][ 'viewjournal' ] = $article[ 'jrna_jrnid' ];
                        
                        $article[ 'jrna_article_text' ] = json_decode( base64_decode( $article[ 'jrna_article_text' ] ) );
                        $article[ 'jrna_article_text' ] = $article[ 'jrna_article_text' ]->value;
                        
                        $vars = [ '::txt_lnk_backtojournal::' => t( self::__TXT_LNK_BACKTOJOURNAL__ ) ];
                        
                        foreach ( $article as $k => $v ) {
                                if ( !( is_array( $v ) || is_object( $v ) ) ) $vars[ "::$k::" ] = $v;
                        }
                        
                        $this->tmpl = $this->renderVars( $this->tmpl, $vars );
                        
                        $this->renderVars();
                        return [
                                '#type'     => 'markup', '#markup' => $this->tmpl, /*'content' => array(
                $this->login_form
            )*/
                                '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        
                /**
                 * @param null $cfp_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function conference_readpaper ($cfp_id = null ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        if ( $cfp_id === null ) {
                                return [ '#type' => 'markup', '#markup' => t( self::__NO_PAPER_GIVEN__ ), '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        }
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'cfp_id' ] = $cfp_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'cfp_id' => $cfp_id ] );
                        
                        $this->tmpl = file_get_contents( $this->modpath . '/inc/publisso_gold_conference_paper.tmpl.inc.php' );
                        $article = \Drupal::database()->select( 'rwPubgoldConferencePapers', 'cp' )->fields( 'cp', [] )->condition( 'cfp_id', $cfp_id, '=' )->execute()->fetchAssoc();
                        
                        $_SESSION[ 'action' ][ 'viewconference' ] = $article[ 'cfp_cfid' ];
                        
                        $article[ 'cfp_paper_text' ] = json_decode( base64_decode( $article[ 'cfp_paper_text' ] ) );
                        $article[ 'cfp_paper_text' ] = $article[ 'cfp_paper_text' ]->value;
                        
                        $vars = [ '::txt_lnk_backtoconference::' => t( self::__TXT_LNK_BACKTOCONFERENCE__ ) ];
                        
                        foreach ( $article as $k => $v ) {
                                if ( !( is_array( $v ) || is_object( $v ) ) ) $vars[ "::$k::" ] = $v;
                        }
                        
                        $this->tmpl = $this->renderVars( $this->tmpl, $vars );
                        
                        $this->renderVars();
                        return [
                                '#type'     => 'markup', '#markup' => $this->tmpl, /*'content' => array(
                $this->login_form
            )*/
                                '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        
                /**
                 * @param null $jrn_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function journals_journal ($jrn_id = null ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        if ( $jrn_id === null ) {
                                
                                return [ '#type' => 'markup', '#markup' => t( self::__NO_JOURNAL_GIVEN__ ), '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        }

                        $journal = new Journal($jrn_id);
                        if(!$journal->getElement('id')){
                                throw new NotFoundHttpException();
                        }

                        $_SESSION[ 'action' ][ 'viewjournal' ] = $jrn_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'jrn_id' ] = $jrn_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'jrn_id' => $jrn_id ] );
                        
                        
                        $this->renderVars();
                        return [
                                '#type'        => 'markup', '#markup' => $this->tmpl, 'content' => [ /*$editorial_board,*/ #$chapters
                                ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        
                /**
                 * @param null $cf_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function conference_addpaper ($cf_id = null ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        if ( $cf_id === null ) {
                                return [ '#type' => 'markup', '#markup' => t( self::__NO_CONFERENCE_GIVEN__ ), '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        }
                        
                        $_SESSION[ 'action' ][ 'viewconference' ] = $cf_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'cf_id' ] = $cf_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'cf_id' => $cf_id ] );
                        
                        $wf_id = isset( $_REQUEST[ 'wf_id' ] ) ? $_REQUEST[ 'wf_id' ] : null;
                        $form_addpaper = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldConferenceAddPaper", [ 'cf_id' => $cf_id, 'wf_id' => $wf_id ] );
                        $this->renderVars();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form_addpaper ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param null $cf_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function conferences_conference ($cf_id = null ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        if ( $cf_id === null ) {
                                
                                return [ '#type' => 'markup', '#markup' => t( self::__NO_CONFERENCE_GIVEN__ ), '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        }
                        
                        $_SESSION[ 'action' ][ 'viewconference' ] = $cf_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'cf_id' ] = $cf_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'cf_id' => $cf_id ] );
                        
                        $conference = getConference( $this->database, $cf_id );
                        
                        $this->tmpl .= file_get_contents( $this->modpath . '/inc/publisso_gold_conferences_conference.tmpl.inc.php' );
                        
                        $this->tmpl_vars[ '::txt_sustaining_members::' ] = t( 'Sustaining Members' );
                        $this->tmpl_vars[ '::txt_corporation::' ] = t( 'Corporation' );
                        $this->tmpl_vars[ '::txt_isbn::' ] = t( 'ISBN' );
                        $this->tmpl_vars[ '::txt_editorial_board::' ] = t( 'Editorial Board' );
                        $this->tmpl_vars[ '::txt_papers::' ] = (string)t( 'Papers' );
                        $this->tmpl_vars[ '::lnk_create_conference_paper::' ] = t( 'Add Paper' );
                        $this->tmpl_vars[ '::txt_edit_editorial_board::' ] = t( 'edit editorial board' );
                        
                        if ( !empty( $conference[ 'cf_corporation' ] ) ) $conference[ 'cf_corporation' ] = implode( ', ', json_decode( $conference[ 'cf_corporation' ] ) );
                        
                        foreach ( $conference as $k => $v ) {
                                if ( !( is_array( $v ) || is_object( $v ) ) ) $this->tmpl_vars[ "::$k::" ] = $v;
                        }
                        
                        if ( array_key_exists( 'user', $_SESSION ) && $_SESSION[ 'user' ][ 'weight' ] >= 20 ) {
                                
                                $this->tmpl_vars[ '::lnk_add_paper::' ] = '<a href="/publisso_gold/conference/add_paper/' . $conference[ 'cf_id' ] . '">' . t( 'Add Paper' ) . '</a>';
                        }
                        
                        $editorial_board = [ '#type' => 'details', '#title' => t( 'Editorial Board' ), '#open' => false, '#markup' => '' ];
                        
                        $this->renderVars();
                        
                        //get Papers
                        $papers_db = \Drupal::database()->select( 'rwPubgoldConferencePapers', 'cp' )->fields( 'cp', [] )->condition( 'cfp_cfid', $cf_id, '=' )->execute()->fetchAll();
                        
                        //generate paper-output
                        $papers = [];
                        
                        foreach ( $papers_db as $_ ) {
                                
                                $correction = json_decode( base64_decode( $_->cfp_correction ) );
                                $_->cfp_correction = '<h3>' . t( 'Correction' ) . ':</h3>' . $correction->value;
                                
                                $erratum = json_decode( base64_decode( $_->cfp_erratum ) );
                                $_->cfp_erratum = '<h3>' . t( 'Erratum' ) . ':</h3>' . $erratum->value;
                                
                                $abstract = json_decode( base64_decode( $_->cfp_abstract ) );
                                $_->cfp_abstract = '<h3>' . t( 'Abstract' ) . ':</h3>' . $abstract->value;
                                
                                $author = getAllUserData( \Drupal::database(), $_->cfp_author );
                                
                                $_->cfp_keywords = implode( ', ', json_decode( $_->cfp_keywords ) );
                                
                                $metadata = file_get_contents( $this->modpath . '/inc/publisso_gold_conferencemanagement_paper_metadata.tmpl.inc.php' );
                                $vars = [];
                                
                                $vars = [ '::txt_license::' => t( self::__TXT_LICENSE__ ), '::txt_publication_year::' => t( self::__TXT_PUBLICATION_YEAR__ ), '::txt_publication_place::' => t( self::__TXT_PUBLICATION_PLACE__ ), '::txt_publisher::' => t( self::__TXT_PUBLISHER__ ), '::txt_doi::' => t( self::__TXT_DOI__ ), '::txt_ddc::' => t( self::__TXT_DDC__ ), '::txt_urn::' => t( self::__TXT_URN__ ), '::txt_corresponding_author::' => t( self::__TXT_CORRESPONDING_AUTHOR__ ), '::txt_presenting_author::' => t( self::__TXT_PRESENTING_AUTHOR__ ), '::txt_keywords::' => t( self::__TXT_KEYWORDS__ ), '::txt_funding_name::' => t( self::__TXT_FUNDING_NAME__ ), '::txt_funding_id::' => t( self::__TXT_FUNDING_ID__ ), '::txt_version::' => t( self::__TXT_VERSION__ ), '::txt_article_type::' => t( self::__TXT_ARTICLE_TYPE__ ), '::txt_doc_number::' => t( self::__TXT_DOC_NUMBER__ ), '::txt_session::' => t( self::__TXT_SESSION__ ), ];
                                
                                foreach ( $_ as $k => $v ) {
                                        if ( !( is_array( $v ) || is_object( $v ) ) ) $vars[ "::$k::" ] = $v;
                                }
                                
                                $metadata = $this->renderVars( $metadata, $vars );
                                
                                $papers[] = [ '#type' => 'details', '#title' => $_->cfp_title, '#description' => t( 'Author' ) . ': ' . $author[ 'profile_graduation' ] . ' ' . $author[ 'profile_lastname' ] . ', ' . $author[ 'profile_firstname' ], '#markup' => $_->cfp_abstract . '<br><a href="/publisso_gold/conference/read_paper/' . $_->cfp_id . '">' . (string)t( 'read paper' ) . '</a>', '#open' => false, 'meta' => [ '#type' => 'details', '#title' => t( 'Metadata' ), '#open' => false, '#markup' => $metadata ], 'errata' => [ '#type' => 'details', '#title' => t( 'Errata' ), '#open' => false, '#markup' => $erratum->value ], 'correction' => [ '#type' => 'details', '#title' => t( 'Correction' ), '#open' => false, '#markup' => $correction->value ] ];
                        }
                        
                        $this->renderVars();
                        return [
                                '#type'        => 'markup', '#markup' => $this->tmpl, 'content' => [ /*$editorial_board,*/
                                        $papers,
                                ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function conferences () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $tmpl_conferencelist = file_get_contents( $this->modpath . '/inc/publisso_gold_conferences_conferencelist.tmpl.inc.php' );
                        $tmpl_conferencelist_link = file_get_contents( $this->modpath . '/inc/publisso_gold_conferences_conferencelist_conferencelink.tmpl.inc.php' );
                        $conferences = getConferences( $this->database );
                        
                        $conferencelist = '';
                        
                        foreach ( $conferences as $_ ) {
                                
                                $tmpl = $tmpl_conferencelist_link;
                                $vars = [];
                                $vars[ '::cf_id::' ] = $_->cf_id;
                                $vars[ '::conferences_conferencelist_link::' ] = $_->cf_title;
                                $tmpl = $this->renderVars( $tmpl, $vars );
                                $conferencelist .= $tmpl;
                        }
                        
                        $this->tmpl_vars[ '::conferences_conferencelist::' ] = $conferencelist;
                        $this->tmpl .= $tmpl_conferencelist;
                        
                        $this->renderVars();
                        
                        return [
                                '#type'     => 'markup', '#markup' => $this->tmpl, /*'content' => array(
                $this->login_form
            )*/
                                '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        
                /**
                 * @return array
                 */
                public function show_picture () {
                        //if($_SESSION['user']['id'] == 1){
                        //echo '<pre>'.print_r($_REQUEST, 1).'</pre>';
                        //phpinfo(); exit();
                        //}
                        //if($this->force_redirect) { $rd = $this->force_redirect; $this->force_redirect = NULL; return $this->redirect($rd); }
                        ob_clean();
                        
                        if ( isset( $_GET[ 'up' ] ) || isset( $_GET[ 'pv' ] ) || isset( $_GET[ 'pth' ] ) || isset( $_GET[ 'jl' ] ) || isset( $_GET[ 'jc' ] ) || isset( $_GET[ 'bl' ] ) || isset( $_GET[ 'bc' ] ) || isset( $_GET[ 'bsm' ] ) || isset( $_GET[ 'jsm' ] ) ) {
                                
                                if ( isset( $_GET[ 'up' ] ) ) {
                                        $picture = $this->database->select( 'rwPubgoldUserProfiles', 'up' )->fields( 'up', [ 'up_picture', 'up_picture_type' ] )->condition( 'up_uid', $_GET[ 'up' ], '=' )->execute()->fetchAssoc();
                                        $picture[ 'picture' ] = $picture[ 'up_picture' ];
                                        $picture[ 'picture_type' ] = $picture[ 'up_picture_type' ];
                                }
                                elseif ( isset( $_GET[ 'pv' ] ) ) {
                                        $picture = $this->database->select( 'rwPubgoldUserprofilePreview', 'pv' )->fields( 'pv', [ 'pv_picture', 'pv_picture_type' ] )->condition( 'pv_id', $_GET[ 'pv' ], '=' )->execute()->fetchAssoc();
                                        $picture[ 'picture' ] = $picture[ 'pv_picture' ];
                                        $picture[ 'picture_type' ] = $picture[ 'pv_picture_type' ];
                                }
                                elseif ( isset( $_GET[ 'pth' ] ) ) {
                                        $picture[ 'picture' ] = '0x' . bin2hex( file_get_contents( 'sites/default/files/inline-images/' . $_GET[ 'pth' ] ) );
                                        $picture[ 'picture_type' ] = mime_content_type( 'sites/default/files/inline-images/' . $_GET[ 'pth' ] );
                                }
                                elseif ( isset( $_GET[ 'jl' ] ) ) {
                                        $picture = $this->database->select( 'rwPubgoldJournals', 'jrn' )->fields( 'jrn', [ 'jrn_logo' ] )->condition( 'jrn_id', $_GET[ 'jl' ], '=' )->execute()->fetchAssoc();
                                        $picture[ 'picture' ] = $picture[ 'jrn_logo' ];
                                        $picture[ 'picture_type' ] = 'image';//$picture['jrn_logo_type'];
                                }
                                elseif ( isset( $_GET[ 'jc' ] ) ) {
                                        $picture = $this->database->select( 'rwPubgoldJournals', 'jrn' )->fields( 'jrn', [ 'jrn_cover' ] )->condition( 'jrn_id', $_GET[ 'jc' ], '=' )->execute()->fetchAssoc();
                                        $picture[ 'picture' ] = $picture[ 'jrn_cover' ];
                                        $picture[ 'picture_type' ] = 'image';//$picture['jrn_logo_type'];
                                }
                                elseif ( isset( $_GET[ 'bl' ] ) ) {
                                        $picture = $this->database->select( 'rwPubgoldBooks', 'bk' )->fields( 'bk', [ 'bk_logo', 'bk_logo_type' ] )->condition( 'bk_id', $_GET[ 'bl' ], '=' )->execute()->fetchAssoc();
                                        $picture[ 'picture' ] = $picture[ 'bk_logo' ];
                                        $picture[ 'picture_type' ] = $picture[ 'bk_logo_type' ];
                                }
                                elseif ( isset( $_GET[ 'bc' ] ) ) {
                                        $picture = $this->database->select( 'rwPubgoldBooks', 'bk' )->fields( 'bk', [ 'bk_cover', 'bk_cover_type' ] )->condition( 'bk_id', $_GET[ 'bc' ], '=' )->execute()->fetchAssoc();
                                        $picture[ 'picture' ] = $picture[ 'bk_cover' ];
                                        $picture[ 'picture_type' ] = $picture[ 'bk_cover_type' ];
                                }
                                elseif ( isset( $_GET[ 'bsm' ] ) ) {
                                        $picture = $this->database->select( 'rwPubgoldBooksSustainingMembers', 'bsm' )->fields( 'bsm', [ 'bsm_image', 'bsm_type' ] )->condition( 'bsm_id', $_GET[ 'bsm' ], '=' )->execute()->fetchAssoc();
                                        $picture[ 'picture' ] = $picture[ 'bsm_image' ];
                                        $picture[ 'picture_type' ] = $picture[ 'bsm_type' ];
                                }
                                elseif ( isset( $_GET[ 'jsm' ] ) ) {
                                        $picture = $this->database->select( 'rwPubgoldJournalsSustainingMembers', 'jsm' )->fields( 'jsm', [ 'jsm_image' ] )->condition( 'jsm_id', $_GET[ 'jsm' ], '=' )->execute()->fetchAssoc();
                                        $picture[ 'picture' ] = $picture[ 'jsm_image' ];
                                        $picture[ 'picture_type' ] = 'image';
                                }
                                #echo var_dump($picture); phpinfo(); exit();
                                if ( !empty( $picture[ 'picture' ] ) ) {
                                        #echo var_dump($picture); phpinfo(); exit();
                                        /*
                $headers = new Response(
                    hex2bin(substr($picture['picture'], 2, strlen($picture['picture']))),
                    200,
                    ['Content-Type' => $picture['picture_type']]
                );
                $headers->send();
                */
                                        header( 'http/1.0 200 ok' );
                                        header( 'content-type: ' . $picture[ 'picture_type' ] );
                                        echo hex2bin( substr( $picture[ 'picture' ], 2, strlen( $picture[ 'picture' ] ) ) );
                                        exit();
                                        return [];
                                }
                                elseif ( !array_key_exists( 'up', $_GET ) ) {
                                        
                                        $picture = getDefaultPic();
                                        header( 'http/1.0 200 ok' );
                                        header( 'content-type: ' . $picture[ 'type' ] );
                                        echo hex2bin( $picture[ 'content' ] );
                                        exit();
                                        return [];
                                }
                        }
                        
                        $picture = getDefaultUserpic();
                        
                        header( 'http/1.0 200 ok' );
                        header( 'content-type: image/png' );
                        echo hex2bin( $picture[ 'content' ] );
                        /*
        $headers = new Response(
            hex2bin($picture['content']),
            200,
            ['Content-Type' => $picture['type']]
        );
        $headers->send();
        */
                        return [];
                }
        
                /**
                 * @return array
                 */
                public function userprofile_edit () {
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'userprofile' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $user = new User( $this->session->get( 'user' )[ 'id' ] );
                        
                        return [
                                'headline' => [
                                        '#prefix' => '<h1>', '#suffix' => '</h1>', '#markup' => (string)$this->t( $this->texts->get( 'userprofile.edit.headline', 'fco'
                                        ), [ '@salutation' => $this->tools->getSalutation( $user->profile->getElement( 'salutation' ) ), '@name' => $user->profile->getReadableFullName() ]
                                        ),
                                ], 'form'  => \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldUserprofile", [ 'user' => $user ]
                                ), 'cache' => [ '#max-age' => 0 ],
                        ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function userprofile () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'userprofile' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $user = new \Drupal\publisso_gold\Controller\User( $this->session->get( 'user' )[ 'id' ] );
                        
                        $rows = [];
                        $_w = '20%';
                        
                        $_ = 'institute';
                        $cell1 = [ 'data' => (string)$this->t( $this->texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $user->profile->getElement( $_ ) ) ) $rows[] = [ $cell1, [ 'data' => $user->profile->getElement( $_ ) ] ];
                        
                        $_ = 'country';
                        $cell1 = [ 'data' => (string)$this->t( $this->texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $user->profile->getElement( $_ ) ) ) $rows[] = [ $cell1, [ 'data' => $this->tools->getCountry( $user->profile->getElement( $_ ) ) ] ];
                        
                        $_ = 'city';
                        $cell1 = [ 'data' => (string)$this->t( $this->texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $user->profile->getElement( $_ ) ) ) $rows[] = [ $cell1, [ 'data' => $user->profile->getElement( $_ ) ] ];
                        
                        $_ = 'street';
                        $cell1 = [ 'data' => (string)$this->t( $this->texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $user->profile->getElement( $_ ) ) ) $rows[] = [ $cell1, [ 'data' => $user->profile->getElement( $_ ) ] ];
                        
                        $_ = 'email';
                        $cell1 = [ 'data' => (string)$this->t( $this->texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $user->profile->getElement( $_ ) ) ) $rows[] = [ $cell1, [ 'data' => $user->profile->getElement( $_ ) ] ];
                        
                        $_ = 'department';
                        $cell1 = [ 'data' => (string)$this->t( $this->texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $user->profile->getElement( $_ ) ) ) $rows[] = [ $cell1, [ 'data' => $user->profile->getElement( $_ ) ] ];
                        
                        $_ = 'telephone';
                        $cell1 = [ 'data' => (string)$this->t( $this->texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $user->profile->getElement( $_ ) ) ) $rows[] = [ $cell1, [ 'data' => $user->profile->getElement( $_ ) ] ];
                        
                        $_ = 'area_of_expertise';
                        $cell1 = [ 'data' => (string)$this->t( $this->texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( count( $user->profile->getAreasOfExpertise() ) ) $rows[] = [ $cell1, [ 'data' => implode( ', ', $user->profile->getAreasOfExpertise() ) ] ];
                        
                        $_ = 'orcid';
                        $cell1 = [ 'data' => (string)$this->t( $this->texts->get( 'userprofile.' . $_ ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $user->profile->getElement( $_ ) ) ) $rows[] = [ $cell1, [ 'data' => $user->profile->getElement( $_ ) ] ];
                        
                        $_ = 'correspondence_language';
                        $cell1 = [ 'data' => (string)$this->t( $this->texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $user->profile->getElement( $_ ) ) ) $rows[] = [ $cell1, [ 'data' => \Drupal::service( 'language_manager' )->getLanguageName( $user->profile->getElement( $_ ) ) ] ];
                        
                        $_ = 'agree_privacy_protection';
                        $cell1 = [ 'data' => (string)$this->t( $this->texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $user->profile->getElement( $_ ) ) ) $rows[] = [ $cell1, [ 'data' => (string)$this->t( $this->texts->get( 'userprofile.data_policy.' . ( $user->profile->agreedLatestPrivacyProtection() ? 'accepted' : 'not_accepted' ) ) ) ] ];
                        
                        $_ = 'show_data_in_boards';
                        $cell1 = [ 'data' => (string)$this->t( $this->texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        $rows[] = [ $cell1, [ 'data' => (string)$this->t( $this->texts->get( 'global.bool_value.' . ( $user->profile->getElement( $_ ) ? 'yes' : 'no' ) ) ) ] ];
                        
                        $addAffil = $user->profile()->getAdditionalAffiliations();
                        if(count($addAffil)){
                                $cell1 = [ 'data' => (string)$this->t( 'Additional affiliations' ), 'width' => $_w, 'class' => 'font_bold' ];
                                
                                $affString = '';
                                foreach($addAffil as $_){
                                        $affString .= (!empty($affString) ? '<br>' : '').implode(', ', array_filter([
                                                        $_['department'],
                                                        $_['institute'],
                                                        $_['city'],
                                                        Publisso::tools()->getCountry($_['country']),
                                        ]));
                                }
                                
                                $rows[] = [ $cell1, [ 'data' => [
                                        '#markup' => $affString
                                ] ] ];
                        }
                        
                        $addAffiliations = [];
                        $urlAddAffiliations = Url::fromRoute('publisso_gold.userprofile.add_affiliations', [], ['attributes' => ['class' => 'btn btn-warning use-ajax', 'data-dialog-type' => 'modal', 'data-dialog-options' => '{"width":1200,"height":800}']]);
                        if($urlAddAffiliations->access()){
                                $addAffiliations = [
                                        '#type' => 'link',
                                        '#url' => $urlAddAffiliations,
                                        '#title' => (string)$this->t('Additional affiliations'),
                                        '#suffix' => '&nbsp;&nbsp;&nbsp;'
                                ];
                        }
                        
                        return [
                                'headline'   => [
                                        '#prefix' => '<h1>', '#suffix' => '</h1>', '#markup' => (string)$this->t($this->texts->get('userprofile.headline', 'fco'
                                        ), ['@salutation' => $this->tools->getSalutation($user->profile->getElement('salutation')), '@name' => $user->profile->getReadableFullName()]
                                        ),
                                ],
                                'picture' => [
                                        '#markup' => '<img src="' . Url::fromRoute('publisso_gold.getPicture', ['type' => 'up', 'id' => $user->getElement('id')])->toString() . '" class="rwUserpic">',
                                        '#prefix' => '<div class="rwUserpic">',
                                        '#suffix' => '</div>'
                                ],
                                'data' => [
                                        '#type' => 'table',
                                        '#rows' => $rows
                                ],
                                'actions' => [
                                        '#prefix' => '<hr>',
                                        '#suffix' => '<hr>',
                                        '#type' => 'container',
                                        'content' => [
                                                'edit' => [
                                                        '#type' => 'link',
                                                        '#url' => Url::fromRoute('publisso_gold.userprofile_edit'),
                                                        '#suffix' => '&nbsp;&nbsp;&nbsp;',
                                                        '#attributes' => [
                                                                'class' => [
                                                                        'btn',
                                                                        'btn-warning'
                                                                ]
                                                        ],
                                                        '#title' => (string)$this->t($this->texts->get('userprofile.btn.edit', 'fc'))
                                                ],
                                                'pw_change' => [
                                                        '#type' => 'link',
                                                        '#url' => Url::fromRoute('publisso_gold.userprofile.chpw'),
                                                        '#suffix' => '&nbsp;&nbsp;&nbsp;',
                                                        '#attributes' => [
                                                                'class' => [
                                                                        'btn',
                                                                        'btn-warning',
                                                                        'use-ajax'
                                                                ],
                                                                'data-dialog-type' => 'modal'
                                                        ],
                                                        '#title' => (string)$this->t($this->texts->get('userprofile.btn.pw_change', 'fc'))
                                                ],
                                                'add_affiliations' => $addAffiliations,
                                                'delete' => [
                                                        '#type' => 'link',
                                                        '#title' => (string)$this->t($this->texts->get('userprofile.btn.delete_account', 'fc')),
                                                        '#url' => Url::fromRoute('publisso_gold.userprofile.delete'),
                                                        '#attributes' => [
                                                                'class' => [
                                                                        'btn',
                                                                        'btn-danger',
                                                                        'use-ajax'
                                                                ],
                                                                'data-dialog-type' => 'modal'
                                                        ]
                                                ]
                                        ]
                                ],
                                'cache' => ['#max-age' => 0],
                        ];
                }
        
                /**
                 * @param $token
                 * @return \Drupal\Core\Ajax\AjaxResponse|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function userprofile_delete ($token ) {
                        
                        if ( $token ) {
                                
                                $qry = \Drupal::database()->select( 'rwPubgoldUserDeletePending', 't' )->fields( 't', [] )->condition( 'token', $token, '=' );
                                if ( !$qry->countQuery()->execute()->fetchField() ) {
                                        return $this->tools->accessDenied( (string)$this->t( $this->texts->get( 'userprofile.delete.confirm.wrong_token' )
                                        )
                                        );
                                }
                                
                                $uid = $qry->execute()->fetchAll()[ 0 ]->uid;
                                
                                $user = new \Drupal\publisso_gold\Controller\User( $uid );
                                $user->delete();
                                
                                \Drupal::service( 'publisso_gold.tools' )->destroyUserSession();
                                \Drupal::service( 'messenger' )->addMessage( (string)$this->t( $this->texts->get( 'userprofile.delete.msg.success' ) ) );
                                return $this->redirect( 'publisso_gold.login' );
                        }
                        
                        $response = new \Drupal\Core\Ajax\AjaxResponse();
                        
                        // Get the modal form using the form builder.
                        $modal_form = \Drupal::formBuilder()->getForm( 'Drupal\publisso_gold\Form\userprofileDelete' );
                        
                        // Add an AJAX command to open a modal dialog with the form as the content.
                        $response->addCommand( new \Drupal\Core\Ajax\OpenModalDialogCommand( (string)$this->t( $this->texts->get( 'userprofile.delete.headline', 'fc' ) ), $modal_form, [ 'width' => '800' ] ) );
                        
                        return $response;
                }
        
                /**
                 * @return \Drupal\Core\Ajax\AjaxResponse
                 */
                public function userprofile_chpw () {
                        
                        $response = new \Drupal\Core\Ajax\AjaxResponse();
                        
                        // Get the modal form using the form builder.
                        $modal_form = \Drupal::formBuilder()->getForm( 'Drupal\publisso_gold\Form\publisso_goldChangeUserPassword' );
                        
                        // Add an AJAX command to open a modal dialog with the form as the content.
                        $response->addCommand( new \Drupal\Core\Ajax\OpenModalDialogCommand( (string)t( 'Change password' ), $modal_form, [ 'width' => '800' ] ) );
                        
                        return $response;
                }
        
                /**
                 * @return array
                 */
                public function change_password () {
                        
                        return [ '#markup' => (string)t( 'You are prompted to change your password' ), '#prefix' => '<h3><span class="rwAlert">', '#suffix' => '</span></h3>', 'content' => \Drupal::formBuilder()->getForm( 'Drupal\publisso_gold\Form\publisso_goldChangeUserPassword' ) ];
                }
        
                /**
                 * @param $cp_id
                 * @return array
                 */
                public function bookSetErratum ($cp_id ) {
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookerrata' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $chapter = new \Drupal\publisso_gold\Controller\Bookchapter( $cp_id );
                        
                        if ( $chapter->getElement( 'next_version' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied( (string)$this->t( 'This chapter not up to date!' ) );
                        
                        $form = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\SetErratumBookChapter", [ 'wf_id' => $chapter->getElement( 'wfid' ), 'bk_id' => $chapter->getElement( 'bkid' ), 'cp_id' => $chapter->getElement( 'id' ) ] );
                        
                        return [ [ $form ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function dashboard () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        //echo '<pre>'.print_r($_SESSION, 1).'</pre>';
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'dashboard' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        unset( $_SESSION[ 'action' ] );
                        $render_dashboard = [ '#type' => 'fieldset', '#title' => (string)t( 'ToDo' ), 'todo' => [] ];

// running submissions
                        
                        $result = \Drupal::database()->select( 'rwPubgoldBookchapter_temp', 't' )->fields( 't', [ 'id' ] )->condition( 'uid', $_SESSION[ 'user' ][ 'id' ], '=' )->execute()->fetchAll();
                        
                        if ( count( $result ) ) {
                                
                                $fieldset = [ '#type' => 'fieldset', '#title' => t( 'My running submissions' ), '#collapsible' => true, '#collapsed' => true, 'content' => [] ];
                                
                                $vars = [ '::txt_created::' => t( 'Created' ) . ':', '::txt_modified::' => t( 'Modified' ) . ':' ];
                                
                                foreach ( $result as $res ) {
                                        
                                        $item = new \Drupal\publisso_gold\Controller\RunningSubmission( $res->id );
                                        $book = new \Drupal\publisso_gold\Controller\Book( $item->getElement( 'bk_id' ) );
                                        
                                        $element = [ '#type' => 'details', '#title' => $item->getDataElement( 'title' ) . ' ' . t( 'in' ) . ' ' . $book->getElement( 'title' ), 'content' => [ '#type' => 'markup' ] ];
                                        
                                        $link = Url::fromRoute( 'publisso_gold.book.addchapter', [ 'bk_id' => $book->getElement( 'id' ), 'continue' => $item->getElement( 'id' ) ] );
                                        $link = \Drupal::l( t( 'Continue editing' ), $link );
                                        
                                        $vars[ '::val_created::' ] = $item->getElement( 'created' );
                                        $vars[ '::val_modified::' ] = $item->getElement( 'modified' );
                                        $vars[ '::link::' ] = $link;
                                        
                                        $element[ 'content' ][ '#markup' ] = file_get_contents( $this->modpath . '/inc/publisso_gold_running_submission_dashbord.tmpl.inc.php' );
                                        $element[ 'content' ][ '#markup' ] = $this->renderVars( $element[ 'content' ][ '#markup' ], $vars );
                                        $fieldset[ 'content' ][] = $element;
                                }
                                
                                $render_dashboard_running_submissions[] = $fieldset;
                        }

// -- running submissions --

// assigned as EiC
                        
                        $sql = "
                        SELECT
                                `wf_id`
                        FROM
                                {rwPubgoldWorkflow}
                        WHERE
                                FIND_IN_SET(:uid, `wf_assigned_to_eic`)
                ";
                        
                        $result = \Drupal::database()->query( $sql, [ ':uid' => $_SESSION[ 'user' ][ 'id' ] ] )->fetchAll();
                        
                        if ( count( $result ) ) {
                                
                                $fieldset = [ '#type' => 'fieldset', '#collapsible' => true, '#collapsed' => true, '#title' => t( 'Submissions where I am EiC' ), 'content' => [] ];
                                
                                $vars = [ '::txt_status::' => t( 'State' ), '::txt_editors::' => t( 'Editors' ) ];
                                
                                foreach ( $result as $res ) {
                                        
                                        $item = new \Drupal\publisso_gold\Controller\Workflow( $res->wf_id );
                                        
                                        switch ( $item->getDataElement( 'type' ) ) {
                                                
                                                case 'bookchapter':
                                                        $medium = new \Drupal\publisso_gold\Controller\Book( $item->getDataElement( 'bk_id' ) );
                                                        break;
                                                
                                                case 'journalarticle':
                                                        $medium = new \Drupal\publisso_gold\Controller\Journal( $item->getDataElement( 'jrn_id' ) );
                                                        break;
                                                
                                                case 'conferencepaper':
                                                        $medium = new \Drupal\publisso_gold\Controller\Conference( $item->getDataElement( 'cf_id' ) );
                                                        break;
                                        }
                                        
                                        $element = [ '#type' => 'details', '#title' => (string)t( '@ID: @ITEM_TITLE in @MEDIUM_TITLE', [ '@ID' => $item->getElement( 'id' ), '@ITEM_TITLE' => $item->getDataElement( 'title' ), '@MEDIUM_TITLE' => $medium->getElement( 'title' ) ] ), 'content' => [ '#type' => 'markup' ] ];
                                        
                                        $vars[ '::val_status::' ] = $item->getDataElement( 'state' );
                                        $vars[ '::val_editors::' ] = '';
                                        
                                        foreach ( explode( ',', $item->getElement( 'assigned_to_editor' ) ) as $_ ) {
                                                
                                                $_user = new \Drupal\publisso_gold\Controller\User( $_ );
                                                $vars[ '::val_editors::' ] .= ( !empty( $vars[ '::val_editors::' ] ) ? '<br>' : '' ) . ( !empty( $_user->profile->getElement( 'graduation' ) ) ? $_user->profile->getElement( 'graduation' ) . ' ' : '' ) . $_user->profile->getElement( 'firstname' ) . ' ' . $_user->profile->getElement( 'lastname' ) . ( !empty( $_user->profile->getElement( 'graduation' ) ) ? ' ' . $_user->profile->getElement( 'graduation_suffix' ) : '' );
                                        }
                                        
                                        $element[ 'content' ][ '#markup' ] = file_get_contents( $this->modpath . '/inc/publisso_gold_eic_submission_dashbord.tmpl.inc.php' );
                                        $element[ 'content' ][ '#markup' ] = $this->renderVars( $element[ 'content' ][ '#markup' ], $vars );
                                        $fieldset[ 'content' ][] = $element;
                                }
                                
                                $render_dashboard_eic = $fieldset;
                        }

// -- assigned as EiC --

// assigned as Editor
                        
                        $sql = "
                        SELECT
                                `wf_id`
                        FROM
                                {rwPubgoldWorkflow}
                        WHERE
                                FIND_IN_SET(:uid, `wf_assigned_to_editor`)
                ";
                        
                        $result = \Drupal::database()->query( $sql, [ ':uid' => $_SESSION[ 'user' ][ 'id' ] ] )->fetchAll();
                        
                        if ( count( $result ) ) {
                                
                                $fieldset = [ '#type' => 'fieldset', '#collapsible' => true, '#collapsed' => true, '#title' => t( 'Submissions where I am Editor' ), 'content' => [] ];
                                
                                $vars = [ '::txt_status::' => t( 'State' ), '::txt_reviewers::' => t( 'Reviewers' ) ];
                                
                                foreach ( $result as $res ) {
                                        
                                        $item = new \Drupal\publisso_gold\Controller\Workflow( $res->wf_id );
                                        
                                        switch ( $item->getDataElement( 'type' ) ) {
                                                
                                                case 'bookchapter':
                                                        $medium = new \Drupal\publisso_gold\Controller\Book( $item->getDataElement( 'bk_id' ) );
                                                        break;
                                                
                                                case 'journalarticle':
                                                        $medium = new \Drupal\publisso_gold\Controller\Journal( $item->getDataElement( 'jrn_id' ) );
                                                        break;
                                                
                                                case 'conferencepaper':
                                                        $medium = new \Drupal\publisso_gold\Controller\Conference( $item->getDataElement( 'cf_id' ) );
                                                        break;
                                        }
                                        
                                        $element = [ '#type' => 'details', '#title' => (string)t( '@ID: @ITEM_TITLE in @MEDIUM_TITLE', [ '@ID' => $item->getElement( 'id' ), '@ITEM_TITLE' => $item->getDataElement( 'title' ), '@MEDIUM_TITLE' => $medium->getElement( 'title' ) ] ), 'content' => [ '#type' => 'markup' ] ];
                                        
                                        $vars[ '::val_status::' ] = $item->getDataElement( 'state' );
                                        $vars[ '::val_reviewers::' ] = '';
                                        
                                        foreach ( explode( ',', $item->getElement( 'assigned_to_reviewer' ) ) as $_ ) {
                                                
                                                $_user = new \Drupal\publisso_gold\Controller\User( $_ );
                                                
                                                //identify review-state by color
                                                $reviewStatus = 'pending';
                                                
                                                $_res = \Drupal::database()->select( 'rwPubgoldWorkflowCache', 't' )->fields( 't', [] )->condition( 'wc_uid', $_user->getElement( 'id' ), '=' )->condition( 'wc_wfid', $item->getElement( 'id' ), '=' )->countQuery()->execute()->fetchField();
                                                
                                                if ( $_res > 0 ) {
                                                        $reviewStatus = 'started';
                                                }
                                                else {
                                                        if ( !in_array( 'u:' . $_user->getElement( 'id' ), explode( ',', $item->getElement( 'assigned_to' ) ) ) ) {
                                                                $reviewStatus = 'finished';
                                                        }
                                                }
                                                
                                                $vars[ '::val_reviewers::' ] .= ( !empty( $vars[ '::val_reviewers::' ] ) ? '<br>' : '' ) . '<span class="reviewStatus' . ucfirst( $reviewStatus ) . '">' . ( !empty( $_user->profile->getElement( 'graduation' ) ) ? $_user->profile->getElement( 'graduation' ) . ' ' : '' ) . $_user->profile->getElement( 'firstname' ) . ' ' . $_user->profile->getElement( 'lastname' ) . ( !empty( $_user->profile->getElement( 'graduation' ) ) ? ' ' . $_user->profile->getElement( 'graduation_suffix' ) : '' ) . '</span>';
                                        }
                                        
                                        //if($medium->getControlElement('allow_invite_users'))
                                        //$vars['::lnk_invite_users::'] = \Drupal::l(t('Invite Users'), Url::fromRoute('publisso_gold.workflow.invite_user', ['wf_id' => $res->wf_id]));
                                        
                                        if ( $item->getDataElement( 'state' ) == 'in review' ) {
                                                
                                                if ( count( $item->readReviewers() ) ) $vars[ '::lnk_unassign_reviewers::' ] = \Drupal::l( t( 'Unassign Reviewers' ), Url::fromRoute( 'publisso_gold.workflow.unassign_reviewers', [ 'wf_id' => $res->wf_id ] ) );
                                                
                                                $vars[ '::lnk_assign_reviewers::' ] = \Drupal::l( t( 'Assign Reviewers' ), Url::fromRoute( 'publisso_gold.workflow.assign_reviewers', [ 'wf_id' => $res->wf_id ] ) );
                                        }
                                        
                                        $element[ 'content' ][ '#markup' ] = file_get_contents( $this->modpath . '/inc/publisso_gold_editor_submission_dashbord.tmpl.inc.php' );
                                        $element[ 'content' ][ '#markup' ] = $this->renderVars( $element[ 'content' ][ '#markup' ], $vars );
                                        $fieldset[ 'content' ][] = $element;
                                }
                                
                                $render_dashboard_editor = $fieldset;
                        }

// -- assigned as Editor --

// assigned as Reviewer
                        
                        $sql = "
                        SELECT
                                `wf_id`
                        FROM
                                {rwPubgoldWorkflow}
                        WHERE
                                FIND_IN_SET(:uid, `wf_assigned_to_reviewer`)
                ";
                        
                        $result = \Drupal::database()->query( $sql, [ ':uid' => $_SESSION[ 'user' ][ 'id' ] ] )->fetchAll();
                        
                        if ( count( $result ) ) {
                                
                                $fieldset = [ '#type' => 'fieldset', '#collapsible' => true, '#collapsed' => true, '#title' => t( 'Submissions where I am Reviewer' ), 'content' => [] ];
                                
                                $vars = [ '::txt_status::' => t( 'State' ), '::txt_reviewers::' => '' ];
                                
                                foreach ( $result as $res ) {
                                        
                                        $item = new \Drupal\publisso_gold\Controller\Workflow( $res->wf_id );
                                        
                                        switch ( $item->getDataElement( 'type' ) ) {
                                                
                                                case 'bookchapter':
                                                        $medium = new \Drupal\publisso_gold\Controller\Book( $item->getDataElement( 'bk_id' ) );
                                                        break;
                                                
                                                case 'journalarticle':
                                                        $medium = new \Drupal\publisso_gold\Controller\Journal( $item->getDataElement( 'jrn_id' ) );
                                                        break;
                                                
                                                case 'conferencepaper':
                                                        $medium = new \Drupal\publisso_gold\Controller\Conference( $item->getDataElement( 'cf_id' ) );
                                                        break;
                                        }
                                        
                                        $element = [ '#type' => 'details', '#title' => (string)t( '@ID: @ITEM_TITLE in @MEDIUM_TITLE', [ '@ID' => $item->getElement( 'id' ), '@ITEM_TITLE' => $item->getDataElement( 'title' ), '@MEDIUM_TITLE' => $medium->getElement( 'title' ) ] ), 'content' => [ '#type' => 'markup' ] ];
                                        
                                        $vars[ '::val_status::' ] = $item->getDataElement( 'state' );
                                        $vars[ '::val_reviewers::' ] = '';
                                        
                                        $element[ 'content' ][ '#markup' ] = file_get_contents( $this->modpath . '/inc/publisso_gold_editor_submission_dashbord.tmpl.inc.php' );
                                        $element[ 'content' ][ '#markup' ] = $this->renderVars( $element[ 'content' ][ '#markup' ], $vars );
                                        $fieldset[ 'content' ][] = $element;
                                }
                                
                                $render_dashboard_reviewer = $fieldset;
                        }

// -- assigned as Reviewer --

// published bookchapters
                        $PublishedChapters = getPublishedBookChapters_Workflow();
                        $render_dashboard_published_bookchapters = [ '#type' => 'details', '#title' => (string)$this->t( 'Published bookchapters' ) ];
                        
                        if ( !count( $PublishedChapters ) ) {
                                $render_dashboard_published_bookchapters = null;
                        }
                        else {
                                
                                $template = getTemplate( 'dashboard_published_bookchapters_item_content' );
                                
                                foreach ( $PublishedChapters as $_wfid ) {
                                        
                                        $tmpl = $template;
                                        $tvars = [];
                                        $_workflow = new \Drupal\publisso_gold\Controller\Workflow( $_wfid );
                                        $_book = new \Drupal\publisso_gold\Controller\Book( $_workflow->getDataElement( 'bk_id' ) );
                                        $_chapter = new \Drupal\publisso_gold\Controller\Bookchapter( getBookchapterIDFromWorkflow( $_workflow->getElement( 'id' ) ) );
                                        $_author = new \Drupal\publisso_gold\Controller\User( $_workflow->getElement( 'created_by_uid' ) );
                                        $_urlErratum = Url::fromROute( 'publisso_gold.book.chapter.seterratum', [ 'cp_id' => $_chapter->getElement( 'id' ) ] );
                                        
                                        foreach ( $_book->getKeys() as $key ) {
                                                $tvars[ "::book_$key::" ] = $_book->getElement( $key );
                                        }
                                        
                                        foreach ( $_chapter->getKeys() as $key ) {
                                                $tvars[ "::chapter_$key::" ] = $_chapter->getElement( $key );
                                        }
                                        
                                        foreach ( $_workflow->getKeys() as $key ) {
                                                $tvars[ "::workflow_$key::" ] = $_workflow->getElement( $key );
                                        }
                                        
                                        foreach ( $_workflow->getDataKeys() as $key ) {
                                                $tvars[ "::workflowdata_$key::" ] = $_workflow->getDataElement( $key );
                                        }
                                        
                                        $tvars[ '::strAuthorDesc::' ] = (string)$this->t( 'Submitted by' );
                                        $tvars[ '::strCreatedDesc::' ] = (string)$this->t( 'Created' );
                                        $tvars[ '::strDOIDesc::' ] = (string)$this->t( 'DOI' );
                                        
                                        if ( $_chapter->getElement( 'pending_erratum' ) != 1 ) {
                                                $tvars[ '::strHelpSetErratum::' ] = (string)$this->t( 'Set Erratum' );
                                                $tvars[ '::strLnkSetErratum::' ] = (string)$this->t( 'Set Erratum' );
                                        }
                                        else {
                                                $tvars[ '::strHelpSetErratum::' ] = (string)$this->t( '' );
                                                $tvars[ '::strLnkSetErratum::' ] = (string)$this->t( '' );
                                        }
                                        
                                        $tvars[ '::urlLnkSetErratum::' ] = $_urlErratum->toString();
                                        $tvars[ '::strAuthor::' ] = implode( ', ', array_filter( [ $_author->profile->getElement( 'lastname' ), $_author->profile->getElement( 'firstname' ) ] ) );
                                        
                                        $markup = $this->renderVars( $template, $tvars );
                                        
                                        $_item = [ '#type' => 'details', '#title' => implode( ' ', [ $_workflow->getDataElement( 'title' ), (string)$this->t( 'in' ), $_book->getElement( 'title' ) ] ), [ '#markup' => $markup ] ];
                                        
                                        array_push( $render_dashboard_published_bookchapters, $_item );
                                }
                        }

// --published bookchapters --
                        
                        
                        $dashboard = getDashboard( $this->database );
                        
                        $entities = [];
                        
                        $db_message = [];
                        
                        if ( count( $dashboard ) ) {
                                
                                foreach ( $dashboard as $entity ) {
                                        
                                        $data = json_decode( base64_decode( $entity->wf_data ) );
                                        
                                        $data->wf_id = $entity->wf_id;
                                        $entities[ $data->type ][] = $data;
                                }
                        }
                        else {
                                $db_message = [ '#type' => 'markup', '#markup' => (string)t( 'No active items in your dashboard' ), '#prefix' => '<br><div class="rwStatus">', '#suffix' => '</div>' ];
                        }
                        
                        $c = 0;
                        
                        foreach ( $entities as $entity ) {
                                
                                foreach ( $entity as $data ) {
                                        
                                        if ( $data->state != 'published' ) {
                                                
                                                if ( $data->type == 'bookchapter' ) {
                                                        $medium = getBook( \Drupal::database(), $data->bk_id );
                                                        
                                                        foreach ( $medium as $k => $v ) {
                                                                $medium[ preg_replace( '/^bk_/', '', $k ) ] = $v;
                                                        }
                                                }
                                                elseif ( $data->type == 'journalarticle' ) {
                                                        $medium = getJournal( \Drupal::database(), $data->jrn_id );
                                                        
                                                        foreach ( $medium as $k => $v ) {
                                                                $medium[ preg_replace( '/^jrn_/', '', $k ) ] = $v;
                                                        }
                                                }
                                                elseif ( $data->type == 'conferencepaper' ) {
                                                        $medium = getConference( \Drupal::database(), $data->cf_id );
                                                        
                                                        foreach ( $medium as $k => $v ) {
                                                                $medium[ preg_replace( '/^cf_/', '', $k ) ] = $v;
                                                        }
                                                }
                                                
                                                $render_dashboard[ 'todo' ][] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldDashboardCheckoutWorkflow", [ 'data' => $data, 'medium' => $medium, 'suffix' => $c ]
                                                )
                                                ;
                                                $c++;
                                        }
                                }
                        }
                        
                        $my_dashboard = [];
                        $c = 0;
                        
                        foreach ( getMyWorkflowItems() as $myItem ) {
                                
                                $_data = json_decode( base64_decode( $myItem->wf_data ) );
                                
                                switch ( $_data->type ) {
                                        
                                        case 'bookchapter':
                                                $book = getBook( \Drupal::database(), $_data->bk_id );
                                                $parent_title = $book[ 'bk_title' ];
                                                break;
                                        
                                        case 'journalarticle':
                                                $journal = getJournal( \Drupal::database(), $_data->jrn_id );
                                                $parent_title = $journal[ 'jrn_title' ];
                                                break;
                                        
                                        case 'conferencepaper':
                                                $conference = getBook( \Drupal::database(), $_data->cf_id );
                                                $parent_title = $conference[ 'cf_title' ];
                                                break;
                                }
                                
                                if ( $c == 0 ) {
                                        $my_dashboard = [ '#type' => 'fieldset', '#collapsible' => true, '#collapsed' => true, '#title' => (string)t( 'My submissions' ), 'content' => [] ];
                                }
                                
                                $element = [
                                        '#type' => 'details', '#title' => (string)t( '@ID: @ITEM_TITLE in @MEDIUM_TITLE', [ '@ID' => $myItem->wf_id, '@ITEM_TITLE' => $_data->title, '@MEDIUM_TITLE' => $parent_title ] ), 'content' => [
                                                'title' => [ '#type' => 'markup', '#markup' => $_data->title, '#prefix' => '<h4>', '#suffix' => '</h4>' ],
                                                
                                                'status' => [ '#type' => 'markup', '#markup' => $_data->state, '#prefix' => '<h6>', '#suffix' => '</h6>' ],
                                        ],
                                ];
                                
                                $my_dashboard[ 'content' ][] = $element;
                                
                                $cc = 0;
                                /*
            foreach(getMyWorkflowComments($myItem->wf_id, $_SESSION['user']['id']) as $_comment){
                
                if($cc == 0){
                    
                    $my_dashboard['content'][$cc]['comments'][] = [
                      
                        '#type' => 'fieldset',
                        '#title' => (string)t('My Workflow Comments'),
                        'content' => []
                    ];
                }
                
                $comment = json_decode(base64_decode($comment->cfa_comment));
                
                $element = [
                    '#type' => 'details',
                    '#title' => (string)t('Comment').' #'.$cc + 1,
                    'content' => [
                        'comment' => [
                            '#type' => 'markup',
                            '#markup' => $comment->value,
                            '#prefix' => '<h4>',
                            '#suffix' => '</h4>'
                        ]
                    ]
                ];
                
                $my_dashboard['content'][$cc]['content'][] = $element;
                $cc++;
            }
            */
                                $c++;
                        }
                        
                        $this->renderVars();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $db_message, $render_dashboard_published_bookchapters, $render_dashboard, $render_dashboard_eic, $render_dashboard_editor, $render_dashboard_reviewer, $render_dashboard_running_submissions, $my_dashboard ], '#attached' => [ 'library' => [ 'publisso_gold/default', 'publisso_gold/devel' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $wf_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function review_item ($wf_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'dashboard' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        //get Item
                        $workflow = getDashboard( \Drupal::database(), $wf_id );
                        $_SESSION[ 'breadcrumb_info' ][ 'wf_id' ] = $wf_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'wf_id' => $wf_id ] );
                        
                        //if none response given
                        if ( !count( $workflow ) ) {
                                
                                $this->tmpl = '<span class="rwAlert">' . t( "Review-item not found!" ) . '</span>';
                                return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        }
                        
                        //render workflow and data
                        $workflow = $workflow[ 0 ];
                        $data = json_decode( base64_decode( $workflow->wf_data ) );
                        $element_tmpl = '';
                        $element_vars = [];
                        $render_content = [];
                        
                        switch ( $data->type ) {
                                
                                case 'bookchapter':
                                        
                                        //get book
                                        $book = getBook( \Drupal::database(), $data->bk_id );
                                        $_SESSION[ 'action' ][ 'viewbook' ] = $book[ 'bk_id' ];
                                        #$render_content[] = \Drupal::formBuilder()->getForm("Drupal\\".$this->modname."\Form\publisso_goldReviewBookchapter", ['workflow' => $workflow, 'book' => $book]);
                                        $render_content[] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldReviewPage", [ 'workflow' => $workflow, 'book' => $book ] );
                                        break;
                                
                                case 'journalarticle':
                                        //get journal
                                        $journal = getJournal( \Drupal::database(), $data->jrn_id );
                                        $_SESSION[ 'action' ][ 'viewjournal' ] = $journal[ 'jrn_id' ];
                                        #$render_content[] = \Drupal::formBuilder()->getForm("Drupal\\".$this->modname."\Form\publisso_goldReviewJournalarticle", ['workflow' => $workflow, 'journal' => $journal]);
                                        $render_content[] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldReviewPage", [ 'workflow' => $workflow, 'journal' => $journal ] );
                                        break;
                                
                                case 'conferencepaper':
                                        //get conference
                                        $conference = getConference( \Drupal::database(), $data->cf_id );
                                        $_SESSION[ 'action' ][ 'viewconference' ] = $conference[ 'cf_id' ];
                                        #$render_content[] = \Drupal::formBuilder()->getForm("Drupal\\".$this->modname."\Form\publisso_goldReviewConferencepaper", ['workflow' => $workflow, 'conference' => $conference]);
                                        $render_content[] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldReviewPage", [ 'workflow' => $workflow, 'conference' => $conference ] );
                                        break;
                        }
                        
                        //merge content
                        $this->tmpl_vars = array_merge( $this->tmpl_vars, $element_vars );
                        $this->tmpl .= $element_tmpl;
                        $this->renderVars();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $render_content ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $wf_id
                 * @return \Drupal\Core\Ajax\AjaxResponse
                 */
                public function wflockinfo_override ($wf_id ) {
                        
                        $response = new \Drupal\Core\Ajax\AjaxResponse();
                        
                        // Get the modal form using the form builder.
                        $modal_form = \Drupal::formBuilder()->getForm( 'Drupal\publisso_gold\Form\workflowLockOverride', [ 'wf_id' => $wf_id ] );
                        
                        // Add an AJAX command to open a modal dialog with the form as the content.
                        $response->addCommand( new \Drupal\Core\Ajax\OpenModalDialogCommand( (string)t( 'Unlock workflow-item' ), $modal_form, [ 'width' => '800' ] ) );
                        
                        return $response;
                }
        
                /**
                 * @param $wf_id
                 * @return array
                 */
                public function workflow_mailhistory ($wf_id ) {
                        
                        $session = \Drupal::service( 'session' );
                        $user = $session->get( 'user' );
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'wfmailinforead', $user[ 'weight' ] ) ) {
                        #        return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        #}
                        
                        $result = \Drupal::database()->select( 'rwPubgoldWorkflowMailHistory', 't' )->fields( 't', [] )->condition( 'wf_id', $wf_id, '=' )->isNotNull( 'mail_sent' )->orderBy( 'mail_sent', 'DESC' )->execute()->fetchAll();
                        
                        $ret = [
                                '#type'    => 'table', '#header' => [ 'created' => [ 'data' => (string)t( 'Created' ), 'style' => [ 'style' => 'width: 10%;' ] ], 'created_by' => [ 'data' => (string)t( 'Created by' ), 'style' => [ 'style' => 'width: 10%;' ] ], 'from' => [ 'data' => (string)t( 'From' ), 'style' => [ 'style' => 'width: 10%;' ] ], 'recipient' => [ 'data' => (string)t( 'Recipient' ), 'style' => [ 'style' => 'width: 10%;' ] ], 'cc' => [ 'data' => (string)t( 'CC' ), 'style' => [ 'style' => 'width: 10%;' ] ], 'bcc' => [ 'data' => (string)t( 'BCC' ), 'style' => [ 'style' => 'width: 10%;' ] ], 'text' => [ 'data' => (string)t( 'Mailtext' ), 'style' => [ 'style' => 'width: 20%;' ] ], 'attachments' => [ 'data' => (string)t( 'Attachments' ), 'style' => [ 'style' => 'width: 10%;' ] ], 'sent' => [ 'data' => (string)t( 'Sent' ), 'style' => [ 'style' => 'width: 10%;' ] ], ], '#attributes' => [//'style' => 'font-size: 8pt;'
                                ], '#rows' => [], '#empty' => (string)t( 'No Mails sent for this workflow-item...' ), '#tabledrag' => [ [ 'action' => 'order', 'relationship' => 'sibling', 'group' => 'mytable-order-weight', ], ],
                        ];
                        
                        $weight = 0;
                        foreach ( $result as $_ ) {
                                
                                $ret[ '#rows' ][ $_->id ][ 'created' ] = $_->init_timestamp;
                                $user = new User( $_->init_uid );
                                $ret[ '#rows' ][ $_->id ][ 'created_by' ] = $user->profile->getReadableName();
                                $ret[ '#rows' ][ $_->id ][ 'from' ] = chunk_split( $_->mail_from, 20, "\n" );
                                $ret[ '#rows' ][ $_->id ][ 'to' ] = $_->mail_to;
                                $ret[ '#rows' ][ $_->id ][ 'cc' ] = chunk_split( implode( ', ', unserialize( $_->mail_cc ) ), 20, "\n" );
                                $ret[ '#rows' ][ $_->id ][ 'bcc' ] = chunk_split( implode( ', ', unserialize( $_->mail_bcc ) ), 20, "\n" );
                                $ret[ '#rows' ][ $_->id ][ 'text' ] = $_->mail_text;
                                $ret[ '#rows' ][ $_->id ][ 'attachments' ] = implode( ', ', unserialize( $_->mail_attachment ) );
                                $ret[ '#rows' ][ $_->id ][ 'sent' ] = $_->mail_sent;
                        }
                        
                        return $ret;
                }
        
                /**
                 * @param $wf_id
                 * @return array
                 */
                public function WorkflowItem($wf_id){
                        
                        $qry = \Drupal::database()->select('rwPubgoldWorkflow', 'w')->fields('w', [])->condition('wf_id', $wf_id, '=');
                        $wf = $qry->execute()->fetchAssoc();
        
                        $assigned_uids = [];
                        foreach(array_filter(array_map("trim", explode(',', $wf['wf_assigned_to']))) as $_){
                                if(preg_match('/^u:(\d+)$/', $_, $matches)){
                                        $assigned_uids[] = $matches[1];
                                }
                        }
        
                        $locked = !!$wf['wf_locked'];
        
        
                        //prüfen der Zugriffsrechte
        
                        //generell darf niemand rein
                        $access = false;
        
                        //ein zugeordneter User darf generell rein
                        $access = $this->session->get('user')['id'] == in_array($this->session->get('user')['id'], $assigned_uids);
                        
                        //für administrative Rollen (PUBLISSO, Administrator), wenn nicht gesperrt
                        if(!$locked && $this->session->get('user')['weight'] >= 70) $access = true;
        
                        //wenn geperrt, muss die Sperre durch mich sein
                        if($locked) $access = $wf['wf_locked_by_uid'] == $this->session->get('user')['id'];
        
                        //wenn das Item in Review ist, muss es auch mir zugeordnet sein
                        if($wf['wf_state'] == 'in review') {
                                $access = in_array( $this->session->get( 'user' )[ 'id' ], $assigned_uids );
                        }
        
                        if(!$access){
                                return $this->tools->accessDenied((string)$this->t('You don\'t have permissions to access this item!'));
                        }
                        
                        if(null === ($workflow = WorkflowManager::getItem($wf_id))){
                                return $this->tools->accessDenied((string)$this->t('Can\'t find item #@nr', ['@nr' => $wf_id]));
                        }
                        
                        if(!$workflow->getLock()){
                                return $this->tools->accessDenied((string)$this->t('Can\'t getLock for item #@nr', ['@nr' => $wf_id]));
                        }
                        
                        if($workflow->getElement('state') !== 'running submission') {
                                
                                $elements = [
                                        \Drupal::formBuilder()->getForm( "Drupal\\publisso_gold\\Form\\Workflow\\Checkin", [ 'wf_id' => $workflow->getElement( 'id' ) ] )
                                ];
                        }
                        
                        foreach($workflow->getSchemaBoxes() as $box){
                                $args = ['hideonempty' => $box['box_hideonempty'], 'follower_overwrite' => $box['box_follower_overwrite']];
                                $elements[] = $workflow->renderSchemaBox($box, $args);
                        }

                        return $elements ?? [];
                }
        
                /**
                 * @param $wf_id
                 * @return \Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function deleteWorkflowRunningSubmission($wf_id){
                        
                        \Drupal::database()->delete('rwPubgoldWorkflow')->condition('wf_state', 'running submission', '=')->condition('wf_id', $wf_id, '=')->execute();
                        \Drupal::service('messenger')->addMessage((string)$this->t('Item successfully deleted'));
                        return $this->redirect('publisso_gold.dashboard', ['action' => 'myrunnings'], [], 301);
                }
        
                /**
                 * @param null $wf_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function workflow_item ($wf_id = null ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'dashboard' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        unset( $_SESSION[ 'action' ] );

                        if(!\Drupal::database()->select('rwPubgoldWorkflow', 't')->fields('t', [])->condition
                        ('wf_id', $wf_id, '=')->countQuery()->execute()->fetchField()) throw new
                        NotFoundHttpException();

                        $workflow = getDashboard( \Drupal::database(), $wf_id );
                        $new_workflow = new Workflow( $wf_id );
                        
                        $this->tmpl .= '<h1>' . $new_workflow->getDataElement( 'title' ) . '</h1>';
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'wf_id' ] = $wf_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'wf_id' => $wf_id ] );
                        
                        if ( count( $workflow ) ) {
                                
                                $wf_render = [];
                                $workflow = $workflow[ 0 ];
                                
                                $workflow_data = json_decode( base64_decode( $workflow->wf_data ) );
                                
                                $form_checkin = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldDashboardCheckinWorkflow", [ 'wf_id' => $wf_id ] );
                                
                                $infoboxes = [ 'meta' => -12, 'files' => -11, 'abstract' => -10, 'origtext' => -9, 'references' => -8, 'erratum' => -7, 'recommendations' => -6, 'comments' => -5, 'commentsforauthor' => -4 , 'commentsforauthor_anon' => -3, 'reviewsheets' => -2];
                                
                                foreach ( $infoboxes as $box => $weight ) {
                                        
                                        $wf_render[ $box ] = $new_workflow->getRenderableTemplate( $box );
                                        $wf_render[ $box ][ '#weight' ] = $weight;
                                }
                                
                                //role- and statedepending forms and actions
                                $forms = [];
                                
                                switch ( $new_workflow->getDataElement( 'state' ) ) {
                                        
                                        case 'submission finished':
                                                unset($wf_render['commentsforauthor_anon']);
                                                $forms = [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldWorkflowSetEic", [ 'wf_id' => $wf_id ] ) ];
                                                break;
                                        
                                        case 'assigned to eic':
                                                unset($wf_render['commentsforauthor_anon']);
                                                unset($wf_render['reviewsheets']);
                                                $forms = [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldWorkflowSetEditors", [ 'wf_id' => $wf_id ] ) ];
                                                break;
                                        
                                        case 'assigned to editor':
                                                
                                                unset($wf_render['commentsforauthor_anon']);
                                                $forms = [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldWorkflowSetReviewers", [ 'wf_id' => $wf_id ] ) ];
                                                break;
                                        
                                        case 'in revision':
                                                unset($wf_render['reviewsheets']);
                                                unset($wf_render['commentsforauthor']);
                                                if ( $workflow_data->type == 'bookchapter' ) {
                                                        $book = getBook( \Drupal::database(), $workflow_data->bk_id );
                                                        return $this->redirect( 'publisso_gold.book.addchapter', [ 'bk_id' => $book[ 'bk_id' ], 'wf_id' => $workflow->wf_id ] );
                                                }
                                                elseif ( $workflow_data->type == 'journalarticle' ) {
                                                        $journal = getJournal( \Drupal::database(), $workflow_data->jrn_id );
                                                        return $this->redirect( 'publisso_gold.journal_addarticle', [ 'jrn_id' => $journal[ 'jrn_id' ], 'wf_id' => $workflow->wf_id ] );
                                                }
                                                elseif ( $workflow_data->type == 'conferencepaper' ) {
                                                        $conference = getConference( \Drupal::database(), $workflow_data->cf_id );
                                                        return $this->redirect( 'publisso_gold.conference_addarticle', [ 'cf_id' => $conference[ 'cf_id' ], 'wf_id' => $workflow->wf_id ] );
                                                }
        
                                                unset($wf_render['comments']);
                                                unset($wf_render['recommendations']);
                                                
                                                break;
                                        
                                        case 'revision finished':
        
                                                unset($wf_render['commentsforauthor_anon']);
                                                $forms = [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldWorkflowEditorActionAfterRevision", [ 'wf_id' => $wf_id ] ) ];
                                                break;
                                        
                                        case 'review finished':
        
                                                unset($wf_render['commentsforauthor_anon']);
                                                //get workflow history
                                                
                                                $wf_history = getWorkflowHistory( \Drupal::database(), $wf_id );
                                                
                                                $wf_render[ 'history' ] = [ '#type' => 'details', '#open' => false, '#title' => t( 'History' ), 'content' => [] ];
                                                
                                                foreach ( $wf_history as $k => $hist ) {
                                                        
                                                        $wf_history[ $k ]->wfh_data = json_decode( base64_decode( $wf_history[ $k ]->wfh_data ) );
                                                        $abstract = json_decode( base64_decode( $hist->wfh_data->abstract ) );
                                                        
                                                        if ( isset( $hist->wfh_data->chapter_text ) ) $_text = base64_decode( $hist->wfh_data->chapter_text );
                                                        
                                                        if ( isset( $hist->wfh_data->article_text ) ) $_text = base64_decode( $hist->wfh_data->article_text );
                                                        
                                                        if ( isset( $hist->wfh_data->paper_text ) ) $_text = base64_decode( $hist->wfh_data->paper_text );
                                                        
                                                        $content = [ '#type' => 'details', '#open' => false, '#title' => t( 'History-Item: @item_id', [ '@item_id' => $hist->wfh_id ] ), 'content' => [ 'orig_text' => [ '#type' => 'inline_template', '#template' => $_text ? $_text : '' ] ] ];
                                                        
                                                        array_push( $wf_render[ 'history' ][ 'content' ], $content );
                                                }
                                                
                                                if ( isset( $workflow_data->comment ) ) {
                                                        
                                                        $workflow_data->comment = json_decode( base64_decode( $workflow_data->comment ) );
                                                        
                                                        foreach ( $workflow_data->comment as $_ ) {
                                                                
                                                                $_->comment = base64_decode( $_->comment );
                                                                
                                                                array_push( $wf_render[ 'comments' ][ 'content' ], [
                                                                        
                                                                                                                         '#type' => 'details', '#open' => false, '#title' => t( 'Comment @time', [ '@time' => $_->created ] ), 'content' => [ '#type' => 'markup', '#markup' => $_->comment ],
                                                                                                                 ]
                                                                );
                                                        }
                                                }
                                                
                                                $wf_render[ 'review_page' ] = $rp_form ?? [];
                                                //$wf_render['review_sheets'] = $new_workflow->getRenderableTemplate('reviewsheets', ['state' => 'all']);
                                                $wf_render[ 'new_comment' ] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldWorkflowSetComment", [ 'wf_id' => $wf_id ] );
                                                $wf_render[ 'new_review' ] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldWorkflowSetReviewers", [ 'wf_id' => $wf_id ] );
                                                $wf_render[ 'forward_eic' ] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldWorkflowForwardEic", [ 'wf_id' => $wf_id ] );
                                                
                                                break;
                                        
                                        case 'rejected':
                                                unset($wf_render['reviewsheets']);
                                                $wf_render[ 'reject_msg' ] = [ '#type' => 'markup', '#markup' => (string)t( 'Your publication has been rejected!' ) ];
                                                
                                                break;
                                        
                                        case 'accepted':
                                                
                                                $wf_render[ 'review_page' ] = $rp_form ?? [];
                                                
                                                unset($wf_render['commentsforauthor_anon']);
                                                switch ( $workflow_data->type ) {
                                                        
                                                        case 'bookchapter':
                                                                
                                                                //$wf_render['form_chapter'][] = \Drupal::formBuilder()->getForm("Drupal\\".$this->modname."\Form\publisso_goldBookmanagementFormalAdaptBookChapter", ['wf_id' => $wf_id, 'bk_id' => $workflow_data->bk_id]);
                                                                $wf_render[ 'form_chapter' ][] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\FormalAdaptBookChapter", [ 'wf_id' => $wf_id, 'bk_id' => $workflow_data->bk_id ] );
                                                                break;
                                                        
                                                        case 'journalarticle':
                                                                
                                                                //$wf_render['form_chapter'] = \Drupal::formBuilder()->getForm("Drupal\\".$this->modname."\Form\publisso_goldJournalmanagementFormalAdaptJournalArticle", ['wf_id' => $wf_id, 'jrn_id' => $workflow_data->jrn_id]);
                                                                $wf_render[ 'form_chapter' ][] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\FormalAdaptJournalArticle", [ 'wf_id' => $wf_id, 'jrn_id' => $workflow_data->jrn_id ] );
                                                                break;
                                                        
                                                        case 'conferencepaper':
                                                                
                                                                //$wf_render['form_chapter'] = \Drupal::formBuilder()->getForm("Drupal\\".$this->modname."\Form\publisso_goldConferencemanagementFormalAdaptConferencePaper", ['wf_id' => $wf_id, 'cf_id' => $workflow_data->cf_id]);
                                                                $wf_render[ 'form_chapter' ][] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\FormalAdaptJournalArticle", [ 'wf_id' => $wf_id, 'jrn_id' => $workflow_data->jrn_id ] );
                                                                break;
                                                }
                                                
                                                break;
                                        
                                        case 'cleared for publication':
        
                                                unset($wf_render['commentsforauthor_anon']);
                                                $wf_render[ 'review_page' ] = $rp_form ?? [];
                                                
                                                $wf_render[ 'editor_change_request' ] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldWorkflowEicClearedForPublication", [ 'wf_id' => $wf_id ] );
                                                break;
                                        
                                        case 'editor change request':
        
                                                unset($wf_render['commentsforauthor_anon']);
                                                $wf_render[ 'review_page' ] = $rp_form ?? [];
                                                
                                                switch ( $workflow_data->type ) {
                                                        
                                                        case 'bookchapter':
                                                                
                                                                if ( $new_workflow->getElement( 'type' ) == 'bcs' ) $formname = 'FormalAdaptBookChapter';
                                                                elseif ( $new_workflow->getElement( 'type' ) == 'bce' ) $formname = 'FormalAdaptBookChapterErratum';
                                                                
                                                                $wf_render[ 'form_chapter' ][] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\\$formname", [ 'wf_id' => $wf_id, 'bk_id' => $workflow_data->bk_id ] );
                                                                break;
                                                        
                                                        case 'journalarticle':
                                                                
                                                                if ( $new_workflow->getElement( 'type' ) == 'jas' ) $formname = 'FormalAdaptJournalArticle';
                                                                elseif ( $new_workflow->getElement( 'type' ) == 'jae' ) $formname = 'FormalAdaptJournalArticleErratum';
                                                                
                                                                $wf_render[ 'form_chapter' ][] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\\$formname", [ 'wf_id' => $wf_id, 'jrn_id' => $workflow_data->jrn_id ] );
                                                                break;
                                                        
                                                        case 'conferencepaper':
                                                                $wf_render[ 'editor_change_request' ] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldWorkflowConferenceEditorChangeRequest", [ 'wf_id' => $wf_id ] );
                                                                break;
                                                }
                                                break;
                                        
                                        case 'decision eic':
                                                
                                                $wf_render[ 'review_page' ] = $rp_form ?? [];
                                                
                                                $wf_render[ 'decision_eic' ] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldWorkflowDecisionEic", [ 'wf_id' => $wf_id ] );
                                                unset($wf_render['commentsforauthor_anon']);
                                                break;
                                        
                                        case 'editor clearing request':
                                                
                                                $wf_render[ 'review_page' ] = $rp_form ?? [];
                                                
                                                $wf_render[ 'editor_clearing_request' ] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldWorkflowEditorClearingRequest", [ 'wf_id' => $wf_id ] );
                                                unset($wf_render['commentsforauthor_anon']);
                                                break;
                                        
                                        case 'author clearing request':
                                                $wf_render[ 'review_page' ] = $rp_form ?? [];
                                                unset($wf_render['reviewsheets']);
                                                unset($wf_render['comments']);
                                                unset($wf_render['recommendations']);
                                                unset($wf_render['commentsforauthor']);
                                                $wf_render[ 'author_clearing_request' ] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldWorkflowAuthorActionAfterAuthorClearingRequest", [ 'wf_id' => $wf_id ] );
                                                break;
                                        
                                        case 'author change request':
        
                                                unset($wf_render['commentsforauthor_anon']);
                                                $wf_render[ 'review_page' ] = $rp_form ?? [];
                                                
                                                switch ( $workflow_data->type ) {
                                                        
                                                        case 'bookchapter':
                                                                
                                                                if ( $new_workflow->getElement( 'type' ) == 'bcs' ) $formname = 'FormalAdaptBookChapter';
                                                                elseif ( $new_workflow->getElement( 'type' ) == 'bce' ) $formname = 'FormalAdaptBookChapterErratum';
                                                                
                                                                $wf_render[ 'form_chapter' ][] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\\$formname", [ 'wf_id' => $wf_id, 'bk_id' => $workflow_data->bk_id ] );
                                                                break;
                                                        
                                                        case 'journalarticle':
                                                                if ( $new_workflow->getElement( 'type' ) == 'jas' ) $formname = 'FormalAdaptJournalArticle';
                                                                elseif ( $new_workflow->getElement( 'type' ) == 'jae' ) $formname = 'FormalAdaptJournalArticleErratum';
                                                                
                                                                $wf_render[ 'form_chapter' ][] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\\$formname", [ 'wf_id' => $wf_id, 'jrn_id' => $workflow_data->jrn_id ] );
                                                                break;
                                                        
                                                        case 'conferencepaper':
                                                                $wf_render[ 'form_paper' ] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldConferencemanagementFormalAdaptConferencePaper", [ 'wf_id' => $wf_id, 'cf_id' => $workflow_data->cf_id ] );
                                                                break;
                                                }
                                                
                                                break;
                                        
                                        case 'ready for publication':
                                                unset($wf_render['commentsforauthor_anon']);
                                                $wf_render[ 'review_page' ] = $rp_form ?? [];
                                                
                                                switch ( $workflow_data->type ) {
                                                        
                                                        case 'bookchapter':
                                                                
                                                                $wf_render[ 'form_publish' ] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldBookmanagementPublishBookChapter", [ 'wf_id' => $wf_id, 'bk_id' => $workflow_data->bk_id ] );
                                                                break;
                                                        
                                                        case 'journalarticle':
                                                                
                                                                $wf_render[ 'form_publish' ] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldJournalmanagementPublishJournalArticle", [ 'wf_id' => $wf_id, 'jrn_id' => $workflow_data->jrn_id ] );
                                                                break;
                                                        
                                                        case 'conferencepaper':
                                                                
                                                                $wf_render[ 'form_publish' ] = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldConferencemanagementPublishConferencePaper", [ 'wf_id' => $wf_id, 'cf_id' => $workflow_data->cf_id ] );
                                                                break;
                                                }
                                                
                                                break;
                                }
                        }
                        $this->tmpl .= '';
                        $this->renderVars();
                        
                        return [ '#type' => 'inline_template', '#template' => $this->tmpl, 'content' => [ $form_checkin, $wf_render, $forms ], '#attached' => [ 'library' => [ 'publisso_gold/default', 'core/drupal.dialog.ajax' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function bookmanagement () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $user = \Drupal::service( 'session' )->get( 'user' );
                        
                        if ( is_array( $user ) ) {
                                $user_has_rights = $user[ 'isEditor' ] | $user[ 'isEiC' ] | $user[ 'isEO' ];
                        }
                        
                        $urlAddBook = Url::fromRoute('publisso_gold.bookmanagement_add', [], ['attributes' => ['class' => ['btn', 'btn-warning']]]);
                        
                        #if ( \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) || \Drupal::service( 'session' )->get( 'user' )[ 'isEO' ] ) {
                        #        $this->tmpl .= file_get_contents( $this->modpath . '/inc/publisso_gold_addbook_lnk.tmpl.inc.php' );
                        #        $this->tmpl_vars[ '::lnk_addbook::' ] = Link::fromTextAndUrl((string)$this->t('Add Book'), $urlAddBook)->toString();
                        #}
                        
                        $form_filter = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldBookmanagementFilter" );
                        
                        $this->renderVars();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form_filter ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param null $cp_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function bookmanagement_readchapter ($cp_id = null ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'cp_id' ] = $cp_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'cp_id' => $cp_id ] );
                        
                        $this->tmpl = file_get_contents( $this->modpath . '/inc/publisso_gold_bookmanagement_chapter.tmpl.inc.php' );
                        
                        if ( $cp_id ) {
                                
                                $chapter = \Drupal::database()->select( 'rwPubgoldBookChapters', 'bc' )->fields( 'bc', [ 'cp_bkid', 'cp_title', 'cp_chapter_text' ] )->condition( 'cp_id', $cp_id, '=' )->execute()->fetchAssoc();
                                
                                $chapter[ 'cp_chapter_text' ] = json_decode( $chapter[ 'cp_chapter_text' ] );
                                $chapter[ 'cp_chapter_text' ] = $chapter[ 'cp_chapter_text' ]->value;
                                
                                $vars = [ '::txt_lnk_backtobook::' => t( self::__TXT_LNK_BACKTOBOOK__ ) ];
                                
                                foreach ( $chapter as $k => $v ) {
                                        if ( !( is_array( $v ) || is_object( $v ) ) ) $vars[ "::$k::" ] = $v;
                                }
                                
                                $this->tmpl = $this->renderVars( $this->tmpl, $vars );
                        }
                        
                        return [
                                '#type'        => 'markup', '#markup' => $this->tmpl, 'content' => [
                                
                                ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        
                /**
                 * @param null $bk_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function bookmanagement_book ($bk_id = null ) {
                        
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        if ( $bk_id === null ) {
                                
                                return [ '#type' => 'markup', '#markup' => t( self::__NO_BOOK_GIVEN__ ), '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        }
                        
                        $_SESSION[ 'action' ][ 'viewbook' ] = $bk_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'bk_id' ] = $bk_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'bk_id' => $bk_id ] );
                        
                        $book = getBook( $this->database, $bk_id );
                        
                        $this->tmpl .= file_get_contents( $this->modpath . '/inc/publisso_gold_bookmanagement_book.tmpl.inc.php' );
                        
                        $this->tmpl_vars[ '::txt_logo::' ] = t( 'Logo' );
                        $this->tmpl_vars[ '::txt_description::' ] = t( 'Description' );
                        $this->tmpl_vars[ '::txt_authors::' ] = t( 'Authors' );
                        $this->tmpl_vars[ '::txt_sustaining_members::' ] = t( 'Sustaining Members' );
                        $this->tmpl_vars[ '::txt_corporation::' ] = t( 'Corporation' );
                        $this->tmpl_vars[ '::txt_issn::' ] = t( 'ISSN' );
                        $this->tmpl_vars[ '::txt_isbn::' ] = t( 'ISBN' );
                        $this->tmpl_vars[ '::txt_edition::' ] = t( 'Edition' );
                        $this->tmpl_vars[ '::txt_funding_number::' ] = t( 'Funding Number' );
                        $this->tmpl_vars[ '::txt_funding_name::' ] = t( 'Funding Name' );
                        $this->tmpl_vars[ '::txt_funding_id::' ] = t( 'Funding ID' );
                        $this->tmpl_vars[ '::txt_begin_publication::' ] = t( 'Begin Publication' );
                        $this->tmpl_vars[ '::txt_editorial_board::' ] = t( 'Editorial Board' );
                        $this->tmpl_vars[ '::txt_chapters::' ] = t( 'Chapters' );
                        $this->tmpl_vars[ '::lnk_create_book_chapter::' ] = t( 'Add Chapter' );
                        $this->tmpl_vars[ '::txt_edit_editorial_board::' ] = t( 'edit editorial board' );
                        
                        if ( !empty( $book[ 'bk_corporation' ] ) ) $book[ 'bk_corporation' ] = implode( ', ', json_decode( $book[ 'bk_corporation' ] ) );
                        
                        foreach ( $book as $k => $v ) {
                                if ( !( is_array( $v ) || is_object( $v ) ) ) $this->tmpl_vars[ "::$k::" ] = $v;
                        }
                        
                        $this->tmpl_vars[ '::logo::' ] = '<img width="100" src="/system/getPicture/bl/' . $bk_id . '">';
                        $this->tmpl_vars[ '::cover::' ] = '<img width="200" src="/system/getPicture/bc/' . $bk_id . '">';
                        $this->tmpl_vars[ '::bk_description::' ] = json_decode( base64_decode( $this->tmpl_vars[ '::bk_description::' ] ) );
                        
                        //set editorial board
                        $eb = json_decode( $book[ 'bk_editorial_board' ] );
                        
                        if ( !( is_array( $eb ) || is_object( $eb ) ) ) {
                                $eb = [];
                        }
                        
                        $this->tmpl_vars[ '::bk_editorial_board::' ] = '';
                        
                        foreach ( $eb as $uid => $data ) {
                                
                                $_user = getAllUserData( \Drupal::database(), $uid );
                                
                                if ( isset( $_user[ 'profile_corresponding_language' ] ) ) $_user[ 'profile_corresponding_language' ] = getCorrespondenceLanguages( $_user[ 'profile_corresponding_language' ] );
                                
                                if ( isset( $_user[ 'profile_salutation' ] ) ) $_user[ 'profile_salutation' ] = (string)getSalutation( $_user[ 'profile_salutation' ] );
                                
                                if ( isset( $_user[ 'profile_country' ] ) ) $_user[ 'profile_country' ] = getCountry( $_user[ 'profile_country' ] );
                                
                                $this->tmpl_vars[ '::bk_editorial_board::' ] .= ( !empty( $this->tmpl_vars[ '::bk_editorial_board::' ] ) ? ', ' : '' ) . $_user[ 'profile_firstname' ] . ' ' . $_user[ 'profile_lastname' ];
                        }
                        
                        
                        if ( isset( $this->tmpl_vars[ '::bk_description::' ]->value ) ) $this->tmpl_vars[ '::bk_description::' ] = $this->tmpl_vars[ '::bk_description::' ]->value;
                        
                        //get sustaining members
                        $sustaining_members = getBookSustainingMembers( $bk_id );
                        $this->tmpl_vars[ '::bk_sustaining_members::' ] = '';
                        
                        foreach ( $sustaining_members as $_ ) {
                                $this->tmpl_vars[ '::bk_sustaining_members::' ] .= '<img width="100" src="/system/getPicture/bsm/' . $_ . '"><br>';
                        }
                        /*
        if(file_exists($this->modpath.'/var/books/'.$bk_id.'/description.html'))
            $this->tmpl_vars['::bk_description::'] = file_get_contents($this->modpath.'/var/books/'.$bk_id.'/description.html');
        
        if(is_dir($this->modpath.'/var/books/'.$bk_id.'/')){
            
            $book_dir = scandir($this->modpath.'/var/books/'.$bk_id.'/');
            
            foreach($book_dir as $entry){
                
                if(preg_match('/^logo\./', $entry)){
                    $this->tmpl_vars['::logo::'] = '<img src="/'.$this->modpath.'/var/books/'.$bk_id.'/'.$entry.'">';
                }
                
                if(preg_match('/^cover\./', $entry)){
                    $this->tmpl_vars['::cover::'] = '<img src="/'.$this->modpath.'/var/books/'.$bk_id.'/'.$entry.'">';
                }
            }
        }
        */
                        //get the authors
                        $authors = [];
                        
                        foreach ( getBookAuthors( $this->database, $book[ 'bk_id' ] ) as $_ ) {
                                $authors[] = $_[ 'up_uid' ];
                        }
                        
                        #if(array_key_exists('user', $_SESSION) && $_SESSION['user']['weight'] >= 20){
                        
                        #if(count($authors) == 0 || in_array($_SESSION['user']['id'], $authors)){
                        $this->tmpl_vars[ '::lnk_add_chapter::' ] = '<a href="/publisso_gold/book/add_chapter/' . $book[ 'bk_id' ] . '" class="rwButton">' . t( 'ADD ARTICLE' ) . '</a>';
                        $this->tmpl_vars[ '::lnk_edit_editorial_board::' ] = '<a href="/publisso_gold/bookmanagement/set_editorialboard/' . $bk_id . '" class="rwButton">' . t( 'SET EDIT. BOARD' ) . '</a>';
                        $this->tmpl_vars[ '::lnk_edit_book::' ] = '<a href="/publisso_gold/bookmanagement/book/edit/' . $bk_id . '" class="rwButton">' . t( 'EDIT BOOK' ) . '</a>';
                        #}
                        #}
                        
                        $editorial_board = [ '#type' => 'details', '#title' => t( 'Editorial Board' ), '#open' => false, '#markup' => '' ];
                        
                        $this->renderVars();
                        
                        //get Chapters
                        $chapters_db = \Drupal::database()->select( 'rwPubgoldBookChapters', 'bc' )->fields( 'bc', [] )->condition( 'cp_bkid', $bk_id, '=' )->execute()->fetchAll();
                        
                        //generate chapter-output
                        $chapters = [];
                        
                        foreach ( $chapters_db as $_ ) {
                                
                                $errata = json_decode( base64_decode( $_->cp_errata ) );
                                $correction = json_decode( base64_decode( $_->cp_correction ) );
                                $abstract = json_decode( base64_decode( $_->cp_abstract ) );
                                $_->cp_abstract = '<h3>' . t( 'Abstract' ) . ':</h3>' . $abstract->value;
                                $_->cp_keywords = implode( ', ', json_decode( $_->cp_keywords ) );
                                
                                //get the author
                                $author = \Drupal::database()->select( 'rwpgvwAllUserdata', 'ud' )->fields( 'ud', [] )->condition( 'profile_userid', $_->cp_author_uid, '=' )->execute()->fetchAssoc();
                                
                                $metadata = file_get_contents( $this->modpath . '/inc/publisso_gold_bookmanagement_chapter_metadata.tmpl.inc.php' );
                                $vars = [];
                                
                                $vars = [ '::txt_license::' => t( self::__TXT_LICENSE__ ), '::txt_publication_year::' => t( self::__TXT_PUBLICATION_YEAR__ ), '::txt_publication_place::' => t( self::__TXT_PUBLICATION_PLACE__ ), '::txt_publisher::' => t( self::__TXT_PUBLISHER__ ), '::txt_doi::' => t( self::__TXT_DOI__ ), '::txt_ddc::' => t( self::__TXT_DDC__ ), '::txt_urn::' => t( self::__TXT_URN__ ), '::txt_corresponding_author::' => t( self::__TXT_CORRESPONDING_AUTHOR__ ), '::txt_keywords::' => t( self::__TXT_KEYWORDS__ ), '::txt_funding_number::' => t( self::__TXT_FUNDING_NUMBER__ ), '::txt_funding_id::' => t( self::__TXT_FUNDING_ID__ ), '::txt_version::' => t( self::__TXT_VERSION__ ) ];
                                
                                foreach ( $_ as $k => $v ) {
                                        if ( !( is_array( $v ) || is_object( $v ) ) ) $vars[ "::$k::" ] = $v;
                                }
                                
                                $metadata = $this->renderVars( $metadata, $vars );
                                
                                $chapters[] = [ '#type' => 'details', '#title' => $_->cp_title, '#description' => t( 'Author' ) . ': ' . $author[ 'profile_graduation' ] . ' ' . $author[ 'profile_lastname' ] . ', ' . $author[ 'profile_firstname' ], '#markup' => $_->cp_abstract . '<br><a href="/publisso_gold/book/read_chapter/' . $_->cp_id . '">' . t( 'read chapter' ) . '</a>', '#open' => false, 'meta' => [ '#type' => 'details', '#title' => t( 'Metadata' ), '#open' => false, '#markup' => $metadata ], 'errata' => [ '#type' => 'details', '#title' => t( 'Errata' ), '#open' => false, '#markup' => isset( $errata->value ) ? $errata->value : null ], 'correction' => [ '#type' => 'details', '#title' => t( 'Correction' ), '#open' => false, '#markup' => isset( $correction->value ) ? $correction->value : null ] ];
                        }
                        
                        $this->renderVars();
                        return [
                                '#type'        => 'markup', '#markup' => $this->tmpl, 'content' => [ /*$editorial_board,*/ #$chapters
                                ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        
                /**
                 * @return array
                 */
                public function Useridentity(){
                        return [];
                }
        
                /**
                 * @param null $bk_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function bookmanagement_addchapter ($bk_id = null ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        if ( $bk_id ) {
                                
                                $_SESSION[ 'breadcrumb_info' ][ 'bk_id' ] = $bk_id;
                                \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'bk_id' => $bk_id ] );
                                $_SESSION[ 'action' ][ 'viewbook' ] = $bk_id;
                                $book = $this->database->select( 'rwPubgoldBooks', 'bk' )->fields( 'bk', [] )->condition( 'bk_id', $bk_id, '=' )->execute()->fetchAssoc();
                                $this->renderVars();
                                
                                $form_add_chapter = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldBookmanagementAddChapter", [ 'bk_id' => $bk_id ] );
                                
                                return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form_add_chapter ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        }
                }
        
                /**
                 * @param null $bk_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function bookmanagement_roles ($bk_id = null ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $book = new \Drupal\publisso_gold\Controller\Book( $bk_id );
                        
                        $user = \Drupal::service( 'session' )->get( 'user' );
                        
                        if ( is_array( $user ) ) {
                                $user_has_rights = $user[ 'isEditor' ] | $user[ 'isEiC' ] | $user[ 'isEO' ];
                        }
                        
                        #if ( !( $user_has_rights || $book->isUserEditorialOffice( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $book->isUserEditorInChief( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $book->isUserEditor( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) ) ) {
                        #        return [ '#type' => 'markup', '#markup' => self::__ACCESS_DENIED__, '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        #}
                        
                        $form_assign_roles = '';
                        
                        if ( !$bk_id ) {
                                drupal_set_message( t( 'No book given!' ), 'error' );
                        }
                        
                        $form_assign_roles = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldBookmanagementAssignRoles", [ 'bk_id' => $bk_id ] );
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form_assign_roles ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param null $bk_id
                 * @return \Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function bookmanagement_delete ($bk_id = null ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $book = new \Drupal\publisso_gold\Controller\Book( $bk_id );
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $tables = [ 'rwPubgoldBookAuthors' => 'ba_bookid', 'rwPubgoldBookChapters' => 'cp_bkid', 'rwPubgoldBookEditorialOffice' => 'beo_bkid', 'rwPubgoldBookEditors' => 'be_bookid', 'rwPubgoldBookEditorsInChief' => 'eic_bkid', 'rwPubgoldBookReviewers' => 'br_bookid', 'rwPubgoldBooksSustainingMembers' => 'bsm_bkid', 'rwPubgoldBooks' => 'bk_id' ];
                        
                        if ( !empty( $bk_id ) ) {
                                $_SESSION[ 'breadcrumb_info' ][ 'bk_id' ] = $bk_id;
                                \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'bk_id' => $bk_id ] );
                                
                                //delete book
                                foreach ( $tables as $table => $field ) {
                                        \Drupal::database()->delete( $table )->condition( $field, $bk_id, '=' )->execute();
                                }
                                
                                //unassign from Table of content
                                \Drupal::database()->update( 'rwPubgoldTableOfContent' )->fields( [ 'toc_medium' => null, 'toc_medium_id' => null ] )->condition( 'toc_medium', 'book', '=' )->condition( 'toc_medium_id', $bk_id, '=' )->execute();
                        }
                        
                        return $this->redirect( 'publisso_gold.bookmanagement' );
                        
                }
        
                /**
                 * @param $token
                 * @return \Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function usermanagement_delete_confirm ($token ) {
                        
                        $qry = \Drupal::database()->select( 'rwPubgoldPendingActions', 't' )->fields( 't', [] )->condition( 'service_id', $token, '=' );
                        
                        if ( !$qry->countQuery()->execute()->fetchField() ) {
                                \Drupal::service( 'messenger' )->addError( (string)$this->t( \Drupal::service( 'publisso_gold.texts' )->get( 'usermanagement.user.delete.invalid_token' ) ) );
                        }
                        else {
                                $result = $qry->execute()->fetchAssoc();
                                $parameters = unserialize( $result[ 'parameters' ] );
                                
                                if ( is_array( $parameters ) && array_key_exists( 'uid', $parameters ) ) {
                                        
                                        $user = new User( $parameters[ 'uid' ] );
                                        
                                        if ( $user->hasActiveProcesses() ) {
                                                \Drupal::service( 'messenger' )->addError( (string)$this->t( \Drupal::service( 'publisso_gold.texts' )->get( 'usermanagement.user.delete.has_active_processes', 'fco' ) ) );
                                        }
                                        else {
                                                $user->delete();
                                                \Drupal::service( 'messenger' )->addMessage( (string)$this->t( \Drupal::service( 'publisso_gold.texts' )->get( 'usermanagement.user.delete.success' ) ) );
                                        }
                                }
                                else {
                                        \Drupal::service( 'messenger' )->addError( (string)$this->t( \Drupal::service( 'publisso_gold.texts' )->get( 'usermanagement.user.delete.invalid_parameters' ) ) );
                                }
                        }
                        
                        return $this->redirect( \Drupal::service( 'session' )->has( 'logged_in' ) && \Drupal::service( 'session' )->get( 'logged_in' ) ? 'publisso_gold.usermanagement' : 'publisso_gold.books' );
                }
        
                /**
                 * @param null $uid
                 * @return \Symfony\Component\HttpFoundation\RedirectResponse
                 * @throws \Exception
                 */
                public function usermanagement_delete ($uid = null ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'usermanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        if ( $uid ) {
                                
                                $user = new User( $uid );
                                
                                /**
                                 * Hat der User ein größeres Gewicht, als der aufrufende User => Fehler
                                 */
                                if ( $user->role[ 'weight' ] > \Drupal::service( 'session' )->get( 'user' )[ 'weight' ] ) {
                                        
                                        \Drupal::service( 'messenger' )->addError( (string)$this->t( \Drupal::service( 'publisso_gold.texts' )->get( 'usermanagement.user.delete.invalid_right', 'fco' ) ) );
                                }
                                else {
                                        
                                        if ( !( \Drupal::service( 'publisso_gold.tools' )->systemHasEqualUsers() || \Drupal::service( 'publisso_gold.tools' )->systemHasHigherUsers() ) ) {
                                                
                                                \Drupal::service( 'messenger' )->addError( (string)$this->t( \Drupal::service( 'publisso_gold.texts' )->get( 'usermanagement.user.delete.last_admin', 'fco' ) ) );
                                        }
                                        else {
                                                /**
                                                 * Hat der User noch unerledigte Aufgaben?
                                                 */
                                                
                                                if ( $user->hasActiveProcesses() ) {
                                                        \Drupal::service( 'messenger' )->addError( (string)$this->t( \Drupal::service( 'publisso_gold.texts' )->get( 'usermanagement.user.delete.has_active_processes', 'fco' ) ) );
                                                }
                                                else {
                                                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'uid' => $uid ] );
                                                        
                                                        $token = \Drupal::service( 'publisso_gold.tools' )->uniqid();
                                                        
                                                        $qry = \Drupal::database()->insert( 'rwPubgoldPendingActions' )->fields( [ 'process' => 'usermanagement_user_delete', 'created_by_uid' => \Drupal::service( 'session' )->get( 'user' )[ 'id' ], 'parameters' => serialize( [ 'uid' => $uid ] ), 'service_id' => $token ] );
                                                        
                                                        try {
                                                                $qry->execute();
                                                                
                                                                if ( null !== ( $template = \Drupal::service( 'publisso_gold.tools' )->getMailtemplate( 'usermanagement user delete' ) ) ) {
                                                                        
                                                                        $vars = [];
                                                                        $vars[ '::lnk.delete.confirm::' ] = Url::fromRoute( 'publisso_gold.usermanagement.user.delete.confirm', [ 'token' => $token ] )->setAbsolute()->toString();
                                                                        
                                                                        foreach ( $user->getKeys() as $_ ) $vars[ "user.$_" ] = $user->getElement( $_ );
                                                                        foreach ( $user->profile->getKeys() as $_ ) $vars[ "::user.profile.$_::" ] = $user->profile->getElement( $_ );
                                                                        
                                                                        $to = str_replace( array_keys( $vars ), array_values( $vars ), $template->recipient );
                                                                        
                                                                        $subject = $this->t( $template->subject, $vars );
                                                                        $body = $this->t( $template->template, $vars );
                                                                        
                                                                        
                                                                        \Drupal::service( 'publisso_gold.tools' )->sendMail( $to, $subject, $body );
                                                                        \Drupal::database()->insert( 'rwPubgoldMaillog' )->fields( [ 'recipient' => $to, 'subject' => $subject, 'body' => $body, 'init_process' => 'profiledelete', 'to_uid' => $user->getElement( 'id' ), 'created_by_uid' => $user->getElement( 'id' ) ] )->execute();
                                                                        
                                                                        \Drupal::service( 'messenger' )->addMessage( (string)$this->t( \Drupal::service( 'publisso_gold.texts' )->get( 'usermanagement.user.delete.mail_sent', 'fco' ), [ '@email' => $to ] ) );
                                                                }
                                                                else {
                                                                        \Drupal::service( 'messenger' )->addError( (string)$this->t( \Drupal::service( 'publisso_gold.texts' )->get( 'usermanagement.user.delete.mail_failed', 'fco' ) ) );
                                                                }
                                                        }
                                                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                                                \Drupal::service( 'messenger' )->addError( 'ERROR: ' . $e->getMessage() );
                                                        }
                                                }
                                        }
                                }
                        }
                        
                        return $this->redirect( 'publisso_gold.usermanagement' );
                        
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function add_book () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $this->tmpl .= file_get_contents( $this->modpath . '/inc/publisso_gold_addbook_lnk.tmpl.inc.php' );
                        $this->tmpl_vars[ '::lnk_addbook::' ] = t( self::__LNK_ADDBOOK__ );
                        $form_addbook = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldAddBook" );
                        $this->renderVars();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form_addbook ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function add_conference () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'conferencemanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $this->tmpl .= file_get_contents( $this->modpath . '/inc/publisso_gold_addconference_lnk.tmpl.inc.php' );
                        $this->tmpl_vars[ '::lnk_addconference::' ] = t( self::__LNK_ADDCONFERENCE__ );
                        $form_addconference = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldAddConference" );
                        $this->renderVars();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form_addconference ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function add_journal () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'journalmanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $this->tmpl .= file_get_contents( $this->modpath . '/inc/publisso_gold_addjournal_lnk.tmpl.inc.php' );
                        $this->tmpl_vars[ '::lnk_addjournal::' ] = t( self::__LNK_ADDJOURNAL__ );
                        $form_addjournal = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\AddJournal" );
                        $this->renderVars();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form_addjournal ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $jrn_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function edit_journal ($jrn_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'journalmanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $form_editjournal = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\EditJournal", [ 'jrn_id' => $jrn_id ] );
                        $this->renderVars();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form_editjournal ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function journalmanagement () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'journalmanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $this->tmpl .= file_get_contents( $this->modpath . '/inc/publisso_gold_addjournal_lnk.tmpl.inc.php' );
                        $this->tmpl_vars[ '::lnk_addjournal::' ] = (string)t( self::__LNK_ADDJOURNAL__ );
                        $form_filter = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldJournalmanagementFilter" );
                        
                        $this->renderVars();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form_filter ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $jrn_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function journalmanagement_addVolume ($jrn_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'journalmanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\AddVolume", [ 'jrn_id' => $jrn_id ] ) ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $jrn_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function journalmanagement_addIssue ($jrn_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'journalmanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\AddIssue", [ 'jrn_id' => $jrn_id ] ) ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param null $jrn_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function journalmanagement_journal ($jrn_id = null ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'journalmanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        if ( $jrn_id ) {
                                
                                $_SESSION[ 'breadcrumb_info' ][ 'jrn_id' ] = $jrn_id;
                                \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'jrn_id' => $jrn_id ] );
                                $_SESSION[ 'action' ][ 'viewjournal' ] = $jrn_id;
                                $journal = $this->database->select( 'rwPubgoldJournals', 'jrn' )->fields( 'jrn', [] )->condition( 'jrn_id', $jrn_id, '=' )->execute()->fetchAssoc();
                                $this->tmpl .= file_get_contents( $this->modpath . '/inc/publisso_gold_journalmanagement_journal.tmpl.inc.php' );
                                
                                $this->tmpl_vars[ '::txt_logo::' ] = (string)t( 'Logo' );
                                $this->tmpl_vars[ '::txt_sustaining_members::' ] = (string)t( 'Sustaining Members' );
                                $this->tmpl_vars[ '::txt_corporation::' ] = (string)t( 'Corporation' );
                                $this->tmpl_vars[ '::txt_issn::' ] = (string)t( 'ISSN' );
                                $this->tmpl_vars[ '::txt_articles::' ] = (string)t( 'Articles' );
                                $this->tmpl_vars[ '::lnk_create_journal_article::' ] = (string)t( 'Add Article' );
                                $this->tmpl_vars[ '::txt_edit_editorial_board::' ] = (string)t( 'edit editorial board' );
                                
                                #$book['bk_sustaining_members'] = implode(', ', json_decode($book['bk_sustaining_members']));
                                if ( $journal[ 'jrn_corporation' ] != '' ) $journal[ 'jrn_corporation' ] = implode( ', ', json_decode( $journal[ 'jrn_corporation' ] ) );
                                
                                foreach ( $journal as $k => $v ) {
                                        if ( !( is_array( $v ) || is_object( $v ) ) ) $this->tmpl_vars[ "::$k::" ] = $v;
                                }
                                
                                $this->renderVars();
                                
                                //get Articles
                                $articles_db = \Drupal::database()->select( 'rwPubgoldJournalArticles', 'jrna' )->fields( 'jrna', [] )->condition( 'jrna_jrnid', $jrn_id, '=' )->execute()->fetchAll();
                                
                                //generate chapter-output
                                $articles = [];
                                
                                foreach ( $articles_db as $_ ) {
                                        /*
                $errata         = json_decode($_->cp_errata);
                $correction     = json_decode($_->cp_correction);
                $abstract       = json_decode($_->cp_abstract);
                $_->cp_abstract = '<h3>'.t('Abstract').':</h3>'.$abstract->value;
                $_->cp_keywords = implode(', ', json_decode($_->cp_keywords));
                
                //get the author
                $author = \Drupal::database()
                    ->select('rwpgvwAllUserdata', 'ud')
                    ->fields('ud', [])
                    ->condition('profile_userid', $_->cp_author_uid, '=')
                    ->execute()
                    ->fetchAssoc();
                
                $metadata = file_get_contents($this->modpath.'/inc/publisso_gold_bookmanagement_chapter_metadata.tmpl.inc.php');
                $vars = array();
                
                $vars = [
                    '::txt_license::'               => t(self::__TXT_LICENSE__),
                    '::txt_publication_year::'      => t(self::__TXT_PUBLICATION_YEAR__),
                    '::txt_publication_place::'     => t(self::__TXT_PUBLICATION_PLACE__),
                    '::txt_publisher::'             => t(self::__TXT_PUBLISHER__),
                    '::txt_doi::'                   => t(self::__TXT_DOI__),
                    '::txt_ddc::'                   => t(self::__TXT_DDC__),
                    '::txt_urn::'                   => t(self::__TXT_URN__),
                    '::txt_corresponding_author::'  => t(self::__TXT_CORRESPONDING_AUTHOR__),
                    '::txt_keywords::'              => t(self::__TXT_KEYWORDS__),
                    '::txt_funding_number::'        => t(self::__TXT_FUNDING_NUMBER__),
                    '::txt_funding_id::'            => t(self::__TXT_FUNDING_ID__),
                    '::txt_version::'               => t(self::__TXT_VERSION__)
                ];
                
                foreach($_ as $k => $v){
                    $vars["::$k::"] = $v;
                }
                
                $metadata = $this->renderVars($metadata, $vars);
                
                $chapters[] = [
                    '#type' => 'details',
                    '#title' => $_->cp_title,
                    '#description' => t('Author').': '.$author['profile_graduation'].' '.$author['profile_lastname'].', '.$author['profile_firstname'],
                    '#markup' => $_->cp_abstract.'<br><a href="/publisso_gold/bookmanagement/read_chapter/'.$_->cp_id.'">'.t('read chapter').'</a>',
                    '#open' => false,
                    'meta' => [
                        '#type' => 'details',
                        '#title' => t('Metadata'),
                        '#open' => false,
                        '#markup' => $metadata
                    ],
                    'errata' => [
                        '#type' => 'details',
                        '#title' => t('Errata'),
                        '#open' => false,
                        '#markup' => $errata->value
                    ],
                    'correction' => [
                        '#type' => 'details',
                        '#title' => t('Correction'),
                        '#open' => false,
                        '#markup' => $correction->value
                    ]
                ];
                */
                                }
                        }
                        
                        return [
                                '#type'        => 'markup', '#markup' => $this->tmpl, 'content' => [ /*$editorial_board,*/
                                        $articles,
                                ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        
                /**
                 * @param null $cf_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function conferencemanagement_conference ($cf_id = null ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'conferencemanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        if ( $cf_id ) {
                                
                                $_SESSION[ 'breadcrumb_info' ][ 'cf_id' ] = $cf_id;
                                \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'cf_id' => $cf_id ] );
                                $_SESSION[ 'action' ][ 'viewconference' ] = $cf_id;
                                $conference = $this->database->select( 'rwPubgoldConferences', 'cf' )->fields( 'cf', [] )->condition( 'cf_id', $cf_id, '=' )->execute()->fetchAssoc();
                                $this->tmpl .= file_get_contents( $this->modpath . '/inc/publisso_gold_conferencemanagement_conference.tmpl.inc.php' );
                                
                                $this->tmpl_vars[ '::txt_title::' ] = (string)t( 'Title' );
                                $this->tmpl_vars[ '::txt_title_abbr::' ] = (string)t( 'Title abbr.' );
                                $this->tmpl_vars[ '::txt_sustaining_members::' ] = (string)t( 'Sustaining Members' );
                                $this->tmpl_vars[ '::txt_corporation::' ] = (string)t( 'Corporation' );
                                $this->tmpl_vars[ '::txt_date_start::' ] = (string)t( 'Date start' );
                                $this->tmpl_vars[ '::txt_date_end::' ] = (string)t( 'Date end' );
                                $this->tmpl_vars[ '::txt_location::' ] = (string)t( 'Location' );
                                $this->tmpl_vars[ '::txt_isbn::' ] = (string)t( 'ISBN' );
                                $this->tmpl_vars[ '::txt_papers::' ] = (string)t( 'Papers' );
                                $this->tmpl_vars[ '::lnk_create_conference_paper::' ] = (string)t( 'Add Paper' );
                                $this->tmpl_vars[ '::txt_edit_editorial_board::' ] = (string)t( 'edit editorial board' );
                                
                                if ( $conference[ 'cf_corporation' ] != '' ) $conference[ 'cf_corporation' ] = implode( ', ', json_decode( $conference[ 'cf_corporation' ] ) );
                                
                                $conference[ 'cf_description' ] = json_decode( base64_decode( $conference[ 'cf_description' ] ) );
                                $conference[ 'cf_description' ] = $conference[ 'cf_description' ]->value;
                                
                                foreach ( $conference as $k => $v ) {
                                        if ( !( is_array( $v ) || is_object( $v ) ) ) $this->tmpl_vars[ "::$k::" ] = $v;
                                }
                                
                                $this->renderVars();
                                
                                //get Papers
                                $papers_db = \Drupal::database()->select( 'rwPubgoldConferencePapers', 'cfp' )->fields( 'cfp', [] )->condition( 'cfp_cfid', $cf_id, '=' )->execute()->fetchAll();
                                
                                //generate chapter-output
                                $papers = [];
                                
                                foreach ( $papers_db as $_ ) {
                                        /*
                $errata         = json_decode($_->cp_errata);
                $correction     = json_decode($_->cp_correction);
                $abstract       = json_decode($_->cp_abstract);
                $_->cp_abstract = '<h3>'.t('Abstract').':</h3>'.$abstract->value;
                $_->cp_keywords = implode(', ', json_decode($_->cp_keywords));
                
                //get the author
                $author = \Drupal::database()
                    ->select('rwpgvwAllUserdata', 'ud')
                    ->fields('ud', [])
                    ->condition('profile_userid', $_->cp_author_uid, '=')
                    ->execute()
                    ->fetchAssoc();
                
                $metadata = file_get_contents($this->modpath.'/inc/publisso_gold_bookmanagement_chapter_metadata.tmpl.inc.php');
                $vars = array();
                
                $vars = [
                    '::txt_license::'               => t(self::__TXT_LICENSE__),
                    '::txt_publication_year::'      => t(self::__TXT_PUBLICATION_YEAR__),
                    '::txt_publication_place::'     => t(self::__TXT_PUBLICATION_PLACE__),
                    '::txt_publisher::'             => t(self::__TXT_PUBLISHER__),
                    '::txt_doi::'                   => t(self::__TXT_DOI__),
                    '::txt_ddc::'                   => t(self::__TXT_DDC__),
                    '::txt_urn::'                   => t(self::__TXT_URN__),
                    '::txt_corresponding_author::'  => t(self::__TXT_CORRESPONDING_AUTHOR__),
                    '::txt_keywords::'              => t(self::__TXT_KEYWORDS__),
                    '::txt_funding_number::'        => t(self::__TXT_FUNDING_NUMBER__),
                    '::txt_funding_id::'            => t(self::__TXT_FUNDING_ID__),
                    '::txt_version::'               => t(self::__TXT_VERSION__)
                ];
                
                foreach($_ as $k => $v){
                    $vars["::$k::"] = $v;
                }
                
                $metadata = $this->renderVars($metadata, $vars);
                
                $chapters[] = [
                    '#type' => 'details',
                    '#title' => $_->cp_title,
                    '#description' => t('Author').': '.$author['profile_graduation'].' '.$author['profile_lastname'].', '.$author['profile_firstname'],
                    '#markup' => $_->cp_abstract.'<br><a href="/publisso_gold/bookmanagement/read_chapter/'.$_->cp_id.'">'.t('read chapter').'</a>',
                    '#open' => false,
                    'meta' => [
                        '#type' => 'details',
                        '#title' => t('Metadata'),
                        '#open' => false,
                        '#markup' => $metadata
                    ],
                    'errata' => [
                        '#type' => 'details',
                        '#title' => t('Errata'),
                        '#open' => false,
                        '#markup' => $errata->value
                    ],
                    'correction' => [
                        '#type' => 'details',
                        '#title' => t('Correction'),
                        '#open' => false,
                        '#markup' => $correction->value
                    ]
                ];
                */
                                }
                        }
                        
                        return [
                                '#type'        => 'markup', '#markup' => $this->tmpl, 'content' => [ /*$editorial_board,*/
                                        $papers,
                                ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        
                /**
                 * @param $token
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 * @throws \Exception
                 */
                public function confirm_register ($token ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $qry = \Drupal::database()->select( 'rwPubgoldRegisterPending', 't' )->fields( 't', [] )->condition( 'rp_uuid', $token, '=' );
                        
                        $tokenExists = $qry->countQuery()->execute()->fetchField();
                        
                        if ( !$tokenExists ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied( $this->t( $this->texts->get( 'userprofile.register.invalid_token', 'fco' )
                        )
                        )
                                ;
                        
                        $result = $qry->execute()->fetchAll();
                        
                        $sso = \Drupal::service( 'publisso_gold.sso_provider' );
                        
                        if ( Publisso::setup()->getValue('system.sso.active') && \Drupal\publisso_sso\Classes\SSO::ssoAvailable() ) {
                                
                                #$ret = $sso->emailExists( $result[ 0 ]->rp_email );
                                $ret = \Drupal\publisso_sso\Classes\SSO::emailExists($result[0]->rp_email);
                                
                                if ( $ret === null ) {
                                        \Drupal::service( 'messenger' )->addError( (string)$this->t( $this->texts->get( 'userprofile.register.sso_failed' )
                                        )
                                        )
                                        ;
                                        
                                        return [];
                                }
                                
                                if ( $ret === true ) {
                                        
                                        \Drupal::service( 'messenger' )->addError( (string)$this->t( $this->texts->get( 'userprofile.register.sso_exists' )
                                        )
                                        )
                                        ;
                                        
                                        return [];
                                }
                        }
                        
                        $data = [ 'user' => $result[ 0 ]->rp_username, 'password' => $result[ 0 ]->rp_password, 'active' => 1, 'locked' => 0, 'reg_notification_sent' => $result[ 0 ]->rp_reg_notification_sent ];
                        
                        $initialPassword = $result[ 0 ]->rp_password_clear;
                        
                        $uid = $this->database->insert( 'rwPubgoldUsers' )->fields( $data )->execute();
                        
                        //create login
                        $this->database->insert( 'rwPubgoldUserroles' )->fields( [ 'user_id' => $uid, 'role_id' => $this->setup->getValue( 'default.user.role.id' ) ] )->execute();
                        
                        //create profile
                        $data = [ 'up_uid' => $uid, 'up_email' => $result[ 0 ]->rp_email, 'up_firstname' => $result[ 0 ]->rp_firstname, 'up_lastname' => $result[ 0 ]->rp_lastname, 'up_agree_privacy_protection' => $result[ 0 ]->rp_timestamp, 'up_show_data_in_boards' => $result[ 0 ]->rp_boards ];
                        $this->database->insert( 'rwPubgoldUserProfiles' )->fields( $data )->execute();
                        
                        //delete temporary data
                        $this->database->delete( 'rwPubgoldRegisterPending' )->condition( 'rp_id', $result[ 0 ]->rp_id, '=' )->execute();
                        
                        //redirect to userprofile
                        \Drupal::service( 'publisso_gold.tools' )->exportUser( $uid, $initialPassword );
                        \Drupal::service( 'messenger' )->addMessage( (string)$this->t( $this->texts->get( 'userprofile.register.success' )
                        )
                        )
                        ;
                        return $this->redirect( 'publisso_gold.login' );
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function conferencemanagement () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'conferencemanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $this->tmpl .= file_get_contents( $this->modpath . '/inc/publisso_gold_addconference_lnk.tmpl.inc.php' );
                        $this->tmpl_vars[ '::lnk_addconference::' ] = t( self::__LNK_ADDCONFERENCE__ );
                        $form_filter = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldConferencemanagementFilter" );
                        
                        $this->renderVars();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form_filter ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param null $uid
                 * @param Request $request
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function usermanagement ($uid = null, Request $request ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $_ = [];
                        if ( $request->query->has( 'page' ) ) $_[ 'page' ] = strtoupper( $request->query->get( 'page' ) );
                        if ( $request->query->has( 'order' ) ) $_[ 'sort' ][ 'field' ] = strtolower( \Drupal::service( 'publisso_gold.tools' )->translateToDefault( $request->query->get( 'order' ) ) );
                        $_[ 'sort' ][ 'order' ] = 'ASC';
                        if ( $request->query->has( 'sort' ) ) $_[ 'sort' ][ 'order' ] = strtoupper( $request->query->get( 'sort' ) );
                        
                        //if($request->query->has('order')) $_['sort']['order'] = strtoupper(\Drupal::service('publisso_gold.tools')->translateToDefault($request->query->get('order')));
                        
                        $user = $this->session->get( 'user' );
                        
                        //if user set as editor, eic or eo
                        $user_has_rights = $user[ 'isEditor' ] | $user[ 'isEiC' ] | $user[ 'isEO' ];
                        
                        #if ( !( \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'usermanagement' ) || $user_has_rights ) ) {
                        #        return $this->tools->accessDenied();
                        #}
                        
                        $lnkAdd = [ '#type' => 'link', '#title' => (string)$this->t( $this->texts->get( 'usermanagement.lnk.add', 'fc' ) ), '#url' => Url::fromRoute( 'publisso_gold.adduser' ), '#attributes' => [ 'class' => [ 'btn', 'btn-warning' ] ], '#suffix' => '<hr>' ];
                        
                        if ( $uid == null ) $form = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldUsermanagementFilter", $_ );
                        else {
                                $_user = new \Drupal\publisso_gold\Controller\User( $uid );
                                $profile = [
                                        'headline'   => [
                                                '#prefix' => '<h1>', '#suffix' => '</h1>', '#markup' => (string)$this->t( $this->texts->get( 'userprofile.headline', 'fco'
                                                ), [ '@salutation' => $this->tools->getSalutation( $_user->profile->getElement( 'salutation' ) ), '@name' => $_user->profile->getReadableFullName() ]
                                                ),
                                        ], 'picture' => [ '#markup' => '<img src="' . Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'up', 'id' => $uid ] )->toString() . '" class="rwUserpic">', '#prefix' => '<div class="rwUserpic">', '#suffix' => '</div>' ], 'data' => $_user->profile->getRenderableVcard(),
                                ];
                                
                                $form = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldUsermanagementUser", [ 'uid' => $uid ] );
                        }
                        
                        
                        return [ isset( $lnkAdd ) ? $lnkAdd : null, isset( $profile ) ? $profile : null, isset( $form ) ? $form : null, '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function adduser () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        //if user set as editor, eic or eo
                        
                        $user = \Drupal::service( 'session' )->get( 'user' );
                        
                        if ( is_array( $user ) ) {
                                $user_has_rights = $user[ 'isEditor' ] | $user[ 'isEiC' ] | $user[ 'isEO' ];
                        }
                        
                        #if ( !( \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'usermanagement' ) || $user_has_rights ) ) {
                        #        return [ '#type' => 'markup', '#markup' => self::__ACCESS_DENIED__, '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        #}
                        
                        $filter_form = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldUsermanagementAddUser" );
                        $this->renderVars();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $filter_form ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function adduser_preview () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'usermanagement' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $user = $this->database->select( 'rwPubgoldUserprofilePreview', 'u' )->fields( 'u', [] )->condition( 'pv_creator', \Drupal::service( 'session' )->get( 'user' )[ 'id' ], '=' )->execute()->fetchAssoc();
                        
                        if ( $user !== false ) {
                                
                                $form = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldUsermanagementAddUserPreview" );
                                $this->tmpl .= implode( '', file( $this->modpath . '/inc/publisso_gold_usermanagementAddUserPreview.tmpl.inc.php' ) );
                                
                                
                                $user[ 'pv_country' ] = getCountry( $user[ 'pv_country' ] );
                                $user[ 'pv_salutation' ] = $this->tools->getSalutation( $user[ 'pv_satutation' ] );
                                $user[ 'pv_area_of_expertise_aoeid' ] = json_decode( $user[ 'pv_area_of_expertise_aoeid' ] );
                                $aoe = '';
                                
                                foreach ( $user[ 'pv_area_of_expertise_aoeid' ] as $_ ) {
                                        $aoe .= ( !empty( $aoe ) ? ', ' : '' ) . getAreasOfExpertise( $this->database, $_ );
                                }
                                
                                $user[ 'pv_area_of_expertise' ] .= ( !empty( $user[ 'pv_area_of_expertise' ] ) ? ', ' : '' ) . $aoe;
                                $user[ 'pv_correspondence_language' ] = t( (string)getCorrespondenceLanguages( $user[ 'pv_correspondence_language' ] ) );
                                
                                
                                foreach ( $user as $key => $var ) {
                                        if ( !( is_array( $var ) || is_object( $var ) ) ) $this->tmpl_vars[ "::$key::" ] = $var;
                                }
                                
                                $this->tmpl_vars[ '::text_headline::' ] = t( 'Userprofile of' );
                                $this->tmpl_vars[ '::text_userprofile_edit::' ] = t( 'edit user profile' );
                                $this->tmpl_vars[ '::text_institute::' ] = t( 'Institute' );
                                $this->tmpl_vars[ '::text_country::' ] = t( 'Country' );
                                $this->tmpl_vars[ '::text_city::' ] = t( 'City' );
                                $this->tmpl_vars[ '::text_street::' ] = t( 'Street' );
                                $this->tmpl_vars[ '::text_email::' ] = t( 'Email-address' );
                                $this->tmpl_vars[ '::text_orcid::' ] = t( 'ORCID' );
                                $this->tmpl_vars[ '::text_department::' ] = t( 'Department' );
                                $this->tmpl_vars[ '::text_telephone::' ] = t( 'Telephone' );
                                $this->tmpl_vars[ '::text_area_of_expertise::' ] = t( 'Area of Expertise' );
                                $this->tmpl_vars[ '::text_correspondence_language::' ] = t( 'Correspondence-language' );
                        }
                        
                        $this->renderVars();
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function reviewpages () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'reviewpagemgmt' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $this->renderVars();
                        $reviewpages = getReviewpages();
                        $form = [];
                        
                        $form[] = [ '#type' => 'markup', '#markup' => '<a href="add_reviewpage">' . ( (string)t( 'add reviewpage' ) ) . '</a>' ];
                        
                        
                        foreach ( getReviewpages() as $_ ) {
                                
                                $element = [ '#type' => 'details', '#title' => $_->rp_name, '#open' => false, 'content' => [] ];
                                
                                $rp = json_decode( base64_decode( $_->rp_schema ), true );
                                
                                foreach ( $rp as $text => $type ) {
                                        
                                        $element[ 'content' ][] = [ '#type' => 'markup', '#markup' => $text . ': ' . ( $type == 'checkbox' ? 'bool' : 'text' ) . '<br>' ];
                                        
                                }
                                
                                $form[] = $element;
                        }
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function add_reviewpage () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'reviewpagemgmt' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $this->renderVars();
                        
                        $form = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldReviewPageAdd" );
                        
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $term
                 * @param Request $request
                 * @return array
                 */
                public function search ($term, Request $request ) {
                        
                        $term = $request->query->has( 'keys' ) ? $request->query->get( 'keys' ) : $term;
                        $start = $request->query->has( 'start' ) ? $request->query->get( 'start' ) : 0;
                        
                        if ( $term ) {
                                $data = [ "query" => $term, "start" => $start, "rows" => 10, "operator" => "AND", "collapsing" => [ "max" => 2, "mode" => "OFF", "type" => "OPTIMIZED" ], "returnedFields" => [ "url", 'title' ], "snippets" => [ [ "field" => "content", "tag" => "code", "separator" => " … ", "maxSize" => 400, "maxNumber" => 1, "fragmenter" => "SENTENCE" ] ], "enableLog" => false, "searchFields" => [ [ "field" => "title", "mode" => "TERM_AND_PHRASE", "boost" => 10 ], [ "field" => "content", "mode" => "TERM_AND_PHRASE", "boost" => 1 ], [ "field" => "titleExact", "mode" => "TERM_AND_PHRASE", "boost" => 10 ], [ "field" => "contentExact", "mode" => "TERM_AND_PHRASE", "boost" => 1 ], [ "field" => "titlePhonetic", "mode" => "TERM_AND_PHRASE", "boost" => 10 ], [ "field" => "contentPhonetic", "mode" => "TERM_AND_PHRASE", "boost" => 1 ] ] ];
                                
                                $queryCore = \Drupal::service( 'publisso_gold.setup' )->getValue( 'searchengine.core.name' );
                                $searchUser = \Drupal::service( 'publisso_gold.setup' )->getValue( 'searchengine.login.user' );
                                $searchKey = \Drupal::service( 'publisso_gold.setup' )->getValue( 'searchengine.login.key' );
                                $url = "http://search.publisso.de:9090/services/rest/index/$queryCore/search/field?login=$searchUser&key=$searchKey";
                                
                                $ch = curl_init();
                                curl_setopt( $ch, CURLOPT_PORT, 9090 );
                                curl_setopt( $ch, CURLOPT_URL, $url );
                                curl_setopt( $ch, CURLOPT_POST, true );
                                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                                curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $data ) );
                                curl_setopt( $ch, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json', 'Accept: application/xml' ] );
                                
                                $out = curl_exec( $ch );
                                curl_close( $ch );
                                
                                if ( $out ) {
                                        $xml = new \DOMDocument( "1.0", "UTF-8" );
                                        $xml->loadXML( $out );
                                        $xpath = new \DOMXpath( $xml );
                                        
                                        if ( $xpath->evaluate( '/result[@numFound > 0]' )->length ) {
                                                
                                                $params = $request->query->all();
                                                $route = \Drupal::routeMatch()->getRouteName();
                                                $start = $xpath->evaluate( '/result' )[ 0 ]->getAttribute( 'start' );
                                                $total = $xpath->evaluate( '/result' )[ 0 ]->getAttribute( 'numFound' );
                                                $rows = $xpath->evaluate( '/result' )[ 0 ]->getAttribute( 'rows' );
                                                $pages = ceil( $total / $rows );
                                                $page = floor( $start / $rows ) + 1;
                                                
                                                $minDoc = $maxDoc = 0;
                                                $li = '';
                                                
                                                foreach ( $xpath->evaluate( '/result/document' ) as $node ) {
                                                        if ( $minDoc == null ) $minDoc = $node->getAttribute( 'pos' ) + 1;
                                                        $doc = new \DOMDocument( "1.0", "UTF-8" );
                                                        $doc->loadXML( $node->C14N() );
                                                        $xp = new \DOMXpath( $doc );
                                                        $url = $title = '';
                                                        
                                                        if($xp->evaluate( "/document/field[./name = 'url']/value" )[ 0 ])
                                                                $url = $xp->evaluate( "/document/field[./name = 'url']/value" )[ 0 ]->nodeValue;
                                                        
                                                        if($xp->evaluate( "/document/field[./name = 'title']/value" )[ 0 ])
                                                                $title = $xp->evaluate( "/document/field[./name = 'title']/value" )[ 0 ]->nodeValue;
                                                        
                                                        $content = $xp->evaluate( "/document/snippet[./name = 'content']/value" )[ 0 ]->nodeValue;
                                                        $li .= '<li><a href="' . $url . '">' . $title . '</a><br><div>' . $content . '</div><div><a href="' . $url . '">' . $url . '</a></div></li>';
                                                }
                                                
                                                $maxDoc = $node->getAttribute( 'pos' ) + 1;
                                                $this->tmpl .= '<h3>' . ( (string)$this->t( 'Search result(s) for "@term":', [ '@term' => $term ] ) ) . '</h3>';
                                                if ( $pages > 1 ) $this->tmpl .= '<em>' . ( (string)$this->t( 'Result @min to @max from @total (Page @page from @pages)', [ '@min' => $minDoc, '@max' => $maxDoc, '@total' => $total, '@page' => $page, '@pages' => $pages ] ) ) . '</em><br><br>';
                                                $this->tmpl .= '<ul class="search_result">' . $li . '</ul><hr>';
                                                
                                                $i = 0;
                                                if ( $pages > 20 ) $i = $page - 9 < 1 ? 1 : $page - 9;
                                                
                                                if ( $pages > 1 ) {
                                                        $pagination = '';
                                                        $next = $prev = null;
                                                        
                                                        for ( $c = $i - 1; $c <= $i + 20; $c++ ) {
                                                                
                                                                if ( $c >= 1 && $c <= $pages ) {
                                                                        
                                                                        $cStart = ( $c - 1 ) * $rows;
                                                                        
                                                                        if ( $c < $page ) $prev = $cStart;
                                                                        if ( $c > $page && $next == null ) $next = $cStart;
                                                                        
                                                                        $params[ 'start' ] = $cStart;
                                                                        $url = Url::fromRoute( $route, $params );
                                                                        $link = Link::fromTextAndUrl( $c, $url );
                                                                        
                                                                        if ( $c != $page ) {
                                                                                $pagination .= $link->toString();
                                                                        }
                                                                        else {
                                                                                $pagination .= $c;
                                                                        }
                                                                }
                                                                
                                                                if ( $c < $i + 20 && $c > 0 && $c < $pages ) $pagination .= ' · ';
                                                        }
                                                        
                                                        if ( $i - 1 > 1 ) $pagination = " · ... · " . $pagination;
                                                        
                                                        if ( $page > 1 ) {
                                                                $params[ 'start' ] = $prev;
                                                                $url = Url::fromRoute( $route, $params );
                                                                $link = Link::fromTextAndUrl( "<< " . ( (string)$this->t( 'back' ) ), $url );
                                                                $pagination = $link->toString() . '&nbsp;&nbsp;' . $pagination;
                                                        }
                                                        
                                                        if ( $c < $pages ) $pagination .= ' · ... · ';
                                                        if ( $page < $pages ) {
                                                                
                                                                if ( $next ) {
                                                                        $params[ 'start' ] = $next;
                                                                        $url = Url::fromRoute( $route, $params );
                                                                        $link = Link::fromTextAndUrl( ( (string)$this->t( 'next' ) ) . ' >>', $url );
                                                                        $pagination .= '&nbsp;&nbsp;' . $link->toString();
                                                                }
                                                        }
                                                }
                                                
                                                if ( !empty( $pagination ) ) $this->tmpl .= '<div class="alert alert-info text-center">' . $pagination . '</div>';
                                        }
                                        else {
                                                \Drupal::service( 'messenger' )->addWarning( (string)$this->t( 'No results for search-term "@term" found!', [ '@term' => $term ] ) );
                                        }
                                }
                        }
                        
                        $this->renderVars();
                        return [ '#type' => 'inline_template', '#template' => $this->tmpl, '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function TableOfContent () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'tocmgmt' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $this->renderVars();
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldTableOfContentButtonAdd" ), \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldTableOfContent" ) ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function TableOfContent_add () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'tocmgmt' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $this->renderVars();
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldTableOfContentAdd" ) ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $toc_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function TableOfContent_edit ($toc_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'tocmgmt' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'toc_id' ] = $toc_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'toc_id' => $toc_id ] );
                        $this->renderVars();
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldTableOfContentEdit", [ 'toc_id' => $toc_id ] ) ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $toc_id
                 * @return \Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function TableOfContent_delete ($toc_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'tocmgmt' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'toc_id' ] = $toc_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'toc_id' => $toc_id ] );
                        
                        \Drupal::database()->delete( 'rwPubgoldTableOfContentItem' )->condition( 'toci_tocid', $toc_id, '=' )->execute();
                        \Drupal::database()->delete( 'rwPubgoldTableOfContent' )->condition( 'toc_id', $toc_id, '=' )->execute();
                        
                        return $this->redirect( 'publisso_gold.table_of_content' );
                }
        
                /**
                 * @param $toci_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function TableOfContentItem_edit ($toci_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'tocmgmt' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'toci_id' ] = $toci_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'toci_id' => $toci_id ] );
                        
                        $this->renderVars();
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldTableOfContentItemEdit", [ 'toci_id' => $toci_id ] ) ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $toc_id
                 * @param $toci_id
                 * @return \Symfony\Component\HttpFoundation\RedirectResponse
                 * @throws \Exception
                 */
                public function TableOfContentItem_add ($toc_id, $toci_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'tocmgmt' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'toc_id' ] = $toc_id;
                        $_SESSION[ 'breadcrumb_info' ][ 'toci_id' ] = $toci_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'toc_id' => $toc_id, 'toci_id' => $toci_id ] );
                        
                        \Drupal::database()->insert( 'rwPubgoldTableOfContentItem' )->fields( [ 'toci_parent' => $toci_id == 0 ? null : $toci_id, 'toci_tocid' => $toc_id, 'toci_title' => (string)t( '~ new listentry ~' ), 'toci_weight' => 0 ] )->execute();
                        
                        $toc = new \Drupal\publisso_gold\Controller\TableOfContent( $toc_id );
                        $toc->storeStructure();
                        
                        return $this->redirect( 'publisso_gold.table_of_content.edit', [ 'toc_id' => $toc_id ] );
                        
                }
        
                /**
                 * @param $toci_id
                 * @return \Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function TableOfContentItem_delete ($toci_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        #if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'tocmgmt' ) ) return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'toci_id' ] = $toci_id;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'toci_id' => $toci_id ] );
                        
                        $toc_item = new TableOfContentItem( $toci_id );
                        $toc_id = $toc_item->getElement( 'tocid' );
                        
                        \Drupal::database()->delete( 'rwPubgoldTableOfContentItem' )->condition( 'toci_id', $toci_id, '=' )->execute();
                        
                        $toc = new \Drupal\publisso_gold\Controller\TableOfContent( $toc_id );
                        $toc->storeStructure();
                        #echo '<pre>'.print_r($toc, 1).'</pre>'; exit();
                        return $this->redirect( 'publisso_gold.table_of_content.edit', [ 'toc_id' => $toc_id ] );
                        
                }
        
                /**
                 * @param $bk_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function bookInvitedUsers ($bk_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $book = new \Drupal\publisso_gold\Controller\Book( $bk_id );
                        
                        $user = \Drupal::service( 'session' )->get( 'user' );
                        
                        if ( is_array( $user ) ) {
                                $user_has_rights = $user[ 'isEditor' ] | $user[ 'isEiC' ] | $user[ 'isEO' ];
                        }
                        
                        if ( !( $book->isUserEditorialOffice( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $book->isUserEditorInChief( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || preg_match( "/^1|2$/", \Drupal::service( 'session' )->get( 'user' )[ 'role_id' ] ) ) ) {
                                return [ '#type' => 'markup', '#markup' => self::__ACCESS_DENIED__, '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        }
                        
                        $book = new \Drupal\publisso_gold\Controller\Book( $bk_id );
                        
                        $tabledata = [];
                        $res = \Drupal::database()->select( 'rwPubgoldUserInvitations', 't' )->fields( 't', [] )->condition( 'medium_type', 'book', '=' )->condition( 'medium_id', $bk_id, '=' )->execute()->fetchAll();
                        //print_r($res); exit();
                        foreach ( $res as $_ ) {
                                
                                $user = new \Drupal\publisso_gold\Controller\user( $_->created_by_uid );
                                
                                $idx = count( $tabledata );
                                
                                //assemble table
                                $tabledata[ $idx ][] = $user->profile->getReadableName();
                                $tabledata[ $idx ][] = $_->firstname;
                                $tabledata[ $idx ][] = $_->lastname;
                                $tabledata[ $idx ][] = $_->email;
                                $tabledata[ $idx ][] = $_->user;
                                $tabledata[ $idx ][] = $_->role;
                                $tabledata[ $idx ][] = new FormattableMarkup( '<span class="invitationStatus' . ucfirst( $_->status ) . '">@status</span>', [ '@status' => (string)$this->t( $_->status ) ] );
                        }
                        
                        return [
                                'title'     => [ '#markup' => $book->getElement( 'title' ), '#prefix' => '<h1>', '#suffix' => '</h1>' ], 'table' => [
                                        '#type' => 'table', '#header' => [ (string)$this->t( 'Creator' ), (string)$this->t( 'Firstname' ), (string)$this->t( 'Lastname' ), (string)$this->t( 'Email' ), (string)$this->t( 'Login' ), (string)$this->t( 'Role' ), (string)$this->t( 'Status' ) ],
                                        
                                        '#rows' => $tabledata, '#empty' => (string)$this->t( 'No Invitations for this book....' ),
                                ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        
                /**
                 * @param $bk_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function bookInviteUser ($bk_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $book = new \Drupal\publisso_gold\Controller\Book( $bk_id );
                        
                        $user = \Drupal::service( 'session' )->get( 'user' );
                        
                        if ( is_array( $user ) ) {
                                $user_has_rights = $user[ 'isEditor' ] | $user[ 'isEiC' ] | $user[ 'isEO' ];
                        }
                        
                        #if ( !( $user_has_rights || $book->isUserEditorialOffice( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $book->isUserEditorInChief( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $book->isUserEditor( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) ) ) {
                        #        return [ '#type' => 'markup', '#markup' => self::__ACCESS_DENIED__, '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        #}
                        
                        $book = new \Drupal\publisso_gold\Controller\Book( $bk_id );
                        
                        if ( !$book->getControlElement( 'allow_invite_users' ) ) {
                                return [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldInviteUser", [ 'bk_id' => $bk_id, 'medium' => 'book' ] ) ];
                        }
                        else {
                                
                                if ( !is_file( getCwd() . '/' . $this->modpath . "/src/Form/publisso_goldInviteUser_Book" . $book->getElement( 'id' ) . ".php" ) ) {
                                        \Drupal::service('messenger')->addError('"allow invite users" is active for this book, but no special invitation-process is implemented for this book. Please consider to remove "allow invite users".');
                                        return [];
                                }
                                else {
                                        return [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldInviteUser_Book" . $book->getElement( 'id' ), [ 'bk_id' => $bk_id, 'medium' => 'book' ] ) ];
                                }
                        }
                }
        
                /**
                 * @param $wf_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function inviteUser ($wf_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $wf_id );
                        
                        switch ( $workflow->getDataElement( 'type' ) ) {
                                
                                case 'bookchapter':
                                        $medium = new \Drupal\publisso_gold\Controller\Book( $workflow->getDataElement( 'bk_id' ) );
                                        break;
                                
                                case 'journalarticle':
                                        $medium = new \Drupal\publisso_gold\Controller\Journal( $workflow->getDataElement( 'jrn_id' ) );
                                        break;
                                
                                case 'conferencepaper':
                                        $medium = new \Drupal\publisso_gold\Controller\Book( $workflow->getDataElement( 'cf_id' ) );
                                        break;
                        }
                        
                        $user = \Drupal::service( 'session' )->get( 'user' );
                        
                        if ( is_array( $user ) ) {
                                $user_has_rights = $user[ 'isEditor' ] | $user[ 'isEiC' ] | $user[ 'isEO' ];
                        }
                        
                        #if ( !( $user_has_rights || $medium->isUserEditorialOffice( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $medium->isUserEditorInChief( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $medium->isUserEditor( \Drupal::service( 'session' )->get( 'user' )[ 'id' ]
                        #        ) || $workflow->isEditor( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $workflow->isEditorInChief( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) ) ) {
                        #        return [ '#type' => 'markup', '#markup' => self::__ACCESS_DENIED__, '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        #}
                        
                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $wf_id );
                        
                        switch ( substr($workflow->getElement( 'type' ), 0, 1) ) {
                                
                                case 'b':
                                        $type = 'book';
                                        $medium = new \Drupal\publisso_gold\Controller\Book( $workflow->getDataElement( 'bk_id' ) );
                                        $id = 'bk_id';
                                        $$id = $medium->getElement( 'id' );
                                        break;
                                
                                case 'j':
                                        $type = 'journal';
                                        $medium = new \Drupal\publisso_gold\Controller\Journal( $workflow->getDataElement( 'jrn_id' ) );
                                        break;
                                
                                case 'c':
                                        $type = 'conference';
                                        $medium = new \Drupal\publisso_gold\Controller\Conference( $workflow->getDataElement( 'cf_id' ) );
                                        break;
                        }
                        
                        if ( !$medium->getControlElement( 'allow_invite_users' ) ) {
                                return [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldInviteUser", [ $id => $$id, 'medium' => $type, 'wf_id' => $wf_id ] ) ];
                        }
                        else {
                                
                                if ( !is_file( getCwd() . '/' . $this->modpath . "/src/Form/publisso_goldInviteUser_" . ucfirst( $type ) . $medium->getElement( 'id' ) . ".php" ) ) {
                                        drupal_set_message( '"allow invite users" is active for this ' . $type . ', but no special invitation-process is implemennted for this ' . $type . '. Please consider to remove "allow invite users".', 'error' );
                                        return [];
                                }
                                else {
                                        return [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldInviteUser_" . ucfirst( $type ) . $medium->getElement( 'id' ), [ $id => $$id, 'medium' => $type, 'wf_id' => $wf_id ] ) ];
                                }
                        }
                }
        
                /**
                 * @param $wf_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function unassignReviewers ($wf_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $wf_id );
                        
                        switch ( $workflow->getDataElement( 'type' ) ) {
                                
                                case 'bookchapter':
                                        $medium = new \Drupal\publisso_gold\Controller\Book( $workflow->getDataElement( 'bk_id' ) );
                                        break;
                                
                                case 'journalarticle':
                                        $medium = new \Drupal\publisso_gold\Controller\Journal( $workflow->getDataElement( 'jrn_id' ) );
                                        break;
                                
                                case 'conferencepaper':
                                        $medium = new \Drupal\publisso_gold\Controller\Book( $workflow->getDataElement( 'cf_id' ) );
                                        break;
                        }
                        
                        $user = \Drupal::service( 'session' )->get( 'user' );
                        
                        if ( is_array( $user ) ) {
                                $user_has_rights = $user[ 'isEditor' ] | $user[ 'isEiC' ] | $user[ 'isEO' ];
                        }
                        
                        #if ( !( $user_has_rights || $medium->isUserEditorialOffice( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $medium->isUserEditorInChief( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $medium->isUserEditor( \Drupal::service( 'session' )->get( 'user' )[ 'id' ]
                        #        ) || $workflow->isEditor( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $workflow->isEditorInChief( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) ) ) {
                        #        return [ '#type' => 'markup', '#markup' => self::__ACCESS_DENIED__, '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        #}
                        
                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $wf_id );
                        
                        return [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldUnassignReviewer", [ 'wf_id' => $wf_id ] ) ];
                }
        
                /**
                 * @param $wf_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function assignReviewers ($wf_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $wf_id );
                        
                        switch ( $workflow->getDataElement( 'type' ) ) {
                                
                                case 'bookchapter':
                                        $medium = new \Drupal\publisso_gold\Controller\Book( $workflow->getDataElement( 'bk_id' ) );
                                        break;
                                
                                case 'journalarticle':
                                        $medium = new \Drupal\publisso_gold\Controller\Journal( $workflow->getDataElement( 'jrn_id' ) );
                                        break;
                                
                                case 'conferencepaper':
                                        $medium = new \Drupal\publisso_gold\Controller\Book( $workflow->getDataElement( 'cf_id' ) );
                                        break;
                        }
                        
                        $user = \Drupal::service( 'session' )->get( 'user' );
                        
                        if ( is_array( $user ) ) {
                                $user_has_rights = $user[ 'isEditor' ] | $user[ 'isEiC' ] | $user[ 'isEO' ];
                        }
                        
                        #if ( !( $user_has_rights || $medium->isUserEditorialOffice( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $medium->isUserEditorInChief( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $medium->isUserEditor( \Drupal::service( 'session' )->get( 'user' )[ 'id' ]
                        #        ) || $workflow->isEditor( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || $workflow->isEditorInChief( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) || \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) ) ) {
                        #        return [ '#type' => 'markup', '#markup' => self::__ACCESS_DENIED__, '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                        #}
                        
                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $wf_id );
                        
                        return [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldAssignReviewers", [ 'wf_id' => $wf_id ] ) ];
                }
        
                /**
                 * @param $token
                 * @param $action
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function Invitation ($token, $action ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        if ( !\Drupal::database()->select( 'rwPubgoldUserInvitations', 't' )->fields( 't', [] )->condition( 'token', $token, '=' )->countQuery()->execute()->fetchField() ) {
                                drupal_set_message( t( 'Invalid request-token submitted!' ), 'error' );
                        }
                        else {
                                switch ( $action ) {
                                        case 'accept':
                                                $form = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldInvitationActionAccept", [ 'token' => $token ] );
                                                break;
                                        
                                        case 'reject':
                                                $form = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldInvitationActionReject", [ 'token' => $token ] );
                                                break;
                                }
                        }
                        
                        return [ '#type' => 'markup', '#markup' => $msg, 'content' => [ $form ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function password_reset () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $this->tmpl = '
            <h2>' . ( (string)t( 'Reset Password' ) ) . '</h2>
        ';
                        
                        $form = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldPasswordReset" );
                        
                        $this->renderVars();
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $wf_id
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function BookWorkflowAddFiles ($wf_id ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $wf_id );
                        
                        if ( !$workflow->getElement( 'id' ) ) {
                                return [
                                        '#type'     => 'markup', '#markup' => (string)$this->t( "Submission not found!" ), /*'content' => array(
                                    $this->login_form
                                )*/
                                        '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                                ];
                        }
                        
                        $now = time();
                        $wfModified = strtotime( $workflow->getElement( 'modified' ) );
                        $timeDiff = $now - $wfModified;
                        
                        if ( $workflow->getElement( 'created_by_uid' ) != \Drupal::service( 'session' )->get( 'user' )[ 'id' ] || $timeDiff > 30 * 60 ) {
                                return [
                                        '#type'     => 'markup', '#markup' => (string)$this->t( "You are not authorized to add files to this submission!" ), /*'content' => array(
                                    $this->login_form
                                )*/
                                        '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                                ];
                        }
                        
                        $this->renderVars();
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldBookSubmissionAddFiles", [ 'wf_id' => $wf_id ] ) ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $token
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function password_reset_do ($token ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $this->tmpl = '
            <h2>' . ( (string)t( 'New Password' ) ) . '</h2>
        ';
                        
                        $form = null;
                        
                        $_SESSION[ 'breadcrumb_info' ][ 'token' ] = $token;
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'token' => $token ] );
                        
                        $query = \Drupal::database();
                        $query = $query->select( 'rwPubgoldPWReset', 't' );
                        $query = $query->fields( 't', [ 'pwr_loginid' ] );
                        $query = $query->condition( 'pwr_token', $token, '=' );
                        
                        $query_count = $query->countQuery();
                        $query_count = $query->execute();
                        
                        if ( $query_count->fetchField() == 0 ) {
                                $this->tmpl = '';
                                drupal_set_message( (string)t( 'Token not valid! Please contact Support!' ), 'error' );
                        }
                        else {
                                
                                $query = $query->execute();
                                $result = $query->fetchAssoc();
                                $form = \Drupal::formBuilder()->getForm( "Drupal\\" . $this->modname . "\Form\publisso_goldSetPassword", [ 'login_id' => $result[ 'pwr_loginid' ] ] );
                        }
                        
                        $this->renderVars();
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @return \Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function user_logout () {
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        Publisso::LoginProvider()->logout();
                        \Drupal::service( 'messenger' )->addMessage( (string)$this->t( $texts->get( 'global.message.logout.success', 'fc' ) ) );
                        \Drupal::cache('menu')->invalidateAll(); // for clearing the menu cache
                        \Drupal::service('plugin.manager.menu.link')->rebuild(); // rebuild the menu
                        return $this->redirect( 'publisso_gold.login' );
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function debug () {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        
                        $book = new Book( 39 );
                        $this->tmpl = '<pre>' . print_r( $book, 1 ) . '</pre>';
                        
                        $this->renderVars();
                        return [
                                '#type'     => 'markup', '#markup' => $this->tmpl, /*'content' => array(
                $this->login_form
            )*/
                                '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        
                /**
                 * @param null $tmpl
                 * @param null $vars
                 * @return string|string[]|null
                 */
                private function renderVars ($tmpl = null, $vars = null ) {
                        
                        if ( $tmpl == null || $vars == null ) {
                                //set Site-Vars
                                $this->tmpl = str_replace( array_keys( $this->tmpl_vars ), array_values( $this->tmpl_vars ), $this->tmpl );
                                
                                //remove unused vars
                                $this->tmpl = preg_replace( '(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl );
                        }
                        else {
                                //set Site-Vars
                                $tmpl = str_replace( array_keys( $vars ), array_values( $vars ), $tmpl );
                                
                                //remove unused vars
                                return preg_replace( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
                        }
                }
        
                /**
                 * @param $bk_id
                 * @return string
                 */
                public function getPageTitleFromBook ($bk_id ) {
                        
                        $medium = new \Drupal\publisso_gold\Controller\Book( $bk_id );
                        return (string)t( 'Book' ) . ': ' . $medium->getElement( 'title' );
                }
        
                /**
                 * @param $bk_id
                 * @param $cp_id
                 * @return string
                 */
                public function getPageTitleFromBookChapter ($bk_id, $cp_id ) {
                        
                        $medium = new \Drupal\publisso_gold\Controller\Book( $bk_id );
                        $submedium = new \Drupal\publisso_gold\Controller\Bookchapter( $cp_id );
                        return (string)t( 'Book' ) . ': ' . $medium->getElement( 'title' ) . ' - ' . (string)t( 'Chapter' ) . ': ' . $submedium->getElement( 'title' );
                }
        
                /**
                 * @param $bk_id
                 * @return string
                 */
                public function getPageTitleFromBookChapterPreview ($bk_id ) {
                        
                        $medium = new \Drupal\publisso_gold\Controller\Book( $bk_id );
                        return (string)t( 'Book' ) . ': ' . $medium->getElement( 'title' ) . ' - ' . (string)t( 'Chapter' ) . ': ' . t( 'Chapterpreview' );
                }
                
                public function setSessionDataFromPost () {
                        
                        if ( array_key_exists( 'data', $_REQUEST ) ) {
                                $_SESSION[ 'data' ][ $_REQUEST[ 'store_data_control_key' ] ] = $_REQUEST[ 'data' ];
                        }
                        else {
                                $_SESSION[ 'data' ][ $_REQUEST[ 'store_data_control_key' ] ] = $_REQUEST;
                        }
                        echo print_r( $_REQUEST, 1 );
                        exit();
                }
        
                /**
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function MailSpool () {
                        
                        $session = \Drupal::service( 'session' );
                        if ( $this->force_redirect && $this->force_redirect != 'publisso_gold.mailspool' ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        $form = [];
                        
                        if ( !\Drupal::database()->select( 'rwPubgoldMailout', 't' )->fields( 't', [] )->condition( 'ready_to_send', 0, '=' )->condition( 'created_by_uid', $session->get( 'user' )[ 'id' ] )->isNull( 'autosend_parent' )->countQuery()->execute()->fetchField() ) {
                                drupal_set_message( t( 'There are no unsent Mails you initiated' ) );
                        }
                        else {
                                $form = \Drupal::formBuilder()->getForm( "\\Drupal\\" . $this->modname . "\Form\publisso_goldMailspool" );
                        }
                        
                        $this->renderVars();
                        return [ '#type' => 'markup', '#markup' => $this->tmpl, 'content' => [ $form ], '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param Request $request
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function BookSearch (Request $request ) {
                        
                        if ( $this->force_redirect ) {
                                $rd = $this->force_redirect;
                                $this->force_redirect = null;
                                return $this->redirect( $rd );
                        }
                        /**
                         *put content here
                         */
                        
                        $term = $request->query->has( 'keys' ) ? $request->query->get( 'keys' ) : '';
                        $bk_id = $request->query->has( 'bk_id' ) ? $request->query->get( 'bk_id' ) : '';
                        
                        //search in title, abstract and chapter_text
                        
                        if ( $term && $bk_id ) {
                                
                                $sql = "
                                SELECT * FROM {rwPubgoldBookChapters} WHERE
                                (`cp_title` LIKE '%$term%' OR
                                FROM_BASE64(`cp_abstract`) LIKE '%$term%' or
                                FROM_BASE64(`cp_chapter_text`) LIKE '%$term%') AND
                                `cp_bkid` = $bk_id
                        ";
                                
                                $result = \Drupal::database()->query( $sql )->fetchAll();
                                
                                foreach ( $result as $res ) {
                                        
                                        $url = Url::fromRoute( 'publisso_gold.book.chapter', [ 'bk_id' => $bk_id, 'cp_id' => $res->cp_id ] );
                                        $this->tmpl .= Link::fromTextAndUrl( $res->cp_title, $url )->toString() . '<br>';
                                }
                        }
                        
                        $this->renderVars();
                        return [
                                '#type'     => 'markup', '#markup' => $this->tmpl, /*'content' => array(
                                $this->login_form
                        )*/
                                '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        
                /**
                 * @param $wf_id
                 * @return array
                 */
                public function workflow_review ($wf_id ) {

                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $wf_id );
                        $medium = $workflow->getMedium();

                        if ( !$workflow->getDataElement( 'state' ) == 'in review' ) {
                                return \Drupal::service( 'publisso_gold.tools' )->accessDenied( $this->texts->get( 'workflow.item.wrong_state', 'fc' ) );
                        }
                        
                        if ( !$workflow->isReviewer( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) ) {
                                return \Drupal::service( 'publisso_gold.tools' )->accessDenied( $this->texts->get( 'workflow.item.user_not_reviewer', 'fc' ) );
                        }
                        
                        $form = null;

                        if ( $medium->getControlElement( 'review_change_origtext' ) == 1 ) {
                                
                                if ( $workflow->getLock() === false ) {
                                        
                                        $form = [ '#markup' => (string)t( $this->texts->get( 'workflow.item.lock.failed', 'fc' ) ) ];
                                }
                                else {
                                        $form = \Drupal::formBuilder()->getForm( "\\Drupal\\" . $this->modname . "\Form\ReviewItem", [ 'wf_id' => $wf_id ] );
                                }
                        }

                        if ( $form === null ) $form = \Drupal::formBuilder()->getForm( "\\Drupal\\" . $this->modname . "\Form\ReviewItem", [ 'wf_id' => $wf_id ] );


                        return [ 'title' => [ '#markup' => (string)t( $this->texts->get( 'workflow.item.review.title', 'fc' ), [ '@item_title' => $workflow->getDataElement( 'title' ), '@medium_title' => $medium->getElement( 'title' ) ] ), '#prefix' => '<h1>', '#suffix' => '</h1>' ], 'form' => $form, '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
                }
        
                /**
                 * @param $id
                 * @return \Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function deleteRunningSubmission($id){
                        
                        if($this->session->get('user')['id']) {
                                \Drupal::database()->delete('rwPubgoldBookchapter_temp')->condition('id', $id, '=')->condition('uid', $this->session->get('user')['id'], '=')->execute();
                                return $this->redirect( 'publisso_gold.dashboard', [ 'action' => 'myrunnings' ] );
                        }
                        else {
                                return $this->redirect( 'publisso_gold.books' );
                        }
                }
        
                /**
                 * @param $wf_id
                 * @return string
                 */
                public function workflow_review_title ($wf_id ) {
                        
                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $wf_id );
                        $medium = $workflow->getMedium();
                        return (string)t( $this->texts->get( 'workflow.item.review.title', 'fc' ), [ '@item_title' => $workflow->getDataElement( 'title' ), '@medium_title' => $medium->getElement( 'title' ) ] );
                }
                
                private function getModuleAccessPermissions () {
                        
                        $sql = "SELECT * FROM {rwPubgoldModuleaccess}";
                        $result = db_query( $sql );
                        $result = $result->fetchAll();
                        
                        foreach ( $result as $res ) {
                                $this->modaccessweights[ $res->mod_name ] = $res->mod_minaccessweight;
                        }
                }
        
                /**
                 * @param $modname
                 * @return bool
                 */
                private function userHasAccessRight ($modname ) {
                        
                        if ( !array_key_exists( $modname, $this->modaccessweights ) ) {
                                return false;
                        }
                        
                        return $this->modaccessweights[ $modname ] <= \Drupal::service( 'session' )->get( 'user' )[ 'weight' ];
                }
                
#                private function accessDenied () {
#                        return [ '#type' => 'markup', '#markup' => self::__ACCESS_DENIED__, '#attached' => [
# 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ] ];
#                }
                
                /**
                 * @return string[]
                 */
                public function notFound(){

                        return [
                                '#markup' => (string)$this->t('Something went wrong. The requested content could not be found. If you believe that the URL you provided is correct, please contact Support.'),
                                '#prefix' => '<div class="alert alert-danger">',
                                '#suffix' => '</div>'
                        ];
                }
                
                /**
                 * @param null $msg
                 * @return string[]
                 */
                public function accessDenied($msg = null){
        
                        $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
                        $msg = $msg ?? $tempstore->get('reasonAccessDenied');
                        $tempstore->delete('reasonAccessDenied');
                        
                        return [
                                '#markup' => (string)$this->t($msg ?? 'Something went wrong. You are not authorized to access this content. If you think, this is an error, please contact support.'),
                                '#prefix' => '<div class="alert alert-danger">',
                                '#suffix' => '</div>'
                        ];
                }
        }

?>


