<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\Blob.
 */

namespace Drupal\publisso_gold\Controller;

use Drupal\Core\Url;
        
        /**
         * Class Blob
         * @package Drupal\publisso_gold\Controller
         */
        class Blob {
    
        private $id;
        
                /**
                 * @return string
                 */
                public function __toString(){
                return "class blob";
        }
        
                /**
                 * Blob constructor.
                 * @param null $id
                 */
                public function __construct($id = null){
            
                $this->id = null;
                $this->data = 0x0;
                $this->meta = (object)[];
                $this->type = null;
                $this->hash = null;
                $this->hash_meta = null;
                
                if($id) $this->load($id);
        }
        
                /**
                 * @param $data
                 * @param $type
                 * @param $meta
                 * @return null
                 * @throws \Exception
                 */
                public function create($data, $type, $meta){
                
                if(is_array($meta)) $meta = serialize($meta);
                if(is_object($meta)) $meta = serialize((array)$meta);
                
                $md5_data = md5($data);
                $md5_meta = md5($meta);
                
                $id = \Drupal::database()->select('rwPubgoldBlobs', 't')->fields('t', ['id'])->condition('hash', $md5_data, '=')->condition('hash_meta', $md5_meta, '=')->execute()->fetchField();
                
                if($id){
                        return $this->load($id);
                }
                else{
                        $this->id = \Drupal::database()->insert('rwPubgoldBlobs')->fields(['data' => $data, 'type' => $type, 'meta' => $meta, 'hash' => $md5_data, 'hash_meta' => $md5_meta])->execute();
                        return $this->load($this->id);
                }
        }
        
                /**
                 * @param $meta
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 */
                public function setMeta($meta){
        
                if(is_array($meta)) $meta = serialize($meta);
                if(is_object($meta)) $meta = serialize((array)$meta);
                $md5_meta = md5($meta);
        
                $this->meta = unserialize($meta);
                
                return \Drupal::database()->update('rwPubgoldBlobs')
                                          ->fields([
                                                  'meta' => $meta,
                                                  'hash_meta' =>$md5_meta
                                                   ])
                                          ->condition('id', $this->id, '=')
                                          ->execute();
        }
        
                /**
                 * @param $meta
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 */
                public function updateMeta($meta){
                return $this->setMeta($meta);
        }
        
                /**
                 * @param $id
                 * @return null
                 */
                public function load($id){
                
                $res = \Drupal::database()->select('rwPubgoldBlobs', 't')->fields('t', [])->condition('id', $id, '=')->execute()->fetchAssoc();
        
                $this->data = $res['data'];
                $this->type = $res['type'];
                $this->meta = unserialize($res['meta']);
                $this->hash = $res['hash'];
                $this->hash_meta = $res['hash_meta'];
                $this->id = $id;
        
                if(!$this->hash      && $this->id) \Drupal::database()->update('rwPubgoldBlobs')->fields(['hash'      => md5($this->data)])->condition('id', $this->id, '=')->execute();
                if(!$this->hash_meta && $this->id) \Drupal::database()->update('rwPubgoldBlobs')->fields(['hash_meta' => md5($this->meta)])->condition('id', $this->id, '=')->execute();
        
                if(!empty($res['data'])) {
                        
                        if ( preg_match( '/file:\/\/(.+)/', $res[ 'data' ], $matches ) ) {
                                
                                if(empty($this->meta->mime)){
                                        if($type = mime_content_type($matches[1])){
                                                $this->meta['mime'] = $type;
                                                $this->updateMeta($this->meta);
                                        }
                                }
                                
                                $res[ 'data' ] = file_get_contents( $matches[ 1 ] );
                        }
                        else {
                                $blobDir = \Drupal::service( 'publisso_gold.tools' )->getVarDirAbsolute();
                                $blobDir = "$blobDir/blob/".date('Y-m', time());
                
                                if ( !is_dir( $blobDir ) ) {
                                        mkdir( $blobDir, 0750, true );
                                }
                
                                if ( is_dir( $blobDir ) ) {
                                        $filename = time() . '_' . md5( $res[ 'data' ] ) . '-DBID'.$res['id'] .'.blob';
                        
                                        if ( file_put_contents( "$blobDir/$filename", $res[ 'data' ] ) ) {
                                                \Drupal::database()->update( 'rwPubgoldBlobs' )->fields( [ 'data' => 'file://' . "$blobDir/$filename" ] )->condition( 'id', $res[ 'id' ], '=' )->execute();
                                        }
                                }
                        }
                        
                        $this->data = $res['data'];
                }
                
                return $this->id;
        }
        
                /**
                 * @param $id
                 */
                public function delete($id){
        
                $file = \Drupal::database()->select('rwPubgoldBlobs', 't')->fields('t', ['data'])->condition('id', $this->id, '=')->execute()->fetchField();
                
                if ( preg_match( '/file:\/\/(.+)/', $file, $matches ) ) {
                        unlink( $matches[ 1 ]);
                }
                
                \Drupal::database()->delete('rwPubgoldBlobs')->condition('id', $id ?? $this->id, '=')->execute();
        }
        
                /**
                 * @return null
                 */
                public function getId(){
                return $this->id;
        }
        
                /**
                 * @return string
                 */
                public function getMD5(){
                return md5($this->data);
        }
        
                /**
                 * @return string
                 */
                public function getSHA256(){
                return hash('sha256', $this->data);
        }
        
                /**
                 * @param $content
                 * @return bool
                 */
                public function setContent($content){
                
                if(!$this->id){
                        $this->data = $content;
                        return true;
                }
                
                $file = \Drupal::database()->select('rwPubgoldBlobs', 't')->fields('t', ['data'])->condition('id', $this->id, '=')->execute()->fetchField();
                
                if ( preg_match( '/file:\/\/(.+)/', $file, $matches ) ) {
                        
                        file_put_contents($matches[1], $content);
        
                        if($type = mime_content_type($matches[1])){
                                $this->meta['mime'] = $type;
                                $this->updateMeta($this->meta);
                        }
                }
                else{
                        \Drupal::database()->update('rwPubgoldBlobs')->fields(['data' => $content])->condition('id', $this->id, '=')->execute();
                        $this->data = $content;
                }
                
                return true;
        }
        
        public function getUrl(array $options = []) :Url{
                
                return Url::fromRoute('publisso_gold.getFile', ['fid' => $this->getId() ?? 0], $options);
        }
}

?>
