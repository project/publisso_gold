<?php
        
        
        namespace Drupal\publisso_gold\Controller;
        
        use \Drupal;
        use Drupal\Core\Access\AccessResult;
        use Drupal\Core\Routing\Access\AccessInterface;
        use Drupal\Core\Routing\RouteMatchInterface;
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Symfony\Component\HttpFoundation\Session\Session;
        use Symfony\Component\Routing\Route;

        /**
         * Class PublissoAccess
         * @package Drupal\publisso_gold\Controller
         */
        class PublissoAccess implements AccessInterface {
                
                private $elevatedRights;
        
                /**
                 * @param Route $route
                 * @param RouteMatchInterface $routeMatch
                 * @return Drupal\Core\Access\AccessResultForbidden
                 */
                public function access(Route $route, RouteMatchInterface $routeMatch){
                        
                        $routeName = explode('.', $routeMatch->getRouteName());
                        array_shift($routeName);
                        $method = implode('_', $routeName);
                        
                        $this->elevatedRights = !!(Publisso::currentUser()->isAdministrator() || Publisso::currentUser()->isPublisso());
                        
                        try{
                                return $this->$method(\Drupal::service('session'), $route, $routeMatch);
                        }
                        catch(\Error $e){
                                error_log('ERROR: '.$e->getMessage());
                                return AccessResult::forbidden();
                        }
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function conference_submit(Session $session, $route, $routeMatch){
        
                        $user = $session->get('user');
                        if(!$user) return AccessResult::forbidden();
                        
                        if(null === ($cf_id = $routeMatch->getParameter('param'))) return AccessResult::forbidden();
                        
                        $conference = ConferenceManager::getConference($cf_id);
                        $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
                        $tempstore->set('reasonAccessDenied', 'No abstracts may be submitted for this conference at this time.');
                        if(!$conference->submissionPossible()) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function authorscontracts(){
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('authorscontracts') ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function authorscontracts_add(){
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('authorscontracts') ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function authorscontracts_delete(){
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('authorscontracts.delete') ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function siteadmin(Session $session){
                        
                        $user = $session->get('user');
                        if(!$user) return AccessResult::forbidden();
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('administration') ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function siteadmin_tools_remap_references(Session $session){
                        return Publisso::currentUser()->isAdministrator() ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function usermenu(Session $session){
                        return $session->has('user') ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function usermenu_management(Session $session){
                        
                        if(!$session->get('logged_in')) return AccessResult::forbidden();
                        
                        $user = $session->get('user');
                        
                        $userHasAccess = $user['isEditor'] | $user['isEiC'] | $user['isEO'];
                
                        if($user['weight'] > 20 || $userHasAccess || $session->has('orig_user')) {
                                return AccessResult::allowed();
                        }
                
                        return AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function dashboard(Session $session){
                        return $session->has('user') ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function dashboard_abstracts(Session $session){
                        
                        return count(array_unique(array_merge(
                                ConferenceManager::getConferencesForUserRole('manager'),
                                ConferenceManager::getConferencesForUserRole('eo'),
                                ConferenceManager::getConferencesForUserRole('eic'),
                                ConferenceManager::getConferencesForUserRole('editor'),
                                ConferenceManager::getConferencesForUserRole('reviewer'),
                        ))) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function userprofile(Session $session){
                        return $session->has('user') ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function userprofile_add_affiliations(Session $session){
                        return $session->has('user') ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function userprofile_delete(Session $session){
                        return $session->has('user') ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function userprofile_chpw(Session $session){
                        return $session->has('user') ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function userprofile_edit(Session $session){
                        return $session->has('user') ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function directsubmit(Session $session){
                        return $session->has('user') ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function management(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        
                        $user = $session->get('user');
                        $user_has_rights = $user['isEditor'] | $user['isEiC'] | $user['isEO'];
                        
                        foreach(['book', 'journal', 'conference'] as $_){
                                $var = "user_has_".$_."management_rights";
                                $$var = $user['is'.ucfirst($_).'Editor'] | $user['is'.ucfirst($_).'EiC'] | $user['is'.ucfirst($_).'EO'];
                        }
                        
                        if($user['weight'] > 20 || $user_has_rights || $session->has('orig_user')) {
                                return AccessResult::allowed();
                        }
                        
                        return AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function useridentity(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return preg_match('/^1|2$/', $user['role_id']) || $session->has('orig_user') ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function usermanagement(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('usermanagement', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function usermanagement_user_delete_confirm(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('usermanagement', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function usermanagement_user_delete(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('usermanagement', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function usermanagement_filter(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('usermanagement', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function usermanagement_user(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('usermanagement', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function usermanagement_adduser(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('usermanagement', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function usermanagement_adduserpreview(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('usermanagement', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function bookmanagement(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        
                        $user_has_rights = $user['isEditor'] | $user['isEiC'] | $user['isEO'];
                        
                        foreach(['book'] as $_){
                                $user_has_bookmanagement_rights = $user['is'.ucfirst($_).'Editor'] | $user['is'.ucfirst($_).'EiC'] | $user['is'.ucfirst($_).'EO'];
                        }
                        
                        if(!(\Drupal::service('publisso_gold.tools')->userHasAccessRight('bookmanagement', $user['weight']) || $user_has_bookmanagement_rights)) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function bookmanagement_add(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        
                        if(!(\Drupal::service('publisso_gold.tools')->userHasAccessRight('bookmanagement', $user['weight']))) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function book_edit(Session $session, $route, $routeMatch){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
        
                        if(null === ($bk_id = $routeMatch->getParameter('bk_id'))) return AccessResult::forbidden();
        
                        $book = new \Drupal\publisso_gold\Controller\Medium\Book($bk_id);
                        
                        if($user['weight'] < 70 && !$book->isUserEditorialOffice($user['id'])) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function bookmanagement_book_delete(Session $session, $route, $routeMatch){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        
                        if($user['weight'] < 70) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function book_invite_user(Session $session, $route, $routeMatch){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        
                        if(null === ($bk_id = $routeMatch->getParameter('bk_id'))) return AccessResult::forbidden();
                        
                        $book = new \Drupal\publisso_gold\Controller\Medium\Book($bk_id);
                        
                        if(!($book->isUserEditor ($user['id']) || $book->isUserEditorInChief ($user['id']) || $book->isUserEditorialOffice ($user['id']) || $user['weight'] >= 70)) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function book_invited_users(Session $session, $route, $routeMatch){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        
                        if(null === ($bk_id = $routeMatch->getParameter('bk_id'))) return AccessResult::forbidden();
                        
                        $book = new \Drupal\publisso_gold\Controller\Medium\Book($bk_id);
                        
                        if(!($book->isUserEditorInChief ($user['id']) || $book->isUserEditorialOffice ($user['id']) || $user['weight'] >= 70)) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function bookmanagement_book_roles(Session $session, $route, $routeMatch){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        $bk_id = $routeMatch->getParameter('bk_id');
                        $book = new \Drupal\publisso_gold\Controller\Medium\Book($bk_id);
                        
                        if(!($book->isUserEditor ($user['id']) || $book->isUserEditorInChief ($user['id']) || $book->isUserEditorialOffice ($user['id']) || $user['weight'] >= 70)) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function bookmanagement_seteditorialboard(Session $session, $route, $routeMatch){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        $bk_id = $routeMatch->getParameter('bk_id');
                        $book = new \Drupal\publisso_gold\Controller\Medium\Book($bk_id);
                        
                        if(!\Drupal::service('publisso_gold.tools')->userHasAccessRight('bookmanagement', $user['weight'])) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function journalmanagement(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        
                        $user_has_rights = $user['isEditor'] | $user['isEiC'] | $user['isEO'];
                        
                        foreach(['journal'] as $_){
                                $user_has_journalmanagement_rights = $user['is'.ucfirst($_).'Editor'] | $user['is'.ucfirst($_).'EiC'] | $user['is'.ucfirst($_).'EO'];
                        }
                        
                        if(!(\Drupal::service('publisso_gold.tools')->userHasAccessRight('bookmanagement', $user['weight']) || $user_has_journalmanagement_rights)) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function edit_journal(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        
                        $user_has_rights = $user['isEditor'] | $user['isEiC'] | $user['isEO'];
                        
                        foreach(['journal'] as $_){
                                $user_has_journalmanagement_rights = $user['is'.ucfirst($_).'Editor'] | $user['is'.ucfirst($_).'EiC'] | $user['is'.ucfirst($_).'EO'];
                        }
                        
                        if(!(\Drupal::service('publisso_gold.tools')->userHasAccessRight('bookmanagement', $user['weight']) || $user_has_journalmanagement_rights)) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function journalmanagement_volume_add(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        
                        $user_has_rights = $user['isEditor'] | $user['isEiC'] | $user['isEO'];
                        
                        foreach(['journal'] as $_){
                                $user_has_journalmanagement_rights = $user['is'.ucfirst($_).'Editor'] | $user['is'.ucfirst($_).'EiC'] | $user['is'.ucfirst($_).'EO'];
                        }
                        
                        if(!(\Drupal::service('publisso_gold.tools')->userHasAccessRight('bookmanagement', $user['weight']) || $user_has_journalmanagement_rights)) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function journalmanagement_issue_add(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        
                        $user_has_rights = $user['isEditor'] | $user['isEiC'] | $user['isEO'];
                        
                        foreach(['journal'] as $_){
                                $user_has_journalmanagement_rights = $user['is'.ucfirst($_).'Editor'] | $user['is'.ucfirst($_).'EiC'] | $user['is'.ucfirst($_).'EO'];
                        }
                        
                        if(!(\Drupal::service('publisso_gold.tools')->userHasAccessRight('bookmanagement', $user['weight']) || $user_has_journalmanagement_rights)) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function journal_management_roles_assign(Session $session){
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        
                        $user_has_rights = $user['isEditor'] | $user['isEiC'] | $user['isEO'];
                        
                        foreach(['journal'] as $_){
                                $user_has_journalmanagement_rights = $user['is'.ucfirst($_).'Editor'] | $user['is'.ucfirst($_).'EiC'] | $user['is'.ucfirst($_).'EO'];
                        }
                        
                        if(!(\Drupal::service('publisso_gold.tools')->userHasAccessRight('bookmanagement', $user['weight']) || $user_has_journalmanagement_rights)) return AccessResult::forbidden();
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function conferencemanagement(Session $session, $route, $routeMatch){
                        
                        $action = $routeMatch->getParameter('action');
                        $cf_id = null;
                        
                        if(preg_match('/(.+)\|(\d+)/', $action, $matches)){
                                
                                $action = $matches[1];
                                $cf_id = $matches[2];
                        }
                        
                        $param = $routeMatch->getParameter('param');
                        
                        //für eine Übersicht der Konferenzen
                        if(($action == 'overview' && ($this->elevatedRights || ConferenceManager::isManagerOfAnyConference())) ||
                           ($action == 'list'     && ($this->elevatedRights || ConferenceManager::isManagerOfAnyConference()))){
                                return AccessResult::allowed();
                        }
                        elseif($action == 'complete' && ($this->elevatedRights || ConferenceManager::isManagerForConference($cf_id))){
                                return AccessResult::allowed();
                        }
                        //für alle anderen Aktionen
                        elseif($this->elevatedRights){
                                return AccessResult::allowed();
                        }
                        
                        return AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function conferencemanagement_overview(Session $session, $route, $routeMatch){
                
                        //für eine Übersicht der Konferenzen
                        if(!(\Drupal::service('publisso_gold.tools')->userHasAccessRight('conferencemanagement.overview', $session->get('user')['weight']) || ConferenceManager::isManagerOfAnyConference())){
                                return AccessResult::forbidden();
                        }
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function conferencemanagement_abstracts_overview(Session $session, $route, $routeMatch){
                        
                        $cf_id = $routeMatch->getParameter('cf_id') ?? 0;
                        
                        //für eine Übersicht der Abstracts
                        if(!(\Drupal::service('publisso_gold.tools')->userHasAccessRight('conferencemanagement.overview', $session->get('user')['weight']) || ConferenceManager::isManagerForConference($cf_id))){
                                return AccessResult::forbidden();
                        }
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function conferences_import(Session $session, $route, $routeMatch){
                
                        $cf_id = $routeMatch->getParameter('cf_id') ?? 0;
                
                        if(!( Publisso::currentUser()->isAdministrator() || Publisso::currentUser()->isPublisso() || ConferenceManager::isManagerForConference($cf_id))){
                                return AccessResult::forbidden();
                        }
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function reviewsheets(Session $session, $route, $routeMatch){
                        
                        $action = $routeMatch->getParameter('action');
                        $param = $routeMatch->getParameter('param');
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('reviewpagemgmt', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function reviewsheet(Session $session, $route, $routeMatch){
                        
                        $action = $routeMatch->getParameter('action');
                        $param = $routeMatch->getParameter('param');
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('reviewpagemgmt', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function table_of_content(Session $session, $route, $routeMatch){
                        
                        $action = $routeMatch->getParameter('action');
                        $param = $routeMatch->getParameter('param');
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('tocmgmt', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function table_of_content_add(Session $session, $route, $routeMatch){
                        
                        $action = $routeMatch->getParameter('action');
                        $param = $routeMatch->getParameter('param');
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('tocmgmt', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function table_of_content_edit(Session $session, $route, $routeMatch){
                        
                        $action = $routeMatch->getParameter('action');
                        $param = $routeMatch->getParameter('param');
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('tocmgmt', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function table_of_content_delete(Session $session, $route, $routeMatch){
                        
                        $action = $routeMatch->getParameter('action');
                        $param = $routeMatch->getParameter('param');
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('tocmgmt', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function table_of_content_item_add(Session $session, $route, $routeMatch){
                        
                        $action = $routeMatch->getParameter('action');
                        $param = $routeMatch->getParameter('param');
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('tocmgmt', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function table_of_content_item_edit(Session $session, $route, $routeMatch){
                        
                        $action = $routeMatch->getParameter('action');
                        $param = $routeMatch->getParameter('param');
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('tocmgmt', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function table_of_content_item_delete(Session $session, $route, $routeMatch){
                        
                        $action = $routeMatch->getParameter('action');
                        $param = $routeMatch->getParameter('param');
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight('tocmgmt', $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function privacy_protection(Session $session, $route, $routeMatch){
                        
                        $action = $routeMatch->getParameter('action');
                        
                        if(preg_match('/^(.+)\|(.+)/', $action, $matches)){
                                $action = $matches[1];
                        }
                        
                        if(!$session->has('user')) $user = ['weight' => 0];
                        else $user = $session->get('user');
                        
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight("privacyprotection.$action", $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function newsletter(Session $session, $route, $routeMatch){
                        
                        $action = $routeMatch->getParameter('action');
                        $param = $routeMatch->getParameter('param');
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight("newsletter.$action", $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function references(Session $session, $route, $routeMatch){
                        
                        $action = $routeMatch->getParameter('action');
                        $param = $routeMatch->getParameter('param');
                        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        $user = $session->get('user');
                        return \Drupal::service('publisso_gold.tools')->userHasAccessRight("referencemgmt", $user['weight']) ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function book_addchapter(Session $session, $route, $routeMatch){ //deprecated (alter workflow)
        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        
                        $parameters = $routeMatch->getParameters();
                        if(!$parameters->has('bk_id')){
                                Publisso::tools()->logMsg('ERROR: No book-id (bk_id) provided!');
                                return AccessResult::forbidden();
                        }
                        
                        if(!BookManager::bookExists($parameters->get('bk_id'))){
                                Publisso::tools()->logMsg('ERROR: No book (bk_id) found!');
                                return AccessResult::forbidden();
                        }
                        
                        $book = BookManager::getBook($parameters->get('bk_id'));
                        
                        if($book->countAuthors() && !preg_match('/^1|2$/', \Drupal::service('session')->get('user')['role_id'])){
                                if(!$book->isUserAuthor()) return AccessResult::forbidden();
                        }
                        
                        return AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function book_template(Session $session, $route, $routeMatch){
        
                        if(!$session->has('user')) return AccessResult::forbidden();
                        
                        $parameters = $routeMatch->getParameters();
                        if(!$parameters->has('id')){
                                Publisso::tools()->logMsg('ERROR: No template-id (bk_id) provided!');
                                return AccessResult::forbidden();
                        }
                        
                        $template = new BookchapterTemplate($route, $parameters->get('id'));
                        $book = BookManager::getBook($template->getElement('bk_id'));
                        
                        if(in_array($parameters->get('action'), ['fill', 'references'])) {

                                if ( $book->countAuthors() && !preg_match( '/^1|2$/', \Drupal::service( 'session' )->get( 'user' )[ 'role_id' ] ) ) {
                                        if ( !$book->isUserAuthor() ) return AccessResult::forbidden();
                                }
        
                                return AccessResult::allowed();
                        }
                        Publisso::log('Action for book.template not covered!');
                        return AccessResult::forbidden();
                }
        
                /**
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function sso_soap_request(){
                        
                        $config         = \Drupal::configFactory()->getEditable('publisso_sso.config');
                        $allowedIPs     = array_filter(array_map("trim", explode(PHP_EOL, $config->get('allowed_ip'))));
                        $remoteIP       = \Drupal::requestStack()->getCurrentRequest()->server->get('REMOTE_ADDR');
                        $accessSecret   = $config->get('access_secret');
                        
                        //allow local server
                        $allowedIPs[] = gethostbyname(gethostname());
                        
                        return in_array($remoteIP, $allowedIPs) || (!empty($accessSecret) && $accessSecret == \Drupal::requestStack()->getCurrentRequest()->get('access_secret'))
                                ? AccessResult::allowed()
                                : AccessResult::forbidden();
                }
        
                /**
                 * @param $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function workflow_item($session, $route, $routeMatch){
                        $wf_id = $routeMatch->getParameters()->get('wf_id');
                        $qry = \Drupal::database()->select('rwPubgoldWorkflow', 'w')->fields('w', [])->condition('wf_id', $wf_id, '=');
                        $wf = $qry->execute()->fetchAssoc();
        
                        $assigned_uids = [];
                        foreach(array_filter(array_map("trim", explode(',', $wf['wf_assigned_to']))) as $_){
                                if(preg_match('/^u:(\d+)$/', $_, $matches)){
                                        $assigned_uids[] = $matches[1];
                                }
                        }
        
                        $locked = !!$wf['wf_locked'];
        
                        //ein zugeordneter User darf generell rein, sonst erstmal keiner
                        $access = \Drupal::service('session')->get('user')['id'] == in_array(\Drupal::service('session')->get('user')['id'], $assigned_uids);
        
                        //für administrative Rollen (PUBLISSO, Administrator), wenn nicht gesperrt
                        if(!$locked && \Drupal::service('session')->get('user')['weight'] >= 70) $access = true;
        
                        //wenn geperrt, muss die Sperre durch mich sein
                        if($locked) $access = $wf['wf_locked_by_uid'] == \Drupal::service('session')->get('user')['id'];
        
                        //wenn das Item in Review ist, muss es auch mir zugeordnet sein
                        if($wf['wf_state'] == 'in review') {
                                $access = in_array( \Drupal::service('session')->get( 'user' )[ 'id' ], $assigned_uids );
                        }
                        
                        return $access ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function workflow_running_submission_delete($session, $route, $routeMatch){
                        
                        return !!\Drupal::database()->select('rwPubgoldWorkflow', 't')->fields('t', [])
                                ->condition('wf_assigned_to', 'u:'.$session->get('user')['id'], '=')
                                ->condition('wf_state', 'running submission', '=')
                                ->countQuery()->execute()->fetchField() ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function workflow_review(Session $session, $route, $routeMatch){
                        if(null === ($wf_id = $routeMatch->getParameter('wf_id'))) return AccessResult::forbidden();
                        $workflow = Drupal\publisso_gold\Controller\Manager\WorkflowManager::getItem($wf_id);
                        return $workflow->userIsAssigned() ? AccessResult::allowed() : AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function workflow_abstract_comments(Session $session, $route, $routeMatch){
                        
                        if(null === ($wf_id = $routeMatch->getParameter('wf_id'))) return AccessResult::forbidden();
                        $workflow = Drupal\publisso_gold\Controller\Manager\WorkflowManager::getItem($wf_id);
                        $user = Publisso::currentUser();
                        return $user->isAnonymous() ? AccessResult::forbidden() : AccessResult::allowed();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function workflow_adapt(Session $session, $route, $routeMatch){
                        if(null === ($wf_id = $routeMatch->getParameter('wf_id'))) return AccessResult::forbidden();
                        $currentUser = new User($session->get('user')['id']);
                        $workflow = Drupal\publisso_gold\Controller\Manager\WorkflowManager::getItem($wf_id);
                        if($workflow->getElement('state') == 'published' && $currentUser->isAdministrator()) return AccessResult::allowed();
                        elseif($workflow->getElement('state') != 'published' && ($currentUser->isPublisso() || $currentUser->isAdministrator())) return AccessResult::allowed();
                        elseif($workflow->getElement('state') == 'accepted' && $workflow->userIsAssigned()) return AccessResult::allowed();
                        else return AccessResult::forbidden();
                }
        
                /**
                 * @param Session $session
                 * @param $route
                 * @param $routeMatch
                 * @return Drupal\Core\Access\AccessResultAllowed|Drupal\Core\Access\AccessResultForbidden
                 */
                private function workflow_edit(Session $session, $route, $routeMatch){
                        if(null === ($wf_id = $routeMatch->getParameter('wf_id'))) return AccessResult::forbidden();
                        $currentUser = new User($session->get('user')['id']);
                        $workflow = Drupal\publisso_gold\Controller\Manager\WorkflowManager::getItem($wf_id);
                        
                        if($workflow->getElement('state') == 'published' && $currentUser->isAdministrator()) return AccessResult::allowed();
                        elseif($workflow->getElement('state') != 'published' && ($currentUser->isPublisso() || $currentUser->isAdministrator())) return AccessResult::allowed();
                        elseif(preg_match('/accepted|in revision/', $workflow->getElement('state'))  && $workflow->userIsAssigned()) return AccessResult::allowed();
                        else return AccessResult::forbidden();
                }
        }
        
        
        /*
                namespace Drupal\publisso_gold\Controller;
        
        
                use Drupal\Core\Access\AccessResult;
                use Drupal\Core\Routing\Access\AccessInterface;
                use Drupal\Core\Routing\RouteMatchInterface;
                use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
                use Symfony\Component\HttpFoundation\Session\Session;
                use Symfony\Component\Routing\Route;
        
                class PublissoAccess implements AccessInterface {
                        
                        public function accessDenied(){
                                return AccessResult::forbidden();
                        }
                        
                        public function access(){
                                
                                $routeName = explode('.', \Drupal::routeMatch()->getRouteName());
                                array_shift($routeName);
                                $method = implode('_', $routeName);
                                
                                try{
                                        return $this->$method();
                                }
                                catch(\Error $e){
                                        error_log('ERROR in '.$e->getFile().' Line '.$e->getLine().': '.$e->getMessage().' -- URL called: '.\Drupal::requestStack()->getCurrentRequest()->getRequestUri());
                                        return AccessResult::forbidden();
                                }
                        }
                        
                        private function siteadmin(){
                                return self::userHasAccess('administration') ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                        
                        private function usermenu(){
                                return Publisso::currentUser()->isAnonymous() ? AccessResult::forbidden() : AccessResult::allowed();
                        }
                        
                        private function dashboard(){
                                return Publisso::currentUser()->isAnonymous() ? AccessResult::forbidden() : AccessResult::allowed();
                        }
                        
                        private function userprofile(){
                                return Publisso::currentUser()->isAnonymous() ? AccessResult::forbidden() : AccessResult::allowed();
                        }
                
                        private function userprofile_delete(){
                                return Publisso::currentUser()->isAnonymous() ? AccessResult::forbidden() : AccessResult::allowed();
                        }
                
                        private function userprofile_chpw(){
                                return Publisso::currentUser()->isAnonymous() ? AccessResult::forbidden() : AccessResult::allowed();
                        }
                
                        private function userprofile_edit(){
                                return Publisso::currentUser()->isAnonymous() ? AccessResult::forbidden() : AccessResult::allowed();
                        }
                
                        private function directsubmit(){
                                return Publisso::currentUser()->isAnonymous() ? AccessResult::forbidden() : AccessResult::allowed();
                        }
                
                        private function usermenu_management(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                
                                $user = Publisso::currentUser();
                                $userHasAccess = $user->isEditor() | $user->isEiC() | $user->isEO();
                
                                if($user->getWeight() > 20 || $userHasAccess || $user->isOverridden()) {
                                        return AccessResult::allowed();
                                }
                                
                                return AccessResult::forbidden();
                        }
                        
                        private function useridentity(){
                                return preg_match('/^1|2$/', Publisso::currentUser()->getRoleElement('id')) || Publisso::currentUser()->isOverridden() ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                        
                        private function usermanagement(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess('usermanagement') ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function usermanagement_user_delete_confirm(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess('usermanagement') ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function usermanagement_user_delete(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess('usermanagement') ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function usermanagement_filter(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess('usermanagement') ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function usermanagement_user(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess('usermanagement') ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function usermanagement_adduser(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess('usermanagement') ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function usermanagement_adduserpreview(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess('usermanagement') ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                        
                        private function bookmanagement(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                $user = Publisso::currentUser();
                                $userHasAccess = $user->isBookEO() | $user->isBookEiC() | $user->isBookEditor();
                                if(!(self::userHasAccess('bookmanagement') || $userHasAccess)) return AccessResult::forbidden();
                                
                                return AccessResult::allowed();
                        }
                
                        private function bookmanagement_add(){
                                return self::userHasAccess('bookmanagement') ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                        
                        private function book_edit(){
                                if(Publisso::currentUser()->getWeight() < 70) return AccessResult::forbidden();
                                return AccessResult::allowed();
                        }
                
                        private function bookmanagement_book_delete(){
                                if(Publisso::currentUser()->getWeight() < 70) return AccessResult::forbidden();
                                return AccessResult::allowed();
                        }
                
                        private function book_invite_user(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                $user = Publisso::currentUser();
                                if(null === ($bk_id = \Drupal::routeMatch()->getParameter('bk_id'))) return AccessResult::forbidden();
                                $book = new \Drupal\publisso_gold\Controller\Medium\Book($bk_id);
                                if(!($book->isUserEditorInChief ($user->getId()) || $book->isUserEditorialOffice ($user->getId()) || $user->getWeight() >= 70)) return AccessResult::forbidden();
                                return AccessResult::allowed();
                        }
                
                        private function book_invited_users(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                $user = Publisso::currentUser();
                                if(null === ($bk_id = \Drupal::routeMatch()->getParameter('bk_id'))) return AccessResult::forbidden();
                                $book = new \Drupal\publisso_gold\Controller\Medium\Book($bk_id);
                                if(!($book->isUserEditorInChief ($user->getId()) || $book->isUserEditorialOffice ($user->getId()) || $user->getWeight() >= 70)) return AccessResult::forbidden();
                                return AccessResult::allowed();
                        }
                        
                        private function bookmanagement_book_roles(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                $bk_id = \Drupal::routeMatch()->getParameter('bk_id');
                                $book = new \Drupal\publisso_gold\Controller\Medium\Book($bk_id);
                                $user = Publisso::currentUser();
                                if(!($book->isUserEditor ($user->getId()) || $book->isUserEditorInChief ($user->getId()) || $book->isUserEditorialOffice ($user->getId()) || $user->getWeight() >= 70)) return AccessResult::forbidden();
                                
                                return AccessResult::allowed();
                        }
                
                        private function bookmanagement_seteditorialboard(){
                        
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                $user = Publisso::currentUser();
                                $userHasAccess = $user->isBookEditor() | $user->isBookEiC() | $user->isBookEO();
                                if(!(self::userHasAccess('bookmanagement') || $userHasAccess)) return AccessResult::forbidden();
                                return AccessResult::allowed();
                        }
                        
                        private function journalmanagement(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                $user = Publisso::currentUser();
                                $userHasAccess = $user->isJournalEditor() | $user->isJournalEiC() | $user->isJournalEO();
                                if(!(self::userHasAccess('journalmanagement') || $userHasAccess)) return AccessResult::forbidden();
                                return AccessResult::allowed();
                        }
                
                        private function edit_journal(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                $user = Publisso::currentUser();
                                $userHasAccess = $user->isJournalEditor() | $user->isJournalEiC() | $user->isJournalEO();
                                if(!(self::userHasAccess('journalmanagement') || $userHasAccess)) return AccessResult::forbidden();
                                return AccessResult::allowed();
                        }
                
                        private function journalmanagement_volume_add(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                $user = Publisso::currentUser();
                                $userHasAccess = $user->isJournalEditor() | $user->isJournalEiC() | $user->isJournalEO();
                                if(!(self::userHasAccess('journalmanagement') || $userHasAccess)) return AccessResult::forbidden();
                                return AccessResult::allowed();
                        }
                
                        private function journalmanagement_issue_add(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                $user = Publisso::currentUser();
                                $userHasAccess = $user->isJournalEditor() | $user->isJournalEiC() | $user->isJournalEO();
                                if(!(self::userHasAccess('journalmanagement') || $userHasAccess)) return AccessResult::forbidden();
                                return AccessResult::allowed();
                        }
                
                        private function journal_management_roles_assign(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                $user = Publisso::currentUser();
                                $userHasAccess = $user->isJournalEditor() | $user->isJournalEiC() | $user->isJournalEO();
                                if(!(self::userHasAccess('journalmanagement') || $userHasAccess)) return AccessResult::forbidden();
                                return AccessResult::allowed();
                        }
                
                        private function conferencemanagement(){
                                
                                $action = \Drupal::routeMatch()->getParameter('action');
                                
                                //für eine Übersicht der Konferenzen
                                if($action == 'overview' && (!(self::userHasAccess("conferencemanagement.overview")) || ConferenceManager::isManagerOfAnyConference())) return AccessResult::forbidden();
                                elseif($action == 'list' && (!(self::userHasAccess("conferencemanagement.list")) || ConferenceManager::isManagerOfAnyConference())) return AccessResult::forbidden();
                                //für alle anderen Aktionen
                                elseif(!(self::userHasAccess("conferencemanagement.add"))) return AccessResult::forbidden();
                                
                                return AccessResult::allowed();
                        }
                
                        private function reviewsheets(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess("reviewpagemgmt") ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function reviewsheet(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess("reviewpagemgmt") ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function table_of_content(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess("tocmgmt") ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function table_of_content_add(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess("tocmgmt") ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function table_of_content_edit(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess("tocmgmt") ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function table_of_content_delete(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess("tocmgmt") ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function table_of_content_item_add(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess("tocmgmt") ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function table_of_content_item_edit(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess("tocmgmt") ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function table_of_content_item_delete(){
                
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess("tocmgmt") ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function privacy_protection(){
                        
                                $action = \Drupal::routeMatch()->getParameter('action');
                                if(preg_match('/^(.+)\|(.+)/', $action, $matches)) $action = $matches[1];
                                error_log($action . ' -- ' . Publisso::currentUser()->getWeight());
                                return self::userHasAccess("privacyprotection.$action") ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function newsletter(){
                        
                                $action = \Drupal::routeMatch()->getParameter('action');
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess("newsletter.$action") ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        private function references(){
                        
                                if(Publisso::currentUser()->isAnonymous()) return AccessResult::forbidden();
                                return self::userHasAccess("referencemgmt") ? AccessResult::allowed() : AccessResult::forbidden();
                        }
                
                        public static function userHasAccess(string $module, User $user = null){
                        
                                if(null === $user) $user = Publisso::currentUser();
                                return \Drupal::database()->select('rwPubgoldModuleaccess', 't')->fields('t', [])->execute()->fetchAllKeyed(1, 3)[$module] <= $user->getWeight();
                        }
                }
*/
