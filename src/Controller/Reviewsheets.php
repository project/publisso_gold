<?php

	/**
	 * @file
	 * Contains \Drupal\publisso_gold\Controller\Reviewsheets.
	 */

	namespace Drupal\publisso_gold\Controller;
	use Drupal\Core\Url;
	use Drupal\Core\Link;
	use Drupal\Core\Controller\ControllerBase;
	use Drupal\publisso_gold\Controller\Template;
	use Drupal\publisso_gold\Controller\UserTable;
	use Drupal\Component\Render\FormattableMarkup;
	use Symfony\Component\HttpFoundation\JsonResponse;
        
        /**
         * Class Reviewsheets
         * @package Drupal\publisso_gold\Controller
         */
        class Reviewsheets extends ControllerBase{
		
                private $modname;
                private $session;
                private $setup;
                private $tools;
                private $route;
                private $texts;
                private $baseTable;
                private $fieldPrefix;
                private $pages;
                
                /**
                 * @return string
                 */
                public function __toString(){
			return 'Reviewsheets';
		}
		
		public function __construct(){
                        
                        \Drupal::service('publisso_gold.tools')->forceUserAction();
                        
                        $this->modname     = 'reviewpagemgmt';
                        $this->session     = \Drupal::service('session');
                        $this->setup       = \Drupal::service('publisso_gold.setup');
                        $this->tools       = \Drupal::service('publisso_gold.tools');
                        $this->texts       = \Drupal::service('publisso_gold.texts');
                        $this->baseTable   = 'rwPubgoldReviewPage';
                        $this->fieldPrefix = 'rp_';
                        $this->pageIDs     = [];
                        $this->pages       = [];
                        
                        $this->route['name'] = \Drupal::routeMatch()->getRouteName();
                        $this->route['param'] = \Drupal::routeMatch()->getRawParameters()->all();
                        
                        $this->loadReviewsheets();
		}
                
                /**
                 * @param $action
                 * @return mixed
                 */
                public function main($action){
                        
                        \Drupal::service('page_cache_kill_switch')->trigger();
                        
                        return $this->$action();
		}
		
		private function list(){
                        
                        $table = [
                                '#type'  => 'table',
                                '#empty' => (string)t($this->texts->get('reviewsheets.list.empty', 'fc')),
                                '#header' => [
                                        [
                                                'data' => (string)t($this->texts->get('reviewsheets.list.id', 'ac')),
                                                'field' => 'id'
                                        ],
                                        [
                                                'data' => (string)t($this->texts->get('reviewsheets.list.name', 'fc')),
                                                'field' => 'name'
                                        ],
                                        [
                                                'data' =>(string)t($this->texts->get('reviewsheets.list.description', 'fc')),
                                                'field' => 'desc'
                                        ],
                                        [
                                                'data' => (string)t($this->texts->get('reviewsheets.list.actions', 'fc')),
                                                'width' => '110'
                                        ]
                                ]
                        ];
                        $rows = [];
                        foreach($this->pageIDs as $id){
                                
                                $rp = new Reviewsheet($id);
                                
                                if(empty($rp->getElement('sheet'))) continue;
                                
                                $doc = new \DOMDocument();
                                $doc->loadXML($rp->getElement('sheet'));
                                $xpath = new \DOMXpath($doc);
                                $rows[] = [
                                        [
                                                'data'  => [
                                                        '#markup' => $rp->getElement('id')
                                                ],
                                                'field' => 'id'
                                        ],
                                        [
                                                'data' => [
                                                        '#markup' => $rp->getElement('name')
                                                ],
                                                'field' => 'name'
                                        ],
                                        [
                                                'data' => [
                                                        '#markup' => $xpath->evaluate('/reviewsheet/meta/description')[0]->nodeValue
                                                ],
                                                'field' => 'desc'
                                        ],
                                        [
                                                'data' => [
                                                        '#type' => 'dropbutton',
                                                        '#links' => [
                                                                'edit' => [
                                                                        'title' => (string)t($this->texts->get('reviewsheets.list.edit')),
                                                                        'url' => Url::fromRoute('publisso_gold.reviewsheet', ['action' => 'edit|'.$rp->getElement('id')])
                                                                ],
                                                                'copy' => [
                                                                        'title' => (string)t($this->texts->get('reviewsheets.list.copy')),
                                                                        'url' => Url::fromRoute('publisso_gold.reviewsheet', ['action' => 'copy|'.$rp->getElement('id')])
                                                                ],
                                                                'delete' => [
                                                                        'title' => (string)t($this->texts->get('reviewsheets.list.delete')),
                                                                        'url' => Url::fromRoute('publisso_gold.reviewsheet', ['action' => 'remove|'.$rp->getElement('id')]),
                                                                        '#attributes' => [
                                                                                'class' => ['use-ajax'],
                                                                                'data-dialog-type' => 'modal'
                                                                        ]
                                                                ]
                                                        ]
                                                ]
                                        ]
                                ];
                                /*
                                $rows[]['data'] = [
                                        'id'      => ['#markup' => $rp->getElement('id')],
                                        'name'    => ['#markup' => $rp->getElement('name')],
                                        'desc'    => ['#markup' => $xpath->evaluate('/reviewsheet/meta/description')[0]->nodeValue],
                                        'actions' => [
                                                '#type' => 'markup',
                                                '#markup' => 'Hallo'
                                        ]
                                ];
                                */
                        }
                        $table['#rows'] = $rows;
                        $lnkAdd = [
                                '#type'       => 'link',
                                '#url'        => Url::fromRoute('publisso_gold.reviewsheet', ['action' => 'add']),
                                '#title'      => (string)t($this->texts->get('reviewsheets.list.button.add', 'fc')),
                                '#attributes' => [
                                        'class' => 'btn btn-warning'
                                ],
                                '#suffix'     => '<br><br>'
                        ];
                        
                        return [
                                $table,
                                $lnkAdd,
                                '#cache' => [
                                        'max-age' => 0
                                ]
                        ];
		}
                
                /**
                 * @return bool
                 */
                public function loadReviewsheets(){
                        
		        $dbh = \Drupal::database()->select($this->baseTable, 't')->fields('t', [$this->fieldPrefix.'id', $this->fieldPrefix.'name']);
		        
		        foreach($dbh->condition($this->fieldPrefix.'active', 1, '=')->execute()->fetchAllAssoc($this->fieldPrefix.'id') as $id => $page){
		                $this->pages[$id] = $page->{$this->fieldPrefix.'name'};
		                $this->pageIDs[] = $id;
		        }
		        
                        return true;
		}
                
                /**
                 * @param $action
                 * @return mixed
                 */
                public function title($action){
                        return $this->texts->get('reviewsheets.route.action.'.$action, 'afc');
		}
                
                /**
                 * @return array
                 */
                public function pageIDs(){
		        return array_keys($this->pages);
		}
                
                /**
                 * @return array
                 */
                public function pageNames(){
		        return array_values($this->pages);
		}
                
                /**
                 * @return array
                 */
                public function getPageIDsNames(){
		        return $this->pages;
		}
                
                /**
                 * @return array
                 */
                public function getSheets(){
		        
		        $sheets = [];
		        
		        foreach($this->pageIDs as $id){
		                $sheets[] = new Reviewsheet($id);
		        }
		        
		        return $sheets;
		}
                
                /**
                 * @param int $id
                 * @return Reviewsheet
                 */
                public function getSheet(int $id){
		        return new Reviewsheet($id);
		}
	}
?>
