<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\PublissoGoldSettings.
 */

namespace Drupal\publisso_gold\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class PublissoGoldSettings
 * @package Drupal\publisso_gold\Controller
 */
class PublissoGoldSettings extends ControllerBase {
        
        /**
         * @return array
         */
        public function content() {
		
		return array(
			'#type' => 'markup',
			'#markup' => $this->t('<pre>'.print_r(array_keys(\Drupal::service('user.permissions')->getPermissions()), 1).'</pre>'),
		);
	}
}
?>