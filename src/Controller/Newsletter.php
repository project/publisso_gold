<?php

	/**
	 * @file
	 * Contains \Drupal\publisso_gold\Controller\Newsletter.
	 */

	namespace Drupal\publisso_gold\Controller;
	use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\StringTranslation\TranslationManager;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Drupal\Core\Database\Connection;
	use Symfony\Component\HttpFoundation\Session\Session;
	use Drupal\Core\Url;
	use Drupal\Core\Link;
	use Symfony\Component\HttpFoundation\Request;
	
	use \Drupal\publisso_gold\Controller\Texts;
	use \Drupal\publisso_gold\Controller\Tools;
        
        /**
         * Class Newsletter
         * @package Drupal\publisso_gold\Controller
         */
        class Newsletter extends ControllerBase{
		
                private $modname;
                private $session;
                private $route;
                private $texts;
		private $tools;
		private $ReflectionClass;
                
                /**
                 * @return string
                 */
                public function __toString(){
			return $this->ReflectionClass->getName();
		}
		
		/**
		 * {@inheritdoc}
		 */
		public static function create(ContainerInterface $container){
                        return new static(
                                $container->get('session'),
                                $container->get('publisso_gold.texts'),
                                $container->get('publisso_gold.tools')
                        );
		}
		
		public function __construct(Session $session, Texts $texts, Tools $tools){
                        
                        \Drupal::service('publisso_gold.tools')->forceUserAction();
                        
                        $this->ReflectionClass = new \ReflectionClass($this);
                        
                        $this->modname = strtolower($this->ReflectionClass->getShortName());
                        $this->session = $session;
                        $this->texts = $texts;
                        $this->tools = $tools;
                        $this->route['name'] = \Drupal::routeMatch()->getRouteName();
                        $this->route['param'] = \Drupal::routeMatch()->getRawParameters()->all();
		}
                
                /**
                 * @param $action
                 * @return array|void
                 */
                public function main($action){
                        
                        //Zugriffsrechte prüfen
                        if(!$this->tools->userHasAccessRight($this->modname.'.'.$action, $this->session->get('user')['weight'])){
                                return $this->tools->accessDenied();
                        }
                        
                        try{
                                return $this->$action();
                        }
                        catch(\Drupal\Core\Database\DatabaseException $e){
                                
                                $m = \Drupal::service('messenger');
                                error_log("Error in file >>".$e->getFile()."<< Line ".$e->getLine().": ".$e->getMessage());
                                $m->addError((string)t(\Drupal::service('publisso_gold.texts')->get('global.message.system.error', 'fc')), 'error');
                                return [];
                        }
                        catch(\InvalidArgumentException $e){
                                
                                $m = \Drupal::service('messenger');
                                error_log("Error in file >>".$e->getFile()."<< Line ".$e->getLine().": ".$e->getMessage());
                                $m->addError((string)t(\Drupal::service('publisso_gold.texts')->get('global.message.system.error', 'fc')), 'error');
                                return [];
                        }
                        catch(\Error $e){
                                
                                $m = \Drupal::service('messenger');
                                error_log("Error in file >>".$e->getFile()."<< Line ".$e->getLine().": ".$e->getMessage());
                                $m->addError((string)t(\Drupal::service('publisso_gold.texts')->get('global.message.system.error', 'fc')), 'error');
                                return [];
                        }
		}
                
                /**
                 * @param $action
                 * @return string
                 */
                public function _title($action){
                        return (string)$this->t($this->texts->get('newsletter.page.title.'.$action, 'fc'));
		}
		
		private function list(){
                        
                        $newsletters = \Drupal::database()->select('rwPubgoldNewsletter', 't')
                                ->fields('t', [])->execute()->fetchAll();
                        
                        $ret = [
                                'headline' => [
                                        '#markup' => (string)$this->t($this->texts->get('newsletter.list.headline', 'fc')),
                                        '#prefix' => '<h1>',
                                        '#suffix' => '</h1>'
                                ],
                                'data' => [
                                        '#type' => 'table',
                                        '#header' => [
                                                'date'       => (string)$this->t($this->texts->get('newsletter.list.date', 'fc')),
                                                'creator'    => (string)$this->t($this->texts->get('newsletter.list.created_by', 'fc')),
                                                'subject'    => (string)$this->t($this->texts->get('newsletter.list.subject', 'fc')),
                                                'recipients' => (string)$this->t($this->texts->get('newsletter.list.recipients', 'fc')),
                                                'body'       => (string)$this->t($this->texts->get('newsletter.list.body', 'fc')),
                                        ],
                                        '#empty' => (string)$this->t($this->texts->get('newsletter.list.empty', 'fc')),
                                ],
                                'add' => [
                                        '#type'  => 'link',
                                        '#url'   => Url::fromRoute($this->route['name'], ['action' => 'add']),
                                        '#title' => (string)$this->t($this->texts->get('newsletter.list.lnk.add', 'fc')),
                                        '#attributes' => [
                                                'class' => [
                                                        'btn',
                                                        'btn-warning'
                                                ]
                                        ],
                                        '#suffix' => '<br><br>'
                                ]
                        ];
                        
                        foreach($newsletters as $_){
                                
                                $user = new \Drupal\publisso_gold\Controller\User($_->created_by_uid);
                                
                                $ret['data'][] = [
                                        
                                        'sent' => [
                                                '#markup' => $_->created
                                        ],
                                        'creator' => [
                                                '#markup' => $user->profile->getReadableName()
                                        ],
                                        'subject' => [
                                                '#markup' => $_->subject
                                        ],
                                        'recipients' => [
                                                '#type'         => 'textarea',
                                                '#value'        => implode("; ", unserialize($_->recipient)),
                                                '#attributes'   => [
                                                        'readonly' => 'readonly',
                                                        'disabled' => 'disabled'
                                                ],
                                                '#rows'         => 3
                                        ],
                                        'body' => [
                                                '#markup' => nl2br($_->body)
                                        ],
                                ];
                        }
                        
                        return $ret;
		}
                
                /**
                 * @return array
                 */
                private function add(){
                        
                        $ret = [
                                'headline' => [
                                        '#markup' => (string)$this->t($this->texts->get('newsletter.add.headline', 'fc')),
                                        '#prefix' => '<h1>',
                                        '#suffix' => '</h1>'
                                ],
                                'form' => \Drupal::formBuilder()->getForm("Drupal\publisso_gold\Form\addNewsletter")
                        ];
                        
                        return $ret;
		}
        }
?>
