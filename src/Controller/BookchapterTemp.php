<?php
        
        /**
         * @file
         * Contains \Drupal\publisso_gold\Controller\BookchapterTemp.
         */
        
        namespace Drupal\publisso_gold\Controller;
        
        /**
         * Class BookchapterTemp
         * @package Drupal\publisso_gold\Controller
         */
        class BookchapterTemp extends Bookchapter {
                
                private $id;
                private $elements          = [];
                private $tbl_column_prefix = 'cp_';
        
                /**
                 * @return string
                 */
                public function __toString () {
                        return "class BookchapterTemp";
                }
        
                /**
                 * BookchapterTemp constructor.
                 * @param null $FuncParam__data
                 */
                public function __construct ($FuncParam__data = null ) {
        
                        $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
                        $FuncParam__data = unserialize($tempstore->get('preview_bookchapter'));
                        
                        $this->id = 0;
                        
                        foreach ( $FuncParam__data as $k => $v ) {
                                $this->elements[ $k ] = $v;
                        }
                        
                        $this->elements[ 'abstract' ] = base64_encode( $this->getElement( 'abstract' ) );
                        $this->elements[ 'chapter_text' ] = base64_encode( $this->getElement( 'chapter_text' ) );
                        $this->elements[ 'keywords' ] = json_encode( array_map( 'trim', explode( ';', $this->elements[ 'keywords' ] ) ) );
                        
                        $weight = 0;
                        $authors = [];
                        
                        foreach ( $FuncParam__data[ 'author_db' ] as $_ ) {
                                
                                list( $author, $data ) = preg_split( '/;\s{1,}/', $_->author, 2 );
                                
                                if ( !empty( $author ) ) {
                                        $res = \Drupal::database()->select( 'rwPubgoldUserProfiles', 't' )->fields( 't', [] )->where( "CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [ ':sep' => ' ', ':author' => $author ] )->execute()->fetchAll();
                                        $found = 0;
                                        $last_id = null;
                                        $string = '';
                                        
                                        foreach ( $res as $v ) {
                                                $string .= ' || ' . implode( '; ', array_filter( [ implode( ' ', array_filter( [ $v->up_firstname, $v->up_lastname ] ) ), $v->up_department, $v->up_institute, getCountry( $v->up_country ) ] ) ) . ' | ' . implode( '; ', [ $author, $data ] );
                                                if (
                                                        implode( '; ', [ $author, $data ] )
                                                        ==
                                                        implode( '; ', array_filter( [ implode( ' ', array_filter( [ $v->up_firstname, $v->up_lastname ] ) ), $v->up_department, $v->up_institute, getCountry( $v->up_country ) ] ) )
                                                ) {
                                                        $found++;
                                                        $last_id = $v->up_uid;
                                                }
                                        }
                                        
                                        if ( $found == 1 ) {
                                                $author = new \Drupal\publisso_gold\Controller\User( $last_id );
                                                
                                                $authors[] = [
                                                        'firstname'        => $author->profile->getElement( 'firstname' ),
                                                        'lastname'         => $author->profile->getElement( 'lastname' ),
                                                        'affiliation'      => $data,
                                                        'uid'              => $author->getElement( 'id' ),
                                                        'is_corresponding' => $_->is - corresponding ? true : false,
                                                        'weight'           => $_->weight,
                                                ];
                                        }
                                }
                        }
                        
                        foreach ( $FuncParam__data[ 'author_add' ] as $_ ) {
                                
                                if ( !( empty( $_->firstname ) || empty( $_->lastname ) || empty( $_->affiliation ) ) ) {
                                        
                                        $authors[] = [
                                                'firstname'        => $_->firstname,
                                                'lastname'         => $_->lastname,
                                                'affiliation'      => $_->affiliation,
                                                'uid'              => null,
                                                'is_corresponding' => $_->is - corresponding ? true : false,
                                                'weight'           => $_->weight,
                                        ];
                                }
                        }
                        
                        $this->elements[ 'authors' ] = json_encode( $authors );
                        $this->elements[ 'corporation' ] = json_encode( $FuncParam__data[ 'corporation' ] );
                }
        
                /**
                 * @param Workflow $workflow
                 */
                public function loadWorkflow (\Drupal\publisso_gold\Controller\Workflow &$workflow ) {
                        
                        foreach ( $workflow->getDataKeys() as $key ) {
                                $this->elements[ $key ] = $workflow->getDataElement( $key );
                        }
                }
        
                /**
                 * @param $elemName
                 * @param null $elemValue
                 * @return bool
                 */
                public function setElement ($elemName, $elemValue = null ) {
                        
                        if ( array_key_exists( $elemName, $this->elements ) ) {
                                
                                $oldValue = $this->elements[ $elemName ];
                                $this->elements[ $elemName ] = $elemValue;
                        }
                        else {
                                return false;
                        }
                }
        
                /**
                 * @param $elemName
                 * @return false|int|mixed|null
                 */
                public function getElement ($elemName ) {
                        if ( $elemName == 'id' ) return $this->id;
                        return $this->elements[ $elemName ];
                }
        
                /**
                 * @return mixed
                 */
                public function readAuthors () {
                        
                        $authors = json_decode( $this->elements[ 'authors' ], true );
                        usort( $authors, [ 'self', 'sortAuthors' ] );
                        return $authors;
                }
        
                /**
                 * @param $a
                 * @param $b
                 * @return bool|int
                 */
                private static function sortAuthors ($a, $b ) {
                        if ( $a[ 'weight' ] == $b[ 'weight' ] )
                                return 0;
                        return $b[ 'weight' ] < $a[ 'weight' ];
                }
                
        }

?>
