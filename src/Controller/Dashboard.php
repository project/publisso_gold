<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\Dashboard.
 */

namespace Drupal\publisso_gold\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\publisso_gold\Controller\Manager\AbstractManager;
use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
use Drupal\publisso_gold\Controller\Manager\TemplateManager;
use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
use Drupal\publisso_gold\Controller\Medium\Book;
use Drupal\Core\Database\Connection;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\publisso_gold\Form\publisso_goldLogin;
use http\Client\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

//require_once(drupal_get_path('module', 'publisso_gold').'/inc/constants.inc.php') || die('Cant require Constants');

/**
 * Class Dashboard
 * @package Drupal\publisso_gold\Controller
 */
class Dashboard extends ControllerBase{
       
        private $tmpl = '';
        private $tmpl_vars = array();
        private $modpath;
        private $userrole = 'guest';
        private $user = 'guest';
        private $user_id = NULL;
        private $userweight = 10;
        private $login_form;
        private $route_name;
        private $modname = 'publisso_gold';
        private $database;
        private $force_redirect = NULL;
        private $force_redirect_data = [];
        private $page_title;
        private $session;
        private $texts;
        private $tools;
        protected $currentUser = NULL;
        
        /**
         * @param $action
         * @return string
         */
        public function _pageTitle($action){
                
                if(preg_match('/^(.+)\|(.+)$/', $action, $matches)){
                        $action = $matches[1];
                        $param = $matches[2];
                }
                
                $title = 'Dashboard';
                
                switch($action){
                        
                        case 'todo':
                                $title = 'Dashboard';
                                break;
        
                        case 'published':
                                $title = 'Published Chapters';
                                break;
        
                        case 'meeic':
                                $title = 'Submissions where I am EiC';
                                break;
        
                        case 'meeditor':
                                $title = 'Submissions where I am Editor';
                                break;
        
                        case 'mereviewer':
                                $title = 'Submissions where I am Reviewer';
                                break;
        
                        case 'myrunnings':
                                $title = 'My running submissions';
                                break;
        
                        case 'mysubmissions':
                                $title = 'My submissions';
                                break;
                                
                        case 'workflow':
                                $title = 'Workflow Item '.$param ?? '';
                                break;
        
                        case 'conferences':
                                $title = 'Conferences';
                                break;
                }
                
                return (string)$this->t($title);
        }
        
        /**
         * @param ContainerInterface $container
         * @return Dashboard|static
         */
        public static function create(ContainerInterface $container){
                
                #\Drupal::service('page_cache_kill_switch')->trigger();
                return new static($container->get('database'));
        }
    
        public function __construct(Connection $database){
                
                $this->session  = \Drupal::service('session');
                $this->texts    = \Drupal::service('publisso_gold.texts');
                $this->tools = \Drupal::service('publisso_gold.tools');
                
                //set various parameters
                $this->route_name = \Drupal::routeMatch()->getRouteName();
                $this->modpath = drupal_get_path('module', $this->modname);
                
                //if password change required and called path is not password-change-form -> redirect
                $unprivUser = $this->session->get('user')['weight'] < 70;
                
                if($this->session->has('orig_user')){
                        $orig_user = $this->session->get('orig_user');
                        $unprivUser = !!($unprivUser & $orig_user['weight'] < 70);
                }
                
                if($this->session->has('user') && $this->session->get('user')['pw_change'] == 1 && $unprivUser){
                        if($this->route_name != 'publisso_gold.change_password'){
                                $this->force_redirect = 'publisso_gold.change_password';
                                //$this->force_redirect = 'publisso_gold.userprofile.chpw';
                        }
                }
                
                $user = $this->session->get('user');
                
                $user['isBookEditor'] 		= \Drupal::service('publisso_gold.tools')->isUserBookEditor($this->session->get('user')['id']);
                $user['isBookEiC'] 			= \Drupal::service('publisso_gold.tools')->isUserBookEiC($this->session->get('user')['id']);
                $user['isBookEO'] 			= \Drupal::service('publisso_gold.tools')->isUserBookEO($this->session->get('user')['id']);

                $user['isJournalEditor'] 	        = \Drupal::service('publisso_gold.tools')->isUserJournalEditor($this->session->get('user')['id']);
                $user['isJournalEiC'] 		= \Drupal::service('publisso_gold.tools')->isUserJournalEiC($this->session->get('user')['id']);
                $user['isJournalEO'] 		= \Drupal::service('publisso_gold.tools')->isUserJournalEO($this->session->get('user')['id']);

                $user['isConferenceEditor'] = \Drupal::service('publisso_gold.tools')->isUserConferenceEditor($this->session->get('user')['id']);
                $user['isConferenceEiC'] 	= \Drupal::service('publisso_gold.tools')->isUserConferenceEiC($this->session->get('user')['id']);;
                $user['isConferenceEO'] 	= \Drupal::service('publisso_gold.tools')->isUserConferenceEO($this->session->get('user')['id']);;

                $user['isEditor'] 	= \Drupal::service('session')->get('user')['isBookEditor'] | \Drupal::service('session')->get('user')['isJournalEditor'] | \Drupal::service('session')->get('user')['isConferenceEditor'];
                $user['isEiC'] 		= \Drupal::service('session')->get('user')['isBookEiC'] | \Drupal::service('session')->get('user')['isJournalEiC'] | \Drupal::service('session')->get('user')['isConferenceEiC'];
                $user['isEO'] 		= \Drupal::service('session')->get('user')['isBookEO'] | \Drupal::service('session')->get('user')['isJournalEO'] | \Drupal::service('session')->get('user')['isConferenceEO'];
		
		$this->session->set('user', $user);
		
		//if profile not edited and called path is not userprofile-edit -> redirect
                if($this->session->get('user')['id']){
                        
                        $this->session->set('current_user', new \Drupal\publisso_gold\Controller\User($this->session->get('user')['id']));
                        
                        if($this->session->get('current_user')->profile->getElement('edited') == -1 && $this->route_name != 'publisso_gold.userprofile_edit'){
                                $this->force_redirect = 'publisso_gold.userprofile_edit';
                        }
                }
                
                if(array_key_exists('form_id', $_REQUEST)){ //Altlast?
                
                        switch($_REQUEST['form_id']){
                        
                                case 'publisso_goldsearch':
                                        $this->force_redirect = 'publisso_gold.search';
                                        $_SESSION['page']['search'] = $_REQUEST['keys'];
                                        break;
                        }
                }

                //create login-section
                if($_SESSION['logged_in'] === false){
                        $this->login_form = \Drupal::formBuilder()->getForm("Drupal\\".$this->modname."\Form\publisso_goldLogin");
                }
                else{
                        $this->login_form = \Drupal::formBuilder()->getForm("Drupal\\".$this->modname."\Form\publisso_goldLogout");
                }
        }
        
        /**
         * @param $action
         * @return array
         */
        public function main($action) {
        
                \Drupal::service('tempstore.private')->get('publisso_gold.dashboard')->delete('dashboardPage');
                \Drupal::cache('menu')->invalidateAll(); // for clearing the menu cache
                \Drupal::service('plugin.manager.menu.link')->rebuild(); // rebuild the menu
                #if($this->force_redirect) { $rd = $this->force_redirect; $this->force_redirect = NULL; return $this->redirect($rd); }
                
                $param = null;
                
                if(preg_match('/^(.+)\|(.+)$/', $action, $matches)){
                        $action = $matches[1];
                        $param = $matches[2];
                }
                
                unset($_SESSION['action']);
                
                $header = '<div id="dashboard-categories">';
                
                //active Actions
                $btn_class = $action == 'todo' ? 'warning' : 'default';
                $url = Url::fromRoute('publisso_gold.dashboard', ['action' => 'todo']);
                $url->setOptions(['attributes' => ['class' => ['btn-'.$btn_class, 'btn']]]);
                $link = LINK::fromTextAndUrl((string)t('ToDo\'s'), $url)->toString();
                $header .= $link;
                
                //published
                if(count(\Drupal::service('publisso_gold.tools')->getPublishedBookChapters_Workflow($this->session->get('user')['role_id']))){
                        $btn_class = $action == 'published' ? 'warning' : 'default';
                        $url = Url::fromRoute('publisso_gold.dashboard', ['action' => 'published']);
                        $url->setOptions(['attributes' => ['class' => ['btn-'.$btn_class, 'btn']]]);
                        $link = LINK::fromTextAndUrl((string)t('Published chapters'), $url)->toString();
                        $header .= $link;
                }
                
                //i am eic
                if(count(\Drupal::service('publisso_gold.tools')->getMyEiCSubmissions($this->session->get('user')['id']))){
                        $btn_class = $action == 'meeic' ? 'warning' : 'default';
                        $url = Url::fromRoute('publisso_gold.dashboard', ['action' => 'meeic']);
                        $url->setOptions(['attributes' => ['class' => ['btn-'.$btn_class, 'btn']]]);
                        $link = LINK::fromTextAndUrl((string)t('Submissions where I am EiC'), $url)->toString();
                        $header .= $link;
                }
                
                //i am editor
                if(count(\Drupal::service('publisso_gold.tools')->getMyEditorSubmissions($this->session->get('user')['id']))){
                        $btn_class = $action == 'meeditor' ? 'warning' : 'default';
                        $url = Url::fromRoute('publisso_gold.dashboard', ['action' => 'meeditor']);
                        $url->setOptions(['attributes' => ['class' => ['btn-'.$btn_class, 'btn']]]);
                        $link = LINK::fromTextAndUrl((string)t('Submissions where I am Editor'), $url)->toString();
                        $header .= $link;
                }
                
                //i am reviewer
                if(count(\Drupal::service('publisso_gold.tools')->getMyReviewerSubmissions($this->session->get('user')['id']))){
                        $btn_class = $action == 'mereviewer' ? 'warning' : 'default';
                        $url = Url::fromRoute('publisso_gold.dashboard', ['action' => 'mereviewer']);
                        $url->setOptions(['attributes' => ['class' => ['btn-'.$btn_class, 'btn']]]);
                        $link = LINK::fromTextAndUrl((string)t('Submissions where I am Reviewer'), $url)->toString();
                        $header .= $link;
                }
                
                if(count(array_merge(
                        ConferenceManager::getConferencesForUserRole('manager'),
                        ConferenceManager::getConferencesForUserRole('eo'),
                        ConferenceManager::getConferencesForUserRole('eic'),
                        ConferenceManager::getConferencesForUserRole('editor'),
                        ConferenceManager::getConferencesForUserRole('reviewer'),
                ))){
                        $btn_class = $action == 'conferences' ? 'warning' : 'default';
                        $url = Url::fromRoute('publisso_gold.dashboard', ['action' => 'conferences']);
                        $url->setOptions(['attributes' => ['class' => ['btn-'.$btn_class, 'btn']]]);
                        $link = LINK::fromTextAndUrl((string)t('Conferences'), $url)->toString();
                        $header .= $link;
                }
                
                //my running submissions
                if(count(array_merge(\Drupal::service('publisso_gold.tools')->getMyRunningSubmissions($this->session->get('user')['id']), WorkflowManager::getItems(['wf_id'], [['wf_created_by_uid', Publisso::currentUser()->getId()], ['wf_state', 'running submission']])))){
                        $btn_class = $action == 'myrunnings' ? 'warning' : 'default';
                        $url = Url::fromRoute('publisso_gold.dashboard', ['action' => 'myrunnings']);
                        $url->setOptions(['attributes' => ['class' => ['btn-'.$btn_class, 'btn']]]);
                        $link = LINK::fromTextAndUrl((string)t('My running submissions'), $url)->toString();
                        $header .= $link;
                }
                
                //my submissions
                if(count(\Drupal::service('publisso_gold.tools')->getMyWorkflowItems($this->session->get('user')['id']))){
                        $btn_class = $action == 'mysubmissions' ? 'warning' : 'default';
                        $url = Url::fromRoute('publisso_gold.dashboard', ['action' => 'mysubmissions']);
                        $url->setOptions(['attributes' => ['class' => ['btn-'.$btn_class, 'btn']]]);
                        $link = LINK::fromTextAndUrl((string)t('My submissions'), $url)->toString();
                        $header .= $link;
                }
                $header .= '</div><br><br>';

                $ret = [];
                
                try{
                        $ret =  $this->$action($param);
                }
                catch(\Error $e){
                
                        $m = \Drupal::service('messenger');
                        error_log("Error in file >>".$e->getFile()."<< Line ".$e->getLine().": ".$e->getMessage());
                        $m->addError((string)t(\Drupal::service('publisso_gold.texts')->get('global.message.system.error', 'fco')), 'error');
                }
                
                if(is_array($ret)) return $ret; //wird ein array zurückgegeben, dann direkte Ausgabe
                
                //Es wird ein Markup eingebettet - sollte es ein serialisiertes Array sein, dann unserialisiert einbetten
                return array(
                        'header' => [
                                '#markup' => $header,
                        ],
                        'content' => !@unserialize($ret) ? [
                                '#markup' => $ret.'<br><br><br>'
                        ] : unserialize($ret),
                        '#attached' => [
                                'library' => [
                                'publisso_gold/default'
                                ]
                        ],
                        '#cache' => [
                                'max-age' => 0
                        ]
                );


        }
        
        /**
         * @param $wf_id
         * @return array
         */
        public function workflow($wf_id){
                
                $previousUrl = null;
                $previousUrl = \Drupal::request()->server->get('HTTP_REFERER');
                $fakeRequest = \Symfony\Component\HttpFoundation\Request::create($previousUrl);
                if($fakeRequest->query->has('page')) \Drupal::service('tempstore.private')->get('publisso_gold')->set('dashboardPage', $fakeRequest->query->get('page') ?? null);
                if($fakeRequest->query->has('item')) \Drupal::service('tempstore.private')->get('publisso_gold')->set('dashboardItem', $fakeRequest->query->get('item') ?? null);
                
                $qry = \Drupal::database()->select('rwPubgoldWorkflow', 'w')->fields('w', [])->condition('wf_id', $wf_id, '=');
                $wf = $qry->execute()->fetchAssoc();
                
                $assigned_uids = [];
                foreach(array_filter(array_map("trim", explode(',', $wf['wf_assigned_to']))) as $_){
                        if(preg_match('/^u:(\d+)$/', $_, $matches)){
                                $assigned_uids[] = $matches[1];
                        }
                }
                
                $locked = !!$wf['wf_locked'];
                
                
                //prüfen der Zugriffsrechte
                
                //generell darf niemand rein
                $access = false;
                
                //ein zugeordneter User darf generell rein
                $access = $this->session->get('user')['id'] == in_array($this->session->get('user')['id'], $assigned_uids);
                
                //für administrative Rollen (PUBLISSO, Administrator), wenn nicht gesperrt
                if(!$locked && $this->session->get('user')['weight'] >= 70) $access = true;
                
                //wenn geperrt, muss die Sperre durch mich sein
                if($locked) $access = $wf['wf_locked_by_uid'] == $this->session->get('user')['id'];
                
                //wenn das Item in Review ist, muss es auch mir zugeordnet sein
                if($wf['wf_state'] == 'in review') {
                        $access = in_array( $this->session->get( 'user' )[ 'id' ], $assigned_uids );
                }
                
                if(!$access){
                        return $this->tools->accessDenied((string)$this->t('You don\'t have permissions to access this item!'));
                }
                
                if(null === ($workflow = $this->tools->getWorkflowItem($wf_id))){
                        return $this->tools->accessDenied((string)$this->t('Can\'t find item #@nr', ['@nr' => $wf_id]));
                }
                
                if(!$workflow->getLock()){
                        return $this->tools->accessDenied((string)$this->t('Can\'t getLock for item #@nr', ['@nr' => $wf_id]));
                }
                
                $elements = [
                        \Drupal::formBuilder()->getForm( "Drupal\\publisso_gold\\Form\\Workflow\\Checkin", [ 'wf_id' => $workflow->getElement('id') ] )
                ];
                
                foreach($workflow->getSchemaBoxes() as $box){
                        $args = ['hideonempty' => $box['box_hideonempty'], 'follower_overwrite' => $box['box_follower_overwrite']];
                        $elements[] = $workflow->renderSchemaBox($box, $args);
                }
                
                return $elements;
        }
        
        /**
         * @return string
         */
        public function todo(){
        
                $limit = 20;
                $filter = [];
        
                if(\Drupal::request()->query->has('item')){
                        $limit = null;
                        $filter[] = ['wf_id', \Drupal::request()->query->get('item')];
                }
                
                $user = \Drupal::service('session')->get('user');
                $where = "substring(wf_type, 1, 1) != 'c'"; //No Conference-Items
        
                if($user['weight'] < 70){ //not Publisso or Administrator
                        $where .= " AND (
                                FIND_IN_SET('u:".$user['id']."', `wf_assigned_to`)
                                OR
                                FIND_IN_SET('r:".$user['id']."', `wf_assigned_to`)
                                OR
                                (FIND_IN_SET('".$user['id']."', `wf_assigned_to_eo`) AND `wf_state` NOT IN('ready for publication', 'running submission'))
                        )";
                }
                
                $qry = \Drupal::database()->select('rwPubgoldWorkflow', 'w')->fields('w', ['wf_id']);
                
                if($limit)
                        $qry = $qry->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit($limit);
                
                $condition = $qry->andConditionGroup();
                if(!empty($where)) $condition->where($where);
                
                $condition->condition('wf_state', ['published', 'rejected', 'running submission'], 'NOT IN');
                
                foreach($filter as $_) $qry->condition($_[0], $_[1], $_[2] ?? '=');
                
                $qry->condition($condition);
                $result = $qry->execute();
                
                $markup = !$qry->countQuery()->execute()->fetchField() ? (string)t('No active items found') : '';
                
                foreach($result->fetchCol() as $wf_id){

                        $markup .= '<div id="rwDashboardItem-'.$wf_id.'"><a name="rwDashboardItem-'.$wf_id.'"></a>';
                        $markup .= !empty($markup) ? '<hr>' : '';
                        $workflow = new Workflow($wf_id);
                        $user = new User($workflow->getElement('created_by_uid'));
                        
                        switch(substr($workflow->getElement('type'), 0, 2)){
                                
                                case 'bc':
                                        $medium = new Book($workflow->getDataElement('bk_id'), true);
                                        break;
                                
                                case 'ja':
                                        $medium = new Journal($workflow->getDataElement('jrn_id'), true);
                                        break;
                                
                                case 'ca':
                                        $medium = AbstractManager::getAbstract($workflow->getDataElement('ca_id'));
                                        break;
                        }
                        
                        $markup .= '<strong>'.((string)t('[@ID] @title in @parent_title', ['@ID' => $workflow->getElement('id'),'@title' => $workflow->getDataElement('title'), '@parent_title' => $medium->getElement('title')])).'</strong><br>';
                        $markup .= ((string)t('Submitted by: @user', ['@user' => implode(', ', [$user->profile->getElement('lastname'), $user->profile->getElement('firstname')])])).'<br>';
                        
                        if(empty($workflow->getElement('schema_identifier')))
                                $markup .= ((string)t('Status: @status', ['@status' => $workflow->getDataElement('state')])).'<br><br>';
                        else
                                $markup .= ((string)t('Status: @status [#@identifier]', ['@status' => $workflow->getDataElement('state'), '@identifier' => $workflow->getElement('schema_identifier')])).'<br><br>';
                        
                        if(!$workflow->locked() || $workflow->getElement('locked_by_uid') == $this->session->get('current_user')->getElement('id')){
                                
                                if(empty($workflow->getElement('schema')))
                                        $markup .= LINK::fromTextAndUrl((string)t('Open item'), Url::fromRoute('publisso_gold.dashboard.checkout', ['wf_id' => $wf_id]))->toString();
                                else
                                        $markup .= LINK::fromTextAndUrl((string)t('Open item'), Url::fromRoute('publisso_gold.dashboard', ['action' => 'workflow|'.$wf_id]))->toString();
                                
                                if($workflow->getElement('locked_by_uid') == $this->session->get('current_user')->getElement('id')){
                                	$markup .= '&nbsp;&nbsp;('.((string)$this->t($this->texts->get('dashboard.toto.current_item.lock_info.current_user'))).')';
                                }
                        }
                        else{
                                $markup .= $this->lockInfo($workflow);
                                $markup .= $this->lockOverride($workflow);
                        }
                        
                        $markup .= $this->mailInfo($workflow);
                        $markup .= '</div>';
                }
        
                $filterID = [
                        '#type' => 'number',
                        '#min' => 1,
                        '#title' => (string)$this->t('Go to ID'),
                        '#attributes' => [
                                'style' => [
                                        'width: auto'
                                ],
                                'onchange' => [
                                        'javascript: this.value ? location.search = (location.search == "" ? "?" : "&") + "item=" + this.value : location.search = "";'
                                ]
                        ],
                        '#value' => \Drupal::request()->query->has('item') ? \Drupal::request()->query->get('item') : ''
                ];
                
                return serialize([
                        'pager_top' => [
                                '#type' => 'pager'
                        ],
                        $filterID,
                        'markup' => [
                                '#type' => 'markup',
                                '#markup' => $markup.'<br><br><br>'
                        ],
                        'pager_bottom' => [
                                '#type' => 'pager'
                        ]
                ]);
        }
        
        /**
         * @return string
         */
        public function myrunnings(){
                
                $list = \Drupal::service('publisso_gold.tools')->getMyRunningSubmissions(Publisso::currentUser()->getId());
                
                $template = new Template();
                $template->get('publisso_gold_running_submission_dashbord');
                $template->setVar('txt_created', ((string)t('Created')).': ');
                $template->setVar('txt_modified', ((string)t('Modified')).': ');
                $markup = '';
                
                foreach($list as $_){
                        
                        $markup .= !empty($markup) ? '<hr>' : '';
                        
                        
                        $item     = new RunningSubmission($_['id'], $_['type']);
                        
                        if(!$_['type']){
                                
                                $medium     = new Book($item->getElement('bk_id'));
                                $link = Url::fromRoute('publisso_gold.book.addchapter', ['bk_id' => $medium->getElement('id'), 'continue' => $item->getElement('id')]);
                                $link = Link::fromTextAndUrl($this->t('Continue editing'), $link);
                        }
                        else{
                                
                                switch($_['type']){
                                        
                                        case 'bookchapter':
                                                $medium = new Book($item->getElement('medium_id'));
                                                $link = Url::fromRoute('publisso_gold.book.addchapter', ['bk_id' => $medium->getElement('id'), 'continue' => 'bc|'.$item->getElement('id')]);
                                                $link = Link::fromTextAndUrl($this->t('Continue editing'), $link);
                                                break;
                                        
                                        case 'journalarticle':
                                                $medium = new Journal($item->getElement('medium_id'));
                                                $link = Url::fromRoute('publisso_gold.journal_addarticle', ['jrn_id' => $medium->getElement('id'), 'continue' => 'ja|'.$item->getElement('id')]);
                                                $link = Link::fromTextAndUrl($this->t('Continue editing'), $link);
                                                break;
                                }
                        }
                        
                        
                        $template->setVar('val_created', $item->getElement('created'));
                        $template->setVar('val_modified', $item->getElement('modified'));
                        $template->setVar('link', $link->toString());
                        
                        $link = Url::fromRoute('publisso_gold.running_submission.delete', ['id' => $_['id']]);
                        $link = Link::fromTextAndUrl($this->t('Delete'), $link);
                        $template->setVar('link.delete', $link->toString());
                        
                        $markup .= '<strong>'.((string)$this->t(
                        		'[@ID] @title in @parent_title', [
                        			'@ID' => $item->getElement('id'),
                        			'@title' => empty($item->getDataElement('title')) ? 'N.A.' : $item->getDataElement('title'),
                        			'@parent_title' => $medium->getElement('title')
                        		])).'</strong><br>';
                        $markup .= $template->parse();
                }
                
                //new stuff - extended workflow
                $template = TemplateManager::load('publisso_gold_running_submission_dashbord');
                $template->setVar('txt_created' , ((string)t('Created' )).': ');
                $template->setVar('txt_modified', ((string)t('Modified')).': ');
                foreach(WorkflowManager::getItems(['wf_id'], [['wf_created_by_uid', Publisso::currentUser()->getId()], ['wf_state', 'running submission']]) as $wf_id => $_){
                        $url = Url::fromRoute('publisso_gold.workflow.item', ['wf_id' => $wf_id]);
                        
                        if($url->access()) {
                                $workflow = WorkflowManager::getItem($wf_id);
                                $template->clear();
                                $markup .= !empty( $markup ) ? '<hr>' : '';
        
                                $template->setVar('val_created', $workflow->getElement('created'));
                                $template->setVar('val_modified', $workflow->getElement('modified'));
                                
                                $template->setVar('link', Link::fromTextAndUrl((string)$this->t('Continue editing'), $url)->toString());
        
                                $link = Url::fromRoute('publisso_gold.workflow.running_submission.delete', ['wf_id' => $wf_id]);
        
                                $template->setVar( 'link.delete', '' );
                                if($link->access()) {
                                        $link = Link::fromTextAndUrl( $this->t( 'Delete' ), $link );
                                        $template->setVar( 'link.delete', $link->toString() );
                                }
                                
                                $markup .= '<strong>'.((string)$this->t(
                                                '[@ID] @title in @parent_title', [
                                                '@ID' => $wf_id,
                                                '@title' => $workflow->getDataElement('title') ?? 'N.A.',
                                                '@parent_title' => $workflow->getParentMedium()->getElement('title') ?? 'N.A.'
                                        ])).'</strong><br>';
                                $markup .= $template->parse();
                        }
                }
                if(empty($markup)) $markup = (string)t('No active items found');
                return $markup;
        }
        
        /**
         * @return string
         */
        public function published(){
                
                if(Publisso::currentUser()->getId() >= 1){
                        
                        $rows = [];
                        $limit = 20;
                        $filter = [];
                        
                        if(\Drupal::request()->query->has('item')){
                                $limit = null;
                                $filter[] = ['wf_id', \Drupal::request()->query->get('item')];
                        }
                        
                        foreach(WorkflowManager::getItems([], array_merge($filter, [['wf_state', 'published', '=']]), 20) as $_){
                                
                                $workflow = WorkflowManager::getItem($_->wf_id);
                                $medium = $workflow->getMedium();
                                $creator = Publisso::User($workflow->getElement('created_by_uid'));
                                
                                if(!($workflow && $medium && $creator)){ \Drupal::service('messenger')->addError((string)$this->t('Cant load all information for item #@item', ['@item' => $_->wf_id])); continue; }
                                
                                $urlErratum = $urlSubMedium = $urlMedium = null;
                                
                                try {
                                        $urlErratum = $workflow->getPublishedEntity()->getErratumUrl();
                                }
                                catch(\Error $e){}
        
                                try {
                                        $urlSubMedium = $workflow->getPublishedEntity()->getLink();
                                }
                                catch(\Error $e){}
        
                                try {
                                        $urlMedium = $medium->getUrl()->toString();
                                }
                                catch(\Error $e){}
                                
                                $titleSubMedium = $urlSubMedium ? '<a href="'.$urlSubMedium.'">'.$workflow->getDataElement('title').'</a>' : $workflow->getDataElement('title');
                                $titleMedium    = $urlMedium    ? '<a href="'.$urlMedium.'">'   .$medium->getElement('title')                 .'</a>' : $medium->getElement('title')                        ;
                                
                                $rows[] = [
                                        ['data' => [
                                                '#markup' => $_->wf_id
                                        ]],
                                        ['data' => [
                                                '#markup' => $titleSubMedium.'<br>In: '.$titleMedium
                                        ]],
                                        ['data' => [
                                                '#markup' => $workflow->getElement('created')
                                        ]],
                                        ['data' => [
                                                '#markup' => $creator->profile()->getReadableName()
                                        ]],
                                        ['data' => [
                                                '#markup' => $workflow->getPublishedEntity()->getElement('doi')
                                        ]],
                                        ['data' => $urlErratum ? [
                                                '#type' => 'link',
                                                '#title' => (string)$this->t('Set erratum'),
                                                '#url' => $urlErratum
                                        ] : ['#markup' => '']]
                                ];
                        }
        
                        $list = [
                                '#type' => 'table',
                                '#header' => [
                                        ['data' => (string)$this->t('ID')],
                                        ['data' => (string)$this->t('Title')],
                                        ['data' => (string)$this->t('Created')],
                                        ['data' => (string)$this->t('Creator')],
                                        ['data' => (string)$this->t('DOI')],
                                        []
                                ],
                                '#rows' => $rows,
                                '#empty' => (string)$this->t('No items found...')
                        ];
                        
                        $filterID = [
                                '#type' => 'number',
                                '#min' => 1,
                                '#title' => (string)$this->t('Go to ID'),
                                '#attributes' => [
                                        'style' => [
                                                'width: auto'
                                        ],
                                        'onchange' => [
                                                'javascript: this.value ? location.search = (location.search == "" ? "?" : "&") + "item=" + this.value : location.search = "";'
                                        ]
                                ],
                                '#value' => \Drupal::request()->query->has('item') ? \Drupal::request()->query->get('item') : ''
                        ];
                        
                        return serialize([
                                'pager_top' => [
                                        '#type' => 'pager'
                                ],
                                $filterID,
                                'list' => $list,
                                'pager_bottom' => [
                                        '#type' => 'pager'
                                ]
                        ]);
                }
                
                $markup = [];
                $list = \Drupal::service('publisso_gold.tools')->getPublishedBookChapters_Workflow($this->session->get('user')['role_id']);
                $markup = !count($list) ? (string)t('No active items found') : '';
                
                $template = new Template();
                $template->get('dashboard_published_bookchapters_item_content');
                
                $tvars = [
                        'strCreatedDesc'        => (string)$this->t('Created'),
                        'strAuthorDesc'         => (string)$this->t('Submitted by'),
                        'strDOIDesc'            => (string)$this->t('DOI')
                ];
                
                foreach($tvars as $var => $val) $template->setVar($var, $val);
                
                foreach($list as $id){
                	
                        $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($id);
                        if(!$workflow) continue;
                        
                        $book     = new Book($workflow->getDataElement('bk_id'), true);
                        if(!$book) continue;
                        
                        $chapter  = new Bookchapter(\Drupal::service('publisso_gold.tools')->getBookChapterIDFromWorkflow($id), true);
                        if(!$chapter) continue;
                        
                        $author   = new User($workflow->getElement('created_by_uid'));
			
                        if(!($workflow->getElement('id') && $book->getElement('id') && $chapter->getElement('id') && $author->getElement('id'))){
                        	continue;
                        }
                        
                        $markup .= !empty($markup) ? '<hr>' : '';
                        
                        $template->setVar('strHelpSetErratum', '');
                        $template->setVar('strLnkSetErratum' , '');
                        $template->setVar('strAuthor', implode(', ', array_filter([$author->profile->getElement('lastname'), $author->profile->getElement('firstname')])));
                        $template->setVar('strTitle', implode(' ', [$workflow->getDataElement('title'), (string)t('in'), $book->getElement('title')]));
                        
                        foreach($book->getKeys() ?? [] as $key)
                                $template->setVar("book_$key", $book->getElement($key));
                        
                        foreach($chapter->getKeys() as $key)
                                $template->setVar("chapter_$key", $chapter->getElement($key));
                        
                        foreach($workflow->getKeys() as $key)
                                $template->setVar("workflow_$key", $workflow->getElement($key));
                        
                        foreach($workflow->getDataKeys() as $key)
                                $template->setVar("workflowdata_$key", $workflow->getDataElement($key));
                        
                        if($chapter->getElement('pending_erratum') != 1){
                                
                                $_urlErratum    = Url::fromROute('publisso_gold.book.chapter.seterratum', ['cp_id' => $chapter->getElement('id')]);
                                $template->setVar('urlLnkSetErratum' , $_urlErratum->toString());
                                $template->setVar('strHelpSetErratum', (string)t('Set Erratum'));
                                $template->setVar('strLnkSetErratum' , (string)t('Set Erratum'));
                        }
                        $markup .= $template->parse();
                }
                
                return $markup;
        }
        
        /**
         * @return string
         */
        public function meeic(){
                
                $list = \Drupal::service('publisso_gold.tools')->getMyEiCSubmissions($this->session->get('user')['id']);
                $markup = !count($list) ? (string)t('No active items found') : '';
                
                $template = new Template();
                $template->get('publisso_gold_eic_submission_dashbord');
                $template->setVar('txt_status' , (string)t('State'  ));
                $template->setVar('txt_editors', (string)t('Editors'));
                
                foreach($list as $wf_id){
                        
                        $markup .= !empty($markup) ? '<hr>' : '';
                        $workflow = new Workflow($wf_id);
                        
                        switch(substr($workflow->getElement('type'), 0, 2)){
                                
                                case 'bc': //bookchapters
                                        $medium = new Book($workflow->getDataElement('bk_id'), true);
                                        break;
                                
                                case 'ja': //journalarticles
                                        $medium = new Journal($workflow->getDataElement('jrn_id'), true);
                                        break;
                                
                                case 'cp': //conferencepapers
                                        break;
                        }
                        
                        $template->setVar('val_editors', '');
                        
                        foreach(explode(',', $workflow->getElement('assigned_to_editor')) as $_){
                                
                                $_user = new \Drupal\publisso_gold\Controller\User($_);
                                
                                !empty($template->getVar('val_editors')) ? $template->appendToVar('val_editors', '<br>') : '';
                                
                                $template->appendToVar('val_editors',
                                        implode(' ', array_filter([
                                                $_user->profile->getElement('graduation'        ),
                                                $_user->profile->getElement('firstname'         ),
                                                $_user->profile->getElement('lastname'          ),
                                                $_user->profile->getElement('graduation_suffix' )
                                        ]))
                                );
                        }
                        
                        $template->setVar('txtTitle', (string)t(
                                '[@ID] @ITEM_TITLE in @MEDIUM_TITLE', [
                                        '@ID'           => $workflow->getElement('id'),
                                        '@ITEM_TITLE'   => $workflow->getDataElement('title'),
                                        '@MEDIUM_TITLE' => $medium->getElement('title')
                        ]));
                        
                        $template->setVar('val_status', $workflow->getDataElement('state'));
                        
                        $markup .= $template->parse();
                }
                
                return $markup;
        }
        
        /**
         * @return string
         */
        public function meeditor(){
                
                $list = \Drupal::service('publisso_gold.tools')->getMyEditorSubmissions($this->session->get('user')['id']);
                $markup = !count($list) ? (string)t('No active items found') : '';
                
                $template = new Template();
                $template->get('publisso_gold_editor_submission_dashbord');
                
                $template->setVar('txt_status'   , (string)t('State'    ));
                
                foreach($list as $wf_id){
                        
                        $workflow = new Workflow($wf_id);
                        
                        if($workflow->getDataElement('state') == 'published') continue;
                        
                        $markup .= !empty($markup) ? '<hr>' : '';
                        $template->setVar('val_reviewers', '');
                        $template->setVar('lnk_unassign_reviewers', '');
                        $template->setVar('lnk_assign_reviewers'  , '');
                        $template->setVar('lnk_review_pages'      , '');
                        $template->setVar('txt_reviewers', '');
                        
                        switch(substr($workflow->getElement('type'), 0, 2)){
                                
                                case 'bc': //bookchapters
                                        $medium = new Book($workflow->getDataElement('bk_id'), true);
                                        break;
                                
                                case 'ja': //journalarticles
                                        $medium = new Journal($workflow->getDataElement('jrn_id'), true);
                                        break;
                                
                                case 'cp': //conferencepapers
                                        break;
                        }
                        
                        if($workflow->getDataElement('state') == 'in review'){
                                
                                $template->setVar('txt_reviewers', (string)t('Reviewers'));
                                $rs 	   = $workflow->getUserReviewsheets(['state' => 'all', 'nogroup' => true]);
                                
                                foreach($rs as $rs_id){
                                	
                                	$reviewsheet = $workflow->getReviewsheetByID($rs_id);
                                	
                                	$_user = new \Drupal\publisso_gold\Controller\User($reviewsheet->uid);
                                	!empty($template->getVar('val_reviewers')) ? $template->appendToVar('val_reviewers', '<br>') : '';
                                	
                                	$reviewStatus = $reviewsheet->state;
                                	
                                	$template->appendToVar('val_reviewers', '<span class="reviewStatus'.ucfirst($reviewStatus).'">');
                                	
                                	$template->appendToVar('val_reviewers',
                                		implode(', ',
                                			array_filter([
                                				$_user->profile->getReadableFullName('FL', ' '),
                                				$_user->profile->getAffiliation()
                                			])
                                		)
                                	);
                                	
                                }
                                
                                if(count($workflow->readReviewers()))
                                        $template->setVar('lnk_unassign_reviewers', \Drupal::l(t('Unassign Reviewers'), Url::fromRoute('publisso_gold.workflow.unassign_reviewers', ['wf_id' => $wf_id])));
                                
                                $template->setVar('lnk_assign_reviewers', Link::fromTextAndUrl(t('Assign Reviewers'), Url::fromRoute('publisso_gold.workflow.assign_reviewers', ['wf_id' => $wf_id]))->toString());
                                
                                if($workflow->hasCompletedReviewsheets()){
                                        
                                        $template->setVar(
                                                'lnk_review_pages',
                                                Link::fromTextAndUrl(
                                                        (string)$this->t('View completed review sheets'),
                                                        Url::fromRoute('publisso_gold.dashboard.workflow.reviews', ['wf_id' => $wf_id])
                                                )->toString()
                                        );
                                }
                        }

                        
                        $template->setVar('txtTitle', (string)t(
                                '[@ID] @ITEM_TITLE in @MEDIUM_TITLE', [
                                        '@ID'           => $workflow->getElement('id'),
                                        '@ITEM_TITLE'   => $workflow->getDataElement('title'),
                                        '@MEDIUM_TITLE' => $medium->getElement('title')
                        ]));
                        
                        $template->setVar('val_status', $workflow->getDataElement('state'));
                        
                        $markup .= $template->parse();
                }
                
                return $markup;
        }
        
        /**
         * @return string
         */
        public function mereviewer(){
                
                $list = \Drupal::service('publisso_gold.tools')->getMyReviewerSubmissions($this->session->get('user')['id']);
                $markup = !count($list) ? (string)t('No active items found') : '';
                
                $template = new Template();
                $template->get('publisso_gold_reviewer_submission_dashbord');
                $template->setVar('txt_status' , (string)t('State'  ));

                
                foreach($list as $wf_id){
                        
                        $markup .= !empty($markup) ? '<hr>' : '';
                        $workflow = new Workflow($wf_id);
                        
                        switch(substr($workflow->getElement('type'), 0, 2)){
                                
                                case 'bc': //bookchapters
                                        $medium = new Book($workflow->getDataElement('bk_id'), true);
                                        break;
                                
                                case 'ja': //journalarticles
                                        $medium = new Journal($workflow->getDataElement('jrn_id'), true);
                                        break;
                                
                                case 'cp': //conferencepapers
                                        break;
                        }
                        
                        $template->setVar('txtTitle', (string)t(
                                '[@ID] @ITEM_TITLE in @MEDIUM_TITLE', [
                                        '@ID'           => $workflow->getElement('id'),
                                        '@ITEM_TITLE'   => $workflow->getDataElement('title'),
                                        '@MEDIUM_TITLE' => $medium->getElement('title')
                        ]));
                        
                        $template->setVar('val_status', $workflow->getDataElement('state'));
                        
                        $markup .= $template->parse();
                }
                
                return $markup;
        }
        
        /**
         * @return string
         */
        public function mysubmissions(){
                
                $qry = \Drupal::database()->select('rwPubgoldWorkflow', 'wf')->fields('wf', ['wf_id']);
                $pager = $qry->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit(20);
                $pager->condition('wf_created_by_uid', \Drupal::service('session')->get('user')['id'], '=');
                
                $markup = !$pager->countQuery()->execute()->fetchField() ? (string)t('No active items found') : '';
                
                $template = TemplateManager::load('publisso_gold_my_submission_dashbord');
                $template->setVar('txt_status' , (string)t('State'  ));

                
                foreach($pager->execute()->fetchCol() as $wf_id){
                        
                        $markup .= !empty($markup) ? '<hr>' : '';
                        $workflow = new Workflow($wf_id);
                        
                        switch(substr($workflow->getElement('type'), 0, 2)){
                                
                                case 'bc': //bookchapters
                                        $medium = new Book($workflow->getDataElement('bk_id'), true);
                                        break;
                                
                                case 'ja': //journalarticles
                                        $medium = new Journal($workflow->getDataElement('jrn_id'), true);
                                        break;
                                
                                case 'cp': //conferencepapers
                                        break;
                        }
                        
                        $template->setVar('txtTitle', (string)t(
                                '[@ID] @ITEM_TITLE in @MEDIUM_TITLE', [
                                        '@ID'           => $workflow->getElement('id'),
                                        '@ITEM_TITLE'   => $workflow->getDataElement('title'),
                                        '@MEDIUM_TITLE' => $medium->getElement('title')
                        ]));
                        
                        $template->setVar('val_status', $workflow->getDataElement('state'));
                        
                        $markup .= $template->parse();
                }
                
                return serialize([
                         'pager_top' => [
                                 '#type' => 'pager'
                         ],
                         'markup' => [
                                 '#type' => 'markup',
                                 '#markup' => $markup.'<br><br><br>'
                         ],
                         'pager_bottom' => [
                                 '#type' => 'pager'
                         ]
                 ]);
        }
        
        /**
         * @return string
         */
        public function conferences(){
                
                $currentUser = Publisso::currentUser();
                
                $conferences = array_unique(array_merge(
                        ConferenceManager::getConferencesForUserRole('manager'),
                        ConferenceManager::getConferencesForUserRole('eo'),
                        ConferenceManager::getConferencesForUserRole('eic'),
                        ConferenceManager::getConferencesForUserRole('editor'),
                        ConferenceManager::getConferencesForUserRole('reviewer'),
                ));
                
                $conferences = ConferenceManager::getConferences(['by_ids' => $conferences], 20);
                
                $rows = [];
                
                foreach($conferences as $cf_id){
                        
                        $result = AbstractManager::getAbstractList($cf_id, ['w.wf_id'], [['ca_cfid', $cf_id]]);
                        $cntWorkflow = 0;
                        foreach($result as $res){
                                $workflow = WorkflowManager::getItem($res->wf_id);
                                if($workflow->userIsAssigned() || $currentUser->isAdministrator() || $currentUser->isPublisso()){
                                        $cntWorkflow++;
                                }
                        }
                        
                        $lnkAbstracts = ['data' => ['#markup' => '']];
                        
                        if(!!$cntWorkflow){
                                $lnkAbstractList = Url::fromRoute('publisso_gold.dashboard.abstracts', ['cf_id' => $cf_id]);
                                
                                if($lnkAbstractList->access()){
                                        $lnkAbstracts = ['data' => ['#type' => 'link', '#url' => $lnkAbstractList, '#title' => (string)$this->t('Abstracts')]];
                                }
                        }
        
                        $conference = ConferenceManager::getConference($cf_id);
                        $rows[] = [
                                ['data' => ['#markup' => $conference->getElement('title')]],
                                $lnkAbstracts
                        ];
                }
                
                return serialize([
                        'pager-top' => ['#type' => 'pager'],
                        'table' => [
                                '#type' => 'table',
                                '#header' => [
                                        ['data' => ['#markup' => (string)$this->t('Title')]],
                                        ['data' => ['#markup' => '']]
                                ],
                                '#empty' => (string)$this->t('No conferences found...'),
                                '#rows' => $rows
                        ],
                        'pager-bottom' => ['#type' => 'pager']
                ]);
        }
        
        /**
         * @param $wf_id
         * @return mixed
         */
        public function checkoutItem($wf_id){
        
                $myTodos = \Drupal::service('publisso_gold.tools')->getMyTodo($this->session->get('user')['id'], $this->session->get('user')['weight'], $this->session->get('user')['role_id']);
                
                if(!in_array($wf_id, $myTodos)){
                        $msg = "You don't have permissions to open this Item!";
                        return \Drupal::service('publisso_gold.tools')->accessDenied($msg);
                }

                $workflow = new Workflow($wf_id);

                if($workflow->getLock($this->session->get('user')['id'])){

                        //if in revision, return to dashboard
                        if($workflow->getDataElement('state') == 'in review'){
                                
                                //if new Reviewsheet -> call separate Controller
                                $medium = $workflow->getMedium();

                                $rp = new \Drupal\publisso_gold\Controller\Reviewsheet($medium->getElement('rpid'));

                                if(!empty($rp->getElement('schema')) && empty($rp->getElement('sheet'))){
                                        return \Drupal::service('publisso_gold.tools')->redirect('publisso_gold.review_item', ['wf_id' => $wf_id], 'publisso_gold.dashboard');
                                }
                                elseif(!empty($rp->getElement('sheet')) && empty($rp->getElement('schema'))){
                                        return \Drupal::service('publisso_gold.tools')->redirect( 'publisso_gold.workflow.item.review', ['wf_id' => $wf_id], 'publisso_gold.dashboard');
                                }
                                else{
                                        \Drupal::service('messenger')->addMessage((string)t($this->texts->get('workflow.item.review.no_method')));
                                        return \Drupal::service('publisso_gold.tools')->redirect('publisso_gold.dashboard');
                                }
                        }
                        else{

                                return \Drupal::service('publisso_gold.tools')->redirect( 'publisso_gold.workflow_item', ['wf_id' => $wf_id]);
                        }
                }
                else{
                        $msg = "This item is currently locked!";
                        return \Drupal::service('publisso_gold.tools')->accessDenied($msg);
                }
        }
        
        /**
         * @param $wf_id
         * @return array
         */
        public function reviews($wf_id){
                
                $workflow = new Workflow($wf_id);
                
                $headline = (string)t('Reviewsheets of submission "@title"', ['@title' => $workflow->getDataElement('title')]);
                
                
                return [
                        'headline' => [
                                '#markup' => $headline,
                                '#prefix' => '<h2>',
                                '#suffix' => '</h2>'
                        ],
                        'content' => [
                                \Drupal::formBuilder()->getForm("Drupal\\publisso_gold\\Form\\displayReviewsheetSkel", ['wf_id' => $wf_id])
                        ],
                        'back' => [
                                '#type'         => 'link',
                                '#attributes'   => [
                                        'class'         => [
                                                'btn',
                                                'btn-warning'
                                        ]
                                ],
                                '#url'          => Url::fromRoute('publisso_gold.dashboard', ['action' => 'meeditor']),
                                '#title'        => (string)$this->t('Back'),
                                '#suffix'       => '<br><br>'
                        ],
                        '#cache' => [
                                'max-age' => 0
                        ]
                ];
        }
        
        /**
         * @param $workflow
         * @return string
         */
        private function mailInfo(&$workflow){
                
                $userWeight = \Drupal::service('session')->get('user')['weight'];
                $retstr = '';
                
                if(\Drupal::service('publisso_gold.tools')->userHasAccessRight('wfmailinforead', $userWeight)){
                        
                        if(($result = \Drupal::database()->select('rwPubgoldWorkflowMailHistory', 't')->fields('t', [])->condition('wf_id', $workflow->getElement('id'), '=')->countQuery()->execute()->fetchField()) > 0){
                                
                                $url = Url::fromRoute('publisso_gold.workflow.mailhistory', ['wf_id' => $workflow->getElement('id')]);
                                $lnk = new Link(t('Mailhistory'), $url);
                                $template = new Template();
                                $template->get('workflow_mailinfo');
                                $template->setVar('lnkWFMailinfo', $lnk->toString());
                                $retstr .= $template->parse();
                        }
                }
                
                return $retstr;
        }
        
        /**
         * @param $workflow
         * @return string
         */
        private function lockInfo(&$workflow){
                
                $userWeight = \Drupal::service('session')->get('user')['weight'];
                $retstr = '';
                
                if(\Drupal::service('publisso_gold.tools')->userHasAccessRight('wflockinforead', $userWeight)){
                        
                        $template = new Template();
                        $template->get('workflow_lockinfo');
                        
                        $template->setVar('strWFLockedDesc', (string)t('Workflow locked'));
                        $template->setVar('strWFLockedByDesc', (string)t('Workflow locked by'));
                        
                        if(!$workflow->getElement('locked')){
                                return $retstr;
                        }
                        else{
                                $template->setVar('strWFLocked', (string)t('Yes'));
                                
                                $user = new User($workflow->getElement('locked_by_uid'));
                                
                                $template->setVar('strWFLockedBy', $user->profile->getReadableFullName());
                        }
                        
                        $retstr .= $template->parse();
                }
                
                return $retstr;
        }
        
        /**
         * @param $workflow
         * @return string
         */
        private function lockOverride(&$workflow){
        
                $userWeight = \Drupal::service('session')->get('user')['weight'];
                $retstr = '';
                
                if(\Drupal::service('publisso_gold.tools')->userHasAccessRight('wflockinfowrite', $userWeight) && $workflow->locked()){
                        
                        $template = new Template();
                        $template->get('workflow_lockinfo_override');
                        
                        $urlOverride = Url::fromRoute('publisso_gold.workflow.lockinfo.override', ['wf_id' => $workflow->getElement('id')]);
                        $urlOptions = [
                                'attributes' => [
                                        'class' => [
                                                'use-ajax'
                                        ],
                                        'data-dialog-type' => [
                                                'modal'
                                        ]
                                ]
                        ];
                        $urlOverride->setOptions($urlOptions);
                        
                        $lnkOverride = new Link((string)t('Unlock item'), $urlOverride);
                        
                        $template->setVar('lnkWFUnlock', $lnkOverride->toString());
                        
                        $retstr .= $template->parse();
                }
                
                return $retstr;
        }
}
