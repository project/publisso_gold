<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\SSOProvider.
 */

namespace Drupal\publisso_gold\Controller;

/**
 * Class SSOProvider
 * @package Drupal\publisso_gold\Controller
 */
class SSOProvider{
        
        private static $url;
        private static $reqType;
        private static $validateCert;
        private $statusCode;
        private $statusMessage;
        
        public function __construct(){
                
                self::$url              =   Publisso::setup()->getValue('system.sso.gw_url');
                self::$reqType          =   Publisso::setup()->getValue('system.sso.gw_reqtype');
                self::$validateCert     = !!Publisso::setup()->getValue('system.sso.gw_validatecert');
                
                $this->statusCode = $this->statusMessage = null;
        }
        
        /**
         * @param array $data
         * @return \Psr\Http\Message\ResponseInterface
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        private function request(array &$data){
                
                return \Drupal::httpClient()->request(
                        self::$reqType,
                        self::$url,
                        [
                                'headers' => [
                                        'Content-Type' => 'application/json'
                                ],
                                'http_errors' => false,
                                'verify' => self::$validateCert,
                                'query' => [
                                            'data' => base64_encode(json_encode($data)),/*
                                            'secpin' => 'HammerHai88' //nur zum Testen - muss wieder raus*/
                                ]
                        ]
                );
        }
        
        /**
         * @param string $login
         * @param string $password
         * @return false|mixed
         */
        public function getUserData(string $login, string $password){
                
                $data = [
                        'method' => 'get_user_data',
                        'parameters' => [
                                'remote_id'     => $login,
                                'pw'            => $password,
                                'with_sysdata'  => 1
                        ],
                        'values' => []
                ];
                
                
                $response = $this->request($data);
                
                if($response === null){
                        $this->statusCode = 700;
                        $this->statusMessage = 'SSO-Host not available';
                        return false;
                }
                
                //HTTP-Response
                if($response->getStatusCode() != 200){
                        $this->statusCode       = $response->getStatusCode();
                        $this->statusMessage    = $response->getReasonPhrase();
                        return false;
                }
                
                //Script-Response
                $ret = json_decode($response->getBody()->getContents());
                
                if($ret->code != 200){
                        $this->statusCode       = $ret->code;
                        $this->statusMessage    = $ret->msg;
                        return false;
                }
                
                //erwarte JSON://....
                if(!preg_match('/^JSON:\/\/(.*)/', $ret->msg, $matches)){
                        $this->statusCode       = 100;
                        $this->statusMessage    = $ret->msg;
                        return false;
                }
                
                //nur die Daten, die wir auch benötigen
                return json_decode($matches[1], true);
                
        }
        
        /**
         * @return null
         */
        public function getLastResponseCode(){
                return $this->statusCode;
        }
        
        /**
         * @return null
         */
        public function getLastResponseMessage(){
                return $this->statusMessage;
        }
        
        /**
         * @param User $user
         * @param string $username
         * @param string $password
         * @return bool
         */
        public function createUser(\Drupal\publisso_gold\Controller\User &$user, string $username, string $password){
                
                $data = [
                        'method' => 'register_user',
                        'parameters' => [
                                'remote_id' => $user->getElement('initial_email') ? $user->getElement('initial_email') : $user->getElement('user'),
                                'pw' => $password,
                                'address' => [
                                        'institut' => $user->profile->getElement('institute'),
                                        'LKURZ' => preg_match('/^\d+$/', $user->profile->getElement('country')) ? array_search(Publisso::tools()->getCountry($user->profile->getElement('country')), Publisso::tools()->getCountrylist()) : $user->profile->getElement('country'),
                                        'LORT' => $user->profile->getElement('city'),
                                        'name' => $user->profile->getReadableName(),
                                        'LSTRAS' => $user->profile->getElement('street'),
                                        'LPZAHL' => $user->profile->getElement('postal_code'),
                                        'LTELNR' => $user->profile->getElement('telephone'),
                                        'LSPRACHE' => $user->profile->getElement('correspondence_language'),
                                        'LLAND' => \Drupal::service('publisso_gold.tools')->getCountry($user->profile->getElement('country')),
                                        'PRGRP' => 'z2',
                                        'LEMAILADR' => $user->profile->getElement('email')
                                ]
                        ],
                        'values' => []
                ];
                
                //Dummyeinträge, bis Schnittstelle angepasst
                if(empty($data['parameters']['address']['LKURZ'])) $data['parameters']['address']['LKURZ'] = 'DE';
                if(empty($data['parameters']['address']['LLAND'])) $data['parameters']['address']['LLAND'] = 'Deutschland';
                if(empty($data['parameters']['address']['LORT' ])) $data['parameters']['address']['LORT' ] = 'Stadt';
                
                //Felder löschen, wenn Wert leer
                if(is_array($data)) {
                        foreach ( $data[ 'parameters' ][ 'address' ] as $k => $v ) if ( !$v ) unset( $data[ 'parameters' ][ 'address' ][ $k ] );
                }
                $response = $this->request($data);
                
                if($response === null){
                        $this->statusCode       = 700;
                        $this->statusMessage    = $response->getReasonPhrase();
                        return false;
                }
                
                //HTTP-Response
                if($response->getStatusCode() != 200){
                        $this->statusCode       = $response->getStatusCode();
                        $this->statusMessage    = $response->getContent();
                        return false;
                }
                
                //Script-Response
                $ret = json_decode($response->getBody()->getContents());
                
                if($ret->code != 200){
                        $this->statusCode       = $ret->code;
                        $this->statusMessage    = $ret->msg;
                        return false;
                }
                
                preg_match('/K\d+$/', $ret->msg, $matches);
                $kennung_zbmed = $matches[0];
                $user->setElement('zbmed_kennung', $matches[0]);
                $user->setElement('initial_email', $user->getElement('user'));
                
                return true;
        }
        
        /**
         * @param User $user
         * @return bool
         */
        public function updateUser(\Drupal\publisso_gold\Controller\User &$user){
                
                if(!(\Drupal::service('session')->get('user')['password'])){
                        \Drupal::service('publisso_gold.tools')->logMsg('Can\'t update SSO-user (empty Password)!');
                        return false;
                }
                
                if(empty($user->getElement('initial_email'))) $user->setElement('initial_email', $user->profile->getElement('email'));
                
                $data = [
                        'method' => 'change_user_data',
                        'parameters' => [
                                'remote_id' => $user->getElement('zbmed_kennung'),
                                'pw' => \Drupal::service('session')->get('user')['password'],
                                'address' => [
                                        'institut' => $user->profile->getElement('institute'),
                                        'LKURZ' => preg_match('/^\d+$/', $user->profile->getElement('country')) ? array_search(\Drupal::service('publisso_gold.tools')->getCountry($user->profile->getElement('country')), \Drupal::service('publisso_gold.tools')->getCountrylist()) : $user->profile->getElement('country'),
                                        'LORT' => $user->profile->getElement('city'),
                                        'name' => $user->profile->getReadableName(),
                                        'LSTRAS' => $user->profile->getElement('street'),
                                        'LPZAHL' => $user->profile->getElement('postal_code'),
                                        'LTELNR' => $user->profile->getElement('telephone'),
                                        'LSPRACHE' => $user->profile->getElement('correspondence_language'),
                                        'LLAND' => \Drupal::service('publisso_gold.tools')->getCountry($user->profile->getElement('country')),
                                        'PRGRP' => 'z2',
                                        'LEMAILADR' => $user->profile->getElement('email')
                                ]
                        ],
                        'values' => []
                ];
                
                $response = $this->request($data);
                
                if($response === null){
                        $this->statusCode       = 700;
                        $this->statusMessage    = $response->getReasonPhrase();
                        return false;
                }
                
                //HTTP-Response
                if($response->getStatusCode() != 200){
                        $this->statusCode       = $response->getStatusCode();
                        $this->statusMessage    = $response->getContent();
                        return false;
                }
                
                //Script-Response
                $ret = json_decode($response->getBody()->getContents());
                
                if($ret->code != 200){
                        $this->statusCode       = $ret->code;
                        $this->statusMessage    = $ret->msg;
                        return false;
                }
                
                return true;
        }
        
        /**
         * @return bool
         */
        public function SSOActive(){
                
                $ssoPositiveValues = ['yes', '1', 'on', 'enabled'];
                return (in_array(strtolower(\Drupal::service('publisso_gold.setup')->getValue('system.sso.active')), $ssoPositiveValues));
        }
        
        /**
         * @param $password
         * @return bool
         */
        public function setUserPassword($password){
                
                $user = new User(\Drupal::service('session')->get('user')['id']);
                
                $data = [
                        'method' => 'change_password',
                        'parameters' => [
                                'remote_id' => $user->getElement('zbmed_kennung'),
                                'pw' => \Drupal::service('session')->get('user')['password'],
                                'new_pw' => $password
                        ],
                        'values' => []
                ];
                
                $response = $this->request($data);
                
                if($response === null){
                        $this->statusCode       = 700;
                        $this->statusMessage    = $response->getReasonPhrase();
                        return false;
                }
                
                //HTTP-Response
                if($response->getStatusCode() != 200){
                        $this->statusCode       = $response->getStatusCode();
                        $this->statusMessage    = $response->getContent();
                        return false;
                }
                
                //Script-Response
                $ret = json_decode($response->getBody()->getContents());
                
                if($ret->code != 200){
                        $this->statusCode       = $ret->code;
                        $this->statusMessage    = $ret->msg;
                        return false;
                }
                
                return true;
        }
        
        /**
         * @param string $email
         * @return bool|null
         */
        public function emailExists(string $email){
                
                $data = [
                        'method' => 'check_id',
                        'parameters' => [
                                'remote_id'     => $email
                        ],
                        'values' => []
                ];
                
                $response = $this->request($data);
                
                if($response === null){
                        $this->statusCode       = 700;
                        $this->statusMessage    = $response->getReasonPhrase();
                        return null;
                }
                
                //HTTP-Response
                if($response->getStatusCode() != 200){
                        $this->statusCode       = $response->getStatusCode();
                        $this->statusMessage    = $response->getContent();
                        return null;
                }
                
                //Script-Response
                $ret = json_decode($response->getBody()->getContents());
                
                if($ret->code != 200){
                        
                        $this->statusCode       = $ret->code;
                        $this->statusMessage    = $ret->msg;
                        
                        switch(strtoupper($this->statusMessage)){
                                
                                case 'UNKNOWN':
                                        return false;
                                        break;
                                
                                default:
                                        return null;
                        }
                }
                else{
                	return true;
                }
        }
}
