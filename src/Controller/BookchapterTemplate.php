<?php
        
        /**
         * @file
         * Contains
         * \Drupal\publisso_gold\Controller\BookchapterTemplate.
         */
        
        namespace Drupal\publisso_gold\Controller;
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Url;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Symfony\Component\HttpFoundation\Request;
        
        /**
         * Class BookchapterTemplate
         * @package Drupal\publisso_gold\Controller
         */
        class BookchapterTemplate extends ControllerBase {
                
                private $id;
                private $elements;
                private $tbl_column_prefix;
                private $tablename;
                private $session;
                private $modname;
                private $tools;
        
                /**
                 * @return string
                 */
                public function __toString() {
                        return "class BookchapterTemplate";
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return BookchapterTemplate|static
                 */
                public static function create(ContainerInterface $container) {
                        \Drupal::service('page_cache_kill_switch')->trigger();
                        return new static($container->get('current_route_match'));
                }
        
                /**
                 * BookchapterTemplate constructor.
                 * @param null $currentRoute
                 * @param null $id
                 */
                public function __construct($currentRoute = null, $id = null) {
                        
                        if (!($this->id = $id)) $this->id = $currentRoute->getParameter('id');
                        
                        $this->tablename = 'rwPubgoldBookchapterTemplates';
                        $this->elements = [];
                        $this->tbl_column_prefix = '';
                        $this->session = \Drupal::service('session');
                        $this->modname = 'bookchaptertemplate';
                        $this->tools = \Drupal::service('publisso_gold.tools');
                        $this->load($this->id);
                }
        
                /**
                 * @return mixed
                 */
                public function ID() {
                        return $this->id;
                }
        
                /**
                 * @param $elemName
                 * @param null $elemValue
                 * @param bool $storeOnDB
                 * @return bool
                 */
                public function setElement($elemName, $elemValue = null, $storeOnDB = true) {
                        
                        if (array_key_exists($elemName, $this->elements)) {
                                
                                $oldValue = $this->elements[$elemName];
                                $this->elements[$elemName] = $elemValue;
                                
                                if ($storeOnDB === false)
                                        return true;
                                
                                if ($this->save($elemName) === false) {
                                        
                                        $this->elements[$elemName] = $oldValue;
                                        return false;
                                } else {
                                        return true;
                                }
                        } else {
                                return false;
                        }
                }
        
                /**
                 * @param $elemName
                 * @return mixed
                 */
                public function getElement($elemName) {
                        if ($elemName == 'id') return $this->id;
                        return $this->elements[$elemName];
                }
        
                /**
                 * @param null $elemName
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 */
                public function save($elemName = null) {
                        
                        $fields = $this->elements;
                        
                        if ($elemName !== null) { //if specific element selected
                                $fields = [$elemName => $fields[$elemName]];
                        }
                        
                        foreach ($fields as $k => $v) {
                                $fields[$this->tbl_column_prefix . $k] = $v;
                                unset($fields[$k]);
                        }
                        
                        return \Drupal::database()->update($this->tablename)
                                      ->fields($fields)
                                      ->condition($this->tbl_column_prefix . 'id', $this->id)
                                      ->execute();
                }
                
                private function load() {
                        
                        if ($this->id) {
                                
                                $result = \Drupal::database()->select($this->tablename, 'bc')
                                                 ->fields('bc', [])
                                                 ->condition($this->tbl_column_prefix . 'id', $this->id, '=')
                                                 ->execute()
                                                 ->fetchAssoc();
                                
                                foreach ($result as $k => $v) {
                                        
                                        if ($k != $this->tbl_column_prefix . 'id') //store chapter except id
                                                $this->elements[str_replace($this->tbl_column_prefix, '', $k)] = $v;
                                }
                        }
                }
        
                /**
                 * @return array
                 */
                public function getKeys() {
                        return array_keys($this->elements);
                }
        
                /**
                 * @param $action
                 * @param Request $request
                 * @return array
                 */
                public function main($action, Request $request) {
                        
                        if (!$this->tools->userHasAccessRight($this->modname . '.' . $action, $this->session->get('user')['weight'])) {
                                #return $this->tools->accessDenied();
                        }
                        
                        if (!method_exists($this, $action)) {
                                return [
                                        '#type'     => 'markup',
                                        '#markup'   => (string)t('This function ist not implemented!'),
                                        '#attached' => [
                                                'library' => [
                                                        'publisso_gold/default',
                                                ],
                                        ],
                                        '#cache'    => [
                                                'max-age' => 0,
                                        ],
                                ];
                        }
                        
                        return $this->$action($request);
                }
        
                /**
                 * @return string[]
                 */
                private function show() {
                        
                        return [
                                '#type'     => 'inline_template',
                                '#template' => $this->getTemplateHTML(),
                        ];
                }
        
                /**
                 * @param Request $request
                 * @return array
                 */
                private function fill(Request $request) {
                        $keys = $request->query->keys();
                        $args = [];
                        foreach ($keys as $k => $v) $args[$v] = $request->query->get($v);
                        return \Drupal::formBuilder()->getForm("Drupal\publisso_gold\Form\\fillBookchapterTemplate", array_merge(['templateID' => $this->ID()], $args));
                }
        
                /**
                 * @return array
                 */
                private function references() {
                        return \Drupal::formBuilder()->getForm("Drupal\publisso_gold\Form\\setBookchapterTemplateReferences", ['templateID' => $this->ID()]);
                }
        
                /**
                 * @return string
                 */
                public function getTemplateHTML() {
                        
                        $templateDOM = new \DOMDocument();
                        $templateDOM->loadXML($this->elements['template']);
                        $templateXpath = new \DOMXpath($templateDOM);
                        return $templateDOM->saveHTML($templateXpath->evaluate('/template//document')[0]);
                }
        
                /**
                 * @return false|string
                 */
                public function getTemplateXML() {
                        
                        $templateDOM = new \DOMDocument();
                        $templateDOM->loadXML($this->elements['template']);
                        $templateXpath = new \DOMXpath($templateDOM);
                        return $templateDOM->saveXML($templateXpath->evaluate('/template//document')[0]);
                }
        
                /**
                 * @param null $uid
                 * @return bool
                 * @throws \Exception
                 */
                public function createTemplateArticle($uid = null) {
                        
                        if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];
                        if (!$uid) return false;
                        
                        try {
                                \Drupal::database()->insert('rwPubgoldBookchapterTemplateArticles')->fields([
                                        'uid'         => $uid,
                                        'template_id' => $this->id,
                                        'bk_id'       => $this->getElement('bk_id'),
                                        'template'    => $this->getElement('template'),
                                ])->execute();
                                
                                return true;
                        } catch (\Drupal\Core\Database\DatabaseExceptionWrapper $e) {
                                \Drupal::service('publisso_gold.tools')->logMsg(__METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage());
                                \Drupal::service('messenger')->addError('An error occured. Please inform the site-admin.');
                                return false;
                        }
                }
        
                /**
                 * @param null $uid
                 * @return false
                 */
                public function getTemplateArticle($uid = null) {
                        
                        if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];
                        if (!$uid) return false;
                        
                        $qry = \Drupal::database()->select('rwPubgoldBookchapterTemplateArticles', 't')->fields('t', ['template']);
                        
                        $cond = $qry->andConditionGroup();
                        $cond->condition('uid', $uid, '=');
                        $cond->condition('template_id', $this->id, '=');
                        $qry->condition($cond);
                        
                        if ($qry->countQuery()->execute()->fetchField() == 0) return false;
                        return $qry->execute()->fetchField();
                }
        
                /**
                 * @param $xml
                 * @param null $uid
                 * @return bool
                 */
                public function saveTemplateArticle($xml, $uid = null) {
                        
                        if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];
                        if (!$uid) return false;
                        
                        try {
                                \Drupal::database()->update('rwPubgoldBookchapterTemplateArticles')->fields([
                                        'template' => $xml,
                                ])->condition('uid', $uid, '=')->condition('template_id', $this->id, '=')->execute();
                                return true;
                        } catch (\Drupal\Core\Database\DatabaseExceptionWrapper $e) {
                                \Drupal::service('publisso_gold.tools')->logMsg(__METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage());
                                \Drupal::service('messenger')->addError('An error occured. Please inform the site-admin.');
                                return false;
                        }
                }
        
                /**
                 * @param array $filter
                 * @return array|false
                 */
                public function getTemplateArticlesIDs(array $filter = []) {
                        
                        $qry = \Drupal::database()->select('rwPubgoldBookchapterTemplateArticles', 't')->fields('t', ['id']);
                        
                        foreach ($filter as $field => $value) {
                                $qry->condition($field, $value, '=');
                        }
                        
                        $qry->condition('template_id', $this->id, '=');
                        
                        $ids = [];
                        
                        try {
                                $rows = $qry->execute()->fetchAll();
                                
                                foreach ($rows as $row) {
                                        $ids[] = $row->id;
                                }
                                
                                return $ids;
                        } catch (\Drupal\Core\Database\DatabaseExceptionWrapper $e) {
                                \Drupal::service('publisso_gold.tools')->logMsg(__METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage());
                                \Drupal::service('messenger')->addError('An error occured. Please inform the site-admin.');
                                return false;
                        }
                }
        
                /**
                 * @param $filter
                 * @param null $formdata
                 * @return array
                 * @throws \Exception
                 */
                public function getForm($filter, $formdata = null) {
                        
                        $html = $this->getTemplateXML();
                        $dom = new \DOMDocument();
                        $dom->loadXML($this->elements['template']);
                        $xpath = new \DOMXpath($dom);
                        
                        $form = [];
                        $maxFormGroupWeight = 0;
                        $versioningFields = [];
                        $dbFields = [];
                        
                        foreach ($xpath->evaluate('/root/metas/meta[@id="versioning-fields"]/fields/field') as $_) {
                                $versioningFields[] = $_->getAttribute('field-id');
                        }
                        
                        $this->storeVersioningFields($versioningFields);
                        
                        $dataHasWorkflowItem = false;
                        $dataWorkflowItemID = null;
                        
                        $tocs = $tocsNew = [];
                        
                        foreach ($xpath->evaluate('/root/template/variables/variable') as $varNode) {
                                
                                //Erstellen der Gruppe, wenn sie noch nicht vorhanden ist
                                if ($varNode->hasAttribute('formgroup') && !array_key_exists($varNode->getAttribute('formgroup'), $form)) {
                                        
                                        $_ = $xpath->evaluate('/root/template/formgroups/formgroup[@id="' . $varNode->getAttribute('formgroup') . '"]');
                                        
                                        if ($_[0]->getAttribute('weight') > $maxFormGroupWeight) $maxFormGroupWeight = $_[0]->getAttribute('weight');
                                        
                                        if ($_->length) {
                                                
                                                $title = $xpath->evaluate('title', $_[0]);
                                                $desc = $xpath->evaluate('description', $_[0]);
                                                
                                                $form[$varNode->getAttribute('formgroup')] = [
                                                        '#type'   => 'fieldset',
                                                        '#weight' => $_[0]->getAttribute('weight'),
                                                        '#title'  => (string)$this->t($title->length ? trim($title[0]->nodeValue) : 'Title not available'),
                                                        'content' => [],
                                                ];
                                                
                                                if ($_[0]->hasAttribute('collapsible')) {
                                                        $form[$varNode->getAttribute('formgroup')]['#collapsible'] = !!$_[0]->getAttribute('collapsible');
                                                }
                                                
                                                if ($_[0]->hasAttribute('collapsed-on-start')) {
                                                        $form[$varNode->getAttribute('formgroup')]['#collapsed'] = !!$_[0]->getAttribute('collapsed-on-start');
                                                }
                                                
                                                $form[$varNode->getAttribute('formgroup')]['#description'] = '';
                                                
                                                if ($desc->length) {
                                                        $form[$varNode->getAttribute('formgroup')]['#description'] .= trim($desc[0]->nodeValue);
                                                }
                                                
                                                #$form[$varNode->getAttribute('formgroup')]['#description'] .= '<br><a name="'.$varNode->getAttribute('formgroup').'-Overview"></a>::toc::';
                                                
                                                #if($varNode->hasAttribute('formgroup') && !array_key_exists($varNode->getAttribute('formgroup'), $tocs)) $tocs[$varNode->getAttribute('formgroup')] = ['title' => $title[0]->nodeValue, 'items' => []];
                                        }
                                }
                                
                                if ($varNode->hasAttribute('formgroup')) {
                                        
                                        $desc = $xpath->evaluate('description', $varNode);
                                        $desc = !!$desc->length ? trim($desc[0]->nodeValue) : null;
                                        
                                        $title = $xpath->evaluate('label', $varNode);
                                        $title = !!$title->length ? trim($title[0]->nodeValue) : null;
                                        
                                        $placeholder = $xpath->evaluate('placeholder', $varNode);
                                        $placeholder = !!$placeholder->length ? trim($placeholder[0]->nodeValue) : null;
                                        
                                        $value = null;
                                        $qry = $versionQry = $qryValuesNode = '';
                                        
                                        if (is_array($filter)) {
                                                
                                                $found = true;
                                                foreach ($versioningFields as $field) {
                                                        if (!array_key_exists($field, $filter)) $found = false;
                                                }
                                                
                                                if ($found) { //alle Versionierungsparameter wurden im Filter gefunden
                                                        
                                                        foreach ($filter as $k => $v) {
                                                                
                                                                if (in_array($k, $versioningFields))
                                                                        $qry .= (!empty($qry) ? ' and ' : '') . '@' . $k . '="' . $v . '"';
                                                        }
                                                        
                                                        $qry = "data/values[$qry]";
                                                }
                                                
                                                if ($qry) {
                                                        
                                                        $valuesNodeNL = $xpath->evaluate($qry, $varNode);
                                                        
                                                        if ($valuesNodeNL->length) {
                                                                
                                                                $valueNode = $valuesNodeNL->item(0);
                                                                $maxversion = $valueNode->getAttribute('maxversion');
                                                                
                                                                $versionQry = 'value[@version="' . $maxversion . '"]';
                                                                if (array_key_exists('version', $filter)) {
                                                                        $versionQry = 'value[@version="' . $filter['version'] . '"]';
                                                                }
                                                                
                                                                $valueNodeNL = $xpath->evaluate($versionQry, $valueNode);
                                                                
                                                                if ($valueNodeNL->length) {
                                                                        $valueNode = $valueNodeNL->item(0);
                                                                        $valueNodePublished = !!$valueNode->getAttribute('published');
                                                                        $dataHasWorkflowItem = !!$valueNode->getAttribute('wf_id');
                                                                        
                                                                        if ($dataHasWorkflowItem) $dataWorkflowItemID = $valueNode->getAttribute('wf_id');
                                                                        
                                                                        $nlEntries = $xpath->evaluate('entry', $valueNode);
                                                                        
                                                                        if ($nlEntries->length == 1) {
                                                                                $value = trim($nlEntries->item(0)->nodeValue);
                                                                        } else if ($nlEntries->length > 1) {
                                                                                
                                                                                $sep = $varNode->getAttribute('valueseparator');
                                                                                if ($sep == "EOL") $sep = PHP_EOL;
                                                                                $values = [];
                                                                                
                                                                                for ($i = 0; $i < $nlEntries->length; $i++) {
                                                                                        $values[] = $nlEntries->item($i)->nodeValue;
                                                                                }
                                                                                
                                                                                $value = implode("$sep", $values);
                                                                        }
                                                                        
                                                                        $version = $valueNodePublished ? ($maxversion + 1) : $valueNode->getAttribute('version');
                                                                        $createVersion = $version != $maxversion;
                                                                } else {
                                                                        $version = ($maxversion + 1);
                                                                        $createVersion = true;
                                                                }
                                                        }
                                                }
                                        }
                                        
                                        $addReferences = false;
                                        if ($varNode->hasAttribute('linkreferences') && $varNode->getAttribute('linkreferences') == 1) $addReferences = true;
                                        
                                        $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')] = [
                                                '#title'                   => $title ?? '',
                                                '#type'                    => $varNode->getAttribute('type'),
                                                '#label_attributes'        => [
                                                        'class' => [
                                                                'displayBlock',
                                                        ],
                                                ],
                                                '#attributes'              => [
                                                        'class'          => [
                                                                'form-field-width100',
                                                        ],
                                                        'valueseparator' => $varNode->hasAttribute('valueseparator') && $varNode->getAttribute('valueseparator') ? $varNode->getAttribute('valueseparator') : '',
                                                ],
                                                '#required'                => $varNode->getAttribute('required') ?? false,
                                                '#ajax'                    => [
                                                        'event'           => 'change',
                                                        'prevent'         => 'blur',
                                                        'progress'        => [
                                                                'type' => 'none',
                                                        ],
                                                        'callback'        => '::autosave',
                                                        'disable-refocus' => true,
                                                        'wrapper'         => 'messages',
                                                ],
                                                '#limit_validation_errors' => [],
                                        ];
                                        
                                        if ($varNode->getAttribute('type') == 'markup') {
                                                $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#type'] = 'inline_template';
                                                $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#template'] = $desc;
                                        }
                                        
                                        if ($varNode->getAttribute('type') == 'number' && $varNode->hasAttribute('step')) {
                                                $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#step'] = $varNode->getAttribute('step') ?? 1;
                                        }
                                        
                                        if ($varNode->getAttribute('type') == 'number' && $varNode->hasAttribute('min')) {
                                                $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#min'] = $varNode->getAttribute('min');
                                        }
                                        
                                        if ($varNode->getAttribute('type') == 'number' && $varNode->hasAttribute('max')) {
                                                $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#max'] = $varNode->getAttribute('max');
                                        }
                                        
                                        if ($varNode->hasAttribute('maxlength')) {
                                                $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#maxlength'] = $varNode->getAttribute('maxlength');
                                        }
                                        
                                        $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#default_value'] = $formdata[$varNode->getAttribute('id')] ?? $value ?? null;
                                        
                                        if ($placeholder) {
                                                $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#placeholder'] = $placeholder;
                                        }
                                        
                                        if ($form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#attributes']['valueseparator']) {
                                                $sep = $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#attributes']['valueseparator'];
                                                #$desc .= (!empty($desc) ? '<br>' : '').((string)$this->t('Here several values can be separated by "@separator" be entered.', ['@separator' => $sep]));
                                                $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#description'] = ((string)$this->t('For entering several items pls separate by @separator', ['@separator' => $sep]));
                                        }
                                        
                                        if ($addReferences) {
                                                $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#prefix'] = '<div class="rwTable">';
                                                
                                                
                                                if ($desc) {
                                                        $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#suffix'] = '<div class="rwTableCaption before><span class="BKTemplateFormDesc">' . $desc . '</span></div></div>';
                                                } else {
                                                        $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#suffix'] = '</div>';
                                                }
                                                
                                                $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#prefix'] .= '<div class="rwTableRow"><div class="rwTablecell width50percent">';
                                        } else if ($varNode->getAttribute('type') != 'markup') {
                                                $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#suffix'] = '<div class="BKTemplateFormDesc">' . $desc . '</div>';
                                        }
                                        
                                        if ($varNode->getAttribute('type') == 'select') {
                                                
                                                if ($varNode->hasAttribute('required') && $varNode->getAttribute('required') == 0) {
                                                        
                                                        $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#options'] = [
                                                                '- ' . ((string)$this->t('Select')) . ' -',
                                                        ];
                                                }
                                                
                                                
                                                $options = $xpath->evaluate('preset/option', $varNode);
                                                
                                                foreach ($options as $option) {
                                                        $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#options'][trim($option->nodeValue)] = trim($option->nodeValue);
                                                }
                                                
                                                if ($varNode->hasAttribute('options')) {
                                                        
                                                        $options = $varNode->getAttribute('options');
                                                        
                                                        if (preg_match('/^(.+?)\/\/(.+?)::(.+?)$/', $options, $matches)) {
                                                                
                                                                switch ($matches[1]) {
                                                                        case 'DrupalService':
                                                                                $service = $matches[2];
                                                                                $method = $matches[3];
                                                                                
                                                                                foreach (\Drupal::service($service)->$method() as $k => $v) {
                                                                                        $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#options'][(string)$v] = (string)$v;
                                                                                }
                                                                                
                                                                                break;
                                                                }
                                                        }
                                                }
                                        }
                                        
                                        $this->storeField($varNode, $title, $desc, $placeholder);
                                        
                                        if ($addReferences) {
                                                
                                                $references = [];
                                                
                                                if ($valueNode ?? false) {
                                                        foreach ($xpath->evaluate('references/reference', $valueNode) as $_) {
                                                                $references[] = $_->nodeValue;
                                                        }
                                                }
                                                
                                                $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id') . '_references'] = [
                                                        '#type'     => 'select',
                                                        '#multiple' => true,
                                                        '#title'    => (string)$this->t('References'),
                                                        '#options'  => [],
                                                        '#prefix'   => '<div class="rwTablecell width50percent">',
                                                        
                                                        '#required'      => $varNode->getAttribute('referencesrequired') ?? false,
                                                        '#default_value' => array_key_exists($varNode->getAttribute('id') . '_references', $formdata ?? []) ? array_values($formdata[$varNode->getAttribute('id') . '_references']) : $references,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'prevent'         => 'blur',
                                                                'progress'        => [
                                                                        'type' => 'none',
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true,
                                                                'wrapper'         => 'messages',
                                                        ],
                                                ];
                                                
                                                $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id') . '_link'] = [
                                                        '#type'   => 'link',
                                                        '#title'  => (string)$this->t('Add (further) references'),
                                                        '#url'    => \Drupal\Core\Url::fromRoute('publisso_gold.book.template', ['id' => $this->ID(), 'action' => 'references']),
                                                        '#suffix' => '</div></div></div><hr>',
                                                ];
                                                
                                                foreach ($xpath->evaluate('/root/references/reference') as $refNode) {
                                                        $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id') . '_references']['#options'][$refNode->getAttribute('md5')] = $refNode->nodeValue;
                                                }
                                        } else {
                                                if ($varNode->getAttribute('type') != 'markup')
                                                        $form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#suffix'] = ($form[$varNode->getAttribute('formgroup')]['content'][$varNode->getAttribute('id')]['#suffix'] ?? '') . '<hr>';
                                        }
                                }
                        }
                        
                        $form['ctrl#version'] = [
                                '#type'     => 'number',
                                '#value'    => $version ?? 1,
                                '#title'    => (string)$this->t('Version'),
                                '#weight'   => ++$maxFormGroupWeight,
                                '#disabled' => true,
                                '#access'   => false,
                        ];
                        
                        $form['ctrl#create_version'] = [
                                '#type'     => 'checkbox',
                                '#value'    => $createVersion ?? 0,
                                '#title'    => (string)$this->t('Create version'),
                                '#weight'   => ++$maxFormGroupWeight,
                                '#disabled' => true,
                                '#access'   => false,
                        ];
                        
                        if ($dataHasWorkflowItem) {
                                
                                $form['ctrl#wf_id'] = [
                                        '#type'  => 'value',
                                        '#value' => $dataWorkflowItemID,
                                ];
                        }
                        
                        $form['ctrl#has_workflow'] = [
                                '#type'  => 'value',
                                '#value' => (int)$dataHasWorkflowItem,
                        ];
                        
                        $options = [];
                        foreach ($versioningFields as $_) {
                                $options[$_] = $_;
                        }
                        
                        $form['ctrl#versioning-fields'] = [
                                '#type'     => 'select',
                                '#title'    => (string)$this->t('Versioning fields'),
                                '#options'  => $options,
                                '#value'    => $versioningFields,
                                '#multiple' => true,
                                '#weight'   => ++$maxFormGroupWeight,
                                '#disabled' => true,
                                '#access'   => false,
                        ];
                        
                        return $form;
                }
        
                /**
                 * @param null $uid
                 * @return mixed|null
                 */
                public function getCachedForm($uid = null){
                        
                        if(!$uid) $uid = Publisso::currentUser()->getId();
        
                        $qry = \Drupal::database()->select('rwPubgoldBookchapterTemplateArticles', 't')->fields('t', ['form']);
        
                        $cond = $qry->andConditionGroup();
                        $cond->condition('uid', $uid, '=');
                        $cond->condition('template_id', $this->id, '=');
                        $qry->condition($cond);
        
                        if ($qry->countQuery()->execute()->fetchField() == 0) return null;
                        $form = $qry->execute()->fetchField();
                        return !is_null($form) ? unserialize($form) : null;
                }
        
                /**
                 * @param array $form
                 * @param null $uid
                 */
                public function setCachedForm(array $form, $uid = null){
                        
                        if(!$uid) $uid = Publisso::currentUser()->getId();
                        
                        \Drupal::database()->update('rwPubgoldBookchapterTemplateArticles')->fields(['form' => serialize($form)])
                                ->condition('template_id', $this->id, '=')
                                ->condition('uid', $uid, '=')
                                ->execute();
                }
        
                /**
                 * @param string $xml
                 * @param $filter
                 * @param null $formdata
                 * @return array
                 */
                public function getFormArticle(string $xml, $filter, $formdata = null) {
                        
                        $origXML = $this->getElement('template');
                        $this->setElement('template', $xml, false);
                        
                        return $this->getForm($filter, $formdata);
                        $this->setElement('template', $origXML, false);
                }
        
                /**
                 * @param $xmlString
                 * @param array $filter
                 * @return int
                 */
                public function getMaxDataVersion($xmlString, array $filter) {
                        
                        $xml = new \DOMDocument();
                        $xml->loadXML($xmlString);
                        $xpath = new \DOMXPath($xml);
                        $versioningFields = [];
                        $qry = '';
                        
                        foreach ($xpath->evaluate('/root/metas/meta[@id="versioning-fields"]/fields/field') as $_) {
                                $versioningFields[] = $_->getAttribute('field-id');
                        }
                        
                        $found = true;
                        foreach ($versioningFields as $field) {
                                if (!array_key_exists($field, $filter)) $found = false;
                        }
                        
                        if ($found) { //alle Versionierungsparameter wurden im Filter gefunden
                                
                                foreach ($filter as $k => $v) {
                                        
                                        if (in_array($k, $versioningFields))
                                                $qry .= (!empty($qry) ? ' and ' : '') . '@' . $k . '="' . $v . '"';
                                }
                                
                                $qry = "//data/values[$qry]";
                        }
                        
                        if ($qry) {
                                
                                $valuesNodeNL = $xpath->evaluate($qry);
                                
                                if ($valuesNodeNL->length) {
                                        
                                        $valueNode = $valuesNodeNL->item(0);
                                        $maxversion = $valueNode->getAttribute('maxversion');
                                        return $maxversion;
                                }
                        }
                        
                        return 1;
                }
        
                /**
                 * @param $xmlString
                 * @param array $filter
                 * @return |null
                 */
                public function getDataVersionWorkflowID($xmlString, array $filter) {
                        
                        $xml = new \DOMDocument();
                        $xml->loadXML($xmlString);
                        $xpath = new \DOMXPath($xml);
                        $versioningFields = [];
                        $qry = '';
                        
                        foreach ($xpath->evaluate('/root/metas/meta[@id="versioning-fields"]/fields/field') as $_) {
                                $versioningFields[] = $_->getAttribute('field-id');
                        }
                        
                        $found = true;
                        foreach ($versioningFields as $field) {
                                
                                if (!array_key_exists($field, $filter)) $found = false;
                        }
                        
                        if ($found) { //alle Versionierungsparameter wurden im Filter gefunden
                                
                                foreach ($filter as $k => $v) {
                                        
                                        if (in_array($k, $versioningFields))
                                                $qry .= (!empty($qry) ? ' and ' : '') . '@' . $k . '="' . $v . '"';
                                }
                                
                                $qry = "//data/values[$qry]";
                        }
                        
                        if ($qry && array_key_exists('version', $filter)) {
                                
                                $valueNodeNL = $xpath->evaluate($qry . '/value[@version="' . $filter['version'] . '"]');
                                
                                if ($valueNodeNL->length) {
                                        
                                        $valueNode = $valueNodeNL->item(0);
                                        return $valueNode->getAttribute('wf_id') ?? null;
                                }
                        }
                        
                        return null;
                }
        
                /**
                 * @param $xmlString
                 * @param array $filter
                 * @param $wf_id
                 */
                public function setDataVersionWorkflowID($xmlString, array $filter, $wf_id) {
                        
                        $xml = new \DOMDocument();
                        $xml->loadXML($xmlString);
                        $xpath = new \DOMXPath($xml);
                        $versioningFields = [];
                        $qry = '';
                        
                        foreach ($xpath->evaluate('/root/metas/meta[@id="versioning-fields"]/fields/field') as $_) {
                                $versioningFields[] = $_->getAttribute('field-id');
                        }
                        
                        $found = true;
                        foreach ($versioningFields as $field) {
                                
                                if (!array_key_exists($field, $filter)) $found = false;
                        }
                        
                        if ($found) { //alle Versionierungsparameter wurden im Filter gefunden
                                
                                foreach ($filter as $k => $v) {
                                        
                                        if (in_array($k, $versioningFields))
                                                $qry .= (!empty($qry) ? ' and ' : '') . '@' . $k . '="' . $v . '"';
                                }
                                
                                $qry = "//data/values[$qry]";
                        }
                        
                        if ($qry && array_key_exists('version', $filter)) {
                                
                                $valueNodeNL = $xpath->evaluate($qry . '/value[@version="' . $filter['version'] . '"]');
                                
                                foreach ($valueNodeNL as $valueNode) {
                                        $valueNode->setAttribute('wf_id', $wf_id);
                                }
                        }
                        
                        $this->saveTemplateArticle($xml->saveXML());
                }
        
                /**
                 * @param $xmlString
                 * @param array $filter
                 * @return bool
                 */
                public function isDataVersionPublished($xmlString, array $filter) {
                        
                        $xml = new \DOMDocument();
                        $xml->loadXML($xmlString);
                        $xpath = new \DOMXPath($xml);
                        $versioningFields = [];
                        $qry = '';
                        
                        foreach ($xpath->evaluate('/root/metas/meta[@id="versioning-fields"]/fields/field') as $_) {
                                $versioningFields[] = $_->getAttribute('field-id');
                        }
                        
                        $found = true;
                        foreach ($versioningFields as $field) {
                                
                                if (!array_key_exists($field, $filter)) $found = false;
                        }
                        
                        if ($found) { //alle Versionierungsparameter wurden im Filter gefunden
                                
                                foreach ($filter as $k => $v) {
                                        
                                        if (in_array($k, $versioningFields))
                                                $qry .= (!empty($qry) ? ' and ' : '') . '@' . $k . '="' . $v . '"';
                                }
                                
                                $qry = "//data/values[$qry]";
                        }
                        
                        if ($qry && array_key_exists('version', $filter)) {
                                
                                $valueNodeNL = $xpath->evaluate($qry . '/value[@version="' . $filter['version'] . '"]');
                                
                                if ($valueNodeNL->length) {
                                        
                                        $valueNode = $valueNodeNL->item(0);
                                        return !!$valueNode->getAttribute('published');
                                }
                        }
                        
                        return false;
                }
        
                /**
                 * @param $xml
                 * @param array $filter
                 * @return int
                 */
                public function getNextDataVersion($xml, array $filter) {
                        
                        $maxversion = $this->getMaxDataVersion($xml, $filter);
                        $published = $this->isDataVersionPublished($xml, array_merge($filter, ['version' => $maxversion]));
                        return $published ? ++$maxversion : $maxversion;
                }
        
                /**
                 * @param \DOMDocument $xml
                 * @param string $field
                 * @param array $versioningFields
                 * @param int $version
                 * @return |null
                 */
                public function getDataValueNode(\DOMDocument &$xml, string $field, array $versioningFields, int $version = 1) {
                        
                        $xpath = new \DOMXPath($xml);
                        
                        $qry = '';
                        foreach ($versioningFields as $k => $v) {
                                $qry .= (!empty($qry) ? " and " : "") . "@$k=\"$v\"";
                        }
                        if (!empty($qry)) $qry = "[$qry]";
                        
                        $varNode = $xpath->evaluate('/root/template/variables/variable[@id="' . $field . '"]');
                        
                        if (!$varNode->length) return null;
                        
                        $varNode = $varNode->item(0);
                        
                        $dataNode = $xpath->evaluate('data', $varNode);
                        if (!$dataNode->length) $dataNode = \Drupal::service('publisso_gold.tools')->DOMAppendChild('data', '', [], $xml, $varNode);
                        else $dataNode = $dataNode->item(0);
                        
                        $valuesNode = $xpath->evaluate('data/values' . $qry, $varNode);
                        if (!$valuesNode->length) {
                                $valuesNode = \Drupal::service('publisso_gold.tools')->DOMAppendChild('values', '', array_merge(['maxversion' => 1], $versioningFields), $xml, $dataNode);
                                \Drupal::service('publisso_gold.tools')->DOMAppendChild('value', '', ['version' => 1, 'published' => 0], $xml, $valuesNode);
                                $version = 1;
                        } else $valuesNode = $valuesNode->item(0);
                        
                        $valueNode = $xpath->evaluate('value[@version="' . $version . '"]', $valuesNode);
                        if (!$valueNode->length) $valueNode = \Drupal::service('publisso_gold.tools')->DOMAppendChild('value', '', ['version' => $version, 'published' => 0], $xml, $valuesNode);
                        else $valueNode = $valueNode->item(0);
                        
                        if ($valueNode->getAttribute('version') > $valuesNode->getAttribute('maxversion')) $valuesNode->setAttribute('maxversion', $valueNode->getAttribute('version'));
                        
                        return $valueNode;
                }
        
                /**
                 * @param string $ParamXML
                 * @return array
                 */
                public function getStoredDataFilter(string $ParamXML) {
                        
                        $xml = new \DOMDocument();
                        $xml->loadXML($ParamXML);
                        $xpath = new \DOMXPath($xml);
                        $versioningFields = [];
                        $filter = [];
                        
                        foreach ($xpath->evaluate('/root/metas/meta[@id="versioning-fields"]/fields/field') as $_) {
                                $versioningFields[] = $_->getAttribute('field-id');
                        }
                        
                        //erstes Variablen-Element
                        $nl = $xpath->evaluate('/root/template/variables/variable[position()=1]');
                        
                        if ($nl->length) {
                                
                                $varNode = $nl->item(0);
                                $valuesNodeNL = $xpath->evaluate('data/values', $varNode);
                                
                                foreach ($valuesNodeNL as $valuesNode) {
                                        
                                        $ary = [];
                                        
                                        for ($i = 0; $i < $valuesNode->attributes->length; $i++) {
                                                
                                                $attrNode = $valuesNode->attributes->item($i);
                                                $name = $attrNode->nodeName;
                                                
                                                if (in_array($name, $versioningFields)) {
                                                        
                                                        $value = $attrNode->nodeValue;
                                                        $ary[$name] = $value;
                                                }
                                        }
                                        
                                        $filter[implode(' ', array_values($ary))] = $ary;
                                }
                        }
                        
                        ksort($filter);
                        return $filter;
                }
                
                public function getUrlFromWorkflow(int $wf_id): ?Url {
                        
                        $xml = $this->getTemplateArticle();
                        
                        $doc = new \DOMDocument();
                        $doc->loadXML($xml);
                        $xpath = new \DOMXPath($doc);
                        
                        //first node with wf_id
                        $nl = $xpath->evaluate('//value[@wf_id="' . $wf_id . '"]');
                        
                        if ($nl->length) {
                                
                                $node = $nl->item(0)->parentNode; //values-node
                                $filter = array_shift($this->getStoredDataFilter($xml));
                                $filter['id'] = $this->id;
                                $filter['action'] = 'fill';
                                
                                return Url::fromRoute('publisso_gold.book.template', $filter);
                        }
                        
                        return null;
                }
        
                /**
                 * @param array $fields
                 * @throws \Exception
                 */
                protected function storeVersioningFields(array &$fields){
                        
                        $qry = \Drupal::database()->select('rwPubgoldBookchapterTemplates_Filter', 't')
                                ->fields('t', [])->condition('template_id', $this->id, '=');
                        
                        if(!$qry->countQuery()->execute()->fetchField()){
                                \Drupal::database()->insert('rwPubgoldBookchapterTemplates_Filter')
                                        ->fields([
                                                'template_id' => $this->id,
                                                'fields' => implode('|', array_map('trim', array_filter($fields)))
                                        ])->execute();
                        }
                }
        
                /**
                 * @param \DOMElement $varNode
                 * @param string $label
                 * @param string|null $description
                 * @param string|null $placeholder
                 * @return bool
                 * @throws \Exception
                 */
                protected function storeField(\DOMElement &$varNode, string $label, string $description = null, string $placeholder = null){
                        
                        foreach(['label', 'description', 'placeholder'] as $_){
                                ${$_} = trim(${$_}); if(empty(${$_})) ${$_} = null;
                        }
                        
                        if($varNode->getAttribute('type') == 'markup') return true;
                        
                        $attributes = [];
                        
                        if($varNode->hasAttributes()) {
                                foreach ($varNode->attributes as $node) {
                
                                        $attributes[$node->nodeName] = $node->nodeValue;
                                }
                        }
                        
                        $qry = \Drupal::database()->select('rwPubgoldBookchapterTemplates_Fields', 't')->fields('t', [])
                                ->condition('template_id', $this->id, '=')
                                ->condition('identifier', $varNode->getAttribute('id'), '=');
                        
                        if(!$qry->countQuery()->execute()->fetchField()){
                                \Drupal::database()->insert('rwPubgoldBookchapterTemplates_Fields')->fields([
                                        'template_id' => $this->id,
                                        'identifier' => $varNode->getAttribute('id'),
                                        'label' => $label,
                                        'description' => $description,
                                        'placeholder' => $placeholder,
                                        'attributes' => serialize($attributes)
                                ])->execute();
                        }
                }
        }

?>
