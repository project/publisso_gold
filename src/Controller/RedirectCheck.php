<?php
        
        
        namespace Drupal\publisso_gold\Controller;
        
        
        use Drupal\Core\Url;
        use Symfony\Component\EventDispatcher\EventSubscriberInterface;
        use Symfony\Component\HttpFoundation\Request;
        use Symfony\Component\HttpKernel\Event\GetResponseEvent;
        use Symfony\Component\HttpKernel\KernelEvents;
        use Symfony\Component\HttpFoundation\RedirectResponse;

        /**
         * Class RedirectCheck
         * @package Drupal\publisso_gold\Controller
         */
        class RedirectCheck implements EventSubscriberInterface {
        
                /**
                 * @return array|mixed
                 */
                public static function getSubscribedEvents(){
                        
                        $events[KernelEvents::REQUEST][] = array('checkSession');
                        $events[KernelEvents::REQUEST][] = array('checkForRedirection');
                        $events[KernelEvents::CONTROLLER][] = array('clearMenu');
                        return $events;
                }
                
                public function clearMenu(){
                        \Drupal::cache('menu')->invalidateAll(); // for clearing the menu cache
                        \Drupal::service('plugin.manager.menu.link')->rebuild(); // rebuild the menu
                }
        
                /**
                 * @param GetResponseEvent $event
                 */
                public function checkSession(GetResponseEvent $event){
                        
                        $session = $event->getRequest()->getSession();
                        if (!$session->isStarted()) {
                                $session->migrate();
                        }
        
                        $session->set('lastaccess', time());
                }
        
                /**
                 * @param GetResponseEvent $event
                 * @return bool|null
                 */
                public function checkForRedirection(GetResponseEvent $event){
                        
                        if($event->isMasterRequest()) {
                                
                                //Domain redirects
                                $config = \Drupal::config('publisso_gold.config');
                                $redirects = $config->get('domain_redirect') ?? [];
                                foreach($redirects as $k => $v){
                                        $redirects[str_replace(':', '.', $k)] = $v;
                                        unset($redirects[$k]);
                                }
                                $request = \Drupal::requestStack()->getCurrentRequest();
                                $url = $request->getHttpHost().$request->getRequestUri();
                                
                                foreach($redirects as $_url => $_route){
                                        
                                        if(preg_match("/^".str_replace('/', '\/', $_url)."(\/?)$/", $url)){
                                                
                                                try {
                                                        $route = Url::fromRoute($_route);
                                                        #$route->setAbsolute();
                                                        #$route = preg_replace('/^http(s?):\/\//', '', $route->toString());
                                                        $event->setResponse(new RedirectResponse($route->toString(), 301));
                                                        return true;
                                                }
                                                catch(\Exception $e){
                                                        Publisso::log($e->getMessage());
                                                }
                                        }
                                }
                                
                                $noRedirect = [
                                        'publisso_gold.userprofile.delete',
                                        'publisso_gold.useridentity'
                                ];
        
                                $session = $event->getRequest()->getSession();
                                $url = Url::fromRoute(\Drupal::routeMatch()->getRouteName(), \Drupal::routeMatch()->getParameters());
                                
                                if(in_array(\Drupal::routeMatch()->getRouteName(), $noRedirect)) return null;
        
                                if($session->get('logged_in') === true){
                
                                        $user = new \Drupal\publisso_gold\Controller\User($session->get('user')['id']);
                                        $ret = null;
                
                                        //Datenschutzerklärung
                                        if(($ret === null && ($user->getId() && $user->profile->agreedLatestPrivacyProtection() === false)) && \Drupal::service('publisso_gold.tools')->systemHasPrivacyProtectInformation() && !$session->has('orig_user')){
                                                $ret = ['route' => 'publisso_gold.privacy_protection', 'params' => ['action' => 'agree']];
                                        }
                
                                        //unsent mails in spooler?
                                        if($ret === null){
                                                if(\Drupal::database()->select('rwPubgoldMailout', 't')->fields('t', [])->condition('ready_to_send', 0, '=')->condition('created_by_uid', \Drupal::service('session')->get('user')['id'])->countQuery()->execute()->fetchField()){
                                
                                                        $ret = [
                                                                'route'  => 'publisso_gold.mailspool',
                                                                'params' => []
                                                        ];
                                                }
                                        }
                
                                        //if password change required and called path is not password-change-form -> redirect
                                        if($ret === null){
                        
                                                $unprivUser = $session->get('user')['weight'] < 70;
                        
                                                if($session->has('orig_user')){
                                                        $orig_user = $session->get('orig_user');
                                                        $unprivUser = !!($unprivUser & $orig_user['weight'] < 70);
                                                }
                        
                                                if($session->has('user') && $session->get('user')['pw_change'] == 1 && $unprivUser){
                                
                                                        $ret = [
                                                                'route'  => 'publisso_gold.change_password',
                                                                'params' => []
                                                        ];
                                
                                                }
                                        }
                
                                        //has user to update profile
                                        if($ret === null){
                        
                                                if(\Drupal::service('session')->has('user') && \Drupal::service('session')->get('user')['id']){
                                                        $currentUser = new User(\Drupal::service('session')->get('user')['id']);
                                                        if($currentUser->profile->getElement('edited') == -1){
                                        
                                                                $ret = [
                                                                        'route'  => 'publisso_gold.userprofile_edit',
                                                                        'params' => []
                                                                ];
                                                        }
                                                }
                                        }
                
                                        //verhindere Endlosschleife
                                        if($ret != null && $ret['route'] != \Drupal::routeMatch()->getRouteName()){
                        
                                                $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
                                                
                                                if(!$tempstore->get('urlReferer')) {
                                                        $tempstore->set( 'urlReferer', $url );
                                                }
                                                
                                                $event->setResponse(new RedirectResponse(Url::fromRoute($ret['route'], $ret['params'])->toString(), 303));
                                        }
                                }
                        }
                }
        }
