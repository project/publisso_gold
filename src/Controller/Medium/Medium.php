<?php
        
        namespace Drupal\publisso_gold\Controller\Medium;
        
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Url;
        use \Drupal\publisso_gold\Controller\Interfaces\MediumInterface;
        use Drupal\publisso_gold\Controller\Publisso;
        use \Drupal\publisso_gold\Controller\User;
        use \Drupal\publisso_gold\Controller\TableOfContent;
        
        use \Drupal\Core\Link;

        /**
         * Class Medium
         * @package Drupal\publisso_gold\Controller\Medium
         */
        class Medium extends ControllerBase implements MediumInterface {
                protected $id;
                protected $databaseTable;
                protected $databaseTable_Authors;
                protected $databaseTable_Editors;
                protected $databaseTable_EOs;
                protected $databaseTable_EiCs;
                protected $databaseTable_Reviewers;
                protected $databaseTable_SustainingMembers;
                
                protected $databaseTableColumnPrefix;
                protected $databaseTableColumnPrefix_Authors;
                protected $databaseTableColumnPrefix_Editors;
                protected $databaseTableColumnPrefix_EOs;
                protected $databaseTableColumnPrefix_EiCs;
                protected $databaseTableColumnPrefix_Reviewers;
                protected $databaseTableColumnPrefix_SustainingMembers;
                
                protected $elements = [];
                protected $loadNoChilds;
                protected $url;
                protected $cover_link;
                protected $routeName;
                protected $routeParams;
                protected $mediumType;
                protected $subMediumType;
                protected $mediumClass;
                protected $subMediumClass;
                protected $authors = [];
                protected $editors = [];
                protected $editorialoffice = [];
                protected $editorsinchief = [];
                protected $advisoryboard = [];
                protected $editorialboard = [];
                protected $sustainingmembers = [];
                protected $reviewers = [];
                protected $processes = [];
                protected $tableofcontent = [];
                protected $eb_ab_annotations;
        
                /**
                 * Medium constructor.
                 * @param null $id
                 * @param false $noChilds
                 */
                public function __construct ($id = null, $noChilds = false ) { }
        
                /**
                 * @param $a
                 * @param $b
                 * @return int|mixed
                 */
                private static function sortEditorialBoard ($a, $b ) {
                        if(!array_key_exists('weight', $a) || !array_key_exists('weight', $b)) return 0;
                        return $a[ 'weight' ] ?? 0 <=> $b[ 'weight' ] ?? 0;
                }
        
                /**
                 * @param $id
                 * @param false $loadNoChilds
                 */
                public function load ($id, $loadNoChilds = false ) { }
        
                /**
                 * @return array
                 */
                public function getElements () {
                        return $this->elements;
                }
        
                /**
                 * @param string $title
                 * @return false|null
                 */
                public function getIDFromTitle (string $title ) {
                        
                        try {
                                $id = \Drupal::database()->select( $this->databaseTable, 't' )->fields( 't', [ $this->databaseTableColumnPrefix . 'id' ] )->condition( $this->databaseTableColumnPrefix . 'title', $title, '=' )->execute()->fetchField();
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                                \Drupal::service( 'messenger' )->addError( 'An error occured. Please inform the site-admin.' . $e->getMessage() );
                                return false;
                        }
                        finally {
                                return $id ?? null;
                        }
                }
        
                /**
                 * @return mixed|null
                 */
                public function getCoverLink () {
                        return empty( $this->elements[ 'cover_link' ] ) ? $this->cover_link : $this->getElement( 'cover_link' );
                }
        
                /**
                 * @return mixed
                 */
                public function getUrl () {
                        return $this->url;
                }
        
                /**
                 * @param $uid
                 * @throws \Exception
                 */
                public function addEditor ($uid ) {
                        
                        if ( $uid ) {
                                try {
                                        if ( !\Drupal::database()->select( $this->databaseTable_Editors, 't' )->fields( 't', [] )->condition( $this->databaseTableColumnPrefix_Editors . 'uid', $uid, '=' )->condition( $this->databaseTableColumnPrefix_Editors . $this->mediumType . 'id', $this->getElement( 'id' ), '=' )->countQuery()->execute()->fetchField() ) {
                                                \Drupal::database()->insert( $this->databaseTable_Editors )->fields( [
                                                                                                                             $this->databaseTableColumnPrefix_Editors . 'uid' => $uid, $this->databaseTableColumnPrefix_Editors . $this->mediumType . 'id' => $this->getElement( 'id' ),
                                                                                                                     ]
                                                )->execute()
                                                ;
                                        }
                                }
                                catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                        \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                                }
                                finally {
                                        $this->initEditors();
                                }
                        }
                }
        
                /**
                 * @param array $editor_uids
                 * @throws \Exception
                 */
                public function setEditors (array $editor_uids ) {
                        
                        try {
                                \Drupal::database()->delete( $this->databaseTable_Editors )->condition( $this->databaseTableColumnPrefix_Editors . $this->mediumType . 'id', $this->id, '=' )->execute();
                                
                                foreach ( $editor_uids as $uid ) {
                                        \Drupal::database()->insert( $this->databaseTable_Editors )->fields( [
                                                                                                                     $this->databaseTableColumnPrefix_Editors . $this->mediumType . 'id' => $this->id, $this->databaseTableColumnPrefix_Editors . 'uid' => $uid,
                                                                                                             ]
                                        )->execute()
                                        ;
                                }
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                        }
                        finally {
                                $this->initEditors();
                        }
                }
        
                /**
                 * @return array
                 */
                public function readEditors () {
                        
                        foreach ( $this->editors as $k => $v ) {
                                if ( gettype( $v ) != 'object' ) $this->editors[ $k ] = new User( $v );
                        }
                        
                        return $this->editors;
                }
        
                /**
                 * @param int $uid
                 * @throws \Exception
                 */
                public function addEditorialOffice (int $uid ) {
                        
                        if ( $uid ) {
                                
                                try {
                                        if ( !\Drupal::database()->select( $this->databaseTable_EOs, 't' )->fields( 't', [] )->condition( $this->databaseTableColumnPrefix_EOs . 'uid', $uid, '=' )->condition( $this->databaseTableColumnPrefix_EOs . ( str_replace( '_', '', $this->databaseTableColumnPrefix ) ) . 'id', $this->getElement( 'id' ), '=' )->countQuery()->execute()->fetchField() ) {
                                                \Drupal::database()->insert( $this->databaseTable_EOs )->fields( [
                                                                                                                         $this->databaseTableColumnPrefix_EOs . 'uid' => $uid, $this->databaseTableColumnPrefix_EOs . ( str_replace( '_', '', $this->databaseTableColumnPrefix ) ) . 'id' => $this->getElement( 'id' ),
                                                                                                                 ]
                                                )->execute()
                                                ;
                                        }
                                }
                                catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                        \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                                }
                                finally {
                                        $this->initEditorialOffice();
                                }
                        }
                }
        
                /**
                 * @param array $eo_uids
                 * @throws \Exception
                 */
                public function setEditorialOffice (array $eo_uids ) {
                        
                        try {
                                \Drupal::database()->delete( $this->databaseTable_EOs )->condition(
                                        $this->databaseTableColumnPrefix_EOs . ( str_replace( '_', '', $this->databaseTableColumnPrefix ) )
                                        . 'id',
                                        $this->id, '='
                                )
                                       ->execute()
                                ;
                                
                                foreach ( $eo_uids as $uid ) {
                                        \Drupal::database()->insert( $this->databaseTable_EOs )
                                               ->fields( [
                                                                 $this->databaseTableColumnPrefix_EOs . ( str_replace( '_', '', $this->databaseTableColumnPrefix ) ) . 'id' =>
                                                                         $this->id, $this->databaseTableColumnPrefix_EOs . 'uid'                                            => $uid,
                                                         ]
                                               )->execute()
                                        ;
                                }
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                        }
                        finally {
                                $this->initEditorialOffice();
                        }
                }
        
                /**
                 * @return array
                 */
                public function readEditorialOffice () {
                        
                        foreach ( $this->editorialoffice as $k => $v ) {
                                
                                if ( gettype( $v ) != 'object' ) $this->editorialoffice[ $k ] = Publisso::User($v);
                        }
                        
                        return $this->editorialoffice;
                }
        
                /**
                 * @param $uid
                 * @throws \Exception
                 */
                public function addEditorsInChief ($uid ) {
                        
                        if ( $uid ) {
                                
                                try {
                                        if ( !\Drupal::database()->select( $this->databaseTable_EiCs, 't' )->fields( 't', []
                                        )->condition( $this->databaseTableColumnPrefix_EiCs . 'uid', $uid, '=' )->condition(
                                                $this->databaseTableColumnPrefix_EiCs . str_replace( '_', '', $this->databaseTableColumnPrefix ) . 'id',
                                                $this->getElement(
                                                        'id'
                                                ), '='
                                        )->countQuery()->execute()->fetchField() ) {
                                                \Drupal::database()->insert( $this->databaseTable_EiCs )->fields( [
                                                                                                                          $this->databaseTableColumnPrefix_EiCs . 'uid' => $uid,
                                                                                                                          $this->databaseTableColumnPrefix_EiCs . ( str_replace( '_', '', $this->databaseTableColumnPrefix ) ) . 'id'
                                                                                                                                                                        => $this->getElement( 'id' ),
                                                                                                                  ]
                                                )->execute()
                                                ;
                                        }
                                }
                                catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                        \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                                }
                                finally {
                                        $this->initEditorsInChief();
                                }
                        }
                }
        
                /**
                 * @param array $eic_uids
                 * @throws \Exception
                 */
                public function setEditorsInChief (array $eic_uids ) {
                        
                        
                        try {
                                \Drupal::database()->delete( $this->databaseTable_EiCs )->condition(
                                        $this->databaseTableColumnPrefix_EiCs . ( str_replace( '_', '', $this->databaseTableColumnPrefix ) ) . 'id',
                                        $this->id, '='
                                )
                                       ->execute()
                                ;
                                
                                foreach ( $eic_uids as $uid ) {
                                        \Drupal::database()->insert( $this->databaseTable_EiCs
                                        )->fields( [
                                                           $this->databaseTableColumnPrefix_EiCs . ( str_replace( '_', '',
                                                                                                                  $this->databaseTableColumnPrefix
                                                           ) ) . 'id' => $this->id, $this->databaseTableColumnPrefix_EiCs . 'uid' => $uid,
                                                   ]
                                        )->execute()
                                        ;
                                }
                                
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                        }
                        finally {
                                $this->initEditorsInChief();
                        }
                        
                }
        
                /**
                 * @return array
                 */
                public function readEditorsInChief () {
                        
                        foreach ( $this->editorsinchief as $k => $v ) {
                                if ( gettype( $v ) != 'object' ) $this->editorsinchief[ $k ] = new User( $v );
                        }
                        
                        return $this->editorsinchief;
                }
        
                /**
                 * @param $uid
                 * @throws \Exception
                 */
                public function addReviewer ($uid ) {
                        
                        if ( $uid ) {
                                
                                try {
                                        if ( !\Drupal::database()->select( $this->databaseTable_Reviewers, 't' )->fields(
                                                't', []
                                        )->condition( $this->databaseTableColumnPrefix_Reviewers . 'uid', $uid,
                                                      '='
                                        )->condition(
                                                $this->databaseTableColumnPrefix_Reviewers
                                                . $this->mediumType . 'id',
                                                $this->getElement( 'id' ), '='
                                        )->countQuery()->execute()->fetchField() ) {
                                                \Drupal::database()->insert( $this->databaseTable_Reviewers )->fields( [
                                                                                                                               $this->databaseTableColumnPrefix_Reviewers . 'uid' => $uid,
                                                                                                                               $this->databaseTableColumnPrefix_Reviewers . $this->mediumType . 'id'
                                                                                                                                                                                  => $this->getElement( 'id' ),
                                                                                                                       ]
                                                )->execute()
                                                ;
                                        }
                                }
                                catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                        \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                                }
                                finally {
                                        $this->initReviewers();
                                }
                        }
                }
        
                /**
                 * @param array $reviewer_uids
                 * @throws \Exception
                 */
                public function setReviewers (array $reviewer_uids ) {
                        
                        try {
                                
                                \Drupal::database()->delete( $this->databaseTable_Reviewers )->condition(
                                        $this->databaseTableColumnPrefix_Reviewers . $this->mediumType . 'id',
                                        $this->id, '='
                                )
                                       ->execute()
                                ;
                                
                                foreach ( $reviewer_uids as $uid ) {
                                        \Drupal::database()->insert( $this->databaseTable_Reviewers )->fields( [
                                                                                                                       $this->databaseTableColumnPrefix_Reviewers . $this->mediumType . 'id' =>
                                                                                                                               $this->id, $this->databaseTableColumnPrefix_Reviewers . 'uid' =>
                                                                                                                               $uid,
                                                                                                               ]
                                        )->execute()
                                        ;
                                }
                                
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                        }
                        finally {
                                $this->initReviewers();
                        }
                }
        
                /**
                 * @return array
                 */
                public function readReviewers () {
                        
                        foreach ( $this->reviewers as $k => $v ) {
                                if ( gettype( $v ) != 'object' ) $this->reviewers[ $k ] = new User( $v );
                        }
                        
                        return $this->reviewers;
                }
        
                /**
                 * @param $uid
                 * @return bool
                 */
                public function isUserReviewer ($uid ) {
                        
                        foreach ( $this->reviewers as $user ) {
                                
                                if ( gettype( $user ) == 'object' && $user->getElement( 'id' ) == $uid ) {
                                        return true;
                                        break;
                                }
                                elseif ( gettype( $user ) != 'object' && $user == $uid ) {
                                        return true;
                                        break;
                                }
                                else {
                                        continue;
                                }
                        }
                        
                        return false;
                }
        
                /**
                 * @param null $uid
                 * @return bool
                 */
                public function isUserAuthor ($uid = null ) {
                        
                        if ( $uid === null ) $uid = \Drupal::service( 'session' )->get( 'user' )[ 'id' ];
                        
                        foreach ( $this->authors as $user ) {
                                
                                if ( gettype( $user ) == 'object' && $user->getElement( 'id' ) == $uid ) {
                                        return true;
                                        break;
                                }
                                elseif ( gettype( $user ) != 'object' && $user == $uid ) {
                                        return true;
                                        break;
                                }
                                else {
                                        continue;
                                }
                        }
                        
                        return false;
                }
        
                /**
                 * @param null $uid
                 * @return bool
                 */
                public function isUserEditor ($uid = null ) {
                
                        if ( $uid === null ) $uid = \Drupal::service( 'session' )->get( 'user' )[ 'id' ];
                
                        foreach ( $this->editors as $user ) {
        
                                if ( gettype( $user ) == 'object' && $user->getElement( 'id' ) == $uid ) {
                                        return true;
                                        break;
                                }
                                elseif ( gettype( $user ) != 'object' && $user == $uid ) {
                                        return true;
                                        break;
                                }
                                else {
                                        continue;
                                }
                        }
                }
        
                /**
                 * @param null $uid
                 * @return bool
                 */
                public function isUserEditorialOffice ($uid = null ) {
                
                        if ( $uid === null ) $uid = \Drupal::service( 'session' )->get( 'user' )[ 'id' ];
                
                        foreach ( $this->editorialoffice as $user ) {
        
                                if ( gettype( $user ) == 'object' && $user->getElement( 'id' ) == $uid ) {
                                        return true;
                                        break;
                                }
                                elseif ( gettype( $user ) != 'object' && $user == $uid ) {
                                        return true;
                                        break;
                                }
                                else {
                                        continue;
                                }
                        }
                
                        return false;
                }
        
                /**
                 * @param null $uid
                 * @return bool
                 */
                public function isUserEditorInChief ($uid = null ) {
                
                        if ( $uid === null ) $uid = \Drupal::service( 'session' )->get( 'user' )[ 'id' ];
                
                        foreach ( $this->editorsinchief as $user ) {
        
                                if ( gettype( $user ) == 'object' && $user->getElement( 'id' ) == $uid ) {
                                        return true;
                                        break;
                                }
                                elseif ( gettype( $user ) != 'object' && $user == $uid ) {
                                        return true;
                                        break;
                                }
                                else {
                                        continue;
                                }
                        }
                }
        
                /**
                 * @param int $id
                 * @return bool
                 */
                public function hasAuthor(int $id){
                        return $this->isUserAuthor($id);
                }
        
                /**
                 * @param $uid
                 * @throws \Exception
                 */
                public function addAuthor ($uid ) {
                        
                        if ( $uid ) {
                                
                                try {
                                        if ( !\Drupal::database()->select( $this->databaseTable_Authors, 't' )->fields( 't',
                                                                                                                        []
                                        )
                                                     ->condition(
                                                             $this->databaseTableColumnPrefix_Authors . 'uid', $uid, '='
                                                     )->condition( $this->databaseTableColumnPrefix_Authors . $this->mediumType . 'id', $this->getElement( 'id' ), '=' )->countQuery()->execute()->fetchField() ) {
                                                \Drupal::database()->insert( $this->databaseTable_Authors )->fields( [
                                                                                                                             $this->databaseTableColumnPrefix_Authors . 'uid' => $uid,
                                                                                                                             $this->databaseTableColumnPrefix_Authors . $this->mediumType . 'id'
                                                                                                                                                                              => $this->getElement( 'id' ),
                                                                                                                     ]
                                                )->execute()
                                                ;
                                        }
                                }
                                catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                        \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                                }
                                finally {
                                        $this->initAuthors();
                                }
                        }
                }
        
                /**
                 * @param array $author_uids
                 * @throws \Exception
                 */
                public function setAuthors (array $author_uids ) {
                        
                        try {
                                
                                \Drupal::database()->delete( $this->databaseTable_Authors )->condition(
                                        $this->databaseTableColumnPrefix_Authors . $this->mediumType . 'id',
                                        $this->id, '='
                                )
                                       ->execute()
                                ;
                                
                                foreach ( $author_uids as $uid ) {
                                        \Drupal::database()->insert(
                                                $this->databaseTable_Authors
                                        )->fields( [
                                                           $this->databaseTableColumnPrefix_Authors . $this->mediumType . 'id' =>
                                                                   $this->id,
                                                           $this->databaseTableColumnPrefix_Authors . 'uid'                    => $uid,
                                                   ]
                                        )->execute()
                                        ;
                                }
                                
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                        }
                        finally {
                                $this->initAuthors();
                        }
                }
        
                /**
                 * @param $process_name
                 * @return false|mixed
                 */
                public function getMailout ($process_name ) {
                        
                        foreach ( $this->processes as $pid => $process ) {
                                
                                if ( $process[ 'name' ] == $process_name ) {
                                        
                                        if ( array_key_exists( 'mailout', $process ) ) {
                                                return $process[ 'mailout' ];
                                        }
                                        else {
                                                return false;
                                        }
                                }
                        }
                        
                        return false;
                }
        
                /**
                 * @param $element_name
                 * @return bool
                 */
                public function hasControlElement ($element_name ) {
                        return in_array( $element_name, $this->getControlKeys() );
                }
        
                /**
                 * @return array
                 */
                public function getControlKeys () {
                        return @array_keys(  $this->getElement( 'control' ), true ) ?? [] ;
                }
        
                /**
                 * @param $element_name
                 * @param $element_value
                 */
                public function setControlElement ($element_name, $element_value ) {
                        $control = $this->elements[ 'control' ];
                        $control[ $element_name ] = $element_value;
                        $this->setElement( 'control', $control );
                }
        
                /**
                 * @param $elemName
                 * @param null $elemValue
                 * @param bool $storeOnDB
                 * @return bool
                 */
                public function setElement ($elemName, $elemValue = null, $storeOnDB = true ) {
                        
                        if ( array_key_exists( $elemName, $this->elements ) ) {
                                
                                $oldValue = $this->elements[ $elemName ];
                                $this->elements[ $elemName ] = $elemValue;
                                
                                if ( $storeOnDB === false ) return true;
                                
                                if ( $this->save( $elemName ) === false ) {
                                        $this->elements[ $elemName ] = $oldValue;
                                        return false;
                                }
                                else {
                                        return true;
                                }
                        }
                        else {
                                return false;
                        }
                }
        
                /**
                 * @param null $elemName
                 * @return bool
                 */
                public function save ($elemName = null ) {
                        
                        $fields = $this->elements;
                        
                        if ( $elemName !== null ) { //if specific element selected
                                $fields = [ $elemName => $fields[ $elemName ] ];
                        }
                        
                        foreach ( $fields as $k => $var ) {
                                $fields[ $this->databaseTableColumnPrefix . $k ] = (is_object($var) || is_array($var)) ? serialize($var) : $var;
                                unset( $fields[ $k ] );
                        }
                        
                        try {
                                \Drupal::database()->update( $this->databaseTable )->fields( $fields )->condition(
                                        $this->databaseTableColumnPrefix . 'id', $this->id
                                )->execute()
                                ;
                                return true;
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                                return false;
                        }
                }
                
                public function getDataKeys () {
                        return $this->getKeys();
                }
                
                public function getKeys () {
                        return $this->getElementKeys();
                }
        
                /**
                 * @return array
                 */
                public function getElementKeys () {
                        return array_keys( $this->elements );
                }
        
                /**
                 * @return array
                 */
                public function getTableOfContent () {
                        return $this->tableofcontent;
                }
        
                /**
                 * @param string $type
                 * @return array
                 */
                public function getTOCStructure ($type = 'default' ) {
                        
                        if ( $type == 'default' ) return $this->tableofcontent->getStructure();
                        elseif ( $type == 'flat' ) return $this->flat( $this->tableofcontent->getStructure() );
                }
        
                /**
                 * @param array $array
                 * @return array
                 */
                protected function flat (array $array ) {
                        
                        $ret = [];
                        array_walk_recursive( $array, function ( $a ) use ( &$ret ) { $ret[] = $a; } );
                        return $ret;
                }
        
                /**
                 * @return int|void
                 */
                public function countAuthors () {
                        return count( $this->authors );
                }
        
                /**
                 * @return array
                 */
                public function readAuthors () {
                        
                        foreach ( $this->authors as $k => $v ) {
                                if ( gettype( $v ) != 'object' ) $this->authors[ $k ] = new User( $v );
                        }
                        
                        return $this->authors;
                }
        
                /**
                 * @return array
                 */
                public function readEditorialBoard () {
                        return $this->editorialboard;
                }
        
                /**
                 * @return array
                 */
                public function readSustainingMembers () {
                        return $this->sustainingmembers;
                }
        
                /**
                 * @param string $name
                 * @return mixed|null
                 */
                public function getEbAbAnnotation ($name = '' ) {
                        return $this->eb_ab_annotations[ $name ] ?? null;
                }
        
                /**
                 * @return array|null
                 */
                public function readAdvisoryBoard () {
                        return $this->advisoryboard ?? null;
                }
        
                /**
                 * @param $element_name
                 * @return mixed|null
                 */
                public function getControlElement ($element_name ) {
                        
                        return $this->elements['control'][ $element_name ] ?? null;
                }
        
                /**
                 * @return bool
                 */
                protected function init () {
                        
                        if ( !$this->id ) return false;
                        
                        $this->elements = [];
                        $qry = \Drupal::database()->select( $this->databaseTable, 't' )->fields( 't', [] );
                        
                        if(preg_match('/^\d+$/', $this->id))
                                $qry->condition( $this->databaseTableColumnPrefix . 'id', $this->id, '=' );
                        else
                                $qry->condition( $this->databaseTableColumnPrefix . 'title', $this->id, '=' );
                        
                        try {
                                $res = $qry->execute()->fetchAssoc();
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( $e->getMessage() );
                                \Drupal::service( 'messenger' )->addError( 'An error occured. Please inform the site-admin.' );
                                return false;
                        }
                        finally {
                                if ( isset( $res ) && is_array( $res ) ) {
                                        foreach ( $res as $k => $v ) {
                                                if ( $k != $this->databaseTableColumnPrefix . 'id' ) $this->elements[ str_replace( $this->databaseTableColumnPrefix, '', $k ) ] = $v;
                                        }
                                }
        
                                //unserialize data
                                foreach($this->elements as $k => $v){
                                        if(false !== ($data = @unserialize($this->elements[$k]))) $this->elements[$k] = $data;
                                }
                                
                                $this->initUrl();
                                $this->initEditors();
                                $this->initEditorialOffice();
                                $this->initEditorsInChief();
                                $this->initReviewers();
                                $this->initSustainingMembers();
                                $this->initAuthors();
                                $this->parseEditorialBoard();
                                $this->initProcesses();
                                $this->initMailtemplates();
                                
                                $this->tableofcontent = new TableOfContent( null, [
                                        'medium' => $this->mediumType, 'id' =>
                                                $this->id,
                                ]
                                );
                                
                                return true;
                        }
                }
                
                protected function initUrl () {
                        
                        if ( empty( $this->getElement( 'cover_link' ) ) ) {
                                
                                $this->routeParams[ $this->databaseTableColumnPrefix . 'id' ] = $this->getElement( 'id' );
                                
                                try {
                                        $this->url = \Drupal\Core\Url::fromRoute( $this->routeName, $this->routeParams );
                                }
                                catch ( \Symfony\Component\Routing\Exception\InvalidParameterException $e ) {
                                        
                                        $this->routeParams[ $this->databaseTableColumnPrefix . 'id' ] = \urlencode( $this->routeParams[ $this->databaseTableColumnPrefix . 'id' ] );
                                        
                                        try {
                                                $this->url = \Drupal\Core\Url::fromRoute($this->routeName, $this->routeParams);
                                        }
                                        catch ( \Symfony\Component\Routing\Exception\InvalidParameterException $e ) {
                                                \Drupal::service( 'messenger' )->addError( 'Can\'t create Url for book "' . $this->getElement( 'title' ) . '" => ' . $e->getMessage() );
                                        }
                                }
                                finally {
                                        $this->cover_link = $this->url->toString();
                                }
                        }
                        else {
                                try {
                                        $this->url = \Drupal\Core\Url::fromUri( $this->getElement( 'cover_link' ) );
                                }
                                catch ( \Symfony\Component\Routing\Exception\InvalidParameterException $e ) {
                                        \Drupal::service( 'messenger' )->addError( 'Can\'t create Url for book "' . $this->getElement( 'title' ) . '" => ' . $e->getMessage() );
                                        $this->routeParams[ $this->databaseTableColumnPrefix . 'id' ] = $this->getElement( 'id' );
                                        
                                        
                                        $this->url = \Drupal\Core\Url::fromRoute( $this->routeName, $this->routeParams );
                                }
                                finally {
                                        $this->cover_link = $this->url->toString();
                                }
                        }
                }
        
                /**
                 * @param string $elemName
                 * @return mixed|null
                 */
                public function getElement (string $elemName ) {
                        if ( $elemName == 'id' ) return $this->getId();
                        return $this->elements[ $elemName ] ?? null;
                }
                
                public function getId () {
                        return $this->id;
                }
        
                protected function initEditors () {
                        
                        try {
                                $editors = \Drupal::database()->select( $this->databaseTable_Editors, 'be' )->fields( 'be', [] )->condition( $this->databaseTableColumnPrefix_Editors . $this->mediumType . 'id', $this->id, '=' )->execute()->fetchAll();
                                
                                $this->editors = [];
                                
                                foreach ( $editors as $editor ) {
                                        
                                        $this->editors[] = $editor->{$this->databaseTableColumnPrefix_Editors . 'uid'};
                                }
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                        }
                }
        
                protected function initEditorialOffice () {
                        
                        try {
                                $editors = \Drupal::database()->select( $this->databaseTable_EOs, 'beo' )->fields( 'beo', [] )->condition( $this->databaseTableColumnPrefix_EOs . ( str_replace( '_', '', $this->databaseTableColumnPrefix ) ) . 'id', $this->id, '=' )->execute()->fetchAll();
                                
                                $this->editorialoffice = [];
                                
                                foreach ( $editors as $editor ) {
                                        $this->editorialoffice[] = $editor->{$this->databaseTableColumnPrefix_EOs . 'uid'};
                                }
                                
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                        }
                }
        
                protected function initEditorsInChief () {
                        
                        try {
                                $editors = \Drupal::database()->select( $this->databaseTable_EiCs, 'beic' )->fields( 'beic', [] )->condition( $this->databaseTableColumnPrefix_EiCs . ( str_replace( '_', '', $this->databaseTableColumnPrefix ) ) . 'id', $this->id, '=' )->execute()->fetchAll();
                                
                                $this->editorsinchief = [];
                                
                                foreach ( $editors as $editor ) {
                                        $this->editorsinchief[] = $editor->{$this->databaseTableColumnPrefix_EiCs . 'uid'};
                                }
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                        }
                }
        
                protected function initReviewers () {
                        
                        try {
                                $editors = \Drupal::database()->select( $this->databaseTable_Reviewers, 'br' )->fields( 'br', [] )->condition( $this->databaseTableColumnPrefix_Reviewers . $this->mediumType . 'id', $this->id, '=' )->execute()->fetchAll();
                                
                                $this->reviewers = [];
                                
                                foreach ( $editors as $editor ) {
                                        $this->reviewers[] = $editor->{$this->databaseTableColumnPrefix_Reviewers . 'uid'};
                                }
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                        }
                }
        
                protected function initSustainingMembers () {
                        
                        if($this->databaseTable_SustainingMembers) {
                                try {
                                        $result = \Drupal::database()->select($this->databaseTable_SustainingMembers, 'bsm'
                                        )->fields('bsm', [])->condition($this->databaseTableColumnPrefix_SustainingMembers . str_replace('_', '', $this->databaseTableColumnPrefix) . 'id', $this->id, '=')->execute()->fetchAll();
                
                                        $this->sustainingmembers = [];
                
                                        foreach ($result as $_) $this->sustainingmembers[] = $_->{substr($this->mediumType, 0, 1) . 'sm_id'};
                                } catch (\Drupal\Core\Database\DatabaseExceptionWrapper $e) {
                                        \Drupal::service('publisso_gold.tools')->logMsg(__METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage());
                                }
                        }
                }
        
                protected function initAuthors () {
                        
                        if($this->databaseTable_Authors) {
                                try {
                                        $result = \Drupal::database()->select( $this->databaseTable_Authors, 'ba' )->fields( 'ba', [] )->condition( $this->databaseTableColumnPrefix_Authors . $this->mediumType . 'id', $this->id, '=' )->execute()->fetchAll();
                
                                        $this->authors = [];
                
                                        foreach ( $result as $_ ) {
                                                $this->authors[] = $_->{$this->databaseTableColumnPrefix_Authors . 'uid'};
                                        }
                                }
                                catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                        \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                                }
                        }
                }
        
                /**
                 * @return |null
                 */
                protected function parseEditorialBoard () {
                        
                        if ( !$this->getElement( 'editorial_board' ) ) return null;
                        
                        $eb = json_decode( $this->getElement( 'editorial_board' ), 1 );
                        
                        if ( !array_key_exists( 'main', $eb ) ) {
                                
                                uasort( $eb, [ 'self', 'sortEditorialBoard' ] );
                                
                                foreach ( $eb as $uid => $data ) {
                                        
                                        $user = new User( $uid );
                                        if ( !$user->profile->getElement( 'show_data_in_boards' ) ) continue;
                                        
                                        if ( array_key_exists( 'show_in_editorial_board', $data ) && $data[ 'show_in_editorial_board' ] == 1 ) {
                                                
                                                $this->editorialboard[ $uid ] = $user;
                                                
                                                foreach ( $data as $key => $show ) {
                                                        
                                                        if ( $show == 0 ) {
                                                                //disable data not shown, but don't store in database
                                                                $this->editorialboard[ $uid ]->profile->setElement( str_replace( 'profile_', '', $key ), '', false );
                                                        }
                                                }
                                        }
                                }
                        }
                        else {
                                
                                $ab = array_key_exists('advisory', $eb) ? $eb[ 'advisory' ] : [];
                                $this->eb_ab_annotations[ 'main' ] = $eb[ 'annotation' ] ?? null;
                                uasort( $eb[ 'main' ], [ 'self', 'sortEditorialBoard' ] );
                                
                                foreach ( $eb[ 'main' ] as $uid => $data ) {
                                        
                                        $user = new User( $uid );
                                        
                                        if ( !$user->profile->getElement( 'show_data_in_boards' ) ) continue;
                                        if ( $data[ 'show_in_editorial_board' ] == 1 ) {
                                                
                                                $this->editorialboard[ $uid ] = $user;
                                                $this->editorialboard[ $uid ]->tempInfo[ 'eb_notes' ] = $data[ 'notes' ];
                                                
                                                foreach ( $data as $key => $show ) {
                                                        
                                                        if ( $show == 0 ) {
                                                                //disable data not shown, but don't store in database
                                                                $this->editorialboard[ $uid ]->profile->setElement( str_replace( 'profile_', '', $key ), '', false );
                                                        }
                                                }
                                        }
                                }
        
                                //advisory boards
                                foreach ( $ab as $advisoryBoard ) {
                                        
                                        $advisoryBoard = (array)$advisoryBoard;
                                        
                                        $user = (array)$advisoryBoard[ 'user' ];
                                        $name = $advisoryBoard[ 'name' ];
                                        $this->eb_ab_annotations[ $name ] = $advisoryBoard[ 'annotation' ] ?? null;
                                        //echo '<pre>'.print_r($user, 1).'</pre>';
                                        uasort( $user, [ 'self', 'sortEditorialBoard' ] );
                                        //echo '<pre>'.print_r($user, 1).'</pre>';
                                        $this->advisoryboard[ $name ] = [];
                                        
                                        foreach ( $user as $uid => $data ) {
                                                
                                                $user = new User( $uid );
                                                if ( !$user->profile->getElement( 'show_data_in_boards' ) ) continue;
                                                
                                                if ( $data[ 'show_in_editorial_board' ] == 1 ) {
                                                        
                                                        $this->advisoryboard[ $name ][ $uid ] = $user;
                                                        $this->advisoryboard[ $name ][ $uid ]->tempInfo[ 'eb_notes' ] = $data[ 'notes' ] ?? null;
                                                        
                                                        foreach ( $data as $key => $show ) {
                                                                
                                                                if ( $show == 0 ) {
                                                                        //disable data not shown, but don't store in database
                                                                        $this->advisoryboard[ $name ][ $uid ]->profile->setElement( str_replace( 'profile_', '', $key ), '', false );
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                }
        
                protected function initProcesses () {
                        
                        try {
                                $result = \Drupal::database()->select( 'rwPubgoldProcesses', 't' )->fields( 't', [] )->execute()->fetchAll();
                                
                                foreach ( $result as $res ) {
                                        $this->processes[ $res->pc_id ][ 'name' ] = $res->pc_name;
                                }
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                        }
                }
        
                protected function initMailtemplates () {
                        
                        foreach ( $this->processes as $pc_id => $process ) {
                                
                                $result = \Drupal::database()->select( 'rwPubgoldMailassignement', 't' )->fields( 't', [ 'ma_mailtemplate_id', 'ma_attachment' ] )->condition( 'ma_medium_type', $this->mediumType, '=' )->condition( 'ma_medium_id', $this->id, '=' )->condition( 'ma_process_id', $pc_id, '=' )->execute()->fetchAssoc();
                                
                                $attachment = [];
                                
                                if ( !empty( $result[ 'ma_mailtemplate_id' ] ) ) {
                                        
                                        if ( !empty( $result[ 'ma_attachment' ] ) ) $attachment = array_filter( json_decode( $result[ 'ma_attachment' ], true ) );
                                        
                                        $result = \Drupal::database()->select( 'rwPubgoldMailtemplates', 't' )->fields( 't', [] )->condition( 'id', $result[ 'ma_mailtemplate_id' ], '=' )->execute()->fetchAssoc();
                                        
                                        $this->processes[ $pc_id ][ 'mailout' ] = $result;
                                        $this->processes[ $pc_id ][ 'mailout' ][ 'attachment' ] = $attachment;
                                }
                                else {
                                        
                                        $result = \Drupal::database()->select( 'rwPubgoldMailassignement', 't' )->fields( 't', [ 'ma_mailtemplate_id', 'ma_attachment' ] )->condition( 'ma_medium_type', 'default', '=' )->condition( 'ma_process_id', $pc_id, '=' )->execute()->fetchAssoc();
                                        
                                        if ( !empty( $result[ 'ma_attachment' ] ) ) $attachment = array_filter( json_decode( $result[ 'ma_attachment' ], true ) );
                                        
                                        if ( !empty( $result[ 'ma_mailtemplate_id' ] ) ) {
                                                
                                                $qry = \Drupal::database()->select( 'rwPubgoldMailtemplates', 't' )->fields( 't', [] )->condition( 'id', $result[ 'ma_mailtemplate_id' ], '=' );
                                                
                                                $result = $qry->execute()->fetchAssoc();
                                                $this->processes[ $pc_id ][ 'mailout' ] = $result;
                                                $this->processes[ $pc_id ][ 'mailout' ][ 'attachment' ] = $attachment;
                                        }
                                }
                        }
                }
                
                protected function initSpecific () { }
        
                /**
                 * @param null[] $data
                 * @throws \Exception
                 */
                protected function setSustainingMember ($data = ['image' => null, 'type' => null ] ) {
                        
                        if ( array_key_exists( 'image', $data ) && $data[ 'image' ] != null && array_key_exists( 'type', $data ) && $data[ 'type' ] != null ) {
                                
                                try {
                                        \Drupal::database()->insert( $this->databaseTable_SustainingMembers )->fields( [
                                                                                                                               $this->databaseTableColumnPrefix_SustainingMembers . str_replace( '_', '',
                                                                                                                                                                                                 $this->databaseTableColumnPrefix
                                                                                                                               ) . 'id' => $this->id, $this->databaseTableColumnPrefix_SustainingMembers . 'image'
                                                                                                                                        => $data[ 'image' ], 'type' => $data[ 'type' ],
                                                                                                                       ]
                                        )->execute()
                                        ;
                                }
                                catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                        \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                                }
                                $this->initSustainingMembers();
                        }
                }
        
                /**
                 * @param $active_tab
                 * @param $template
                 */
                protected function overwriteActiveTabs ($active_tab, $template ) {
                        
                        //overwrite active tabs/content?
                        if ( !empty( $active_tab ) ) {
                                
                                $sects = [ 'chapter_content', 'manuscript_guidelines', 'about', 'editorial_board', 'authors', 'imprint', 'overview_chapters', 'media' ];
                                
                                if ( in_array( $active_tab, $sects ) ) {
                                        
                                        foreach ( $sects as $sect ) {
                                                $template->setVar( "tab_" . $sect . "_class_active", '' );
                                                $template->setVar( "content_" . $sect . "_class_active", '' );
                                        }
                                        
                                        $template->setVar( 'tab_' . $active_tab . '_class_active', 'active' );
                                        $template->setVar( 'content_' . $active_tab . '_class_active', 'active' );
                                }
                        }
                }
        
                /**
                 * @return string|string[]|null
                 */
                protected function getTOC () {
                        
                        $template = new \Drupal\publisso_gold\Controller\Template();
                        $template->get( 'publisso_gold_' . $this->mediumType . '_' . $this->subMediumType . '_list' );
                        $template->setVar( $this->mediumType . '_title', $this->getElement( 'title' ) );
                        $template->setVar( 'txt_table_of_contents', (string)$this->t( 'Table of Content' ) );
                        
                        $toc = new \Drupal\publisso_gold\Controller\TableOfContent( false, [
                                'medium' =>
                                        $this->mediumType, 'id'
                                         => $this->getElement( 'id' ),
                        ]
                        );
                        
                        $template->setVar( $this->subMediumType . '_list', '<div id="nav">' . $this->parseTOCStructure(
                                                                                 $toc->readStructure(), 1
                                                                         ) . '</div>'
                        );
                        
                        return $template->parse();
                }
        
                /**
                 * @param $structure
                 * @param $level
                 * @return string
                 */
                protected function parseTOCStructure ($structure, $level ) {
                        
                        $markup1 = '';
                        if ( count( $structure ) > 0 ) {
                                
                                $markup1 .= '<ul class="lvl' . $level . '">';
                                
                                if ( is_array( $structure ) ) {
                                        foreach ( $structure as $item ) {
                                                
                                                $childs = $item[ 'childs' ];
                                                $item = $item[ 'item' ];
                                                $LNK = $URL = null;
                                                
                                                $markup1 .= '<li><div class="li-wrapper"><div class="t"><div class="tr"><div class="td td-icon"><span id="icon"></span></div><div class="td td-text">';
                                                
                                                $link = false;
                                                
                                                if ( !empty( $item->getElement( 'link' ) ) ) {
                                                        
                                                        $link = $item->getElement( 'link' );
                                                        
                                                        if ( substr( $link, 0, 9 ) == 'intern://' ) {
                                                                
                                                                $link = str_replace( 'intern://', '', $link );
                                                                [ $_, $sub_medium_id ] = explode( '/',
                                                                                                      $link
                                                                );
                                                                
                                                                $subMedium = new $this->subMediumClass( $sub_medium_id, true );
                                                                $url = $subMedium->getLink( 'url' );
                                                                $link = Link::fromTextAndUrl( $item->getElement( 'title' ), $url );
                                                        }
                                                        elseif(UrlHelper::isValid($link)){
                                                                $url = Url::fromUri($link, ['attributes' =>
                                                                        ['target' => '_blank']]);
                                                                $link = Link::fromTextAndUrl($item->getElement('title'), $url);
                                                        }
                                                }
                                                
                                                $markup1 .= ( $link ? $link->toString() : $item->getElement( 'title' ) ) . '</div></div></div></div>';
                                                
                                                if ( count( $childs ) > 0 ) {
                                                        $markup1 .= $this->parseTOCStructure( $childs, $level + 1 );
                                                }
                                                
                                                $markup1 .= '</li>';
                                        }
                                }
                                
                                $markup1 .= '</ul>';
                        }
                        
                        return $markup1;
                }
        }
