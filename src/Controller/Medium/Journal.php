<?php
        
        namespace Drupal\publisso_gold\Controller\Medium;
        
        use Drupal\Component\Utility\UrlHelper;
        use \Drupal\publisso_gold\Controller\Bookchapter;
        use Drupal\publisso_gold\Controller\Issue;
        use Drupal\publisso_gold\Controller\Journalarticle;
        use \Drupal\publisso_gold\Controller\Template;
        use \Drupal\publisso_gold\Controller\TableOfContent;
        use Drupal\publisso_gold\Controller\Volume;
        use \Drupal\publisso_gold\Controller\Workflow;
        use \Drupal\Core\Url;
        use \Drupal\Core\Link;
        use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
        use Zend\Feed\Uri;
        use \Drupal\Core\Render\Markup;

        /**
         * Class Journal
         * @package Drupal\publisso_gold\Controller\Medium
         */
        class Journal extends Medium {
                
                private $articles;
                private $volumes;
                private $issues;
        
                /**
                 * Journal constructor.
                 * @param null $id
                 * @param false $noChilds
                 */
                public function __construct ($id = null, $noChilds = false ) {
                        
                        $this->mediumType = 'journal';
                        $this->subMediumType = 'article';
                        
                        $this->databaseTable = self::DBTBL_PREFIX . 'Journals';
                        $this->databaseTableColumnPrefix = 'jrn_';
                        
                        $this->databaseTable_Editors = self::DBTBL_PREFIX . 'JournalEditors';
                        $this->databaseTableColumnPrefix_Editors = 'je_';
                        
                        $this->databaseTable_EOs = self::DBTBL_PREFIX . 'JournalsEditorialOffice';
                        $this->databaseTableColumnPrefix_EOs = 'jeo_';
                        
                        $this->databaseTable_EiCs = self::DBTBL_PREFIX . 'JournalsEditorsInChief';
                        $this->databaseTableColumnPrefix_EiCs = 'jeic_';
                        
                        $this->databaseTable_Reviewers = self::DBTBL_PREFIX . 'JournalReviewers';
                        $this->databaseTableColumnPrefix_Reviewers = 'jr_';
                        
                        $this->databaseTable_SustainingMembers = self::DBTBL_PREFIX . 'JournalsSustainingMembers';
                        $this->databaseTableColumnPrefix_SustainingMembers = 'jsm_';
                        
                        $this->routeName = 'publisso_gold.journals_journal';
                        $this->routeParams = [];
                        
                        $this->mediumClass = '\Drupal\publisso_gold\Controller\Medium\Journal';
                        $this->subMediumClass = '\Drupal\publisso_gold\Controller\Journalarticle';
                        
                        $this->articles = $this->volumes = $this->issues = [];
                        if ( $id ) $this->load( $id, $noChilds );
                }
        
                /**
                 * @param $id
                 * @param false $noChilds
                 */
                public function load ($id, $noChilds = false ) {

                        $this->id = $id;
                        $this->routeParams[ $this->databaseTableColumnPrefix . 'id' ] = $id;
                        $this->init();
                        
                        if ( !$noChilds ){
                                $this->initArticles();
                        }
                }
        
                /**
                 * @param $vol_id
                 * @return Volume|false|mixed
                 */
                public function getVolume ($vol_id ){
                        if ( !in_array( $vol_id, $this->volumes ) ) return false;
                        
                        if(is_object($this->volumes[$vol_id])) return $this->volumes[$vol_id];
                        else return new Volume($vol_id);
                }
                
                private function initArticles () {
                        
                        $result = \Drupal::database()->select( 'rwPubgoldJournalArticles', 'bc' )->fields( 'bc', [ 'jrna_id', 'jrna_volume', 'jrna_issue' ] )->condition( 'jrna_jrnid', $this->id, '=' )->execute()->fetchAll();
                        
                        $this->articles = [];
                        
                        foreach ( $result as $_ ){
                                
                                $this->articles[] = $_->jrna_id;
                                if($_->jrna_volume && !array_key_exists($_->jrna_volume, $this->volumes))
                                        $this->volumes[$_->jrna_volume] = new Volume($_->jrna_volume);
                                if($_->jrna_issue && !array_key_exists($_->jrna_issue, $this->issues))
                                        $this->issues[$_->jrna_issue] = new Issue($_->jrna_issue);
                                
                                
                        }
                }
        
                /**
                 * @param $id
                 * @return bool
                 */
                public function hasArticle($id){
                        return in_array($id, $this->articles);
                }
        
                /**
                 * @return array
                 */
                public function readArticles () {
                        
                        foreach ( $this->articles as $k => $article ) {
                                
                                if ( gettype( $article ) != 'object' ) {
                                        $this->articles[ $k ] = new Journalarticle( $article );
                                }
                        }
                        
                        return $this->articles;
                }
        
                /**
                 * @param $jrna_id
                 * @return Journalarticle|false|mixed
                 */
                public function readArticle ($jrna_id ) {
                        
                        if ( $jrna_id ) {
                                
                                if ( preg_match( '/^\d+$/', $jrna_id ) ) {
                                        
                                        foreach ( $this->articles as $article ) {
                                                
                                                if ( gettype( $article ) == 'object' && $article->getElement( 'id' ) ==
                                                                                        $jrna_id ) {
                                                        return $article;
                                                }
                                                elseif ( gettype( $article ) != 'object' && $article == $jrna_id ) {
                                                        return new Journalarticle( $jrna_id );
                                                }
                                                else {
                                                        continue;
                                                }
                                        }
                                }
                                else {
                                        return new Journalarticle( $jrna_id );
                                }
                        }
                        
                        return false;
                }
        
                /**
                 * @return mixed
                 */
                public function getCurrentVolumeID (){
                        return \Drupal::database()->select( 'rwPubgoldJournalVolume', 't' )->fields( 't', [ 'id' ] )->condition( 'jrn_id', $this->id, '=' )->orderBy( 'year', 'DESC' )->orderBy( 'number', 'DESC' )->execute()->fetchField();
                }
        
                /**
                 * @return Volume
                 */
                public function getCurrentVolume (){
                        return new \Drupal\publisso_gold\Controller\Volume( $this->getCurrentVolumeID()
                        );
                }
        
                /**
                 * @return array
                 */
                public function getVolumes (){
                        return $this->volumes;
                }
        }
