<?php
        
        namespace Drupal\publisso_gold\Controller\Medium;
        
        use Drupal\Component\Utility\UrlHelper;
        use \Drupal\publisso_gold\Controller\Bookchapter;
        use Drupal\publisso_gold\Controller\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\AbstractManager;
        use Drupal\publisso_gold\Controller\Manager\UserManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use \Drupal\publisso_gold\Controller\Template;
        use \Drupal\publisso_gold\Controller\TableOfContent;
        use Drupal\publisso_gold\Controller\user;
        use \Drupal\publisso_gold\Controller\Workflow;
        use \Drupal\Core\Url;
        use \Drupal\Core\Link;
        use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
        use Zend\Feed\Uri;
        use \Drupal\Core\Render\Markup;

        /**
         * Class Conference
         * @package Drupal\publisso_gold\Controller\Medium
         */
        class Conference extends Medium {
                
                private $days;
                private $session;
                private $papers;
                
                private $databaseTable_CMs;
                private $databaseTableColumnPrefix_CMs;
                private $conferencemanagers;
        
                /**
                 * Conference constructor.
                 * @param null $id
                 * @param false $noChilds
                 */
                public function __construct ($id = null, $noChilds = false ) {
                        
                        $this->mediumType = 'conference';
                        $this->subMediumType = 'paper';
                        
                        $this->databaseTable = self::DBTBL_PREFIX . 'Conferences';
                        $this->databaseTableColumnPrefix = 'cf_';
                        
                        $this->databaseTable_Editors = self::DBTBL_PREFIX . 'ConferencesEditors';
                        $this->databaseTableColumnPrefix_Editors = 'ce_';
                        
                        $this->databaseTable_EOs = self::DBTBL_PREFIX . 'ConferencesEditorialOffice';
                        $this->databaseTableColumnPrefix_EOs = 'ceo_';
                        
                        $this->databaseTable_EiCs = self::DBTBL_PREFIX . 'ConferencesEditorsInChief';
                        $this->databaseTableColumnPrefix_EiCs = 'ceic_';
                        
                        $this->databaseTable_Reviewers = self::DBTBL_PREFIX . 'ConferencesReviewers';
                        $this->databaseTableColumnPrefix_Reviewers = 'cr_';
        
                        $this->databaseTable_CMs = self::DBTBL_PREFIX . 'ConferencesConferenceManager';
                        $this->databaseTableColumnPrefix_CMs = 'cm_';
                        
                        $this->routeName = 'publisso_gold.conferences_conference';
                        $this->routeParams = [];
                        
                        $this->mediumClass = '\Drupal\publisso_gold\Controller\Medium\Conference';
                        $this->subMediumClass = '\Drupal\publisso_gold\Controller\ConferencePaper';
                        
                        $this->days = [];
                        $this->sessions = [];
                        $this->papers = [];
                        
                        if ( $id ) $this->load( $id, $noChilds );
                }
        
                /**
                 * @param $id
                 * @param false $noChilds
                 */
                public function load ($id, $noChilds = false ) {

                        $this->id = $id;
                        $this->routeParams[ $this->databaseTableColumnPrefix . 'id' ] = $id;
                        $this->init();
                        $this->initConferenceManager();
                        if ( !$noChilds ) $this->initPapers();
                }
                
                private function initConferenceManager(){
                        
                        $this->conferencemanagers = [];
        
                        try {
                                $cms = \Drupal::database()->select( $this->databaseTable_CMs, 'cm' )->fields( 'cm', [] )->condition( $this->databaseTableColumnPrefix_CMs . ( str_replace( '_', '', $this->databaseTableColumnPrefix ) ) . 'id', $this->id, '=' )->execute()->fetchAll();
                
                                foreach ( $cms as $cm ) {
                                        $this->conferencemanagers[] = $cm->{$this->databaseTableColumnPrefix_CMs . 'uid'};
                                }
                
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                        }
                        
                }
        
                /**
                 * @return mixed
                 */
                public function readConferenceManager(){
                        
                        foreach ( $this->conferencemanagers as $k => $v ) {
                
                                if ( gettype( $v ) != 'object' ) $this->conferencemanagers[ $k ] = UserManager::getUserFromID( $v );
                        }
         
                        return $this->conferencemanagers;
                }
                
                private function initPapers () { //todo
                }
        
                /**
                 * @param $id
                 * @return bool
                 */
                public function hasPaper($id){
                        return in_array($id, $this->papers);
                }
        
                /**
                 * @return array
                 */
                public function readPapers () {
                        
                        foreach ( $this->papers as $k => $paper ) {
                                
                                if ( gettype( $paper ) != 'object' ) {
                                        $this->papers[ $k ] = new Conferencepaper( $paper );
                                }
                        }
                        
                        return $this->papers;
                }
        
                /**
                 * @param $id
                 * @return Conferencepaper|false|mixed
                 */
                public function readPaper ($id ) {
                        
                        if ( $id ) {
                                
                                if ( preg_match( '/^\d+$/', $id ) ) {
                                        
                                        foreach ( $this->papers as $paper ) {
                                                
                                                if ( gettype( $paper ) == 'object' && $paper->getElement( 'id' ) ==
                                                                                        $id ) {
                                                        return $paper;
                                                }
                                                elseif ( gettype( $paper ) != 'object' && $paper == $id ) {
                                                        return new Conferencepaper( $id );
                                                }
                                                else {
                                                        continue;
                                                }
                                        }
                                }
                                else {
                                        return new Conferencepaper( $id );
                                }
                        }
                        
                        return false;
                }
        
                /**
                 * @param $cf_id
                 * @param $tab
                 * @param $pp_id
                 * @return mixed|string|null
                 */
                public function getPageTitle ($cf_id, $tab, $pp_id ) {
                        if ( !$this->id ) {
                                $this->id = $cf_id;
                                $this->load( $this->id );
                        }
                        $title = $this->getElement( 'title' );
                        
                        if ( $pp_id ) {
                                $paper = new Conferencepaper( $pp_id, true );
                                $title = (string)$this->t( '@conference: @paper', [ '@paper' => $paper->getElement( 'title' ), '@conference' => $this->getElement( 'title' ) ] );
                        }
                        
                        return $title;
                }
        
                /**
                 * @param $cf_id
                 * @return array
                 */
                public function edit ($cf_id ) {
                        
                        if ( !$this->id ) {
                                $this->id = $cf_id;
                                $this->load( $this->id );
                        }
                        
                        if ( !( $this->isUserEditorialOffice() || $this->isUserEditorInChief() || $this->isUserEditor() || \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'conferencemanagement' ) ) ) {
                                return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        }
                        
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'cf_id' => $cf_id ] );
                        
                        return \Drupal::formBuilder()->getForm( "Drupal\\publisso_gold\\Form\\publisso_goldEditConference", [ 'cf_id' => $cf_id ] );
                }
        
                /**
                 * @param $cf_id
                 * @param $tab
                 * @param null $pp_id
                 * @return array
                 */
                public function getHTML ($cf_id, $tab, $pp_id = null ) {

                        if(!\Drupal::database()->select($this->databaseTable, 't')->fields('t', [])->condition
                        ($this->databaseTableColumnPrefix.'id', $cf_id, '=')->countQuery()->execute()->fetchField())
                                throw new NotFoundHttpException();

                        $head = '';
                        if ( !$this->id ) {
                                $this->id = $cf_id;
                                $this->load( $this->id );
                        }
                        
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'cf_id' => $this->id ] );
                        
                        if ( $pp_id ) {

                                if(!$this->hasPaper($pp_id))
                                        throw new NotFoundHttpException();

                                \Drupal::service( 'session' )->set( 'breadcrumb_info', array_merge( [ 'pp_id' => $pp_id ], \Drupal::service( 'session' )->get( 'breadcrumb_info' ) ) );
                                $paper = new Conferencepaper( $pp_id );
                                $head = \Drupal::service( 'publisso_gold.tools' )->getPaperDCMeta( $paper, $this );
                        }
        
                        $ret = [
                                '#type' => 'container',
                                'content' => [
                                        'header' => [
                                                '#type' => 'inline_template',
                                                '#template' => $this->getHTMLHeader()
                                        ],
                                        'content' => [
                                                '#type' => 'inline_template',
                                                '#template' => $this->getHTMLContent( $pp_id ? $paper : null, $tab )
                                        ],
                                ],
                                '#cache' => [
                                        'max-age' => 0
                                ]
                        ];
                        
                        if ( !empty( $head ) ) {
                                $ret[ '#attached' ] = [ 'html_head' => $head ];
                        }
                        
                        return $ret;
                }
        
                /**
                 * @return string|string[]|null
                 */
                private function getHTMLHeader () {
                        
                        $session = \Drupal::service( 'session' );
                        $template = new Template();
                        $template->get( 'publisso_conference_header' );
                        return $template->parse();
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Conferencepaper|null $paper
                 * @param string $tab
                 * @return string|string[]|null
                 */
                private function getHTMLContent (\Drupal\publisso_gold\Controller\Conferencepaper $paper = null, $tab = 'about' ) {
                        
                        $tempstore = \Drupal::service( 'user.private_tempstore' )->get( 'publisso_gold' );
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        
                        $route[ 'route_name' ] = \Drupal::routeMatch()->getRouteName();
                        $route[ 'route_param' ] = \Drupal::routeMatch()->getRawParameters()->all();
                        
                        $template = new Template();
                        $template->get( 'publisso_conference_paper' );

                        return $template->parse();
                }
        
                /**
                 * @param array $eo_uids
                 * @throws \Exception
                 */
                public function setConferenceManager (array $eo_uids ) {
                
                        try {
                                \Drupal::database()->delete( $this->databaseTable_CMs )->condition(
                                        $this->databaseTableColumnPrefix_CMs . ( str_replace( '_', '', $this->databaseTableColumnPrefix ) )
                                        . 'id',
                                        $this->id, '='
                                )
                                       ->execute()
                                ;
                        
                                foreach ( $eo_uids as $uid ) {
                                        \Drupal::database()->insert( $this->databaseTable_CMs )
                                               ->fields( [
                                                                 $this->databaseTableColumnPrefix_CMs . ( str_replace( '_', '', $this->databaseTableColumnPrefix ) ) . 'id' =>
                                                                         $this->id, $this->databaseTableColumnPrefix_CMs . 'uid'                                            => $uid,
                                                         ]
                                               )->execute()
                                        ;
                                }
                        }
                        catch ( \Drupal\Core\Database\DatabaseExceptionWrapper $e ) {
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                        }
                        finally {
                                $this->initEditorialOffice();
                        }
                }
        
                /**
                 * @param null $uid
                 * @return bool
                 */
                public function isUserManager ($uid = null ) {
                
                        if ( $uid === null ) $uid = \Drupal::service( 'session' )->get( 'user' )[ 'id' ];
                
                        foreach ( $this->conferencemanagers as $user ) {
                        
                                if ( gettype( $user ) == 'object' && $user->getElement( 'id' ) == $uid ) {
                                        return true;
                                        break;
                                }
                                elseif ( gettype( $user ) != 'object' && $user == $uid ) {
                                        return true;
                                        break;
                                }
                                else {
                                        continue;
                                }
                        }
                
                        return false;
                }
        
                /**
                 * @return bool
                 */
                public function submissionPossible(){
                        return $this->getStatusText() === 'submission possible';
                }
                
                public function getStatusText() :string{
                        
                        $status = '';
                        
                        $qry = \Drupal::database()->select('rwPubgoldConferences', 'c')->fields('c', []);
                        $qry->condition('cf_id', $this->id);
                        
                        $qrySubmission = clone $qry;
                        $qrySubmission->where(
                                <<<EOT
                                case when (isnull(cf_date_submission_start) and isnull(cf_date_submission_end)) then 0
                                  else (
                                      case
                                          when not isnull(cf_date_submission_start) and not isnull(cf_date_submission_end)
                                              then date_format(now(), '%y-%m-%d') between cf_date_submission_start and cf_date_submission_end
                                          else (
                                              case
                                                  when not isnull(cf_date_submission_start)
                                                      then date_format(now(), '%y-%m-%d') >= cf_date_submission_start
                                                  else (
                                                      case
                                                          when not isnull(cf_date_submission_end)
                                                              then date_format(now(), '%y-%m-%d') <= cf_date_submission_end end
                                                      ) end
                                              ) end
                                      ) end
                                EOT
                        );
                        if(!!$qrySubmission->countQuery()->execute()->fetchField()) $status = 'submission possible';
                        else $status = 'submission closed';
                        
                        $qryPublished = clone $qry;
                        $qryPublished->condition('cf_published', 1, '=');
                        if(!!$qryPublished->countQuery()->execute()->fetchField()) $status = 'published';
                        
                        return $status;
                }
        
                /**
                 * @param false $abstractsIncl
                 * @return array
                 */
                public function getSessions($abstractsIncl = false){
                        
                        $result = [];
                        $dayCnt = $sessionCnt = $abstractCnt = 0;
                        $this->readSessionsFromDB(null, $result, $abstractsIncl);
                        foreach ( $result as $id => $row ) {
                                $row->cnt = ${$row->type."Cnt"}++;
                                $result[ $id ]->depth = $this->getSessionDepth( $row, 0, $result);
                        }
                        return $result;
                }
        
                /**
                 * @param null $id
                 * @param array $result
                 * @param false $abstractsIncl
                 */
                private function readSessionsFromDB($id = null, array &$result, $abstractsIncl = false){
                        
                        $qry = \Drupal::database()->select('rwPubgoldConferenceSessions', 't')->fields('t', []);
                        $qry->condition('cf_id', $this->getId(), '=');
                        if(!$abstractsIncl) $qry->condition('type', ['session', 'day'], 'IN');
                        if($id) $qry->condition('parent', $id, '='); else $qry->isNull('parent');
                
                        foreach($qry->orderBy('weight', 'ASC')->execute()->fetchAllAssoc('id') as $id => $row){
                        
                                $result[$row->id] = $row;
                        
                                $childs = \Drupal::database()->select('rwPubgoldConferenceSessions', 't')->fields('t', [])->condition('parent', $row->id, '=')->countQuery()->execute()->fetchField();
                                if(!!$childs){
                                        $result[$row->id]->cntChilds = $childs;
                                        $this->readSessionsFromDB($row->id, $result, $abstractsIncl);
                                }
                                else{
                                        $result[$row->id]->cntChilds = 0;
                                }
                        }
                }
        
                /**
                 * @param false $abstractsIncl
                 * @return \DOMDocument
                 */
                public function getSessionsXML($abstractsIncl = false){
                        
                        $sessions = $this->getSessions($abstractsIncl);
        
                        $xml = new \DOMDocument("1.0", "UTF-8");
                        $xpath = new \DOMXPath($xml);
                        
                        $rootNode = $xml->appendChild($xml->createElement('sessions'));
                        
                        foreach($sessions as $_){
                                if(!$_->parent){
                                        
                                        $node = $rootNode->appendChild($xml->createElement('session'));
                                        $node->setAttribute('id', $_->id);
                                }
                                else{
                                        $nl = $xpath->query('//session[@id="'.$_->parent.'"]');
                                        if($nl && $nl->length){
                                                $parentNode = $nl->item(0);
                                
                                                $nl = $xpath->query('sessions', $parentNode);
                                                if(!$nl || !$nl->length){
                                                        $sessionsNode = $parentNode->appendChild($xml->createElement('sessions'));
                                                }
                                                else{
                                                        $sessionsNode = $nl->item(0);
                                                }
                                
                                                $node = $sessionsNode->appendChild($xml->createElement('session'));
                                                $node->setAttribute('id', $_->id);
                                        }
                                }
                
                                foreach($_ as $k => $v){
                                        $node->appendChild($xml->createElement($k))->appendChild($xml->createCDATASection($v));
                                }
                        }
                        $xml->formatOutput = true;
                        return $xml;
                }
        
                /**
                 * @param null $expandedTree
                 * @param null $maxTitleLength
                 * @return \DOMDocument
                 */
                public function getSessionsUL($expandedTree = null, $maxTitleLength = null){
                
                        $sessions = $this->getSessions(true);
                        $xml = new \DOMDocument("1.0", "UTF-8");
                        $xpath = new \DOMXPath($xml);
                
                        $rootNode = $xml->appendChild($xml->createElement('ul'));
                        $rootNode->setAttribute('id', 'conference-session-list');
                        
                        foreach($sessions as $_){
                                
                                if(!$_->parent){
                                
                                        $node = $rootNode->appendChild($xml->createElement('li'));
                                        $node->setAttribute('id', $_->id);
                                }
                                else{
                                        $nl = $xpath->query('//li[@id="'.$_->parent.'"]');
                                        if($nl && $nl->length){
                                                $parentNode = $nl->item(0);
                                        
                                                $nl = $xpath->query('ul', $parentNode);
                                                if(!$nl || !$nl->length){
                                                        $sessionsNode = $parentNode->appendChild($xml->createElement('ul'));
                                                        $sessionsNode->setAttribute('child-of', $parentNode->getAttribute('id'));
                                                        $sessionsNode->setAttribute('class', 'collapse');
                                                        
                                                        $spanNode = $xpath->query('span', $parentNode)->item(0);
                                                        $spanNode->setAttribute('data-toggle', 'collapse');
                                                        $spanNode->setAttribute('data-target', 'ul[child-of=\''.$parentNode->getAttribute('id').'\']');
                                                        $spanNode->setAttribute('style', 'cursor: pointer; color: #375b9a;');
                                                }
                                                else{
                                                        $sessionsNode = $nl->item(0);
                                                }
                                        
                                                $node = $sessionsNode->appendChild($xml->createElement('li'));
                                                $node->setAttribute('id', $_->id);
                                        }
                                }
                                
                                if($_->type == 'day'){
                                        $value = $_->day;
                                }
                                elseif($_->type == 'session'){
                                        $from = $_->from ? date('H:i', strtotime($_->from)) : null;
                                        $to   = $_->to   ? date('H:i', strtotime($_->to  )) : null;
                                        
                                        $value = $_->title;#.(($from || $to ? " ($from &minus; $to)" : ''));
                                }
                                
                                if($_->cntChilds) $node->setAttribute('style', 'list-style: disclosure-closed;');
                                else $node->setAttribute('style', 'list-style: circle;');
                                
                                
                                
                                if($_->type == 'abstract'){
        
                                        $abstract = AbstractManager::getAbstract($_->ca_id);
                                        
                                        if($abstract->getElement('published')) {
                                                $a = $node->appendChild( $xml->createElement( 'a', (is_null($maxTitleLength) || strlen($_->title) <= $maxTitleLength) ? $_->title : substr($_->title, 0, $maxTitleLength).'...' ) );
                                                $link = Url::fromRoute( 'publisso_gold.conferences_conference.abstract', [ 'cf_id' => $this->getId(), 'ca_id' => $abstract->getId() ] )->toString();
                                                $a->setAttribute( 'href', $link );
                                        }
                                        else{
                                                $node->appendChild($xml->createElement('span', $_->title));
                                        }
                                }
                                else{
                                        $node->appendChild($xml->createElement('span', $value));
                                }
                        }
                        
                        
                        
                        if($expandedTree){
                                
                                $id = \Drupal::database()->select('rwPubgoldConferenceSessions', 't')->fields('t', ['id'])->condition('ca_id', $expandedTree, '=')->execute()->fetchField();
                                
                                $nl = $xpath->evaluate('//li[@id="'.$id.'"]');
                                
                                if($nl->length){
                                        
                                        $node = $nl->item(0);
                                        
                                        while($node->parentNode){
                                              
                                                $node = $node->parentNode;
                                                
                                                if($node && $node->nodeType == XML_ELEMENT_NODE){
                                                        
                                                        if($node->hasAttribute('class') && $node->getAttribute('class') == 'collapse'){
                                                                $node->setAttribute('class', 'collapse in marked');
                                                                $node->parentNode->setAttribute('style', 'list-style: disclosure-open');
                                                        }
                                                }
                                        }
                                }
                        }
                        
                        $xml->formatOutput = true;
                        
                        return $xml;
                }
                
                public function renewTOC(){
                        $this->setElement('toc', null);
                        $this->getSessionToC();
                }
                
                public function getSessionToC() : string {
        
                        if(null !== $this->getElement('toc')) return base64_decode($this->getElement('toc'));
                        
                        $xml = $this->getSessionsXML(true);
                        $newXML = str_replace(['<sessions', '</sessions', '<session', '</session'], ['<ul', '</ul', '<li', '</li'], utf8_decode($xml->saveHTML($xml->documentElement)));
                        $xml->loadHTML($newXML, LIBXML_NOWARNING | LIBXML_NOERROR);
                        $xpt = new \DOMXPath($xml);
                        
                        $nl = $xpt->query('//ul');
                        
                        for($i = 0; $i < $nl->length; $i++){
                                $ulNode = $nl->item($i);
                                $firstChild = $ulNode->firstChild;
                                
                                $depth = $xpt->query('depth', $firstChild)->item(0)->nodeValue;
                                $ulNode->setAttribute('class', 'lvl'.($depth + 1));
                        }
                        
                        $nodesToRemove = [];
        
                        foreach ($xpt->query('//ul') as $node) {
                                $parentNode = $node->parentNode;
                                if($parentNode->getAttribute('id')){
                                        $node->setAttribute('child-of', 'navitem-'.$parentNode->getAttribute('id'));
                                        $class = $node->getAttribute('class');
                                        $class .= (!empty($class) ? ' ' : '').'collapse';
                                        $node->setAttribute('class', $class);
                                }
                        }
                        
                        foreach ($xpt->query('//li') as $node){
                                
                                foreach($node->childNodes as $child){
                                        if($child->nodeName != 'ul') $nodesToRemove[] = $child;
                                }
        
                                $type           =   $xpt->query('type'     , $node)->item(0)->nodeValue;
                                $day            =   $xpt->query('day'      , $node)->item(0)->nodeValue;
                                $title          =   $xpt->query('title'    , $node)->item(0)->nodeValue;
                                $ca_id          =   $xpt->query('ca_id'    , $node)->item(0)->nodeValue;
                                $cntChilds      =   $xpt->query('cntchilds', $node)->item(0)->nodeValue;
                                $id             =   $xpt->query('id'       , $node)->item(0)->nodeValue;
        
                                if(!$cntChilds) $wrapperDivNode = $node->appendChild($xml->createElement('div'));
                                else {
                                        $refNode = $node->firstChild;
                                        $wrapperDivNode = $refNode->parentNode->insertBefore($xml->createElement('div'), $refNode);
                                }
        
                                $wrapperDivNode->setAttribute('class', 'li-wrapper');
                                
                                $tDivNode = $wrapperDivNode->appendChild($xml->createElement('div'));
                                $tDivNode->setAttribute('class', 't');
                                $trDivNode = $tDivNode->appendChild($xml->createElement('div'));
                                $trDivNode->setAttribute('class', 'tr');
                                $tdDivNode = $trDivNode->appendChild($xml->createElement('div'));
                                $tdDivNode->setAttribute('class', 'td td-icon');
                                $spanNode = $tdDivNode->appendChild($xml->createElement('span'));
                                $spanNode->setAttribute('id', 'icon');
        
                                if($cntChilds){
                                        $spanNode->setAttribute('class', 'icon-arrow-up');
                                        $spanNode->setAttribute('data-toggle', 'collapse');
                                        $spanNode->setAttribute('data-target', "ul[child-of='navitem-$id']");
                                }
        
                                $tdDivNode = $trDivNode->appendChild($xml->createElement('div'));
                                $tdDivNode->setAttribute('class', 'td td-text');
                                $spanNode = $tdDivNode->appendChild($xml->createElement('span'));
                                
                                $abstract = null;
                                if($ca_id) $abstract = AbstractManager::getAbstract($ca_id);
                                
                                if($abstract && $abstract->getElement('published')){
                
                                        $url = Url::fromRoute('publisso_gold.conferences_conference.abstract', ['cf_id' => $this->getId(), 'ca_id' => $ca_id]);
                                        $aNode = $spanNode->appendChild($xml->createElement('a', ($title)));
                                        $aNode->setAttribute('href', $url->toString());
                                }
                                else{
                                        if($type == 'day') $title = $day;
                                        $spanNode->appendChild($xml->createTextNode(($title)));
                                }
                                
                        }
                        
                        foreach($nodesToRemove as $node){
                                $node->parentNode->removeChild($node);
                        }
                        
                        $xml->formatOutput = true;
                        
                        $toc = '
                                <section class="block block-publisso-gold block-publisso-gold-book-table-of-contents-block clearfix">
                                        <div class="panel-container" style="font-size: 16px;">
                                        <button type="button" class="offcanvas-toggle" data-target="#offcanvas-panel" style="font-size: 16px;">
                                            <span style="font-size: 20px;">Table of Content</span>
                                        </button>
                                        <nav class="navbar navbar-offcanvas navbar-offcanvas-touch" role="navigation" id="offcanvas-panel" aria-label="Inhaltsverzeichnis" style="font-size: 16px;">
                                                <button type="button" class="close offcanvas-close" aria-label="Close" style="font-size: 21px;">
                                                    <span class="icon-cancel" aria-hidden="true" style="font-size: 21px;"></span>
                                                </button>
                                                <h4 class="pub-title" style="font-size: 23px;">'.$this->getElement('title').'</h4>
                                                <div id="nav">'.$xml->saveHTML($xml->documentElement->firstChild->firstChild).'</div>
                                        </nav>
                                        </div>
                                </section>
                        ';
                        if(null == $this->getElement('toc')) $this->setElement('toc', base64_encode($toc));
                        return $toc;
                }
        
                /**
                 * @param $row
                 * @param $depth
                 * @param $results
                 * @return mixed
                 */
                private function getSessionDepth($row, $depth, $results){
                
                        if(!$row->parent) return $depth;
                        else return $this->getSessionDepth($results[$row->parent], $depth + 1, $results);
                }
        
                /**
                 * @return array
                 */
                public function dumpElements(){
                        return $this->elements;
                }
        }
