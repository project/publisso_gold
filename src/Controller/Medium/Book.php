<?php
        
        namespace Drupal\publisso_gold\Controller\Medium;
        
        use Drupal\Component\Utility\UrlHelper;
        use \Drupal\publisso_gold\Controller\Bookchapter;
        use \Drupal\publisso_gold\Controller\Template;
        use \Drupal\publisso_gold\Controller\TableOfContent;
        use \Drupal\publisso_gold\Controller\Workflow;
        use \Drupal\Core\Url;
        use \Drupal\Core\Link;
        use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
        use Zend\Feed\Uri;
        use \Drupal\Core\Render\Markup;

        /**
         * Class Book
         * @package Drupal\publisso_gold\Controller\Medium
         */
        class Book extends Medium {
                
                private $chapters;
        
                /**
                 * Book constructor.
                 * @param null $id
                 * @param false $noChilds
                 */
                public function __construct ($id = null, $noChilds = false ) {
                        
                        $this->mediumType = 'book';
                        $this->subMediumType = 'chapter';
                        
                        $this->databaseTable = self::DBTBL_PREFIX . 'Books';
                        $this->databaseTableColumnPrefix = 'bk_';
                        
                        $this->databaseTable_Editors = self::DBTBL_PREFIX . 'BookEditors';
                        $this->databaseTableColumnPrefix_Editors = 'be_';
                        
                        $this->databaseTable_EOs = self::DBTBL_PREFIX . 'BookEditorialOffice';
                        $this->databaseTableColumnPrefix_EOs = 'beo_';
                        
                        $this->databaseTable_EiCs = self::DBTBL_PREFIX . 'BookEditorsInChief';
                        $this->databaseTableColumnPrefix_EiCs = 'eic_';
                        
                        $this->databaseTable_Reviewers = self::DBTBL_PREFIX . 'BookReviewers';
                        $this->databaseTableColumnPrefix_Reviewers = 'br_';
                        
                        $this->databaseTable_SustainingMembers = self::DBTBL_PREFIX . 'BooksSustainingMembers';
                        $this->databaseTableColumnPrefix_SustainingMembers = 'bsm_';
                        
                        $this->databaseTable_Authors = self::DBTBL_PREFIX . 'BookAuthors';
                        $this->databaseTableColumnPrefix_Authors = 'ba_';
                        
                        $this->routeName = 'publisso_gold.books_book';
                        $this->routeParams = [];
                        
                        $this->mediumClass = '\Drupal\publisso_gold\Controller\Medium\Book';
                        $this->subMediumClass = '\Drupal\publisso_gold\Controller\Bookchapter';
                        
                        $this->chapters = [];
                        if ( $id ) $this->load( $id, $noChilds );
                }
        
                /**
                 * @param $id
                 * @param false $noChilds
                 */
                public function load ($id, $noChilds = false ) {

                        $this->id = $id;
                        $this->routeParams[ $this->databaseTableColumnPrefix . 'id' ] = $id;
                        $this->init();
                        
                        if(is_object(@json_decode($this->elements['control']))){
                                $this->setElement('control', serialize(json_decode($this->elements['control'], 1)));
                                $this->elements['control'] = unserialize($this->elements['control']);
                        }
                        
                        if ( !$noChilds ) $this->initChapters();
                }
                
                private function initChapters () {
                        
                        $result = \Drupal::database()->select( 'rwPubgoldBookChapters', 'bc' )->fields( 'bc', [ 'cp_id' ] )->condition( 'cp_bkid', $this->id, '=' )->execute()->fetchAll();
                        
                        $this->chapters = [];
                        
                        foreach ( $result as $_ ) $this->chapters[] = $_->cp_id;
                }
        
                /**
                 * @param $id
                 * @return bool
                 */
                public function hasChapter($id){
                        return in_array($id, $this->chapters);
                }
        
                /**
                 * @return array
                 */
                public function readChapters () {
                        
                        foreach ( $this->chapters as $k => $chapter ) {
                                
                                if ( gettype( $chapter ) != 'object' ) {
                                        $this->chapters[ $k ] = new Bookchapter( $chapter );
                                }
                        }
                        
                        return $this->chapters;
                }
        
                /**
                 * @param $cp_id
                 * @return Bookchapter|false|mixed
                 */
                public function readChapter ($cp_id ) {
                        
                        if ( $cp_id ) {
                                
                                if ( preg_match( '/^\d+$/', $cp_id ) ) {
                                        
                                        foreach ( $this->chapters as $chapter ) {
                                                
                                                if ( gettype( $chapter ) == 'object' && $chapter->getElement( 'id' ) ==
                                                                                        $cp_id ) {
                                                        return $chapter;
                                                }
                                                elseif ( gettype( $chapter ) != 'object' && $chapter == $cp_id ) {
                                                        return new Bookchapter( $cp_id );
                                                }
                                                else {
                                                        continue;
                                                }
                                        }
                                }
                                else {
                                        return new Bookchapter( $cp_id );
                                }
                        }
                        
                        return false;
                }
        
                /**
                 * @param null $currChapter
                 * @return Bookchapter
                 */
                public function getNextChapter ($currChapter = null ) {
                        return new Bookchapter( $this->getNextChapterID( $currChapter ) );
                }
        
                /**
                 * @param null $currChapterID
                 * @return mixed|string|null
                 */
                public function getNextChapterID ($currChapterID = null ) {
                        
                        $structure = $this->flat( $this->tableofcontent->getStructure() );
                        
                        if ( $currChapterID == null ) {
                                $link = $structure[ 0 ]->getElement( 'link' );
                                $link = str_replace( 'intern://', '', $link );
                                list( $sub_medium, $sub_medium_id ) = explode( '/', $link );
                                return $sub_medium_id;
                        }
                        else {
                                $next = false;
                                foreach ( $structure as $_ ) {
                                        
                                        $item = $_;
                                        $link = $item->getElement( 'link' );
                                        
                                        if ( substr( $link, 0, 9 ) == 'intern://' ) {
                                                
                                                $link = str_replace( 'intern://', '', $link );
                                                list( $sub_medium, $sub_medium_id ) = explode( '/', $link );
                                                if ( $next ) {
                                                        return $sub_medium_id;
                                                        break;
                                                }
                                                
                                                if ( $sub_medium_id == $currChapterID ) $next = true;
                                        }
                                }
                        }
                        
                        return $currChapterID;
                }
        
                /**
                 * @param null $currChapter
                 * @return Bookchapter
                 */
                public function getPreviousChapter ($currChapter = null ) {
                        return new Bookchapter( $this->getPreviousChapterID( $currChapter ) );
                }
        
                /**
                 * @param null $currChapterID
                 * @return mixed|string|null
                 */
                public function getPreviousChapterID ($currChapterID = null ) {
                        
                        $structure = $this->flat( $this->tableofcontent->getStructure() );
                        $structure = array_reverse( $structure );
                        
                        if ( $currChapterID == null ) {
                                $link = $structure[ 0 ]->getElement( 'link' );
                                $link = str_replace( 'intern://', '', $link );
                                list( $sub_medium, $sub_medium_id ) = explode( '/', $link );
                                return $sub_medium_id;
                        }
                        else {
                                $next = false;
                                foreach ( $structure as $_ ) {
                                        
                                        $item = $_;
                                        $link = $item->getElement( 'link' );
                                        
                                        if ( substr( $link, 0, 9 ) == 'intern://' ) {
                                                
                                                $link = str_replace( 'intern://', '', $link );
                                                list( $sub_medium, $sub_medium_id ) = explode( '/', $link );
                                                if ( $next ) {
                                                        return $sub_medium_id;
                                                        break;
                                                }
                                                
                                                if ( $sub_medium_id == $currChapterID ) $next = true;
                                        }
                                }
                        }
                        
                        return $currChapterID;
                }
        
                /**
                 * @param $bk_id
                 * @param $tab
                 * @param $cp_id
                 * @return mixed|string|null
                 */
                public function getPageTitle ($bk_id, $tab, $cp_id ) {
                        if ( !$this->id ) {
                                $this->id = $bk_id;
                                $this->load( $this->id );
                        }
                        $title = $this->getElement( 'title' );
                        
                        if ( $cp_id ) {
                                $chapter = new Bookchapter( $cp_id, true );
                                $title = (string)$this->t( '@book: @chapter', [ '@chapter' => $chapter->getElement( 'title' ), '@book' => $this->getElement( 'title' ) ] );
                        }
                        
                        return $title;
                }
        
                /**
                 * @param $bk_id
                 * @return array
                 */
                public function edit ($bk_id ) {
                        
                        if ( !$this->id ) {
                                $this->id = $bk_id;
                                $this->load( $this->id );
                        }
                        
                        if ( !( $this->isUserEditorialOffice() || $this->isUserEditorInChief() || $this->isUserEditor() || \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) ) ) {
                                
                                return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        }
                        
                        return \Drupal::formBuilder()->getForm( "Drupal\\publisso_gold\\Form\\publisso_goldEditBook", [ 'bk_id' => $bk_id ] );
                }
        
                /**
                 * @return bool
                 */
                public function hasTemplate () {
                        
                        return !!\Drupal::database()->select( 'rwPubgoldBookchapterTemplates', 't' )->condition( 'bk_id', $this->id, '=' )->countQuery()->execute()->fetchField();
                }
        
                /**
                 * @param int $id
                 * @return mixed
                 */
                public function getTemplate (int $id ) {
                        
                        return \Drupal::database()->select( 'rwPubgoldBookchapterTemplates', 't' )->condition( 'bk_id', $this->id, '=' )->condition( 'id', $id, '=' )->execute()->fetchField();
                }
        
                /**
                 * @return mixed
                 */
                public function getTemplates () {
                
                        return \Drupal::database()->select( 'rwPubgoldBookchapterTemplates', 't' )->fields('t', ['id', 'title'])->condition( 'bk_id', $this->id, '=' )->execute()->fetchAllAssoc('id');
                }
        
                /**
                 * @param $bk_id
                 * @param $tab
                 * @param null $cp_id
                 * @return array
                 */
                public function getHTML ($bk_id, $tab, $cp_id = null ) {

                        if(!\Drupal::database()->select($this->databaseTable, 't')->fields('t', [])->condition
                        ($this->databaseTableColumnPrefix.'id', $bk_id, '=')->countQuery()->execute()->fetchField())
                                throw new NotFoundHttpException();

                        $head = '';
                        if ( !$this->id ) {
                                $this->id = $bk_id;
                                $this->load( $this->id );
                        }
                        
                        \Drupal::service( 'session' )->set( 'breadcrumb_info', [ 'bk_id' => $this->id ] );
                        
                        if ( $cp_id ) {

                                if(!$this->hasChapter($cp_id))
                                        throw new NotFoundHttpException();

                                \Drupal::service( 'session' )->set( 'breadcrumb_info', array_merge( [ 'cp_id' => $cp_id ], \Drupal::service( 'session' )->get( 'breadcrumb_info' ) ) );
                                $chapter = new Bookchapter( $cp_id );
                                $head = \Drupal::service( 'publisso_gold.tools' )->getChapterDCMeta( $chapter, $this );
                        }
                        
                        $ret = [ '#type' => 'container', 'content' => [ 'header' => [ '#type' => 'inline_template', '#template' => $this->getHTMLHeader() ], 'content' => [ '#type' => 'inline_template', '#template' => $this->getHTMLContent( $cp_id ? $chapter : null, $tab ) ], 'toc' => [ '#type' => 'inline_template', '#template' => $this->getTOC() ] ], '#cache' => [ 'max-age' => 0 ], ];
                        
                        if ( !empty( $head ) ) {
                                $ret[ '#attached' ] = [ 'html_head' => $head ];
                        }
                        
                        return $ret;
                }
        
                /**
                 * @return string|string[]|null
                 */
                private function getHTMLHeader () {
                        
                        $session = \Drupal::service( 'session' );
                        
                        switch ( $this->getElement( 'theme' ) ) {
                                
                                case 'gms':
                                        $templateTheme = 'GMS';
                                        $linkClasses = [ 'btnPublissoDefault' ];
                                        break;
                                
                                default:
                                        $templateTheme = 'publisso_gold';
                                        $linkClasses = [ 'link--calltoaction', 'inline', 'btn' ];
                        }
                        
                        $template = new Template();
                        $template->get( $templateTheme . '_book_header' );
                        #$this->tmpl = file_get_contents( $this->modpath . '/inc/' . $templateTheme . '_book_header
                        #.tmpl.inc.php' );
                        
                        $urlPublish = Url::fromRoute( 'publisso_gold.book.addchapter', [ 'bk_id' => $this->id ], [ 'attributes' => [ 'class' => $linkClasses ] ]
                        );
                        
                        if ( \Drupal::service( 'session' )->get( 'logged_in' ) === true ) {
                                
                                $uid = \Drupal::service( 'session' )->get( 'user' )[ 'id' ];
                                $weight = \Drupal::service( 'session' )->get( 'user' )[ 'weight' ];
                                
                                if ( $this->hasAuthor( $uid ) || $this->countAuthors() == 0 || $weight >= 70 ) {
                                        
                                        $template->setVar( 'lnk_publish_chapter', Link::fromTextAndUrl( Markup::create( '<span>' . (string)$this->t( 'Submit' ) . '</span>' ), $urlPublish
                                        )->toString()
                                        );
                                }
                        }
                        
                        if ( \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) ) {
                                
                                $urlEdit = Url::fromRoute( 'publisso_gold.book.edit', [ 'bk_id' => $this->id ], [ 'attributes' => [ 'class' => $linkClasses ] ] );
                                
                                $urlEditEB = Url::fromRoute( 'publisso_gold.bookmanagement_seteditorialboard', [ 'bk_id' => $this->id ], [ 'attributes' => [ 'class' => $linkClasses ] ] );
                                
                                $template->setVar( 'lnk_edit_book', Link::fromTextAndUrl( Markup::create( '<span>' . (string)$this->t( 'Edit book' ) . '</span>' ), $urlEdit
                                )->toString()
                                );
                                
                                $template->setVar( 'lnk_edit_book_eb', Link::fromTextAndUrl( Markup::create( '<span>' . (string)$this->t( 'Edit edit. board' ) . '</span>' ), $urlEditEB
                                )->toString()
                                );
                        }
                        
                        $template->setVar( 'bk_id', $this->id );
                        $template->setVar( 'book_title', $this->getElement( 'title' ) );
                        $template->setVar( 'lnk_cover', !empty( $this->getElement( 'cover_link' ) ) ? $this->getElement( 'cover_link' ) : '#' );
                        $template->setVar( 'book_editors', $this->getElement( 'public_editors' ) );
                        $template->setVar( 'book_authors', '' );
                        
                        if ( !empty( $this->getElement( 'isbn' ) ) ) $template->setVar( 'li_item_book_isbn', '<li>' . $this->getElement( 'isbn' ) . '</li>' );
                        
                        if ( !empty( $this->getElement( 'issn' ) ) ) $template->setVar( 'li_item_book_issn', '<li>' . $this->getElement( 'issn' ) . '</li>' );
                        
                        if ( !empty( $this->getElement( 'funding_name' ) ) ) $template->setVar( 'li_item_book_funding_name', '<li>' . ( (string)$this->t( 'Funding name' ) ) . ': ' . $this->getElement( 'funding_name' ) . '</li>' );
                        
                        if ( !empty( $this->getElement( 'funding_number' ) ) ) $template->setVar( 'li_item_book_funding_number', '<li>' . ( (string)$this->t( 'Funding number' ) ) . ': ' . $this->getElement( 'funding_number' ) . '</li>' );
                        
                        foreach ( $this->readAuthors() as $author ) {
                                $template->appendToVar( 'book_authors', $author->profile->getReadableName( 'FL', ' ' ) );
                        }
                        
                        $_ = [];
                        foreach ( $this->readEditorialBoard() as $uid => $user ) {
                                $_[] = $user->profile->getReadableName( 'FL', ' ' );
                        }
                        
                        $template->setVar( 'li_item_book_editorial_board', '<li>' . implode( '</li><li>', $_ ) . '</li>' );
                        $template->setVar( 'li_item_funding_text', '<li>' . $this->getElement( 'funding_text' ) . '</li>' );
                        
                        return $template->parse();
                }
        
                /**
                 * @param Bookchapter|null $chapter
                 * @param string $tab
                 * @return string|string[]|null
                 */
                private function getHTMLContent (\Drupal\publisso_gold\Controller\Bookchapter $chapter = null, $tab = 'about' ) {
                        
                        $tempstore = \Drupal::service( 'user.private_tempstore' )->get( 'publisso_gold' );
                        $theme = \Drupal::service( 'theme.manager' )->getActiveTheme()->getName();
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        
                        $route[ 'route_name' ] = \Drupal::routeMatch()->getRouteName();
                        $route[ 'route_param' ] = \Drupal::routeMatch()->getRawParameters()->all();
                        
                        $active_tab = \Drupal::service('request_stack')->getCurrentRequest()->query->get('tab') ?? 'about';
                        
                        $templateName = strtoupper( $this->getElement( 'theme' ) ) . "_book_content";
                        $template = new Template();
                        $template->get( $templateName );
                        if ( $template->isLoaded() == false ) $template->get( 'publisso_gold_book_content' );
                        
                        $template->setVar( 'txt_about', (string)$this->t( 'About the Book' ) );
                        $template->setVar( 'txt_media', (string)$this->t( $this->getElement( 'theme' ) == 'gms' ? 'Surgical Techniques' : 'Media' ) );
                        $template->setVar( 'txt_editorial_board', (string)$this->t( 'Editorial Board' ) );
                        $template->setVar( 'txt_authors', (string)$this->t( 'Authors' ) );
                        $template->setVar( 'txt_manuscript_guidelines', (string)$this->t( 'Manuscript Guidelines' ) );
                        $template->setVar( 'txt_imprint', (string)$this->t( 'Imprint' ) );
                        $template->setVar( 'txt_overview_chapters', (string)$this->t( 'Overview Chapters' ) );
                        $template->setVar( 'book_about_sidebar_blocks', '' );
                        $template->setVar( 'book_chapter_content_sidebar_blocks', '' );
                        
                        foreach ( [ 'about', 'media', 'manuscript_guidelines', 'imprint' ] as $_ ) $template->setVar( "book_$_", base64_decode( $this->getElement( $_ ) ) );
                        
                        $template->setVar( 'book_funding_text', $this->getElement( 'funding_text' ) );
                        
                        $toc = new TableOfContent( false, [ 'medium' => 'book', 'id' => $this->id ], true );
                        $template->setVar( 'book_chapter_list', $this->parseChapterlistStructure( $toc->readStructure(), 1 ) );
                        
                        
                        $renderer = \Drupal::service( 'renderer' );
                        
                        $_tmpl = new Template();
                        $_tmpl->get( 'publisso_gold_book_content_sidebar_block' );
                        
                        if ( strtolower( $this->getElement( 'theme' ) ) == 'gms' ) {
                                
                                $_tmpl->setVar( 'sbb_title', (string)$this->t( 'Search' ) );
                                $_tmpl->setVar( 'sbb_content', '' );
                                $_tmpl->setVar( 'view_name', 'partner-list' );
                                $_tmpl->appendToVar( 'sbb_title', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAAbCAYAAACKlipAAAADlElEQVRogeWZMW6zQBCFfZEUKXyN3MRXyAnc+hiOI1G7p0C+ABIN5RZuoKJCQrTzVw89P+8CxjHwJyOt5GC8zM6382bYbGxlVjft0i4sapulHTAzu5ZVB4I//0VbHEjdtLZ9f7Pt+5vtDmfLXfGnoawKyPb9zZLU2bWs7FpWS7u2iK0WSO6KP5klqwMSxZkXSN20lqTOojizKM46cL8N2qqBQLZyV9jH57d37A7n7je/wVYLBLv/WlZBGDr2x8t/D+alQMbISR+Qa1nZ/njpMmEsmCjOXiZlr5bIzf54sd3hfDf2x4slqZs8MeaN4qz3vj4guStuQGA+QPJJF3/3jP8+S1LXxedVYDa6YN2J++Nl0sQIzKNATl+nIBAE2Cdj+K5u2pvrPylhSeq6eV8OZHc4d7qtgZiyqFcAyV1xN78GqG7a4G+etdmBhB7OO9P3fqBdkdktEG5Zk9TdtbNjgKhMcJaoNDEs3UzXsuqkDbIcCu61rCxJXTd43kWBIHOGAsBzDBVj3rkfn9+DQHzSiWdocNh3fk4UZ8HWmcHVTeutU7yeWYBgl/OCEIhngPC8DAiLGgPEV6BzV3ivc/Yg0AwDWaFdHPxR3/XeWYBoUUfXwto8BYjWEN/uZSC4rkB4B/tqmkqp/g5/qz9cc4aUYEwNQQ1Gt8jxg1zyyTY3I1Gc3WZI3bTd8D3oJ4BwADAPA8HIXXEDRIOtht2sz9cgo6XnweviUwEFPwQEPptZd8SDwON67opOdVCX2OdgDVGbGwikCwGEQY44+L5rCB6AcC1iSeSXUOzuUIc2BIR3P87aEAvOCi4DiA8an7sMGQNEH+CDOkay4PDp69QNPfnVOXBNd5beB0gqWb51siooPL6X65AvVnw/B1r94szhz3XTjgfCi+LU1w5Eg4RAoe3laz5DQJAl2gHxvOoDPx/zsFSwrypZnPXsJ56h3WJIsrR2cNAxN75DU4L6YmaPAeFswG+4P1cgIWhjjjS4yUDgojjzniZooPkIRaUt1M6qPGmLjHkVCJ80AwgbWmgE3ecPx33TV8j7wGjfrnPo50f/CwjJAQDtotChhAIcKszqz5BM+9bFf/OLJdp6PXWGrz6pVP8WP37vM0jWmMwNgZnrOJ7hojuccmyzaiBoQ8eaSupcQLROhK6NsVUDeVRKYaz1cwBBnYCsQoqm+L5qIM8YsuUnT3v7DO8R/P4xxX4tELPpGbak/QPYnkh3mayzNQAAAABJRU5ErkJggg=="><br>' );
                                
                                $form = \Drupal::formBuilder()->getForm( '\\Drupal\\publisso_gold\\Form\\PubmedSearch', [ 'bk_id' => $this->getElement( 'id' ) ] );
                                $_tmpl->setVar( 'sbb_content', $renderer->render( $form ) . '' );
                                
                                $template->appendToVar( 'book_about_sidebar_blocks', $_tmpl->parse('::', 0) );
                        }
                        
                        if ( count( $this->readSustainingMembers() ) ) {
                                
                                $_tmpl->setVar( 'sbb_title', (string)$this->t( $texts->get( "book.content.sidebar.$theme.sustaining_members.title", 'afc' ) ) );
                                $_tmpl->setVar( 'sbb_content', '' );
                                $_tmpl->setVar( 'view_name', 'partner-list' );
                                
                                foreach ( $this->readSustainingMembers() as $bsm_id ) {
                                        
                                        $urlPicture = Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'bsm', 'id' => $bsm_id ] );
                                        $_tmpl->appendToVar( 'sbb_content', '<img src="' . $urlPicture->toString() . '"><br>' );
                                }
                                
                                $template->appendToVar( 'book_about_sidebar_blocks', $_tmpl->parse('::', 0) );
                        }
                        
                        
                        /* create template for authors
 *
 * use template book_content_author
 */
                        $tmplAuthors = new Template();
                        $tmplAuthors->get( 'publisso_gold_book_content_author' );
                        
                        $content_authors = '';
                        $c = 0;
                        
                        if ( count( $this->readAuthors() ) == 0 ) {
                                $content_authors = '<p>' . ( (string)$this->t( 'Here the authors of the chapters will be listed.' ) ) . '</p>';
                        }
                        else {
                                $authors = [];
                                
                                foreach ( $this->readAuthors() as $user ) {
                                        if ( !$user->profile->getElement( 'show_data_in_boards' ) ) continue;
                                        $authors[ $user->profile->getReadableName() ] = $user;
                                }
                                
                                ksort( $authors );
                                
                                foreach ( $authors as $user ) {
                                        
                                        $lnkOrcid = null;
                                        
                                        if ( !empty( $user->profile->getElement( 'orcid' ) ) ) {
                                                
                                                $uri = new Uri( $texts->get( 'book.content.authors_board.lnk.orcid' ) . $user->profile->getElement( 'orcid' ) );
                                                
                                                if ( $uri->isValid() ) {
                                                        $urlOrcid = Url::fromUri( $texts->get( 'book.content.authors_board.lnk.orcid' ) . $user->profile->getElement( 'orcid' ), [ 'attributes' => [ 'target' => '_blank' ] ] );
                                                        $lnkOrcid = Link::fromTextAndUrl( $user->profile->getElement( 'orcid' ), $urlOrcid );
                                                        $lnkOrcid = $lnkOrcid->toString();
                                                }
                                                else {
                                                        $lnkOrcid = $user->profile->getElement( 'orcid' );
                                                }
                                        }
                                        
                                        $tmplAuthors->clear();
                                        
                                        
                                        $tmplAuthors->setVar( 'txt_more', (string)$this->t( 'more' ) );
                                        $tmplAuthors->setVar( 'txt_address', (string)$this->t( 'Address' ) );
                                        $tmplAuthors->setVar( 'txt_contact', (string)$this->t( 'Contact' ) );
                                        
                                        $tmplAuthors->setVar( 'modal_id', 'm_authors_' . ( $c + 1 ) );
                                        
                                        $tmplAuthors->setVar( 'author_name', $user->profile->getElement( 'graduation' ) . ' ' . $user->profile->getElement( 'firstname' ) . ' ' . $user->profile->getElement( 'lastname' ) . ' ' . $user->profile->getElement( 'graduation_suffix' ) );
                                        
                                        $tmplAuthors->setVar( 'author_institute', $user->profile->getElement( 'institute' ) );
                                        $tmplAuthors->setVar( 'author_department', !empty( $user->profile->getElement( 'department' ) ) ? '<br>' . $user->profile->getElement( 'department' ) : '' );
                                        
                                        $tmplAuthors->setVar( 'author_street', $user->profile->getElement( 'street' ) );
                                        $tmplAuthors->setVar( 'author_postal_code', $user->profile->getElement( 'postal_code' ) );
                                        $tmplAuthors->setVar( 'author_city', $user->profile->getElement( 'city' ) );
                                        $tmplAuthors->setVar( 'author_country', \Drupal::service( 'publisso_gold.tools' )->getCountry( $user->profile->getElement( 'country' ) ) );
                                        $tmplAuthors->setVar( 'author_telephone', $user->profile->getElement( 'telephone' ) );
                                        $tmplAuthors->setVar( 'author_email', $user->profile->getElement( 'email' ) );
                                        $tmplAuthors->setVar( 'author_pic_link', '/system/getPicture/up/' . $user->getId() );
                                        
                                        $tmplAuthors->setVar( 'author_address', implode( ', ', [
                                                                                                     implode( ' ', [ $tmplAuthors->getVar( 'author_postal_code' ), $tmplAuthors->getVar( 'author_city' ) ]
                                                                                                     ), $tmplAuthors->getVar( 'author_country' ),
                                                                                             ]
                                                                              )
                                        );
                                        
                                        $tmplAuthors->setVar( 'author_orcid', $lnkOrcid ? '<hr>' . $texts->get( 'book.content.authors_board.orcid', 'ac' ) . ' ' . $lnkOrcid : '' );
                                        
                                        if ( $c % 2 == 0 ) {
                                                $tmplAuthors->prependHTML( '<div class="row flex-row-sm">' );
                                        }
                                        
                                        if ( $c % 2 != 0 ) {
                                                $tmplAuthors->appendHTML( '</div><hr>' );
                                        }
                                        
                                        $content_authors .= $tmplAuthors->parse('::', 0);
                                        $c++;
                                }
                        }
                        
                        if ( $c > 0 && $c % 2 != 0 ) {
                                $content_authors .= '</div>';
                        }
                        
                        $template->setVar( 'content_authors', $content_authors );
                        /* =========================== */
                        
                        /* create template for editorial board
 *
 * use template book_content_author
 * (like authors, though same var-names are used)
 */
                        $tmplEB = new Template();
                        $tmplEB->get( 'publisso_gold_book_content_author' );
                        
                        $content_eb = '';
                        $modal_id = 0;
                        $c = 0;
                        
                        $eb = $this->readEditorialBoard();
                        
                        $content_eb .= '<tt>' . $this->getEbAbAnnotation( 'main' ) . '</tt>';
                        
                        foreach ( $eb as $uid => $user ) {
                                
                                $lnkAvatar = Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'up', 'id' => $user->getId() ] );
                                $modal_id++;
                                $tmplEB->clear();
                                
                                $tmplEB->setVar( 'txt_more', (string)$this->t( 'more' ) );
                                $tmplEB->setVar( 'txt_address', (string)$this->t( 'Address' ) );
                                $tmplEB->setVar( 'txt_contact', (string)$this->t( 'Contact' ) );
                                
                                $tmplEB->setVar( 'modal_id', 'm_editors_' . ( $modal_id ) );
                                
                                $tmplEB->setVar( 'author_name', implode( ' ', [ $user->profile->getElement( 'graduation' ), $user->profile->getElement( 'firstname' ), $user->profile->getElement( 'lastname' ), $user->profile->getElement( 'graduation_suffix' ) ] ) );
                                
                                $tmplEB->setVar( 'author_institute', $user->profile->getElement( 'institute' ) );
                                $tmplEB->setVar( 'author_department', $user->profile->getElement( 'department' ) );
                                $tmplEB->setVar( 'author_street', $user->profile->getElement( 'street' ) );
                                $tmplEB->setVar( 'author_notes', !empty( $user->tempInfo[ 'eb_notes' ] ) ? $user->tempInfo[ 'eb_notes' ] . '<br><br>' : '' );
                                
                                $tmplEB->setVar( 'author_postal_code', $user->profile->getElement( 'postal_code' ) );
                                $tmplEB->setVar( 'author_city', $user->profile->getElement( 'city' ) );
                                $tmplEB->setVar( 'author_country', \Drupal::service( 'publisso_gold.tools' )->getCountry( $user->profile->getElement( 'country' ) ) );
                                $tmplEB->setVar( 'author_telephone', $user->profile->getElement( 'telephone' ) );
                                $tmplEB->setVar( 'author_email', $user->profile->getElement( 'email' ) );
                                $tmplEB->setVar( 'author_pic_link', $lnkAvatar->toString() );
                                $tmplEB->setVar( 'author_address', implode( ', ', [ implode( ' ', [ $tmplEB->getVar( 'author_postal_code' ), $tmplEB->getVar( 'author_city' ) ] ), $tmplEB->getVar( 'author_country' ) ] ) );
                                
                                if ( $c % 2 == 0 ) {
                                        $tmplEB->prependHTML( '<div class="row flex-row-sm">' );
                                }
                                
                                if ( $c % 2 != 0 ) {
                                        $tmplEB->appendHTML( '</div><hr>' );
                                }
                                
                                $content_eb .= $tmplEB->parse('::', 0);
                                $c++;
                        }
                        
                        if ( $c > 0 && $c % 2 != 0 ) {
                                $content_eb .= '</div>';
                        }
                        
                        //advisory boards
                        $advisoryBoards = $this->readAdvisoryBoard();
                        
                        if(is_array($advisoryBoards)) {
                                foreach ( $advisoryBoards as $boardName => $users ) {
                
                                        $content_eb .= '<h4><center><i>' . $boardName . '</i></center></h4>';
                                        $content_eb .= '<tt>' . $this->getEbAbAnnotation( $boardName ) . '</tt>';
                                        $c = 0;
                
                                        foreach ( $users as $uid => $user ) {
                        
                                                $lnkAvatar = Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'up', 'id' => $user->getId() ] );
                                                $modal_id++;
                                                $tmplEB->clear();
                        
                                                $tmplEB->setVar( 'txt_more', (string)$this->t( 'more' ) );
                                                $tmplEB->setVar( 'txt_address', (string)$this->t( 'Address' ) );
                                                $tmplEB->setVar( 'txt_contact', (string)$this->t( 'Contact' ) );
                        
                                                $tmplEB->setVar( 'modal_id', 'm_editors_' . ( $modal_id ) );
                        
                                                $tmplEB->setVar( 'author_name', implode( ' ', [ $user->profile->getElement( 'graduation' ), $user->profile->getElement( 'firstname' ), $user->profile->getElement( 'lastname' ), $user->profile->getElement( 'graduation_suffix' ) ] ) );
                        
                                                $tmplEB->setVar( 'author_institute', $user->profile->getElement( 'institute' ) );
                                                $tmplEB->setVar( 'author_department', $user->profile->getElement( 'department' ) );
                                                $tmplEB->setVar( 'author_street', $user->profile->getElement( 'street' ) );
                                                $tmplEB->setVar( 'author_notes', $user->tempInfo[ 'eb_notes' ] . '<br><br>' );
                                                $tmplEB->setVar( 'author_postal_code', $user->profile->getElement( 'postal_code' ) );
                                                $tmplEB->setVar( 'author_city', $user->profile->getElement( 'city' ) );
                                                $tmplEB->setVar( 'author_country', \Drupal::service( 'publisso_gold.tools' )->getCountry( $user->profile->getElement( 'country' ) ) );
                                                $tmplEB->setVar( 'author_telephone', $user->profile->getElement( 'telephone' ) );
                                                $tmplEB->setVar( 'author_email', $user->profile->getElement( 'email' ) );
                                                $tmplEB->setVar( 'author_pic_link', $lnkAvatar->toString() );
                                                $tmplEB->setVar( 'author_address', implode( ', ', [ implode( ' ', [ $tmplEB->getVar( 'author_postal_code' ), $tmplEB->getVar( 'author_city' ) ] ), $tmplEB->getVar( 'author_country' ) ] ) );
                        
                                                if ( $c % 2 == 0 ) {
                                                        $tmplEB->prependHTML( '<div class="row flex-row-sm">' );
                                                }
                        
                                                if ( $c % 2 != 0 ) {
                                                        $tmplEB->appendHTML( '</div><hr>' );
                                                }
                        
                                                $content_eb .= $tmplEB->parse('::', 0);
                                                $c++;
                                        }
                
                                        if ( $c > 0 && $c % 2 != 0 ) {
                                                $content_eb .= '</div>';
                                        }
                                }
                        }
                        
                        $template->setVar( 'content_eb', $content_eb );
                        /* =========================== */
                        
                        if ( $chapter ) {
                                
                                //$chapter = new Bookchapter($cp_id);
                                $active_tab = 'chapter_content';
                                $origWorkflow = new Workflow( $chapter->getElement( 'wfid' ) );
                                $templateName = strtoupper( $this->getElement( 'theme' ) ) . '_book_content_tab_chapter_content';
                                $_tmpl->get( $templateName );
                                if ( !$_tmpl->isLoaded() ) $_tmpl->get( 'publisso_gold_book_content_tab_chapter_content' );
                                
                                
                                $_tmpl->setVar( 'txt_chapter_content', (string)$this->t( 'Chapter content' ) );
                                #$_tmpl->setVar( 'tab_chapter_content_class_active', 'active' );
                                
                                $template->setVar( 'li_item_chapter_content', $_tmpl->parse('::', 0) );
                                
                                $templateName = strtoupper( $this->getElement( 'theme' ) ) . '_book_content_content_chapter_content';
                                $_tmpl->get( $templateName );
                                if ( !$_tmpl->isLoaded() ) $_tmpl->get( 'publisso_gold_book_content_content_chapter_content' );
        
                                if ( $this->getElement( 'theme' ) == 'gms' ) {
                
                                        $_tmpl->setVar( 'book_chapter_navigation', $this->getBookChapterNavigation(
                                                $chapter ) );
                                        $_tmpl->setVar( 'booksearch', $this->getBooksearch( $chapter ) );
                                }
                                
                                $_abstract = base64_decode( $chapter->getElement( 'abstract' ) );
                                $_erratum = $chapter->getElement( 'errata' );
                                $_fulltext = base64_decode( $chapter->getElement( 'chapter_text' ) );
                                
                                $_references = base64_decode( $chapter->getElement( 'references' ) );
                                
                                $_references = nl2br( $_references );

//set the authors
                                
                                $authors = [];
                                $affiliations = [];
                                $strAuthors = '';
                                $strAffiliations = '';
                                
                                $chapter_authors = json_decode( $chapter->getElement( 'authors' ) );
                                
                                foreach ( $chapter_authors as $_ ) {
                                        
                                        $weight = $_->weight;
                                        
                                        //falls Autoren mit gleicher Gewichtung angegeben sind, damit sie sich nicht gegenseitig überschreiben
                                        while ( array_key_exists( $weight, $authors ) ) $weight++;
                                        
                                        $authors[ $weight ] = $_;
                                }
                                
                                ksort( $authors );
                                
                                foreach ( $authors as $_ ) {
                                        
                                        if ( property_exists( $_, 'uid' ) && property_exists( $_, 'profile_archive_id' ) ) {
                                                $user = new \Drupal\publisso_gold\Controller\User( $_->uid );
                                                $user->profile->loadSnapshot( $_->profile_archive_id );
                                                $affiliations[] = $user->profile->getAffiliation();
                                        }
                                        else {
                                                $affiliations[] = $_->affiliation;
                                        }
                                }
                                
                                $affiliations = array_keys( array_flip( $affiliations ) ); //make affs unique
                                
                                for ( $c = 1; $c <= count( $affiliations ); $c++ ) {
                                        if ( $affiliations[ $c - 1 ] ) $strAffiliations .= ( !empty( $strAffiliations ) ? '<br>' : '' ) . '<sup>' . $c . '</sup>&nbsp;' . $affiliations[ $c - 1 ];
                                }
                                
                                foreach ( $authors as $_ ) {
                                        
                                        if ( isset( $email ) ) unset( $email );
                                        
                                        if ( property_exists( $_, 'uid' ) && property_exists( $_, 'profile_archive_id' ) ) {
                                                $user = new \Drupal\publisso_gold\Controller\User( $_->uid );
                                                $user->profile->loadSnapshot( $_->profile_archive_id );
                                                $affiliation = $user->profile->getAffiliation();
                                                $email = $user->profile->getElement( 'email' );
                                        }
                                        else {
                                                $affiliation = $_->affiliation;
                                        }
                                        
                                        $affKey = array_search( $affiliation, $affiliations ) + 1;
                                        
                                        $str_ = implode( ' ', [ $_->firstname, $_->lastname ] ) . ( !empty( $affKey ) ? " <sup>$affKey</sup>" : "" );
                                        
                                        $isCorresponding = property_exists( $_, 'is_corresponding' ) ? !!$_->is_corresponding : false;
                                        
                                        if ( $isCorresponding && $email ) {
                                                
                                                $data = md5( $email );
                                                
                                                if ( null === ( $encdata = $tempstore->get( 'encdata' ) ) ) {
                                                        $encdata = serialize( [] );
                                                }
                                                $encdata = unserialize( $encdata );
                                                $encdata[ $data ] = $email;
                                                
                                                $tempstore->set( 'encdata', serialize( $encdata ) );
                                                $str_ = '<span class="glyphicon glyphicon-envelope"></span>&nbsp;<a class="encmailadd" data="' . $data . '" href="?subject=' . $chapter->getElement( 'title', $lang ?? null ) . '">' . $str_ . '</a>';
                                        }
                                        
                                        $strAuthors .= ( !empty( $strAuthors ) ? '<br>' : '' ) . '<span class="chapterAuthor">' . $str_ . '</span>';
                                }
                                
                                $strAuthors = $strAuthors . '<br>' . implode( '<br>', array_filter( json_decode( $chapter->getElement( 'corporation' ), 1 ) ) ) . '<br><br>' . $strAffiliations;
                                // -- set the authors --
                                // create the content
                                $_tmpl->setVar( 'book_chapter_content', '
                                <div>
                                        <h1>
                                                ' . $chapter->getElement( 'title' ) . '
                                        </h1>
                                </div>
                                ' . $strAuthors . '
                                <div>
                                        <div id="abstract">' . ( !empty( $_abstract ) ? '<h2>' . t( 'Abstract' ) . '</h2>' . $_abstract . '<hr id="sepAbstractText">' : '<br>' ) . '</div>
                                        <div>' . $_fulltext . '</div>
                                        <div>' . ( !empty( $_references ) ? '<hr id="sepTextReferences"><h2>' . ( (string)t( 'References' ) ) . '</h2>' . $_references : '' ) . '</div>
                                        <div>' . ( !empty( $_erratum ) ? '<hr><h2>' . t( 'Erratum' ) . '</h2>' . $_erratum : '' ) . '</div>
                                </div>
                        '
                                );
                                // -- create the content --
                                
                                #$_tmpl->setVar( 'content_chapter_content_class_active', 'active' );
                                $_tmpl->setVar( 'book_citation_note', $this->getElement( 'citation_note' ) );
                                
                                // create the sibebar-boxes
                                $__tmpl = new Template();
                                $__tmpl->get( 'publisso_gold_book_content_sidebar_block' );
                                
                                //citation note
                                $__tmpl->setVar( 'sbb_title', (string)$this->t( 'Citation note' ) );
                                $__tmpl->setVar( 'section-id', 'chapter-citationnote' );
                                $__tmpl->setVar( 'view_name', 'citationnote' );
                                $__tmpl->setVar( 'sbb_content', $chapter->getCitationNote( $this ) );
                                $_tmpl->setVar( 'citation_note', $__tmpl->parse('::', 0) );
                                
                                //sustaining_members
                                if ( count( $this->readSustainingMembers() ) ) {
                                        
                                        $__tmpl->clear();
                                        $__tmpl->setVar( 'section-id', 'chapter-sustainingmembers' );
                                        $__tmpl->setVar( 'sbb_title', (string)$this->t( \Drupal::service( 'publisso_gold.texts' )->get( "book.content.sidebar.$theme.sustaining_members.title", 'fc' ) ) );
                                        $__tmpl->setVar( 'sbb_content', '' );
                                        $__tmpl->setVar( 'view_name', 'partner-list' );
                                        
                                        foreach ( $this->readSustainingMembers() as $bsm_id ) {
                                                
                                                $urlPicture = Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'bsm', 'id' => $bsm_id ] );
                                                $__tmpl->appendToVar( 'sbb_content', '<img src="' . $urlPicture->toString() . '"><br>' );
                                        }
                                        
                                        $_tmpl->setVar( 'sustaining_members', $__tmpl->parse('::', 0) );
                                }
                                
                                //if($this->getElement('theme') == 'gms'){
                                
                                $__tmpl->clear();
                                $__tmpl->setVar( 'section-id', 'chapter-partnerlist' );
                                $__tmpl->setVar( 'sbb_title', '' );
                                $__tmpl->setVar( 'sbb_content', '' );
                                $__tmpl->setVar( 'view_name', 'partner-list' );
                                
                                $urlExportPDF = Url::fromRoute( 'publisso_gold.export.chapter', [ 'cp_id' => $chapter->getElement( 'id' ) ] );
                                $urlRecreatePDF = Url::fromRoute( 'publisso_gold.export.chapter.force_new', [ 'cp_id' => $chapter->getElement( 'id' ) ] );
                                $urlPreviewPDF = Url::fromRoute( 'publisso_gold.export.chapter.volatile', [ 'cp_id' => $chapter->getElement( 'id' ) ] );
                                $urlDeletePDF = Url::fromRoute( 'publisso_gold.book.chapter.pdf', [ 'bk_id' => $this->id, 'cp_id' => $chapter->getElement( 'id' ), 'action' => 'delete' ] );
                                $urlExportRIS = Url::fromRoute('publisso_gold.book.chapter.ris', ['bk_id' => $this->id, 'cp_id' => $chapter->getElement('id')]);
                                
                                $__tmpl->setVar( 'sbb_content', '' );
                                $__tmpl->setVar( 'section-id', 'chapter-downloads' );
                                $__tmpl->setVar( 'view_name', 'downloads' );
                                if ( $chapter->getElement( 'pdf_blob_id' ) ) $__tmpl->appendToVar( 'sbb_content', '<!--suppress ALL -->
<!--suppress XmlDuplicatedId -->
<div id="lnkChapterPDFDownload" ><a href="' . $urlExportPDF->toString() . '" class="download-pdf" id="lnkChapterPDFDownload" rel="nofollow" target="_self"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;Download PDF</a></div>' );
                                $__tmpl->appendToVar( 'sbb_content', '<div id="lnkChapterRIS" ><a href="'.$urlExportRIS->toString().'" class="download-ris" rel="nofollow" target="_self"><span class="glyphicon glyphicon-bookmark"></span>&nbsp;&nbsp;Download RIS</a><br></div>' );
                                
                                if ( \Drupal::service( 'session' )->get( 'user' )[ 'weight' ] >= 70 ) {
                                        $__tmpl->appendToVar( 'sbb_content', '<a href="' . $urlRecreatePDF->toString() . '" class="download-pdf" rel="nofollow" target="_self"><span class="glyphicon glyphicon-repeat"></span>&nbsp;&nbsp;(Re)Create PDF</a><br>' );
                                        $__tmpl->appendToVar( 'sbb_content', '<a href="' . $urlPreviewPDF->toString() . '" class="download-pdf" rel="nofollow" target="_self"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;Preview PDF</a><br>' );
                                        
                                        if ( $chapter->getElement( 'pdf_blob_id' ) ) $__tmpl->appendToVar( 'sbb_content', '<div id="lnkChapterPDFDelete" ><a href="' . $urlDeletePDF->toString() . '" class="download-pdf use-ajax" data-dialog-type="modal" rel="nofollow" target="_self"><span class="glyphicon glyphicon-trash"></span>&nbsp;&nbsp;Delete PDF</a><br></div>' );
                                }
        
                                $__tmpl->appendToVar( 'sbb_content', '<div id="lnkChapterPrint" ><a href="javascript:printChapter();" class="print-chapter use-ajax" data-dialog-type="modal" rel="nofollow" target="_self"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Print</a><br></div>' );
                                /*
                                $__tmpl->setVar('sbb_content', '
                                        <a href="'.$url->toString().'" rel="nofollow" target=""><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Print</a><br>
                                        <a href="'.$url->toString().'" class="download-pdf" rel="nofollow" target="_blank"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;Download PDF</a><br>
                                        <a href="'.$url->toString().'" class="export-xml" rel="nofollow" target=""><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;Export as XML</a><br>
                                        <a href="'.$url->toString().'" class="download-ris" rel="nofollow" target=""><span class="glyphicon glyphicon-link"></span>&nbsp;&nbsp;Download to RIS</a>
                                ');
                                */
                                
                                if ( !empty( $__tmpl->getVar( 'sbb_content' ) ) ) $_tmpl->setVar( 'downloads', $__tmpl->parse('::', 0) );
                                //override temporary
                                
                                //if($chapter->getElement('id') == 10)
                                //$_tmpl->setVar('downloads', '');
                                //}
                                
                                //license
                                if ( !empty( $chapter->getElement( 'license' ) ) ) {
                                        
                                        $__tmpl->clear();
                                        $__tmpl->setVar( 'section-id', 'chapter-license' );
                                        $__tmpl->setVar( 'view_name', 'license' );
                                        $__tmpl->setVar( 'sbb_title', (string)$this->t( 'License' ) );
                                        
                                        $license = new \Drupal\publisso_gold\Controller\License( $chapter->getElement( 'license' ) );
                                        $__tmpl->setVar( 'sbb_content', $license->getImageLink( [ 'height' => 40 ], '_blank' ) );
                                        $__tmpl->appendToVar( 'sbb_content', '<br>&copy;&nbsp;' );
                                        $__tmpl->appendToVar( 'sbb_content', $chapter->getAuthorsIntNames()[ 0 ] ?? '' );
                                        $__tmpl->appendToVar( 'sbb_content', count( $chapter_authors ) > 1 ? ' et al.' : '' );
                                        $__tmpl->appendToVar( 'sbb_content', '<br>' );
                                        $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'This chapter is distributed under the terms of the license <a href="' . $license->getElement( 'url' ) . '" target="_blank">@license</a>.', [ '@license' => $license->getElement( 'description' ) ] ) );
                                        $__tmpl->setVar( 'view_name', 'license' );
                                        $_tmpl->setVar( 'chapter_license', $__tmpl->parse('::', 0) );
                                }
                                
                                //pubmed-search
                                if ( !!$this->getControlElement( 'search_pubmed_enabled' )) {
                                        
                                        $authors = [];
                                        foreach ( json_decode( $chapter->getElement( 'authors' ) ) as $_ ) $authors[ $_->weight ] = $_;
                                        ksort( $authors );
                                        reset( $authors );
                                        $firstAuthor = current( $authors );
                                        $firstAuthor = implode( ' ', array_filter( [ $firstAuthor->firstname, $firstAuthor->lastname ] ) );
                                        
                                        $__tmpl->setVar( 'sbb_title', (string)$this->t( 'Search' ) );
                                        $__tmpl->setVar( 'section-id', 'chapter-search' );
                                        $__tmpl->setVar( 'view_name', 'search' );
                                        $__tmpl->setVar( 'sbb_content', '' );
                                        $__tmpl->setVar( 'view_name', 'partner-list' );
                                        $__tmpl->appendToVar( 'sbb_title', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAAbCAYAAACKlipAAAADlElEQVRogeWZMW6zQBCFfZEUKXyN3MRXyAnc+hiOI1G7p0C+ABIN5RZuoKJCQrTzVw89P+8CxjHwJyOt5GC8zM6382bYbGxlVjft0i4sapulHTAzu5ZVB4I//0VbHEjdtLZ9f7Pt+5vtDmfLXfGnoawKyPb9zZLU2bWs7FpWS7u2iK0WSO6KP5klqwMSxZkXSN20lqTOojizKM46cL8N2qqBQLZyV9jH57d37A7n7je/wVYLBLv/WlZBGDr2x8t/D+alQMbISR+Qa1nZ/njpMmEsmCjOXiZlr5bIzf54sd3hfDf2x4slqZs8MeaN4qz3vj4guStuQGA+QPJJF3/3jP8+S1LXxedVYDa6YN2J++Nl0sQIzKNATl+nIBAE2Cdj+K5u2pvrPylhSeq6eV8OZHc4d7qtgZiyqFcAyV1xN78GqG7a4G+etdmBhB7OO9P3fqBdkdktEG5Zk9TdtbNjgKhMcJaoNDEs3UzXsuqkDbIcCu61rCxJXTd43kWBIHOGAsBzDBVj3rkfn9+DQHzSiWdocNh3fk4UZ8HWmcHVTeutU7yeWYBgl/OCEIhngPC8DAiLGgPEV6BzV3ivc/Yg0AwDWaFdHPxR3/XeWYBoUUfXwto8BYjWEN/uZSC4rkB4B/tqmkqp/g5/qz9cc4aUYEwNQQ1Gt8jxg1zyyTY3I1Gc3WZI3bTd8D3oJ4BwADAPA8HIXXEDRIOtht2sz9cgo6XnweviUwEFPwQEPptZd8SDwON67opOdVCX2OdgDVGbGwikCwGEQY44+L5rCB6AcC1iSeSXUOzuUIc2BIR3P87aEAvOCi4DiA8an7sMGQNEH+CDOkay4PDp69QNPfnVOXBNd5beB0gqWb51siooPL6X65AvVnw/B1r94szhz3XTjgfCi+LU1w5Eg4RAoe3laz5DQJAl2gHxvOoDPx/zsFSwrypZnPXsJ56h3WJIsrR2cNAxN75DU4L6YmaPAeFswG+4P1cgIWhjjjS4yUDgojjzniZooPkIRaUt1M6qPGmLjHkVCJ80AwgbWmgE3ecPx33TV8j7wGjfrnPo50f/CwjJAQDtotChhAIcKszqz5BM+9bFf/OLJdp6PXWGrz6pVP8WP37vM0jWmMwNgZnrOJ7hojuccmyzaiBoQ8eaSupcQLROhK6NsVUDeVRKYaz1cwBBnYCsQoqm+L5qIM8YsuUnT3v7DO8R/P4xxX4tELPpGbak/QPYnkh3mayzNQAAAABJRU5ErkJggg=="><br>' );
                                        
                                        $renderer1 = \Drupal::service( 'renderer' );
                                        $form = \Drupal::formBuilder()->getForm( '\\Drupal\\publisso_gold\\Form\\PubmedSearch', [ 'bk_id' => $this->getElement( 'id' ), 'term' => $firstAuthor ] );
                                        $__tmpl->setVar( 'sbb_content', $renderer1->render( $form ) . '' );
                                        $_tmpl->setVar( 'search', $__tmpl->parse('::', 0) );
                                }

                                //livivo search
                                
                                if(!!$this->getControlElement('search_livivo_enabled')) {
                                        
                                        $renderer2 = \Drupal::service( 'renderer' );
                                        $form = \Drupal::formBuilder()->getForm( '\\Drupal\\publisso_gold\\Form\\LivivoSearch', [ 'bk_id' => $this->getElement( 'id' ), 'term' => '' ] );
                                        $__tmpl->clear();
                                        $__tmpl->setVar( 'sbb_content', $renderer2->render( $form ) . '' );
                                        $__tmpl->setVar( 'sbb_title', ( (string)$this->t( 'Search' ) ) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAAAjCAYAAAAnvgICAAAUCXpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarZppciM3toX/YxVvCZguhuVgjOgd9PLfdyBWuV2WHW1Hi1UklUwigTucASl3/v2v6/6Pn9zNu2y1lV6K5yf33OPgTfNfP1+vwef3/H7S/nwWfn/cxfT5IHJI7z+/l/M5f3DcfvtCzZ/j8/fHXV2fcdpnoM8HPwZMunLkzee89hkoxa/j4fO765/vjfwfy/n8nz+WYV8vv/6eK8HYxngpunhSSJ7npqskZpB6Grzae+5RRwrvE58OjrbvY+d+vv0leD1+Hzs/Pmek34fC+fI5ofwSo8/xYN/H7kXoP2cUfryNv/8g3fBjTn+I3b273Xu+VjdyIVLFfRblP0O8d5w4CWV6Xys8Kv+N9/U9Oo/GEhcZ22Rz8lgu9BCJ9g057DDCDee9rrCYYo4nVl5jXDG9Yy3V2ON6Scl6hBsr6dkuNbKxyFricPw5l/Cu29/1VmhceQfOjIHBAt/4w8N9d/CfPH4OdK9KNwQFs8cXK+YVFXCmoczpmbNISLifmNqL73u4n2n97UeJTWTQXpgbCxx+fg0xLfxWW+nlOXGe+ez8V2uEuj8DECKubUwmJDLgS0gWSvA1xhoCcWzkZzDzmHKcZCCYxR3cJTcpFZLToq7Nd2p450aLX4eBFhJhNEolNTQQycrZqJ+aGzU0LFl2ZlasWrNuo6SSi5VSahFGjZpqrlZLrbXVXkdLLTdrpdXWWm+jx56AMOulV9db730MLjoYevDtwRljzDjTzNNmmXW22edYlM/Ky1ZZdbXV19hxp03777Kr2233PU44lNLJx0459bTTz7jU2k03X7vl1ttuv+Nn1j5Z/X3Wwi+Z++ushU/WlLH8zqu/ZY3Dtf4YIghOTDkjYzEHMl6VAQo6Kme+hZyjMqecUXQ0hUWyFkzJ2UEZI4P5hGg3/Mzdb5n7y7w5y38rb/HPMueUuv9F5pxS98ncH/P2Tdb2eIySXoLUhYopMAiwcdKIjX/g8d97TX6ufEeotxxX+g57ciGmumfyPdWy9t6trWrhTGO9xTrgWlbcY9+19j2mNjtnlSRWKfVmN2bJJxLX2fuePSvws1nIFgx2+q/n6P7Zon4uDiDrsPvtrh5by+o41a9c6rBCIWdO29sAZNJXLWfKouwW2mRtROEsktdIUCo3keIak2sr3DxmHeX2ecjsKavZuh3kyVO0u8xGp2jjvJM4UkN1wHalryYum4HLruqo3zYaRbotrHMiwypAZaW/t0z3d+JSZhct3UIfsXzRYzgUOaEaznrn4iu3ldoF6oaZ75OGaScTDMq8DErBxtzLn0rDzdlHvHGWdWPkCDzVlP7DSvl1bat95jVTvwqXP2fPnU6J6Y5tg5auPK3Td+qrVFq5jjR9I9o37ONGr5MWHav4eWZrmaxQqCWBoJVp1rDq5vJ3BRquBqp0hVNJ9Yyn7GC0Tjs2XST/p3Y1n92V+rAzKNo117Sbcu39lJLWKsY0+6mQh3qUyabcJx1LKGys4BhmNyoonfrGTMTmTj8Oy6Vs6pplgxKhrk21zFMmbNOabTDt7g62Ebd+KEhDXlQf0WYATvUHWEUTDED1+Ako9XX2ldzNMBuT3YKsOS55ievOA9z3a8NJJYSaDzIWyARIyFLPl7gQrEzAW0eT5OpD26XP3RJJIo+zX4i7Tj2Pi87Wm5JZOdDK8i6gwhGQByzvNMDa0cCvnAcRRbkersX8QCgwnMiBDCPvk90NvUQwsY01ijpsB2JLayy/KI8R6YEO+AO86e7jE9CITPJx5q2xgJFCe9IifJqsrkK7rgrSTCQHTad5NaO2KIzSjToAklHAtzaLficK6GrO6YKdrAKEtMP4lbIGncuSRKvhnvVi4EdVXFq9VEO6sAry4XSLowgB2uKiOV2m4fZeCuBW59dDCPvkOEMn41BAX5zeqUfKYhJ7BX5sSvuGNdbsJaxGPkNzSdzNly6NeaoWqpmcuWlCACVPBgqTwqQWyCtDQYRg7gU8DKQBeGudPTqCAOsAJHOGEMZamfpljmDJHbUe4DknynF51gGYz4sK7WWztt27ndymBxAZqBIphSmcW0cPmkQeSxWwzcABzlw7MTEUL5ONo59DS1Ba9SJ612Cd9Lb7sRAghY7KRCooDy8nWCwvvBEgyT/98to7HMI7ij5W1IgJUU9vWgtRaZM622G2NSbXz9JXxAVw2WNk4IjY9FmLNWp/C2TMoB/X6wKGaPp1EIAkqdJNDOAbZD1TExSj4Qd6fgmdar+LpS1U/dp4zpDaXu2AkNakDWKfwAYylKo/l9kQld4oaxR810XgeewH1bSnIfVrgoN2AJKyvxGCcbjZxLyTLXLfAoB5Buwfdzwo2wn69kK0YZlWoaQpdKQCYftFsoMut+ac0am4EiVtXLZxOB4qdGI84OH2inOWGGiLuDWX0AGQiXUZh4j1ncvYw2a9iNEdTglHvqaVQe1kqIxYvr6LrCTTPtH8CbnQ+h1tEmza8UB12JevVLJlIORWVzAbhgvHxqDsKLQKyYCpuZ6Yr4dFAE9gmkwc64FaAv/iAH8XQE283ASuLnWNvCg70a8RdmzXT7B+U5yhXGOMTnPYmWsY7AAvxJ0XYg0yOuia8bofXQVk9UC5P4+GIqj7cLGzI+b8kMZKxaTL4haowhDt7GPEUZR+ygiIT6fZNYFQA9zB8irZAx6DdxAWDrDODVCUXe4Ei2EVgABE4Rz4BAG00xoRKwo1DH9yR1IhSgGVwXdav742gJJOYLqwLbjMGsSctBjtXg9FC2qejSRBqWJq4hhDWLa/uksysdPDD9RbuRvoDOVwjYMgRtQBq18KhqCAASPD93WR/no0D/gLhHhQA9zJv3JpKo0i1mf9AouI+ImuAv8b3KnEbWThTCjWwkAg1ixpVJ8N2GXNBgLy6SOau6r/Qu8kUC0gA0F4R46weCjQkExK7i2a+VPTq3n6nsjDd9iOIHIpp2FGuhhBFuOlDgneFuNTtQsSuvDxXQ6gAk6T9k+0FXJwktRchNXhmUgS6aChoO9qBYYC84ByOgVhD3YWBBGUuRBaCDa+7MMTat+8om4R00EluwF4EMpotBSpkkCVkSXqf5pD2GdNAYzMB7hHe1SJrFUyzOOvRBkdILtZJsIgU1BIBGQNXeZZWh5ileBgddYLxOMZhHoAJ6aGmsoWIYLw9p8WMAOUz419B182WIvxkgdNQBUMk6+L1OLyFaRvqFaDTCbtCJTGPRmYpNIOT8qc2Bgk8ozJ2fgc/FsEHqjIXsDsQXs1ATg6DcPQIRvmR+8yRkKZlIX6WQimfPOi4LERgOtKMnD7KbuSw1zobF5F0UynWkLZ3Q4gJFUrEaACw66duAuciNYDTUS0UUTIKvpHhTGPo7qQNzNLGZJ1oCah6v2U/EpYMQpxyEcTMMzL2FX+FNMn2whFEgrYHYHuzj10MVOdSSoP8qYpaGJVMiql9FHKoFuBu0UFgNsJkXYpRYWUuQJIcMdxWXL5YKkiIeoLq/ooE7o4qkP85S1R3cEJgCltD8dUooXG3RWMHRRXturgyxWgtp3J48qyxYhyhCLDo5grNo2CtEqsPBgDF/uA4EQvGZEwoAbVRBbdrheYIKF0SjKPni+A6ZblJyEBB0iMkNd864KWLHKAhSpLgOuugWQA1ry5gnKjell6h1Qlzeehv1pAx4dBXAigyADWPpsioLbQoquXLCFdITIA2eJ1cuogtpQscmYjjy8roWU6lh78R8WAuwgtFDwNqmJC/gUqA124wP4EeVjdDv01tpSzNBaNiIHpAN0Tg51lmYiSMVKNG3jpIKrOy9pLQCuFEdIqARHxTMoYUPmhxFg73GLdUPs0AdyJIusIsyJ3hzyB75BRq2fmMTFKrB0cXru4g4Ygr7ZZhxTerPQlKg+zQkmjefgV0JmUOjUP2oP86FfmQM9smSEmjbJ0lEQHsFJXfrgCdeKTSQVo+wuSprs6Y9CQQ/oltQTwX226wGwsDZua6J2n2JZKZzQa16N3sLSyi+pjuLgAlkaYuewUeg6cL8wdkYdytiwXrIjVO/AVmwCAX9/QmLG0DOppgwf6IOsMRNTpvqvaBj50RYr6JhJLygDRIdfjkCTgljpCUwxIDD+sEjncskWARRstaaP8EKUUOkK3wpM9eskvqQXoffvgQPNEma7IFTH1HevLGg+CmKhN2TWfgGjgSKRoAV3H07wZfo2eF5DU0xFuazsDwvoT7Ed8wnuMPJGNkkjwZypqG5AIhWlr0PyIQmfAAbgifbhVBdoRR3NsztiwA46cDEK9XltZ0o1kHFPX01hxHPoXcVmxNg5rRKkY2rovYAKZAY6I94MV/eIbi7we7AMiCBe2De6vuFWZiS1z8uyew7kixJocEcRCPio0MltFk4tnEao0FaAJZZ2Ne1h5P8pmsQEBcC+dD04M9NHCc9IvMISqnhLEJd2U+pcfo48GsSo3zyHRow6HLpB/kBwoTeWgB/EimhhUNLLUndQM1jVilWTYYUC07CDZSN8F7YPfZUidQtXaLUtwhpwtEtk19A5RQeqgPkeVOcfEdm3WDL8wzpLU8phIpCjlgg5HGDekN6MTiyTTfpIz2HPd8jYK5oqqNzQBHonhkeSwDYm7inAVWyG0ttS4dPvdcikeSxZ7c+pUaC1X+UYEAAcH0mpc6iuBOHSz2gMpeenoGIBsFPgO+7lU9PTBItDVwuxmjzlsYKu9vSA3Ghy8vDDAvAUi0F0FKh0xtWWQ65NQB4BJKwc8XyXYrCqsmJRqbN6FYbk0BhWpRGxAfGUVpSv1N1+fvfQjLCAJUO508HM7UJkMTMJwuDQ25C6Tmn/azAcvVwEhMQHmRTyTOPbiTG8+r1+xvANh7yhTZLL25VQnN7a0haZc6UYCfWjcXrXbw28w8zyVmVObNJO0NlnuchnbIRnAmK0PQWoMfQROSKD2c/UtyBJCRjNRJwCZkKCT/VK1O4jawbh11M9xUZtMqdCsGyPLOivFBIDuQfTRUlTWoa/xiep7FE1FJS2xdGsgz0KJVnkiJ8FEV2HZsJWZ+DC87ngM7TNj9ECGERF6QdljoVwUS6ybRwaCM+mndzHHAE5mldv6hnY4a2mHDAOHh4d0qCqKMkrT36x1462QEX2r2XH31pClnavgaSHpRiqDfB5lOwlDmEkg2yNXR5PIoHe/4p/5dr061ZU0RaX47vMj5TUiC8FtMrl0fT9jUvs41EsMqCUaOIJJcMCYzJgkUZDgN7yPggKnxW6yHfL5E6+5WlFpb21Q0RNwO+skWMhb/Px9e7Zj4vA7CAmnQaoHf45amAhA2P7tW1FMcvBywegCemKBKSkCNe9KK8LaaAHqkQoc1BGThStKVisF9GtUVqqyWKTRmVeOmexdWSVODx/IQ9ZP0q8ZQlbAyOMXmIhhkM4B4ocZ5QcFQxR7I+Q31ZywuWNp66fAUg0M662qd58rd5Q0rjsTEDJKJEKb0GQjoSfLXFwE0yODRJXOr80XQ2ZgOweST4G42gp1rIfulmoE5AkRp0tvIe6oeN2nxExp1wgNhYWlvAgPxWaQNf2NsULZau/Urd6QaXQ9ChrYH4STWpSOPU8jUtvUgXagprbUkJ5d/dq083KZ08DWHajDUdfp8RQKYY2aWdo6u1aBxG5QHdosvAWVBpvUgczQcrB4EnW6JyETissOTaAFPCP0sPnE/FIZOAv0je6nIERRmKEQPRUZJ2qfSNtVHjbs86uQi1vMZkui4Opx5EXKcD8qA08FH8Sn2Pl4XAqRy0UcASfN/npgvE8czY1SYi0S64o80768Fanv1zIYx8r4gEYgPqfhrBF3/GNV2KMr+DnBsQRVmFzrpGy5dCBS9BVfWqQbwQ2WqzI0JIul/C/9Ica847ytMW2eu5CPkd+IFFqmu2A7QMfgLLCatCtyjNWg3EaAg6onY9qzvfJ2lDEUKQ2KPJ6TulMhQyNMsl7BXIiEK9AQJ8q14Dl3K9DeuxFUvjXS7jt1RQsYkhsh9PR5kci4Rjk2okC8KwIXmVl0z53SIe94Eeu65xHlppQgbf6jhw9OaOWOr/V4HMJF9jMOdrSvv9xoROTr3tF5qh6dzfKwrWVLdOMSJhlHl5BY3erp06ZhtGiaGBr/+LJA1hpSg+lJw/nRDziJOYZvSShMRy9PdB7drv1tZB3Scuq2C5WJvzISmXRjMgSO02Wtm9dWLmuzhWJDLUMa9D3Kggwg0wfFr00e3ciMCHMsNZpj469G8qd1dRKcS2cShZEi6/fOyIvup2+CJlv5djFY5tS2MPl92wpA89nrWUxSqk3V+7m7FgUUJ68jzAZ5VS9wK3rNgCv9Bc3LoG4zoe080OyvR6ZyIgPjdPU3FPqzibIoCv2FhHa0pJapYQy9PMRZoViAEyOE6HmKBSBvmDQso1xsDgi0d5+IDJSu3UGoxOkiNDH6zo48GCHC7zd85WNurozApRKA+LZ0WzAlIDxxuO1OpI5az3x0A4OHUhK+ZKBJf64h2Y0gxx68HUM5bqg4BRi/i9dgN7wjchhr7rXvEigNxzPwqb9LoJegSepWwflrWv3m1f3dL/x4HahAZX+Ds9qNxtRs5UGcFjmE8NGeCv+HmrogSIADahoHg++inr2G8ZBFQ3kgXuEz1p1dhwc6tplFdeiRhGq7rmMqaUVsRbKiVA6UMm5taK+zIrQaFHZAJWKC6YVIHZxDDivJKtptw3klHHREAnZ4nfMSXiDm6skUC0NC8nJ0d8IvyBgMauvrrug/iM93r//VQNQhmCpx4rWlOSQuUCd2cPgriUd8djt97oxEEbb/3O39Eu2tafNmfG4BR/u6V05vf/Pq/uyD12lXO8DUyP8D2ftjWQj1LNcAAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQfjBQkOIDvH/eP6AAAISElEQVRo3u2Za4xcZRnHf+c658zOZZnZzi673ba0227ZdKAWFFGkCGi9BAX5Ihr9YDDRGPCDH0w0RaMfNFFiIAE1MUoCpIUYaAEhJEovAm2xpXtpl+52Zy/d2WG3M9s5cz33OX6Y7dQaY6Kwu6byfHvfc3mf/3mf5/9/nvcQrKDVTTv4zu5ng1K5FqymCUEQBKyglco1YtEwgiCwWrbioP8XTFzpBV3fIiC4ckFXrQInp56iEfgAFKuzvDJ4LwvF0SsXdEhu4+qrbkBYWkZTovQmvkw4lFxV0B/k9HKb32hQqZp4nr+qoOX3VY5qOWr2It2JNADzxVHOzu/jI30PElIivHVigl89eYxv3H0dn7n9ehYXF5mdnUWSZNrb4wiCiKLIzOVyaKEQnZ2dzM7O0t3dg+PYmKbJunXrEEUR13VRFAXf9wmCgGLRIBaLooZCjI2NkUwkaW+PoygKrutiWRaaplEsFt9f0H7DxfPN1thr2FjuPEHQWGJun1LdxXG9Zm4JAt3d3UxOTqFpIVQ1hGEY2JYNgCRJJBIJDMMgkUhQKpU5c+YMsqxQLpeJx2OEw2E8z8O2HQyjyIYNG3Bsh2q1QqlUolar0du7lkqlQrlSwayby5/TQdBAEJpZ5Hk+pUqdaJuOqspMT0+jaRqVSpVYPIaqNMHoepharUosFuf8+QV6enqwLJt6vYaqhhBFAcMwiEQipFIpstk5dF1DVVUikQh100TXNLLZOSKRNlKpFLlcjnrdbPqz0qAt20VVZVRF5uLS/1id/bM7F6/5vo8kSa173ktF956ILL9Yplypt8aj41kOvXlJg8dyr3Jw9CEuVGYAePq5I+z61lM89sRrNBoBk5NTVKtVLMuiXq/jeR5zc3PUajXGx89Sq9WwbZuFhQXGxsawLAvLsiiVytTrdWzbxlyac123Nec4zvIR2YnhKbrWxNm+bQMAx4em+evbWW69+VoEQSBfHqRQfZyKdReJ6HrO5Qz8IODcuxV838fzXHK5HL7v4/sNNm3aiG07lEolLMskk8mgaTrd3VezsHCe6ekZkskk42fHkUQJTQuhaTqCAJIkUygUiEQiRKNR1q9ftzI6bdkunucRadMBqJgLlM15UrF+FFljdDzL4OlZtm7qZMd1GykaBr7n4XkemqYTj8eYmTlHKKSiKMpFF1FVhXy+QDwea7G14zjoehhZkTHrdWRZxrJsQiEVSZLo6OhYneIkCAKCAESxmX+O41E3bXRNJRRqgmo0AgSBFe26/qPwdhwPRZFaDp7L5gmpCp2pdgICJucPYzqLbO35PJKo8vs9hxmfucC9u9Js37aB3Q+/yKHTBbatjfLI7ruZzuZ58vkT9HZG+eZXdyLLTR4QBJH+7l0IgshisYJRqrFpQ9dlEaWFlOUnskajwU8feZlzc4Wm5roeD//uEHte+NvFbWUk+xDvvHs/VSuP43jsOzzFodMFBkezWLZDJlcB4FS2Qt2yGZuY5+CpPC8fnaVSM7G9GqfmvsZobjeOVwPg1QMjfH33S60qrrBY5vs/24/jeMu/06Iosvu7n0VRmo8oisz37r/t0hcXBNJrf4LpLBLR1iCJMvfs3MjY9CLbB9aiaSp9PTGyxTzp3ihhPcTWvi5u27aGdV0xom06sgzpnqcQBBFVbgNg1yfT3LRjI7LclKuOZIxf/OBuVPW/5+CVzWnXo1630XWVkLp6OS3dd999P77IdI7jcPTYMYoXiniex+xsllRqTVOT8wUmJiYwDIPh4RGq1RoXLiziOA6xWAyATCZDuVwhHA5z5MhR3j55EkkUSSYTCILAmYk5Xj82ju/7dHcl8BsOC8Y7NAIXTW2+o163cT2/FVHL0mUNDQ23BoqiEDQCOjtTCILA3meexXXdpgYfP06hUCAajbL3mT8SDocpVyocOHCotav797+I53ns2fsM2bk5+rds5vFf/5aZmXMAPPfKEI8+d5qn9w/iuh6F8iRvnN3JW5mft7qwR//wGo89caDl08TUu+zdd3T5WktBEJBlGVlW6OxMYdsOxaJBo9Hg5OAQW7f2k0hcRSTSRkdHki2bN/PmkWN4nodlWQwNn6Kjo4Pn973EF79wFwMDA+z69J2MjIwAsL6nHUkQWN8dQ5JEwqEEqeiDXB3f2XRGEPjULf3ceUt/y6furgS3fax/ZSRLlmU+etOHyWQyKIqMaZqkUik87xJrxmJxent7mJ+fp1qtcusnPo5lmSSTV6GqIQDi7XEymUkAvnLPzXzpczeiqjKiKBLVU9x67UOwlM+CIHDD9Rsv8yOshwjroZXT6XR6G3/+y2vousb116VRFOUy0JIkcuMNOxgfP4thGHxox3YkScIyLVg6/HMdF03TmqDEBoJUQxQjly0tsLLHwf9Wp3t6eshm5zhw8DDpdPpf3jMwMMDrb7zJibdP0rdpE+3t7c26O5+n0WgwPn6WjddcA8DUwuu8OnInZ+b+1Pwgnsmh0R8xMX+w9b7chRHmjXeWtyI7fz7P4OAgAH19fczOZpFlmWQyQTgcZsuWzQwPn2Lt2h4ABgeHqFZrHD9+gjvuuH0p922SyWSLxR944Ns89vhvSCaTKIpCOr2tGRmiiix2IYmhlraHlA4USb9EprKOKEjLezBYLpdbOq3rOqZpIooibW3N4sCyLDzPJxJpjk3LwnNdJEkiHA4vyUwdQRDQ9UvOVyoVPM8jFou1+uBG4OO4NVQljCjIrJZ9cBq63GaUaux5/ggLeaM1l5map1I1r1zQlarJC4czFI1aa+70+Bylcv3KDm/Ldgmp8gd/La/onIbm/+lGI/j/AW1aDj/85ctUa+aqgv47z4zuLkRxSaQAAAAASUVORK5CYII=">' );
                                        $_tmpl->setVar( 'search_livivo', $__tmpl->parse() );
                                }
        
                                //version
                                if ( $chapter->getElement( 'version' ) || $chapter->getElement( 'next_version' ) || $chapter->getElement( 'previous_version' ) ) {
                                        
                                        $__tmpl->clear();
                                        $__tmpl->setVar( 'section-id', 'chapter-version' );
                                        $__tmpl->setVar( 'view_name', 'version' );
                                        $__tmpl->setVar( 'sbb_title', (string)$this->t( 'Version' ) );
                                        $__tmpl->setVar( 'sbb_content', $chapter->getElement( 'version' ) );
                                        $__tmpl->setVar( 'view_name', 'version' );
                                        
                                        $links = [];
                                        
                                        if ( !empty( $chapter->getElement( 'next_version' ) ) ) {
                                                $route_param = \Drupal::routeMatch()->getRawParameters()->all();
                                                $route_param[ 'cp_id' ] = $chapter->getElement( 'next_version' );
                                                $url = Url::fromRoute( \Drupal::routeMatch()->getRouteName(), $route_param );
                                                
                                                $__tmpl->appendToVar( 'sbb_content', '<div class="rwError">' . ( (string)$this->t( 'This version is not up-to-date' ) ) . '</div>' );
                                                
                                                $publication_date_next_version = $chapter->getRawElement( 'published', '<next>' );
                                                $publication_date_next_version = implode( '-', array_reverse( explode( '-', $publication_date_next_version ) ) );
                                                
                                                $links[] = Link::fromTextAndUrl( ( (string)$this->t( 'Version @date', [ '@date' => $publication_date_next_version ] ) ), $url )->toString();
                                        }
                                        
                                        if ( !empty( $chapter->getElement( 'previous_version' ) ) ) {
                                                $route_param = \Drupal::routeMatch()->getRawParameters()->all();
                                                $route_param[ 'cp_id' ] = $chapter->getElement( 'previous_version' );
                                                $url = Url::fromRoute( \Drupal::routeMatch()->getRouteName(), $route_param );
                                                
                                                $publication_date_previous_version = $chapter->getRawElement( 'published', '<previous>' );
                                                $publication_date_previous_version = implode( '-', array_reverse( explode( '-', $publication_date_previous_version ) ) );
                                                
                                                $links[] = Link::fromTextAndUrl( ( (string)$this->t( 'Version @date', [ '@date' => $publication_date_previous_version ] ) ), $url )->toString();
                                        }
                                        
                                        $__tmpl->appendToVar( 'sbb_content', '<div>' . implode( '<br>', $links ) . '</div>' );
                                        $_tmpl->setVar( 'chapter_version', $__tmpl->parse('::', 0) );
                                }
                                /*
                        //keywords
                        $keywords = json_decode($chapter->getElement('keywords'), true);
                        
                        if(count($keywords)){
                                
                                $__tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_sidebar_block.tmpl.inc.php');
                                $__vars = [];
                                $__vars['::sbb_title::'] = (string)t('Keywords');
                                $__vars['::sbb_content::'] = '<ul class="list-unstyled">';
                                $__vars['::view_name::'] = 'keywords';
                                
                                foreach($keywords as $_){
                                $__vars['::sbb_content::'] .= '<li><a href="#" class="intern">'.$_.'</a></li>';
                                }
                                
                                $__vars['::sbb_content::'] .= '</ul>';
                                $_vars['::chapter_keywords::'] = $this->renderVars($__tmpl, $__vars);
                        }
                        */
                                
                                //Data
                                $__tmpl->clear();
                                $__tmpl->setVar( 'section-id', 'chapter-publicationdata' );
                                $__tmpl->setVar( 'view_name', 'publicationdata' );
                                $__tmpl->setVar( 'sbb_title', (string)$this->t( 'Publication data' ) );
                                $__tmpl->setVar( 'sbb_content', '' );
                                
                                if ( $this->getControlElement( 'show_submission_date' ) == 1 && $chapter->getDate( 'received', $lang ?? null ) ) {
                                        $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'Received: @date', [ '@date' => date( 'Y-m-d', strtotime( $chapter->getDate( 'received', $lang ?? null ) ) ) ] ) );
                                }
                                
                                if ( $this->getControlElement( 'show_revision_date' ) == 1 && $chapter->getDate( 'revised', $lang ?? null
                                        ) ) {
                                        if ( $__tmpl->getVar( 'sbb_content' ) ) $__tmpl->appendToVar( 'sbb_content', '<br>' );
                                        $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'Revised: @date', [ '@date' => date( 'Y-m-d', strtotime( $chapter->getDate( 'revised', $lang ?? null ) ) ) ] ) );
                                }
                                
                                if ( $this->getControlElement( 'show_acceptance_date' ) == 1 && $chapter->getDate( 'accepted', $lang ?? null ) ) {
                                        if ( $__tmpl->getVar( 'sbb_content' ) ) $__tmpl->appendToVar( 'sbb_content', '<br>' );
                                        $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'Accepted: @date', [ '@date' => date( 'Y-m-d', strtotime( $chapter->getDate( 'accepted', $lang ?? null ) ) ) ] ) );
                                }
                                
                                if ( $this->getControlElement( 'show_publication_date' ) == 1 && $chapter->getDate( 'published', $lang ?? null ) ) {
                                        
                                        if ( $__tmpl->getVar( 'sbb_content' ) ) $__tmpl->appendToVar( 'sbb_content', '<br>' );
                                        $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'Published: @date', [ '@date' => date( 'Y-m-d', strtotime( $chapter->getDate( 'published', $lang ?? null ) ) ) ] ) );
                                }
                                
                                if ( $__tmpl->getVar( 'sbb_content' ) ) {
                                        $_tmpl->appendToVar( 'dates', $__tmpl->parse('::', 0) );
                                }
                                
                                if ( \Drupal::service( 'session' )->get( 'user' )[ 'weight' ] > 70 && !$chapter->getElement( 'next_version' ) ) {
                                        
                                        switch ( $this->getElement( 'theme' ) ) {
                                                
                                                case 'gms':
                                                        $templateTheme = 'GMS';
                                                        $linkClasses = [ 'btnPublissoDefault' ];
                                                        break;
                                                
                                                default:
                                                        $templateTheme = 'publisso_gold';
                                                        $linkClasses = [ 'link--calltoaction', 'inline', 'btn' ];
                                                        $linkClasses = [ 'btn', 'btnPublissoDefault' ];
                                        }
                                        
                                        $_urlErratum = Url::fromRoute( 'publisso_gold.book.chapter.seterratum', [ 'cp_id' => $chapter->getElement( 'id' ) ], [ 'attributes' => [ 'class' => $linkClasses ] ] );
                                        $_lnkErratum = Link::fromTextAndUrl( (string)$this->t( 'Set erratum' ), $_urlErratum );
                                        $_tmpl->setVar( 'lnk_set_erratum', $_lnkErratum->toString() );
                                }
                                
                                $template->setVar( 'content_item_chapter_content', $_tmpl->parse('::', 0) );
                        }
                        else {
                                #$template->setVar( 'tab_about_class_active', 'active' );
                                #$template->setVar( 'content_about_class_active', 'active' );
                                
                                
                        }
        
                        $this->overwriteActiveTabs( $active_tab, $template );
                        
                        return $template->parse();
                }
        
                /**
                 * @param $structure
                 * @param $level
                 * @return string
                 */
                private function parseChapterlistStructure ($structure, $level ) {
                        
                        $markup = '';
                        $class_levels = [ '1' => 'nav', '2' => 'dropdown-menu sub-menu', '3' => 'sub-menu deep-level', '4' => 'expanded', '5' => 'expanded last' ];
                        
                        if ( count( $structure ) > 0 ) {
                                
                                $markup .= '<ul>';
                                if ( is_array( $structure ) ) {
                                        foreach ( $structure as $item ) {
                                                
                                                $childs = $item[ 'childs' ];
                                                $item = $item[ 'item' ];
                                                
                                                $markup .= '<li>';
                                                
                                                $link = false;
                                                
                                                if ( !empty( $item->getElement( 'link' ) ) ) {
                                                        
                                                        $link = $item->getElement( 'link' );
                                                        
                                                        if ( substr( $link, 0, 9 ) == 'intern://' ) {
                                                                
                                                                $link = str_replace( 'intern://', '', $link );
                                                                list( $sub_medium, $sub_medium_id ) = explode( '/', $link );
                                                                
                                                                $chapter = new Bookchapter( $sub_medium_id, true );
                                                                $url = $chapter->getChapterLink( 'url' );
                                                                $link = Link::fromTextAndUrl( $item->getElement( 'title' ), $url );
                                                                
                                                        }
                                                        elseif(UrlHelper::isValid($link)){
                                                                $url = Url::fromUri($link, ['attributes' =>
                                                                        ['target' => '_blank']]);
                                                                $link = Link::fromTextAndUrl($item->getElement('title'), $url);
                                                        }
                                                }
                                                
                                                $markup .= $link ? $link->toString() : $item->getElement( 'title' );
                                                
                                                if ( $item->getElement( 'description' ) != '' ) {
                                                        $markup .= '<div class="description">' . $item->getElement( 'description' ) . '</div>';
                                                }
                                                
                                                if ( $item->getElement( 'authors' ) != '' ) {
                                                        $markup .= '<div class="authors">' . $item->getElement( 'authors' ) . '</div>';
                                                }
                                                
                                                if ( count( $childs ) > 0 ) {
                                                        $markup .= $this->parseChapterlistStructure( $childs, $level + 1 );
                                                }
                                                
                                                $markup .= '</li>';
                                        }
                                }
                                $markup .= '</ul>';
                        }
                        
                        return $markup;
                }
        
                /**
                 * @param $chapter
                 * @return string|string[]|null
                 */
                private function getBookChapterNavigation ($chapter ) {
                        
                        $template = new \Drupal\publisso_gold\Controller\Template();
                        $template->get( 'book_chapter_navigation' );
                        
                        $nextChapterID = $this->getNextChapterID( $chapter->getElement( 'id' ) );
                        
                        if ( $nextChapterID != $chapter->getElement( 'id' ) ) {
                                $urlNext = Url::fromRoute( 'publisso_gold.book.chapter', [ 'bk_id' => $this->id, 'cp_id' => $this->getNextChapterID( $chapter->getElement( 'id' ) ) ] );
                                $template->setVar( 'lnkNextChapter', Link::fromTextAndUrl( (string)t( 'Go to next chapter' ), $urlNext )->toString() );
                        }
                        else {
                                $template->setVar( 'lnkNextChapter', (string)t( 'Go to next chapter' ) );
                        }
                        
                        $prevChapterID = $this->getPreviousChapterID( $chapter->getElement( 'id' ) );
                        
                        if ( $prevChapterID != $chapter->getElement( 'id' ) ) {
                                $urlPrev = Url::fromRoute( 'publisso_gold.book.chapter', [ 'bk_id' => $this->id, 'cp_id' => $this->getPreviousChapterID( $chapter->getElement( 'id' ) ) ] );
                                $template->setVar( 'lnkPrevChapter', Link::fromTextAndUrl( (string)t( 'Go to previous chapter' ), $urlPrev )->toString() );
                        }
                        else {
                                $template->setVar( 'lnkPrevChapter', (string)t( 'Go to previous chapter' ) );
                        }
                        
                        return $template->parse();
                }
        
                /**
                 * @param $chapter
                 * @return string|string[]|null
                 */
                private function getBookSearch ($chapter ) {
                        
                        $template = new \Drupal\publisso_gold\Controller\Template();
                        $template->get( 'publisso_gold_book_content_sidebar_noheadline_block' );
                        
                        $template->setVar( 'sbb_title', (string)t( \Drupal::service( 'publisso_gold.texts' )->get( 'book.content.sidebar.booksearch.title', 'fc' ) ) );
                        
                        $renderer = \Drupal::service( 'renderer' );
                        $form = \Drupal::formBuilder()->getForm( '\\Drupal\\publisso_gold\\Form\\BookSearch', [ 'bk_id' => $this->id ] );
                        
                        $template->setVar( 'sbb_content', $renderer->render( $form )
                        );
                        
                        return $template->parse();
                }
        
                /**
                 * @return bool
                 */
                public function hasLogo(){
                        return $this->elements['logo'] !== null;
                }
        }
