<?php
        
        /**
         * @file
         * Contains \Drupal\publisso_gold\Controller\Userprofile.
         */
        
        namespace Drupal\publisso_gold\Controller;
        
        /**
         * Class Userprofile
         * @package Drupal\publisso_gold\Controller
         */
        class Userprofile {
                
                private $id;
                private $login_id;
                private $elements          = [];
                private $tbl_column_prefix = 'up_';
        
                /**
                 * @return string
                 */
                public function __toString () {
                        return __CLASS__;
                }
        
                /**
                 * Userprofile constructor.
                 * @param null $id
                 * @param null $login_id
                 */
                public function __construct ($id = null, $login_id = null ) {
                        
                        $this->id = $id;
                        $this->login_id = $login_id;
                        $this->load( $this->id, $this->login_id );
                }
        
                /**
                 * @return mixed|null
                 */
                public function getId () {
                        return $this->id;
                }
        
                /**
                 * @return mixed|null
                 */
                public function getLoginId () {
                        return $this->login_id;
                }
        
                /**
                 * @param array $affiliations
                 * @throws \Exception
                 */
                public function setAdditionalAffiliations(array $affiliations){
                        
                        foreach($affiliations as $k => $v) $affiliations[$k]['profile_id'] = $this->id;
                        \Drupal::database()->delete('rwPubgoldUserProfileAddAffiliation')->condition('profile_id', $this->id, '=')->execute();
                        Publisso::log(print_r($affiliations, 1));
                        
                        foreach($affiliations as $affiliation){
                                $qry = \Drupal::database()->insert('rwPubgoldUserProfileAddAffiliation');
                                $qry->fields($affiliation)->execute();
                        }
                }
                
                public function getAdditionalAffiliations() : array {
                        return \Drupal::database()->select('rwPubgoldUserProfileAddAffiliation', 't')->fields('t', ['profile_id', 'institute', 'department', 'city', 'country'])->condition('profile_id', $this->id, '=')->execute()->fetchAll(\PDO::FETCH_ASSOC);
                }
        
                /**
                 * @param $elemName
                 * @param null $elemValue
                 * @param bool $storeOnDB
                 * @return bool
                 */
                public function setElement ($elemName, $elemValue = null, $storeOnDB = true ) {
                        
                        if ( array_key_exists( $elemName, $this->elements ) || !$storeOnDB ) {
                                
                                $oldValue = $this->elements[ $elemName ] ?? null;
                                $this->elements[ $elemName ] = $elemValue;
                                
                                if ( $storeOnDB === false )
                                        return true;
                                
                                if ( $this->save( $elemName ) === false ) {
                                        
                                        $this->elements[ $elemName ] = $oldValue;
                                        return false;
                                }
                                else {
                                        return true;
                                }
                        }
                        else {
                                return false;
                        }
                }
        
                /**
                 * @param $elemName
                 * @return mixed|string
                 */
                public function getElement ($elemName ) {
                        
                        if ( array_key_exists( $elemName, $this->elements ) )
                                return $this->elements[ $elemName ];
                        else
                                return '';
                }
        
                /**
                 * @param null $elemName
                 * @return \Drupal\Core\Database\StatementInterface|false|int|string|null
                 */
                public function save ($elemName = null ) {
                        
                        if ( !$this->id ) return false;
                        
                        $fields = $this->elements;
                        
                        if ( $elemName !== null ) { //if specific element selected
                                $fields = [ $elemName => $fields[ $elemName ] ];
                        }
                        
                        foreach ( $fields as $k => $v ) {
                                $fields[ $this->tbl_column_prefix . $k ] = $v;
                                unset( $fields[ $k ] );
                        }
                        
                        return \Drupal::database()->update( 'rwPubgoldUserProfiles' )
                                      ->fields( $fields )
                                      ->condition( $this->tbl_column_prefix . 'id', $this->id )
                                      ->execute()
                                ;
                }
                
                private function load () {
                        
                        if ( $this->id || $this->login_id ) {
                                
                                $query = \Drupal::database()->select( 'rwPubgoldUserProfiles', 'u' )
                                                ->fields( 'u', [] )
                                ;
                                
                                if ( $this->id )
                                        $query->condition( $this->tbl_column_prefix . 'id', $this->id, '=' );
                                
                                if ( $this->login_id )
                                        $query->condition( $this->tbl_column_prefix . 'uid', $this->login_id, '=' );
                                
                                /*
                                $condition = $query->orConditionGroup()
                                                   ->condition($this->tbl_column_prefix.'id', $this->id, '=')
                                                   ->condition($this->tbl_column_prefix.'uid', $this->login_id, '=');
                                
                                $query->condition($condition);
                                */
                                $user = $query->execute()->fetchAssoc();
                                
                                if ( is_array( $user ) ) {
                                        foreach ( $user as $k => $v ) {
                                                
                                                if ( $k != $this->tbl_column_prefix . 'id' && $k != $this->tbl_column_prefix . 'uid' ) //store book except id
                                                        $this->elements[ str_replace( $this->tbl_column_prefix, '', $k ) ] = $v;
                                                
                                                if ( $k == $this->tbl_column_prefix . 'id' )
                                                        $this->id = $v;
                                                
                                                if ( $k == $this->tbl_column_prefix . 'uid' )
                                                        $this->login_id = $v;
                                        }
                                }
                        }
                }
        
                /**
                 * @return array
                 */
                public function getElementKeys () {
                        return array_keys( (array)$this->elements );
                }
        
                /**
                 * @return array
                 */
                public function getKeys () {
                        return array_keys( (array)$this->elements );
                }
        
                /**
                 * @param $uid
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 * @throws \Exception
                 */
                public function makeSnapshot ($uid ) {
                        return \Drupal::database()->insert( 'rwPubgoldUserProfiles_Archive' )->fields( [
                                                                                                               'data' => base64_encode( json_encode( $this->elements ) ),
                                                                                                               'uid'  => $uid,
                                                                                                       ]
                        )->execute()
                                ;
                }
        
                /**
                 * @param string $sort
                 * @param string $delimiter
                 * @return string
                 */
                public function getReadableName ($sort = 'LF', $delimiter = ', ' ) {
                        
                        $name = [
                                $this->getElement( 'lastname' ),
                                $this->getElement( 'firstname' ),
                        ];
                        
                        if ( strtoupper( $sort ) == 'FL' ) $name = array_reverse( $name );
                        
                        return implode( $delimiter, array_filter( $name ) );
                }
        
                /**
                 * @param string $sort
                 * @param string $delimiter
                 * @return string
                 */
                public function getReadableFullName ($sort = 'LF', $delimiter = ', ' ) {
                        
                        $name = [
                                $this->getElement( 'lastname' ),
                                $this->getElement( 'firstname' ),
                        ];
                        
                        if ( strtoupper( $sort ) == 'FL' ) $name = array_reverse( $name );
                        
                        $name = [
                                $this->getElement( 'graduation' ),
                                implode( $delimiter, array_filter( $name ) ),
                                $this->getElement( 'graduation_suffix' ),
                        ];
                        
                        return implode( ' ', array_filter( $name ) );
                }
        
                /**
                 * @return string
                 */
                public function getAffiliation () {
                        
                        return implode( ', ', array_filter( [
                                                                    $this->getElement( 'department' ),
                                                                    $this->getElement( 'institute' ),
                                                                    $this->getElement( 'city' ),
                                                                    \Drupal::service( 'publisso_gold.tools' )->getCountry( $this->getElement( 'country' ) ),
                                                            ]
                        )
                        );
                }
        
                /**
                 * @param $id
                 */
                public function loadSnapshot ($id ) {
                        
                        $res = \Drupal::database()->select( 'rwPubgoldUserProfiles_Archive', 't' )->fields( 't', [] )->condition( 'id', $id, '=' )->execute()->fetchAssoc();
                        
                        if ( is_array( json_decode( base64_decode( $res[ 'data' ] ), true ) ) ) {
                                foreach ( json_decode( base64_decode( $res[ 'data' ] ), true ) as $k => $v ) {
                                        $this->setElement( $k, $v, false );
                                }
                        }
                }
        
                /**
                 * @return string
                 */
                public function getFirstnameInitials () {
                        
                        $names = array_filter( preg_split( '/([\\s-]+)/u', $this->getElement( 'firstname' ), -1 ) );
                        $ret = '';
                        
                        for ( $i = 0; $i < 2; $i++ ) {
                                $ret .= mb_substr( $names[ $i ], 0, 1 );
                        }
                        
                        return strtoupper($ret);
                }
        
                /**
                 * @return bool
                 */
                public function agreedLatestPrivacyProtection () {
                        
                        return strtotime(
                                       \Drupal::database()->select( 'rwPubgoldPrivacyProtection', 't' )->fields( 't', [ 'id' ] )->orderBy( 'id', 'DESC' )->range( 0, 1 )->execute()->fetchField()
                               ) < strtotime( $this->getElement( 'agree_privacy_protection' ) );
                }
        
                /**
                 * @return array
                 */
                public function getAreasOfExpertise () {
                        
                        $aoe = [];
                        
                        $aoe_ids = \Drupal::database()->select( 'rwPubgoldUserAreasOfExpertise', 't' )->fields( 't', [ 'uaoe_id' ] )
                                          ->condition( 'uaoe_uid', $this->login_id, '=' )->execute()->fetchCol()
                        ;
                        
                        if ( $aoe_ids ) {
                                $aoe = \Drupal::database()->select( 'rwPubgoldAreasOfExpertise', 't' )->fields( 't', [ 'aoe_name' ] )
                                              ->condition( 'aoe_id', $aoe_ids, 'IN' )->execute()->fetchCol()
                                ;
                        }
                        
                        $aoe[] = $this->getElement( 'area_of_expertise' );
                        
                        return array_filter( $aoe );
                }
        
                /**
                 * @return array
                 */
                public function getRenderableVcard () {
                        
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        $tools = \Drupal::service( 'publisso_gold.tools' );
                        $t = \Drupal::service( 'string_translation' );
                        
                        $rows = [];
                        $_w = '20%';
                        
                        $data = [
                                'institute', 'city', 'street', 'email', 'department', 'telephone',
                                'orcid',
                        ];
                        
                        foreach ( $data as $_ ) {
                                
                                $cell1 = [ 'data' => (string)$t->translate( $texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                                if ( !empty( $this->getElement( $_ ) ) )
                                        $rows[] = [ $cell1, [ 'data' => $this->getElement( $_ ) ] ];
                        }
                        
                        $_ = 'country';
                        $cell1 = [ 'data' => (string)$t->translate( $texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $this->getElement( $_ ) ) )
                                $rows[] = [ $cell1, [ 'data' => $tools->getCountry( $this->getElement( $_ ) ) ] ];
                        
                        $_ = 'area_of_expertise';
                        $cell1 = [ 'data' => (string)$t->translate( $texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( count( $this->getAreasOfExpertise() ) )
                                $rows[] = [ $cell1, [ 'data' => implode( ', ', $this->getAreasOfExpertise() ) ] ];
                        
                        $_ = 'correspondence_language';
                        $cell1 = [ 'data' => (string)$t->translate( $texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $this->getElement( $_ ) ) )
                                $rows[] = [ $cell1, [ 'data' => \Drupal::service( 'language_manager' )->getLanguageName( $this->getElement( $_ ) ) ] ];
                        
                        $_ = 'agree_privacy_protection';
                        $cell1 = [ 'data' => (string)$t->translate( $texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $this->getElement( $_ ) ) )
                                $rows[] = [ $cell1, [ 'data' => (string)$t->translate( $texts->get( 'userprofile.data_policy.' . ( $this->agreedLatestPrivacyProtection() ? 'accepted' : 'not_accepted' ) ) ) ] ];
                        
                        $_ = 'show_data_in_boards';
                        $cell1 = [ 'data' => (string)$t->translate( $texts->get( 'userprofile.' . $_, 'fc' ) ), 'width' => $_w, 'class' => 'font_bold' ];
                        if ( !empty( $this->getElement( $_ ) ) )
                                $rows[] = [ $cell1, [ 'data' => (string)$t->translate( $texts->get( 'global.bool_value.' . ( $this->agreedLatestPrivacyProtection() ? 'yes' : 'no' ) ) ) ] ];
                        
                        return [
                                '#type' => 'table',
                                '#rows' => $rows,
                        ];
                }
        }

?>
