<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\System.
 */

namespace Drupal\publisso_gold\Controller;

use Drupal;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\Unicode;
use Drupal\publisso_gold\Classes\AuthorsContract;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns autosave responses.
 */
class System extends ControllerBase {

        protected $modname = 'publisso_gold';
        protected $modpath;
        protected $session;
        protected $tools;

        public function __construct() {
                $this->modpath = drupal_get_path('module', $this->modname);
                require_once($this->modpath . '/inc/publisso_gold.lib.inc.php');

                $this->session = \Drupal::service('session');
                $this->tools = \Drupal::service('publisso_gold.tools');
        }
        
        /**
         * @param $fid
         * @return Response
         */
        public function getFile($fid) {

                $blob = new \Drupal\publisso_gold\Controller\Blob($fid);
                
                $filename = $blob->meta['name'];
                
                if(array_key_exists('mime', $blob->meta)) {
                        $ext = \Drupal::service( 'publisso_gold.tools' )->mime2ext( $blob->meta[ 'mime' ] );
        
                        if ( $ext && !preg_match( '/\.' . $ext . '$/', $filename ) ) {
                                $filename .= ".$ext";
                        }
                }
                
                $response = new Response();
                $response->headers->set('Content-Type', $blob->type);
                $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');
                $response->setContent($blob->data);

                return $response;
        }
        
        /**
         * @return Response
         */
        public function getSession() {

                $response = new Response();
                $content = '';

                if (strtolower(\Drupal::service('publisso_gold.setup')->getValue('system.sso.active')) != 'yes') {
                        $response->setContent($content);
                        return $response;
                }

                if (\Drupal::service('session')->get('logged_in') === true) {

                        $user_local = new \Drupal\publisso_gold\Controller\User(\Drupal::service('session')->get('user')['id']);

                        $userdata = [
                                'institut' => $user_local->profile->getElement('institute'),
                                'LKURZ' => preg_match('/^\d+$/', $user_local->profile->getElement('country')) ? array_search(getCountry($user_local->profile->getElement('country')), getCountrylist()) : $user_local->profile->getElement('country'),
                                'LORT' => $user_local->profile->getElement('city'),
                                'name' => implode(', ', [$user_local->profile->getElement('lastname'), $user_local->profile->getElement('firstname')]),
                                'LSTRAS' => $user_local->profile->getElement('street'),
                                'LPZAHL' => $user_local->profile->getElement('postal_code'),
                                'LTELNR' => $user_local->profile->getElement('telephone'),
                                'LSPRACHE' => $user_local->profile->getElement('correspondence_language'),
                                'LLAND' => getCountry($user_local->profile->getElement('country')),
                                'PRGRP' => 'z2',
                                'LEMAILADR' => $user_local->profile->getElement('email'),
                                'remote_id' => $user_local->getElement('zbmed_kennung'),
                                'sso_system' => \Drupal::service('publisso_gold.setup')->getValue('system.sso.name')
                        ];

                        $userdata = base64_encode(serialize($userdata));

                        $key = pack('H*', \Drupal::service('publisso_gold.setup')->getValue('system.sso.crypt_key'));
                        $content = bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $userdata, MCRYPT_MODE_ECB));

                        $data = base64_decode(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, hex2bin($content), MCRYPT_MODE_ECB));
                        $content .= '<br>' . $data;
                }

                $response->setContent($content);
                return $response;
        }
        
        /**
         * @param Request $request
         * @return JsonResponse
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        public function getContact(Request $request) {

                $response = new JsonResponse();
                $url = 'https://www.publisso.de/?type=1419337801&tx_dreipccontact_contactbox[action]=getContacts&tx_dreipccontact_contactbox[controller]=Contact';
                $position = $request->get('tx_dreipccontact_contactbox')['position'];
                $client_response = \Drupal::httpClient()->request('GET', $url, ['headers' => ['Content-Type' => 'application/text'], 'http_errors' => false, 'query' => [
                                'type' => 1419337801,
                                'tx_dreipccontact_contactbox' => [
                                        'action' => 'getContacts',
                                        'controller' => 'Contact',
                                        'position' => $position
                                ]
                ]]);

                $ret = $client_response->getBody()->getContents();

                $response->setContent(str_replace('src=\"\/uploads', 'src=\"https:\/\/www.publisso.de\/uploads', $ret));

                //$response->setContent(str_replace('\/uploads"', 'https:\/\/www.publisso.de\/uploads', $client_response->getBody()->getContents()));
                return $response;
                echo '<pre>' . print_r($client_response->getBody()->getContents(), 1) . '</pre>';
                exit();
        }
        
        /**
         * @param Request $request
         * @return JsonResponse|RedirectResponse
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        public function setToken(Request $request) {

                $response = new JsonResponse();
                $session = \Drupal::service('session');
                //error_log(print_r($session->get('user'), 1));
                $ary['route_name'] = \Drupal::routeMatch()->getRouteName();
                $ary['route_param'] = \Drupal::routeMatch()->getRawParameters()->all();

                if (strtolower(\Drupal::service('publisso_gold.setup')->getValue('system.sso.active')) != 'yes') {
                        $response->setContent('');
                        return $response;
                }

                $token = $request->get('token');

                if (!$token) {
                        $response->setContent('');
                        return $response;
                }

                $_SESSION['sso_token'] = $token;
                $this->session->set('sso_token', $token);

                $client_response = \Drupal::httpClient()->request('POST', 'https://lvps46-163-114-198.dedicated.hosteurope.de/zbmedauth/sso', ['headers' => ['Content-Type' => 'application/text'], 'http_errors' => false, 'verify' => false, 'query' => ['sso_system' => \Drupal::service('publisso_gold.setup')->getValue('system.sso.name'), 'token' => $token, 'getSession' => '1', 'apikey' => \Drupal::service('publisso_gold.setup')->getValue('system.sso.apikey')]]);
                //error_log('SSO-Request: '.print_r(['headers' => ['Content-Type' => 'application/text'], 'http_errors' => false, 'query' => ['sso_system' => $_SESSION['setup']->getValue('system.sso.name'), 'token' => $token, 'getSession' => '1', 'apikey' => $_SESSION['setup']->getValue('system.sso.apikey')]], 1));

                if ($client_response->getStatusCode() == 200) {

                        $data = $client_response->getBody()->getContents();

                        $key = pack('H*', \Drupal::service('publisso_gold.setup')->getValue('system.sso.crypt_key'));
                        $data = unserialize(base64_decode(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, hex2bin($data), MCRYPT_MODE_ECB)));

                        if ($data['remote_id'] && !$_SESSION['orig_user']) {
                                //error_log('User SSO: '.$data['remote_id']);
                                if ($_SESSION['user']['id']) {

                                        $id = $session->get('user')['id'];
                                        $user = new \Drupal\publisso_gold\Controller\User($id);
                                }
                                //error_log(print_r($user, 1));
                                if (!$id || $user->getElement('initial_email') != $data['remote_id']) {

                                        $id = \Drupal::database()->select('rwPubgoldUsers', 't')
                                                ->fields('t', ['id'])
                                                ->condition('initial_email', $data['remote_id'], '=')
                                                ->execute()
                                                ->fetchField();
                                        $user = new \Drupal\publisso_gold\Controller\User($id);
                                }



                                //$_SESSION['user'] = array();
                                $sessUser = $session->get('user');
                                if (!is_array($sessUser))
                                        $sessUser = [];

                                $sessUser['name'] = $user->getElement('user');
                                $sessUser['role'] = $user->role['name'];
                                $sessUser['id'] = $user->getElement('id');
                                $sessUser['weight'] = $user->role['weight'];
                                $sessUser['pw_change'] = $user->getElement('pw_change_required');
                                $sessUser['role_id'] = $user->role['id'];
                                //$_SESSION['user']['password' ] = $form_state->getValue('password');

                                $_SESSION['logged_in'] = true;
                                $session->set('user', $sessUser);
                                $session->set('logged_in', true);
                                //error_log('New Session built!');
                                return $this->redirect($ary['route_name'], $ary['route_param']);
                        }
                        elseif (!$_SESSION['orig_user']) {
                                unset($_SESSION['user']);
                                $_SESSION['logged_in'] = false;
                        }
                } else {
                        error_log($client_response->getStatusCode() . ': ' . $client_response->getBody()->getContents());
                }
                $response->setContent('DATA:' . print_r($client_response->getBody()->getContents(), 1));
                return $response;
        }
        
        /**
         * @param Request $request
         * @param $answer_for
         * @return Response
         */
        public function uploadFile(Request $request, $answer_for) {

                $response = new Response();
                $response->headers->set('Content-type', 'text/plain');

                $host = $request->server->get('HTTP_HOST');
                $ref = $request->server->get('HTTP_REFERER');

                $ref = preg_replace('/^[a-zA-Z]+:\/\//', '', $ref);
                $ref = explode('/', $ref);
                $ref = $ref[0];

                $cont = '';

                if ($ref === $host) {

                        $req = $request->files->get('file');
                        if (!$req)
                                $req = $request->files->get('upload');

                        if ($req) {

                                if (preg_match('/^(image|video)\//', $req->getClientMimeType())) {

                                        if (!preg_match('/^image\/tif/', $req->getClientMimeType())) {

                                                if (!$req->getError()) {
                                                        error_log('Beginne Upload');
                                                        $new_file_name = explode('.', $req->getClientOriginalName());
                                                        $new_file_name = time() . uniqid() . '.' . strtolower(end($new_file_name));
                                                        $dir = \Drupal::service('publisso_gold.tools')->getUserHomeDir();

                                                        try {
                                                                $req->move(DRUPAL_ROOT . "/" . $dir, $new_file_name);
                                                                $cont = "/$dir/$new_file_name";
                                                        } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                                                                error_log('Error in file "' . __FILE__ . '" on line ' . __LINE__ . ': ' . $e->getMessage() . ': ' . $req->getErrorMessage());
                                                                $cont = 'ERROR: Can\'t save uploaded File';
                                                        }

                                                        //search for duplicates - if found - delete the new file and return former uploaded one
                                                        if (substr($cont, 0, 5) != 'ERORR') {

                                                                $newMD5 = md5_file($cont);
                                                                $dh = opendir($dir);

                                                                while (($file = readdir($dh)) !== false) {

                                                                        if (!preg_match('/^\.+$/', $file)) { //not the parent and current directory
                                                                                if (md5_file("$dir/$file") == $newMD5 && $file != $new_file_name) { //file is uploaded earlier
                                                                                        unlink($cont);
                                                                                        $cont = DRUPAL_ROOT . "/$dir/$file";
                                                                                        error_log($cont);
                                                                                        break;
                                                                                }
                                                                        }
                                                                }
                                                        }
                                                } else {
                                                        error_log('Error in file "' . __FILE__ . '" on line ' . __LINE__ . ': ' . $req->getError() . ': ' . $req->getErrorMessage());
                                                        $cont = 'ERROR: ' . $req->getError() . ': ' . $req->getErrorMessage();
                                                }
                                        } else {
                                                error_log('Error in file "' . __FILE__ . '" on line ' . __LINE__ . ': Disallowed upload of *.tif(f)-Image!');
                                                $cont = 'ERROR: Upload of *.tif(f)-Files is not enabled!';
                                        }
                                } else {
                                        error_log('Error in file "' . __FILE__ . '" on line ' . __LINE__ . ': ' . 'File-type not accepted!');
                                        $cont = 'ERROR: ' . ((string) $this->t('File-type not accepted!'));
                                }
                        } else {
                                error_log('Error in file "' . __FILE__ . '" on line ' . __LINE__ . ': ' . 'Unknown upload-error!');
                                $cont = 'ERROR: ' . ((string) $this->t('Unknown upload-error!'));
                        }
                }

                if ($answer_for == 'default') {
                        $response->setContent($cont);
                } elseif ('ckeditor') {

                        if (substr($cont, 0, 5) == 'ERROR') {
                                $response->setContent(json_encode(['uploaded' => 0, 'error' => ['message' => $cont]]));
                        } else {
                                $cont = str_replace(DRUPAL_ROOT, '', $cont);
                                $response->setContent(json_encode(['uploaded' => 1, 'fileName' => $new_file_name, 'url' => $cont]));
                        }
                }
                return $response;
        }
        
        /**
         * @param $type
         * @param $id
         * @return BinaryFileResponse
         * @throws Drupal\Core\Entity\EntityStorageException
         */
        public function getPicture($type, $id) {
                
                $default = [];
                
                switch ($type) {

                        case 'up': //user-picture
                                $table = 'rwPubgoldUserProfiles';
                                $cols = ['up_picture' => 'picture', 'up_picture_type' => 'picture_type'];
                                $id_col = 'up_uid';
                                break;

                        case 'pv': //user-picture profile-preview
                                $table = 'rwPubgoldUserprofilePreview';
                                $cols = ['pv_picture' => 'picture', 'pv_picture_type' => 'picture_type'];
                                $id_col = 'pv_id';
                                break;

                        case 'bc': //book-cover
                                $table = 'rwPubgoldBooks';
                                $cols = ['bk_cover' => 'picture', 'bk_cover_type' => 'picture_type'];
                                $id_col = 'bk_id';

                                if (is_file(\Drupal::service('module_handler')->getModule('publisso_gold')->getPath() . '/images/book-cover-default.png')) {
                                        $default['picture'] = bin2hex(file_get_contents(\Drupal::service('module_handler')->getModule('publisso_gold')->getPath() . '/images/book-cover-default.png'));
                                        $default['type'] = 'image/png';
                                }
                                break;

                        case 'bl': //book-logo
                                $table = 'rwPubgoldBooks';
                                $cols = ['bk_logo' => 'picture', 'bk_logo_type' => 'picture_type'];
                                $id_col = 'bk_id';
                                break;

                        case 'bsm': //book-sustaining member
                                $table = 'rwPubgoldBooksSustainingMembers';
                                $cols = ['bsm_image' => 'picture', 'bsm_type' => 'picture_type'];
                                $id_col = 'bsm_id';
                                break;

                        case 'jsm': //journal-sustaining member
                                $table = 'rwPubgoldJournalsSustainingMembers';
                                $cols = ['jsm_image' => 'picture', 'jsm_type' => 'picture_type'];
                                $id_col = 'bsm_id';
                                break;

                        case 'ul': //uploaded files

                                if ( ($file = File::load( $id)) !== null) {
                                        $file->setPermanent();
                                        \Drupal::service('file.usage')->add($file, 'publisso_gold', 'user', 1);
                                        $file->save();
                                        
                                        $path = \Drupal::service('file_system')->realpath($file->toArray()['uri'][0]['value']);
        
                                        if(($content = @file_get_contents($path)) !== false) {
                                                $picture['picture'] = bin2hex($content);
                                                $picture['picture_type'] = $file->getMimeType();
                                        }
                                }
                                break;
                }

                if (isset($table) && is_array($cols) && isset($id_col)) {

                        $qry = \Drupal::database()->select($table, 't');

                        foreach ($cols as $col => $alias)
                                $qry->addField('t', $col, $alias);
                        $qry->condition($id_col, $id, '=');
                        $picture = $qry->execute()->fetchAssoc();

                        if (empty($picture['picture']) && array_key_exists('picture', $default)) {
                                $picture['picture'] = $default['picture'];
                                $picture['picture_type'] = $default['type'];
                        }

                        //is string binary -> convert to hex
                        if (preg_match('~[^\x20-\x7E\t\r\n]~', $picture['picture']) > 0) {
                                $picture['picture'] = bin2hex($picture['picture']);
                        }
                }

                if (!isset($picture) || !is_array($picture) || empty($picture['picture'])) {

                        switch ($type) {

                                case 'pth':
                                        $picture['picture'] = '0x' . bin2hex(file_get_contents('sites/default/files/inline-images/' . $id));
                                        $picture['picture_type'] = mime_content_type('sites/default/files/inline-images/' . $id);
                                        break;

                                case 'up':
                                        $picture = getDefaultUserpic();

                                        if (!array_key_exists('picture', $picture) && array_key_exists('content', $picture)) {
                                                $picture['picture'] = $picture['content'];
                                                $picture['picture_type'] = $picture['type'];
                                        }
                                        break;

                                case 'lic':
                                        $license = new \Drupal\publisso_gold\Controller\License($id);
                                        $picture['picture_type'] = $license->getElement('image_type');
                                        $picture['picture'] = '0x' . bin2hex($license->getImageBin());
                                        break;

                                default:
                                        $picture = getDefaultPic();

                                        if (!array_key_exists('picture', $picture) && array_key_exists('content', $picture)) {
                                                $picture['picture'] = $picture['content'];
                                                $picture['picture_type'] = $picture['type'];
                                        }
                        }
                }

                $file = '/tmp/' . time() . uniqid();
                file_put_contents($file, pack("H*", preg_replace('/^(0x)/', '', $picture['picture'])));
                $response = new BinaryFileResponse($file);
                $response->deleteFileAfterSend(true);
                $response->headers->set('Content-type', $picture['picture_type']);
                return $response;
        }
        
        /**
         * @param $name
         * @return Response
         */
        public function getHTMLSnip($name) {

                $response = new Response();

                $template = new Template();
                $template->get($name);
                $template->setVar('header', (string) t('Sample headline'));
                $template->setVar('body', (string) t('Sample text'));
                $response->setContent($template->parse());
                return $response;
        }
        
        /**
         * @return Response
         */
        public function getMatomoSiteID() {

                $response = new Response();
                $response->setContent(\Drupal::service('publisso_gold.setup')->getValue('matomo.site.id'));
                return $response;
        }
        
        /**
         * @return JsonResponse
         */
        public function getUsedCSS() {
                error_log("Call CSS");
                $doc = new \DOMDocument();
                $url = Url::fromRoute('publisso_gold.content');
                $url->setAbsolute();

                $html = file_get_contents($url->toString());
                $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
                $xpath = new \DOMXPath($doc);

                $src = [];

                foreach ($xpath->evaluate('//link[@rel="stylesheet"]') as $node) {
                        array_push($src, $node->getAttribute('href'));
                }
                error_log("Deliver CSS");
                $response = new JsonResponse();
                $response->setContent(json_encode($src));
                return $response;
        }
        
        /**
         * @param $func
         * @param $val
         * @return mixed
         */
        public function control($func, $val) {
                return $this->$func($val);
        }
        
        /**
         * @param $data
         * @return Response
         */
        private function getEncData($data){
                
                $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
                
                if(null === ($encdata = $tempstore->get('encdata'))){
                        $encdata = serialize([]);
                }
                
                $encdata = unserialize($encdata);
                
                if(array_key_exists($data, $encdata)) $ret = $encdata[$data];
                else $ret = '';
                
                $response = new Response();
                $response->setContent($ret);
                return $response;
        }
        
        /**
         * @param $val
         * @return Response
         */
        private function getFontSize($val) {
                $session = \Drupal::service('session');
                $response = new Response();
                $response->setContent($session->get('FontSize'));
                return $response;
        }
        
        /**
         * @param $val
         * @return Response
         */
        private function setFontSize($val) {
                $session = \Drupal::service('session');
                $session->set('FontSize', $val);
                $response = new Response();
                $response->setContent("");
                return $response;
        }
        
        /**
         * @param $val
         * @return Response
         */
        private function getUserLang($val) {

                $lm = \Drupal::service('language_manager');

                $response = new Response();
                $response->setContent($lm->getCurrentLanguage()->getId());
                return $response;
        }
        
        /**
         * @param $bk_id
         * @return JsonResponse
         */
        public function getBookDetails($bk_id) {

                $book = new \Drupal\publisso_gold\Controller\Book($bk_id, false);
                $keys = [];

                foreach ($book->getKeys() as $key) {
                        $keys[$key] = $book->getElement($key);
                }

                $response = new JsonResponse();
                $response->setContent(json_encode($keys));
                return $response;
        }
        
        /**
         * @param $cp_id
         * @return JsonResponse
         */
        public function getChapterDetails($cp_id) {

                $chapter = new \Drupal\publisso_gold\Controller\Bookchapter($cp_id);
                $keys = [];

                foreach ($chapter->getKeys() as $key) {
                        $keys[$key] = $chapter->getElement($key);
                }

                $response = new JsonResponse();
                $response->setContent(json_encode($keys));
                return $response;
        }
        
        /**
         * @param $id
         * @return Response
         */
        public function getLicenseImageData($id) {

                $license = new License($id);
                $response = new Response();
                $response->setContent($license->getInlineImageData());
                return $response;
        }
        
        /**
         * @return Response
         */
        public function csTokenEndpoint() {

                $response = new Response();

                if (!\Drupal::service('session')->get('logged_in')) {

                        $response->setContent(null);
                        return $response;
                }

                $user = new \Drupal\publisso_gold\Controller\User(\Drupal::service('session')->get('user')['id']);

                $header = [
                        'alg' => 'HS256',
                        'typ' => 'JWT'
                ];

                $payload = [
                        'iss' => 'NQoFK1NLVelFWOBQtQ8A',
                        'iat' => time() + 24 * 3600,
                        'user' => [
                                'id' => $user->getElement('id'),
                                'email' => $user->profile->getElement('email'),
                                'name' => $user->profile->getReadableFullName()
                        ],
                        'services' => [
                                'ckeditor' => [
                                        'permissions' => [
                                                '*' => 'write'
                                        ]
                                ]
                        ]
                ];

                $base64UrlHeader = base64_encode(json_encode($header));
                $base64UrlPayload = base64_encode(json_encode($payload));
                $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'abC123!', true);
                $base64UrlSignature = base64_encode($signature);
                $response->setContent($base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature);
                return $response;
        }
        
        /**
         * @return Response
         */
        public function ppAvailable() {

                $response = new Response();
                $response->setContent(\Drupal::service('publisso_gold.tools')->systemHasPrivacyProtectInformation() ? 1 : 0);
                return $response;
        }
        
        /**
         * @param $val
         * @return mixed
         */
        public function import($val) {
                $func = "import_$val";
                return $this->$func();
        }

        private function import_journals() {
                
                $varPath = DRUPAL_ROOT . '/' . \Drupal::service('module_handler')->getModule('publisso_gold')->getPath() . '/var';
                $importPath = "$varPath/import/journals";
                $articleIDs = [];

                $journalPropertyMap = [
                        'egms.owner.title.full' => 'jrn_title',
                        'egms.owner.title.short' => 'jrn_title_abbr',
                        'egms.owner.association.full' => 'jrn_corporation',
                        'egms.owner.association.short' => 'jrn_corporation_short',
                        'egms.owner.association.url' => 'jrn_corporation_url',
                        'egms.owner.path' => 'path'
                ];

                $articleMetaMap = [
                        'Title' => 'title',
                        'egms.article.authors' => 'authors'
                ];

                $articleMap = [
                ];
                
                $TRANS = [
                        "iexcl" =>"¡",
                        "cent" =>"¢",
                        "pound" =>"£",
                        "curren" =>"¤",
                        "yen" =>"¥",
                        "brvbar" =>"¦",
                        "sect" =>"§",
                        "uml" =>"¨",
                        "copy" =>"©",
                        "ordf" =>"ª",
                        "laquo" =>"«",
                        "not" =>"¬",
                        "reg" =>"®",
                        "macr" =>"¯",
                        "deg" =>"°",
                        "plusmn" =>"±",
                        "sup2" =>"²",
                        "sup3" =>"³",
                        "acute" =>"´",
                        "micro" =>"µ",
                        "para" =>"¶",
                        "middot" =>"·",
                        "cedil" =>"¸",
                        "sup1" =>"¹",
                        "ordm" =>"º",
                        "raquo" =>"»",
                        "frac14" =>"¼",
                        "frac12" =>"½",
                        "frac34" =>"¾",
                        "iquest" =>"¿",
                        "Agrave" =>"À",
                        "Aacute" =>"Á",
                        "Acirc" =>"Â",
                        "Atilde" =>"Ã",
                        "Auml" =>"Ä",
                        "Aring" =>"Å",
                        "AElig" =>"Æ",
                        "Ccedil" =>"Ç",
                        "Egrave" =>"È",
                        "Eacute" =>"É",
                        "Ecirc" =>"Ê",
                        "Euml" =>"Ë",
                        "Igrave" =>"Ì",
                        "Iacute" =>"Í",
                        "Icirc" =>"Î",
                        "Iuml" =>"Ï",
                        "ETH" =>"Ð",
                        "Ntilde" =>"Ñ",
                        "Ograve" =>"Ò",
                        "Oacute" =>"Ó",
                        "Ocirc" =>"Ô",
                        "Otilde" =>"Õ",
                        "Ouml" =>"Ö",
                        "times" =>"×",
                        "Oslash" =>"Ø",
                        "Ugrave" =>"Ù",
                        "Uacute" =>"Ú",
                        "Ucirc" =>"Û",
                        "Uuml" =>"Ü",
                        "Yacute" =>"Ý",
                        "THORN" =>"Þ",
                        "szlig" =>"ß",
                        "agrave" =>"à",
                        "aacute" =>"á",
                        "acirc" =>"â",
                        "atilde" =>"ã",
                        "auml" =>"ä",
                        "aring" =>"å",
                        "aelig" =>"æ",
                        "ccedil" =>"ç",
                        "egrave" =>"è",
                        "eacute" =>"é",
                        "ecirc" =>"ê",
                        "euml" =>"ë",
                        "igrave" =>"ì",
                        "iacute" =>"í",
                        "icirc" =>"î",
                        "iuml" =>"ï",
                        "eth" =>"ð",
                        "ntilde" =>"ñ",
                        "ograve" =>"ò",
                        "oacute" =>"ó",
                        "ocirc" =>"ô",
                        "otilde" =>"õ",
                        "ouml" =>"ö",
                        "divide" =>"÷",
                        "oslash" =>"ø",
                        "ugrave" =>"ù",
                        "uacute" =>"ú",
                        "ucirc" =>"û",
                        "uuml" =>"ü",
                        "yacute" =>"ý",
                        "thorn" =>"þ",
                        "yuml" =>"ÿ",
                        "OElig" =>"Œ",
                        "oelig" =>"œ",
                        "Scaron" =>"Š",
                        "scaron" =>"š",
                        "Yuml" =>"Ÿ",
                        "fnof" =>"ƒ",
                        "circ" =>"ˆ",
                        "tilde" =>"˜",
                        "Alpha" =>"Α",
                        "Beta" =>"Β",
                        "Gamma" =>"Γ",
                        "Delta" =>"Δ",
                        "Epsilon" =>"Ε",
                        "Zeta" =>"Ζ",
                        "Eta" =>"Η",
                        "Theta" =>"Θ",
                        "Iota" =>"Ι",
                        "Kappa" =>"Κ",
                        "Lambda" =>"Λ",
                        "Mu" =>"Μ",
                        "Nu" =>"Ν",
                        "Xi" =>"Ξ",
                        "Omicron" =>"Ο",
                        "Pi" =>"Π",
                        "Rho" =>"Ρ",
                        "Sigma" =>"Σ",
                        "Tau" =>"Τ",
                        "Upsilon" =>"Υ",
                        "Phi" =>"Φ",
                        "Chi" =>"Χ",
                        "Psi" =>"Ψ",
                        "Omega" =>"Ω",
                        "alpha" =>"α",
                        "beta" =>"β",
                        "gamma" =>"γ",
                        "delta" =>"δ",
                        "epsilon" =>"ε",
                        "zeta" =>"ζ",
                        "eta" =>"η",
                        "theta" =>"θ",
                        "iota" =>"ι",
                        "kappa" =>"κ",
                        "lambda" =>"λ",
                        "mu" =>"μ",
                        "nu" =>"ν",
                        "xi" =>"ξ",
                        "omicron" =>"ο",
                        "pi" =>"π",
                        "rho" =>"ρ",
                        "sigmaf" =>"ς",
                        "sigma" =>"σ",
                        "tau" =>"τ",
                        "upsilon" =>"υ",
                        "phi" =>"φ",
                        "chi" =>"χ",
                        "psi" =>"ψ",
                        "omega" =>"ω",
                        "thetasym" =>"ϑ",
                        "upsih" =>"ϒ",
                        "piv" =>"ϖ",
                        "ndash" =>"–",
                        "mdash" =>"—",
                        "horbar" =>"―",
                        "lsquo" =>"‘",
                        "rsquo" =>"’",
                        "sbquo" =>"‚",
                        "ldquo" =>"“",
                        "rdquo" =>"”",
                        "bdquo" =>"„",
                        "dagger" =>"†",
                        "Dagger" =>"‡",
                        "bull" =>"•",
                        "hellip" =>"…",
                        "permil" =>"‰",
                        "prime" =>"′",
                        "Prime" =>"″",
                        "lsaquo" =>"‹",
                        "rsaquo" =>"›",
                        "oline" =>"‾",
                        "frasl" =>"⁄",
                        "euro" =>"€",
                        "image" =>"ℑ",
                        "weierp" =>"℘",
                        "real" =>"ℜ",
                        "trade" =>"™",
                        "alefsym" =>"ℵ",
                        "larr" =>"←",
                        "uarr" =>"↑",
                        "rarr" =>"→",
                        "darr" =>"↓",
                        "harr" =>"↔",
                        "crarr" =>"↵",
                        "lArr" =>"⇐",
                        "uArr" =>"⇑",
                        "rArr" =>"⇒",
                        "dArr" =>"⇓",
                        "hArr" =>"⇔",
                        "forall" =>"∀",
                        "part" =>"∂",
                        "exist" =>"∃",
                        "empty" =>"∅",
                        "nabla" =>"∇",
                        "isin" =>"∈",
                        "notin" =>"∉",
                        "ni" =>"∋",
                        "prod" =>"∏",
                        "sum" =>"∑",
                        "minus" =>"−",
                        "lowast" =>"∗",
                        "radic" =>"√",
                        "prop" =>"∝",
                        "infin" =>"∞",
                        "ang" =>"∠",
                        "and" =>"∧",
                        "or" =>"∨",
                        "cap" =>"∩",
                        "cup" =>"∪",
                        "int" =>"∫",
                        "there4" =>"∴",
                        "sim" =>"∼",
                        "cong" =>"≅",
                        "asymp" =>"≈",
                        "ne" =>"≠",
                        "equiv" =>"≡",
                        "le" =>"≤",
                        "ge" =>"≥",
                        "sub" =>"⊂",
                        "sup" =>"⊃",
                        "nsub" =>"⊄",
                        "sube" =>"⊆",
                        "supe" =>"⊇",
                        "oplus" =>"⊕",
                        "otimes" =>"⊗",
                        "perp" =>"⊥",
                        "sdot" =>"⋅",
                        "lceil" =>"⌈",
                        "rceil" =>"⌉",
                        "lfloor" =>"⌊",
                        "rfloor" =>"⌋",
                        "lang" =>"〈",
                        "rang" =>"〉",
                        "loz" =>"◊",
                        "spades" =>"♠",
                        "clubs" =>"♣",
                        "hearts" =>"♥",
                        "diams" =>"♦",
                        "bdquo" => "„",
                        "ldquo" => "“",
                        "rdquo" => "”"
                ];
                
                $TRANS_PRAE = [
                        "&#8226;" => "•",
                        "&#8230;" => "…",
                        "&#8251;" => "※",
                        "&#8254;" => "‾",
                        "&#8258;" => "⁂",
                        "&#8364;" => "€",
                        "&#8270;" => "⁎",
                        "&#8273;" => "⁑",
                        "&#8482;" => "™",
                        "&#9679;" => "●",
                        "&#9824;" => "♠",
                        "&#9827;" => "♣",
                        "&#9829;" => "♥",
                        "&#9830;" => "♦",
                        "&#8209;" => "‑",
                        "&#8211;" => "–",
                        "&#8212;" => "—",
                        "&#8216;" => "'",
                        "&#8217;" => "'",
                        "&#8218;" => "'",
                        "&#8220;" => "\"",
                        "&#8221;" => "\"",
                        "&#8222;" => "\"",
                        "&#8224;" => "†",
                        "&#8225;" => "‡",
                        "&#8239;" => " ",
                        "&#8240;" => "‰",
                        "&#8249;" => "‹",
                        "&#8250;" => "›",
                        "&#8242;" => "′",
                        "&#8243;" => "″",
                        "&#8260;" => "⁄",
                        "&#8465;" => "ℑ",
                        "&#8472;" => "℘",
                        "&#8476;" => "ℜ",
                        "&#8501;" => "ℵ",
                        "&#8704;" => "∀",
                        "&#8706;" => "∂",
                        "&#8707;" => "∃",
                        "&#8709;" => "∅",
                        "&#8711;" => "∇",
                        "&#8712;" => "∈",
                        "&#8713;" => "∉",
                        "&#8715;" => "∋",
                        "&#8719;" => "∏",
                        "&#8721;" => "∑",
                        "&#8722;" => "−",
                        "&#8727;" => "∗",
                        "&#8730;" => "√",
                        "&#8733;" => "∝",
                        "&#8734;" => "∞",
                        "&#8736;" => "∠",
                        "&#8743;" => "∧",
                        "&#8744;" => "∨",
                        "&#8745;" => "∩",
                        "&#8746;" => "∪",
                        "&#8747;" => "∫",
                        "&#8756;" => "∴",
                        "&#8764;" => "∼",
                        "&#8773;" => "≅",
                        "&#8776;" => "≈",
                        "&#8800;" => "≠",
                        "&#8801;" => "≡",
                        "&#8804;" => "≤",
                        "&#8805;" => "≥",
                        "&#8834;" => "⊂",
                        "&#8835;" => "⊃",
                        "&#8836;" => "⊄",
                        "&#8838;" => "⊆",
                        "&#8839;" => "⊇",
                        "&#8853;" => "⊕",
                        "&#8855;" => "⊗",
                        "&#8869;" => "⊥",
                        "&#8901;" => "⋅",
                        "&#9674;" => "◊",
                        "&#913;" => "Α",
                        "&#914;" => "Β",
                        "&#915;" => "Γ",
                        "&#916;" => "Δ",
                        "&#917;" => "Ε",
                        "&#918;" => "Ζ",
                        "&#919;" => "Η",
                        "&#920;" => "Θ",
                        "&#921;" => "Ι",
                        "&#922;" => "Κ",
                        "&#923;" => "Λ",
                        "&#924;" => "Μ",
                        "&#925;" => "Ν",
                        "&#926;" => "Ξ",
                        "&#927;" => "Ο",
                        "&#928;" => "Π",
                        "&#929;" => "Ρ",
                        "&#931;" => "Σ",
                        "&#932;" => "Τ",
                        "&#933;" => "Υ",
                        "&#934;" => "Φ",
                        "&#935;" => "Χ",
                        "&#936;" => "Ψ",
                        "&#937;" => "Ω",
                        "&#946;" => "β",
                        "&#947;" => "γ",
                        "&#948;" => "δ",
                        "&#949;" => "ε",
                        "&#950;" => "ζ",
                        "&#951;" => "η",
                        "&#952;" => "θ",
                        "&#953;" => "ι",
                        "&#954;" => "κ",
                        "&#955;" => "λ",
                        "&#956;" => "μ",
                        "&#957;" => "ν",
                        "&#958;" => "ξ",
                        "&#959;" => "ο",
                        "&#960;" => "π",
                        "&#961;" => "ρ",
                        "&#962;" => "ς",
                        "&#963;" => "σ",
                        "&#964;" => "τ",
                        "&#965;" => "υ",
                        "&#966;" => "φ",
                        "&#967;" => "χ",
                        "&#968;" => "ψ",
                        "&#969;" => "ω",
                        "&#976;" => "ϐ",
                        "&#977;" => "ϑ",
                        "&#978;" => "ϒ",
                        "&#981;" => "ϕ",
                        "&#982;" => "ϖ",
                        "&#983;" => "ϗ",
                        "&#984;" => "Ϙ",
                        "&#985;" => "ϙ",
                        "&#986;" => "Ϛ",
                        "&#987;" => "ϛ",
                        "&#988;" => "Ϝ",
                        "&#989;" => "ϝ",
                        "&#990;" => "Ϟ",
                        "&#991;" => "ϟ",
                        "&#1008;" => "ϰ",
                        "&#1009;" => "ϱ",
                        "&#1015;" => "Ϸ",
                        "&#1016;" => "ϸ",
                        "&#1018;" => "Ϻ",
                        "&#1019;" => "ϻ",
                        "&#257;" => "ā",
                        "&#275;" => "ē",
                        "&#283;" => "ě",
                        "&#299;" => "ī",
                        "&#333;" => "ō",
                        "&#363;" => "ū",
                        "&#462;" => "ǎ",
                        "&#464;" => "ǐ",
                        "&#466;" => "ǒ",
                        "&#468;" => "ǔ",
                        "&#470;" => "ǖ",
                        "&#472;" => "ǘ",
                        "&#474;" => "ǚ",
                        "&#476;" => "ǜ",
                        "&#161;" => "¡",
                        "&#162;" => "¢",
                        "&#163;" => "£",
                        "&#164;" => "¤",
                        "&#165;" => "¥",
                        "&#166;" => "¦",
                        "&#167;" => "§",
                        "&#168;" => "¨",
                        "&#169;" => "©",
                        "&#170;" => "ª",
                        "&#171;" => "«",
                        "&#172;" => "¬",
                        "&#174;" => "®",
                        "&#175;" => "¯",
                        "&#176;" => "°",
                        "&#177;" => "±",
                        "&#178;" => "²",
                        "&#179;" => "³",
                        "&#180;" => "´",
                        "&#181;" => "µ",
                        "&#182;" => "¶",
                        "&#183;" => "·",
                        "&#184;" => "¸",
                        "&#185;" => "¹",
                        "&#186;" => "º",
                        "&#187;" => "»",
                        "&#188;" => "¼",
                        "&#189;" => "½",
                        "&#190;" => "¾",
                        "&#191;" => "¿",
                        "&#192;" => "À",
                        "&#193;" => "Á",
                        "&#194;" => "Â",
                        "&#195;" => "Ã",
                        "&#196;" => "Ä",
                        "&#197;" => "Å",
                        "&#198;" => "Æ",
                        "&#199;" => "Ç",
                        "&#200;" => "È",
                        "&#201;" => "É",
                        "&#202;" => "Ê",
                        "&#203;" => "Ë",
                        "&#204;" => "Ì",
                        "&#205;" => "Í",
                        "&#206;" => "Î",
                        "&#207;" => "Ï",
                        "&#208;" => "Ð",
                        "&#209;" => "Ñ",
                        "&#210;" => "Ò",
                        "&#211;" => "Ó",
                        "&#212;" => "Ô",
                        "&#213;" => "Õ",
                        "&#214;" => "Ö",
                        "&#215;" => "×",
                        "&#216;" => "Ø",
                        "&#217;" => "Ù",
                        "&#218;" => "Ú",
                        "&#219;" => "Û",
                        "&#220;" => "Ü",
                        "&#221;" => "Ý",
                        "&#222;" => "Þ",
                        "&#223;" => "ß",
                        "&#224;" => "à",
                        "&#225;" => "á",
                        "&#226;" => "â",
                        "&#227;" => "ã",
                        "&#228;" => "ä",
                        "&#229;" => "å",
                        "&#230;" => "æ",
                        "&#231;" => "ç",
                        "&#232;" => "è",
                        "&#233;" => "é",
                        "&#234;" => "ê",
                        "&#235;" => "ë",
                        "&#236;" => "ì",
                        "&#237;" => "í",
                        "&#238;" => "î",
                        "&#239;" => "ï",
                        "&#240;" => "ð",
                        "&#241;" => "ñ",
                        "&#242;" => "ò",
                        "&#243;" => "ó",
                        "&#244;" => "ô",
                        "&#245;" => "õ",
                        "&#246;" => "ö",
                        "&#247;" => "÷",
                        "&#248;" => "ø",
                        "&#249;" => "ù",
                        "&#250;" => "ú",
                        "&#251;" => "û",
                        "&#252;" => "ü",
                        "&#253;" => "ý",
                        "&#254;" => "þ",
                        "&#255;" => "ÿ",
                        "&#338;" => "Œ",
                        "&#339;" => "œ",
                        '&#8216' => '\'',
                        '&#8217' => '\'',
                        '&#8218' => '\'',
                        '&#8219' => '\'',
                        '&#8220;' => '"',
                        '&#8221;' => '"',
                        '&#8222;' => '"',
                        '&#8223' => '"',
                        "&#945;" => "&alpha;",
                        "&#946;" => "β",
                        "&#947;" => "γ",
                        "&#948;" => "δ",
                        "&#949;" => "ε",
                        "&#950;" => "ζ",
                        "&#951;" => "η",
                        "&#952;" => "θ",
                        "&#953;" => "ι",
                        "&#954;" => "κ",
                        "&#955;" => "λ",
                        "&#956;" => "μ",
                        "&#957;" => "ν",
                        "&#958;" => "ξ",
                        "&#959;" => "ο",
                        "&#960;" => "π",
                        "&#961;" => "ρ",
                        "&#963;" => "σ",
                        "&#964;" => "τ",
                        "&#965;" => "υ",
                        "&#966;" => "φ",
                        "&#967;" => "χ",
                        "&#968;" => "ψ",
                        "&#969;" => "ω",
                        "&#913;" => "Α",
                        "&#914;" => "Β",
                        "&#915;" => "Γ",
                        "&#916;" => "Δ",
                        "&#917;" => "Ε",
                        "&#918;" => "Ζ",
                        "&#919;" => "Η",
                        "&#920;" => "Θ",
                        "&#921;" => "Ι",
                        "&#922;" => "Κ",
                        "&#923;" => "Λ",
                        "&#924;" => "Μ",
                        "&#925;" => "Ν",
                        "&#926;" => "Ξ",
                        "&#927;" => "Ο",
                        "&#928;" => "Π",
                        "&#929;" => "Ρ",
                        "&#931;" => "Σ",
                        "&#932;" => "Τ",
                        "&#933;" => "Υ",
                        "&#934;" => "Φ",
                        "&#935;" => "Χ",
                        "&#936;" => "Ψ",
                        "&#937;" => "Ω",
                        "&#169;" => "©",
                        "&#174;" => "®",
                        "&#8471;" => "℗",
                        "&#8482;" => "™",
                        "&#8480;" => "℠",
                        "&#36;" => "$",
                        "&#8364;" => "€",
                        "&#163;" => "£",
                        "&#165;" => "¥",
                        "&#162;" => "¢",
                        "&#8377;" => "₹",
                        "&#8360;" => "₨",
                        "&#8369;" => "₱",
                        "&#8361;" => "₩",
                        "&#3647;" => "฿",
                        "&#8363;" => "₫",
                        "&#8362;" => "₪",
                        "&#38;" => "&",
                        "&#8226;" => "•",
                        "&#9702;" => "◦",
                        "&#8729;" => "∙",
                        "&#8227;" => "‣",
                        "&#8259;" => "⁃",
                        "&#176;" => "°",
                        "&#8734;" => "∞",
                        "Ã¼" => "ü"
                ];
                
                $TRANS = array_flip($TRANS);
                foreach($TRANS as $k => $v) $TRANS[$k] = "&$v;";

                #echo "<pre>";
                #print_r($TRANS);
                #echo "</pre>";
                #exit();
                $articles = (object)[];
                ob_flush();
                foreach (glob("$importPath/*.zip") as $zipArchive) {
                        echo "$zipArchive<br>";
                        $tmpPath = "$varPath/tmp/" . md5(time());

                        $zip = new \ZipArchive;

                        if ($zip->open($zipArchive)) {

                                if (!($zip->extractTo($tmpPath)))
                                        continue;
                                $zip->close();
                                
                                if (!is_file("$tmpPath/manifest.xml")){
                                        echo "No manifest found...";
                                        contine;
                                }

                                $manifestXML = new \DOMDocument("1.0", "ISO-8859-1");
                                //exec('recode "latin1..utf8" '."$tmpPath/manifest.xml");
                                file_put_contents("$tmpPath/manifest.xml", str_replace(array_keys($TRANS), array_values($TRANS), str_replace(array_keys($TRANS_PRAE), array_values($TRANS_PRAE), file_get_contents("$tmpPath/manifest.xml"))));
                                exec('recode "utf8..latin1" '."$tmpPath/manifest.xml");
                                $manifestXML->load("$tmpPath/manifest.xml");
                                $manifestXPATH = new \DOMXPath($manifestXML);


//                              $manifestXML->formatOutput = true;
//                              echo $manifestXML->saveXML();

                                $defaultLanguage = null;
                                $res = $manifestXPATH->evaluate("/export/files/file[./type = 'folder' and ./properties/property[./name = 'Title' and ./value = 'GMS | german medical science']]/destination")[0];
                                if ($res)
                                        $defaultLanguage = $res->nodeValue;
                                else
                                        continue;

                                $containerFolder = "$defaultLanguage/journals/";

                                $journalNode = $manifestXPATH->evaluate("/export/files/file/destination[starts-with(text(), '$containerFolder')]")[0];
                                if (!$journalNode)
                                        continue;

                                $journal = (object) [];

                                if (($node = $manifestXPATH->evaluate("../datecreated", $journalNode)[0]) === null)
                                        continue;
                                $journal->created = date("Y-m-d H:i:s", strtotime($node->nodeValue));

                                foreach ($manifestXPATH->evaluate("../properties/property", $journalNode) as $node) {

                                        if (!($manifestXPATH->evaluate("./name", $node)[0] && $manifestXPATH->evaluate("./value", $node)[0]))
                                                continue;
                                        $name = $manifestXPATH->evaluate("./name", $node)[0]->nodeValue;
                                        $value = $manifestXPATH->evaluate("./value", $node)[0]->nodeValue;

                                        if (array_key_exists($name, $journalPropertyMap))
                                                $journal->{$journalPropertyMap[$name]} = $value;

                                        if ($name == 'egms.owner.title.addon' && preg_match('/^ISSN\s*(.+)$/', $value, $matches)) {
                                                $journal->jrn_issn = $matches[1];
                                        }
                                }
                                echo "Load logo<br>";
                                ob_flush();
                                if($journal->path){
                                        
                                        //try to load the logo

                                        //png
                                        $content = file_get_contents('https://www.egms.de/static/resources/journals/'.$journal->path.'/logo_'.$journal->path.'.png');
                                        
                                        if($content){
                                                if((substr($content, 0, 1) != '<')){
                                                        $journal->logo = $content;
                                                        $journal->logo_mime = 'image/png';
                                                }
                                        }
                                        
                                        if(!$journal->logo){
                                                //jpg
                                                $content = file_get_contents('https://www.egms.de/static/resources/journals/'.$journal->path.'/logo_'.$journal->path.'.jpg');

                                                if($content){
                                                        if((substr($content, 0, 1) != '<')){
                                                                $journal->logo = $content;
                                                                $journal->logo_mime = 'image/jpg';
                                                        }
                                                }
                                        }
                                        
                                        if(!$journal->logo){
                                                //gif
                                                $content = file_get_contents('https://www.egms.de/static/resources/journals/'.$journal->path.'/logo_'.$journal->path.'.gif');

                                                if($content){
                                                        if((substr($content, 0, 1) != '<')){
                                                                $journal->logo = $content;
                                                                $journal->logo_mime = 'image/gif';
                                                        }
                                                }
                                        }
                                }
                                
                                $ary = ['about' => 'about', 'imprint' => 'imprint', 'contact' => 'contact', 'authors' => 'manuscript_guidelines'];
                                
                                foreach ($ary as $_) {
                                        
                                        if ( $content = file_get_contents( 'https://www.egms.de/static/en/journals/' . $journal->path . '/'.$_.'.htm' ) ) {
                
                                                $_doc = new \DOMDocument();
                                                $_doc->loadHTML( $content );
                                                $_xpt = new \DOMXpath( $_doc );
                                                $_nl = $_xpt->evaluate( '//div[@class="einespalte"]' );
                                                $_doc->formatOutput = true;
                
                                                if ( $_nl->length ) {
                                                        $_node = $_nl->item(0);
                                                        $_h3 = $_xpt->evaluate('h3', $_node);
                                                        if($_h3->length){
                                                                $_h3 = $_h3->item(0);
                                                                $_node->removeChild($_h3);
                                                        }
                                                        $journal->$_ = $_nl->item( 0 )->C14N();
                                                        $journal->$_ = preg_replace( '/^\<div class="einespalte"\>|\<\/div\>$/', '', $journal->$_ );
                                                        $journal->$_ = str_replace( [ '</br>', '<br>' ], [ '', '<br />' ], $journal->$_ );
                                                        $journal->$_ = trim($journal->$_);
                                                        $journal->$_ = preg_replace('/\s{2,}/', ' ', $journal->$_);
                                                        
                                                        if($_ == 'contact'){
                                                                #echo $journal->$_; exit;
                                                        }
                                                }
                                        }
                                }
                                
                                //try to get welcome-message
                                if ( $content = file_get_contents( 'https://www.egms.de/dynamic/en/journals/'.$journal->path.'/index.htm' ) ) {
        
                                        $_doc = new \DOMDocument();
                                        $_doc->loadHTML( $content );
                                        $_xpt = new \DOMXPath( $_doc );
                                        $_nl = $_xpt->evaluate( '//div[@id="content"]' );
        
                                        if ( $_nl->length ) {
                                                
                                                $_cntNode = $_nl->item(0);
                                                $_h3found = false;
                                                $_welcome = '';
                
                                                foreach ( $_xpt->evaluate( '*', $_cntNode ) as $_node){
                                                        
                                                        if($_node->nodeName == 'h3' && $_h3found) break;
                                                        
                                                        if($_node->nodeName == 'h3' && $_node->nodeValue == 'Welcome'){
                                                                $_h3found = true;
                                                        }
                                                        
                                                        if($_node->nodeName != 'h3' && !($_node->nodeName == 'div' && $_node->hasAttribute('id'))){
                                                                $_welcome .= $_node->C14N();
                                                        }
                                                }
                                        }
                                        
                                        if(!empty($_welcome)) $journal->welcome = html_entity_decode($_welcome);
                                }
                                
                                //search the Journal - if not exists, create one
                                $jrn_id = \Drupal::database()->select('rwPubgoldJournals', 't')->fields('t', ['jrn_id'])->condition('jrn_title', $journal->jrn_title, '=')->execute()->fetchField();
                                if(!$jrn_id) $jrn_id = \Drupal::database()->insert('rwPubgoldJournals')->fields(['jrn_title' => $journal->jrn_title])->execute();
                                
                                $objJournal = new Journal($jrn_id);
                                
                                if($journal->logo){
                                        if(isset($file)) unset($file);
                                        $name = md5($journal->logo).'.'.explode("/", $journal->logo_mime)[1];
                                        $file = file_save_data($journal->logo, "public://$name", FILE_EXISTS_RENAME);
                                        $file->setPermanent();
                                        $file->save;
                                        $objJournal->setElement('logo', [$file->id()]);
                                }
                                
                                //update jopurnal-data
                                if(empty($objJournal->getElement('created'))) $objJournal->setElement('created', $journal->created ? $journal->created : null);
                                if(empty($objJournal->getElement('corporation')))$objJournal->setElement('corporation', $journal->jrn_corporation ? $journal->jrn_corporation : null);
                                if(empty($objJournal->getElement('corporation_short'))) $objJournal->setElement('corporation_short', $journal->jrn_corporation_short ? $journal->jrn_corporation_short : null);
                                if(empty($objJournal->getElement('corporation_url'))) $objJournal->setElement('corporation_url', $journal->jrn_corporation_url ? $journal->jrn_corporation_url : null);
                                if(empty($objJournal->getElement('issn'))) $objJournal->setElement('issn', $journal->jrn_issn ? $journal->jrn_issn : null);
                                if(empty($objJournal->getElement('title_abbr'))) $objJournal->setElement('title_abbr', $journal->jrn_title_abbr ? $journal->jrn_title_abbr : null);
                                if(empty($objJournal->getElement('title_nlm'))) $objJournal->setElement('title_nlm', $journal->jrn_title_abbr ? $journal->jrn_title_abbr : null);
                                if(empty($objJournal->getElement('imprint'))) $objJournal->setElement('imprint', $journal->imprint ?? null);
                                if(empty($objJournal->getElement('about'))) $objJournal->setElement('about', $journal->about ?? null);
                                if(empty($objJournal->getElement('contact'))) $objJournal->setElement('contact', $journal->contact ?? null);
                                if(empty($objJournal->getElement('welcome'))) $objJournal->setElement('welcome', $journal->welcome ?? null);
                                if(empty($objJournal->getElement('manuscript_guidelines'))) $objJournal->setElement('manuscript_guidelines', $journal->manuscript_guidelines ?? null);
                               
                                $journal->jrn_id = $objJournal->getElement('id');
                                ob_flush();
                                foreach ($manifestXPATH->evaluate("/export/files/file[./type = 'egms-gmsarticle']") as $articleMetaNode) {
                                        
                                        echo "Start Article...<br>";
                                        if(isset($articleMeta)) unset($articleMeta);
                                        
                                        $articleMeta = (object) [];
                                        $articleData = (object) [];
                                        
                                        if (null == ($manifestXPATH->evaluate('./source', $articleMetaNode)[0])) {
                                                echo "Can't find Source! Skipping!<br>";
                                                continue;
                                        }
                                        echo "Treat source-file ".$manifestXPATH->evaluate('./source', $articleMetaNode)[0]->nodeValue."<br>";
                                        if (null == ($manifestXPATH->evaluate('./datecreated', $articleMetaNode)[0])) {
                                                echo "Can't find creation-date! Skipping<br>";
                                                continue;
                                        }
                                
                                        $ownerPath = $manifestXPATH->evaluate("/export/files/file/properties/property/name[text() = 'egms.owner.path']/value")[0]->nodeValue;
                                        
                                        $articleMeta->source = $manifestXPATH->evaluate('./source', $articleMetaNode)[0]->nodeValue;
                                        echo "Source: ".$manifestXPATH->evaluate('./source', $articleMetaNode)[0]->nodeValue.'<br>';
                                        ob_flush();
                                        $articleMeta->created = date('Y-m-d H:i:s', strtotime($manifestXPATH->evaluate('./datecreated', $articleMetaNode)[0]->nodeValue));
                                        
                                        if (!is_file($tmpPath . '/' . $articleMeta->source) && !is_readable($tmpPath . '/' . $articleMeta->source)) {
                                                echo "File ".$articleMeta->source ." doesn't exists or is not readable! Skipping!<br>";
                                                continue;
                                        }

                                        foreach ($manifestXPATH->evaluate("./properties/property", $articleMetaNode) as $node) {
                                                if (!($manifestXPATH->evaluate("./name", $node)[0] && $manifestXPATH->evaluate("./value", $node)[0]))
                                                        continue;
                                                $name = $manifestXPATH->evaluate("./name", $node)[0]->nodeValue;
                                                $value = $manifestXPATH->evaluate("./value", $node)[0]->nodeValue;

                                                if (array_key_exists($name, $articleMetaMap))
                                                        $articleMeta->{$articleMetaMap[$name]} = $value;
                                        }
                                        
                                        $articleSourceXML = new \DOMDocument("1.0", "ISO-8859-1");
                                        //exec('recode "latin1..utf8" '.$tmpPath . '/' . $articleMeta->source);
                                        file_put_contents($tmpPath . '/' . $articleMeta->source, str_replace(array_keys($TRANS), array_values($TRANS), str_replace(array_keys($TRANS_PRAE), array_values($TRANS_PRAE), file_get_contents($tmpPath . '/' . $articleMeta->source))));
                                        exec('recode "utf8..latin1" '.$tmpPath . '/' . $articleMeta->source);
                                        $articleSourceXML->load($tmpPath . '/' . $articleMeta->source);
                                        $articleSourceXPATH = new \DOMXpath($articleSourceXML);
                                        ob_flush();
                                        foreach($articleSourceXPATH->evaluate("/GmsArticles/GmsArticle") as $articleNode){
                                                
                                                if(isset($article)) unset($article);
                                                $article = (object) [];
                                                
                                                $imagesUsed = [];
                                                
                                                $article->meta = $articleMeta;
                                                $article->language = $articleNode->getAttribute('language');
                                                $article->identifier = $articleSourceXPATH->evaluate('Identifier', $articleNode)[0]->nodeValue;
                                                
                                                if($article->identifier != '000267') continue;
                                                
                                                $article->title = $articleSourceXPATH->evaluate('Title', $articleNode)[0]->nodeValue;
                                                echo $article->title.'<br>';
                                                ob_flush();
                                                if ($articleSourceXPATH->evaluate("ArticleHasTranslation", $articleNode)[0])
                                                        $article->has_translation = $articleSourceXPATH->evaluate("ArticleHasTranslation", $articleNode)[0]->nodeValue == 'true' ? 1 : 0;

                                                if ($articleSourceXPATH->evaluate("ArticleType", $articleNode)[0])
                                                        $article->type = $articleSourceXPATH->evaluate("ArticleType", $articleNode)[0]->nodeValue;
                                                
                                                if ($articleSourceXPATH->evaluate("ArticleOriginalLanguage", $articleNode)[0])
                                                        $article->language_orig = $articleSourceXPATH->evaluate("ArticleOriginalLanguage", $articleNode)[0]->nodeValue;

                                                if ($articleSourceXPATH->evaluate("License/xlinkHref", $articleNode)[0]) {
                                                        $url = $articleSourceXPATH->evaluate("License/xlinkHref", $articleNode)[0]->nodeValue;
                                                        $url = preg_replace('/^http:\/\//', 'https://', $url);
                                                }
                                                else{ // Fallback, falls keine Lizenz angegeben wurde. Siehe Mail von
                                                        // Herrn Krämer vom 22.01.2019
                                                        $url = 'https://creativecommons.org/licenses/by-nc-nd/3.0/deed.en';
                                                }
        
                                                $license = new License();
                                                $license->loadByUrl($url);
                                                $article->license = $license->getId();

                                                if ($articleSourceXPATH->evaluate("PublicationYear", $articleNode)[0])
                                                        $article->publication_year = $articleSourceXPATH->evaluate("PublicationYear", $articleNode)[0]->nodeValue;

                                                if ($articleSourceXPATH->evaluate("IdentifierDoi", $articleNode)[0])
                                                        $article->doi = $articleSourceXPATH->evaluate("IdentifierDoi", $articleNode)[0]->nodeValue;

                                                if ($articleSourceXPATH->evaluate("IdentifierUrn", $articleNode)[0])
                                                        $article->urn = $articleSourceXPATH->evaluate("IdentifierUrn", $articleNode)[0]->nodeValue;


                                                if ($articleSourceXPATH->evaluate("ArticleNo", $articleNode)[0])
                                                        $article->doc_no = $articleSourceXPATH->evaluate("ArticleNo", $articleNode)[0]->nodeValue;

                                                if ($articleSourceXPATH->evaluate("Publisher", $articleNode)[0]) {
                                                        $node = $articleSourceXPATH->evaluate("Publisher", $articleNode)[0];
                                                        $publisher = $publication_place = null;

                                                        if ($articleSourceXPATH->evaluate('Corporatename', $node)[0])
                                                                $publisher = $articleSourceXPATH->evaluate('Corporatename', $node)[0]->nodeValue;
                                                        if ($articleSourceXPATH->evaluate('Address', $node)[0])
                                                                $publication_place = $articleSourceXPATH->evaluate('Address', $node)[0]->nodeValue;

                                                        $article->publisher = $publisher;
                                                        $article->publication_place = $publication_place;
                                                }

                                                $article->keywords = [];
                                                foreach ($articleSourceXPATH->evaluate("Keyword", $articleNode) as $node) {
                                                        array_push($article->keywords, $node->nodeValue);
                                                }

                                                if ($articleSourceXPATH->evaluate("SubjectheadingDDB", $articleNode)[0]){
                                                        $article->ddc = $articleSourceXPATH->evaluate("SubjectheadingDDB", $articleNode)[0]->nodeValue;
                                                }
                                                
                                                if ($articleSourceXPATH->evaluate("SectionHeading", $articleNode)[0]){
                                                        $article->sub_category = $articleSourceXPATH->evaluate("SectionHeading", $articleNode)[0]->nodeValue;
                                                }
                                                
                                                if($articleSourceXPATH->evaluate("DateReceived", $articleNode)[0]){
                                                        $article->received = $articleSourceXPATH->evaluate("DateReceived", $articleNode)[0]->nodeValue;
                                                        $article->received = substr($article->received, 0, 4).'-'.substr($article->received, 4, 2).'-'.substr($article->received, 6, 2);
                                                }

                                                if($articleSourceXPATH->evaluate("DateRevised", $articleNode)[0]){
                                                        $article->revised = $articleSourceXPATH->evaluate("DateRevised", $articleNode)[0]->nodeValue;
                                                        $article->revised = substr($article->revised, 0, 4).'-'.substr($article->revised, 4, 2).'-'.substr($article->revised, 6, 2);
                                                }
        
                                                if($articleSourceXPATH->evaluate("DateAccepted", $articleNode)[0]){
                                                        $article->accepted = $articleSourceXPATH->evaluate("DateAccepted", $articleNode)[0]->nodeValue;
                                                        $article->accepted = substr($article->accepted, 0, 4).'-'.substr($article->accepted, 4, 2).'-'.substr($article->accepted, 6, 2);
                                                }
                                                
                                                if($articleSourceXPATH->evaluate("DatePublished", $articleNode)[0]){
                                                        $article->published = $articleSourceXPATH->evaluate("DatePublished", $articleNode)[0]->nodeValue;
                                                        $article->published = substr($article->published, 0, 4).'-'.substr($article->published, 4, 2).'-'.substr($article->published, 6, 2);
                                                        $article->publication_year = substr($article->published, 0, 4);
                                                }
                                                
                                                $_nl = $articleSourceXPATH->evaluate("DateReceived", $articleNode);
                                                if($_nl->length){
                                                        $article->received = $_nl->item(0)->nodeValue;
                                                        $article->received = substr($article->received, 0, 4).'-'.substr($article->received, 4, 2).'-'.substr($article->received, 6, 2);
                                                }
        
                                                $_nl = $articleSourceXPATH->evaluate("DateRevised", $articleNode);
                                                if($_nl->length){
                                                        $article->revised = $_nl->item(0)->nodeValue;
                                                        $article->revised = substr($article->revised, 0, 4).'-'.substr($article->revised, 4, 2).'-'.substr($article->revised, 6, 2);
                                                }
                                                
                                                if($articleSourceXPATH->evaluate("JournalVolume", $articleNode)[0])
                                                        $article->volume = $articleSourceXPATH->evaluate("JournalVolume", $articleNode)[0]->nodeValue;

                                                if($articleSourceXPATH->evaluate("JournalIssue", $articleNode)[0])
                                                        $article->issue = $articleSourceXPATH->evaluate("JournalIssue", $articleNode)[0]->nodeValue;
        
                                                if($articleSourceXPATH->evaluate("JournalIssueTitle", $articleNode)[0])
                                                        $article->issue_title = $articleSourceXPATH->evaluate("JournalIssueTitle", $articleNode)[0]->nodeValue;
                                                
                                                $article->folder = implode('-', array_filter([$article->publication_year, $article->volume, $article->issue]));
                                                
                                                $article->authors = [];

                                                $weight = 0;
                                                foreach ($articleSourceXPATH->evaluate("Creator", $articleNode) as $node) {

                                                        $firstname = $lastname = $affiliation = $address = $initials = $acad_title = null;

                                                        if ($articleSourceXPATH->evaluate("Lastname", $node)[0])
                                                                $lastname = $articleSourceXPATH->evaluate("Lastname", $node)[0]->nodeValue;
                                                        if ($articleSourceXPATH->evaluate("Firstname", $node)[0])
                                                                $firstname = $articleSourceXPATH->evaluate("Firstname", $node)[0]->nodeValue;
                                                        if ($articleSourceXPATH->evaluate("AddressAffiliation", $node)[0])
                                                                $affiliation = $articleSourceXPATH->evaluate("AddressAffiliation", $node)[0]->nodeValue;
                                                        if ($articleSourceXPATH->evaluate("Email", $node)[0])
                                                                $email = $articleSourceXPATH->evaluate("Email", $node)[0]->nodeValue;
                                                        if ($articleSourceXPATH->evaluate("Address", $node)[0])
                                                                $address = $articleSourceXPATH->evaluate("Address", $node)[0]->nodeValue;
                                                        if ($articleSourceXPATH->evaluate("AcademicTitle", $node)[0])
                                                                $acad_title = $articleSourceXPATH->evaluate("AcademicTitle", $node)[0]->nodeValue;
                                                        if ($articleSourceXPATH->evaluate("Initials", $node)[0])
                                                                $initials = $articleSourceXPATH->evaluate("Initials", $node)[0]->nodeValue;
                                                        
                                                        if (!($firstname && $lastname && $affiliation))
                                                                continue;

                                                        $ary = [
                                                                'firstname' => $firstname,
                                                                'lastname' => $lastname,
                                                                'affiliation' => $affiliation,
                                                                'email' => $email,
                                                                'weight' => ++$weight,
                                                                'graduation' => $acad_title,
                                                                'address' => $address
                                                        ];

                                                        if ($articleSourceXPATH->evaluate('CreatorroleCorresponding', $node)[0]) {
                                                                $articleSourceXPATH->evaluate('CreatorroleCorresponding', $node)[0]->nodeValue == 'true' ? $ary['is_corresponding'] = 1 : $ary['is_corresponding'] = 0;
                                                        }

                                                        if ($articleSourceXPATH->evaluate('CreatorrolePresenting', $node)[0]) {
                                                                $articleSourceXPATH->evaluate('CreatorrolePresenting', $node)[0]->nodeValue == 'true' ? $ary['is_presenting'] = 1 : $ary['is_presenting'] = 0;
                                                        }

                                                        array_push($article->authors, $ary);
                                                }
                                                
                                                /*
                                                 * search the pdf-file
                                                 */
                                                $filePattern = $article->identifier.'.pdf';
                                                $iti = new \RecursiveDirectoryIterator($tmpPath);
                                                foreach(new \RecursiveIteratorIterator($iti) as $file){
                                                     if(strpos($file , $filePattern) !== false){
                                                             break;
                                                     }
                                                }

                                                if($file){
                                                        $content = file_get_contents($file->getPath().'/'.$filePattern);
                                                }

                                                if($content){

                                                        $blob = new Blob();
                                                        $article->jrna_pdf_blob_id = $blob->create($content, 'application/pdf', ['name' => $article->identifier.'.pdf']);
                                                }
        
                                                /*
                                                 * search the xml-file
                                                 */
                                                $filePattern = $article->identifier.'.xml';
                                                $iti = new \RecursiveDirectoryIterator($tmpPath);
                                                foreach(new \RecursiveIteratorIterator($iti) as $file){
                                                        if(strpos($file , $filePattern) !== false){
                                                                break;
                                                        }
                                                }
        
                                                if($file){
                                                        $content = file_get_contents($file->getPath().'/'.$filePattern);
                                                }
        
                                                if($content){
                
                                                        $blob = new Blob();
                                                        $article->jrna_xml_blob_id = $blob->create($content, 'text/xml', ['name' => $article->identifier.'.xml']);
                                                }
                                                
                                                $article->jrna_files = [];
                                                
                                                /**
                                                 * catch & store the references
                                                 */
                                                foreach($articleSourceXPATH->evaluate("//Reference", $articleNode) as $referenceNode){
                                                        ob_flush();
                                                        if(!property_exists($article, 'references')) $article->references = [];
                                                        
                                                        if($articleSourceXPATH->evaluate("RefNo", $referenceNode)[0]){
                                                                $refNo = $articleSourceXPATH->evaluate("RefNo", $referenceNode)[0]->nodeValue;
                                                                $article->references[$refNo] = [];
                                                        }
                                                        else continue;
                                                        
                                                        foreach($articleSourceXPATH->evaluate("*", $referenceNode) as $referenceItemNode){
                                                                
                                                                $name = strtolower(preg_replace('/^Ref/', '', $referenceItemNode->nodeName));
                                                                
                                                                if(array_key_exists($name, $article->references[$refNo]) && is_string($article->references[$refNo][$name])){
                                                                        
                                                                        $value = $article->references[$refNo][$name];
                                                                        $article->references[$refNo][$name] = [$value];
                                                                        array_push($article->references[$refNo][$name], $referenceItemNode->nodeValue);
                                                                }
                                                                else{
                                                                        $article->references[$refNo][$name] = $referenceItemNode->nodeValue;
                                                                }
                                                        }
                                                        
                                                        if(array_key_exists('link', $article->references[$refNo]) && preg_match('/^http(s{0,1}):\/\/.+?\.doi.org\//', $article->references[$refNo]['link'])){
                                                                $article->references[$refNo]['doi'] = preg_replace('/^http(s{0,1}):\/\/.+?\.doi.org\//', '', $article->references[$refNo]['link']);
                                                        }
                                                        
                                                        if(array_key_exists('page', $article->references[$refNo])){
                                                                
                                                                $page = $article->references[$refNo]['page'];
                                                                
                                                                if(preg_match('/^(\d+)-{0,1}(\d+){0,1}/', $page, $matches)){
                                                                        
                                                                        if(count($matches) == 2){
                                                                                $article->references[$refNo]['fpage'] = $article->references[$refNo]['lpage'] = $page;
                                                                        }
                                                                        
                                                                        if(count($matches) == 3){
                                                                                $fpage = $matches[1];
                                                                                $lpage = $matches[2];
                                                                                
                                                                                if($lpage < $fpage){
                                                                                        
                                                                                        $strlen = strlen($lpage) * (-1);
                                                                                        $lpage = substr_replace($fpage, $lpage, $strlen);
                                                                                }
                                                                                
                                                                                $article->references[$refNo]['fpage'] = $fpage;
                                                                                $article->references[$refNo]['lpage'] = $lpage;
                                                                        }
                                                                }
                                                        }
                                                        
                                                        $article->references[$refNo]['type'] = null;
                                                        
                                                        $nodeList = $articleSourceXPATH->evaluate("RefJournal", $referenceNode);
                                                        if($nodeList->length > 0) $article->references[$refNo]['type'] = 'journal';
                                                        
                                                        $nodeList = $articleSourceXPATH->evaluate("RefBookTitle", $referenceNode);
                                                        if($nodeList->length > 0) $article->references[$refNo]['type'] = 'book';
                                                        
                                                        $nodeList = $articleSourceXPATH->evaluate("RefLink", $referenceNode);
                                                        if($nodeList->length > 0) $article->references[$refNo]['link'] = $nodeList->item(0)->nodeValue;
                                                        
                                                        /*
                                                         * create new reference-object
                                                         */
                                                        if(!empty($article->references[$refNo]['total'])){
                                                                
                                                                $reference = new Reference();
                                                                $article->references[$refNo]['rwPubrefID'] = $reference->create($article->references[$refNo]['total']);
                                                                
                                                                if($article->references[$refNo]['rwPubrefID']){
                                                                        
                                                                        /*
                                                                         * if reference-object exists, assign the authors
                                                                         */
                                                                        if(array_key_exists('author', $article->references[$refNo])){
                                                                                
                                                                                $authors = [];
                                                                                if(is_string($article->references[$refNo]['author']))
                                                                                        array_push($authors, $article->references[$refNo]['author']);
                                                                                
                                                                                if(is_array($article->references[$refNo]['author']))
                                                                                        $authors = $article->references[$refNo]['author'];
                                                                                
                                                                                foreach($authors as $author){
                                                                                        $author = explode(' ', $author, 2);
                                                                                        if(!($reference->addAuthor(null, $author[0], $author[1]))){
                                                                                                error_log('Failed to add RefAuthor for Article #'.$article->identifier.' (RefID #'.$reference->getId().')');
                                                                                        }
                                                                                }
                                                                        }
                                                                        
                                                                        /*
                                                                         * add other items to reference
                                                                         */
                                                                        $refItemMap = [
                                                                                'title'         => 'title',
                                                                                'type'          => 'type',
                                                                                'year'          => 'year',
                                                                                'journal'       => 'parent_title',
                                                                                'booktitle'     => 'parent_title',
                                                                                'page'          => 'pages',
                                                                                'fpage'         => 'fpage',
                                                                                'lpage'         => 'lpage',
                                                                                'doi'           => 'doi',
                                                                                'link'          => 'link'
                                                                        ];
                                                                        
                                                                        foreach($refItemMap as $k => $v){
                                                                                
                                                                                if(array_key_exists($k, $article->references[$refNo]) && !empty($article->references[$refNo][$k])){
                                                                                        $reference->setItem($v, $article->references[$refNo][$k]);
                                                                                }
                                                                        }
                                                                }
                                                        }
                                                }
                                                
                                                foreach(['Abstract', 'TextBlock'] as $section){
                                                        
                                                        ob_flush();
                                                        $objIdentifier = strtolower($section);
                                                        $article->$objIdentifier = '';
                                                        $linkList = [];
                                                        $usedImages = [];
                                                        
                                                        foreach($articleSourceXPATH->evaluate("$section", $articleNode) as $textBlockNode){
                                                                
                                                                flush();
                                                                //echo $textBlockNode->C14N();
                                                                if(($mainHeadlineNode = $articleSourceXPATH->evaluate("MainHeadline", $textBlockNode)[0]) !== null){
                                                                        $article->$objIdentifier .= '<h2>'.$mainHeadlineNode->nodeValue.'</h2>';
                                                                }

                                                                foreach($articleSourceXPATH->evaluate("Content", $textBlockNode) as $textBlockContentNode){
                                                                        
                                                                        foreach($articleSourceXPATH->evaluate("*", $textBlockContentNode) as $textBlockContentChildNode){
                                                                        #foreach($textBlockContentNode->childNodes as $textBlockContentChildNode){
                                                                                
                                                                                ob_flush();
                                                                                
                                                                                if(substr($textBlockContentChildNode->nodeName, 0, 11) == 'SubHeadline'){
                                                                                        $headlineLevel = substr($textBlockContentChildNode->nodeName, 11);
                                                                                        if(!(int)$headlineLevel) $headlineLevel = 3;
                                                                                        else $headlineLevel += 2;
                                                                                        
                                                                                        $article->$objIdentifier .= "<h$headlineLevel>".$textBlockContentChildNode->nodeValue."</h$headlineLevel>";
                                                                                }

                                                                                if($textBlockContentChildNode->nodeName == 'Pgraph'){
                                                                                        
                                                                                        $linkNodeList = $articleSourceXPATH->evaluate("links/link", $textBlockContentChildNode);

#                                                                                        $linkList = [];
                                                                                        foreach($linkNodeList as $_){
                                                                                                $linkList[$_->getAttribute('name')] = [];
                                                                                                foreach($_->childNodes as $__) $linkList[$_->getAttribute('name')][$__->nodeName] = $__->nodeValue;
                                                                                        }

                                                                                        if(($pgraphContentNode = $articleSourceXPATH->evaluate("content", $textBlockContentChildNode)[0]) === null) continue;
                                                                                        $pgraphContent = $pgraphContentNode->nodeValue;

                                                                                        $pgraphContentXML = new \DOMDocument();
                                                                                        $pgraphContentXML->loadHTML($pgraphContent);
                                                                                        $pgraphContentXPATH = new \DOMXpath($pgraphContentXML);
        
                                                                                        //treat inlineImages (esp. Equations)
                                                                                        $inlineImagesNL = $pgraphContentXPATH->evaluate('//img[@class="inlineFigure"]');
                                                                                        
                                                                                        if($inlineImagesNL->length){
                                                                                                foreach($inlineImagesNL as $inlineImage){
                                                                                                        
                                                                                                        $src = $inlineImage->getAttribute('src');
                                                                                                        $src = str_replace(['%', '(', ')'], '', $src);
                                                                                                        $path = $tmpPath.str_replace('/sites/egms', '', $linkList[$src]['target']);
                                                                                                        $mime = mime_content_type($path);
                                                                                                        
                                                                                                        $inlineImage->setAttribute('src', "data:$mime;base64,".base64_encode(file_get_contents($path)));
                                                                                                }
                                                                                        }
                                                                                        ob_flush();
                                                                                        $replaceLinks = [
                                                                                                'oldLinks' => [],
                                                                                                'newLinks' => [],
                                                                                        ];

                                                                                        $imgList = [];
                                                                                        $images = [];

                                                                                        foreach($pgraphContentXPATH->evaluate('//a[@class]') as $aNode){
                                                                                                
                                                                                                flush();
                                                                                                $linkName = str_replace(['%', '(', ')'], '', $aNode->getAttribute('href'));
                                                                                                echo $linkName."<br>";
                                                                                                if(isset($type)) unset($type);
                                                                                                echo $aNode->getAttribute('class').'<br>';
                                                                                                switch($aNode->getAttribute('class')){

                                                                                                        case 'link-ext':

                                                                                                                $target = $linkList[$linkName]['target'];

                                                                                                                if(array_key_exists('anchor', $linkList[$linkName]) && !empty($linkList[$linkName]['anchor'])){
                                                                                                                        $target .= '#'.$linkList[$linkName]['anchor'];
                                                                                                                }
                                                                                                                $text = str_replace('Â', '', $aNode->nodeValue);
                                                                                                                $newANode = $pgraphContentXML->createElement('a', $text);
                                                                                                                $newANode->setAttribute('target', '_blank');
                                                                                                                $newANode->setAttribute('href', $target);

                                                                                                                $replaceLinks['oldLinks'][] = $aNode;
                                                                                                                $replaceLinks['newLinks'][] = $newANode;
                                                                                                                break;

                                                                                                        case 'link-intRef':

                                                                                                                $reference = $article->references[substr($linkList[$linkName]['anchor'], 3)];

                                                                                                                if($reference['rwPubrefID']){

                                                                                                                        $anchor  = "#rwPubRef~".$reference['rwPubrefID'];
                                                                                                                        $text = str_replace(['[', ']'], '', str_replace('Â', '', $aNode->nodeValue));

                                                                                                                        $fragment = $pgraphContentXML->createDocumentFragment();

                                                                                                                        $fragment->appendChild($pgraphContentXML->createTextNode('['));
                                                                                                                        $newANode = $pgraphContentXML->createElement('a', $text);
                                                                                                                        $newANode->setAttribute('href', $anchor);
                                                                                                                        $fragment->appendChild($newANode);
                                                                                                                        $fragment->appendChild($pgraphContentXML->createTextNode(']'));

                                                                                                                        $replaceLinks['oldLinks'][] = $aNode;
                                                                                                                        $replaceLinks['newLinks'][] = $fragment;
                                                                                                                }
                                                                                                                break;

                                                                                                        case 'link-intAtt':

                                                                                                                if(!in_array('anchor', $linkList[$linkName])){

                                                                                                                        $id = substr($linkList[$linkName]['anchor'], 3);
                                                                                                                        $qry = "//Attachment[./MediaID[text() = '$id']]";

                                                                                                                        if(null !== ($imgNode = $articleSourceXPATH->evaluate($qry, $articleNode)[0])){

                                                                                                                                if(isset($filename)) unset($filename);
                                                                                                                                if(isset($title)) unset($title);
                                                                                                                                foreach($imgNode->childNodes as $_){
                                                                                                                                        if($_->nodeName == 'AttFilename') $filename = $_->nodeValue;
                                                                                                                                        if($_->nodeName == 'AttTitle') $title = $_->nodeValue;
                                                                                                                                }

                                                                                                                                if($filename){

                                                                                                                                        //try to open file
                                                                                                                                        $source = explode('/', $articleMeta->source);
                                                                                                                                        $path = $source[2].'/'.$source[3];
                                                                                                                                        $path = "http://www.egms.de/tools/download.jsp?path=journals/$path/".$filename;

                                                                                                                                        if(false !== ($content = file_get_contents($path))){
                                                                                                                                                $blob = new Blob();
                                                                                                                                                $fid = $blob->create($content, $attachment['mimetype'], ['name' => $filename, 'description' => $title]);
                                                                                                                                                $article->jrna_files[] = $fid;

                                                                                                                                                $url = Url::fromRoute('publisso_gold.getFile', ['fid' => $fid]);

                                                                                                                                                $anchor  = $url->toString();
                                                                                                                                                $text = str_replace('Â', '', $aNode->nodeValue);

                                                                                                                                                $fragment = $pgraphContentXML->createDocumentFragment();


                                                                                                                                                $newANode = $pgraphContentXML->createElement('a', $text);
                                                                                                                                                $newANode->setAttribute('href', $anchor);
                                                                                                                                                $fragment->appendChild($newANode);


                                                                                                                                                $replaceLinks['oldLinks'][] = $aNode;
                                                                                                                                                $replaceLinks['newLinks'][] = $fragment;
                                                                                                                                        }
                                                                                                                                }
                                                                                                                        }
                                                                                                                }

                                                                                                                break;

                                                                                                        case 'link-figure':

                                                                                                                $type = 'figure';

                                                                                                        case 'link-table':

                                                                                                                if(!isset($type)) $type = 'table';
                                                                                                                $linkList[$linkName]['type'] = $type;
                                        
                                                                                                                if(array_key_exists('query', $linkList[$linkName])) {
                                                                                                                        echo $linkList[$linkName]['query'].'<br>';
                                                                                                                        ob_flush();
                                                                                                                        if ( !array_key_exists( $linkList[ $linkName ][ 'query' ], $imgList ) ) {
                
                                                                                                                                $mid = substr( explode( '=', $linkList[ $linkName ][ 'query' ] )[ 1 ], 1 );
                                                                                                                                $qry = "//" . ucfirst( $type ) . "[./MediaID[text() = '$mid']]";
                                                                                                                                echo $mid.'<br>';
                                                                                                                                if ( null !== ( $imgNode = $articleSourceXPATH->evaluate( $qry, $articleNode )[ 0 ] ) ) {
                                                                                        
                                                                                                                                        echo "Node found...!<br>";
                                                                                                                                        foreach ( $imgNode->childNodes as $_ ) {
                                
                                                                                                                                                if ( $_->nodeName == 'ImgFormat' ) $format = $_->nodeValue;
                                                                                                                                                if ( $_->nodeName == 'ImgWidth' ) $width = $_->nodeValue;
                                                                                                                                                if ( $_->nodeName == 'ImgHeight' ) $height = $_->nodeValue;
                                                                                                                                        }
                        
                                                                                                                                        if ( $format) {
                                                                                                                                                echo "Attributes found...!<br>";
                                                                                                                                                if ( isset( $file ) ) unset( $file );
                                                                                                                                                $filePattern = $article->identifier . '.' . explode( '=', $linkList[ $linkName ][ 'query' ] )[ 1 ] . '.' . $format;
                                
                                                                                                                                                $iti = new \RecursiveDirectoryIterator( $tmpPath );
                                                                                                                                                foreach ( new \RecursiveIteratorIterator( $iti ) as $file ) {
                                                                                                                                                        if ( strpos( $file, $filePattern ) !== false ) {
                                                                                                                                                                break;
                                                                                                                                                        }
                                                                                                                                                }
                                
                                                                                                                                                if ( $file ) {
                                                                                                                                                        $file = $file->getPath() . '/' . $filePattern;
                                        
                                                                                                                                                        $content = file_get_contents( $file );
                                                                                                                                                        $hash = md5( $linkList[ $linkName ][ 'query' ] );
                                                                                                                                                        $content = base64_encode( $content );
                                        
                                                                                                                                                        $qry .= "//content";
                                                                                                                                                        if ( $articleSourceXPATH->evaluate( $qry, $articleNode )->length > 0 ) {
                                                                                                                                                                $caption = str_replace( [ '<p>', '</p>' ], '', $articleSourceXPATH->evaluate( $qry, $articleNode )[ 0 ]->nodeValue );
                                                                                                                                                        }
                                        
                                        
                                                                                                                                                        $imgList[ $linkList[ $linkName ][ 'query' ] ] = $hash;
                                                                                                                                                        $images[ $hash ] = '
                                                                                                                                                        <a name="' . $linkList[ $linkName ][ 'type' ] . '~' . $hash . '"></a>
                                                                                                                                                        <figure>
                                                                                                                                                                <img src="data:image/' . $format . ';base64,' . $content . '">
                                                                                                                                                                <figcaption>' . ( $caption ? $caption : '' ) . '</figcaption>
                                                                                                                                                        </figure>
                                                                                                                                                ';
                                                                                                                                                }
                                                                                                                                        }
                                                                                                                                }
                                                                                                                        }
                                                                                                                }
                                                                                                                elseif($type == 'figure' && array_key_exists('target', $linkList[$linkName])){
        
                                                                                                                        if ( isset( $file ) ) unset( $file );
                                                                                                                        $_name = explode('/', $linkList[$linkName]['target']);
                                                                                                                        $filePattern = $_name[count($_name)];
        
                                                                                                                        $iti = new \RecursiveDirectoryIterator( $tmpPath );
                                                                                                                        foreach ( new \RecursiveIteratorIterator( $iti ) as $file ) {
                                                                                                                                if ( strpos( $file, $filePattern ) !== false ) {
                                                                                                                                        break;
                                                                                                                                }
                                                                                                                        }
        
                                                                                                                        if ( $file ) {
                                                                                                                                $file = $file->getPath() . '/' . $filePattern;
                                                                                                                                
                                                                                                                                $iiNr = preg_replace('/^i/', '', explode('.', $filePattern)[1]);
        
                                                                                                                                $_iiNode = $articleSourceXPATH->evaluate( '//InlineFigure/MediaID[text()="'.$iiNr.'"]', $articleNode )[ 0 ]->parentNode;
                                                                                                                                if($_iiNode){
                                                                                                                                        $alt = $articleSourceXPATH->evaluate('/ImgAltText', $_iiNode)->nodeValue;
                                                                                                                                        $format = $articleSourceXPATH->evaluate('/ImgFormat', $_iiNode)->nodeValue;
                                                                                                                                }
                                                                                                                                
                                                                                                                                $content = file_get_contents( $file );
                                                                                                                                $hash = md5( $content);
                                                                                                                                $content = base64_encode( $content );
                
                                                                                                                                $imgList[ $linkList[ $linkName ][ 'query' ] ] = $hash;
                                                                                                                                $images[ $hash ] = '
                                                                                                                                                        <a name="' . $linkList[ $linkName ][ 'type' ] . '~' . $hash . '"></a>
                                                                                                                                                        <figure>
                                                                                                                                                                <img src="data:image/' . $format . ';base64,' . $content . '" alt="'.$alt.'">
                                                                                                                                                        </figure>
                                                                                                                                                ';
                                                                                                                        }
                                                                                                                }
                                                                                                                
                                                                                                                $anchor  = "#".$linkList[$linkName]['type']."~".$imgList[$linkList[$linkName]['query']];
                                                                                                                $text = str_replace(['[', ']'], '', str_replace('Â', '', $aNode->nodeValue));

                                                                                                                $fragment = $pgraphContentXML->createDocumentFragment();

                                                                                                                $fragment->appendChild($pgraphContentXML->createTextNode('['));
                                                                                                                $newANode = $pgraphContentXML->createElement('a', $text);
                                                                                                                $newANode->setAttribute('href', $anchor);
                                                                                                                $fragment->appendChild($newANode);
                                                                                                                $fragment->appendChild($pgraphContentXML->createTextNode(']'));

                                                                                                                $replaceLinks['oldLinks'][] = $aNode;
                                                                                                                $replaceLinks['newLinks'][] = $fragment;

                                                                                                                break;
                                                                                                }

                                                                                        }
                                                                                        
                                                                                        foreach($replaceLinks['oldLinks'] as $index => $_){
                                                                                                $_->parentNode->replaceChild($replaceLinks['newLinks'][$index], $_);
                                                                                        }
                                                                                        
                                                                                        ob_flush();
                                                                                        
                                                                                        $content = $pgraphContentXML->saveHTML();
                                                                                        $content = str_replace(['<p>', '</p>'], ["\n<p>", "<p>\n"], $content);
                                                                                        
                                                                                        preg_match_all('/(\<p\>){0,1}(.+)(\<\/p\>){0,1}/', $content, $matches);
                                                                                        
                                                                                        foreach($matches[0] as $text){
                                                                                                
                                                                                                $app = '';
                                                                                                
                                                                                                foreach($images as $hash => $snip){
                                                                                                        
                                                                                                        if(strstr($text, $hash)){
                                                                                                               
                                                                                                               if(!in_array($hash, $usedImages)) {
                                                                                                                        #echo $hash.'<br>'.$snip;
                                                                                                                        $app .= $snip;
                                                                                                                        unset( $images[ $hash ] );
                                                                                                                        $usedImages[] = $hash;
                                                                                                               }
                                                                                                        }
                                                                                                }
                                                                                                
                                                                                                $article->$objIdentifier .= $text.$app;
                                                                                        }
                                                                                        
                                                                                        //Behandle einige spezielle Sachen
                                                                                        
                                                                                        //Leere Absätze
                                                                                        $article->$objIdentifier = str_replace('<p></p>', '', $article->$objIdentifier);
                                                                                        
                                                                                        //Description-Lists
                                                                                        $article->$objIdentifier = preg_replace('/(\<dl .*?\>)/', '<ol>', $article->$objIdentifier);
                                                                                        $article->$objIdentifier = preg_replace('/(\<\/dl\>)/', '</ol>', $article->$objIdentifier);
                                                                                        $article->$objIdentifier = preg_replace('/(\<dt.+?\/dt\>)/', '', $article->$objIdentifier);
                                                                                        $article->$objIdentifier = preg_replace('/(\<dd.*?\>)/', '<li>', $article->$objIdentifier);
                                                                                        $article->$objIdentifier = preg_replace('/(\<\/dd.*?\>)/', '</li>', $article->$objIdentifier);
                                                                                        
                                                                                        //div mit Einrückung
                                                                                        $article->$objIdentifier = preg_replace('/\<div class="indentation"\>/', '<div class="alert alert-info">', $article->$objIdentifier);
                                                                                }
                                                                        }
                                                                }
                                                        }
                                                }
                                                
                                                /*
                                                 * assign this article to collection
                                                 */
                                                $identifier   = $article->identifier;
                                                $origLanguage = $article->language_orig;
                                                $language = $article->language;
                                                $articles->$identifier->article->$language = $article;
                                                $articleData->$language = $article;
                                                
                                                //echo $articleNode->C14N();
                                        }
                                        
                                        ob_flush();
                                        echo '<pre>';
                                        echo "Imported $article->identifier\n";
                                        echo '</pre>';
                                        
                                        if($origLanguage && $articleData->$origLanguage){
                                                
                                                $_ = $articleData->$origLanguage;
                                                
                                                //search the article
                                                $jrna_id = \Drupal::database()->select('rwPubgoldJournalArticles', 't')->fields('t', ['jrna_id'])->condition('jrna_title', $_->title, '=')->execute()->fetchField();
                                                if(!$jrna_id) $jrna_id = \Drupal::database()->insert('rwPubgoldJournalArticles')->fields(['jrna_title' => $_->title])->execute();
                                                
                                                if(!in_array($jrna_id, $articleIDs)) $articleIDs[] = $jrna_id;
                                                
                                                $objArticle = new Journalarticle($jrna_id);
                                                
                                                if($_->volume){
                                
                                                        $volume = new Volume();
                                                        $volume->create($journal->jrn_id, $_->publication_year, $_->volume);
                                                        $objArticle->setElement('volume', $volume->getId());
                                                        
                                                }
                                                
                                                if($_->issue){
                                                        
                                                        $issue = new Issue();
                                                        $issue->create($journal->jrn_id, $volume->getId(), ['number' => $_->issue]);
                                                        $objArticle->setElement('issue', $issue->getId());
                                                }
                                                
                                                $objArticle->setElement('license', $_->license ? $_->license : null);
                                                $objArticle->setElement('publication_year', $_->publication_year ? $_->publication_year : null);
                                                $objArticle->setElement('doi', $_->doi ? $_->doi : null);
                                                $objArticle->setElement('urn', $_->urn ? $_->urn : null);
                                                $objArticle->setElement('docno', $_->doc_no ? $_->doc_no : null);
                                                $objArticle->setElement('publisher', $_->publisher ? \Drupal::service('publisso_gold.tools')->createPublisher(html_entity_decode($_->publisher)) : null);
                                                $objArticle->setElement('publication_place', $_->publication_place ? \Drupal::service('publisso_gold.tools')->createPublicationPlace(html_entity_decode($_->publication_place)) : null);
                                                $objArticle->setElement('keywords', json_encode($_->keywords));
                                                $objArticle->setElement('received', $_->received ? $_->received : null);
                                                $objArticle->setElement('revised', $_->revised ? $_->revised : null);
                                                $objArticle->setElement('accepted', $_->accepted ? $_->accepted : null);
                                                $objArticle->setElement('sub_category', $_->sub_category ? $_->sub_category : null);
                                                $objArticle->setElement('ddc', $_->ddc ? $_->ddc : null);
                                                $objArticle->setElement('published', $_->published ? $_->published : null);
                                                $objArticle->setElement('files', $_->jrna_files ? implode(",", array_unique($_->jrna_files)) : null);
                                                $objArticle->setElement('jrnid', $journal->jrn_id);
                                                $objArticle->setElement('article_type', $_->type);
                                                
                                                $authors = [];
                                                foreach($_->authors as $author){
                                                        $authors[] = (object)$author;
                                                }

                                                $objArticle->setElement('authors', json_encode($authors));

                                                $objArticle->setElement('pdf_blob_id', $_->jrna_pdf_blob_id ? $_->jrna_pdf_blob_id : null);
                                                $objArticle->setElement('xml_blob_id', $_->jrna_xml_blob_id ? $_->jrna_xml_blob_id : null);
                                                $objArticle->setElement('abstract', $_->abstract ? base64_encode($_->abstract) : null);
                                                $objArticle->setElement('article_text', $_->textblock ? base64_encode($_->textblock) : null);
                                                
                                                if(empty($objArticle->getElement('wfid'))) {
                                                        $workflow = new Workflow();
                                                        $workflow->create( 1 );
                                                        $workflow->setElement( 'state', 'published' );
                                                        $workflow->setDataElement( 'article_text', base64_encode( $_->textblock ) );
                                                        $workflow->setDataElement( 'abstract', base64_encode( $_->abstract ) );
                                                        $objArticle->setElement( 'wfid', $workflow->getElement( 'id' ) );
        
                                                        $references = [];
                                                        foreach ( $article->references as $ref ) {
                                                                $references[] = "[" . $ref[ 'no' ] . "] " . $ref[ 'total' ];
                                                        }
        
                                                        $workflow->setDataElement( 'references', implode( "\n", $references ) );
                                                }
                                                $langs = [];
                                                foreach($articleData as $lang => $article){
                                                        if($lang != $origLanguage) $langs[] = $lang;
                                                }
                                                
                                                foreach($langs as $lang){
                                                        flush();
                                                        echo "Store translation '$lang'";
                                                        $_ = $articleData->$lang;
                                                        
                                                        $jrna_id = \Drupal::database()->select('rwPubgoldJournalArticles_'.$lang, 't')->fields('t', ['jrna_id'])->condition('jrna_id', $objArticle->getElement('id'), '=')->execute()->fetchField();
                                                        if(!$jrna_id) $jrna_id = \Drupal::database()->insert('rwPubgoldJournalArticles_'.$lang)->fields(['jrna_id' => $objArticle->getElement('id')])->execute();
                                                        
                                                        if($_->volume){

                                                                $volume = new Volume();
                                                                $volume->create($journal->jrn_id, $_->publication_year, $_->volume);
                                                                $_->volume = $volume->getId();

                                                        }

                                                        if($_->issue){

                                                                $issue = new Issue();
                                                                $issue->create($journal->jrn_id, $volume->getId(), ['number' => $_->issue, 'title' => $_->issue_title ?? null]);
                                                                $_->issue = $issue->getId();
                                                        }
                                                        
                                                        $authors = [];
                                                        foreach($_->authors as $author){
                                                                $authors[] = (object)$author;
                                                        }
                                                        $_->authors = json_encode($authors);
                                                        
                                                        $fields = [
                                                                'jrna_license' => $_->license,
                                                                'jrna_publication_year' => $_->publication_year,
                                                                'jrna_doi' => $_->doi,
                                                                'jrna_urn' => $_->urn,
                                                                'jrna_docno' => $_->doc_no,
                                                                'jrna_publisher' => $_->publisher ? \Drupal::service('publisso_gold.tools')->createPublisher(html_entity_decode($_->publisher)) : null,
                                                                'jrna_publication_place' => $_->publication_place ? \Drupal::service('publisso_gold.tools')->createPublicationPlace(html_entity_decode($_->publication_place)) : null,
                                                                'jrna_keywords' => json_encode($_->keywords),
                                                                'jrna_received' => $_->received,
                                                                'jrna_revised' => $_->revised,
                                                                'jrna_published' => $_->published,
                                                                'jrna_files' => implode(',', array_unique($_->jrna_files)),
                                                                'jrna_volume' => $_->volume,
                                                                'jrna_issue' => $_->issue,
                                                                'jrna_authors' => $_->authors,
                                                                'jrna_title' => $_->title,
                                                                'jrna_abstract' => base64_encode($_->abstract),
                                                                'jrna_article_text' => base64_encode($_->textblock),
                                                                'jrna_pdf_blob_id' => $_->jrna_pdf_blob_id,
                                                                'jrna_article_type' => $_->type,
                                                                'jrna_sub_category' => $_->sub_category ? $_->sub_category : null,
                                                                'jrna_ddc' => $_->ddc ? \Drupal::service('publisso_gold.tools')->createDDC($_->ddc): null,
                                                                'jrna_jrnid' => $journal->jrn_id
                                                        ];
                                                        
                                                        \Drupal::database()->update('rwPubgoldJournalArticles_'.$lang)->fields($fields)->condition('jrna_id', $objArticle->getElement('id'), '=')->execute();
                                                }
                                        }
                                }
                        }

                        exec("rm -rf $tmpPath");
                }
                
                exit();
        }
        
        private function test_diff() :array{
                
                $response = new Response();
                $response->setContent(__METHOD__);
                
                $oldChapter = new Bookchapter(154);
                $newChapter = new Bookchapter(157);
                
                $oldText = base64_decode($oldChapter->getElement('chapter_text'));
                $newText = base64_decode($newChapter->getElement('chapter_text'));
                
                $diff = new Drupal\publisso_gold\Classes\Diff\Diff($oldText, $newText);
                $diff->build();
                
                return [
                        '#markup' => $diff->getDifference()
                ];
        }
        
        /**
         * @return array
         */
        private function test_table(){
                return \Drupal::formBuilder()->getForm('\Drupal\publisso_gold\Form\TableDragExampleNestedForm');
        }
        
        /**
         * @return array|Response
         */
        public function test(){
                
                if(\Drupal::requestStack()->getCurrentRequest()->get('method')){
                        $m = 'test_'.\Drupal::requestStack()->getCurrentRequest()->get('method');
                        return $this->$m() ?? [];
                }
                
                $response = new Response();
                $response->headers->set('Content-Type', 'text/html');
                $response->setContent(gethostbyname(gethostname()));
                
                return $response;
        }
        
        /**
         * @return array
         */
        public function siteSkel(){
                return [];
        }
        
        /**
         * @return array
         */
        public function exportJournals(){
                
                $session = \Drupal::service('session');
                $tools   = \Drupal::service('publisso_gold.tools');
                $currentUser = $session->get('user') ?? [];
                
                if(!(array_key_exists('weight', $currentUser) && $currentUser['weight'] >= 80)){
                        return $tools->accessDenied();
                }
                $journals = [];
                
                foreach(\Drupal::database()->select('rwPubgoldJournals', 't')->fields('t', [])->execute()->fetchAll() as $k => $v){
                        
                        $xml = new \DOMDocument('1.0', 'utf-8');
                        $xpath = new \DOMXpath($xml);
                        $impl = new \DOMImplementation();
                        $xml->appendChild($impl->createDocumentType('journal'));
                        $rootNode = $tools->DOMAppendChild('journal', '', [], $xml, $xpath->evaluate('/')[0]);
                        
                        $v = (array)$v;
                        $journals[$v['jrn_id']] = $v;
                        $journals[$v['jrn_id']]['articles'] = ['_default_' => [], 'de' => [], 'en' => []];
                        $journals[$v['jrn_id']]['covers'] = [];
                        $journals[$v['jrn_id']]['logos'] = [];
                        
                        //Cover
                        if($covers = unserialize($v['jrn_cover'])){
                                
                                foreach($covers as $cover){
                                        
                                        if ( ($file = File::load( $cover)) !== null) {
                                                
                                                $uri = $file->url();
                
                                                if(($content = file_get_contents($uri)) !== false) {
                                                        $picture['picture_link'] = 'http://livinghandbooks-dev.rheinware.de/system/getPicture/ul/'.$cover;
                                                        $picture['picture_type'] = $file->getMimeType();
                                                        $journals[$v['jrn_id']]['covers'][] = $picture;
                                                }
                                        }
                                }
                        }
                        
                        //Logo
                        
                        if($logos = unserialize($v['jrn_logo'])){
                
                                foreach($logos as $logo){
                        
                                        if ( ($file = File::load( $logo)) !== null) {
                                
                                                $uri = $file->url();
                                
                                                if(($content = file_get_contents($uri)) !== false) {
                                                        $picture['picture_link'] = 'http://livinghandbooks-dev.rheinware.de/system/getPicture/ul/'.$logo;
                                                        $picture['picture_type'] = $file->getMimeType();
                                                        $journals[$v['jrn_id']]['logos'][] = $picture;
                                                }
                                        }
                                }
                        }
                        
                        //Artikel
                        foreach(\Drupal::database()->select('rwPubgoldJournalArticles', 't')->fields('t', [])->condition('jrna_jrnid', $v['jrn_id'], '=')->execute()->fetchAll() as $_v){
                                $_v = (array)$_v;
                                $journals[$v['jrn_id']]['articles']['default'][$_v['jrna_id']] = $_v;
                        }
        
                        foreach(\Drupal::database()->select('rwPubgoldJournalArticles_en', 't')->fields('t', [])->condition('jrna_jrnid', $v->jrn_id, '=')->execute()->fetchAll() as $_v){
                                $_v = (array)$_v;
                                $journals[$v['jrn_id']]['articles']['en'][$_v['jrna_id']] = $_v;
                        }
        
                        foreach(\Drupal::database()->select('rwPubgoldJournalArticles_de', 't')->fields('t', [])->condition('jrna_jrnid', $v->jrn_id, '=')->execute()->fetchAll() as $_v){
                                $_v = (array)$_v;
                                $journals[$v['jrn_id']]['articles']['de'][$_v['jrna_id']] = $_v;
                        }
                        
                        foreach($journals[$v['jrn_id']] as $_ => $__){
                                $this->exportJournals_appendNode($_, $__, $xml, $rootNode);
                        }
                        
                        $xml->formatOutput = true;
                        echo $xml->saveXML();
                }
                exit();
                return [];
        }
        
        /**
         * @param $k
         * @param $v
         * @param $xml
         * @param $parentNode
         */
        private function exportJournals_appendNode($k, $v, &$xml, $parentNode){
                
                $node = \Drupal::service('publisso_gold.tools')->DOMAppendChild($k, '', [], $xml, $parentNode);
                if(is_string($v))
                        \Drupal::service('publisso_gold.tools')->appendCdata($xml, $node, $v);
                elseif(is_array($v)){
                        foreach($v as $_ => $__){
                                $this->exportJournals_appendNode(preg_match('/^\d/', $_) ? 'item' : $_, $__, $xml, $node);
                        }
                }
        }
}
