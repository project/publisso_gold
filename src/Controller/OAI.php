<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\OAI.
 */

namespace Drupal\publisso_gold\Controller;
use Drupal\Core\Controller\ControllerBase;
        use Drupal\publisso_gold\Classes\OAIFactory;
        use Drupal\publisso_gold\Controller\Manager\OAIManager;
        use PhpParser\Error;
        use Symfony\Component\HttpFoundation\Request;
        use \Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Url;
        
        /**
         * Class OAI
         * @package Drupal\publisso_gold\Controller
         */
        class OAI extends ControllerBase{
	
	private $session;
	private $setup;
	private $tools;
	private $texts;
        
        /**
         * OAI constructor.
         */
        public function __construct(){
		$this->texts = \Drupal::service('publisso_gold.texts');
		$this->setup = \Drupal::service('publisso_gold.setup');
		$this->tools = \Drupal::service('publisso_gold.tools');
		$this->session = \Drupal::service('session');
	}
        
        /**
         * @return string
         */
        public function __toString(){
		return 'OAI-Controller';
	}
        
        /**
         * @param         $paramType
         * @param         $paramID
         * @param Request $request
         *
         * @return array|Response
         */
        public function main($paramType, $paramID, Request $request){
		
                \Drupal::service( 'page_cache_kill_switch' )->trigger();
                
                $params = array_merge($request->request->all(), $request->query->all());
                
                if(!$paramID && array_key_exists('verb', $params)){
                        $paramID = $params['verb'];
                }
                
                if(!$paramType) $paramType = 'response';
                
//                if(!$paramType || !$paramID){
//
//                        $urlPrefix = !$paramType ? 'oai/' : '';
//
//                        $response = new Response();
//
//                        $dom = new  \DOMDocument();
//                        $impl = new \DOMImplementation();
//                        $dom->appendChild($impl->createDocumentType('html'));
//                        $dom->loadHTML(OAIFactory::getDefaultResponse());
//                        $response->setContent($dom->saveHTML());
//                        return $response;
//                }
//                else {
                        try {
                                return $this->$paramType( ucfirst($paramID), $request );
                        }
                        catch ( \Error $e ) {
                                error_log( "Error in file >>" . $e->getFile() . "<< Line " . $e->getLine() . ": " . $e->getMessage() );
                                \Drupal::service( 'messenger' )->addError( $e->getMessage() );
                                return [];
                        }
                //}
	}
        
        /**
         * @param $paramType
         * @param $paramID
         *
         * @return mixed
         */
        public function title($paramType, $paramID){
		return $this->texts->get('oai.page.title.'.$paramType, 'afc');
	}
        
        /**
         * @param $paramID
         * @param Request $request
         * @return Response
         */
        private function request($paramID, Request $request){
                
                $renderer = \Drupal::service( 'renderer' );
                $response = new Response();
                
                //create the document
                $impl = new \DOMImplementation();
                $dom = $impl->createDocument(null, 'html', $impl->createDocumentType('html', '-//W3C//DTD XHTML 1.0 Transitional//EN', 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'));
                $xpath = new \DOMXPath($dom);
                $xpath->query('/html')->item(0)->appendChild($dom->createElement('head'));
                $xpath->query('/html')->item(0)->appendChild($dom->createElement('body'));
                
                //embed the styles
                $xpath->query('/html/head')->item(0)->appendChild($dom->createElement('style'));
                $xpath->query('/html/head/style')->item(0)->setAttribute('type', 'text/css');
                $xpath->query('/html/head/style')->item(0)->nodeValue = OAIFactory::getCSS();
                
                //append the navigation
                $navigation = new \DOMDocument();
                $navigation->loadHTML(OAIFactory::getNavigation());
                $navigationXpath = new \DOMXPath($navigation);
                $navigationHTML =  $navigation->saveHTML($navigationXpath->query('//body/div[@id="navigation"]')->item(0));
                $chunk = $dom->createDocumentFragment();
                $chunk->appendXML($navigationHTML);
                $xpath->query('/html/body')->item(0)->appendChild($chunk);
                
                //append the form
                $form = \Drupal::formBuilder()->getForm('\Drupal\publisso_gold\Form\OAI\\'.$paramID);
                $chunk = $dom->createDocumentFragment();
                $chunk->appendXML($renderer->render($form));
                $xpath->query('/html/body')->item(0)->appendChild($chunk);
                
                $response->setContent($dom->saveHTML());
                return $response;
        }
        
        /**
         * @param         $paramID
         * @param Request $request
         *
         * @return Response
         */
        private function response($paramID, Request $request){
                
                $response = new Response();
                $response->headers->set('Content-Type', 'text/xml');
                
                $xslPath = '//'.\Drupal::request()->getHost().OAIManager::getAssetsDir(false).'/oai.xsl';
                $xml = new \DOMDocument("1.0", "UTF-8");
                $xslNode = $xml->createProcessingInstruction('xml-stylesheet', 'type="text/xsl" href="'.$xslPath.'"');
                $xml->appendChild($xslNode);

                $rootNode = $xml->createElement('OAI-PMH');
                $rootNode->setAttribute('xmlns', 'http://www.openarchives.org/OAI/2.0/');
                $rootNode->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
                $rootNode->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'schemaLocation', 'http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd');
                $rootNode = $xml->appendChild($rootNode);

                $rootNode->appendChild($xml->createElement('responseDate', date("c")));
                $_node = $rootNode->appendChild($xml->createElement('request', '//'.explode('://', $request->getUri())[1]));
                
                if($paramID) {
                        $_node->setAttribute( 'verb', ucfirst( $paramID ) );
                        $_node = $rootNode->appendChild( $xml->createElement( $paramID ) );
                }
                
                $frag = $xml->createDocumentFragment();
                
                try {
                        $frag->appendXML( OAIFactory::getResponseSchema( $paramID ) );
                        $_node->appendChild( $frag );

                        OAIFactory::setSchemaValues( $xml, $paramID );
                }
                catch(\Error $e){
                        $errorNode = $xml->createElement('error');
                        $errorNode->setAttribute('code', 'badVerb');
                        $_node->parentNode->appendChild($errorNode);
                        error_log( "Error in file >>" . $e->getFile() . "<< Line " . $e->getLine() . ": " . $e->getMessage() );
                }
                
                $xml->formatOutput = true;
                $response->setContent($xml->saveXML());
                
                return $response;
        }
        
        
        
        /**
         * @param              $paramID
         * @param Request      $request
         * @param \DOMDocument $doc
         * @param \DOMXPath    $xpath
         */
        private function setResponseXMLElements( &$paramID, Request &$request, \DOMDocument &$doc, \DOMXPath &$xpath){
	       
	        $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold.oai.response')->get('data');
		$tempstore['response'] = \Drupal::service('request_stack')->getCurrentRequest()->getUri();
                $this->tools->DOMAppendChild('request', '//'.\Drupal::request()->getHost().'/publisso_gold/oai/',
                ['verb' =>
                        $paramID], $doc,
                $xpath->evaluate('/OAI-PMH')
		[0]);
                $rootNode = $this->tools->DOMAppendChild($paramID, '', [], $doc, $xpath->evaluate('/OAI-PMH')[0]);
                $method = "setResponseXMLElements_$paramID";
        
                try {
                        $this->$method($request, $doc, $xpath, $rootNode);
                }
                catch ( \Error $e ){
                        $parentNode = $rootNode->parentNode;
                        $rootNode->parentNode->removeChild($rootNode);
                        $this->tools->DOMAppendChild('error', '', ['code' => 'Illegal Verb'], $doc, $parentNode);
                        return;
                }
        }
        
        /**
         * @param Request $request
         * @param         $doc
         * @param         $xpath
         * @param         $rootNode
         */
        private function setResponseXMLElements_ListSets( Request &$request, &$doc, &$xpath, &$rootNode){
	        $setNode = $this->tools->DOMAppendChild('set', '', [], $doc, $rootNode);
	        $this->tools->DOMAppendChild('setSpec', 'open_access', [], $doc, $setNode);
	        $this->tools->DOMAppendChild('setName', 'Open Access Documents', [], $doc, $setNode);
        
                $setNode = $this->tools->DOMAppendChild('set', '', [], $doc, $rootNode);
                $this->tools->DOMAppendChild('setSpec', 'ddc', [], $doc, $setNode);
                $this->tools->DOMAppendChild('setName', 'DDC classified objects', [], $doc, $setNode);
        
                $setNode = $this->tools->DOMAppendChild('set', '', [], $doc, $rootNode);
                $this->tools->DOMAppendChild('setSpec', 'doc-type:article', [], $doc, $setNode);
                $this->tools->DOMAppendChild('setName', 'Article', [], $doc, $setNode);
        
                $setNode = $this->tools->DOMAppendChild('set', '', [], $doc, $rootNode);
                $this->tools->DOMAppendChild('setSpec', 'doc-type:chapter', [], $doc, $setNode);
                $this->tools->DOMAppendChild('setName', 'Chapter', [], $doc, $setNode);
        
                $setNode = $this->tools->DOMAppendChild('set', '', [], $doc, $rootNode);
                $this->tools->DOMAppendChild('setSpec', 'doc-type:conferenceObject', [], $doc, $setNode);
                $this->tools->DOMAppendChild('setName', 'Conference Object', [], $doc, $setNode);
        
                $setNode = $this->tools->DOMAppendChild('set', '', [], $doc, $rootNode);
                $this->tools->DOMAppendChild('setSpec', 'fundedressources', [], $doc, $setNode);
                $this->tools->DOMAppendChild('setName', 'Funded Ressources', [], $doc, $setNode);
        }
        
        /**
         * @param Request $request
         * @param         $doc
         * @param         $xpath
         * @param         $rootNode
         */
        private function setResponseXMLElements_ListMetadataFormats( Request &$request, &$doc, &$xpath, &$rootNode){
	       
	        $metadataFormatNode = $this->tools->DOMAppendChild('metadataFormat', '', [], $doc, $rootNode);
	        $this->tools->DOMAppendChild('metadataPrefix', 'oai_dc', [], $doc, $metadataFormatNode);
                $this->tools->DOMAppendChild('schema', 'http://www.openarchives.org/OAI/2.0/oai_dc.xsd', [], $doc, $metadataFormatNode);
                $this->tools->DOMAppendChild('metadataNamespace', 'http://www.openarchives.org/OAI/2.0/oai_dc/', [], $doc, $metadataFormatNode);
        }
        
        /**
         * @param Request $request
         * @param         $doc
         * @param         $xpath
         * @param         $rootNode
         */
        private function setResponseXMLElements_ListIdentifiers( Request &$request, &$doc, &$xpath, &$rootNode){
                
                $param = $request->query;
                
                $resumptionToken = null;
                if($param->has('resumptionToken')){
                        
                        if(count($param->all()) > 2 && !(in_array('verb', $param->all()))){
                                $parentNode = $rootNode->parentNode;
                                $rootNode->parentNode->removeChild($rootNode);
                                $this->tools->DOMAppendChild('error', '', ['code' => 'badArgument'], $doc, $parentNode);
                                return;
                        }
                        
                        $resumptionToken = $param->get('resumptionToken');
                }
                
                $resumptionToken = explode('|', $resumptionToken);
                $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold.oai.response')->get('data');
                
                foreach($tempstore as $k => $v){
                        if(!empty($v)) $resumptionToken[] = "$k=$v";
                }
                
                foreach($resumptionToken as $k => $v){
                        $_ = explode('=', $v);
                        $resumptionToken[$_[0]] = $_[1];
                        unset($resumptionToken[$k]);
                }
                
                foreach($param->all() as $k => $v){
                        $resumptionToken[$k] = $v;
                }
                
                if(!(array_key_exists('next', $resumptionToken))) $resumptionToken['next'] = 1;
                
                $requestNode = $xpath->evaluate('//request')->item(0);
                $requestNode->setAttribute('metadataPrefix', 'oai_dc');
                $requestNode->setAttribute('verb', 'ListIdentifiers');
                
                $qry = \Drupal::database()->select('rwpgvwEntities', 't')->fields('t', []);
                
                if(array_key_exists('from', $resumptionToken)){
                        
                        if(!preg_match('/\d{4}-\d{2}-\d{2}/', $resumptionToken['from'])){
                                $parentNode = $rootNode->parentNode;
                                $rootNode->parentNode->removeChild($rootNode);
                                $this->tools->DOMAppendChild('error', '', ['code' => 'badArgument'], $doc, $parentNode);
                                return;
                        }
                        
                        $qry->condition('published', $resumptionToken['from'], '>=');
                }
                
                if(array_key_exists('until', $resumptionToken)){
        
                        if(!preg_match('/\d{4}-\d{2}-\d{2}/', $resumptionToken['until'])){
                                $parentNode = $rootNode->parentNode;
                                $rootNode->parentNode->removeChild( $rootNode );
                                $this->tools->DOMAppendChild( 'error', '', [ 'code' => 'badArgument' ], $doc, $parentNode );
                                return;
                        }
                        
                        $qry->condition('published', $resumptionToken['until'], '<=');
                }
                
                if((array_key_exists('from', $resumptionToken) && array_key_exists('until', $resumptionToken)) && (strlen($resumptionToken['from']) != strlen($resumptionToken['until']))){
                        $parentNode = $rootNode->parentNode;
                        $rootNode->parentNode->removeChild( $rootNode );
                        $this->tools->DOMAppendChild( 'error', '', [ 'code' => 'badArgument' ], $doc, $parentNode );
                        return;
                }
                
                if((array_key_exists('from', $resumptionToken) && array_key_exists('until', $resumptionToken))) {
        
                        $from = strtotime( $resumptionToken[ 'from' ] );
                        $until = strtotime( $resumptionToken[ 'until' ] );
        
                        if ( $from > $until ) {
                                $parentNode = $rootNode->parentNode;
                                $rootNode->parentNode->removeChild( $rootNode );
                                $this->tools->DOMAppendChild( 'error', '', [ 'code' => 'badArgument' ], $doc, $parentNode );
                                return;
                        }
                }
                if(array_key_exists('set', $resumptionToken)){
                        
                        switch($resumptionToken['set']){
                                
                                case 'open_access':
                                case 'ddc':
                                        $qry->isNotNull('ddc');
                                        break;
                                
                                case 'doc-type:article':
                                        $qry->condition('type', 'JA', '=');
                                        break;
                                
                                case 'doc-type:chapter':
                                        $qry->condition('type', 'BC', '=');
                                        break;
                                
                                case 'doc-type:conferenceObject':
                                        $qry->condition('type', 'CP', '=');
                                        break;
                                
                                case 'fundedressources':
                                        $qry->isNotNull('funding_id');
                                        break;
                                        
                                default:
                                        $parentNode = $rootNode->parentNode;
                                        $rootNode->parentNode->removeChild($rootNode);
                                        $this->tools->DOMAppendChild('error', '', ['code' => 'illegalSet'], $doc, $parentNode);
                                        return;
                        }
                }
                
                $numDataset = $qry->countQuery()->execute()->fetchField();
                $recordNode = null;
                foreach($qry->range($resumptionToken['next'] - 1, 100)->execute()->fetchAll() as $res){
                        
                        if($recordNode) $rootNode = $recordNode->parentNode;
                        $method = "setResponseXMLElements_ListIdentifier_".$res->type;
                        $recordNode = $this->$method($res->id, $request, $doc, $xpath, $rootNode);
                }
                
                if($resumptionToken['next'] + 100 < $numDataset){
                        $next = $resumptionToken['next'] + 100;
                        
                        $params = [
                                'metadataPrefix' => 'oai_dc',
                                'next' => $next
                        ];
                        
                        foreach($resumptionToken as $k => $v){
                                if(!array_key_exists($k, $params)) $params[$k] = $v;
                        }
                        
                        $paramStr = '';
                        $availKeys = [
                                'set',
                                'from',
                                'until',
                                'next',
                                'metadataPrefix'
                        ];
                        
                        foreach($params as $k => $v){
                                if(!(empty($k) || empty($v)) && in_array($k, $availKeys))
                                        $paramStr .= (!empty($paramStr) ? '|' : '')."$k=$v";
                        }
                        
                        $this->tools->DOMAppendChild('resumptionToken', $paramStr, ['completeListSize' => $numDataset], $doc, $rootNode, true);
                }
        }
        
        /**
         * @param Request $request
         * @param         $doc
         * @param         $xpath
         * @param         $rootNode
         */
        private function setResponseXMLElements_ListRecords( Request &$request, &$doc, &$xpath, &$rootNode){
         
	        $param = $request->query;
	        
	        $resumptionToken = null;
	        if($param->has('resumptionToken')){
	                $resumptionToken = $param->get('resumptionToken');
                }
	        
	        $resumptionToken = explode('|', $resumptionToken);
                $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold.oai.response')->get('data');
                
                foreach($tempstore as $k => $v){
                        if(!empty($v)) $resumptionToken[] = "$k=$v";
                }
                
	        foreach($resumptionToken as $k => $v){
                        $_ = explode('=', $v);
	                $resumptionToken[$_[0]] = $_[1];
	                unset($resumptionToken[$k]);
                }
                
	        if(!(array_key_exists('next', $resumptionToken))) $resumptionToken['next'] = 1;
	        
                $requestNode = $xpath->evaluate('//request')->item(0);
	        $requestNode->setAttribute('metadataPrefix', 'oai_dc');
                $requestNode->setAttribute('verb', 'ListRecords');
                
                $qry = \Drupal::database()->select('rwpgvwEntities', 't')->fields('t', []);
                
                if(array_key_exists('from', $resumptionToken) && array_key_exists('until', $resumptionToken)){
                        
                        $from = strtotime($resumptionToken['from']);
                        $until = strtotime($resumptionToken['until']);
                        
                        if($from > $until){
                                $parentNode = $rootNode->parentNode;
                                $rootNode->parentNode->removeChild($rootNode);
                                $this->tools->DOMAppendChild('error', '', ['code' => 'cannotDisseminateFormat,badArgument'], $doc, $parentNode);
                                return;
                        }
                }
                
                if(array_key_exists('from', $resumptionToken)){
                        $qry->condition('published', $resumptionToken['from'], '>=');
                }
        
                if(array_key_exists('until', $resumptionToken)){
                        $qry->condition('published', $resumptionToken['until'], '<=');
                }
                
                if(array_key_exists('set', $resumptionToken)){
                        
                        switch($resumptionToken['set']){
                                
                                case 'ddc':
                                        $qry->isNotNull('ddc');
                                        break;
                                        
                                case 'doc-type:article':
                                        $qry->condition('type', 'JA', '=');
                                        break;
        
                                case 'doc-type:chapter':
                                        $qry->condition('type', 'BC', '=');
                                        break;
        
                                case 'doc-type:conferenceObject':
                                        $qry->condition('type', 'CP', '=');
                                        break;
        
                                case 'fundedressources':
                                        $qry->isNotNull('funding_id');
                                        break;
                                        
                                case 'status-type:publishedVersion':
                                        $qry->condition('status', 'published', '=');
                                        break;
        
                                case 'status-type:updatedVersion':
                                        $qry->condition('status', 'updated', '=');
                                        break;
                                        
                                default:
                                        if(preg_match('/^ddc:(.+)/', $resumptionToken['set'], $matches)){
                                                $qry->condition('ddc', $matches[1], '=');
                                        }
                        }
                }
                $qry->orderBy('published', 'ASC');
                $numDataset = $qry->countQuery()->execute()->fetchField();
                $recordNode = null;
                foreach($qry->range($resumptionToken['next'] - 1, 100)->execute()->fetchAll() as $res){
                        
                        if($recordNode) $rootNode = $recordNode->parentNode;
                        $method = "setResponseXMLElements_GetRecord_".$res->type;
                        $recordNode = $this->$method($res->id, $request, $doc, $xpath, $rootNode);
                }
                
                if($resumptionToken['next'] + 100 < $numDataset){
                        $next = $resumptionToken['next'] + 100;
                        
                        $params = [
                                'metadataPrefix' => 'oai_dc',
                                'next' => $next
                        ];
                        
                        foreach($resumptionToken as $k => $v){
                                if(!array_key_exists($k, $params)) $params[$k] = $v;
                        }
                        
                        $paramStr = '';
                        $availKeys = [
                                'set',
                                'from',
                                'until',
                                'next',
                                'metadataPrefix'
                        ];
                        
                        foreach($params as $k => $v){
                                if(!(empty($k) || empty($v)) && in_array($k, $availKeys))
                                        $paramStr .= (!empty($paramStr) ? '|' : '')."$k=$v";
                        }
                        date_default_timezone_set("UTC");
                        $this->tools->DOMAppendChild('resumptionToken', $paramStr, ['completeListSize' => $numDataset, 'expirationDate' => date('c', time() + 48 * 3600)], $doc, $rootNode, true);
                }
        }
        
        /**
         * @param Request $request
         * @param         $doc
         * @param         $xpath
         * @param         $rootNode
         *
         * @return mixed
         */
        private function setResponseXMLElements_GetRecord(Request &$request, &$doc, &$xpath, &$rootNode){
        
                $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold.oai.response')->get('data');
                
                if($request->query->has('set')){
                        $set = $request->query->get('set');
                        $sets = explode('&', $set);
                        foreach($sets as $set){
                                list($k, $v) = explode('=', $set);
                                $tempstore[$k] = $v;
                        }
                }
                
                if($request->query->has('identifier')){
                        $tempstore['identifier'] = $request->query->get('identifier');
                }
                
                if(!$tempstore['identifier'] || ($tempstore['metadataPrefix'] != "" && $tempstore['metadataPrefix'] != 'oai_dc')){
                        
                        $parentNode = $rootNode->parentNode;
                        $rootNode->parentNode->removeChild($rootNode);
                        $this->tools->DOMAppendChild('error', '', ['code' => 'Bad Argument'], $doc, $parentNode);
                        return;
                }
                
                $identifier = $tempstore['identifier'];
                
                $requestNode = $xpath->evaluate('//request')->item(0);
                $requestNode->setAttribute('identifier', $identifier);
                $requestNode->setAttribute('metadataPrefix', 'oai_dc');
                
                if(!preg_match('/^oai:oai\.publisso\.de:(BC|JA|CP)!(DOI|IID)!([^!:]+)$/', $identifier, $matches)){
                        $parentNode = $rootNode->parentNode;
                        $rootNode->parentNode->removeChild($rootNode);
                        $this->tools->DOMAppendChild('error', '', ['code' => 'Malformed Identifier'], $doc, $parentNode);
                        return;
                }
                
                $type   = $matches[1];
                $IDType = $matches[2];
                $ID     = $matches[3];
                
                $qry = \Drupal::database()->select('rwpgvwEntities', 't')->fields('t', ['id'])->condition('type', $type, '=');
                
                if($IDType == 'DOI')
                        $qry->condition('doi', '%'.$ID, 'LIKE');
                elseif($IDType == 'IID')
                        $qry->condition('id', $ID, '=');
                
                if(!($id = $qry->execute()->fetchField())){
                        $parentNode = $rootNode->parentNode;
                        $rootNode->parentNode->removeChild($rootNode);
                        $this->tools->DOMAppendChild('error', '', ['code' => 'Entity Not Found'], $doc, $parentNode);
                        return;
                }
                
                $method = "setResponseXMLElements_GetRecord_$type";
                return $this->$method($id, $request, $doc, $xpath, $rootNode);
        }
        
        /**
         * @param         $cp_id
         * @param Request $request
         * @param         $doc
         * @param         $xpath
         * @param         $rootNode
         *
         * @return mixed
         */
        private function setResponseXMLElements_ListIdentifier_BC($cp_id, Request &$request, &$doc, &$xpath, &$rootNode){
                
                $chapter = new Bookchapter($cp_id);
                $book = $chapter->getMedium();
                return $this->setResponseXMLElements_ListIdentifier_EntityInfo($chapter, $book, $request, $doc, $xpath, $rootNode, 'bookPart', 'BC');
        }
        
        /**
         * @param         $jrna_id
         * @param Request $request
         * @param         $doc
         * @param         $xpath
         * @param         $rootNode
         *
         * @return mixed
         */
        private function setResponseXMLElements_ListIdentifier_JA($jrna_id, Request &$request, &$doc, &$xpath, &$rootNode){
                
                $article = new Journalarticle($jrna_id);
                $journal = $article->getMedium();
                return $this->setResponseXMLElements_ListIdentifier_EntityInfo($article, $journal,$request, $doc, $xpath, $rootNode, 'article', 'JA');
        }
        
        /**
         * @param         $cp_id
         * @param Request $request
         * @param         $doc
         * @param         $xpath
         * @param         $rootNode
         *
         * @return mixed
         */
        private function setResponseXMLElements_GetRecord_BC($cp_id, Request &$request, &$doc, &$xpath, &$rootNode){
         
	        $chapter = new Bookchapter($cp_id);
                $book = $chapter->getMedium();
                return $this->setResponseXMLElements_GetRecord_EntityInfo($chapter, $book, $request, $doc, $xpath, $rootNode, 'bookPart', 'BC');
        }
        
        /**
         * @param         $jrna_id
         * @param Request $request
         * @param         $doc
         * @param         $xpath
         * @param         $rootNode
         *
         * @return mixed
         */
        private function setResponseXMLElements_GetRecord_JA($jrna_id, Request &$request, &$doc, &$xpath, &$rootNode){
                
                $article = new Journalarticle($jrna_id);
                $journal = $article->getMedium();
                return $this->setResponseXMLElements_GetRecord_EntityInfo($article, $journal,$request, $doc, $xpath, $rootNode, 'article', 'JA');
        }
        
        /**
         * @param $entity
         * @param $medium
         * @param $request
         * @param $doc
         * @param $xpath
         * @param $rootNode
         * @param $docType
         * @param $identityType
         *
         * @return mixed
         */
        private function setResponseXMLElements_ListIdentifier_EntityInfo(&$entity, &$medium, &$request, &$doc, &$xpath, &$rootNode, $docType, $identityType){
                $parentNode = $rootNode->parentNode;
                $headerNode = $this->tools->DOMAppendChild('header', '', ['xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance', 'xmlns:dimdi' => 'http://www.dimdi.de/database'], $doc, $rootNode);
                if(!empty($entity->getElement('identifier'))) {
                        $identifier = 'oai:oai.publisso.de:'.$identityType.'!DOI!'.$entity->getElement('doi');
                }
                else{
                        $identifier = 'oai:oai.publisso.de:'.$identityType.'!IID!'.$entity->getElement('id');
                }
        
                $this->tools->DOMAppendChild( 'identifier', $identifier, [], $doc, $headerNode);
                $this->tools->DOMAppendChild('datestamp', $entity->getElement('published'), [], $doc, $headerNode);
                $this->tools->DOMAppendChild('setSpec', 'doc-type:'.$docType, [], $doc, $headerNode);
                $this->tools->DOMAppendChild('setSpec', 'open_access', [], $doc, $headerNode);
        
                if ( !empty( $entity->getElement('ddc') ) ) {
                        $this->tools->DOMAppendChild('setSpec', 'ddc:'.$entity->getElement('ddc'), [], $doc, $headerNode);
                }
                $this->tools->DOMAppendChild('setSpec', 'status-type:'.($entity->getElement('version') > 1 ? 'updatedVersion' : 'publishedVersion'), [], $doc, $headerNode);
                
                return $headerNode;
        }
        
        /**
         * @param $entity
         * @param $medium
         * @param $request
         * @param $doc
         * @param $xpath
         * @param $rootNode
         * @param $docType
         * @param $identityType
         *
         * @return mixed
         */
        private function setResponseXMLElements_GetRecord_EntityInfo(&$entity, &$medium, &$request, &$doc, &$xpath, &$rootNode, $docType, $identityType){
	        $parentNode = $rootNode->parentNode;
                $recordNode = $this->tools->DOMAppendChild('record', '' , ['xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance', 'xmlns:dimdi' => 'http://www.dimdi.de/database'], $doc, $rootNode);
                $headerNode = $this->tools->DOMAppendChild('header', '', [], $doc, $recordNode);
                if(!empty($entity->getElement('identifier'))) {
                        $identifier = 'oai:oai.publisso.de:'.$identityType.'!DOI!'.$entity->getElement('doi');
                }
                else{
                        $identifier = 'oai:oai.publisso.de:'.$identityType.'!IID!'.$entity->getElement('id');
                }
        
                $this->tools->DOMAppendChild( 'identifier', $identifier, [], $doc, $headerNode);
                $this->tools->DOMAppendChild('datestamp', $entity->getElement('published'), [], $doc, $headerNode);
                $this->tools->DOMAppendChild('setSpec', 'doc-type:'.$docType, [], $doc, $headerNode);
                $this->tools->DOMAppendChild('setSpec', 'open_access', [], $doc, $headerNode);
                if(!empty($entity->getElement('ddc')))
                        $this->tools->DOMAppendChild('setSpec', 'ddc:'.$entity->getElement('ddc'), [], $doc, $headerNode);
                $this->tools->DOMAppendChild('setSpec', 'status-type:'.($entity->getElement('version') > 1 ? 'updatedVersion' : 'publishedVersion'), [], $doc, $headerNode);
                
                $metadataNode = $this->tools->DOMAppendChild('metadata', '', [], $doc, $recordNode);
                $dc = $this->tools->DOMAppendChild(
                        'oai_dc:dc',
                        '',
                        [
                                'xsi:schemaLocation' 	=> "http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd",
                                'xmlns:dc' 		=> "http://purl.org/dc/elements/1.1/",
                                'xmlns:oai_dc' 		=> "http://www.openarchives.org/OAI/2.0/oai_dc/"
                        ],
                        $doc,
                        $metadataNode
                );
        /*
                $doc->loadXML($doc->saveXML());
                $doc->formatOutput = true;
                $xpath = new \DOMXpath($doc);
                $xpath->registerNamespace('', 'http://www.openarchives.org/OAI/2.0/');
                $xpath->registerNamespace('oai_dc', 'http://www.openarchives.org/OAI/2.0/oai_dc/');
                $xpath->registerNamespace('dimdi', 'http://www.dimdi.de/database');
          */
                #$dc = $xpath->query('//oai_dc:dc[last()]')[0];
        
                //(dc:title)
                $this->tools->DOMAppendChild('dc:title', $entity->getElement('title'), [], $doc, $dc, true);
        
                //(dc:creator)
                $authors = [];
                
                foreach($entity->readAuthors() as $author){
                        $authors[] = implode(', ', array_filter([$author->profile->getElement('lastname'), substr($author->profile->getElement('firstname'), 0, 1)]));
                }
        
                foreach(array_filter(json_decode($entity->getElement('corporation')) ?? []) as $corporation){
                        $authors[] = $corporation;
                }
        
                foreach($authors as $_ => $author){
                        $_ = $this->tools->DOMAppendChild('dc:creator', htmlspecialchars($author), [], $doc, $dc, true);
                }
        
                //(dc:relations)
                $references = array_filter(explode("\n", base64_decode($entity->getElement('references'))));
                foreach($references as $_ => $reference){
                        $reference = trim(preg_replace('/^\[\d+?\]/', '', strip_tags(trim($reference))));
                        $this->tools->DOMAppendChild('dc:relation', $reference, [], $doc, $dc, true);
                }
        
                //(dc:description)
                if(!empty($entity->getElement('abstract')))
                        $this->tools->DOMAppendChild('dc:description', preg_replace('/&#(\d+?);|\r|\n/', '', html_entity_decode(str_replace('"', '\"', strip_tags(base64_decode($entity->getElement('abstract')))))), [], $doc, $dc, true);
        
                //(dc:publisher)
                $this->tools->DOMAppendChild('dc:publisher', implode(';', array_filter([$medium->getElement('publisher'), $medium->getElement('publication_place')])), [], $doc, $dc, true);
        
                //(dc:coverage)
                if(!empty($entity->getElement('ddc')))
                        $this->tools->DOMAppendChild('dc:coverage', 'ddc:'.$entity->getElement('ddc'), [], $doc, $dc, true);
        
                //(dc:language)
                if(!empty($medium->getElement('language')))
                        $this->tools->DOMAppendChild('dc:language', $medium->getElement('language'), [], $doc, $dc, true);
        
                $this->tools->DOMAppendChild('dc:date', $entity->getElement('published'), [], $doc, $dc, true);
        
                //(dc:identifier)
                if(!empty($entity->getElement('doi')))
                        $this->tools->DOMAppendChild('dc:identifier', $entity->getElement('doi'), [], $doc, $dc, true);
                if(!empty($entity->getElement('doi')))
                        $this->tools->DOMAppendChild('dc:identifier', 'http://dx.doi.org/'.$entity->getElement('doi'), [], $doc, $dc);
        
                $url = $entity->getLink('url');
                $url->setAbsolute();
                $this->tools->DOMAppendChild('dc:identifier', $url->toString(), [], $doc, $dc);
        
                //(dc:license)
                if(!empty($entity->getElement('license'))){
                
                        $license = new License($entity->getElement('license'));
                        $this->tools->DOMAppendChild('dc:rights', $license->getElement('url'), [], $doc, $dc);
                }
        
                //(dc:subject)
                $subject = json_decode($entity->getElement('keywords'));
                $subject[] = 'ddc:'.$medium->getElement('ddc');
        
                foreach(array_filter($subject) as $_ => $kw){
                        $this->tools->DOMAppendChild('dc:subject', $kw, [], $doc, $dc, true);
                }
        
                $this->tools->DOMAppendChild('dc:type', "text", [], $doc, $dc, true);
                $this->tools->DOMAppendChild('dc:type', $docType, [], $doc, $dc, true);
                
                $this->tools->DOMAppendChild('dc:source', 'Chapter of '.$medium->getElement('title'), [], $doc, $dc, true);
                return $recordNode;
        }
        
        /**
         * @param Request $request
         * @param         $doc
         * @param         $xpath
         * @param         $rootNode
         */
        private function setResponseXMLElements_Identify( Request &$request, &$doc, &$xpath, &$rootNode){
                
                $params = $request->query->all();
                if(count($params) > 1){
                        $parentNode = $rootNode->parentNode;
                        $rootNode->parentNode->removeChild($rootNode);
                        $this->tools->DOMAppendChild('error', '', ['code' => 'badArgument'], $doc, $parentNode);
                        return;
                }
                
                $baseUrl = Url::fromRoute('publisso_gold.oai');
                $baseUrl->setAbsolute();
                
		//frühestes Buchkapitel mit DOI als Beispiel heraussuchen
		$res = \Drupal::database()->select('rwPubgoldBookChapters', 't')->fields('t', ['cp_doi', 'cp_published'])->isNotNull('cp_doi')->orderBy('cp_published', 'ASC')->execute()->fetchAssoc();
		$date = $res['cp_published'];
		$doi = end(explode('/', $res['cp_doi']));

		$this->tools->DOMAppendChild('repositoryName', 'PUBLISSO OAI Repository', [], $doc, $rootNode);
                #$this->tools->DOMAppendChild('baseURL', $request->getSchemeAndHttpHost().$request->getPathInfo(), [], $doc, $rootNode);
                $this->tools->DOMAppendChild('baseURL', $baseUrl->toString(), [], $doc, $rootNode);
		$this->tools->DOMAppendChild('protocolVersion', '2.0', [], $doc, $rootNode);
		$this->tools->DOMAppendChild('adminEmail', $this->texts->get('oai.admin.email'), [], $doc, $rootNode);
		$this->tools->DOMAppendChild('earliestDatestamp', $date, [], $doc, $rootNode);
		$this->tools->DOMAppendChild('deletedRecord', 'transient', [], $doc, $rootNode);
		$this->tools->DOMAppendChild('granularity', 'YYYY-MM-DD', [], $doc, $rootNode);

		foreach($request->getEncodings() as $encoding){
			$this->tools->DOMAppendChild('compression', $encoding, [], $doc, $rootNode);
		}

		$descNode = $this->tools->DOMAppendChild('description', '', [], $doc, $rootNode);
		$oaiNode = $this->tools->DOMAppendChild(
			'oai-identifier',
			'',
			[
				'xmlns' 		=> 'http://www.openarchives.org/OAI/2.0/oai-identifier',
				'xmlns:xsi' 		=> 'http://www.w3.org/2001/XMLSchema-instance',
				'xsi:schemaLocation' 	=> 'http://www.openarchives.org/OAI/2.0/oai-identifier http://www.openarchives.org/OAI/2.0/oai-identifier.xsd'
			],
			$doc,
			$descNode,
			false
		);

		$this->tools->DOMAppendChild('scheme', 'oai', [], $doc, $oaiNode);
		$this->tools->DOMAppendChild('repositoryIdentifier', 'oai.publisso.de', [], $doc, $oaiNode);
		$this->tools->DOMAppendChild('delimiter', ':', [], $doc, $oaiNode);
		$this->tools->DOMAppendChild('sampleIdentifier', 'oai:oai.publisso.de:BC!DOI!'.$doi, [], $doc,
			$oaiNode);
		$descNode = $this->tools->DOMAppendChild('description', '', [], $doc, $rootNode);
		$eprintsNode = $this->tools->DOMAppendChild(
			'eprints',
			'',
			[
				'xmlns' 		=> 'http://www.openarchives.org/OAI/1.1/eprints',
				'xmlns:xsi' 		=> 'http://www.w3.org/2001/XMLSchema-instance',
				'xsi:schemaLocation' 	=> 'http://www.openarchives.org/OAI/1.1/eprints http://www.openarchives.org/OAI/1.1/eprints.xsd'
			],
			$doc,
			$descNode,
			false
		);

		$contentNode = $this->tools->DOMAppendChild('content', '', [], $doc, $eprintsNode);
		$this->tools->DOMAppendChild('text', $this->texts->get('oai.identifier.description.text'), [], $doc, $contentNode);
		$this->tools->DOMAppendChild('URL', $this->texts->get('oai.identifier.description.url'), [], $doc, $contentNode);
		$mdPolicyNode = $this->tools->DOMAppendChild('metadataPolicy', '', [], $doc, $eprintsNode);
		$this->tools->DOMAppendChild('text', $this->texts->get('oai.identifier.description.metadata-policy'), [], $doc, $mdPolicyNode);
		$dataPolicyNode = $this->tools->DOMAppendChild('dataPolicy', '', [], $doc, $eprintsNode);
		$this->tools->DOMAppendChild('text', $this->texts->get('oai.identifier.description.data-policy'), [], $doc, $dataPolicyNode);
	}
        
        /**
         * @return string
         */
        public function getHTMLNavigation(){
	        return '<h1>OAI</h1>
                        <div class="navigation_bar">
                                <span class="navigation_item"> <a href="/publisso_gold/oai/request/Identify">Identify</a>
                                </span> <span class="navigation_item"> <a href="/publisso_gold/oai/request/GetRecord">GetRecord</a>
                                </span> <span class="navigation_item"> <a href="/publisso_gold/oai/request/ListIdentifiers">ListIdentifiers</a>&nbsp;(<a
                                        href="/publisso_gold/oai/request/ListIdentifiersResumption">Resumption</a>)
                                </span> <span class="navigation_item"> <a href="/publisso_gold/oai/request/ListMetadataFormats">ListMetadataFormats</a>
                                </span> <span class="navigation_item"> <a href="/publisso_gold/oai/request/ListRecords">ListRecords</a>&nbsp;(<a
                                        href="/publisso_gold/oai/request/ListRecordsResumption">Resumption</a>)
                                </span> <span class="navigation_item"> <a href="/publisso_gold/oai/request/ListSets">ListSets</a>&nbsp;(<a href="/publisso_gold/oai/request/ListSetsResumption">Resumption</a>)
                                </span>
                        </div>';
        }
        
        /**
         * @param $id
         *
         * @return Response
         */
        private function chapter($id){
		$response = new Response();
		$chapter = new Bookchapter($id);
		$book = $chapter->getMedium();
		
		$doc = new \DOMDocument('1.0', 'UTF-8');
		$xslNode = $doc->createProcessingInstruction('xml-stylesheet', 'type="text/xsl" href="/'.\Drupal::service('module_handler')->getModule('publisso_gold')->getPath().'/var/oai_chapter.xsl'.'"');
		$doc->appendChild($xslNode);
		$xpath = new \DOMXPath($doc);
		$doc->formatOutput = true;
		
		$this->tools->DOMAppendChild(
				'OAI-PMH',
				'',
				[
					'xmlns' 		=> 'http://www.openarchives.org/OAI/2.0/',
					'xmlns:xsi' 		=> 'http://www.w3.org/2001/XMLSchema-instance',
					'xsi:schemaLocation' 	=> 'http://www.openarchives.org/OAI/2.0/  http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd'
				],
				$doc,
				$xpath->evaluate('/')[0],
				false
		);
		
		date_default_timezone_set("UTC");
		
		$this->tools->DOMAppendChild('responseDate',  date("c"), [], $doc, $xpath->evaluate('/OAI-PMH')[0]);
		
		$currentUrl = \Drupal::routeMatch()->getRouteName();
		$currentParams = \Drupal::routeMatch()->getRawParameters()->all();
		$url = Url::fromRoute($currentUrl, $currentParams);
		$url->setAbsolute();
		$this->tools->DOMAppendChild('request', $url->toString() , ['verb' => 'GetRecord', 'identifier' => 'chapter:'.$chapter->getElement('id'), 'metadataPrefix' => 'oai_dc'], $doc, $xpath->evaluate('/OAI-PMH')[0]);
		
		$this->tools->DOMAppendChild('GetRecord', '' , [], $doc, $xpath->evaluate('/OAI-PMH')[0]);
		$this->tools->DOMAppendChild('record', '' , ['xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance', 'xmlns:dimdi' => 'http://www.dimdi.de/database'], $doc, $xpath->evaluate('/OAI-PMH/GetRecord')[0]);
		
// 		//add header-section
 		$this->tools->DOMAppendChild('header', '', [], $doc, $xpath->evaluate('/OAI-PMH/GetRecord/record')[0]);
		
// 		//Identifier
                if(!empty($chapter->getElement('identifier'))) {
                        $identifier = 'oai:oai.publisso.de:BC!DOI!'.$chapter->getElement('doi');
                }
                else{
                        $identifier = 'oai:oai.publisso.de:BC!IID!'.$chapter->getElement('id');
                }
                
                $this->tools->DOMAppendChild( 'identifier', $identifier, [], $doc, $xpath->evaluate( '/OAI-PMH/GetRecord/record/header' )[ 0 ] );
		
// 		//dateStamp
 		$this->tools->DOMAppendChild('datestamp', $chapter->getElement('published'), [], $doc, $xpath->evaluate('/OAI-PMH/GetRecord/record/header')[0]);
		
// 		//doc-type
 		$this->tools->DOMAppendChild('setSpec', 'doc-type:bookPart', [], $doc, $xpath->evaluate('/OAI-PMH/GetRecord/record/header')[0]);
		
// 		//open_access
 		$this->tools->DOMAppendChild('setSpec', 'open_access', [], $doc, $xpath->evaluate('/OAI-PMH/GetRecord/record/header')[0]);
		
// 		//chapter-ddc
                if(!empty($chapter->getElement('ddc')))
 		        $this->tools->DOMAppendChild('setSpec', 'ddc:'.$chapter->getElement('ddc'), [], $doc, $xpath->evaluate('/OAI-PMH/GetRecord/record/header')[0]);
		
// 		//status-type
 		$this->tools->DOMAppendChild('setSpec', 'status-type:'.(empty($chapter->getElement('previous_version')) ? 'publishedVersion' : 'updatedVersion'), [], $doc, $xpath->evaluate('/OAI-PMH/GetRecord/record/header')[0]);
		
// 		//add metadata-section
 		$this->tools->DOMAppendChild('metadata', '', [], $doc, $xpath->evaluate('/OAI-PMH/GetRecord/record')[0]);
		
// 		//namespace
  		$this->tools->DOMAppendChild(
  				'oai_dc:dc',
  				'',
  				[
  					'xsi:schemaLocation' 	=> "http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd",
  					'xmlns:dc' 		=> "http://purl.org/dc/elements/1.1/",
  					'xmlns:oai_dc' 		=> "http://www.openarchives.org/OAI/2.0/oai_dc/"
  				],
  				$doc,
  				$xpath->evaluate('/OAI-PMH/GetRecord/record/metadata')[0]
  		);
		
  		$doc->loadXML($doc->saveXML());
  		$doc->formatOutput = true;
  		$xpath = new \DOMXpath($doc);
  		$xpath->registerNamespace('oai_dc', 'http://www.openarchives.org/OAI/2.0/oai_dc/');
  		
  		$dc = $xpath->query('//oai_dc:dc')[0];
  		
  		//(dc:title)
  		$this->tools->DOMAppendChild('dc:title', $chapter->getElement('title'), [], $doc, $dc);
  		
  		//(dc:creator)
  		$authors = [];
  		foreach($chapter->readAuthors() as $author){
  			$authors[] = implode(', ', array_filter([$author->profile->getElement('lastname'), substr($author->profile->getElement('firstname'), 0, 1)]));
  		}
  		
  		foreach(array_filter(json_decode($chapter->getelement('corporation'))) as $corporation){
  			$authors[] = $corporation;
  		}
  		
  		foreach($authors as $_ => $author){
  			$this->tools->DOMAppendChild('dc:creator', $author, [], $doc, $dc);
  		}
  		
  		//(dc:relations)
  		$references = array_filter(explode("\n", base64_decode($chapter->getElement('references'))));
  		foreach($references as $_ => $reference){
  			$reference = trim(preg_replace('/^\[\d+?\]/', '', strip_tags(trim($reference))));
  			$this->tools->DOMAppendChild('dc:relation', $reference, [], $doc, $dc);
  		}
  		
  		//(dc:description)
  		if(!empty($chapter->getElement('abstract')))
  			$this->tools->DOMAppendChild('dc:description', preg_replace('/&#(\d+?);|\r|\n/', '', html_entity_decode(str_replace('"', '\"', strip_tags(base64_decode($chapter->getElement('abstract')))))), [], $doc, $dc);
  		
  		//(dc:publisher)
  		$this->tools->DOMAppendChild('dc:publisher', implode(';', array_filter([$book->getElement('publisher'), $book->getElement('publication_place')])), [], $doc, $dc);
  		
  		//(dc:coverage)
  		if(!empty($book->getElement('ddc')))
  			$this->tools->DOMAppendChild('dc:coverage', 'ddc:'.$book->getElement('ddc'), [], $doc, $dc);
  		
  		//(dc:language)
  		if(!empty($book->getElement('language')))
  			$this->tools->DOMAppendChild('dc:language', $book->getElement('language'), [], $doc, $dc);
  		
  		$this->tools->DOMAppendChild('dc:date', $chapter->getElement('published'), [], $doc, $dc);
  		
  		//(dc:identifier)
  		if(!empty($chapter->getElement('doi')))
  			$this->tools->DOMAppendChild('dc:identifier', $chapter->getElement('doi'), [], $doc, $dc);
  		
  		//(dc:license)
  		if(!empty($chapter->getElement('license'))){
  			
  			$license = new License($chapter->getElement('license'));
  			$this->tools->DOMAppendChild('dc:rights', $license->getElement('url'), [], $doc, $dc);
  		}
  		
  		//(dc:subject)
  		$subject = json_decode($chapter->getElement('keywords'));
  		$subject[] = 'ddc:'.$book->getElement('ddc');
  		
  		foreach(array_filter($subject) as $_ => $kw){
  			$this->tools->DOMAppendChild('dc:subject', $kw, [], $doc, $dc);
  		}
  		
  		$this->tools->DOMAppendChild('dc:type', "text", [], $doc, $dc);
  		$this->tools->DOMAppendChild('dc:type', "bookPart", [], $doc, $dc);
  		$this->tools->DOMAppendChild('dc:identifier', 'http://dx.doi.org/'.$chapter->getElement('doi'), [], $doc, $dc);
  		
  		$url = $chapter->getChapterLink('url');
  		$url->setAbsolute();
  		$this->tools->DOMAppendChild('dc:identifier', $url->toString(), [], $doc, $dc);
  		
  		$this->tools->DOMAppendChild('dc:source', 'Chapter of '.$book->getElement('title'), [], $doc, $dc);
		
		$response->setcontent($doc->saveXML());
		
		$doc->formatOutput = true;
		$response->headers->set('Content-Type', 'text/xml');
		$response->setContent($doc->saveXML());
		return $response;
	}
}
?>
