<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\Volume.
 */

namespace Drupal\publisso_gold\Controller;
        
        /**
         * Class Volume
         * @package Drupal\publisso_gold\Controller
         */
        class Volume {
    
        private $id      ;
        private $elements;
        private $table   ;
        private $issues  ;
        private $articles;
        
        /**
         * @return string
         */
        public function __toString(){
                return "class volume";
        }
        
        /**
         * Volume constructor.
         * @param null $id
         */
        public function __construct($id = null){
            
                $this->id = $id;
                $this->elements = (object)[];
                $this->table = 'rwPubgoldJournalVolume';
                
                if($id) $this->load($id);
        }
        
        /**
         * @param $jrn_id
         * @param $year
         * @param $number
         * @param array $elements
         * @throws \Exception
         */
        public function create($jrn_id, $year, $number, $elements = []){
                
                $qry = \Drupal::database()->select($this->table, 't')->fields('t', ['id'])->condition('year', $year, '=')->condition('number', $number, '=')->condition('jrn_id', $jrn_id, "=");
                $res = $qry->countQuery()->execute()->fetchField();
                
                if($res){
                        $this->id = $qry->execute()->fetchField();
                }
                else{
                
                        $this->id = \Drupal::database()->insert($this->table)->fields(['jrn_id' => $jrn_id, 'year' => $year, 'number' => $number])->execute();
                        $this->load($this->id);
                        
                        foreach($elements as $k => $v){
                                $this->setElement($k, $v, false);
                        }
                        $this->save();
                }
                
                $this->load();
        }
        
        /**
         * @param null $id
         */
        public function load($id = null){
                
                if($this->id) $id = $this->id;
                $res = \Drupal::database()->select($this->table, 't')->fields('t', [])->condition('id', $id, '=')->execute()->fetchAssoc();
                
                foreach($res as $k => $v){
                        if($k != 'id') $this->elements->$k = $v;
                }
                
                $this->id = $id;
                
                $this->loadArticles();
                $this->loadIssues();
        }
        
        private function loadIssues(){
                $this->issues = [];
                $this->issues = \Drupal::database()->select('rwPubgoldJournalIssue', 't')
                                                    ->fields('t', ['id'])
                                                    ->condition('vol_id', $this->id, '=')
                                                    ->orderBy('number', 'ASC')
                                                    ->execute()
                                                    ->fetchCol();
        }
        
        public function getIssues(){
                return $this->issues;
        }
        
        /**
         * @param $iss_id
         * @return Issue|false
         */
        public function getIssue($iss_id){
                if(!in_array($iss_id, $this->issues)) return false;
                return new \Drupal\publisso_gold\Controller\Issue($iss_id);
        }
        
        /**
         * @return bool
         */
        public function hasIssues(){
                return !!count($this->issues);
        }
        
        /**
         * @param $id
         * @return bool
         */
        public function hasIssue($id){
                return in_array($id, $this->issues);
        }
        
        /**
         * @return mixed
         */
        public function getCurrentIssueID(){
                return \Drupal::database()->select('rwPubgoldJournalIssue', 't')
                                          ->fields('t', ['id'])
                                          ->condition('vol_id', $this->id, '=')
                                          ->orderBy('number', 'DESC')
                                          ->execute()
                                          ->fetchField();
        }
        
        /**
         * @return Issue
         */
        public function getCurrentIssue(){
                return new \Drupal\publisso_gold\Controller\Issue(
                        $this->getCurrentIssueID()
                );
        }
        
        private function loadArticles(){
                $this->articles = [];
                $this->articles = \Drupal::database()->select('rwPubgoldJournalArticles', 't')
                                                    ->fields('t', ['jrna_id'])
                                                    ->condition('jrna_volume', $this->id, '=')
                                                    ->isNull('jrna_issue')
                                                    ->orderBy('jrna_id', 'ASC')
                                                    ->execute()
                                                    ->fetchCol();
        }
        
        public function getArticles(){
                return $this->articles;
        }
        
        /**
         * @return bool
         */
        public function hasArticles(){
                return !!count($this->articles);
        }
        
        /**
         * @param $jrna_id
         * @return bool
         */
        public function hasArticle($jrna_id){
                return in_array($jrna_id, $this->articles);
        }
        
        /**
         * @return bool
         */
        public function hasAnyArticles(){
                return !!\Drupal::database()->select('rwPubgoldJournalArticles', 't')
                                                    ->fields('t', ['jrna_id'])
                                                    ->condition('jrna_volume', $this->id, '=')
                                                    ->countQuery()
                                                    ->execute()
                                                    ->fetchField();
        }
        
        /**
         * @param $jrna_id
         * @return Journalarticle
         */
        public function getArticle($jrna_id){
                return new \Drupal\publisso_gold\Controller\Journalarticle($jrna_id);
        }
        
        /**
         * @param $name
         * @param $value
         * @param bool $autosave
         */
        public function setElement($name, $value, $autosave = true){
                
                if(property_exists($this->elements, $name))
                        $this->elements->$name = $value;
                
                if($autosave) $this->save();
        }
        
        private function save(){
                \Drupal::database()->update($this->table)->fields((array)$this->elements)->condition('id', $this->id, '=')->execute();
        }
        
        /**
         * @param $name
         * @return false|mixed|null
         */
        public function getElement($name){
                if($name == 'id') return $this->id;
                if(property_exists($this->elements, $name)) return $this->elements->$name;
                return false;
        }
        
        /**
         * @param $id
         */
        public function delete($id){
                
                \Drupal::database()->delete($this->table)->condition('id', $id, '=')->execute();
        }
        
        /**
         * @return mixed|null
         */
        public function getId(){
                return $this->id;
        }
        
        /**
         * @return array
         */
        public function getKeys(){
                return get_object_vars($this->elements);
        }
}

?>
