<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\TableOfContent.
 */
namespace Drupal\publisso_gold\Controller;
        
        /**
         * Class TableOfContent
         * @package Drupal\publisso_gold\Controller
         */
        class TableOfContent {
        private $id = false;
        private $elements = [ ];
        private $tbl_column_prefix = 'toc_';
        private $items = [ ];
        private $structure = [];
        private $debug;
        
        /**
         * @return string
         */
        public function __toString() {
                return "class TableOfContent";
        }
        
        /**
         * TableOfContent constructor.
         * @param $id
         * @param array $medium
         * @param false $debug
         */
        public function __construct($id, $medium = [], $debug = false) {
                $this->debug = $debug;
                if (($id || (array_key_exists ( 'medium', $medium ) && array_key_exists ( 'id', $medium )))) {
                        if ($id) {
                                $this->load ( [
                                        'id' => $id
                                ] );
                        } else {
                                $this->load ( [
                                        'medium' => $medium ['medium'],
                                        'medium_id' => $medium ['id'],
                                        'lang' => $medium['lang'] ?? null
                                ] );
                        }
                }
        }
        
        /**
         * @param array $condition
         * @return false
         */
        private function load($condition = []) {
                // echo '<pre>'.print_r('[in_class] loading toc....', 1).'</pre>';
                $select = \Drupal::database ()->select ( 'rwPubgoldTableOfContent', 'toc' )->fields ( 'toc', [ ] );
                $group = $select->andConditionGroup ();
                
                foreach ( $condition as $k => $v ) {

                        if($v !== null) {
                                $group->condition($this->tbl_column_prefix . $k, $v, '=');
                        }
                        else{
                                $group->isNull($this->tbl_column_prefix . $k);
                        }
                }

                $result = $select->condition ( $group )->execute ()->fetchAssoc ();
                
                if (! (is_array($result) && count ( $result )))
                        return false;
                if(is_array($result)) {
                        foreach ( $result as $k => $v ) {
                
                                if ( $k != $this->tbl_column_prefix . 'id' ) // store except id
                                        $this->elements [ str_replace( $this->tbl_column_prefix, '', $k ) ] = $v;
                                else
                                        $this->id = $v;
                        }
                }
                // echo '<pre>'.print_r('[in_class] toc loaded....', 1).'</pre>';
                
                // echo '<pre>'.print_r('[in_class] loading toc-items....', 1).'</pre>';
                $this->loadItems ();
                // echo '<pre>'.print_r('[in_class] toc-items loaded....', 1).'</pre>';
                
                // echo '<pre>'.print_r('[in_class] creating toc-structure....', 1).'</pre>';
                // $this->createStructure();
                // echo '<pre>'.print_r($this->structure, 1).'</pre>'; exit();
                $this->structure = unserialize ( ($this->getElement ( 'structure' )) );
                if(!$this->structure) $this->structure = [];
                if ($this->getElement ( 'rebuild' )) {
                        $this->recreateStructure ();
                        $this->setElement ( 'rebuild', 0 );
                        $this->load ( [
                                'id' => $this->id
                        ] );
                }
                // echo '<pre>Class Structure: '.print_r(unserialize(json_decode($this->getElement('structure'), true)), 1).'</pre>';
        }
        
        /**
         * @param $item
         * @param $key
         */
        private function assignItemToStructure($item, $key) {
        }
        private function loadItems() {
                $this->items = [ ];
                
                $result = \Drupal::database ()->select ( 'rwPubgoldTableOfContentItem', 'toci' )->fields ( 'toci', [
                        'toci_id'
                ] )->condition ( 'toci_tocid', $this->id, '=' )->orderBy ( 'toci_parent', 'ASC' )->orderBy ( 'toci_weight', 'ASC' )->execute ()->fetchAll ();
                
                foreach ( $result as $_ ) {
                        $this->items [] = new TableOfContentItem ( $_->toci_id );
                }
        }
        public function recreateStructure() {
                $this->loadItems ();
                
                $this->createStructure ();
                $this->setElement ( 'structure', serialize ( $this->structure ) );
                $this->save ();
        }
        private function createStructure() {
                $this->structure = [ ];
                
                foreach ( $this->items as $item ) {
                        
                        if (empty ( $item->getElement ( 'parent' ) )) {
                                
                                $this->structure [] = [
                                        'item' => $item,
                                        'childs' => [ ]
                                ];
                        } else {
                                
                                $this->addChild ( $item, $this->structure, 1 );
                        }
                }
        }
        
        /**
         * @param $item
         * @param $array
         * @param $level
         */
        private function &addChild($item, &$array, $level) {
                foreach ( $array as $k => $v ) {
                        
                        if ($v ['item']->getElement ( 'id' ) == $item->getElement ( 'parent' )) {
                                
                                $item->level = $level;
                                
                                $array [$k] ['childs'] [] = [
                                        'item' => $item,
                                        'childs' => [ ]
                                ];
                                
                                break;
                        }
                        
                        if (is_array ( $v ['childs'] )) {
                                $this->addChild ( $item, $array [$k] ['childs'], $level + 1 );
                        }
                }
        }
        
        /**
         * @param $elemName
         * @return bool|mixed|null
         */
        public function getElement($elemName) {
                if ($elemName == 'id')
                        return $this->id;
                return $this->elements [$elemName] ?? null;
        }
        
        /**
         * @param null $elemName
         * @return \Drupal\Core\Database\StatementInterface|int|string|null
         */
        public function save($elemName = null) {
                $fields = $this->elements;
                
                if ($elemName !== null) { // if specific element selected
                        $fields = [
                                $elemName => $fields [$elemName]
                        ];
                }
                // echo '<pre>'.print_r($fields, 1).'</pre>'; exit();
                foreach ( $fields as $k => $var ) {
                        $fields [$this->tbl_column_prefix . $k] = $var;
                        unset ( $fields [$k] );
                }

                try {
                        return \Drupal::database()->update('rwPubgoldTableOfContent')->fields($fields)->condition($this->tbl_column_prefix . 'id', $this->id)->execute();
                }
                catch(\Exception $e){
                        \Drupal::service('messenger')->addError('Can\'t save TOC');
                        error_log($e->getMessage());
                        return null;
                }
        }
        
        /**
         * @param $elemName
         * @param null $elemValue
         * @param bool $storeOnDB
         * @return bool
         */
        public function setElement($elemName, $elemValue = null, $storeOnDB = true) {
                if (array_key_exists ( $elemName, $this->elements )) {
                        
                        $oldValue = $this->elements [$elemName];
                        $this->elements [$elemName] = $elemValue;
                        
                        if ($storeOnDB === false)
                                return true;
                        
                        if ($this->save ( $elemName ) === false) {
                                $this->elements [$elemName] = $oldValue;
                                return false;
                        } else {
                                return true;
                        }
                } else {
                        return false;
                }
        }
        
        /**
         * @return array
         */
        public function readItems() {
                return $this->items;
        }
        
        /**
         * @return array
         */
        public function readStructure() {
                return $this->structure;
        }
        public function storeStructure() {
                $this->loadItems ();
                $this->createStructure ();
                $this->setElement ( 'structure', (serialize ( $this->structure )) );
                // echo serialize($this->structure); exit();
        }
        
        /**
         * @return array
         */
        public function getStructure() {
                return $this->structure;
        }
}

?>
