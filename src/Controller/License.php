<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\License.
 */

namespace Drupal\publisso_gold\Controller;
        
        /**
         * Class License
         * @package Drupal\publisso_gold\Controller
         */
        class License {
    
        private $id      ;
        private $elements;
        private $table   ;
        
        /**
         * @return string
         */
        public function __toString(){
                return "class license";
        }
        
        /**
         * License constructor.
         * @param null $id
         */
        public function __construct($id = null){
            
                $this->id = $id;
                $this->elements = (object)[];
                $this->table = 'rwPubgoldLicenses';
                
                if($id) $this->load($id);
        }
        
        /**
         * @param $name
         * @param array $elements
         * @return License
         * @throws \Exception
         */
        public function create($name, $elements = []){
                
                $qry = \Drupal::database()->select($this->table, 't')->fields('t', ['id'])->condition('name', $name, '=');
                $res = $qry->countQuery()->execute()->fetchField();
                
                if($res){
                        $this->id = $qry->execute()->fetchField();
                }
                else{
                
                        $this->id = \Drupal::database()->insert($this->table)->fields(['name' => $name])->execute();
                        $this->load($this->id);
                        
                        foreach($elements as $k => $v){
                                $this->setElement($k, $v, false);
                        }
                        
                        $this->save();
                }
                
                $this->load();
                
                return new self($this->id);
        }
        
        /**
         * @param null $id
         */
        public function load($id = null){
                
                
                if($this->id) $id = $this->id;
                
                $field = preg_match('/^\d+$/', $id) ? 'id' : 'name';
                $res = \Drupal::database()->select($this->table, 't')->fields('t', [])->condition($field, $id, '=')->execute()->fetchAssoc();
                
                foreach($res as $k => $v){
                        if($k != 'id') $this->elements->$k = $v;
                }
                
                $this->id = $id;
        }
        
        /**
         * @param $url
         */
        public function loadByUrl($url){
                
                $res = \Drupal::database()->select($this->table, 't')->fields('t', [])->condition('url', $url, '=')->execute()->fetchAssoc();
                
                foreach($res as $k => $v){
                        if($k != 'id') $this->elements->$k = $v;
                }
                
                $this->id = $res['id'] ? $res['id'] : null;
        }
        
        /**
         * @param $name
         * @param $value
         * @param bool $autosave
         */
        public function setElement($name, $value, $autosave = true){
                
                if(property_exists($this->elements, $name))
                        $this->elements->$name = $value;
                
                if($autosave) $this->save();
        }
        
        private function save(){
                \Drupal::database()->update($this->table)->fields((array)$this->elements)->condition('id', $this->id, '=')->execute();
        }
        
        /**
         * @param $name
         * @return false
         */
        public function getElement($name){
                if(property_exists($this->elements, $name)) return $this->elements->$name;
                return false;
        }
        
        /**
         * @param array $attrs
         * @return string
         */
        public function getInlineImage(array $attrs = []){
                $link = '<img src="data:'.$this->getElement('image_type').';base64,'.base64_encode($this->getElement('image')).'"';
                foreach($attrs as $key => $val) $link .= " $key=\"$val\"";
                $link .= '>';
                return $link;
        }
        
        /**
         * @return string
         */
        public function getInlineImageData(){
                $link = 'data:'.$this->getElement('image_type').';base64,'.base64_encode($this->getElement('image'));
                return $link;
        }
        
        /**
         * @return false
         */
        public function getImageBin(){
                return $this->getElement('image');
        }
        
        /**
         * @param string $text
         * @param string $target
         * @return string
         */
        public function getLink($text = '', $target = "_self"){
                return '<a href="'.$this->getElement('url').'" target="'.$target.'">'.$text.'</a>';
        }
        
        /**
         * @param array $imageAttrs
         * @param string $target
         * @return string|string[]
         */
        public function getImageLink($imageAttrs = [], $target = '_self'){
                return str_replace('[IMAGE]', $this->getInlineImage($imageAttrs), $this->getLink('[IMAGE]', $target));
        }
        
        /**
         * @param $id
         */
        public function delete($id){
                
                \Drupal::database()->delete($this->table)->condition('id', $id, '=')->execute();
        }
        
        /**
         * @return mixed|null
         */
        public function getId(){
                return $this->id;
        }
        
        /**
         * @return array
         */
        public function getKeys(){
                return get_object_vars($this->elements);
        }
}

?>
