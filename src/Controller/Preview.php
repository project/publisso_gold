<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\Preview.
 */

namespace Drupal\publisso_gold\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\publisso_gold\Form\publisso_goldLogin;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Routing;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\Entity;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Symfony\Component\HttpFoundation\Response;
use Drupal\menu_link_content;
use Drupal\Core\Menu\MenuLinktree;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Render\FormattableMarkup;

//require_once(drupal_get_path('module', 'publisso_gold').'/inc/constants.inc.php') || die('Cant require Constants');

/**
 * Class Preview
 * @package Drupal\publisso_gold\Controller
 */
class Preview extends ControllerBase{
       
        private $tmpl = '';
        private $tmpl_vars = array();
        private $modpath;
        private $userrole = 'guest';
        private $user = 'guest';
        private $user_id = NULL;
        private $userweight = 10;
        private $login_form;
        private $route_name;
        private $modname = 'publisso_gold';
        private $database;
        private $force_redirect = NULL;
        private $force_redirect_data = [];
        private $page_title;
        private $session;
        private $medium_id;
        protected $currentUser = NULL;
        
        const __ACCESS_DENIED__             = 'You can not access this page. You may need to log in first.';
        
        /**
         * @param ContainerInterface $container
         * @return Preview|static
         */
        public static function create(ContainerInterface $container){
                \Drupal::service('page_cache_kill_switch')->trigger();
                return new static($container->get('database'));
        }
        
        /**
         * @return array
         */
        public function temp(){
                
                $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
                $data = $tempstore->get('preview_bookchapter');
                $data = unserialize($data);
                
                $workflow = new \Drupal\publisso_gold\Controller\Workflow();
                $workflow->makeTemporary($data);
                return $this->main(null, $workflow);
        }
        
        /**
         * @param null $wf_id
         * @param null $workflow
         * @return array
         */
        public function main($wf_id = null, $workflow = null) {
                
                $session = \Drupal::service('session');
                if($wf_id) $workflow = new Workflow($wf_id, 'wf_', false);
                
                if($session->get('user')['id'] != $workflow->getElement('locked_by_uid') || empty($session->get('user')['id'])){
                        
                        return array(
                                '#type' => 'markup',
                                '#markup' => (string)t(self::__ACCESS_DENIED__),
                                '#attached' => [
                                        'library' => [
                                        'publisso_gold/default'
                                        ]
                                ],
                                '#cache' => [
                                        'max-age' => 0
                                ]
                        );
                }
                
                $template_site    = new Template();
                $template_head    = new Template();
                $template_content = new Template();
                
                $template_site->get('preview_workflow');
                $template_site->setVar('medium_head'   , '');
                $template_site->setVar('medium_content', '');
                
                switch(substr($workflow->getElement('type'), 0, 1)){
                        
                        case 'b':
                                $medium = new Book($workflow->getDataElement('bk_id'));
                                $this->medium_id = $medium->getElement('id');
                                $template_head   ->get('preview_workflow_book_head'   );
                                $template_content->get('preview_workflow_book_content');
                                $this->fillTemplateBookHead($template_head, $medium);
                                $this->fillTemplateBookContent($template_content, $medium, $workflow);
                                break;
                        
                        case 'j':
                                $medium = new Journal($workflow->getDataElement('jrn_id'));
                                $this->medium_id = $medium->getElement('id');
                                $template_head   ->get('publisso_gold_journal_header'   );
                                $template_content->get('publisso_gold_journal_content');
                                $this->fillTemplateJournalHead($template_head, $medium);
                                $this->fillTemplateJournalContent($template_content, $medium, $workflow);
                                break;
                        
                        case 'c':
                                $medium = new Conference($workflow->getDataElement('cf_id'));
                                $this->medium_id = $medium->getElement('id');
                                $template_head   ->get('preview_workflow_conference_head'   );
                                $template_content->get('preview_workflow_conference_content');
                                break;
                }
                
                $template_site->appendToVar('medium_head'   , $template_head->parse()   );
                $template_site->appendToVar('medium_content', $template_content->parse());
                
                return array(
                        '#type' => 'inline_template',
                        '#template' => $template_site->parse(),
                        '#attached' => [
                                'library' => [
                                'publisso_gold/default'
                                ]
                        ],
                        '#cache' => [
                                'max-age' => 0
                        ]
                );
        }
        
        /**
         * @param $template
         * @param $medium
         */
        protected function fillTemplateBookHead(&$template, &$medium){
                
                $session = \Drupal::service('session');
                if(
                        $medium->hasAuthor($session->get('user')['id'])                           ||
                        ($medium->countAuthors() == 0 && $session->get('logged_in') === true)     ||
                        $session->get('user')['weight'] >= 70){
                                $template->setVar('lnk_publish_chapter', '
                                        <a href="/publisso_gold/book/'.$medium->getElement('id').'/chapter/add" class="link--calltoaction btn">
                                        <span>'.((string)t('Submit')).'</span>
                                        </a>
                                ');
                }
                
                if(\Drupal::service('publisso_gold.tools')->userHasAccessRight('bookmanagement', $session->get('user')['weight'])){
                        $template->setVar('lnk_edit_book', '
                                <a href="/publisso_gold/bookmanagement/book/'.$medium->getElement('id').'/edit/" class="link--calltoaction inline btn">
                                <span>'.((string)t('Edit book')).'</span>
                                </a>
                        ');
                        
                        $template->setVar('lnk_edit_book_eb', '
                                <a href="/publisso_gold/bookmanagement/set_editorialboard/'.$medium->getElement('id').'" class="link--calltoaction inline btn">
                                <span>'.((string)t('Edit Edit. Board')).'</span>
                                </a>
                        ');
                }
                
                $template->setVar('bk_id', $medium->getElement('id'));
                $template->setVar('book_title', $medium->getElement('title'));
                $template->setVar('lnk_cover', !empty($medium->getElement('cover_link')) ? $medium->getElement('cover_link') : '#');
                
                $template->setVar('book_editors', '');
                $template->setVar('book_authors', '');
                $template->appendToVar('book_editors', $medium->getElement('public_editors'));
                
                foreach($medium->readAuthors() as $author){
                
                        $template->appendToVar('book_authors',  (!empty($this->tmpl_vars['::book_authors::']) ? ', ' : '')
                                                                .  $author->profile->getElement('firstname')
                                                                .  ' '
                                                                .  $author->profile->getElement('lastname')
                        );
                }
                
                if(!empty($medium->getElement('isbn'))){
                        $template->setVar('li_item_book_isbn', '<li>ISBN: '.$medium->getElement('isbn').'</li>');
                }
                
                if(!empty($medium->getElement('issn'))){
                        $template->setVar('li_item_book_issn', '<li>ISBN: '.$medium->getElement('issn').'</li>');
                }
                
                if(!empty($medium->getElement('funding_name'))){
                        $template->setVar('li_item_book_funding_name', '<li>'.((string)t('Funding name')).': '.$medium->getElement('funding_name').'</li>');
                }
                
                if(!empty($medium->getElement('funding_number'))){
                        $template->setVar('li_item_book_funding_number', '<li>'.((string)t('Funding number')).': '.$medium->getElement('funding_number').'</li>');
                }
                
                $_ = [];
                foreach($medium->readEditorialBoard() as $uid => $user){
                        $_[] = implode(' ', [$user->profile->getElement('firstname'), $user->profile->getElement('lastname')]);
                }
                
                $template->setVar('li_item_book_editorial_board', '<li>'.implode('</li><li>', $_).'</li>');
                $template->setVar('li_item_funding_text', $medium->getElement('funding_text'));
        }
        
        /**
         * @param $template
         * @param $medium
         */
        protected function fillTemplateJournalHead(&$template, &$medium){
                
                $url_submit = Url::fromRoute('publisso_gold.journal_addarticle', ['jrn_id' => $medium->getElement('id')]);
                
                if($this->session->get('logged_in')){
                        $template->setVar('lnk_publish_article', '
                        <a href="'.$url_submit->toString().'" class="link--calltoaction">
                                <span>'.((string)t('Submit')).'</span>
                        </a>
                        ');
                }
                
                if(\Drupal::service('publisso_gold.tools')->userHasAccessRight('journalmanagement', $this->session->get('user')['weight'])){
                        
                        $url_edit = Url::fromRoute('publisso_gold.edit_journal', ['jrn_id' => $medium->getElement('id')]);
                        $template->setVar('lnk_edit_journal', '
                                <a href="'.$url_edit->toString().'" class="link--calltoaction inline">
                                <span>'.((string)t('Edit journal')).'</span>
                                </a>
                        ');
                
                        $template->setVar('lnk_edit_journal_eb', '
                                <a href="/publisso_gold/journalmanagement/set_editorialboard/'.$medium->getElement('id').'" class="link--calltoaction inline">
                                <span>'.((string)t('Edit Edit. Board')).'</span>
                                </a>
                        ');
                }
                
                $template->setVar('jrn_id', $medium->getElement('id'));
                $template->setVar('journal_title', $medium->getElement('title'));
                $template->setVar('journal_editors', []);
                $template->setVar('journal_editors', $medium->getElement('public_editors'));
                $template->setVar('journal_authors', '');
                
                if(is_array($medium->getElement('cover')) && count($medium->getElement('cover'))){
                        
                        $template->setVar('journal_cover', '');
                        
                        foreach($medium->getElement('cover') as $_){
                                if(!empty($template->getVar('journal_cover')))
                                        $template->setVar('journal_cover', '<br>'.$template->getVar('journal_cover'));
                                
                                $template->setVar('journal_cover', '<img class="img-responsive" src="/system/getPicture/ul/'.$_.'" alt="">'.$template->getVar('journal_cover'));
                        }
                }
                
                if(is_array($medium->getElement('logo')) && count($medium->getElement('logo'))){
                        
                        $template->setVar('journal_logo', '');
                        
                        foreach($medium->getElement('logo') as $_){
                        
                                if(!empty($template->getVar('journal_logo')))
                                        $template->setVar('journal_logo', '<br>'.$template->getVar('journal_logo'));
                                $url = Url::fromRoute('publisso_gold.getPicture', ['type' => 'ul', 'id' => $_]);
                                
                                $template->setVar('journal_logo', '<img class="img-responsive" src="/system/getPicture/ul/'.$_.'" alt="">'.$template->getVar('journal_logo'));
                        }
                }
                
                foreach($medium->readAuthors() as $author){
                
                $template->setVar('journal_authors', (!empty($template->getVar('journal_authors')) ? ', ' : '')
                                                        .  $author->profile->getElement('firstname')
                                                        .  ' '
                                                        .  $author->profile->getElement('lastname'));
                }
                
                if(!empty($medium->getElement('issn'))){
                        $template->setVar('li_item_book_issn', '<li>ISBN: '.$medium->getElement('issn').'</li>');
                }
                
                if(!empty($medium->getElement('funding_name'))){
                        $template->setVar('li_item_journal_funding_name', '<li>'.((string)t('Funding name')).': '.$medium->getElement('funding_name').'</li>');
                }
                
                $_ = [];
                foreach($medium->readEditorialBoard() as $uid => $user){
                        $_[] = implode(' ', [$user->profile->getElement('firstname'), $user->profile->getElement('lastname')]);
                }
                
                $template->setVar('li_item_journal_editorial_board', '<li>'.implode('</li><li>', $_).'</li>');
                $template->setVar('li_item_funding_text', '<li>'.$medium->getElement('funding_text').'</li>');
        }
        
        /**
         * @param $template
         * @param $medium
         * @param $workflow
         */
        protected function fillTemplateBookContent(&$template, &$medium, &$workflow){
                
                $chapter = new BookchapterTemp();
                $chapter->loadWorkflow($workflow);
                
                //echo '<pre>'.print_r($chapter, 1).'</pre>'; exit();
                $template->setVar('txt_about'                           , (string)t('About this Book')          );
                $template->setVar('txt_media'                           , (string)t('Media')                    );
                $template->setVar('txt_editorial_board'                 , (string)t('Editorial Board')          );
                $template->setVar('txt_authors'                         , (string)t('Authors')                  );
                $template->setVar('txt_manuscript_guidelines'           , (string)t('Manuscript Guidelines')    );
                $template->setVar('txt_imprint'                         , (string)t('Imprint')                  );
                $template->setVar('txt_overview_chapters'               , (string)t('Overview Chapters')        );
                $template->setVar('book_about_sidebar_blocks'           , ''                                    );
                $template->setVar('book_chapter_content_sidebar_blocks' , ''                                    );
                
                $_ = base64_decode($medium->getElement('about'));
		$template->setVar('book_about', $_);
		
		$_ = base64_decode($medium->getElement('manuscript_guidelines'));
		$template->setVar('book_manuscript_guidelines', $_);
		
		$_ = base64_decode($medium->getElement('imprint'));
		$template->setVar('book_imprint', $_);
		
		$template->setVar('book_funding_text', $medium->getElement('funding_text'));
		
		$toc = new TableOfContent(false, ['medium' => 'book', 'id' => $medium->getElement('id')], true);
		$template->setVar('book_chapter_list', $this->parseTOCStructure($toc->readStructure(), 1));
		
		$tmpl = new Template();
		/**
		 * Sustaining members
		 */
		
		if(count($medium->readSustainingMembers())){
                        
                        $tmpl->get('publisso_gold_book_content_sidebar_block');
                        
                        $tmpl->setVar('sbb_title', (string)t('Sustaining members'));
                        $tmpl->setVar('sbb_content', '');
                        $tmpl->setVar('view_name', 'partner-list');
                        
                        foreach($medium->readSustainingMembers() as $bsm_id){
                                
                                $tmpl->appendToVar('sbb_content', '<img src="/system/getPicture/bsm/'.$bsm_id.'"><br>');
                        }
                        
                        $template->setVar('book_about_sidebar_blocks', $tmpl->parse());
                }
                
                /* create template for authors
                *
                * use template book_content_author
                */
                $tmpl->get('publisso_gold_book_content_author');
                
                $tmpl->setVar('txt_more'        , (string)t('more')     );
                $tmpl->setVar('txt_address'     , (string)t('Address')  );
                $tmpl->setVar('txt_contact'     , (string)t('Contact')  );
                
                $content_authors = '';
                $c = 0;
                
                if(count($medium->readAuthors()) == 0){
                        $content_authors = '<p>Here the authors of the chapters will be listed.</p>';
                }
                else{
                        foreach($medium->readAuthors() as $user){
                                
                                if(!$user->profile->getElement('show_data_in_boards')) continue;
                                $tmpl->setVar('modal_id', 'm_authors_'.($c + 1));
                                $tmpl->setVar('author_name', $user->profile->getElement('graduation').' '.$user->profile->getElement('firstname').' '.$user->profile->getElement('lastname').' '.$user->profile->getElement('graduation_suffix'));
                                $tmpl->setVar('author_institute', $user->profile->getElement('institute'));
                                $tmpl->setVar('author_department', $user->profile->getElement('department'));
                                $tmpl->setVar('author_street', $user->profile->getElement('street'));
                                $tmpl->setVar('author_postal_code', $user->profile->getElement('postal_code'));
                                $tmpl->setVar('author_city', $user->profile->getElement('city'));
                                $tmpl->setVar('author_country', \Drupal::service('publisso_gold.tools')->getCountry($user->profile->getElement('country')));
                                $tmpl->setVar('author_telephone', $user->profile->getElement('telephone'));
                                $tmpl->setVar('author_email', $user->profile->getElement('email'));
                                $tmpl->setVar('author_pic_link', '/system/getPicture/up/'.$user->getId());
                                $tmpl->setVar('author_address', implode(', ', [implode(' ', [$tmpl->getVar('author_postal_code'), $tmpl->getVar('author_city')]), $tmpl->getVar('author_country')]));
                                
                                $_ = $tmpl->parse();
                                
                                if($c % 2 == 0){
                                        $_ = '<div class="row flex-row-sm">'.$_;
                                }
                                else{
                                        $_ .= '</div><hr>';
                                }
                                
                                $content_authors .= $_;
                                $c++;
                        }
                }
                
                if($c > 0 && $c % 2 != 0){
                        $content_authors .= '</div>';
                }
                
                $template->setVar('content_authors', $content_authors);
                /* =========================== */
                
                /* create template for editorial board
                 *
                 * use template book_content_author
                 * (like authors, though same var-names are used)
                 */
                $tmpl->get('publisso_gold_book_content_author');
                
                $content_eb = '';
                $c = 0;
                
                foreach($medium->readEditorialBoard() as $uid => $user){
                        
                        
                        
                        $tmpl->setVar('modal_id', 'm_editors_'.($c + 1));
                        
                        $tmpl->setVar('author_name', implode(' ', [$user->profile->getElement('graduation'), $user->profile->getElement('firstname'), $user->profile->getElement('lastname'), $user->profile->getElement('graduation_suffix')]));
                        $tmpl->setVar('author_institute', $user->profile->getElement('institute'));
                        $tmpl->setVar('author_department', $user->profile->getElement('department'));
                        $tmpl->setVar('author_street', $user->profile->getElement('street'));
                        $tmpl->setVar('author_notes', $user->tempInfo['eb_notes'].'<br><br>');
                        $tmpl->setVar('author_postal_code', $user->profile->getElement('postal_code'));
                        $tmpl->setVar('author_city', $user->profile->getElement('city'));
                        $tmpl->setVar('author_country', \Drupal::service('publisso_gold.tools')->getCountry($user->profile->getElement('country')));
                        $tmpl->setVar('author_telephone', $user->profile->getElement('telephone'));
                        $tmpl->setVar('author_email', $user->profile->getElement('email'));
                        $tmpl->setVar('author_pic_link', '/system/getPicture/up/'.$user->getId());
                        $tmpl->setVar('author_address', implode(', ', [implode(' ', [$tmpl->getVar('author_postal_code'), $tmpl->getVar('author_city')]), $tmpl->getVar('author_country')]));
                        
                        $_ = $tmpl->parse();
                        
                        if($c % 2 == 0){
                                $_= '<div class="row flex-row-sm">'.$_;
                        }
                        else{
                                $_.= '</div><hr>';
                        }
                        
                        $content_eb .= $_;
                        $c++;
                }
                
                if($c > 0 && $c % 2 != 0){
                        $content_eb .= '</div>';
                }
                
                $template->setVar('content_eb', $content_eb);
		
                /* =========================== */
                
                $tmplTab = new Template();
                $tmplContent = new Template();
                $tmplTab->get('publisso_gold_book_content_tab_chapter_content');
                $tmplContent->get('publisso_gold_book_content_content_chapter_content');
                
                $tmplTab->setVar('txt_chapter_content', (string)t('Chapter content'));
                $tmplTab->setVar('tab_chapter_content_class_active', 'active');
                
                $_abstract = base64_decode($workflow->getDataElement('abstract'));
                $_fulltext = base64_decode($workflow->getDataElement('chapter_text'));
                
                list($_fulltext, $_references) = \Drupal::service('publisso_gold.tools')->getReferencesFromText(base64_decode($workflow->getDataElement('chapter_text')));
                $inactiveReferences = \Drupal::service('publisso_gold.tools')->getInactiveReferences($_references, $workflow->getDataElement('references'));
                
                $inactiveReferences = mb_convert_encoding($inactiveReferences, 'utf-8', mb_detect_encoding($inactiveReferences, 'UTF-8, ISO-8859-1'));
                
                $_erratum  = $chapter->getElement('errata');
                $_references = implode('<br>', array_filter([nl2br($_references), nl2br($inactiveReferences)]));
                $tmplContent->setVar('content_chapter_content_class_active', 'active');
                
                $tmplContent->setVar('book_chapter_content', '
                        <div>
                <h1>
                                '.$workflow->getDataElement('title').'
                </h1>
                        </div>
                        ::authors::
                        <div>
                <div>'.(!empty($_abstract) ? '<h3>'.t('Abstract').'</h3>'.$_abstract . '<hr>' : '<br><br>').'</div>
                                <div>'.$_fulltext.'</div>
                                <div>'.(!empty($_references) ? '<hr><h2>'.((string)t('References')).'</h2>'.$_references : '').'</div>
                                <div>'.(!empty($_erratum) ? '<hr><h2>'.t('Erratum').'</h2>'.$_erratum : '').'</div>
                        </div>
                ');
                
                $authors         = [];
                $affiliations    = [];
                $strAuthors      = '';
                $strAffiliations = '';

                $chapter_authors = json_decode($workflow->getDataElement('authors'));
                
                foreach($chapter_authors as $_){
                        
                        $weight = $_->weight;
                        
                        while(array_key_exists($weight, $authors)) $weight++;
                        
                        $authors[$weight] = $_;
                }
                
                ksort($authors);
                
                foreach($authors as $_){
                        
                        if($_->uid && $_->profile_archive_id){
                                $user = new \Drupal\publisso_gold\Controller\User($_->uid);
                                $user->profile->loadSnapshot($_->profile_archive_id);
                                $affiliations[] = $user->profile->getAffiliation();
                        }
                        else{
                                $affiliations[] = $_->affiliation;
                        }
                }
                $affiliations = array_keys(array_flip($affiliations)); //make affs unique

                for($c = 1; $c <= count($affiliations); $c++){
                        if($affiliations[$c - 1]) $strAffiliations .= (!empty($strAffiliations) ? '<br>' : '') .'<sup>'.$c.'</sup>&nbsp;'.$affiliations[$c - 1];
                }

                foreach($authors as $_){
                        
                        if($_->uid && $_->profile_archive_id){
                                $user = new \Drupal\publisso_gold\Controller\User($_->uid);
                                $user->profile->loadSnapshot($_->profile_archive_id);
                                $affiliation = $user->profile->getAffiliation();
                        }
                        else{
                                $affiliation = $_->affiliation;
                        }
                        
                        $affKey = array_search($affiliation, $affiliations) + 1;
                        $strAuthors .= (!empty($strAuthors) ? '<br>' : '').implode(' ', [$_->firstname, $_->lastname]).(!empty($affKey) ? " <sup>$affKey</sup>" : "");
                }
                
                $tmplContent->setVar('authors', $strAuthors.'<br>'.implode('<br>', array_filter(json_decode($workflow->getDataElement('corporation'), 1))).'<br><br>'.$strAffiliations);
                $template->setVar('book_chapter_content', $template->getVar('authors'));

                $template->setVar('book_citation_note', $medium->getElement('citation_note'));

                //citation note
                $tmplSideblock = new Template();
                $tmplSideblock->get('publisso_gold_book_content_sidebar_block');
                $tmplSideblock->setVar('sbb_title', (string)t('Citation note'));
                $tmplSideblock->setVar('sbb_content', '');
                $tmplSideblock->setVar('view_name', 'citation-note');

                $cnstring = '~AUTHORLIST~. ~TITLE~. ~VERSION~In: ~CNSTRING~. ~PUBLICATIONPLACE~: ~PUBLISHER~; ~BEGINPUBLICATION~-.';

                //set title
                $cnstring = str_replace('~TITLE~', $workflow->getDataElement('title'), $cnstring);

                //set version
                $version = $workflow->getDataElement('version');

                if(!empty($version))
                        $cnstring = str_replace('~VERSION~', $version.'. ', $cnstring);
                else
                        $cnstring = str_replace('~VERSION~', '', $cnstring);

                //set cnstring
                $cnstring = str_replace('~CNSTRING~', $medium->getElement('citation_note'), $cnstring);

                //set begin publication
                $_ = !empty($workflow->getDataElement('publication_year')) ? $workflow->getDataElement('publication_year') : $medium->getElement('begin_publication');
                $cnstring = str_replace('~BEGINPUBLICATION~', $medium->getElement('begin_publication'), $cnstring);

                //set publication place
                $_ = !empty($workflow->getDataElement('publication_place')) ? $workflow->getDataElement('publication_place') : $medium->getElement('publication_place');
                $cnstring = str_replace('~PUBLICATIONPLACE~', $_, $cnstring);

                //set publisher
                $_ = !empty($workflow->getDataElement('publisher')) ? $workflow->getDataElement('publisher') : $medium->getElement('publisher');
                $cnstring = str_replace('~PUBLISHER~', $_, $cnstring);

                //set authors
                $authors = [];
                $authors_for_cn = [];

                $chapter_authors = json_decode($workflow->getDataElement('authors'));

                if(!is_array($chapter_authors)){
                        
                        //first author who's written publication
                        #$authors[] = $chapter->getElement('author_uid');

                        //second authors which additional selected
                        if(!empty($workflow->getDataElement('authors'))){
                        
                                foreach(explode(',', $workflow->getDataElement('authors')) as $_){
                                
                                        if(!in_array($_, $authors)){
                                                $authors[] = $_;
                                        }
                                }
                        }

                        //generate author string
                        foreach($authors as $uid){
                                $_user = new \Drupal\publisso_gold\Controller\User($uid);
                                $authors_for_cn[] = $_user->profile->getElement('lastname').' '.substr($_user->profile->getElement('firstname'), 0, 1);
                        }

                        //third more authors
                        if(!empty($workflow->getDataElement('more_authors'))){
                        
                                foreach(json_decode($workflow->getDataElement('more_authors'), true) as $_){
                                
                                        $authors_for_cn[] = $_['more_author_lastname'].' '.substr($_['more_author_name'], 0, 1);
                                }
                        }
                }
                else{
                        $corporations_for_cn = [];
                        $authors = [];
                        
                        foreach($chapter_authors as $_){
                                
                                $weight = $_->weight;
                                
                                while(array_key_exists($weight, $authors)) $weight++;
                                
                                $authors[$weight] = $_;
                        }
                        
                        ksort($authors);

                        foreach($authors as $_){
                        
                                $initials = '';
                                $firstnames = preg_split('/([\\s-]+)/', $_->firstname, -1);

                                $i = 0;
                                foreach($firstnames as $name){
                                
                                        if(!empty($name)) $initials .= strtoupper(substr($name, 0, 1));
                                        $i++;
                                        if($i == 2) break;
                                }

                                $authors_for_cn[] = $_->lastname.' '.$initials;
                        }

                        //fourth corporations
                        if(!empty($workflow->getDataElement('corporations'))){
                        
                                foreach(json_decode($workflow->getDataElement('corporations'), true) as $_){
                                        $corporations_for_cn[] = $_;
                                }
                        }

                        $authors_for_cn = array_filter($authors_for_cn);
                        $corporations_for_cn = array_filter($corporations_for_cn);
                }
                
                if(!empty($workflow->getDataELement('doi')))
                        $cnstring .= '<br>DOI: <a href="http://dx.doi.org/'.$workflow->getDataElement('doi').'">'.$workflow->getDataElement('doi').'</a>';
                
                $author_string_for_cn  = '';
                $author_string_for_cn .= implode(', ', $authors_for_cn);
                $author_string_for_cn .= (!empty($author_string_for_cn) ? (count($corporations_for_cn) ? '; ' : '') : '').implode('; ', $corporations_for_cn);

                $cnstring = str_replace('~AUTHORLIST~', $author_string_for_cn, $cnstring);
                $tmplSideblock->setVar('sbb_content', $chapter->getCitationNote());

                $tmplContent->setVar('citation_note', $tmplSideblock->parse());
                //sustaining_members
                if(count($medium->readSustainingMembers())){

                        $tmplSideblock->setVar('sbb_title', (string)t('Sustaining members'));
                        $tmplSideblock->setVar('sbb_content', '');
                        $tmplSideblock->setVar('view_name', 'partner-list');

                        foreach($medium->readSustainingMembers() as $bsm_id){

                                $tmplSideblock->appendToVar('sbb_content', '<img src="/system/getPicture/bsm/'.$bsm_id.'"><br>');
                        }

                        $tmplContent->setVar('sustaining_members', $tmplSideblock->parse());
                }

                //license
                if(!(empty($workflow->getDataElement('license')) || empty($medium->getElement('license')))){
                        
                        $license = empty($workflow->getDataElement('license')) ? $medium->getElement('license') : $workflow->getDataElement('license');
                        $tmplSideblock->setVar('sbb_title', (string)t('License'));
                        $license = new \Drupal\publisso_gold\Controller\License($license);
                        $tmplSideblock->setVar('sbb_content', $license->getImageLink(['height' => 40], '_blank'));
                        $tmplSideblock->appendToVar('sbb_content', '<br>&copy;&nbsp;');
                        $tmplSideblock->appendToVar('sbb_content', explode(' ', $authors_for_cn[0])[0]);
                        count($article_authors) > 1 ? $tmplSideblock->appendToVar('sbb_content', ' et al.') : '';
                        $tmplSideblock->appendToVar('sbb_content', '<br>');
                        $tmplSideblock->appendToVar('sbb_content', (string)t('This article is distributed under the terms of the license <a href="'.$license->getElement('url').'" target="_blank">@license</a>.', ['@license' => $license->getElement('description')]));
                
                        $tmplSideblock->setVar('view_name', 'license');

                        $tmplContent->setVar('chapter_license', $tmplSideblock->parse());
                }
                
                //version
                if($chapter->getElement('version') || $chapter->getElement('next_version') || $chapter->getElement('previous_version')){
                        
                        $tmplSideblock->clear();
                        $tmplSideblock->setVar('sbb_title', (string)$this->t('Version'));
                        $tmplSideblock->setVar('sbb_content', $chapter->getElement('version'));
                        $tmplSideblock->setVar('view_name', 'version');
                        
                        $links = [];
                        
                        if(!empty($chapter->getElement('next_version'))){
                                $route_param = \Drupal::routeMatch()->getRawParameters()->all();
                                $route_param['cp_id'] = $chapter->getElement('next_version');
                                $url = Url::fromRoute(\Drupal::routeMatch()->getRouteName(), $route_param);
                                
                                $tmplSideblock->appendToVar('sbb_content', '<div class="rwError">'.((string)$this->t('This version is not up-to-date')).'</div>');
                                $links[] = Link::fromTextAndUrl(((string)$this->t('Erratum')), $url)->toString();
                        }
                        
                        if(!empty($chapter->getElement('previous_version'))){
                                $route_param = \Drupal::routeMatch()->getRawParameters()->all();
                                $route_param['cp_id'] = $chapter->getElement('previous_version');
                                $url = Url::fromRoute(\Drupal::routeMatch()->getRouteName(), $route_param);
                                $links[] = Link::fromTextAndUrl(((string)$this->t('Erratum for')), $url)->toString();
                        }
                        
                        $tmplSideblock->appendToVar('sbb_content', '<div>'.implode('<br>', $links).'</div>');
                        $tmplContent->setVar('chapter_version', $tmplSideblock->parse());
                }
                
                /*
                //version
                if(!empty($chapter->getElement('version'))){

                $__tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_sidebar_block.tmpl.inc.php');
                $__vars = [];
                $__vars['::sbb_title::'] = (string)t('Version');
                $__vars['::sbb_content::'] = $chapter->getElement('version');
                $__vars['::view_name::'] = 'version';

                $_vars['::chapter_version::'] = $this->renderVars($__tmpl, $__vars);
                }

                //keywords
                $keywords = json_decode($chapter->getElement('keywords'), true);

                if(count($keywords)){

                $__tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_sidebar_block.tmpl.inc.php');
                $__vars = [];
                $__vars['::sbb_title::'] = (string)t('Keywords');
                $__vars['::sbb_content::'] = '<ul class="list-unstyled">';
                $__vars['::view_name::'] = 'keywords';

                foreach($keywords as $_){
                $__vars['::sbb_content::'] .= '<li><a href="#" class="intern">'.$_.'</a></li>';
                }

                $__vars['::sbb_content::'] .= '</ul>';
                $_vars['::chapter_keywords::'] = $this->renderVars($__tmpl, $__vars);
                }
                */
                $template->setVar('content_item_chapter_content', $template->parse());
                
                $template->setVar('li_item_chapter_content',  $tmplTab->parse());
                $template->setVar('content_item_chapter_content',  $tmplContent->parse());
        }
        
        /**
         * @param $tmpl
         * @param $journal
         * @param $workflow
         */
        protected function fillTemplateJournalContent(&$tmpl, &$journal, &$workflow){
        
                $tmpl->setVar('tab_current_volume_class_active', 'active');
                $tmpl->setVar('content_current_volume_class_active', 'active');
                $tmpl->setVar('txt_about'                               , (string)t('About this journal')       );
                $tmpl->setVar('txt_editorial_board'                     , (string)t('Editorial board')          );
                $tmpl->setVar('txt_manuscript_guidelines'               , (string)t('Manuscript guidelines')    );
                $tmpl->setVar('txt_imprint'                             , (string)t('Imprint')                  );
                $tmpl->setVar('txt_current_volume'                      , (string)t('Current volume')           );
                $tmpl->setVar('journal_about_sidebar_blocks'            , ''                                    );
                $tmpl->setVar('journal_article_content_sidebar_blocks'  , ''                                    );
                
                $_ = base64_decode($journal->getElement('about'));
                $tmpl->setVar('journal_about', $journal->getElement('about'));
                
                $_ = base64_decode($journal->getElement('manuscript_guidelines'));
                $tmpl->setVar('journal_manuscript_guidelines', $journal->getElement('manuscript_guidelines'));
                
                $_ = base64_decode($journal->getElement('imprint'));
                $tmpl->setVar('journal_imprint', $journal->getElement('imprint'));
                
                //$this->tmpl_vars['::book_funding_text::'] = $book->getElement('funding_text');
                $tmpl->setVar('funding', $journal->getElement('funding'));
                
                //echo '<pre>'.print_r('Reading TOC....', 1).'</pre>';
                $toc = new \Drupal\publisso_gold\Controller\TableOfContent(false, ['medium' => 'journal', 'id' => $journal->getElement('id')], true);
                //echo '<pre>'.print_r('Beginning parsing structure....', 1).'</pre>';
                $tmpl->setVar('journal_current_volume', $this->parseTOCStructure($toc->readStructure(), 1));
                
                //template for current volume
                $volume = $journal->getCurrentVolume();
                $tmpl->setVar('journal_current_volume', '');
                
                //view current volume?
                if($volume->getElement('id') != $journal->getCurrentVolumeID()){
                        $tmpl->setVar('txt_current_volume', (string)t('Volume @year - @number', ['@year' => $volume->getElement('year'), '@number' => $volume->getElement('number')]));
                        
                        $tmpl->prependToVar('journal_current_volume',
                                Link::fromTextAndUrl(
                                        (string)t('Go to current volume'),
                                        Url::fromRoute(
                                                'publisso_gold.journal.volume',
                                                [
                                                        'jrn_id' => $journal->getElement('id'),
                                                        'vol_id' => $journal->getCurrentVolumeID()
                                                ]
                                        )
                                )->toString().'<hr>'
                        );
                }
                
                $articles = [];
                if(!$volume->hasIssues()){
                        foreach($volume->getArticles() as $jrna_id){
                        
                                $article = $volume->getArticle($jrna_id);
                                $section = (string)t('Volume @year - @number', ['@number' => $volume->getElement('number'), '@year' => $volume->getElement('year')]);
                                $articles[$section][] = [
                                        'title'   => $article->getElement('title'),
                                        'jrna_id' => $article->getElement('id'),
                                        'volume'  => $volume->getElement('id')
                                ];
                        }
                }
                else{
                        foreach($volume->getIssues() as $iss_id){
                                
                                $issue = $volume->getIssue($iss_id);
                                
                                foreach($issue->getArticles() as $jrna_id){
                                
                                        $article = $volume->getArticle($jrna_id);
                                        $section = (string)t('Volume @year - @vol_number, Issue @iss_number', ['@year' => $volume->getElement('year'), '@vol_number' => $volume->getElement('number'), '@iss_number' => $issue->getElement('number')]);
                                        $articles[$section][] = [
                                                'title'   => $article->getElement('title'),
                                                'jrna_id' => $article->getElement('id'),
                                                'volume'  => $volume->getElement('id'),
                                                'issue'   => $issue->getElement('id')
                                        ];
                                }
                        }
                }
                
                $c = 0;
                foreach($articles as $_section => $_articles){
                        
                        $tmpl_section = new \Drupal\publisso_gold\Controller\Template();
                        $tmpl_section->get('volume_content_current_volume_section');
                        $tmpl_section->setVar('section', $_section);
                        $tmpl_section->setVar('articles', '');
                        
                        $cc = 1;
                        foreach($_articles as $_article){
                                
                                if(array_key_exists('issue', $_article))
                                        $url = Url::fromRoute('publisso_gold.journal.volume.issue.article', ['jrn_id' => $journal->getElement('id'), 'vol_id' => $_article['volume'], 'iss_id' => $_article['issue'], 'jrna_id' => $_article['jrna_id']]);
                                else
                                        $url = Url::fromRoute('publisso_gold.journal.volume.article', ['jrn_id' => $journal->getElement('id'), 'vol_id' => $_article['volume'], 'jrna_id' => $_article['jrna_id']]);
                                
                                $link = Link::fromTextAndUrl((string)t('Article @cc', ['@cc' => $cc]), $url);
                                $tmpl_section->appendToVar('articles', $link->toString().': '.$_article['title'].'<br>');
                                $cc++;
                        }
                        
                        $tmpl->appendToVar('journal_current_volume', $tmpl_section->parse());
                        
                        if($c) $tmpl->appendToVar('journal_current_volume', '<hr>');
                        
                        $c++;
                }
                
                //volume from archive
                if(array_key_exists('vol_id', $route['route_param'])){
                
                        $aVolume = $journal->getVolume($route['route_param']['vol_id']);
                        
                        if($aVolume->getElement('id') != $journal->getCurrentVolumeID()){
                                
                                $route_frag = explode('.', $route['route_name']);
                        
                                $tmpl->setVar('tab_current_volume_class_active', '');
                                $tmpl->setVar('content_current_volume_class_active', '');
                                
                                $tmpl->setVar('li_item_archive_volume'         , '');
                                $tmpl->setVar('content_item_archive_volume'    , '');
                                
                                $tmpl_tab = new \Drupal\publisso_gold\Controller\Template();
                                $tmpl_tab->get('publisso_gold_journal_content_archive_volume_tab');
                                
                                $tmpl_cnt = new \Drupal\publisso_gold\Controller\Template();
                                $tmpl_cnt->get('publisso_gold_journal_content_archive_volume_content');
                                
                                //Don't make active if article is shown
                                if(!in_array('article', $route_frag)){
                                        $tmpl_tab->setVar('tab_archive_volume_class_active', 'active');
                                        $tmpl_cnt->setVar('content_archive_volume_class_active', 'active');
                                }
                                
                                $tmpl_tab->setVar('txt_archive_volume', (string)t('Volume @year - @number', ['@year' => $volume->getElement('year'), '@number' => $volume->getElement('number')]));
                                /*
                                $tmpl_cnt->prependToVar('journal_archive_volume',
                                        Link::fromTextAndUrl(
                                                (string)t('Go to current volume'),
                                                Url::fromRoute(
                                                        'publisso_gold.journal.volume',
                                                        [
                                                                'jrn_id' => $journal->getElement('id'),
                                                                'vol_id' => $journal->getCurrentVolumeID()
                                                        ]
                                                )
                                        )->toString().'<hr>'
                                );
                                */
                                $aArticles = [];
                                if(!$aVolume->hasIssues()){
                                        foreach($aVolume->getArticles() as $jrna_id){
                                        
                                                $aArticle = $aVolume->getArticle($jrna_id);
                                                $section = (string)t('Volume @year - @number', ['@number' => $aVolume->getElement('number'), '@year' => $aVolume->getElement('year')]);
                                                $aArticles[$section][] = [
                                                        'title'   => $aArticle->getElement('title'),
                                                        'jrna_id' => $aArticle->getElement('id'),
                                                        'volume'  => $aVolume->getElement('id')
                                                ];
                                        }
                                }
                                else{
                                        foreach($aVolume->getIssues() as $iss_id){
                                                
                                                $issue = $aVolume->getIssue($iss_id);
                                                
                                                foreach($issue->getArticles() as $jrna_id){
                                                
                                                        $aArticle = $aVolume->getArticle($jrna_id);
                                                        $section = (string)t('Volume @year - @vol_number, Issue @iss_number', ['@year' => $aVolume->getElement('year'), '@vol_number' => $aVolume->getElement('number'), '@iss_number' => $issue->getElement('number')]);
                                                        $aArticles[$section][] = [
                                                                'title'   => $aArticle->getElement('title'),
                                                                'jrna_id' => $aArticle->getElement('id'),
                                                                'volume'  => $aVolume->getElement('id'),
                                                                'issue'   => $issue->getElement('id')
                                                        ];
                                                }
                                        }
                                }
                                
                                $c = 0;
                                foreach($aArticles as $_section => $_articles){
                                        
                                        $tmpl_section = new \Drupal\publisso_gold\Controller\Template();
                                        $tmpl_section->get('volume_content_current_volume_section');
                                        $tmpl_section->setVar('section', $_section);
                                        $tmpl_section->setVar('articles', '');
                                        
                                        $cc = 1;
                                        foreach($_articles as $_article){
                                                
                                                if(array_key_exists('issue', $_article))
                                                        $url = Url::fromRoute('publisso_gold.journal.volume.issue.article', ['jrn_id' => $journal->getElement('id'), 'vol_id' => $_article['volume'], 'iss_id' => $_article['issue'], 'jrna_id' => $_article['jrna_id']]);
                                                else
                                                        $url = Url::fromRoute('publisso_gold.journal.volume.article', ['jrn_id' => $journal->getElement('id'), 'vol_id' => $_article['volume'], 'jrna_id' => $_article['jrna_id']]);
                                                
                                                $link = Link::fromTextAndUrl((string)t('Article @cc', ['@cc' => $cc]), $url);
                                                $tmpl_section->appendToVar('articles', $link->toString().': '.$_article['title'].'<br>');
                                                $cc++;
                                        }
                                        
                                        $tmpl_cnt->appendToVar('journal_archive_volume', $tmpl_section->parse());
                                        
                                        if($c) $tmpl_cnt->appendToVar('journal_archive_volume', '<hr>');
                                        
                                        $c++;
                                }
                                
                                $tmpl->setVar('li_item_archive_volume'         , $tmpl_tab->parse());
                                $tmpl->setVar('content_item_archive_volume'    , $tmpl_cnt->parse());
                        }
                }
                
                //volume-archive
                if(count($journal->getVolumes()) > 1){
                        
                        $archiveVolumes = $journal->getVolumes();
                        array_splice($archiveVolumes, array_search($journal->getCurrentVolumeID(), $archiveVolumes));
                        
                        $tmpl->setVar('li_item_archive'         , '');
                        $tmpl->setVar('content_item_archive'    , '');
                        
                        $tmpl_tab = new \Drupal\publisso_gold\Controller\Template();
                        $tmpl_tab->get('publisso_gold_journal_content_archive_tab');
                        
                        $tmpl_cnt = new \Drupal\publisso_gold\Controller\Template();
                        $tmpl_cnt->get('publisso_gold_journal_content_archive_content');
                        
                        //--
                        $tmpl_tab->setVar('txt_archive'                 , (string)t('Archive'));
                        $tmpl_tab->setVar('journal_archive_content'     , '');
                        
                        foreach(array_reverse($archiveVolumes) as $_){
                                
                                $_volume = $journal->getVolume($_);
                                
                                if($_volume->hasAnyArticles()){
                                        $tmpl_cnt->appendToVar('journal_archive_content',
                                                Link::fromTextAndUrl(
                                                        (string)t('Volume @year - @number', ['@year' => $_volume->getElement('year'), '@number' => $_volume->getElement('number')]),
                                                        Url::fromRoute(
                                                                'publisso_gold.journal.volume',
                                                                [
                                                                        'jrn_id' => $journal->getElement('id'),
                                                                        'vol_id' => $_
                                                                ]
                                                        )
                                                )->toString()
                                        );
                                        
                                        $tmpl_cnt->appendToVar('journal_archive_content', '<br>');
                                }
                        }
                        //--
                        
                        $tmpl->setVar('li_item_archive'         , $tmpl_tab->parse());
                        $tmpl->setVar('content_item_archive'    , $tmpl_cnt->parse());
                }
                
        /* create template for authors
        *
        * use template book_content_author
        *
                $tmpl_author = file_get_contents($this->modpath.'/inc/publisso_gold_book_content_author.tmpl.inc.php');
                $content_authors = '';
                $c = 0;
                
                        if(count($book->readAuthors()) == 0){
                                $content_authors = '<p>Here the authors of the chapters will be listed.</p>';
                        }
                        else{
                                foreach($book->readAuthors() as $user){
                                
                                $_tmpl = $tmpl_author;
                                $_vars = [];
                                
                                $_vars['::txt_more::'] = (string)t('more');
                                $_vars['::txt_address::'] = (string)t('Address');
                                $_vars['::txt_contact::'] = (string)t('Contact');
                                
                                $_vars['::modal_id::'] = 'm_authors_'.($c + 1);
                                
                                $_vars['::author_name::'] = $user->profile->getElement('graduation').' '.$user->profile->getElement('firstname').' '.$user->profile->getElement('lastname').' '.$user->profile->getElement('graduation_suffix');
                                $_vars['::author_institute::'] = $user->profile->getElement('institute');
                                $_vars['::author_department::'] = $user->profile->getElement('department');
                                $_vars['::author_street::'] = $user->profile->getElement('street');
                                $_vars['::author_postal_code::'] = $user->profile->getElement('postal_code');
                                $_vars['::author_city::'] = $user->profile->getElement('city');
                                $_vars['::author_country::'] = getCountry($user->profile->getElement('country'));
                                $_vars['::author_telephone::'] = $user->profile->getElement('telephone');
                                $_vars['::author_email::'] = $user->profile->getElement('email');
                                $_vars['::author_pic_link::'] = '/system/getPicture/up/'.$user->getId();
                                $_vars['::author_address::'] = implode(', ', [implode(' ', [$_vars['::author_postal_code::'], $_vars['::author_city::']]), $_vars['::author_country::']]);
                                
                                if($c % 2 == 0){
                                        $_tmpl = '<div class="row flex-row-sm">'.$_tmpl;
                                }
                                
                                if($c % 2 != 0){
                                        $_tmpl .= '</div><hr>';
                                }
                                
                                $content_authors .= $this->renderVars($_tmpl, $_vars);
                                $c++;
                                }
                }
                        
                        if($c > 0 && $c % 2 != 0){
                                $content_authors .= '</div>';
                        }
                        
                $this->tmpl_vars['::content_authors::'] = $content_authors;
        /* =========================== */

        /* create template for editorial board
        *
        * use template book_content_author
        * (like authors, though same var-names are used)
        */
                
                $content_eb = '';
                $modal_id = 0;
                $c = 0;
                
                        $eb = $journal->readEditorialBoard();
                        $content_eb .= '<tt>'.$journal->getEbAbAnnotation('main').'</tt>';
                $tmpl_eb = new \Drupal\publisso_gold\Controller\Template();
                
                foreach($eb as $uid => $user){
                        
                        $modal_id++;
                        
                        $tmpl_eb->get('publisso_gold_journal_content_author');
                        
                        $tmpl_eb->setVar('txt_more', (string)t('more'));
                        $tmpl_eb->setVar('txt_address', (string)t('Address'));
                        $tmpl_eb->setVar('txt_contact', (string)t('Contact'));
                        $tmpl_eb->setVar('modal_id', 'm_editors_'.($modal_id));
                        $tmpl_eb->setVar('author_name', implode(' ', [$user->profile->getElement('graduation'), $user->profile->getElement('firstname'), $user->profile->getElement('lastname'), $user->profile->getElement('graduation_suffix')]));
                        $tmpl_eb->setVar('author_institute', $user->profile->getElement('institute'));
                        $tmpl_eb->setVar('author_department', $user->profile->getElement('department'));
                        $tmpl->setVar('author_street', $user->profile->getElement('street'));
                        $tmpl_eb->setVar('author_notes', $user->tempInfo['eb_notes'].'<br><br>');
                        $tmpl_eb->setVar('author_postal_code', $user->profile->getElement('postal_code'));
                        $tmpl_eb->setVar('author_city', $user->profile->getElement('city'));
                        $tmpl_eb->setVar('author_country', \Drupal::service('publisso_gold.tools')->getCountry($user->profile->getElement('country')));
                        $tmpl_eb->setVar('author_telephone', $user->profile->getElement('telephone'));
                        $tmpl_eb->setVar('author_email', $user->profile->getElement('email'));
                        $tmpl_eb->setVar('author_pic_link', '/system/getPicture/up/'.$user->getId());
                        $tmpl_eb->setVar('author_address', implode(', ', array_filter([implode(' ', array_filter([$tmpl_eb->getVar('author_postal_code'), $tmpl_eb->getVar('author_city')])), $tmpl_eb->getVar('author_country')])));
                        
                        if($c % 2 == 0){
                                $tmpl_eb->appendHTML('<div class="row flex-row-sm">');
                        }
                        
                        if($c % 2 != 0){
                                $tmpl_eb->appendHTML('</div><hr>');
                        }
                        
                        $content_eb .= $tmpl_eb->parse();
                        $c++;
                }
                        
                        if($c > 0 && $c % 2 != 0){
                                $content_eb .= '</div>';
                        }
                        
                        //advisory boards
                        $advisoryBoards = $journal->readAdvisoryBoard();
                        
                        foreach($advisoryBoards as $boardName => $users){
                                
                                $content_eb .= '<h4><center><i>'.$boardName.'</i></center></h4>';
                                $content_eb .= '<tt>'.$journal->getEbAbAnnotation($boardName).'</tt>';
                                $c = 0;
                                
                                foreach($users as $uid => $user){
                                        
                                        $modal_id++;
                                        $tmpl_eb->get('publisso_gold_journal_content_author');

                                        $tmpl_eb->setVar('txt_more', (string)t('more'));
                                        $tmpl_eb->setVar('txt_address', (string)t('Address'));
                                        $tmpl_eb->setVar('txt_contact', (string)t('Contact'));

                                        $tmpl_eb->setVar('modal_id', 'm_editors_'.($modal_id));

                                        $tmpl_eb->setVar('author_name', implode(' ', array_filter([$user->profile->getElement('graduation'), $user->profile->getElement('firstname'), $user->profile->getElement('lastname'), $user->profile->getElement('graduation_suffix')])));
                                        $tmpl_eb->setVar('author_institute', $user->profile->getElement('institute'));
                                        $tmpl_eb->setVar('author_department', $user->profile->getElement('department'));
                                        $tmpl_eb->setVar('author_street', $user->profile->getElement('street'));
                                        $tmpl_eb->setVar('author_notes', $user->tempInfo['eb_notes'].'<br><br>');
                                        $tmpl_eb->setVar('author_postal_code', $user->profile->getElement('postal_code'));
                                        $tmpl_eb->setVar('author_city', $user->profile->getElement('city'));
                                        $tmpl_eb->setVar('author_country', getCountry($user->profile->getElement('country')));
                                        $tmpl_eb->setVar('author_telephone', $user->profile->getElement('telephone'));
                                        $tmpl_eb->setVar('author_email', $user->profile->getElement('email'));
                                        $tmpl_eb->setVar('author_pic_link', '/system/getPicture/up/'.$user->getId());
                                        $tmpl_eb->setVar('author_address', implode(', ', array_filter([implode(' ', array_filter([$tmpl_eb->getVar('author_postal_code'), $tmpl_eb->getVar('author_city')])), $tmpl_eb->getVar('author_country')])));

                                        if($c % 2 == 0){
                                                $tmpl_eb->prependHTML('<div class="row flex-row-sm">');
                                        }

                                        if($c % 2 != 0){
                                                $tmpl_eb->appendHTML('</div><hr>');
                                        }

                                        $content_eb .= $tmppl_eb->parse();
                                        $c++;
                                }
                                
                                if($c > 0 && $c % 2 != 0){
                                        $content_eb .= '</div>';
                                }
                        }
                        
                $tmpl->setVar('content_eb', $content_eb);
                
                
        /* =========================== */
                        
                        $route_frag = explode('.', $route['route_name']);
                        
                        //if(in_array('article', $route_frag)){
                                
                                $artVolume = new \Drupal\publisso_gold\Controller\Volume($workflow->getDataElement('volume'));
                                $artIssue = $workflow->getDataElement('issue') ? new \Drupal\publisso_gold\Controller\Issue($workflow->getDataElement('issue')) : null;
                                
                                $tmpl_tab = new \Drupal\publisso_gold\Controller\Template();
                                $tmpl_tab->get('publisso_gold_journal_content_article_tab');
                                $tmpl_cnt = new \Drupal\publisso_gold\Controller\Template();
                                $tmpl_cnt->get('publisso_gold_journal_content_article_content');
                                
                                //activate tab
                                $tmpl->setVar('tab_current_volume_class_active'         , ''            );
                                $tmpl->setVar('content_current_volume_class_active'     , ''            );
                                $tmpl_tab->setVar('tab_article_content_class_active'    , 'active'      );
                                $tmpl_cnt->setVar('journal_article_content_class_active', 'active'      );
                                
                                //set labels
                                $tmpl_tab->setVar('txt_article_content', (string)t('Article content'));
                                
                                //init some vars
                                $tmpl_cnt->setVar('journal_article_content', '');
                                $tmpl_cnt->setVar('citation_note', '');
                                
                                //sidebar template
                                $tmpl_sbb = new \Drupal\publisso_gold\Controller\Template();
                                $tmpl_sbb->get('publisso_gold_journal_content_sidebar_block');
                                
                                //citation note
                                $tmpl_sbb->setVar('sbb_title', (string)t('Citation note'));
                                $tmpl_sbb->setVar('sbb_content', '');
                                
                                $cnstring = '~AUTHORLIST~. ~TITLE~. ~JOURABBRNLM~ ~YEAR~;~VOLNO~~ISSNO~:Doc~DOCNO~.';
                
                                //set title
                                $cnstring = str_replace('~TITLE~', $workflow->getDataElement('title'), $cnstring);
                                $cnstring = str_replace('~JOURABBRNLM~', $journal->getElement('title_nlm').'.', $cnstring);
                                $cnstring = str_replace('~VOLNO~', $workflow->getDataElement('volume'), $cnstring);
                                $cnstring = str_replace('~YEAR~', $artVolume->getElement('year'), $cnstring);
                                if($artIssue) $cnstring = str_replace('~ISSNO~', '('.$article->getElement('issue').')', $cnstring);
                                $cnstring = str_replace('~DOCNO~', $workflow->getDataElement('docno'), $cnstring);
                                
                                //set version
                                $version = $workflow->getDataElement('version');
                                
                                if(!empty($version))
                                        $cnstring = str_replace('~VERSION~', $version.'. ', $cnstring);
                                else
                                        $cnstring = str_replace('~VERSION~', '', $cnstring);
                                
                                //set cnstring
                                $citation_note = $journal->getElement('citation_note');
                                if(!empty($citation_note)) $citation_note = "IN: $citation_note. ";
                                $cnstring = str_replace('~CNSTRING~', $citation_note, $cnstring);
                                
                                //set begin publication
                                $_ = !empty($workflow->getDataElement('publication_year')) ? $workflow->getDataElement('publication_year') : $journal->getElement('begin_publication');
                                $cnstring = str_replace('~BEGINPUBLICATION~', $_, $cnstring);
                                
                                //set publication place
                                $_ = !empty($workflow->getDataElement('publication_place')) ? $workflow->getDataElement('publication_place') : $journal->getElement('publication_place');
                                $cnstring = str_replace('~PUBLICATIONPLACE~', \Drupal::service('publisso_gold.tools')->getPublicationPlace($_), $cnstring);
                                
                                //set publisher
                                $_ = !empty($workflow->getDataElement('publisher')) ? $workflow->getDataElement('publisher') : $journal->getElement('publisher');
                                $cnstring = str_replace('~PUBLISHER~', \Drupal::service('publisso_gold.tools')->getPublisher($_), $cnstring);
                                
                                //set authors
                                $authors = [];
                                $authors_for_cn = [];
                                
                                $article_authors = json_decode($workflow->getDataElement('authors'));
                                
                                if(!is_array($article_authors)){
                                        
                                        //first author who's written publication
                                        #$authors[] = $chapter->getElement('author_uid');
                                        
                                        //second authors which additional selected
                                        if(!empty($workflow->getDataElement('authors'))){
                                                foreach(explode(',', $workflow->getDataElement('authors')) as $_){
                                                        if(!in_array($_, $authors)){
                                                                $authors[] = $_;
                                                        }
                                                }
                                        }
                                        
                                        //generate author string
                                        foreach($authors as $uid){
                                                $_user = new \Drupal\publisso_gold\Controller\User($uid);
                                                $authors_for_cn[] = $_user->profile->getElement('lastname').' '.substr($_user->profile->getElement('firstname'), 0, 1);
                                        }
                                        
                                        //third more authors
                                        if(!empty($workflow->getDataElement('more_authors'))){
                                                foreach(json_decode($workflow->getDataElement('more_authors'), true) as $_){
                                                        $authors_for_cn[] = $_['more_author_lastname'].' '.substr($_['more_author_name'], 0, 1);
                                                }
                                        }
                                }
                                else{
                                        $corporations_for_cn = [];
                                        $authors = [];
                                        foreach($article_authors as $_) $authors[$_->weight] = $_;
                                        ksort($authors);
                                        
                                        foreach($authors as $_){
                                                $initials = '';
                                                $firstnames = preg_split('/([\\s-]+)/', $_->firstname, -1);
                                                
                                                $i = 0;
                                                foreach($firstnames as $name){
                                                        if(!empty($name)) $initials .= strtoupper(substr($name, 0, 1));
                                                        $i++;
                                                        if($i == 2) break;
                                                }
                                                
                                                $authors_for_cn[] = $_->lastname.' '.$initials;
                                        }
                                        
                                        //fourth corporations
                                        if(!empty($workflow->getDataElement('corporations'))){
                                                foreach(json_decode($workflow->getDataElement('corporations'), true) as $_){
                                                        $corporations_for_cn[] = $_;
                                                }
                                        }
                                                        
                                                        $authors_for_cn = array_filter($authors_for_cn);
                                                        $corporations_for_cn = array_filter($corporations_for_cn);
                                }
                                
                                $cnstring .= '<br>DOI: <a href="http://dx.doi.org/'.$workflow->getDataElement('doi').'">'.$workflow->getDataElement('doi').'</a>';
                                
                                $author_string_for_cn  = '';
                                $author_string_for_cn .= implode(', ', $authors_for_cn);
                                $author_string_for_cn .= (!empty($author_string_for_cn) ? (count($corporations_for_cn) ? '; ' : '') : '').implode('; ', $corporations_for_cn);
                                
                                $cnstring = str_replace('~AUTHORLIST~', $author_string_for_cn, $cnstring);
                                $tmpl_sbb->setVar('sbb_content', $cnstring);
                                
                                $tmpl_cnt->setVar('citation_note', $tmpl_sbb->parse());
                                
                                //license
                                $tmpl_sbb->setVar('sbb_title', (string)t('License'));
                                $license = new \Drupal\publisso_gold\Controller\License($workflow->getDataElement('license'));
                                $tmpl_sbb->setVar('sbb_content', $license->getImageLink(['height' => 40], '_blank'));
                                $tmpl_sbb->appendToVar('sbb_content', '<br>&copy;&nbsp;');
                                $tmpl_sbb->appendToVar('sbb_content', explode(' ', $authors_for_cn[0])[0]);
                                count($article_authors) > 1 ? $tmpl_sbb->appendToVar('sbb_content', ' et al.') : '';
                                $tmpl_sbb->appendToVar('sbb_content', '<br>');
                                $tmpl_sbb->appendToVar('sbb_content', (string)t('This article is distributed under the terms of the license <a href="'.$license->getElement('url').'" target="_blank">@license</a>.', ['@license' => $license->getElement('description')]));
                                $tmpl_cnt->setVar('license', $tmpl_sbb->parse());
                                
                                //main article content
                                //title
                                $tmpl_cnt->appendToVar('journal_article_content', '<h2>'.$workflow->getDataElement('title').'</h2>');
                                
        //set the authors
                                $authors         = [];
                                $affiliations    = [];
                                $strAuthors      = '';
                                $strAffiliations = '';
                                
                                $article_authors = json_decode($workflow->getDataElement('authors'));
                                
                                foreach($article_authors as $_) $authors[$_->weight] = $_;
                                ksort($authors);
                                
                                foreach($authors as $_){
                                        
                                        if($_->uid && $_->profile_archive_id){
                                                $user = new \Drupal\publisso_gold\Controller\User($_->uid);
                                                $user->profile->loadSnapshot($_->profile_archive_id);
                                                $affiliations[] = $user->profile->getAffiliation();
                                        }
                                        else{
                                                $affiliations[] = $_->affiliation;
                                        }
                                }
                                $affiliations = array_keys(array_flip($affiliations)); //make affs unique

                                for($c = 1; $c <= count($affiliations); $c++){
                                        if($affiliations[$c - 1]) $strAffiliations .= (!empty($strAffiliations) ? '<br>' : '') .'<sup>'.$c.'</sup>&nbsp;'.$affiliations[$c - 1];
                                }

                                foreach($authors as $_){
                                        
                                        if($_->uid && $_->profile_archive_id){
                                                $user = new \Drupal\publisso_gold\Controller\User($_->uid);
                                                $user->profile->loadSnapshot($_->profile_archive_id);
                                                $affiliation = $user->profile->getAffiliation();
                                        }
                                        else{
                                                $affiliation = $_->affiliation;
                                        }
                                        
                                        $affKey = array_search($affiliation, $affiliations) + 1;
                                        $strAuthors .= (!empty($strAuthors) ? '<br>' : '').implode(' ', [$_->firstname, $_->lastname]).(!empty($affKey) ? " <sup>$affKey</sup>" : "");
                                }
                                
                                $strAuthors .= '<br>'.implode('<br>', array_filter(json_decode($workflow->getDataElement('corporations'), 1)));
                                $tmpl_cnt->appendToVar('journal_article_content', "$strAuthors<br><br>$strAffiliations<br><br>");
        // -- set the authors --
                                
                                list($_fulltext, $_references) = \Drupal::service('publisso_gold.tools')->getReferencesFromText(base64_decode($workflow->getDataElement('article_text')));
                                $inactiveReferences = \Drupal::service('publisso_gold.tools')->getInactiveReferences($_references, $workflow->getDataElement('references'));
                                $_references = implode('<br>', array_filter([nl2br($_references), nl2br($inactiveReferences)]));
                                //echo '<pre>'.$_references.'</pre>';
                                
                                //abstract
                                $tmpl_cnt->appendToVar('journal_article_content', !empty($workflow->getDataElement('abstract')) ? base64_decode($workflow->getDataElement('abstract')) : '<br><br>');
                                $tmpl_cnt->appendToVar('journal_article_content', '<hr>');
                                $tmpl_cnt->appendToVar('journal_article_content', $_fulltext);
                                $tmpl_cnt->appendToVar('journal_article_content', '<hr>');
                                $tmpl_cnt->appendToVar('journal_article_content', !empty($_references) ? '<h3>'.((string)t('References')).'</h3>': '');
                                $tmpl_cnt->appendToVar('journal_article_content', !empty($_references) ? $_references : '');
                                
                                $tmpl->setVar('li_item_article_content', $tmpl_tab->parse());
                                $tmpl->setVar('content_item_article_content', $tmpl_cnt->parse());
                        //}
                        
        }
        
        /**
         * @param $structure
         * @param $level
         * @return string
         */
        private function parseTOCStructure(&$structure, $level){
                
                $markup = '';
                $class_levels = [
                '1' => 'nav',
                '2' => 'dropdown-menu sub-menu',
                '3' => 'sub-menu deep-level',
                '4' => 'expanded',
                '5' => 'expanded last'
                ];
                
                if(count($structure) > 0){
                
                $markup .= '<ul class="'.$class_levels[$level + 1000].'">';
                
                foreach($structure as $item){
                        
                        $childs = $item['childs'];
                        $item   = $item['item'  ];
                        
                        $class = count($childs) > 0 ? ' class="dropdown on"' : '';
                        
                        $markup .= '<li>';
                        
                        if(!empty($item->getElement('link'))){
                        
                        $link = $item->getElement('link');
                        
                        if(substr($link, 0, 9) == 'intern://'){
                                $link = str_replace('intern://', '', $link);
                                list($sub_medium, $sub_medium_id) = explode('/', $link);
                                
                                switch($sub_medium){
                                case 'chapter':
                                        $link = '/publisso_gold/book/'.$this->medium_id.'/'.$sub_medium.'/'.$sub_medium_id;
                                        break;
                                }
                        }
                        }
                        
                        if(!(empty($link) || empty($item->getElement('link'))))
                        $markup .= '<a href="'.$link.'">'.$item->getElement('title').'</a>';
                        else
                        $markup .= $item->getElement('title');
                        
                        if($item->getElement('description') != ''){
                        $markup .= '<div class="description">'.$item->getElement('description').'</div>';
                        }
                        
                        if($item->getElement('authors') != ''){
                        $markup .= '<div class="authors">'.$item->getElement('authors').'</div>';
                        }
                        
                        if(count($childs) > 0){
                        $markup .= $this->parseTOCStructure($childs, $level + 1);
                        }
                        
                        $markup .= '</li>';
                }
                
                $markup .= '</ul>';
                }
                
                return $markup;
        }
}
