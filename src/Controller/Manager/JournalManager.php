<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\publisso_gold\Controller\Medium\Journal;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class JournalManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class JournalManager extends ControllerBase {
                
                public static function getJournals() :array{
                        
                        try{
                                return \Drupal::database()->select('rwPubgoldJournals', 'c')->fields('c', ['jrn_id'])->execute()->fetchCol();
                        }
                        catch(\Exception $e){
                                \Drupal::service('messenger')->addError($e->getMessage());
                                return [];
                        }
                }
        
                /**
                 * @param $jrn_id
                 * @param bool $noChilds
                 * @return Journal|null
                 */
                public static function getJournal($jrn_id, bool $noChilds = false){
                        
                        if(in_array($jrn_id, self::getJournals())){
                                return new Journal($jrn_id, $noChilds);
                        }
                        else{
                                \Drupal::service('messenger')->addError('Journal not found!');
                                return null;
                        }
                }
                
                public static function getJournalsList(array $fields = []) :array{
                        
                        try{
                                return \Drupal::database()->select('rwPubgoldJournals', 't')->fields('t', array_merge(['jrn_id'], $fields))->execute()->fetchAllAssoc('jrn_id');
                        }
                        catch(\Exception $e){
                                \Drupal::service('messenger')->addError($e->getMessage());
                                return [];
                        }
                }
        
                public static function _create() :Journal{
                
                        $id = \Drupal::database()->insert('rwPubgoldJournals')->fields(['jrn_public' => 0])->execute();
                        return new Journal($id);
                }
        
                /**
                 * @param null $jrn_id
                 * @return array[]|mixed
                 */
                public static function getHTMLViewTabs($jrn_id = null){
        
                        if($jrn_id) {
        
                                $qry = \Drupal::database()->select( 'rwPubgoldJournals', 'b' )->fields( 'b', [ 'jrn_view_tabs' ] )->condition( 'jrn_id', $jrn_id, '=' );
        
                                try {
                                        if ( $qry->countQuery()->execute()->fetchField() ) {
                                                
                                                $field = $qry->execute()->fetchField();
                                                if($field) $tabs = unserialize( $field );
                                        }
                                }
                                catch ( \Exception $e ) {
                                        error_log( 'ERROR in ' . $e->getFile() . ' in Line ' . $e->getLine() . ': ' . $e->getMessage() );
                                        \Drupal::service( 'messenger' )->addError( 'ERROR: Can\'t load all parts for this view. Please contact support!' );
                                }
                        }
                        
                        return $tabs ?? [
                
                                'current_volume' => [
                                        'title' => 'Current volume',
                                        'weight' => 1,
                                        'show_on_empty' => 1
                                ],
                                
                                'about' => [
                                        'title' => 'About this journal',
                                        'weight' => 2,
                                        'show_on_empty' => 1
                                ],

                                'editorial_board' => [
                                        'title' => 'Editorial Board',
                                        'weight' => 3,
                                        'show_on_empty' => 1
                                ],

                                'manuscript_guidelines' => [
                                        'title' => 'Manuscript Guidelines',
                                        'weight' => 4,
                                        'show_on_empty' => 1
                                ],

                                'imprint' => [
                                        'title' => 'Imprint',
                                        'weight' => 5,
                                        'show_on_empty' => 1
                                ],

                                'contact' => [
                                        'title' => 'Contact',
                                        'weight' => 6,
                                        'show_on_empty' => 0
                                ],
                                
                                'archive' => [
                                        'title' => 'Archive',
                                        'weight' => 7,
                                        'show_on_empty' => 1
                                ]
                        ];
                }
        
                /**
                 * @param null $uid
                 * @return mixed
                 */
                public static function getUserEOJournals($uid = null){
                
                        $uid = $uid ?? Publisso::currentUser()->getId();
                        return \Drupal::database()->select('rwPubgoldJournalsEditorialOffice', 't')->fields('t', ['jeo_jrnid'])->condition('jeo_uid', $uid, '=')->execute()->fetchCol();
                }
                
                public static function getJournalIDsWithVolumes() : array{
                        $qry =  \Drupal::database()->select('rwPubgoldJournalVolume', 't');
                        $qry->addExpression("DISTINCT(jrn_id)", 'jrn_id');
                        return $qry->execute()->fetchCol();
                }
        }
