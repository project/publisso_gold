<?php


        namespace Drupal\publisso_gold\Controller\Manager;

        use Drupal;
        use Drupal\publisso_gold\Classes\SubmissionFunding;
        use Drupal\publisso_gold\Controller\Publisso;
        use Exception;

        /**
         * Class SubmissionFundingManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class SubmissionFundingManager {

                public static function getFunding(int $mediumID, string $mediumType, int $fundingNumber) :? SubmissionFunding{
                        if(!self::fundingExists($mediumID, $mediumType, $fundingNumber)) return null;
                        return new SubmissionFunding($mediumID, $mediumType, $fundingNumber);
                }

                public static function getFundings(int $mediumID, string $mediumType) :array{
                        $fundings = [];
                        foreach(Drupal::database()->select('rwPubgoldSubmissionFunding', 't')->fields('t', ['number'])->condition('submission_type', $mediumType, '=')->condition('submission_id', $mediumID, '=')->execute()->fetchAll() as $row){

                                $fundings[] = self::getFunding($mediumID, $mediumType, $row->number);
                        }
                        return $fundings;
                }

                public static function fundingExists(int $mediumID, string $mediumType, int $fundingNumber = null) :bool{
                        return !!Drupal::database()->select('rwPubgoldSubmissionFunding', 't')
                                              ->fields('t', [])
                                              ->condition('submission_id', $mediumID, '=')->condition('submission_type', $mediumType, '=')->condition('number', $fundingNumber, '=')
                                              ->countQuery()->execute()->fetchField();
                }

                /**
                 * @param int $mediumID
                 * @param string $mediumType
                 * @param int|null $fundingNumber
                 * @return SubmissionFunding|null
                 */
                public static function create(int $mediumID, string $mediumType, int $fundingNumber = null) :? SubmissionFunding{

                        $error = null;

                        try{
                                if(!$fundingNumber){
                                        $qry = Drupal::database()->select('rwPubgoldSubmissionFunding', 't');
                                        $qry->addExpression('MAX(number)');
                                        $maxNumber = $qry->condition('submission_type', $mediumType, '=')->condition('submission_id', $mediumID, '=')->execute()->fetchField() ?? 0;
                                        $fundingNumber = $maxNumber + 1;
                                }

                                Drupal::database()->upsert('rwPubgoldSubmissionFunding')
                                                   ->fields(['submission_id' => $mediumID, 'submission_type' => $mediumType, 'number' => $fundingNumber])
                                                   ->key('submission_id')
                                                   ->key('submission_type')
                                                   ->key('number')
                                                   ->execute();

                                return self::getFunding($mediumID, $mediumType, $fundingNumber);
                        }
                        catch(Exception $e){
                                $error = $e;
                        }
                        finally{

                                if($error){
                                        Publisso::log('Error in file '.$error->getFile().' line '.$error->getLine().': '.$error->getMessage());
                                        Drupal::messenger()->addError((string)Drupal::translation()->translate('Cant create submission-funding. See error-log for details.'));
                                        return null;
                                }
                        }

                        return null;
                }

                /**
                 * @param int $mediumID
                 * @param string $mediumType
                 * @param int|null $fundingNumber
                 * @return null|int
                 */
                public static function delete(int $mediumID, string $mediumType, int $fundingNumber = null) :? int{

                        $error = null;

                        try{
                                $qry =  Drupal::database()->delete('rwPubgoldSubmissionFunding');
                                $qry->condition('submission_type', $mediumType, '=')->condition('submission_id', $mediumID, '=');
                                if($fundingNumber) $qry->condition('number', $fundingNumber, '=');
                                return $qry->execute();
                        }
                        catch(Exception $e){
                                $error = $e;
                        }
                        finally{

                                if($error){
                                        Publisso::log('Error in file '.$error->getFile().' line '.$error->getLine().': '.$error->getMessage());
                                        Drupal::messenger()->addError((string)Drupal::translation()->translate('Cant delete submission-funding #@id. See error-log for details.', ['@id' => $fundingNumber]));
                                        return null;
                                }
                        }

                        return null;
                }
        }
