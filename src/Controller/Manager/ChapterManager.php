<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\publisso_gold\Controller\Bookchapter;
        use Drupal\publisso_gold\Controller\Medium\Book;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class ChapterManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class ChapterManager extends ControllerBase {
                
                public static function getChapters() :array{
                        
                        try{
                                return \Drupal::database()->select('rwPubgoldBookChapters', 'c')->fields('c', ['cp_id'])->execute()->fetchCol();
                        }
                        catch(\Exception $e){
                                \Drupal::service('messenger')->addError($e->getMessage());
                                return [];
                        }
                }
        
                /**
                 * @param $cp_id
                 * @param false $mediumOnly
                 * @return Bookchapter|null
                 */
                public static function getChapter($cp_id, $mediumOnly = false){
                        
                        if(in_array($cp_id, self::getChapters())){
                                return new Bookchapter($cp_id, $mediumOnly);
                        }
                        else{
                                \Drupal::service('messenger')->addError('Chapter not found!');
                                return null;
                        }
                }
                
                public static function getChaptersList(array $fields = []) :array{
                        
                        try{
                                return \Drupal::database()->select('rwPubgoldBookChapters', 't')->fields('t', array_merge(['cp_id'], $fields))->execute()->fetchAllAssoc('cp_id');
                        }
                        catch(\Exception $e){
                                \Drupal::service('messenger')->addError($e->getMessage());
                                return [];
                        }
                }
        
                /**
                 * @param $bk_id
                 * @return Bookchapter
                 * @throws \Exception
                 */
                public static function _create($bk_id) :Bookchapter{
                        
                        $id = \Drupal::database()->insert('rwPubgoldBookChapters')->fields(['cp_bkid' => $bk_id])->execute();
                        return new Bookchapter($id);
                }
        }
