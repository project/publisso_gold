<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        
        use Drupal as D;
        use Drupal\publisso_gold\Controller\Publisso as P;

        /**
         * Class AbstractManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class AbstractManager {
        
                /**
                 * @param int $cf_id
                 * @return D\Core\Database\StatementInterface|int|string|null
                 */
                public static function create(int $cf_id){
                        
                        $doc_no = D::database()->select('rwPubgoldConferenceAbstracts', 't');
                        $doc_no->addExpression('MAX(ca_doc_no)');
                        $doc_no = $doc_no->condition('ca_cfid', $cf_id, '=')->execute()->fetchField() + 1;
                        
                        $qry = D::database()->insert('rwPubgoldConferenceAbstracts')->fields([
                                'ca_created' => date('Y-m-d H:i:s'),
                                'ca_created_by_uid' => P::currentUser()->getId(),
                                'ca_cfid' => $cf_id,
                                'ca_doc_no' => $doc_no
                        ]);
                        
                        try{
                                return $qry->execute();
                        }
                        catch(\Exception $e){
                                P:log("Error in file ".__FILE__.' in line '.__LINE__.': '.$e->getMessage());
                                return null;
                        }
                }
        
                /**
                 * @param int $id
                 * @return D\publisso_gold\Controller\Submedium\ConferenceAbstract
                 */
                public static function getAbstract(int $id){
                        return new D\publisso_gold\Controller\Submedium\ConferenceAbstract($id);
                }
        
                /**
                 * @param int      $cf_id
                 * @param array    $fields
                 * @param array    $filter
                 * @param int|null $limit
                 *
                 * @return array
                 */
                public static function getAbstractList(int $cf_id, array $fields = [], array $filter = [], int $limit = null){
                        
                        $conference = ConferenceManager::getConference($cf_id);
                        $wfSchema = new D\publisso_gold\Controller\Workflow\WorkflowSchema($conference->getElement('wfschema'));
                        $entryIdentifier = $wfSchema->getEntryPointIdentifier();
                        
                        $qry = D::database()->select('rwPubgoldConferenceAbstracts', 'a');
                        $qry->join('rwPubgoldWorkflow', 'w', 'a.ca_wfid = w.wf_id');
                        
                        foreach($fields as $field){
                                $field = explode('.', $field);
                                if(count($field) == 1) array_unshift($field, 'a');
                                $qry->addField($field[0], $field[1]);
                        }
                        
                        $qry->condition('w.wf_schema_identifier', $entryIdentifier, '>');
                        $qry->condition('a.ca_cfid', $cf_id, '=');
                        
                        foreach ($filter as $_){
                                
                                $cond = $qry->andConditionGroup();
                                if(count($_) > 1){
                                        $cond->condition($_[0], $_[1], $_[2] ?? '=');
                                }
                                $qry->condition($cond);
                        }
                        
                        if($limit){
                                $qry = $qry->extend( '\Drupal\Core\Database\Query\PagerSelectExtender' )->limit( $limit );
                        }
                        
                        try{
                                return $qry->execute()->fetchAll();
                        }
                        catch(\Exception $e){
                                D::messenger()->addError($e->getMessage());
                                return [];
                        }
                }
        }