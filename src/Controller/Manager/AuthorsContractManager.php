<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;


        /**
         * Class AuthorsContractManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class AuthorsContractManager {
        
                /**
                 * @return mixed
                 */
                public static function getNameList(){
                        return \Drupal::database()->select('rwPubgoldAuthorsContracts', 't')->fields('t', ['name'])->execute()->fetchCol();
                }
        }
