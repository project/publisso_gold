<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        
        use Drupal;
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\publisso_gold\Controller\Journalarticle;

        /**
         * Class ArticleManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class ArticleManager extends ControllerBase {
        
                /**
                 * @param $jrna_id
                 * @return Journalarticle|null
                 */
                public static function getArticle($jrna_id){
                        
                        $qry = Drupal::database()->select('rwPubgoldJournalArticles', 'a')->fields('a', ['jrna_id'])->condition('jrna_id', $jrna_id, '=');
                        
                        if(!$qry->countQuery()->execute()->fetchField()){
                                Drupal::service('messenger')->addError('Article (ID: '.$jrna_id.') not found!');
                                return null;
                        }
                        
                        return new Journalarticle($jrna_id);
                }
        }
