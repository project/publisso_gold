<?php
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        use Drupal\Core\Archiver\Zip;
        use Drupal\Core\Messenger\LegacyMessenger;
        use Drupal\Core\Messenger\Messenger;
        use Drupal\Core\TempStore\PrivateTempStore;
        use Drupal\Core\Url;
        use Drupal\file\Entity\File;
        use Drupal\media\Entity\Media;
        use Drupal\publisso_gold\Controller\Publisso;
        use PhpOffice\PhpSpreadsheet\IOFactory;

        /**
         * Class ConferenceImport
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class ConferenceImport {
                
                protected static $msg;
                protected static $fs;
                protected static $tmpDirPrefix;
                protected static $tmpDir;
                
                public static function validateImportPublisso_default(string $archiveFilePath) : bool{
                        
                        self::$msg              = \Drupal::messenger();
                        self::$fs               = \Drupal::service('file_system');
                        self::$tmpDir           = Publisso::tools()->uniqid();
                        self::$tmpDirPrefix     = self::$fs->getTempDirectory().'/'.self::$tmpDir;
                        $zip     = new Zip($archiveFilePath);
                        $dataXLS = [];
                        
                        //extrace the Archive
                        $zip->extract(self::$tmpDirPrefix);
                        
                        //check if manifest-file exists
                        $directoryContent = self::$fs->scanDirectory(self::$tmpDirPrefix, '/^PUBLISSO_default\.xls(x?)/');
                        
                        if(!count($directoryContent)){
                                self::$msg->addError('Manifest-file "PUBLISSO_default.xls(x)" not found! Aborting!');
                                if(!self::$fs->deleteRecursive(self::$tmpDirPrefix)) error_log("Failed to delete directory \"".self::$tmpDirPrefix."\"");
                                return false;
                        }
                        
                        //assign manifest-file ad directory-structure
                        $manifestFile     = current($directoryContent)->filename;
                        $manifestFilePath = current($directoryContent)->uri;
                        $directoryContent = self::$fs->scanDirectory(self::$tmpDirPrefix, '/.+/');
                        
                        //read the manifest-file
                        $xls = IOFactory::load($manifestFilePath);
                        
                        $worksheetBlueprint = [
                                'Abstracts', 'Conference', 'Sessions', 'Authors', 'Affiliations', 'Translations'
                        ];
                        
                        if(count(array_diff($worksheetBlueprint, $xls->getSheetNames()))){
                                self::$msg->addError('Make sure the following tables exists in file "'.$manifestFile.'": '.implode(', ', $worksheetBlueprint));
                                return false;
                        }
                        
                        //read conference data
                        $conferenceSheet = $xls->getSheet(array_search('Conference', $xls->getSheetNames()));
                        
                        $conferenceBlueprint = [
                                'title', 'title abbr', 'color', 'ddc', 'license', 'category', 'type', 'date start', 'date end', 'logo', 'banner', 'city', 'country',
                                'venue', 'venue info', 'inviting society', 'inviting society info', 'language', 'about', 'review type', 'funding text', 'email signature',
                                'series title', 'imprint', 'manuscript guidelines', 'award', 'award info', 'award title', 'publisher abbr', 'publisher', 'publisher city',
                                'publishing year'
                        ];
                        
                        $conferenceData = array_filter($conferenceSheet->toArray()[0]) ?? [];
                        
                        if(count(array_diff($conferenceData, $conferenceBlueprint))){
                                self::$msg->addError('Make sure the following columns exists and named (in first row) in table "Conference": '.implode(', ', $conferenceBlueprint));
                                return false;
                        }
                        
                        $dataXLS['Conference'] = array_combine($conferenceData, $conferenceSheet->toArray()[1] ?? []);
        
                        //read abstract data
                        $abstractSheet = $xls->getSheet(array_search('Abstracts', $xls->getSheetNames()));
                        
                        $abstractBlueprint = [
                                'no', 'title', 'keywords', 'submission type', 'category', 'language', 'main text', 'references', 'author', 'corresponding', 'presenting',
                                'corporations', 'conflict of interest', 'award info', 'attachments', 'session', 'doc no', 'doi', 'published', 'received', 'accepted',
                                'revised', 'license', 'version'
                        ];
                        
                        $abstractHead = array_filter($abstractSheet->toArray()[0]) ?? [];
                        
                        if(count(array_diff($abstractHead, $abstractBlueprint))){
                                self::$msg->addError('Make sure the following columns exists and named (in first row) in table "Abstracts": '.implode(', ', $abstractBlueprint));
                                return false;
                        }
                        
                        $abstractData = $abstractSheet->toArray();
        
                        $data = [];
                        for($i = 1; $i < count($abstractData); $i++){
                                
                                $_ = [];
        
                                if(!empty($abstractData[$i][array_search('no', $abstractHead)])) { // no must be set
                                        
                                        foreach ($abstractHead as $colNo => $colVal) {
                                                $_[$colVal] = $abstractData[$i][$colNo];
                                        }
        
                                        $data[] = $_;
                                }
                        }
                        
                        $dataXLS['Abstracts'] = $data;
                        
                        //read the session-list
                        $sessionSheet = $xls->getSheet(array_search('Sessions', $xls->getSheetNames()));
                        
                        $sessionBlueprint = [
                                'no', 'type', 'title', 'day from', 'day to', 'parent', 'abstract no'
                        ];
        
                        $sessionHead = array_filter($sessionSheet->toArray()[0]) ?? [];
        
                        if(count(array_diff($sessionHead, $sessionBlueprint))){
                                self::$msg->addError('Make sure the following columns exists and named (in first row) in table "Sessions": '.implode(', ', $sessionBlueprint));
                                return false;
                        }
                        
                        $sessionData = $sessionSheet->toArray();
                        
                        for($i = 1; $i < count($sessionData); $i++){
                                
                                $_ = [];
                                
                                if(!empty($sessionData[$i][array_search('no', $sessionHead)])) {
                                        
                                        foreach ($sessionHead as $colNo => $colVal){
                                                $_[$colVal] = $sessionData[$i][$colNo];
                                        }
                                }
                                
                                $dataXLS['Sessions'][] = $_;
                        }
                        
                        //read the affiliations
                        $affiliationSheet = $xls->getSheet(array_search('Affiliations', $xls->getSheetNames()));
                        
                        $affiliationBlueprint = [
                                'no', 'department', 'institute', 'city', 'country'
                        ];
        
                        $affiliationHead = array_filter($affiliationSheet->toArray()[0]) ?? [];
        
                        if(count(array_diff($affiliationHead, $affiliationBlueprint))){
                                self::$msg->addError('Make sure the following columns exists and named (in first row) in table "Affiliations": '.implode(', ', $affiliationBlueprint));
                                return false;
                        }
                        
                        $affiliationData = $affiliationSheet->toArray();
                        
                        for($i = 1; $i < count($affiliationData); $i++){
                                
                                $_ = [];
        
                                if(!empty($affiliationData[$i][array_search('no', $affiliationHead)])) {
                
                                        foreach ($affiliationHead as $colNo => $colVal){
                                                $_[$colVal] = $affiliationData[$i][$colNo];
                                        }
                                }
                                
                                $dataXLS['Affiliations'][] = $_;
                        }
        
                        //read the authors
                        $authorSheet = $xls->getSheet(array_search('Authors', $xls->getSheetNames()));
        
                        $authorBlueprint = [
                                'no', 'firstname', 'lastname', 'affiliation', 'email'
                        ];
        
                        $authorHead = array_filter($authorSheet->toArray()[0]) ?? [];
        
                        if(count(array_diff($authorHead, $authorBlueprint))){
                                self::$msg->addError('Make sure the following columns exists and named (in first row) in table "Authors": '.implode(', ', $affiliationBlueprint));
                                return false;
                        }
        
                        $authorData = $authorSheet->toArray();
        
                        for($i = 1; $i < count($authorData); $i++){
                
                                $_ = [];
                
                                if(!empty($authorData[$i][array_search('no', $authorHead)])) {
                        
                                        foreach ($authorHead as $colNo => $colVal){
                                                $_[$colVal] = $authorData[$i][$colNo];
                                        }
                                }
                
                                $dataXLS['Authors'][] = $_;
                        }
        
                        //read the translations
                        $translationSheet = $xls->getSheet(array_search('Translations', $xls->getSheetNames()));
        
                        $translationBlueprint = [
                                'abstract no', 'field', 'value', 'language'
                        ];
        
                        $translationHead = array_filter($translationSheet->toArray()[0]) ?? [];
        
                        if(count(array_diff($translationHead, $translationBlueprint))){
                                self::$msg->addError('Make sure the following columns exists and named (in first row) in table "Translations": '.implode(', ', $translationBlueprint));
                                return false;
                        }
        
                        $translationData = $translationSheet->toArray();
        
                        for($i = 1; $i < count($translationData); $i++){
                
                                $_ = [];
                
                                if(!empty($translationData[$i][array_search('no', $translationHead)])) {
                        
                                        foreach ($translationHead as $colNo => $colVal){
                                                $_[$colVal] = $translationData[$i][$colNo];
                                        }
                                }
                
                                $dataXLS['Translations'][] = $_;
                        }
                        
                       
                        if(self::prepareImportPublisso_default($dataXLS, $directoryContent)){
                                if(!self::$fs->deleteRecursive(self::$tmpDirPrefix)) error_log("Failed to delete directory \"".self::$tmpDirPrefix."\"");
                                return true;
                        }
                        
                        return false;
                }
                
                public static function prepareImportPublisso_default(array &$data, array &$directoryContent) : bool{
                        
                        //make directory content indexed array for search purposes
                        array_walk($directoryContent, function(&$item, $key){
                                $item = (array)$item;
                        });
                        
                        $directoryContent = array_values($directoryContent);
                        
                        foreach($directoryContent as &$entry){
        
                                $subPath = str_replace(self::$tmpDirPrefix, '', $entry['uri']);
                                $uri     = 'public://publisso_gold/conferences/import/tmp/'.self::$tmpDir.$subPath;
                                $dir     = self::$fs->dirname($uri);
                                
                                $file = File::create([
                                        'filename' => $entry['filename'],
                                        'uri' => $uri
                                ]);
                                
                                self::$fs->mkdir($dir, 0750, true);
                                file_put_contents($uri, file_get_contents($entry['uri']));
                                $file->save();
                                $entry['fid'] = $file->id();
                        }
                        
                        //sort and order sessions
                        $sessionsRaw = $data['Sessions'];
                        $colNo          = array_column($sessionsRaw, 'no');
                        $colParent      = array_column($sessionsRaw, 'parent');
                        
                        array_multisort($colParent, $colNo, $sessionsRaw);
                        
                        $sessions = [];
        
                        $func = function(array &$arySessions, array $item, array &$abstracts) use (&$func){
        
                                if($item['type'] == 'Abstract'){
                                        
                                        if(null !== ($key = array_search($item['no'], array_column($abstracts, 'no')))){
                                                    $item['title'] = $abstracts[$key]['title'];
                                        }
                                }
                                
                                if($item['parent']){
                                        
                                        foreach ($arySessions as $k => &$session) {
                                                
                                                if ($item['parent'] == $session['no']) {
                                                        $arySessions[$k]['childs'][] = $item;
                                                }
                                                else if (array_key_exists('childs', $session)) {
                                                        $func($session['childs'], $item, $abstracts);
                                                }
                                        }
                                }
                                else{
                                        $arySessions[] = $item;
                                }
                        };
                        
                        foreach($sessionsRaw as &$sessionItem){
                                $func($sessions, $sessionItem, $data['Abstracts']);
                        }
                        
                        $data['SessionsMapped'] = $sessions;
                        
                        $conference = &$data['Conference'];
                        
                        //checking conference-logo
                        if($conference['logo'] && !preg_match('/^file:\/\/(.+)/', $conference['logo'], $matches)){
                                self::messenger()->addError('File "'.$conference['logo'].'" (used as conference-logo) not found!');
                                return false;
                        }
                        
                        if(is_array($matches) && count($matches) == 2){
                                $logo = explode('/', $matches[1]);
                                $logo = end($logo);
                                
                                if($key = array_search($logo, array_column($directoryContent, 'filename'))){
                                        $path = $directoryContent[$key]['uri'];
                                        if(!(is_file($path) && is_readable($path))){
                                                self::messenger()->addError('Logo-file "'.$logo.'" is not readable!');
                                        }
                                        else{
                                                $conference['logo_fid'] = $directoryContent[$key]['fid'];
                                        }
                                }
                        }
        
                        //checking conference-banner
                        
                        if($conference['banner'] && !preg_match('/^file:\/\/(.+)/', $conference['banner'], $matches)){
                                self::messenger()->addError('File "'.$conference['banner'].'" (used as conference-banner) not found!');
                                return false;
                        }
        
                        if(is_array($matches) && count($matches) == 2){
                                $banner = explode('/', $matches[1]);
                                $banner = end($banner);
                
                                if($key = array_search($banner, array_column($directoryContent, 'filename'))){
                                        $path = $directoryContent[$key]['uri'];
                                        if(!(is_file($path) && is_readable($path))){
                                                self::messenger()->addError('Banner-file "'.$banner.'" is not readable!');
                                        }
                                        else{
                                                $conference['banner_fid'] = $directoryContent[$key]['fid'];
                                        }
                                }
                        }
                        
                        //checking if embedded files in HTML are provided
                        $items = [&$data['Conference']['about'],&$data['Conference']['imprint'], &$data['Conference']['manuscript guidelines']];
                        
                        foreach($data['Abstracts'] as &$abstract){ $items[] = &$abstract['main text']; }
                        
                        foreach($items as &$item){
                                
                                if(preg_match_all('|file://([^"]+)|', $item, $matches)){
                                        
                                        foreach($matches[1] as $index => $file){
                                                
                                                $path = self::$tmpDirPrefix.'/'.$file;
                                                
                                                if(null !== ($key = array_search($path, array_column($directoryContent, 'uri')))){
                                                        
                                                        if(!(is_file($path) && is_readable($path))){
                                                                self::messenger()->addError('Inline file "'.$file.'" is not readable!');
                                                                continue;
                                                        }
                                                }
                                                else{
                                                        self::messenger()->addError('Inline file "'.$file.'" doesn\'t exists!');
                                                        continue;
                                                }
                                                
                                                $file = File::load($directoryContent[$key]['fid']);
                                                $item = str_replace($matches[0][$index], $file->url(), $item);
                                        }
                                }
                        }
                        
                        //assign affiliations to the authors
                        
                        foreach($data['Authors'] as &$author){
                                
                                $author['affiliations'] = [];
                                
                                foreach(preg_split('/,|\./', $author['affiliation']) as $affNo){
                                        
                                        $key = array_search($affNo, array_column($data['Affiliations'], 'no'));
                                        
                                        if($key !== null){
                                                $author['affiliations'][] = $data['Affiliations'][$key];
                                        }
                                }
                        }
                        
                        //assign the authors to the abstracts
                        
                        foreach($data['Abstracts'] as &$abstract){
                                
                                $authorNums             = preg_split('/,|\./', $abstract['author']);
                                $correspondingNums      = preg_split('/,|\./', $abstract['corresponding']);
                                $presentingNums         = preg_split('/,|\./', $abstract['presenting']);
                                $abstract['authors']    = [];
                                
                                foreach($authorNums as $authorNum){
                                        
                                        $authorKey = array_search($authorNum, array_column($data['Authors'], 'no'));
                                        if($authorKey === null) continue;
                                        
                                        $corresponding  = in_array($authorNum, $correspondingNums);
                                        $presenting     = in_array($authorNum, $presentingNums   );
                                        
                                        $author = $data['Authors'][$authorKey];
                                        $author['corresponding'] = (int)$corresponding;
                                        $author['presenting'   ] = (int)$presenting;
                                        
                                        $abstract['authors'][] = $author;
                                }
                        }
                        
                        //map abstracts to session-items
                        foreach($data['Sessions'] as &$session){
                                
                                if($session['type'] == 'Abstract'){
                                        $key = array_search($session['abstract no'], array_column($data['Abstracts'], 'no'));
                                        if($key !== null) $session['abstract'] = $data['Abstracts'][$key];
                                }
                        }
                        
                        $tmpStore = \Drupal::service('tempstore.private')->get('conference.import');
                        $tmpStore->set('data', serialize($data));
                        $tmpStore->set('path', self::$tmpDirPrefix);
                        $tmpStore->set('files', serialize($directoryContent));
                        
                        return true;
                }
        
                /**
                 * @return LegacyMessenger|\Drupal\Core\Messenger\MessengerInterface
                 */
                protected static function messenger(){
                        if(self::$msg) return self::$msg; else return \Drupal::messenger();
                }
        }