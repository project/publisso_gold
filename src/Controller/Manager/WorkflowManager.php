<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\Workflow\AbstractWorkflow;
        use Drupal\publisso_gold\Controller\Workflow\ArticleWorkflow;
        use Drupal\publisso_gold\Controller\Workflow\ChapterErratumWorkflow;
        use Drupal\publisso_gold\Controller\Workflow\ChapterWorkflow;
        use Drupal\publisso_gold\Controller\Workflow\WorkflowFactory;

        /**
         * Class WorkflowManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class WorkflowManager {
        
                /**
                 * @param int|null $itemID
                 * @return AbstractWorkflow|ArticleWorkflow|ChapterErratumWorkflow|ChapterWorkflow|int|null
                 */
                public static function getItem(int $itemID = null){
                        if(!$itemID) return $itemID;
                        $qry = \Drupal::database()->select('rwPubgoldWorkflow', 'w')->fields('w', ['wf_type']);
                        $qry->condition('wf_id', $itemID, '=');
                        $type = $qry->execute()->fetchField();
                        
                        switch ( $type ) {
                                case 'bcs':
                                        return new ChapterWorkflow( $itemID );
                                        break;
                                case 'jas':
                                        return new ArticleWorkflow( $itemID );
                                        break;
                                case 'bce':
                                        return new ChapterErratumWorkflow( $itemID );
                                        break;
                                case 'cas':
                                        return new AbstractWorkflow( $itemID );
                                        break;
                                case '':
                                        Publisso::tools()->logMsg( "ERROR: No workflow-item-type for item $itemID found!" );
                                        return null;
                                        break;
                                default:
                                        Publisso::tools()->logMsg( "ERROR: Workflow-item-type $type not implemented!" );
                                        return null;
                        }
                }
        
                /**
                 * @param string $type
                 * @param int $mediumID
                 * @return ArticleWorkflow|ChapterErratumWorkflow|ChapterWorkflow|null
                 */
                public static function create(string $type, int $mediumID){
                        return WorkflowFactory::create($type, $mediumID);
                }
        
                /**
                 * @param array $fields
                 * @param array $filter
                 * @param int|null $limit
                 * @return mixed
                 */
                public static function getItems(array $fields = [], array $filter = [], int $limit = null){
                        
                        if(count($fields) && !in_array('wf_id', $fields)) $fields[] = 'wf_id';
                        $qry = \Drupal::database()->select('rwPubgoldWorkflow', 'w')->fields('w', $fields);
                        
                        foreach($filter as $_){
                                $qry->condition($_[0], $_[1], $_[2] ?? '=');
                        }
                        
                        if($limit){
                                $qry = $qry->extend( '\Drupal\Core\Database\Query\PagerSelectExtender' )->limit( $limit );
                        }
                        
                        return $qry->execute()->fetchAllAssoc('wf_id');
                }
                
                public static function countFinishedReviews(int $wf_id) :int{
                        return \Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', [])
                                ->condition('wfid', $wf_id, '=')->condition('state', 'finished', '=')->countQuery()->execute()->fetchField();
                }
        
                public static function hasFinishedReviews(int $wf_id) :bool{
                        return !!self::countFinishedReviews($wf_id);
                }
        }
