<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        
        use Drupal\publisso_gold\Controller\License;

        /**
         * Class LicenseManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class LicenseManager {
        
                /**
                 * @param int $id
                 *
                 * @return bool
                 */
                public static function exists(int $id) : bool{
                        return !!\Drupal::database()->select('rwPubgoldLicenses', 't')->fields('t', [])->condition('id', $id, '=')->countQuery()->execute()->fetchField();
                }
        
                /**
                 * @param int $id
                 *
                 * @return License|null
                 */
                public static function get(int $id) :? License{
                        if(self::exists($id)) return new License($id);
                        else return null;
                }
        
                /**
                 * @return array
                 */
                public static function getStandardList() : array{
                        return \Drupal::database()->select('rwPubgoldLicenses', 't')->fields('t', ['id', 'name', 'description'])->execute()->fetchAllAssoc('id');
                }
        
                /**
                 * @return array
                 */
                public static function getFormSelectOptions() : array{
                        $ret = [];
                        
                        foreach(self::getStandardList() as $_){
                                $ret[$_->id] = $_->name;
                        }
                        
                        return $ret;
                }
        }