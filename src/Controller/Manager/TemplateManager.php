<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        use Drupal\publisso_gold\Controller\Template;

        /**
         * Class TemplateManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class TemplateManager{
        
                /**
                 * @param $name
                 * @param string $namespace
                 * @return Template
                 */
                public static function load($name, $namespace = 'publisso') :Template {
                        
                        if(strtolower($namespace) != 'publisso' && !is_dir(self::getAssetsDir().'/'.$namespace)) $namespace = 'publisso';
                        if($namespace == 'publisso') $dir = (is_dir(self::getAssetsDir().'/'.$namespace) ? '$namespace' : '');
                        else $dir = $namespace;
                        
                        //Fallback für altes Schema
                        $pathAbsolute = false;
                        if(is_file(self::getAssetsDir().'/'.$dir.'/'.$name.'.tmpl')) {
                                $name = self::getAssetsDir() . '/' . ( $dir ? "$dir/" : '' ) . $name . '.tmpl';
                                $pathAbsolute = true;
                        }
                        
                        $template = new Template();
                        $template->get($name, $pathAbsolute);
                        return $template;
                }
        
                public static function getAssetsDir(bool $absolute = true) :string{
                        return ($absolute ? DRUPAL_ROOT : '').'/'.\Drupal::service('module_handler')->getModule('publisso_gold')->getPath().'/Assets/Templates';
                }
        
                /**
                 * @return Template
                 */
                public static function skel(){
                        return new Template();
                }
        }
