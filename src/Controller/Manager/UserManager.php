<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\User;

        /**
         * Class UserManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class UserManager {
        
                /**
                 * @param array $fields
                 * @return |null
                 */
                public static function getIDsByField(array $fields){
                        
                        $qry = \Drupal::database()->select('rwPubgoldUsers', 'u')->fields('u', ['id']);
                        foreach($fields as $k => $v) $qry->condition($k, $v, '=');
                        
                        try{
                                return $qry->execute()->fetchCol();
                        }
                        catch(\Exception $e){
                                Publisso::tools()->logMsg('Error in '.__FILE__.' line '.__LINE__.': '.$e->getMessage());
                                return null;
                        }
                }
        
                /**
                 * @param int $id
                 * @return User
                 */
                public static function getUserFromID(int $id){
                        return new User($id);
                }
        
                /**
                 * @param $username
                 * @param $password
                 *
                 * @return User|null
                 */
                public static function getUserFromNameAndPassword($username, $password){
                        
                        $qry = \Drupal::database()->select( 'rwPubgoldUsers', 't' )->fields( 't', [ 'id' ] );
                        $cond1 = $qry->andConditionGroup();
                        $cond2 = $qry->orConditionGroup();
                        $cond2->condition('user', $username)->condition('initial_email', $username)->condition('zbmed_kennung', $username);
                        $cond1->condition($cond2)->where('password = PASSWORD(:password)', [':password' => $password]);
                        $qry->condition($cond1);
                        
                        try{
                                if(!$qry->countQuery()->execute()->fetchField()) return null;
                                return new User($qry->execute()->fetchField());
                        }
                        catch(\Exception $e){
                                Publisso::tools()->logMsg('Error in '.__FILE__.' in line '.__LINE__.': '.$e->getMessage());
                                return null;
                        }
                }
        
                /**
                 * @param string $username
                 * @param string $password
                 * @param string $firstname
                 * @param string $lastname
                 * @param string $email
                 *
                 * @return bool|User
                 */
                public static function createUser(string $username, string $password, string $firstname, string $lastname, string $email, array $additionalFields = []){
                        
                        $qry = \Drupal::database()->insert('rwPubgoldUsers')->fields(array_merge([
                                'user' => $username,
                                'password' => \Drupal::service( 'publisso_gold.tools' )->getDBEncPassword( $password ),
                                'active' => 1
                        ], $additionalFields ?? []));
                        
                        try{
                                $id = $qry->execute();
                                
                                \Drupal::database()->insert( 'rwPubgoldUserroles' )->fields( [
                                     'user_id' => $id,
                                     'role_id' => \Drupal::service( 'publisso_gold.setup' )->getValue( 'default.user.role.id' ),
                                ])->execute();
                                
                                \Drupal::database()->insert( 'rwPubgoldUserProfiles' )->fields( [
                                        'up_uid'       => $id,
                                        'up_email'     => $email,
                                        'up_firstname' => $firstname,
                                        'up_lastname'  => $lastname,
                                ])->execute();
                                
                                $user = self::getUserFromID($id);
                                return $user;
                        }
                        catch(\Exception $e){
                                Publisso::tools()->logMsg('Error in '.__FILE__.' in line '.__LINE__.': '.$e->getMessage());
                                return false;
                        }
                }
        }
