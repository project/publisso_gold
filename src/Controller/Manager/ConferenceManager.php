<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Database\Database;
        use Drupal\publisso_gold\Controller\Medium\Conference;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\User;

        /**
         * Class ConferenceManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class ConferenceManager extends ControllerBase {
                
                public static function getConferences(array $filter = [], int $limit = null) :array{
                        
                        try{
                                $qry = \Drupal::database()->select('rwPubgoldConferences', 'c')->fields('c', ['cf_id']);
                                #print_r($filter); exit;
                                foreach($filter as $key => $value){
                                        
                                        switch(strtolower($key)){
                                                
                                                case 'status':
                                                        
                                                        switch(strtolower($value)){
                                                                
                                                                case 'published':
                                                                        $qry->condition('cf_published', 1, '=');
                                                                        break;
                                                                        
                                                                case 'by_ids':
                                                                        $qry->condition('cf_id', (array)$value, 'IN');
                                                                        break;
                                                                        
                                                                case 'submission_possible':
                                                                        $qry->where(
                                                                                <<<EOT
                                                                                case when (isnull(cf_date_submission_start) and isnull(cf_date_submission_end)) then 0
                                                                                  else (
                                                                                      case
                                                                                          when not isnull(cf_date_submission_start) and not isnull(cf_date_submission_end)
                                                                                              then date_format(now(), '%y-%m-%d') between cf_date_submission_start and cf_date_submission_end
                                                                                          else (
                                                                                              case
                                                                                                  when not isnull(cf_date_submission_start)
                                                                                                      then date_format(now(), '%y-%m-%d') >= cf_date_submission_start
                                                                                                  else (
                                                                                                      case
                                                                                                          when not isnull(cf_date_submission_end)
                                                                                                              then date_format(now(), '%y-%m-%d') <= cf_date_submission_end end
                                                                                                      ) end
                                                                                              ) end
                                                                                      ) end
                                                                                EOT
                                                                        );
                                                                        $qry->condition('cf_published', 1, '!=');
                                                                        break;
                                                                        
                                                                case 'submission_closed':
                                                                        $qry->where(
                                                                                <<<EOT
                                                                                not case when (isnull(cf_date_submission_start) and isnull(cf_date_submission_end)) then 0
                                                                                  else (
                                                                                      case
                                                                                          when not isnull(cf_date_submission_start) and not isnull(cf_date_submission_end)
                                                                                              then date_format(now(), '%y-%m-%d') between cf_date_submission_start and cf_date_submission_end
                                                                                          else (
                                                                                              case
                                                                                                  when not isnull(cf_date_submission_start)
                                                                                                      then date_format(now(), '%y-%m-%d') >= cf_date_submission_start
                                                                                                  else (
                                                                                                      case
                                                                                                          when not isnull(cf_date_submission_end)
                                                                                                              then date_format(now(), '%y-%m-%d') <= cf_date_submission_end end
                                                                                                      ) end
                                                                                              ) end
                                                                                      ) end
                                                                                EOT
                                                                        );
                                                                        $qry->condition('cf_published', 1, '!=');
                                                                        break;
                                                        }
                                                        
                                                        break;
                                                        
                                                case 'title_contains':
                                                        $qry->condition('cf_title', "%$value%", 'LIKE');
                                                        break;
                                                        
                                                case 'society_contains':
                                                        $qry->condition('cf_inviting_society', "%$value%", 'LIKE');
                                                        break;
                                                        
                                                case 'closed_flag':
                                                        $qry->condition('cf_submissions_closed', $value, '=');
                                                        break;
                                        }
                                }
                                
                                if($limit){
                                        $qry = $qry->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit($limit);
                                }
                                
                                return $qry->execute()->fetchCol();
                        }
                        catch(\Exception $e){
                                \Drupal::service('messenger')->addError($e->getMessage());
                                return [];
                        }
                }
        
                /**
                 * @param int $cf_id
                 * @return Conference|null
                 */
                public static function getConference(int $cf_id){
                        
                        if(in_array($cf_id, self::getConferences())){
                                return new Conference($cf_id);
                        }
                        else{
                                \Drupal::service('messenger')->addError('Conference not found!');
                                return null;
                        }
                }
                
                public static function conferenceExists(int $cf_id) :bool {
                        return !!\Drupal::database()->select('rwPubgoldConferences', 'c')->fields('c', [])->condition('cf_id', $cf_id, '=')->countQuery()->execute()->fetchField();
                }
                
                public static function getConferencesList(array $fields = []) :array{
                        
                        try{
                                return \Drupal::database()->select('rwPubgoldConferences', 't')->fields('t', array_merge(['cf_id'], $fields))->execute()->fetchAllAssoc('cf_id');
                        }
                        catch(\Exception $e){
                                \Drupal::service('messenger')->addError($e->getMessage());
                                return [];
                        }
                }
        
                public static function _create() :Conference{
                
                        $id = \Drupal::database()->insert('rwPubgoldConferences')->fields(['cf_creator_uid' => \Drupal::service('session')->get('user')['id'] ?? 0])->execute();
                        return new Conference($id);
                }
        
                /**
                 * @param $cf_id
                 * @return mixed
                 */
                public static function getMagerUIDs($cf_id){
                        return \Drupal::database()->select('rwPubgoldConferencesConferenceManager', 't')->fields('t', ['cm_uid'])->condition('cm_cfid', $cf_id, '=')->execute()->fetchCol();
                }
        
                /**
                 * @param null $uid
                 * @return bool
                 */
                public static function isManagerOfAnyConference($uid = null){
                        
                        if($uid === null) $uid = Publisso::currentUser()->getId();
                        return !!\Drupal::database()->select('rwPubgoldConferencesConferenceManager','t')->fields('t', [])->condition('cm_uid', $uid, '=')->countQuery()->execute()->fetchField();
                }
        
                /**
                 * @param int $cf_id
                 * @param null $uid
                 * @return bool
                 */
                public static function isManagerForConference(int $cf_id, $uid = null){
                        
                        if($uid === null) $uid = Publisso::currentUser()->getId();
                        return !!\Drupal::database()->select('rwPubgoldConferencesConferenceManager','t')->fields('t', [])->condition('cm_cfid', $cf_id, '=')->condition('cm_uid', $uid, '=')->countQuery()->execute()->fetchField();
                }
        
                /**
                 * @param string    $role (allowed roles: manager, eo, eic, editor, reviewer)
                 * @param User|null $user
                 *
                 * @return array
                 */
                public static function getConferencesForUserRole(string $role, User $user = null) :array{
                        
                        $roles = ['manager', 'eo', 'eic', 'editor', 'reviewer'];
                        if(!in_array($role, $roles)) return [];
                        
                        if(!$user) $user = Publisso::currentUser();
                        
                        if($user->isPublisso() || $user->isAdministrator()){
                                return \Drupal::database()->select('rwPubgoldConferences', 't')->fields('t', ['cf_id'])->execute()->fetchCol();
                        }
                        
                        switch($role){
                                case 'manager':
                                        return \Drupal::database()->select('rwPubgoldConferencesConferenceManager', 't')
                                                ->fields('t', ['cm_cfid'])->condition('cm_uid', $user->getId())->execute()->fetchCol();
                                        break;
                                        
                                case 'eo':
                                        return \Drupal::database()->select('rwPubgoldConferencesEditorialOffice', 't')
                                                      ->fields('t', ['ceo_cfid'])->condition('ceo_uid', $user->getId())->execute()->fetchCol();
                                        break;
                                        
                                case 'eic':
                                        return \Drupal::database()->select('rwPubgoldConferencesEditorsInChief', 't')
                                                      ->fields('t', ['ceic_cfid'])->condition('ceic_uid', $user->getId())->execute()->fetchCol();
                                        break;
                                        
                                case 'editor':
                                        return \Drupal::database()->select('rwPubgoldConferencesEditors', 't')
                                                      ->fields('t', ['ce_conferenceid'])->condition('ce_uid', $user->getId())->execute()->fetchCol();
                                        break;
                                        
                                case 'reviewer':
                                        
                                        $wfIDs = Database::getConnection()->select('rwPubgoldWorkflow', 't')->fields('t', ['wf_id'])
                                                ->condition('wf_type', 'cas', '=')->where('FIND_IN_SET('.$user->getId().', `wf_assigned_to_reviewer`)')
                                                ->execute()->fetchCol();
                                        
                                        $cfIDs = [];
                                        
                                        foreach($wfIDs as $wf_id){
                                                $workflow = WorkflowManager::getItem($wf_id);
                                                if(!in_array($workflow->getDataElement('cf_id'), $cfIDs)) $cfIDs[] = $workflow->getDataElement('cf_id');
                                        }
                                        
                                        return $cfIDs;
                                        break;
                        }
                }
        
                /**
                 * @param null $cf_id
                 * @return array[]|mixed
                 */
                public static function getHTMLViewTabs($cf_id = null){
                
                        if($cf_id) {
                        
                                $qry = \Drupal::database()->select( 'rwPubgoldConferences', 'c' )->fields( 'c', [ 'cf_view_tabs' ] )->condition( 'cf_id', $cf_id, '=' );
                        
                                try {
                                        if ( $qry->countQuery()->execute()->fetchField() ) {
                                        
                                                $field = $qry->execute()->fetchField();
                                                if($field) $tabs = unserialize( $field );
                                        }
                                }
                                catch ( \Exception $e ) {
                                        error_log( 'ERROR in ' . $e->getFile() . ' in Line ' . $e->getLine() . ': ' . $e->getMessage() );
                                        \Drupal::service( 'messenger' )->addError( 'ERROR: Can\'t load all parts for this view. Please contact support!' );
                                }
                        }
                
                        return $tabs ?? [
                                        'welcome' => [
                                                'title' => 'Welcome',
                                                'weight' => 1,
                                                'show_on_empty' => 1,
                                                'template' => 'conference-welcome'
                                        ],
                                
                                        'abstracts' => [
                                                'title' => 'Sessions',
                                                'weight' => 2,
                                                'show_on_empty' => 1,
                                                'template' => 'conference-abstracts'
                                        ],

                                        'manuscript_guidelines' => [
                                                'title' => 'Manuscript guidelines',
                                                'weight' => 3,
                                                'show_on_empty' => 0,
                                                'template' => 'conference-manuscript-guidelines'
                                        ],
                                        
                                        'imprint' => [
                                                'title' => 'Imprint',
                                                'weight' => 6,
                                                'show_on_empty' => 0,
                                                'template' => 'conference-imprint'
                                        ],
                                
                                        'contact' => [
                                                'title' => 'Contact',
                                                'weight' => 7,
                                                'show_on_empty' => 0,
                                                'template' => 'conference-contact'
                                        ]
                                ];
                }
        }
