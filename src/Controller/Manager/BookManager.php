<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\publisso_gold\Controller\Medium\Book;
        use Drupal\publisso_gold\Controller\Workflow\ChapterWorkflow;
        use Drupal\publisso_gold\Controller\Workflow\WorkflowSchema;
        use Symfony\Component\HttpFoundation\Request;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class BookManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class BookManager extends ControllerBase {
        
                /**
                 * @param $bk_id
                 * @param Request $request
                 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|void
                 */
                public function addchapter ($bk_id, Request $request ) {
                        $request = \Drupal::requestStack()->getCurrentRequest();
                        
                        $book = new Book( $bk_id );
                        $tempstore = \Drupal::service( 'user.private_tempstore' )->get( 'publisso_gold' );
                        
                        $wf_id = $request->query->get('wf_id') ?? null;
                        $tmp_id = $request->query->get('continue') ?? null;
                        
                        if ( $tmp_id ) {
                        
                                if ( !\Drupal::database()->select( 'rwPubgoldBookchapter_temp', 't' )->fields( 't', [] )->condition( 'id', $tmp_id, '=' )->countQuery()->execute()->fetchField() ) {
                                        return Publisso::tools()->accessDenied( (string)$this->t( 'Running submission not found' ) );
                                }
                        
                                if ( !\Drupal::database()->select( 'rwPubgoldBookchapter_temp', 't' )->fields( 't', [] )->condition( 'id', $tmp_id, '=' )->condition( 'uid', Publisso::currentUser()->getId() )->countQuery()->execute()->fetchField() ) {
                                        return Publisso::tools()->accessDenied();
                                }
                        
                                $tempstore->set( 'bookchapter_tmpid', $tmp_id );
                        }
                        
                        if(!$book->getElement('wfschema')) {
                                $form_addchapter = \Drupal::formBuilder()->getForm( "Drupal\publisso_gold\Form\publisso_goldBookAddChapter", [ 'bk_id' => $bk_id, 'wf_id' => $wf_id, 'tmp_id' => $tmp_id ] );
        
                                if ( !$wf_id ) //new submission
                                        $form_addchapter_new = \Drupal::formBuilder()->getForm( "Drupal\publisso_gold\Form\SubmitBookChapter", [ 'bk_id' => $bk_id, 'tmp_id' => $tmp_id ] );
                                else
                                        $form_addchapter_new = \Drupal::formBuilder()->getForm( "Drupal\publisso_gold\Form\RevisionBookChapter", [ 'bk_id' => $bk_id, 'wf_id' => $wf_id, 'tmp_id' => $tmp_id ] );
        
                                return $form_addchapter_new;
                        }
                        else{
                                if(!$tmp_id){
                                        $workflow = new ChapterWorkflow();
                                        $schema = new WorkflowSchema($book->getElement('wfschema'));
                                        $workflow->init($bk_id, 'bcs', $schema->getEntryPointID());
                                        $tmp_id = $workflow->getElement('id');
                                }
        
                                return $this->redirect( 'publisso_gold.workflow.item', ['wf_id' => $workflow->getElement('id')]);
                        }
                }
                
                public static function getBooks() :array{
                        
                        try{
                                return \Drupal::database()->select('rwPubgoldBooks', 'c')->fields('c', ['bk_id'])->execute()->fetchCol();
                        }
                        catch(\Exception $e){
                                \Drupal::service('messenger')->addError($e->getMessage());
                                return [];
                        }
                }
        
                /**
                 * @param $bk_id
                 * @param bool $loadChilds
                 * @return Book|null
                 */
                public static function getBook($bk_id, $loadChilds = true){
                        
                        if(in_array($bk_id, self::getBooks())){
                                return new Book($bk_id, $loadChilds);
                        }
                        else{
                                \Drupal::service('messenger')->addError('Book not found!');
                                return null;
                        }
                }
        
                /**
                 * @param array $fields
                 * @param array $orderBy
                 * @param null $limit
                 * @return array
                 */
                public static function getBooksList(array $fields = [], array $orderBy = [], $limit = null) :array{
                        
                        try{
                                $qry = \Drupal::database()->select('rwPubgoldBooks', 't')->fields('t', array_merge(['bk_id'], $fields));
                                foreach($orderBy as $_){
                                        $qry->orderBy($_[0], $_[1] ?? 'ASC');
                                }
                                
                                if($limit){
                                        $qry = $qry->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit($limit);
                                }
                                
                                return $qry->execute()->fetchAllAssoc('bk_id');
                        }
                        catch(\Exception $e){
                                \Drupal::service('messenger')->addError($e->getMessage());
                                return [];
                        }
                }
        
                public static function _create() :Book{
                
                        $id = \Drupal::database()->insert('rwPubgoldBook')->fields(['bk_public' => 0])->execute();
                        return new Book($id);
                }
        
                /**
                 * @param int $bookID
                 * @return bool
                 */
                public static function bookExists(int $bookID){
                        return !!\Drupal::database()->select('rwPubgoldBooks', 'b')->fields('b', [])->condition('bk_id', $bookID, '=')->countQuery()->execute()->fetchField();
                }
        
                /**
                 * @param null $bk_id
                 * @return array[]|mixed
                 */
                public static function getHTMLViewTabs($bk_id = null){
        
                        if($bk_id) {
        
                                $qry = \Drupal::database()->select( 'rwPubgoldBooks', 'b' )->fields( 'b', [ 'bk_view_tabs' ] )->condition( 'bk_id', $bk_id, '=' );
        
                                try {
                                        if ( $qry->countQuery()->execute()->fetchField() ) {
                                                
                                                $field = $qry->execute()->fetchField();
                                                if($field) $tabs = unserialize( $field );
                                        }
                                }
                                catch ( \Exception $e ) {
                                        error_log( 'ERROR in ' . $e->getFile() . ' in Line ' . $e->getLine() . ': ' . $e->getMessage() );
                                        \Drupal::service( 'messenger' )->addError( 'ERROR: Can\'t load all parts for this view. Please contact support!' );
                                }
                        }
                        
                        return $tabs ?? [
                                'about' => [
                                        'title' => 'About',
                                        'weight' => 1,
                                        'show_on_empty' => 1
                                ],

                                'media' => [
                                        'title' => 'Media',
                                        'weight' => 2,
                                        'show_on_empty' => 1
                                ],

                                'editorial_board' => [
                                        'title' => 'Editorial Board',
                                        'weight' => 3,
                                        'show_on_empty' => 1
                                ],

                                'authors' => [
                                        'title' => 'Authors',
                                        'weight' => 4,
                                        'show_on_empty' => 1
                                ],

                                'manuscript_guidelines' => [
                                        'title' => 'Manuscript Guidelines',
                                        'weight' => 5,
                                        'show_on_empty' => 1
                                ],

                                'imprint' => [
                                        'title' => 'Imprint',
                                        'weight' => 6,
                                        'show_on_empty' => 1
                                ],

                                'overview_chapters' => [
                                        'title' => 'Overview Chapters',
                                        'weight' => 7,
                                        'show_on_empty' => 1
                                ]
                        ];
                }
        
                /**
                 * @param null $uid
                 * @return mixed
                 */
                public static function getUserEOBooks($uid = null){
                        
                        $uid = $uid ?? Publisso::currentUser()->getId();
                        return \Drupal::database()->select('rwPubgoldBookEditorialOffice', 't')->fields('t', ['beo_bkid'])->condition('beo_uid', $uid, '=')->execute()->fetchCol();
                }
        }
