<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        use Drupal as D;
        use Drupal\publisso_gold\Controller\Publisso as P;

        /**
         * Class FunderManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class FunderManager {
                
                public static function getFunder(int $id) :? D\publisso_gold\Classes\Funder{
                        if(!self::funderExists($id)) return null;
                        return new D\publisso_gold\Classes\Funder($id);
                }
                
                public static function funderExists(int $id) :bool{
                        return !!D::database()->select('rwPubgoldFunders', 't')->fields('t', [])->condition('id', $id, '=')->countQuery()->execute()->fetchField();
                        return false;
                }
        
                /**
                 * @param int|null $limit
                 * @return mixed
                 */
                public static function getStandardList(int $limit = null){
                        $qry = D::database()->select('rwPubgoldFunders', 't')->fields('t', []);
                        if($limit) $qry = $qry->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit($limit);
                        return $qry->execute()->fetchAll();
                }
        
                /**
                 * @param int|null $limit
                 * @return mixed
                 */
                public static function getIndexedList(int $limit = null){
                        $qry = D::database()->select('rwPubgoldFunders', 't')->fields('t', []);
                        if($limit) $qry = $qry->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit($limit);
                        return $qry->execute()->fetchAllAssoc('id');
                }
        
                /**
                 * @param int|null $limit
                 * @return mixed
                 */
                public static function getNormalizedAndIndexedList(int $limit = null){
                        $qry = D::database()->select('rwPubgoldFunders', 't')->fields('t', ['id']);
                        $qry->addExpression('concat_ws(\' \', case when abbr != \'\' then abbr else null end, case when abbr != \'\' then \'-\' else null end, name, case when country != \'\' then concat(\'(\', country, \')\') else null end)', 'name');
                        if($limit) $qry = $qry->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit($limit);
                        return $qry->execute()->fetchAllKeyed(0);
                }
        }