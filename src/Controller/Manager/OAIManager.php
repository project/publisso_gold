<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        
        use Drupal\Core\Extension\ModuleHandler;

        /**
         * Class OAIManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class OAIManager {
                
                public static function getAssetsDir(bool $absolute = true) :string{
                        return ($absolute ? DRUPAL_ROOT : '').'/'.\Drupal::service('module_handler')->getModule('publisso_gold')->getPath().'/Assets/OAI';
                }
        
                public static function getVarDir(bool $absolute = true) :string{
                        return ($absolute ? DRUPAL_ROOT : '').'/'.\Drupal::service('module_handler')->getModule('publisso_gold')->getPath().'/var/OAI';
                }
        
        
                /**
                 * @return array
                 */
                public static function getMetadataPrefixes(){
                        
                        $xml = new \DOMDocument();
                        $frag = $xml->createDocumentFragment();
                        $frag->appendXML(\file_get_contents(self::getAssetsDir().'/oai_listmetadataformats.xml'));
                        $xml->appendChild($frag);
                        $xpath = new \DOMXpath($xml);
                        
                        $metadataFormats = [];
                        foreach($xpath->query('//metadataPrefix') as $_) $metadataFormats[] = $_->nodeValue;
                        return $metadataFormats;
                }
        }
