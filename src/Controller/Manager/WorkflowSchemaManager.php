<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        
        use Drupal\publisso_gold\Controller\Workflow\WorkflowSchema;

        /**
         * Class WorkflowSchemaManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class WorkflowSchemaManager {
                
                public static function getSchemas(array $filter) :array{
                        $qry = \Drupal::database()->select('rwPubgoldWorkflowSchemaMeta', 't')->fields('t', []);
                        foreach($filter as $_) $qry->condition($_[0], $_[1], $_[2] ?? '=');
                        return $qry->execute()->fetchAllAssoc('schema_name');
                }
        
                /**
                 * @param string|null $name
                 * @return WorkflowSchema
                 */
                public static function getSchema(string $name = null){
                        return new WorkflowSchema($name);
                }
        }