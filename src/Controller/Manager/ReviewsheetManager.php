<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        
        use Drupal\publisso_gold\Controller\Reviewsheet;

        /**
         * Class ReviewsheetManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class ReviewsheetManager {
                
                public static function getList() :array{
                        return \Drupal::database()->select('rwPubgoldReviewPage', 't')->fields('t', [])->execute()->fetchAll();
                }
                
                public static function getSheet(int $id) :Reviewsheet{
                        return new Reviewsheet($id);
                }
        }