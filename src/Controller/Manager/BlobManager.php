<?php
        
        
        namespace Drupal\publisso_gold\Controller\Manager;
        
        
        use Drupal\publisso_gold\Controller\Blob;

        /**
         * Class BlobManager
         * @package Drupal\publisso_gold\Controller\Manager
         */
        class BlobManager {
                
                public static function getVarDir(bool $absolute = true) :string{
                        return ($absolute ? DRUPAL_ROOT : '').'/'.\Drupal::service('module_handler')->getModule('publisso_gold')->getPath().'/var/blob';
                }
                
                public static function get(int $blobID) :? Blob {
                        if(self::exists($blobID)) return new Blob($blobID);
                        return null;
                }
                
                public static function exists(int $blobId) : bool {
                        return !!\Drupal::database()->select('rwPubgoldBlobs', 't')->fields('t', [])
                                ->condition('id', $blobId, '=')->countQuery()->execute()->fetchField();
                }
        }