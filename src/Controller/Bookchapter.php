<?php
        
        /**
         * @file
         * Contains \Drupal\publisso_gold\Controller\Bookchapter.
         */
        namespace Drupal\publisso_gold\Controller;
        
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use \Drupal\publisso_gold\Controller\Medium\Book;
        use \Drupal\publisso_gold\Controller\User;
        use Symfony\Component\HttpFoundation\Response;
        use Symfony\Component\HttpFoundation\BinaryFileResponse;
        use Symfony\Component\HttpFoundation\ResponseHeaderBag;
        
        /**
         * Class Bookchapter
         * @package Drupal\publisso_gold\Controller
         */
        class Bookchapter extends \Drupal\Core\Controller\ControllerBase {
                private $id;
                private $elements          = [];
                private $tbl_column_prefix = 'cp_';
                private $authors;
                private $files             = [];
                private $book;
                private $lastStates = [];
        
                /**
                 * Bookchapter constructor.
                 * @param null $id
                 * @param false $mediumOnly
                 */
                public function __construct ($id = null, $mediumOnly = false ) {
                        if ( $id ) {
                                if ( !preg_match( '/^\d+$/', $id ) ) {
                                        $id = $this->getIDFromTitle( $id );
                                }
                                
                                $this->id = $id;
                                $this->load( $this->id, $mediumOnly );
                        }
                }
        
                /**
                 * @param $id
                 * @return false
                 */
                public function getIDFromTitle ($id ) {
                        $qry = \Drupal::database()->select( 'rwPubgoldBookChapters', 't' )->fields( 't', [ $this->tbl_column_prefix . 'id' ] );
                        $condition = 'TRIM(' . $this->tbl_column_prefix . 'title) = TRIM(' . "$id')";
                        $qry->where( $condition );
                        $res = $qry->execute()->fetchField();
                        
                        return $res ? $res : false;
                }
        
                /**
                 * @param $id
                 * @param false $mediumOnly
                 */
                private function load ($id, $mediumOnly = false ) {
                        if ( $this->id ) {
                                
                                $result = \Drupal::database()->select( 'rwPubgoldBookChapters', 'bc' )->fields( 'bc', [] )->condition( $this->tbl_column_prefix . 'id', $this->id, '=' )->execute()->fetchAssoc();
                                
                                foreach ( $result as $k => $v ) {
                                        
                                        if ( $k != $this->tbl_column_prefix . 'id' ) // store chapter except id
                                                $this->elements [ str_replace( $this->tbl_column_prefix, '', $k ) ] = $v;
                                }
                                
                                if ( !$mediumOnly ) {
                                        
                                        // files
                                        if ( array_key_exists( 'files', $result ) ) {
                                                
                                                foreach ( explode( ',', $result [ 'files' ] ) as $fid ) {
                                                        $this->files = new \Drupal\publisso_gold\Controller\Blob ( $fid );
                                                }
                                        }
                                        
                                        // parse the authors
                                        $_tmp = [];
                                        $authors = json_decode( $this->getElement( 'authors' ) );
                                        usort( $authors, [ 'self', 'sortAuthors' ] );
                                        
                                        foreach ( $authors as $author ) {
                                                
                                                if(property_exists($author, 'uid')) {
                                                        $_ = new \Drupal\publisso_gold\Controller\User ( $author->uid );
        
                                                        if ( !$_->getElement( 'id' ) ) {
                
                                                                foreach ( $author as $k => $v ) {
                                                                        $_->profile->setElement( $k, $v, false );
                                                                }
                                                        }
                                                }
                                                else{
                                                        $_ = new \Drupal\publisso_gold\Controller\User ();
                                                        $_->profile->setElement('firstname', $author->firstname, false);
                                                        $_->profile->setElement('lastname', $author->lastname, false);
                                                        $_->profile->setElement('affiliation', $author->affiliation, false);
                                                }
                                                
                                                if ( property_exists($author, 'profile_archive_id') &&
                                                $author->profile_archive_id != '' ) {
                                                        
                                                        $result = \Drupal::database()->select( 'rwPubgoldUserProfiles_Archive', 't' )->fields( 't', [ 'data' ] )->condition( 'id', $author->profile_archive_id, '=' )->execute()->fetchAssoc();
                                                        
                                                        $result [ 'data' ] = json_decode( base64_decode( $result [ 'data' ], true ) );
                                                        
                                                        if(is_array($result['data'])) {
                                                                foreach ( $result [ 'data' ] as $k => $v ) {
                                                                        $_->profile->setElement( $k, $v, false );
                                                                }
                                                        }
                                                }
                                                
                                                if ( $_->profile->getElement( 'affiliation' ) == '' && $_->profile->getElement( 'institute' ) != '' ) {
                                                        $_->profile->setElement( 'affiliation', $_->profile->getElement( 'institute' ), false );
                                                }
                                                
                                                $_tmp [] = $_;
                                        }
                                        
                                        $this->authors = $_tmp;
                                }
                                
                                if ( !$this->getElement( 'wf_state_chain' ) ) $this->firstLoadWFStateChain();
                                $this->initStateChain();
                        }
                }
        
                /**
                 * @param $elemName
                 * @return false|mixed|null
                 */
                public function getElement ($elemName ) {
                        if ( $elemName == 'id' ) return $this->id;
                        
                        if ( $elemName == 'references' && $this->elements [ 'references' ] === null ) {
                                $origWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $this->getElement( 'wfid' ) );
                                $_references = Publisso::tools()->getReferences( base64_decode( $this->elements [ 'chapter_text' ] ), ( string )$origWorkflow->getDataElement( 'references' ) );
                                $this->setElement( 'references', base64_encode( $_references ) );
                        }
                        
                        return $this->elements [ $elemName ] ?? null;
                }
        
                /**
                 * @param $elemName
                 * @param null $elemValue
                 * @param bool $storeOnDB
                 * @return bool
                 */
                public function setElement ($elemName, $elemValue = null, $storeOnDB = true ) {
                        if ( array_key_exists( $elemName, $this->elements ) ) {
                                
                                $oldValue = $this->elements [ $elemName ];
                                $this->elements [ $elemName ] = $elemValue;
                                
                                if ( $storeOnDB === false ) return true;
                                
                                if ( $this->save( $elemName ) === false ) {
                                        
                                        $this->elements [ $elemName ] = $oldValue;
                                        return false;
                                }
                                else {
                                        return true;
                                }
                        }
                        else {
                                return false;
                        }
                }
        
                /**
                 * @param null $elemName
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 */
                public function save ($elemName = null ) {
                        $fields = $this->elements;
                        
                        if ( $elemName !== null ) { // if specific element selected
                                $fields = [ $elemName => $fields [ $elemName ] ];
                        }
                        
                        foreach ( $fields as $k => $v ) {
                                $fields [ $this->tbl_column_prefix . $k ] = $v;
                                unset ( $fields [ $k ] );
                        }
                        
                        return \Drupal::database()->update( 'rwPubgoldBookChapters' )->fields( $fields )->condition( $this->tbl_column_prefix . 'id', $this->id )->execute();
                }
        
                /**
                 * @return bool
                 */
                private function firstLoadWFStateChain () {
                        
                        if ( !$this->getElement( 'wfid' ) ) return false;
                        
                        if ( !array_key_exists( 'wf_state_chain', $this->elements ) || $this->elements[ 'wf_state_chain' ] !== null ) return false;
                        
                        //nur, wenn dieses Kapitel bereits publiziert wurde
                        $qry = \Drupal::database()->select( 'rwPubgoldWorkflowStateChain', 't' );
                        $qry->fields( 't', [] );
                        $qry->condition( 'wf_id', $this->getElement( 'wfid' ), '=' );
                        
                        if ( !$qry->condition( 'state_new', 'published', '=' )->countQuery()->execute()->fetchField() ) return false;
                        
                        $states = [];
                        foreach ( $qry->orderBy( 'time', 'ASC' )->execute()->fetchAll() as $row ) {
                                $states[] = $row;
                        }
                        
                        $this->setElement( 'wf_state_chain', serialize( $states ) );
                        
                        //lösche aus der Verlaufstabelle
                        \Drupal::database()->delete( 'rwPubgoldWorkflowStateChain' )->condition( 'wf_id', $this->getElement( 'wfid' ), '=' )->execute();
                        return true;
                }
        
                /**
                 * @param $a
                 * @param $b
                 * @return int
                 */
                private static function sortAuthors ($a, $b ) {
                        if ( $a->weight == $b->weight ) return 0;
                        return $b->weight > $a->weight ? -1 : +1;
                }
        
                /**
                 * @return string
                 */
                public function __toString () {
                        return "class bookchapter";
                }
        
                /**
                 * @return bool
                 */
                public function hasCorrespondingAuthors () {
                        return !!$this->countCorrespondingAuthors();
                }
        
                /**
                 * @return int
                 */
                public function countCorrespondingAuthors () {
                        $count = 0;
                        
                        foreach ( json_decode( $this->getElement( 'authors' ), true ) as $_ ) {
                                
                                if ( $_ [ 'is_corresponding' ] == 1 && $_ [ 'uid' ] ) {
                                        $count++;
                                }
                        }
                        
                        return $count;
                }
                
                public function getXML4PDF() :\DOMDocument{
                        
                        $book           = BookManager::getBook($this->getElement('bkid'), false);
                        $workflow       = WorkflowManager::getItem($this->getElement('wfid'));
                        $chapter        = $this;
                        
                        $xml = new \DOMDocument('1.0', 'UTF-8');
                        $rootNode = $xml->appendChild($xml->createElement('record'));
                        $rootNode->setAttribute('type', 'chapter');
                        
                        $_fulltext   = base64_decode( $chapter->getElement( 'chapter_text' ) );
                        $_references = base64_decode( $chapter->getElement( 'references' ) );
                        
                        $rootNode->appendChild($xml->createElement('id', $chapter->getElement('id')))->setAttribute('title', 'ID');
                        $rootNode->appendChild($xml->createElement('parent-id', $book->getElement('id')))->setAttribute('title', 'Parent ID');
                        
                        $titleNode = $rootNode->appendChild($xml->createElement('title'));
                        $titleNode->setAttribute('title', 'Title');
                        $titleNode->appendChild($xml->createCDATASection($chapter->getElement('title')));
                        
                        $titleTranslatedNode = $rootNode->appendChild($xml->createElement('title-translated'));
                        $titleTranslatedNode->setAttribute('title', 'Titel');
                        if(!empty($chapter->getElement('title_translated'))) $titleTranslatedNode->appendChild($xml->createCDATASection($chapter->getElement('title_tranlated')));
                        
                        $keywordsNode = $rootNode->appendChild($xml->createElement('keywords'));
                        $keywordsNode->setAttribute('title', 'Keywords');
                        
                        $keywords = json_decode($chapter->getElement('keywords')) ?? unserialize($chapter->getElement('keywords'));
                        $keywords = array_filter($keywords);
                        foreach($keywords as $keyword) $keywordsNode->appendChild($xml->createElement('keyword'))->appendChild($xml->createCDATASection($keyword));
                        
                        $CAsNode = $rootNode->appendChild($xml->createElement('corresponding-authors'));
                        $CAsNode->setAttribute('title', 'Corresponding authors');
        
                        foreach ( $this->getCorrespondingAuthors() as $author ) {
                                $CANode = $CAsNode->appendChild($xml->createElement('author'));
                                $CANode->setAttribute('title', 'Corresponding author');
                                $CANode->appendChild($xml->createElement('graduation-suffix'))->appendChild($xml->createCDATASection($author->profile->getElement('graduation_suffix')));
                                $CANode->appendChild($xml->createElement('graduation'))->appendChild($xml->createCDATASection($author->profile->getElement('graduation')));
                                $CANode->appendChild($xml->createElement('country'))->appendChild($xml->createCDATASection(Publisso::tools()->getCountry($author->profile->getElement('country'))));
                                $CANode->appendChild($xml->createElement('department'))->appendChild($xml->createCDATASection($author->profile->getElement('department')));
                                $CANode->appendChild($xml->createElement('email'))->appendChild($xml->createCDATASection($author->profile->getElement('email')));
                                $CANode->appendChild($xml->createElement('telephone'))->appendChild($xml->createCDATASection($author->profile->getElement('telephone')));
                                $CANode->appendChild($xml->createElement('postal-code'))->appendChild($xml->createCDATASection($author->profile->getElement('postal_code')));
                                $CANode->appendChild($xml->createElement('institute'))->appendChild($xml->createCDATASection($author->profile->getElement('institute')));
                                $CANode->appendChild($xml->createElement('street'))->appendChild($xml->createCDATASection($author->profile->getElement('street')));
                                $CANode->appendChild($xml->createElement('city'))->appendChild($xml->createCDATASection($author->profile->getElement('city')));
                                $CANode->appendChild($xml->createElement('firstname'))->appendChild($xml->createCDATASection($author->profile->getElement('firstname')));
                                $CANode->appendChild($xml->createElement('lastname'))->appendChild($xml->createCDATASection($author->profile->getElement('lastname')));
                                $CANode->appendChild($xml->createElement('firstname'))->appendChild($xml->createCDATASection($author->profile->getElement('firstname')));
                                $CANode->appendChild($xml->createElement('initials'))->appendChild($xml->createCDATASection($author->profile->getFirstnameInitials()));
                                $CANode->appendChild($xml->createElement('affiliation'))->appendChild($xml->createCDATASection($author->profile->getAffiliation()));
                        }
                        
                        $AsNode = $rootNode->appendChild($xml->createElement('authors'));
                        $AsNode->setAttribute('title', 'Authors');
                        
                        $AffsNode = $rootNode->appendChild($xml->createElement('affiliations'));
                        $AffsNode->setAttribute('title', 'Affiliations');
                        
                        $authors = [];
                        $affiliations = [];
        
                        $chapter_authors = json_decode( $chapter->getElement( 'authors' ) );
        
                        foreach ( array_filter($chapter_authors) as $_ ){
                                $weight = $_->weight ? $_->weight : 0;
                                while ( array_key_exists( $weight, $authors ) ) $weight++; //falls Autoren mit gleicher Gewichtung angegeben sind, damit sie sich nicht gegenseitig überschreiben
                                $authors[ $weight ] = $_;
                        }
                        ksort( $authors );
                        
                        foreach ( $authors as $_ ) {
                
                                if ( property_exists($_, 'uid') && $_->uid && $_->profile_archive_id ) {
                                        $user = new \Drupal\publisso_gold\Controller\User ( $_->uid );
                                        $user->profile->loadSnapshot( $_->profile_archive_id );
                                        $affiliations [] = $user->profile->getAffiliation();
                                }
                                else {
                                        $affiliations [] = $_->affiliation;
                                }
        
                                if(property_exists($_, 'add_affiliations')){
                
                                        $addAffiliations = (array)$_->add_affiliations;
                                        usort($addAffiliations, function($a, $b){
                                                return $a->weight <=> $b->weight;
                                        });
                
                                        foreach($addAffiliations as $addAffiliation) $affiliations[] = $addAffiliation->affiliation;
                                }
                        }
        
                        $affiliations = array_keys( array_flip( $affiliations ) ); // make affs unique
        
                        for ( $c = 1; $c <= count( $affiliations ); $c++ ) {
                                if ( $affiliations [ $c - 1 ] ) {
                                        $affNode = $AffsNode->appendChild($xml->createElement('affiliation'));
                                        $affNode->setAttribute('number', $c);
                                        $affNode->setAttribute('title', 'Affiliation');
                                        $affNode->appendChild($xml->createCDATASection($affiliations[$c - 1]));
                                }
                        }
                        
                        foreach ( $authors as $_ ) {
                
                                if ( $_->uid && $_->profile_archive_id ) {
                                        $user = new \Drupal\publisso_gold\Controller\User ( $_->uid );
                                        $user->profile->loadSnapshot( $_->profile_archive_id );
                                        $affiliation = $user->profile->getAffiliation();
                                }
                                else {
                                        $affiliation = $_->affiliation;
                                }
        
                                $affKey = [];
        
                                if(property_exists($_, 'add_affiliations')){
                                        foreach ($_->add_affiliations ?? [] as $addAffiliation){
                                                $affKey[] = array_search($addAffiliation->affiliation, $affiliations) + 1;
                                        }
                                }
        
                                $affKey[] = array_search( $affiliation, $affiliations ) + 1;
                                sort($affKey);
                                
                                $ANode = $AsNode->appendChild($xml->createElement('author'));
                                $ANode->setAttribute('title', 'Author');
                                $ANode->appendChild($xml->createElement('affiliation-number', implode(',', $affKey)));
                                foreach ( ( array )$_ as $k => $v ) {
                                        if(is_scalar($v))
                                                $ANode->appendChild(($xml->createElement($k)))->appendChild($xml->createCDATASection($v));
                                }
                        }
                        
                        if(!empty($chapter->getElement('license'))){
                                $license = new License($chapter->getElement('license'));
                                $licNode = $rootNode->appendChild($xml->createElement('license'));
                                $licNode->setAttribute('title', 'License');
                                $licNameNode = $licNode->appendChild($xml->createElement('name'));
                                $licNameNode->setAttribute('title', 'Name');
                                $licNameNode->appendChild($xml->createCDATASection($license->getElement('name')));
                                $licDescNode = $licNode->appendChild($xml->createElement('description'));
                                $licDescNode->setAttribute('title', 'Description');
                                $licDescNode->appendChild($xml->createCDATASection($license->getElement('description')));
                                $licUrlNode = $licNode->appendChild($xml->createElement('url'));
                                $licUrlNode->setAttribute('title', 'URL');
                                $licUrlNode->appendChild($xml->createCDATASection($license->getElement('url')));
                                $licImgNode = $licNode->appendChild($xml->createElement('img'));
                                $licImgNode->setAttribute('src', $license->getInlineImageData());
                        }
                        
                        $abstractNode = $rootNode->appendChild($xml->createElement('abstract'));
                        $abstractNode->setAttribute('title', 'Abstract');
                        if(!empty($chapter->getElement('abstract'))) $abstractNode->appendChild($xml->createCDATASection(base64_decode($chapter->getElement('abstract'))));
                        
                        $abstractTranslatedNode = $rootNode->appendChild($xml->createElement('abstract-translated'));
                        $abstractTranslatedNode->setAttribute('title', 'Zusammenfassung');
                        if(!empty($chapter->getElement('abstract_translated'))) $abstractTranslatedNode->appendChild($xml->createCDATASection(base64_decode($chapter->getElement('abstract_translated'))));
                        
                        $rootNode->appendChild($xml->createElement('publication-year', $chapter->getElement('publication_year')))->setAttribute('title', 'Publication year');
                        
                        $pubPlaceNode = $rootNode->appendChild($xml->createElement('publication-place'));
                        $pubPlaceNode->setAttribute('title', 'Publication place');
                        $pubPlaceNode->appendChild($xml->createCDATASection($chapter->getElement('publication_place')));
                        
                        $publisher = $chapter->getElement('publisher') ?? $book->getElement('publisher');
                        $pubNode = $rootNode->appendChild($xml->createElement('publisher'));
                        $pubNode->setAttribute('title', 'Publisher');
                        $pubNode->appendChild($xml->createCDATASection($publisher));
                        
                        $chapterNode = $rootNode->appendChild($xml->createElement('fulltext'));
                        $chapterNode->setAttribute('title', 'Chapter text');
                        $chapterNode->appendChild($xml->createCDATASection($_fulltext));
                        
                        $rootNode->appendChild($xml->createElement('doi', $chapter->getElement('doi')))->setAttribute('title', 'DOI');
                        $rootNode->appendChild($xml->createElement('urn', $chapter->getElement('urn')))->setAttribute('title', 'URN');
                        
                        $fundingNode = $rootNode->appendChild($xml->createElement('funding'));
                        $fundingNode->setAttribute('title', 'Funding');
                        
                        if(!(empty($chapter->getElement('funding_id')) || empty($chapter->getElement('funding_name')) || empty($chapter->getElement('funding_number')))){
                                $_ = $fundingNode->appendChild($xml->createElement('funding-id'));
                                $_->setAttribute('title', 'Funding ID');
                                $_->appendChild($xml->createCDATASection($chapter->getElement('funding_id')));
        
                                $_ = $fundingNode->appendChild($xml->createElement('funding-number'));
                                $_->setAttribute('title', 'Funding number');
                                $_->appendChild($xml->createCDATASection($chapter->getElement('funding_number')));
        
                                $_ = $fundingNode->appendChild($xml->createElement('funding-name'));
                                $_->setAttribute('title', 'Funding name');
                                $_->appendChild($xml->createCDATASection($chapter->getElement('funding_name')));
                        }
                        
                        $errNode = $rootNode->appendChild($xml->createElement('erratum'));
                        $errNode->setAttribute('title', 'Erratum');
                        if(!empty($chapter->getElement('erratum'))) $errNode->appendChild($xml->createCDATASection($chapter->getElement('erratum')));
        
                        $corrNode = $rootNode->appendChild($xml->createElement('correction'));
                        $corrNode->setAttribute('title', 'Correction');
                        if(!empty($chapter->getElement('correction'))) $corrNode->appendChild($xml->createCDATASection($chapter->getElement('correction')));
                        
                        $rootNode->appendChild($xml->createElement('author-uid', $chapter->getElement('author_uid')))->setAttribute('title', 'Author UID');
                        $rootNode->appendChild($xml->createElement('version', $chapter->getElement('version')))->setAttribute('title', 'Version');
                        $rootNode->appendChild($xml->createElement('ddc', Publisso::tools()->getDDC($chapter->getElement('author_uid'), 'name')))->setAttribute('title', 'DDC');
                        
                        $corpNode = $rootNode->appendChild($xml->createElement('corporations'));
                        $corpNode->setAttribute('title', 'Corporations');
                        
                        $corporations = json_decode($chapter->getElement('corporation')) ?? unserialize($chapter->getElement('corporation'));
                        $corporations = array_filter($corporations);
                        
                        foreach($corporations as $_){
                                $corpNode->appendChild($xml->createElement('corporation'))->appendChild($xml->createCDATASection($_));
                        }
                        
                        $coiNode = $rootNode->appendChild($xml->createElement('conflict-of-interest'));
                        $coiNode->setAttribute('title', 'Conflict of interest');
                        if(!empty($chapter->getElement('conflict_of_interest'))) $coiNode->appendChild($xml->createCDATASection($chapter->getElement('conflict_of_interest')));
        
                        $coiNode = $rootNode->appendChild($xml->createElement('conflict-of-interest-text'));
                        $coiNode->setAttribute('title', 'Conflict of interest text');
                        if(!empty($chapter->getElement('conflict_of_interest_text'))) $coiNode->appendChild($xml->createCDATASection($chapter->getElement('conflict_of_interest_text')));
                        
                        $subCatNode = $rootNode->appendChild($xml->createElement('subcategory'));
                        $subCatNode->setAttribute('title', 'Subcategory');
                        if(!empty($chapter->getElement('sub_category'))) $subCatNode->appendChild($xml->createCDATASection($chapter->getElement('sub_category')));
                        
                        $rootNode->appendChild($xml->createElement('published', $chapter->getElement('published')))->setAttribute('title', 'Published');
                        
                        $refsNode = $rootNode->appendChild($xml->createElement('references'));
                        $refsNode->setAttribute('title', 'References');
                        foreach(array_filter( explode( "\n", $_references ) ) as $ref){
                                $refsNode->appendChild($xml->createElement('reference'))->appendChild($xml->createCDATASection(preg_replace('/^(\<a.+?\<\/a\>){0,1}\s*(\[.+?\])\s*(.+$)/', '$1$3', $ref)));
                        }
        
                        // citation_note
                        $cn = $this->getCitationNote( $book, 'text' );
                        $cn = (str_replace('<br>', ' ', $cn));
                        $cnNode = $rootNode->appendChild($xml->createElement('citation-note'));
                        $cnNode->setAttribute('title', 'Citation note');
                        $cnNode->appendChild($xml->createCDATASection($cn));
                        
                        // append license-information
                        $copyNode = $rootNode->appendChild($xml->createElement('copyright'));
                        $copyNode->setAttribute('title', 'Copyright');
                        
                        reset($authors);
                        $name = implode(' ', [current($authors)->firstname, current($authors)->lastname]);
                        if(count($authors) > 1) $name .= ' et al.';
                        $copyNode->appendChild($xml->createElement('name'))->appendChild($xml->createCDATASection('© ' . date( 'Y' ).' '.$name));
                        $copyNode->appendChild($xml->createElement('description'))->appendChild($xml->createCDATASection(( ( string )t( ' This is an Open Access publication distributed under the terms of the ' ) ) . ($license ? $license->getElement( 'description' ) : null) . '. ' . ( ( string )t( 'See license information at ' ) ) . ($license ? $license->getElement( 'url' ) : '')));
                        
                        //attachments
                        $attsNode = $rootNode->appendChild($xml->createElement('attachments'));
                        $attsNode->setAttribute('title', 'Attachments');
                        
                        foreach(array_filter(explode(',', $chapter->getElement('files'))) as $blobID){
                                
                                $blob = new Blob($blobID);
                                $attNode = $attsNode->appendChild($xml->createElement('attachment'));
                                foreach(array_keys($blob->meta) as $_){
                                        $attNode->appendChild($xml->createElement($_))->appendChild($xml->createCDATASection($blob->meta[$_]));
                                }
                                $attNode->appendChild($xml->createElement('type'))->appendChild($xml->createCDATASection($blob->type));
                                $attNode->appendChild($xml->createElement('url'))->appendChild($xml->createCDATASection($blob->getUrl()->setAbsolute()->toString()));
                        }
                        
                        $xml->formatOutput = true;
                        return $xml;
                }
        
                /**
                 * @return false|string
                 */
                public function getXML () {
                        $book = new Book ( $this->getElement( 'bkid' ) );
                        $origWorkflow = new Workflow ( $this->getElement( 'wfid' ) );
                        
                        $doc = new \DOMDocument ( "1.0", "UTF-8" );
                        $xpath = new \DOMXpath ( $doc );
                        
                        // create main-node
                        $doc->appendChild( $doc->importNode( $doc->createElement( 'chapter' ) ) );
                        
                        // list($_fulltext, $_references) = \Drupal::service('publisso_gold.tools')->getReferencesFromText(base64_decode($this->getElement('chapter_text')));
                        $_fulltext = base64_decode( $this->getElement( 'chapter_text' ) );
                        // $_references = \Drupal::service('publisso_gold.tools')->getReferences(base64_decode($this->getElement('chapter_text')), (string)$origWorkflow->getDataElement('references'));
                        $_references = base64_decode( $this->getElement( 'references' ) );
                        // $inactiveReferences = \Drupal::service('publisso_gold.tools')->getInactiveReferences($_references, $origWorkflow->getDataElement('references'));
                        
                        /**
                         * Begin with all chapterelements
                         * each element will be named like column-names from database-table rwpg_rwPubgoldBookchapter
                         * each element will have an attribute "title" wich will dont't be automatically translated
                         * some elements are the german translation (i.e.
                         * title_translated) - this title will be fixed set in german
                         */
                        
                        // id
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'id', $this->getElement( 'id' ), [ 'title' => 'ID' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // bkid
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'bkid', $this->getElement( 'bkid' ), [ 'title' => 'Book ID' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // title
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'title', base64_encode(
                                $this->getElement( 'title' ) ), [ 'title' => 'Title' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ], true );
                        
                        // title_translated
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'title_translated',
                                                                                   $this->getElement( 'title_translated' ), [ 'title' => 'Titel' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ], true );
                        
                        // license
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'license', $this->getElement( 'license' ), [ 'title' => 'License' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ], true );
                        
                        // abstract
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'abstract', $this->getElement( 'abstract' ), [ 'title' => 'Abstract' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ], true );
                        
                        // abstract_translated
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'abstract_translated',
                                                                                   $this->getElement( 'abstract_translated' ), [ 'title' => 'Zusammenfassung' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ], true );
                        
                        // chapter_text
                        $_ = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'chapter_text', base64_encode(
                                $_fulltext ), [ 'title' => 'Chapter text' ], $doc, $xpath->evaluate( '/chapter' ) [ 0
                        ], true );
                        
                        // publication_year
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'publication_year',
                                                                                   $this->getElement( 'publication_year' ), [ 'title' => 'Publication year' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ], true );
                        
                        // publication_place
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'publication_place',
                                                                                   $this->getElement( 'publication_place' ), [ 'title' => 'Publication place' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ], true );
                        
                        // publisher
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'publisher', $this->getElement( 'publisher' ), [ 'title' => 'Publisher' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ], true );
                        
                        // doi
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'doi', $this->getElement( 'doi' ),
                                                                                   [ 'title' => 'DOI' ], $doc,
                                                                                   $xpath->evaluate( '/chapter' ) [ 0
                                                                                   ], true );
                        
                        // corresponding_author
                        $authors_string = [];
                        
                        foreach ( $this->getCorrespondingAuthors() as $author ) {
                                
                                $items = [ $author->profile->getReadableFullName( 'FL', ' ' ), $author->profile->getElement( 'institute' ), $author->profile->getElement( 'department' ), $author->profile->getElement( 'street' ), $author->profile->getElement( 'postal_code' ), $author->profile->getElement( 'city' ), \Drupal::service( 'publisso_gold.tools' )->getCountry( $author->profile->getElement( 'country' ) ), ( !empty ( $author->profile->getElement( 'telephone' ) ) ? ( ( string )t( 'Phone: ' ) ) . $author->profile->getElement( 'telephone' ) : '' ), 'E-mail: ' . $author->profile->getElement( 'email' ) ];
                                
                                $authors_string [] = implode( ', ', array_filter( $items ) );
                        }
                        
                        $authors_string = implode( '; ', array_filter( $authors_string ) );
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'corresponding_author', $authors_string, [ 'title' => 'Corresponding author' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // urn
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'urn', $this->getElement( 'urn' ), [ 'title' => 'URN' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // keywords
                        $keywords = json_decode( $this->getElement( 'keywords' ), 1 );
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'keywords', '', [ 'title' => 'Keywords' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        foreach ( $keywords as $keyword ) \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'keyword', $keyword, [], $doc, $xpath->evaluate( '/chapter/keywords' ) [ 0 ] );
                        
                        // keywords_translated
                        $keywords = json_decode( $this->getElement( 'keywords_translated' ), 1 );
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'keywords_translated', '', [ 'title' => 'Schlüsselwörter' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        if(is_array($keywords)) {
                                foreach ( $keywords as $keyword ) \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'keyword', $keyword, [], $doc, $xpath->evaluate( '/chapter/keywords_translated' ) [ 0 ] );
                        }
                        
                        // funding_number
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'funding_number', $this->getElement( 'funding_number' ), [ 'title' => 'Funding number' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // funding_id
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'funding_id', $this->getElement( 'funding_id' ), [ 'title' => 'Funding ID' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // erratum
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'errata', '', [ 'title' => 'Erratum' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        if($this->getElement( 'errata' ))
                                $xpath->evaluate( '/chapter/errata' ) [ 0 ]->appendChild( \Drupal::service( 'publisso_gold.tools' )->makeDOMFragment( $doc, html_entity_decode( $this->getElement( 'errata' ) ) ) );
                        
                        // correction
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'correction', '', [ 'title' => 'Correction' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        if($this->getElement( 'correction' ))
                                $xpath->evaluate( '/chapter/correction' ) [ 0 ]->appendChild( \Drupal::service( 'publisso_gold.tools' )->makeDOMFragment( $doc, html_entity_decode( $this->getElement( 'correction' ) ) ) );
                        
                        // author_uid
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'author_uid', $this->getElement( 'author_uid' ), [ 'title' => 'Author UID' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // version
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'version', $this->getElement( 'version' ), [ 'title' => 'Version' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // ddc
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'ddc', $this->getElement( 'ddc' ), [ 'title' => 'DDC' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // corporations
                        $corporations = json_decode( $this->getElement( 'corporation' ) );
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'corporations', '', [ 'title' => 'Corporations' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        foreach ( $corporations as $corporation ) \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'corporation', $corporation, [], $doc, $xpath->evaluate( '/chapter/corporations' ) [ 0 ] );
                        
                        // conflict_of_interest
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'conflict_of_interest', $this->getElement( 'conflict_of_interest' ), [ 'title' => 'Conflict of interest' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // conflict_of_interest_test
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'conflict_of_interest_text', $this->getElement( 'conflict_of_interest_text' ), [ 'title' => 'Conflict of interest' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // author_contract
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'author_contract', $this->getElement( 'author_contract' ), [ 'title' => 'Accept author\s contract' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // patients_rights
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'patients_rights', $this->getElement( 'patients_rights' ), [ 'title' => 'Patients rights' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // sub_category
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'sub_category', $this->getElement( 'sub_category' ), [ 'title' => 'Subcategory' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // policy_accept
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'policy_accept', $this->getElement( 'policy_accept' ), [ 'title' => 'Accept policies' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // published
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'published', $this->getElement( 'published' ), [ 'title' => 'Published' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // doc_number
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'doc_number', $this->getElement( 'doc_number' ), [ 'title' => 'Doc number' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // funding_name
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'funding_name', $this->getElement( 'funding_name' ), [ 'title' => 'Funding name' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        // datas
                        $datas = [];
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'datas', '', [ 'title' => 'Data' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        foreach ( $datas as $data ) \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'data', $data, [], $doc, $xpath->evaluate( '/chapter/datas' ) [ 0 ] );
                        
                        // attachments
                        $attachments = [];
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'attachments', '', [ 'title' => 'Attachments' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        foreach ( $attachments as $attachment ) \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'attachment', $attachment, [], $doc, $xpath->evaluate( '/chapter/attachments' ) [ 0 ] );
                        
                        // authors and affiliations
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'authors', '', [ 'title' => 'Authors' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'affiliations', '', [ 'title' => 'Affiliations' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        $authors = [];
                        $affiliations = [];
                        
                        $chapter_authors = json_decode( $this->getElement( 'authors' ) );
                        
                        foreach ( array_filter($chapter_authors) as $_ ) $authors [ $_->weight ] = $_;
                        ksort( $authors );
                        
                        foreach ( $authors as $_ ) {
                                
                                if ( $_->uid && $_->profile_archive_id ) {
                                        $user = new \Drupal\publisso_gold\Controller\User ( $_->uid );
                                        $user->profile->loadSnapshot( $_->profile_archive_id );
                                        $affiliations [] = $user->profile->getAffiliation();
                                }
                                else {
                                        $affiliations [] = $_->affiliation;
                                }
                        }
                        
                        $affiliations = array_keys( array_flip( $affiliations ) ); // make affs unique
                        
                        for ( $c = 1; $c <= count( $affiliations ); $c++ ) {
                                if ( $affiliations [ $c - 1 ] ) {
                                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'affiliation', $affiliations [ $c - 1 ], [ 'number' => $c ], $doc, $xpath->evaluate( '/chapter/affiliations' ) [ 0 ], true );
                                }
                        }
                        
                        foreach ( $authors as $_ ) {
                                
                                if ( $_->uid && $_->profile_archive_id ) {
                                        $user = new \Drupal\publisso_gold\Controller\User ( $_->uid );
                                        $user->profile->loadSnapshot( $_->profile_archive_id );
                                        $affiliation = $user->profile->getAffiliation();
                                }
                                else {
                                        $affiliation = $_->affiliation;
                                }
                                
                                $affKey = array_search( $affiliation, $affiliations ) + 1;
                                
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'author', '', [], $doc, $xpath->evaluate( '/chapter/authors' ) [ 0 ] );
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'affiliation_number', $affKey, [], $doc, $xpath->evaluate( '/chapter/authors/author[last()]' ) [ 0 ] );
                                foreach ( ( array )$_ as $k => $v ) \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( $k, $v, [], $doc, $xpath->evaluate( '/chapter/authors/author[last()]' ) [ 0 ] );
                        }
                        
                        // references
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'references', '', [ 'title' => 'References' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        
                        $_references = array_filter( explode( "\n", $_references ) );
                        $refLinks = [];
                        $refDOI = [];
                        
                        foreach ( $_references as $index => $reference ) {
                                #echo "$reference<br>";
                                if ( preg_match( '/^\s*?(\<a.*?\/a\>)(.+)/', $reference, $matches ) ) {
                                        #echo "Found anchor....<br>";
                                        $refLinks[ $index ] = $matches[ 1 ];
                                        $reference = $matches[ 2 ];
                                        
                                        if ( preg_match( '/^(.+)(\<a.*?\/a\>\s*)$/', $reference, $matches ) ) {
                                                #echo "Found doi....<br>";
                                                $refDOI[ $index ] = $matches[ 2 ];
                                                $reference = $matches[ 1 ];
                                        }
                                        else {
                                                $refDOI[ $index ] = null;
                                        }
                                }
                                else {
                                        $refLinks[ $index ] = $refDOI[ $index ] = null;
                                        
                                        if ( preg_match( '/^(.+)(\<a.*?\/a\>\s*)$/', $reference, $matches ) ) {
                                                #echo "Found doi....<br>";
                                                $refDOI[ $index ] = $matches[ 2 ];
                                                $reference = $matches[ 1 ];
                                        }
                                        else {
                                                $refDOI[ $index ] = null;
                                        }
                                }
                                
                                $_references[ $index ] = trim( preg_replace( '/^\s*?\[\d+?\]/', '', $reference ) );
                        }
                        #exit();
                        #echo '<pre>';
                        #print_r($_references);
                        #echo '</pre>';
                        #exit();
                        
                        foreach ( $_references as $index => $reference ) {
                                
                                $xpath->evaluate( '/chapter/references' )[ 0 ]->appendChild( $doc->createElement( 'reference' ) );
                                $fragment = $doc->createDocumentFragment();
                                
                                if ( !empty( $refLinks[ $index ] ) ) {
                                        $fragment->appendChild( \Drupal::service( 'publisso_gold.tools' )->makeDOMFragment( $doc, $refLinks[ $index ] ) );
                                }
                                
                                $xpath->evaluate( '/chapter/references/reference[last()]' )[ 0 ]->appendChild( $doc->createTextNode( $reference ) );
                                $xpath->evaluate( '/chapter/references/reference[last()]' )[ 0 ]->appendChild( $fragment );
                                
                                if ( !empty( $refDOI[ $index ] ) ) {
                                        $xpath->evaluate( '/chapter/references/reference[last()]' )[ 0 ]->appendChild( \Drupal::service( 'publisso_gold.tools' )->makeDOMFragment( $doc, $refDOI[ $index ] ) );
                                }
                        }
                        
                        #echo $doc->saveXML(); exit();
                        
                        /*
                        foreach ( array_filter ( explode ( "\n", $_references ) ) as $_ ) {
                                
                                \Drupal::service ( 'publisso_gold.tools' )->DOMAppendChild ( 'reference', '', [ ], $doc, $xpath->evaluate ( '/chapter/references' ) [0] );
                                #$xpath->evaluate ( '/chapter/references/reference[last()]' ) [0]->appendChild ( \Drupal::service ( 'publisso_gold.tools' )->makeDOMFragment ( $doc, $_ ) );
                                \Drupal::service('publisso_gold.tools')->appendCdata ($doc, $xpath->evaluate ( '/chapter/references/reference[last()]' ) [0], $_);
                        }
                        foreach ( array_filter ( explode ( "\n", $inactiveReferences ) ) as $_ ) {
                                \Drupal::service ( 'publisso_gold.tools' )->DOMAppendChild ( 'reference', '', [ ], $doc, $xpath->evaluate ( '/chapter/references' ) [0] );
                                //$xpath->evaluate ( '/chapter/references/reference[last()]' ) [0]->appendChild ( \Drupal::service ( 'publisso_gold.tools' )->makeDOMFragment ( $doc, $_ ) );
                                \Drupal::service('publisso_gold.tools')->appendCdata ($doc, $xpath->evaluate ( '/chapter/references/reference[last()]' ) [0], $_);
                        }
                        
                        $refNodes = [ ];
                        $newRefNodes = [ ];
                        
                        foreach ( $xpath->evaluate ( '/chapter/references/reference' ) as $refNode ) {
                                
                                
                                echo $refNode->C14N();
                        }
                        */
                        //exit();
                        /*
                         * foreach($xpath->evaluate('/chapter/references/reference') as $refNode){
                         * //echo $refNode->nodeValue;
                         * if($origWorkflow->getElement('state') == 'published'){
                         *
                         * if($xpath->query('a[@name]', $refNode)->length){
                         *
                         * $anchorNode = $xpath->query('a[@name]', $refNode)[0];
                         * $name = $anchorNode->getAttribute('name');
                         * $refNode->removeChild($anchorNode);
                         * $refNode->nodeValue = $refNode->nodeValue;
                         *
                         * $tempDom = new \DOMDocument();
                         * $tempDom->appendChild($tempDom->importNode($refNode, true));
                         *
                         * $html = preg_replace('/\<\/reference>/', '</a></reference>', preg_replace('/\<reference>\[\]\s/', '<reference><a name="'.$name.'">', $tempDom->saveHTML()));
                         * $refNodes[] = $refNode;
                         * //echo $html;
                         * $newRefNodes[] = \Drupal::service('publisso_gold.tools')->makeDOMFragment($doc, html_entity_decode($html));
                         * //var_dump($newRefNodes);
                         * //echo "\n\n\n";
                         * }
                         * }
                         * elseif($origWorkflow->getElement('state') != 'published'){
                         * //var_dump($refNode);
                         * $tempDom = new \DOMDocument();
                         * $tempDom->appendChild($tempDom->importNode($refNode, true));
                         * $html = preg_replace('/\<\/reference>/', '</reference>', preg_replace('/\<reference>\[([0-9, ])+\]\s/', '<reference>', $tempDom->saveHTML()));
                         * $refNodes[] = $refNode;
                         * $newRefNodes[] = \Drupal::service('publisso_gold.tools')->makeDOMFragment($doc, html_entity_decode($html));
                         * }
                         * }
                         * //exit();
                         * for($i = 0; $i < count($refNodes); $i++) $refNodes[$i]->parentNode->replaceChild($newRefNodes[$i], $refNodes[$i]);
                         */
                        // citation_note
                        $cn = $this->getCitationNote( $book, 'text' );
                        $cn = nl2br(str_replace('<br>', ' ', $cn));
                        #$cn = strip_tags($this->getCitationNote( $book, 'text' ));
                        #echo $cn;
                        $cnNode = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'citation_note', base64_encode($cn), [ 'title' => 'Citation note' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ], true);
                        #var_dump($cnNode); exit();
                        // append license-information
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'copyright', '', [ 'title' => 'License/Copyright' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'copyright_text', '', [ 'title' => 'License/Copyright' ], $doc, $xpath->evaluate( '/chapter' ) [ 0 ] );
                        if ( !empty ( $this->getElement( 'license' ) ) ) {
                                
                                $license = new \Drupal\publisso_gold\Controller\License ( $this->getElement( 'license' ) );
                                $xpath->evaluate( '/chapter/copyright' ) [ 0 ]->nodeValue = '© ' . date( 'Y' ) . ( count( $this->readAuthors() ) ? ' ' . ( $this->readAuthors() [ 0 ]->profile->getReadableFullName() . ( count( $this->readAuthors() ) > 1 ? ' (et al.)' : '' ) ) : '' );
                                $xpath->evaluate( '/chapter/copyright_text' ) [ 0 ]->nodeValue = ( ( string )t( ' This is an Open Access publication distributed under the terms of the ' ) ) . $license->getElement( 'description' ) . '. ' . ( ( string )t( 'See license information at ' ) ) . $license->getElement( 'url' );
                        }
                        #echo $doc->saveXML(); exit();
                        return $doc->saveXML();
                }
        
                /**
                 * @return array
                 */
                public function getCorrespondingAuthors () {
                        $authors = [];
                        
                        foreach ( json_decode( $this->getElement( 'authors' ), true ) as $_ ) {
                                
                                if ( array_key_exists('is_corresponding', $_) && $_ [ 'is_corresponding' ] == 1 && $_ [ 'uid' ] ) {
                                        $authors [] = new User ( $_ [ 'uid' ] );
                                }
                        }
                        
                        return $authors;
                }
        
                /**
                 * @param null $book
                 * @param string $format
                 * @return false|string
                 */
                public function getCitationNote ($book = null, $format = 'html' ) {
                        
                        $workflow = WorkflowManager::getItem($this->elements['wfid']) ?? null;
                        if($this->getElement('citation_note') && ($workflow && $workflow->getElement('state') == 'published')) return base64_decode($this->getElement('citation_note'));
                        
                        $cnstring = '~AUTHORLIST~~TITLE~~VERSION~In: ~CNSTRING~~PUBLICATIONPLACE~~PUBLISHER~~BEGINPUBLICATION~-.';
                        $book = null;
                        if ( $book === null ) $book = new Book ( $this->getElement( 'bkid', false ) );
                        
                        if ( $book->hasControlElement( 'cn_version_string' ) && $book->hasControlElement( 'cn_version_string_prefix' ) ) {
                                
                                $vars = [];
                                foreach ( $this->getKeys() as $key ) $vars [ "chapter.field.$key" ] = $this->getElement( $key );
                                $versionString = $book->getControlElement( 'cn_version_string' );
                                $versionString = str_replace( array_keys( $vars ), array_values( $vars ), $versionString );
                                
                                if ( !empty ( $versionString ) ) $version = implode( ' ', array_filter( [ $book->getControlElement( 'cn_version_string_prefix' ), $versionString ] ) );
                        }
                        else {
                                $version = $this->getElement( 'version' );
                        }
                        
                        $cnstring = str_replace( '~TITLE~', $this->getElement( 'title' ) . '. ', $cnstring );
                        $cnstring = str_replace( '~VERSION~', !empty ( $version ) ? $version . '. ' : '', $cnstring );
                        $cnstring = str_replace( '~CNSTRING~', !empty ( $book->getElement( 'citation_note' ) ) ? $book->getElement( 'citation_note' ) . '. ' : '', $cnstring );
                        $cnstring = str_replace( '~BEGINPUBLICATION~', $book->getElement( 'begin_publication' ), $cnstring );
                        $cnstring = str_replace( '~PUBLICATIONPLACE~', ( !empty ( $this->getElement( 'publication_place' ) ) ? $this->getElement( 'publication_place' ) : $book->getElement( 'publication_place' ) ) . ': ', $cnstring );
                        $cnstring = str_replace( '~PUBLISHER~', ( !empty ( $this->getElement( 'publisher' ) ) ? $this->getElement( 'publisher' ) : $book->getElement( 'publisher' ) ) . '; ', $cnstring );
                        
                        // set authors
                        $authors = [];
                        $authors_for_cn = [];
                        
                        $chapter_authors = json_decode( $this->getElement( 'authors' ) );
                        
                        if ( !is_array( $chapter_authors ) ) {
                                
                                // first author who's written publication
                                // $authors[] = $chapter->getElement('author_uid');
                                
                                // second authors which additional selected
                                if ( !empty ( $this->getElement( 'authors' ) ) ) {
                                        foreach ( explode( ',', $this->getElement( 'authors' ) ) as $_ ) {
                                                if ( !in_array( $_, $authors ) ) {
                                                        $authors [] = $_;
                                                }
                                        }
                                }
                                
                                // generate author string
                                foreach ( $authors as $uid ) {
                                        $_user = new User ( $uid );
                                        $authors_for_cn [] = $_user->profile->getElement( 'lastname' ) . ' ' . substr( $_user->profile->getElement( 'firstname' ), 0, 1 );
                                }
                                
                                // third more authors
                                if ( !empty ( $this->getElement( 'more_authors' ) ) ) {
                                        foreach ( json_decode( $this->getElement( 'more_authors' ), true ) as $_ ) {
                                                $authors_for_cn [] = $_ [ 'more_author_lastname' ] . ' ' . substr( $_ [ 'more_author_name' ], 0, 1 );
                                        }
                                }
                        }
                        else {
                                
                                $corporations_for_cn = [];
                                $authors = [];
                                
                                foreach ( $chapter_authors as $_ ) {
                                        
                                        $weight = $_->weight;
                                        
                                        while ( array_key_exists( $weight, $authors ) ) $weight++;
                                        
                                        $authors [ $weight ] = $_;
                                }
                                
                                ksort( $authors );
                                
                                foreach ( $authors as $_ ) {
                                        
                                        $initials = '';
                                        $firstnames = preg_split( '/([\\s-]+)/', $_->firstname, -1 );
                                        
                                        $i = 0;
                                        
                                        foreach ( $firstnames as $name ) {
                                                if ( !empty ( $name ) ) $initials .= strtoupper( substr( $name, 0, 1 ) );
                                                $i++;
                                                if ( $i == 2 ) break;
                                        }
                                        
                                        $authors_for_cn [] = $_->lastname . ' ' . $initials;
                                }
                                
                                // fourth corporations
                                if ( !empty ( $this->getElement( 'corporation' ) ) ) {
                                        foreach ( json_decode( $this->getElement( 'corporation' ), true ) as $_ ) {
                                                $corporations_for_cn [] = $_;
                                        }
                                }
                                
                                $authors_for_cn = array_filter( $authors_for_cn );
                                $corporations_for_cn = array_filter( $corporations_for_cn );
                        }
                        
                        $author_string_for_cn = '';
                        $author_string_for_cn .= implode( ', ', $authors_for_cn );
                        $author_string_for_cn .= ( !empty ( $author_string_for_cn ) ? ( count( $corporations_for_cn ) ? '; ' : '' ) : '' ) . implode( '; ', $corporations_for_cn );
                        
                        $cnstring = str_replace( '~AUTHORLIST~', !empty ( $author_string_for_cn ) ? $author_string_for_cn . '. ' : '', $cnstring );
                        
                        if ( $format == 'html' ) $cnstring .= !empty ( $this->getElement( 'doi' ) ) ? '<br>DOI: <a href="http://dx.doi.org/' . $this->getElement( 'doi' ) . '">' . $this->getElement( 'doi' ) . '</a>' : '';
                        else
                                $cnstring .= !empty ( $this->getElement( 'doi' ) ) ? "\n" . 'DOI: ' . $this->getElement( 'doi' ) : '';
                        
                        $this->setElement('citation_note', base64_encode($cnstring));
                        
                        return $cnstring;
                }
        
                /**
                 * @return array
                 */
                public function getKeys () {
                        return array_keys( $this->elements );
                }
        
                /**
                 * @return mixed
                 */
                public function readAuthors () {
                        return $this->authors;
                }
        
                /**
                 * @param string $type
                 * @return \Drupal\Core\GeneratedUrl|Url|false|int|string
                 */
                public function getLink($type = 'link'){ return $this->getChapterLink($type); }
        
                /**
                 * @param string $type
                 * @return \Drupal\Core\GeneratedUrl|Url|false|int|string
                 */
                public function getChapterLink ($type = 'link' ) {
                        
                        /**
                         * use title only if this title is unique
                         */
                        $book = new Book ( $this->getElement( 'bkid' ), true );
                        $qry = \Drupal::database()->select( 'rwPubgoldBookChapters', 't' );
                        $qry->fields( 't', [ $this->tbl_column_prefix . 'title' ] )->condition( $this->tbl_column_prefix . 'title', $this->getElement( 'title' ), '=' );
                        $qry = $qry->countQuery()->execute();
                        
                        $id = '';
                        
                        if ( $qry->fetchField() == 1 ) {
                                $id = $this->getElement( 'title' );
                        }
                        else {
                                $id = $this->id;
                        }
                        
                        $title = $this->getElement( 'id' );
                        $bookTitle = $book->getElement( 'id' );
                        $url = 0;
                        
                        if ( $title && $bookTitle) {
                                try {
                                        $url = \Drupal\Core\Url::fromRoute( 'publisso_gold.book.chapter', [ 'bk_id' => $bookTitle, 'cp_id' => $title ] );
                                }
                                catch ( \Exception $e ) {
                                        
                                        try {
                                                $urlTitle = urlencode( $title );
                                                $url = \Drupal\Core\Url::fromRoute( 'publisso_gold.book.chapter', [ 'bk_id' => $bookTitle, 'cp_id' => $urlTitle ] );
                                        }
                                        catch ( \Exception $e ) {
                                                
                                                $booktitle = urlencode( $bookTitle );
                                                
                                                try {
                                                        $url = \Drupal\Core\Url::fromRoute( 'publisso_gold.book.chapter', [ 'bk_id' => $bookTitle, 'cp_id' => $title ] );
                                                }
                                                catch ( \Exception $e ) {
                                                        $url = \Drupal\Core\Url::fromRoute( 'publisso_gold.book.chapter', [ 'bk_id' => $bookTitle, 'cp_id' => $urlTitle ] );
                                                }
                                        }
                                }
                        }
                        
                        if($url) $url->setAbsolute();
                        
                        return !$url ? false : $type == 'link' ? $url->toString() : ( $type == 'url' ? $url : false );
                }
        
                /**
                 * @return array
                 */
                public function getAuthorsIntNames () {
                        $authors = $ret = [];
                        foreach ( json_decode( $this->getElement( 'authors' ) ) as $_ ) $authors [ $_->weight ] = $_;
                        ksort( $authors );
                        
                        foreach ( $authors as $_ ) {
                                
                                $initials = '';
                                $firstnames = preg_split( '/([\\s-]+)/', $_->firstname, -1 );
                                
                                $i = 0;
                                foreach ( $firstnames as $name ) {
                                        if ( !empty ( $name ) ) $initials .= strtoupper( substr( $name, 0, 1 ) );
                                        $i++;
                                        if ( $i == 2 ) break;
                                }
                                
                                $ret [] = $_->lastname . ' ' . $initials;
                        }
                        
                        return $ret;
                }
        
                /**
                 * @param string $element
                 * @param string $version
                 * @return false|mixed|null
                 */
                public function getRawElement (string $element, $version = '<current>' ) {
                        $ret = null;
                        
                        switch ( $version ) {
                                case '<previous>' :
                                        if ( $this->getElement( 'previous_version' ) ) $ret = \Drupal::database()->select( 'rwPubgoldBookChapters', 't' )->fields( 't', [ 'cp_' . $element ] )->condition( 'cp_id', $this->getElement( 'previous_version' ), '=' )->execute()->fetchField();
                                        break;
                                
                                case '<current>' :
                                        $ret = $this->getElement( $element );
                                        break;
                                
                                case '<next>' :
                                        if ( $this->getElement( 'next_version' ) ) $ret = \Drupal::database()->select( 'rwPubgoldBookChapters', 't' )->fields( 't', [ 'cp_' . $element ] )->condition( 'cp_id', $this->getElement( 'next_version' ), '=' )->execute()->fetchField();
                                        break;
                        }
                        
                        return $ret;
                }
        
                /**
                 * @param $bk_id
                 * @param $cp_id
                 * @param $action
                 * @return array|string[]|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function PDF ($bk_id, $cp_id, $action ) {
                        $this->id = $cp_id;
                        $this->load( $this->id, true );
                        
                        $currentUrl = \Drupal::routeMatch()->getRouteName();
                        $currentParams = \Drupal::routeMatch()->getRawParameters()->all();
                        
                        if ( !$this->getElement( 'pdf_blob_id' ) ) {
                                
                                return [ '#markup' => ( string )$this->t( 'There is no PDF stored for this chapter!' ), '#prefix' => '<div class="rwError">', '#suffix' => '</div>' ];
                        }
                        
                        switch ( $action ) {
                                
                                case 'show' :
                                        return $this->redirect( 'publisso_gold.export.chapter', [ 'cp_id' => $cp_id ] );
                                        break;
                                
                                case 'delete' :
                                        return [ \Drupal::formBuilder()->getForm( '\Drupal\publisso_gold\Form\publisso_goldBookchapterDeletePDF', [ 'cp_id' => $cp_id, 'route' => [ 'name' => $currentUrl, 'param' => $currentParams ], 'referrer' => \Drupal::request()->server->get( 'HTTP_REFERER' ) ] ) ];
                                        break;
                        }
                }
        
                /**
                 * @param $bk_id
                 * @param $cp_id
                 * @param $action
                 * @return string
                 */
                public function PDF_title ($bk_id, $cp_id, $action ) {
                        return ( string )$this->t( 'Publisso Bookchapter RIS - @action', [ '@action' => $action ] );
                }
        
                /**
                 * @param $bk_id
                 * @param $cp_id
                 * @param $action
                 * @return BinaryFileResponse
                 */
                public function RIS ($bk_id, $cp_id, $action ) {
                        $this->id = $cp_id;
                        $this->load( $this->id, true );
                
                        $currentUrl = \Drupal::routeMatch()->getRouteName();
                        $currentParams = \Drupal::routeMatch()->getRawParameters()->all();
                
                        switch ( $action ) {
                        
                                case 'show' :
                                        return $this->getRISData();
                                        break;
                        }
                }
        
                /**
                 * @return BinaryFileResponse
                 */
                private function getRISData(){
                        
                        $risdata = "TY  - ::TY::\nT1  - ::T1::\nT2  - ::T2::\nY1  - ::Y1::\nA1  - ::A1_1::\nA1  - ::A1_2::\nKW  - ::KW_1::\nKW  - ::KW_2::\nJF  - ::JF::\nPB  - ::PB::\nCY  - ::CY::\nER  -";
        
                        $book = new Book ( $this->getElement( 'bkid', false ) );
                        
                        $ary = [
                                ['TY'    => 'CHAP'],
                                ['T1'    => $this->getElement('title')],
                                ['T2'    => $book->getElement('title')]
                        ];
                        
                        $authors = json_decode($this->getElement('authors'));
                        usort($authors, ['self', 'sortAuthors']);
                        
                        foreach($authors as $_){
                                $ary[] = ['AU' => implode(', ', [$_->lastname, $_->firstname])];
                        }
                        
                        $corporations = json_decode($this->getElement('corporation'));
                        
                        if(is_array($corporations)){
                                foreach($corporations as $_){
                                        $ary[] = ['AU' => $_];
                                }
                        }
                        
                        foreach($book->readEditorsInChief() as $eic){
                                $ary[] = ['ED' => $eic->profile->getReadableName()];
                        }

                        //corresponding author
                        $authors_string = [];

                        foreach ( $this->getCorrespondingAuthors() as $author ) {

                                $items = [ $author->profile->getReadableFullName( 'FL', ' ' ), $author->profile->getElement( 'institute' ), $author->profile->getElement( 'department' ), $author->profile->getElement( 'street' ), $author->profile->getElement( 'postal_code' ), $author->profile->getElement( 'city' ), \Drupal::service( 'publisso_gold.tools' )->getCountry( $author->profile->getElement( 'country' ) ), ( !empty ( $author->profile->getElement( 'telephone' ) ) ? ( ( string )t( 'Phone: ' ) ) . $author->profile->getElement( 'telephone' ) : '' ), 'E-mail: ' . $author->profile->getElement( 'email' ) ];

                                $authors_string [] = implode( ', ', array_filter( $items ) );
                        }

                        $authors_string = implode( '; ', array_filter( $authors_string ) );
                        if(!empty($authors_string)){
                                $ary[] = ['AD' => $authors_string];
                        }

                        if(!empty($this->getElement('abstract'))){
                                $ary[] = ['N2' => preg_replace('/\s{2,}/', ' ', preg_replace('/\n/', ' ', html_entity_decode(strip_tags(base64_decode($this->getElement('abstract'))))))];
                        }

                        $published = explode('-', $this->getElement('published'));
                        $ary[] = ['PY' => $published[0]];
                        $ary[] = ['DA' => implode('/', $published)];

                        if(!empty($this->getElement('doi'))){
                                $ary[] = ['DO' => $this->getElement('doi')];
                        }

                        if(!empty($this->getElement('version'))){
                                $ary[] = ['ET' => $this->getElement('version')];
                        }

                        if(!empty($this->getElement('language'))){
                                $ary[] = ['LA' => $this->getElement('language')];
                        }
                        elseif(!empty($book->getElement('language'))){
                                $ary[] = ['LA' => $book->getElement('language')];
                        }

                        if(!(empty($this->getElement('pdf_blob_id')))){
                                $url = Url::fromRoute('publisso_gold.getFile', ['fid' => $this->getElement('pdf_blob_id')])->setAbsolute()->toString();
                                $ary[] = ['L1' => $url];
                        }

                        if(!empty($this->getElement('files'))){
                                $files = explode(',', $this->getElement('files'));
                                if(is_array($files)){
                                        $url = null;
                                        foreach($files as $_){
                                                $url = Url::fromRoute('publisso_gold.getFile', ['fid' => $_])
                                                          ->setAbsolute()->toString();
                                                $ary[] = ['L1' => $url];
                                        }
                                }
                        }

                        if(!empty($this->getElement('doi'))){
                                $ary[] = ['UR' => 'https://dx.doi.org/'.$this->getElement('doi')];
                                $ary[] = ['L2' => 'https://dx.doi.org/'.$this->getElement('doi')];
                        }
                        else{
                                $ary[] = ['UR' => $this->getLink()];
                                $ary[] = ['L2' => $this->getLink()];
                        }
                        
                        foreach(json_decode($this->getElement('keywords'), true) as $_){
                                $ary[] = ['KW' => $_];
                        }
                        
                        $ary[] = ['PB' => $this->getElement('publisher') ?? $book->getElement('publisher')];
                        $ary[] = ['CY' => $this->getElement('publication_place') ?? $book->getElement('publication_place')];
                        $ary[] = ['ER' => ''];
                        
                        for($i = 0; $i < count($ary); $i++){
                                $ary[$i] = implode('  - ', [key($ary[$i]).'  - '.$ary[$i][key($ary[$i])]]);
                        }
                        
                        $risdata = implode("\n", $ary);
        
                        $outfilename = $this->getElement('id').'.ris';
                        if(!empty($this->getElement('doi'))){
                                $outfilename = explode('/', $this->getElement('doi'));
                                $outfilename = end($outfilename).'.ris';
                        }
                        
                        $filename = \Drupal::service('publisso_gold.tools')->uniqid();
                        file_put_contents("/tmp/$filename", $risdata);
                        $response = new BinaryFileResponse("/tmp/$filename");
                        $response->deleteFileAfterSend(true);
                        $response->setContentDisposition(
                                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                                $outfilename,
                                iconv('UTF-8', 'ASCII//TRANSLIT', $outfilename)
                        );
                        return $response;
                }
        
                /**
                 * @param $bk_id
                 * @param $cp_id
                 * @param $action
                 * @return string
                 */
                public function RIS_title ($bk_id, $cp_id, $action ) {
                        return ( string )$this->t( 'Publisso Bookchapter PDF - @action', [ '@action' => $action ] );
                }
        
                /**
                 * @return Book
                 */
                public function getMedium () {
                        return new Book( $this->getElement( 'bkid' ), true );
                }
        
                /**
                 * @param $elementName
                 * @return mixed|null
                 */
                public function getParentElement($elementName){
                        $book = new Book( $this->getElement( 'bkid' ), true );
                        return $book->getElement($elementName) ?? null;
                }
        
                /**
                 * @param $action
                 * @param null $lang
                 * @return false|string|null
                 */
                public function getDate ($action, $lang = null ) {

                        if($this->getElement($action, $lang)) return date("Y-m-d", strtotime($this->getElement($action)));
                        if ( array_key_exists($action, $this->lastStates) ) return date("Y-m-d", strtotime
                        ($this->lastStates[$action]));
                        else return null;
                }
        
                /**
                 * @return string
                 */
                private function getMediumTheme () {
                        $theme = 'publisso';
                        $qry = \Drupal::database()->select( 'rwPubgoldBooks', 't' )->fields( 't', [ 'bk_theme' ] )->condition( 'bk_id', $this->getElement( 'bkid' ), '=' )->execute();
                        $res = $qry->fetchField();
                        return $res ? $res : $theme;
                }
        
                /**
                 * @return false
                 */
                private function initStateChain () {
                        
                        if($this->getElement('wf_state_chain') === null) return false;
                        
                        $sc = $this->getElement('wf_state_chain');
                        $_ = unserialize($sc);
                        
                        if(!is_array($_)) return false;
                        
                        $stateMap = [
                                'submission finished' => 'received',
                                'revision finished'   => 'revised',
                                'accepted'            => 'accepted',
                                'published'           => 'published'
                        ];
                        
                        $states = [];
                        
                        foreach($_ as $__){
                                
                                if(array_key_exists($__->state_new, $stateMap)){
                                        $states[$stateMap[$__->state_new]] = $__->time;
                                }
                        }
                        
                        $this->lastStates = $states;
                }
        
                /**
                 * @return Url|null
                 */
                public function getErratumUrl(){
                        if($this->id) {
                                return Url::fromRoute( 'publisso_gold.book.chapter.seterratum', [ 'cp_id' => $this->id ] );
                        }
                        else return null;
                }
        }

?>
