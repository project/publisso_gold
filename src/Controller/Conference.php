<?php
        
        
        namespace Drupal\publisso_gold\Controller;
        
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\file\Entity\File;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Workflow\AbstractWorkflow;

        /**
         * Class Conference
         * @package Drupal\publisso_gold\Controller
         */
        class Conference extends ControllerBase {
        
                /**
                 * @param string $action
                 * @param null $param
                 * @return array
                 */
                public function menuAction(string $action, $param = null){
                
                        try{
                                if(preg_match('/^(.+)\|(\d+)/', $action, $matches)){
                                        $action = $matches[1];
                                        $param = $matches[2];
                                }
                                return $this->{'menuAction_'.strtoupper($action)}($param);
                        }
                        catch(\Error $e){
                                \Drupal::service('messenger')->addError((string)$this->t('An error occured. Please contact system-administrator!'));
                                Publisso::log($e->getMessage().' ('.$e->getLine().' in '.$e->getFile().')');
                                return [];
                        }
                }
        
                /**
                 * @param null $param
                 * @return array
                 */
                private function menuAction_OVERVIEW($param = null){
                
                        if(!(\Drupal::service('publisso_gold.tools')->userHasAccessRight('conferencemanagement.overview', \Drupal::service('session')->get('user')['weight']) ||
                                ConferenceManager::isManagerOfAnyConference())){
                                return \Drupal::service('publisso_gold.tools')->accessDenied();
                        }
                
                        $ret = [
                                'content' => [],
                                '#cache' => [
                                        'max-age' => 0
                                ]
                        ];
                        
                        if(\Drupal::service('publisso_gold.tools')->userHasAccessRight('conferencemanagement.add', \Drupal::service('session')->get('user')['weight'])) {
                        
                                $ret[ 'content' ][ 'addConference_top' ] = [
                                        '#type'  => 'link',
                                        '#title' => (string)$this->t( \Drupal::service( 'publisso_gold.texts' )->get( 'conferencemanagement.lnk.add' ) ),
                                        '#url'   => Url::fromRoute( 'publisso_gold.conferencemanagement', [ 'action' => 'add' ], ['attributes' => ['class' => ['btn', 'btn-success']]] ),
                                        '#suffix' => '<hr>'
                                ];
                        }
        
                        if(\Drupal::service('publisso_gold.tools')->userHasAccessRight('conferencemanagement.list', \Drupal::service('session')->get('user')['weight']) ||
                           ConferenceManager::isManagerOfAnyConference()) {
                                
                                $ret['content']['cfTable'] = [
                                        '#type' => 'table',
                                        '#header' => [
                                                [
                                                        'data' => (string)$this->t('Logo')
                                                ],
                                                [
                                                        'data' => (string)$this->t('Title')
                                                ],
                                                [
                                                        'data' => (string)$this->t('Creator')
                                                ],
                                                [
                                                        'data' => (string)$this->t('Edited')
                                                ],
                                                [
                                                        'data' => (string)$this->t('Published')
                                                ],
                                                []
                                        ],
                                        '#empty' => (string)$this->t('No Conferences found'),
                                        '#rows' => []
                                ];
                                
                                foreach(ConferenceManager::getConferencesList(['cf_id', 'cf_title', 'cf_logo', 'cf_edited', 'cf_published', 'cf_creator_uid']) as $_){
                                        
                                        unset($user);
                                        unset($managers);
                                        
                                        $managers = ConferenceManager::getMagerUIDs($_->cf_id);
                                        
                                        if($_->cf_creator_uid){
                                                $user = new User($_->cf_creator_uid);
                                        }
                                        
                                        if($_->cf_logo){
                                                $logo = File::load($_->cf_logo);
                                                $urlLogo = str_replace(DRUPAL_ROOT, '', \Drupal::service('file_system')->realpath($logo->getFileUri()));
                                        }
                                        else{
                                                $urlLogo = '/'.\Drupal::service('module_handler')->getModule('publisso_gold')->getPath().'/images/Conference-Logo-Dummy.png';
                                        }
                                        
                                        $links = [];
                                        
                                        //edit conference
                                        if(\Drupal::service('publisso_gold.tools')->userHasAccessRight('conferencemanagement.add', \Drupal::service('session')->get('user')['weight'])) {
                
                                                $links['edit'] = [
                                                        'url' => Url::fromRoute('publisso_gold.conferencemanagement', ['action' => 'edit|'.$_->cf_id]),
                                                        'title' => (string)$this->t('Edit')
                                                ];
                                        }
        
                                        if(\Drupal::service('publisso_gold.tools')->userHasAccessRight('conferencemanagement.add', \Drupal::service('session')->get('user')['weight']) ||
                                           in_array(\Drupal::service('session')->get('user')['id'], $managers)){
                                                $links['complete'] = [
                                                        'url' => Url::fromRoute('publisso_gold.conferencemanagement', ['action' => 'complete|'.$_->cf_id]),
                                                        'title' => (string)$this->t('Complete')
                                                ];
                                        }
        
                                        if(\Drupal::service('publisso_gold.tools')->userHasAccessRight('conferencemanagement.add', \Drupal::service('session')->get('user')['weight']) ||
                                           in_array(\Drupal::service('session')->get('user')['id'], $managers)){
                                                $links['sessions'] = [
                                                        'url' => Url::fromRoute('publisso_gold.conferencemanagement', ['action' => 'sessions|'.$_->cf_id]),
                                                        'title' => (string)$this->t('Edit sessions')
                                                ];
                                        }
                                        
                                        //complete conference
                                        if(count($links)) {
                                                
                                                $ret[ 'content' ][ 'cfTable' ][ '#rows' ][] = [
                                                        [
                                                                'data' => [
                                                                        '#markup' => '<img src="' . $urlLogo . '" width="50">'
                                                                ]
                                                        ],
                                                        [
                                                                'data' => [
                                                                        '#markup' => $_->cf_title
                                                                ]
                                                        ],
                                                        [
                                                                'data' => [
                                                                        '#markup' => isset( $user ) ? $user->profile->getReadableName() : (string)$this->t( 'SYSTEM' )
                                                                ]
                                                        ],
                                                        [
                                                                'data' => [
                                                                        '#markup' => $_->cf_edited == -1 ? (string)$this->t( 'Never' ) : ( $_->cf_edited == 0 ? (string)$this->t( 'By manager' ) : (string)$this->t( 'By PUBLISSO' ) )
                                                                ]
                                                        ],
                                                        [
                                                                'data' => [
                                                                        '#markup' => $_->cf_published ? (string)$this->t( 'Yes' ) : (string)$this->t( 'No' )
                                                                ]
                                                        ],
                                                        [
                                                                'data' => [
                                                                        '#type'  => 'dropbutton',
                                                                        '#links' => $links
                                                                ]
                                                        ]
                                                ];
                                        }
                                }
                        }
        
                        return $ret;
                }
        
                /**
                 * @param null $param
                 * @return array
                 */
                private function menuAction_ADD($param = null) {
                
                        if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'conferencemanagement.add', \Drupal::service( 'session' )->get( 'user' )[ 'weight' ] ) ) {
                                return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        }
                
                        $ret = [
                                'content' => [],
                                '#cache' => [
                                        'max-age' => 0
                                ]
                        ];
                
                        $ret['content']['add'] = [
                                \Drupal::formBuilder()->getForm('\Drupal\publisso_gold\Form\Conferences\AddConference')
                        ];
                
                        return $ret;
                }
        
                /**
                 * @param null $param
                 * @return array
                 */
                private function menuAction_EDIT($param = null) {
                
                        if ( !\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'conferencemanagement.add', \Drupal::service( 'session' )->get( 'user' )[ 'weight' ] ) ) {
                                return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        }
                
                        $ret = [
                                'content' => [],
                                '#cache' => [
                                        'max-age' => 0
                                ]
                        ];
                
                        $ret['content']['add'] = [
                                \Drupal::formBuilder()->getForm('\Drupal\publisso_gold\Form\Conferences\EditConference', ['cf_id' => $param])
                        ];
                
                        return $ret;
                }
        
                /**
                 * @param null $param
                 * @return array
                 */
                private function menuAction_COMPLETE($param = null) {
                        
                        $conference = ConferenceManager::getConference($param);
                        
                        if ( !(\Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'conferencemanagement.add', \Drupal::service( 'session' )->get( 'user' )[ 'weight' ] ) ||
                                $conference->isUserManager())) {
                                return \Drupal::service( 'publisso_gold.tools' )->accessDenied();
                        }
                
                        $ret = [
                                'content' => [],
                                '#cache' => [
                                        'max-age' => 0
                                ]
                        ];
                
                        $ret['content']['complete'] = [
                                \Drupal::formBuilder()->getForm('\Drupal\publisso_gold\Form\Conferences\CompleteConference', ['cf_id' => $param])
                        ];
                
                        return $ret;
                }
        
                /**
                 * @param null $param
                 * @return array
                 */
                private function menuAction_SESSIONS($param = null){
                        
                        return \Drupal::formBuilder()->getForm('\Drupal\publisso_gold\Form\Conferences\EditSessions', ['cf_id' => $param]);
                }
        
                /**
                 * @param $action
                 * @param $param
                 * @return string
                 */
                public function getTitle($action, $param){
        
                        if(preg_match('/^(.+)\|(\d+)/', $action, $matches)){
                                $action = $matches[1];
                                $param = $matches[2];
                        }
                        
                        $title = ucfirst($action).' Conference';
                        $strParam = [];
                        
                        if($param){
                                
                                $conference = ConferenceManager::getConference($param);
                                
                                if($conference) {
                                        $title .= ' "@title"';
                                        $strParam[ '@title' ] = $conference->getElement( 'title' );
                                }
                        }
                        
                        return (string)$this->t($title ?? 'n.a.', $strParam);
                }
        
                /**
                 * @param $param
                 * @return array|mixed|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                private function menuAction_SUBMIT($param){
                        
                        $conference = ConferenceManager::getConference($param);
                        
                        if(null !== ($pin = $conference->getElement('submission_pin'))){
                                $form = '\Drupal\publisso_gold\Form\ConferencePinCheck';
                        }
                        else{
                                return $this->initSubmission($param);
                        }
                        
                        return \Drupal::formBuilder()->getForm($form, ['cf_id' => $param]);
                }
        
                /**
                 * @param int $cf_id
                 * @param false $returnID
                 * @return mixed|\Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function initSubmission(int $cf_id, $returnID = false){
        
                        $workflow = new AbstractWorkflow();
                        $workflow->init( $cf_id, 'cas');
                        
                        if(!$returnID)
                                return $this->redirect( 'publisso_gold.workflow.item', [ 'wf_id' => $workflow->getElement( 'id' ) ] );
                        else
                                return $workflow->getElement('id');
                }
        }
