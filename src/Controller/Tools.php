<?php

        /**
         * @file
         * Contains \Drupal\publisso_gold\Controller\Tools.
         */

        namespace Drupal\publisso_gold\Controller;

        use Drupal\Core\Url;
        use \Symfony\Component\HttpFoundation\RedirectResponse;
        use \Drupal\file\Entity\File;
        use Drupal\migrate\Plugin\migrate\process\Substr;
        use Symfony\Component\HttpFoundation\Response;
        use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
        
        /**
         * Class Tools
         * @package Drupal\publisso_gold\Controller
         */
        class Tools
        {
        
                /**
                 * @return string
                 */
                public function __toString()
                {
                        return 'Tools';
                }
        
                /**
                 * @param array $filter
                 * @param array $order
                 * @return array
                 */
                public function getJournalIDs(array $filter = [], array $order = [])
                {

                        $ids = [];
                        $result = \Drupal::database()->select('rwPubgoldJournals', 'jrn')->fields('jrn', ['jrn_id']);

                        foreach ($filter as $field => $value) {
                                $result->condition($field, $value, '=');
                        }

                        if (count($order)) {
                                foreach ($order as $field => $dir) {
                                        $result->orderBy($field, $dir);
                                }
                        } else {
                                $result->orderBy('jrn.jrn_id');
                                $result->orderBy('jrn.jrn_title');
                                $result->orderBy('jrn.jrn_title_abbr');
                        }

                        foreach ($result->execute()->fetchAll() as $_) {
                                array_push($ids, $_->jrn_id);
                        }

                        return $ids;
                }
        
                /**
                 * @param $nodeName
                 * @param $nodeValue
                 * @param $attributes
                 * @param $doc
                 * @param $xpath
                 * @param false $withTextNode
                 * @return false
                 */
                public function DOMAppendChild($nodeName, $nodeValue, $attributes, &$doc, &$xpath, $withTextNode = false)
                {

                        if (!$withTextNode) {

                                try {
                                        $element = $doc->createElement($nodeName, $nodeValue);
                                } catch (\Exception $e) {
                                        $this->logMsg(__CLASS__ . '::' . __METHOD__ . ' (Line: ' . __LINE__ . '): '
                                                . $e->getMessage());
                                }
                        }

                        if ($withTextNode || !$element) {
                                $element = $doc->createElement($nodeName);
                                $element->appendChild($doc->createTextNode(html_entity_decode($nodeValue)));
                        }

                        foreach ($attributes as $k => $v) $element->setAttribute($k, $v);
                        if ($element) return $xpath->appendChild($doc->importNode($element, true));
                        else return false;
                }
        
                /**
                 * @param $doc
                 * @param $node
                 * @param $text
                 */
                public function appendCdata(&$doc, &$node, $text)
                {

                        $cd = $doc->createCDATASection($text);
                        $node->appendChild($cd);
                }
        
                /**
                 * @param $d
                 * @param $chunk
                 * @return mixed
                 */
                public function makeDOMFragment($d, $chunk)
                {
                        $fragment = $d->createDocumentFragment();
                        if (!empty($chunk)) $fragment->appendXML($chunk);
                        return $fragment;
                }
        
                /**
                 * @param $jrn_id
                 * @param $year
                 * @param $number
                 * @return bool
                 */
                public function volumeExists($jrn_id, $year, $number)
                {
                        return !!\Drupal::database()->select('rwPubgoldJournalVolume', 't')->fields('t', [])->condition('jrn_id', $jrn_id, '=')->condition('year', $year, '=')->condition('number', $number, '=')->countQuery()->execute()->fetchField();
                }
        
                /**
                 * @param $jrn_id
                 * @return mixed
                 */
                public function getJournalVolumes($jrn_id)
                {
                        $qry = \Drupal::database()->select('rwPubgoldJournalVolume', 't')->fields('t', ['id']);
                        $qry->addExpression("CONCAT_WS(' - ', `number`, `year`)");
                        return $qry->condition('jrn_id', $jrn_id, '=')->orderBy('year', 'DESC')->orderBy('number', 'DESC')->execute()->fetchAllKeyed();
                }
        
                /**
                 * @param $jrn_id
                 * @param $vol_id
                 * @return mixed
                 */
                public function getJournalIssues($jrn_id, $vol_id)
                {
                        return \Drupal::database()->select('rwPubgoldJournalIssue', 't')->fields('t', ['id', 'number'])->condition('jrn_id', $jrn_id, '=')->condition('vol_id', $vol_id, '=')->orderBy('number', 'ASC')->orderBy('id', 'ASC')->execute()->fetchAllKeyed();
                }
        
                /**
                 * @param $vol_id
                 * @return mixed
                 */
                public function getVolumeIssues($vol_id)
                {
                        return \Drupal::database()->select('rwPubgoldJournalIssue', 't')->fields('t', ['id', 'number'])->condition('vol_id', $vol_id, '=')->orderBy('number', 'ASC')->orderBy('id', 'ASC')->execute()->fetchAllKeyed();
                }
        
                /**
                 * @param $jrn_id
                 * @param $vol_id
                 * @param $number
                 * @return bool
                 */
                public function issueExists($jrn_id, $vol_id, $number)
                {
                        return !!\Drupal::database()->select('rwPubgoldJournalIssue', 't')->fields('t', [])->condition('jrn_id', $jrn_id, '=')->condition('vol_id', $vol_id, '=')->condition('number', $number, '=')->countQuery()->execute()->fetchField();
                }
        
                /**
                 * @return mixed
                 */
                public function getJournals()
                {
                        return \Drupal::database()->select('rwPubgoldJournals', 't')->fields('t', ['jrn_id', 'jrn_title'])->orderBy('jrn_title', 'ASC')->execute()->fetchAllKeyed();
                }
        
                /**
                 * @param $vol_id
                 * @return array
                 */
                public function getVolumeArticles($vol_id)
                {
                        return [];
                }
        
                /**
                 * @param $vol_id
                 * @return array
                 */
                public function getIssueArticles($vol_id)
                {
                        return [];
                }
        
                /**
                 * @return mixed
                 */
                public function getArticleTypes()
                {
                        return \Drupal::database()->select('rwPubgoldArticleTypes', 't')->fields('t', ['id', 'name'])->execute()->fetchAllKeyed();
                }
        
                /**
                 * @param $jrn_id
                 * @return int
                 */
                public function getMaxVolumeNumber($jrn_id)
                {

                        $qry = \Drupal::database()->select('rwPubgoldJournalVolume', 't')->condition('jrn_id', $jrn_id, '=');
                        $qry->addExpression('MAX(`number`)');
                        $ret = $qry->execute()->fetchField();

                        return $ret ? $ret : 0;
                }
        
                /**
                 * @return mixed
                 */
                public function getLicenses()
                {
                        return \Drupal::database()->select('rwPubgoldLicenses', 't')->fields('t', ['id', 'name'])->execute()->fetchAllKeyed();
                }
        
                /**
                 * @return mixed
                 */
                public function getPublicationPlaces()
                {
                        return \Drupal::database()->select('rwPubgoldPublicationPlaces', 't')->fields('t', ['id', 'name'])->execute()->fetchAllKeyed();
                }
        
                /**
                 * @param string $key
                 * @return mixed
                 */
                function getDDCs($key = 'id')
                {

                        $qry = \Drupal::database()->select('rwPubgoldDDC', 't')->fields('t', ['id']);
                        $qry->addExpression("CONCAT_WS(' ', `number`, `name`)", 'name');
                        return $qry->execute()->fetchAllKeyed();
                }
        
                /**
                 * @param $ddc
                 * @param string $field
                 * @return mixed
                 */
                function getDDC($ddc, $field = 'name')
                {

                        $qry = \Drupal::database()->select('rwPubgoldDDC', 't')->fields('t', [$field]);
                        $condgroup = $qry->orConditionGroup()->condition('id', $ddc, '=')->condition('name', $ddc, '=')->condition('number', $ddc, '=');
                        $qry->condition($condgroup);

                        return $qry->execute()->fetchField();
                }
        
                /**
                 * @return mixed
                 */
                function getBookCategories()
                {
                        return \Drupal::database()->select('rwPubgoldBookCategories', 't')->fields('t', ['bcat_id', 'bcat_name'])->execute()->fetchAllKeyed();
                }
        
                /**
                 * @param $number
                 * @param null $field
                 * @return mixed
                 */
                function getDDCByNumber($number, $field = null)
                {

                        $qry = \Drupal::database()->select('rwPubgoldDDC', 't')->fields('t', [$field ?? 'name']);
                        $qry->condition('number', $number, '=');

                        return $qry->execute()->fetchField();
                }
        
                /**
                 * @param $name
                 * @return mixed
                 */
                function getDDCIDByName($name)
                {

                        $qry = \Drupal::database()->select('rwPubgoldDDC', 't')->fields('t', ['id']);
                        $qry->condition('name', $name, '=');

                        return $qry->execute()->fetchField();
                }
        
                /**
                 * @return mixed
                 */
                function getReviewpages()
                {

                        return \Drupal::database()
                                ->select('rwPubgoldReviewPage', 'rp')
                                ->fields('rp', [])
                                ->execute()
                                ->fetchAll();
                }
        
                /**
                 * @param $index
                 * @param $name
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 * @throws \Exception
                 */
                function createDDC($index, $name)
                {

                        $qry = \Drupal::database()->select('rwPubgoldDDC', 't')->fields('t', ['id']);
                        $cond = $qry->orConditionGroup()->condition('name', $name, '=')->condition('number', $index, '=');
                        $qry->condition($cond);

                        if ($qry->countQuery()->execute()->fetchField() == 0)
                                return \Drupal::database()->insert('rwPubgoldDDC')->fields(['number' => $index, 'name' => $name])->execute();
                        else
                                return $qry->execute()->fetchField();
                }
        
                /**
                 * @param $pp
                 * @return mixed
                 */
                function getPublicationPlace($pp)
                {

                        $qry = \Drupal::database()->select('rwPubgoldPublicationPlaces', 't')->fields('t', ['name']);
                        $condgroup = $qry->orConditionGroup()->condition('id', $pp, '=')->condition('name', $pp, '=');
                        $qry->condition($condgroup);

                        return $qry->execute()->fetchField();
                }
        
                /**
                 * @param $name
                 * @return mixed
                 */
                function getPublicationPlaceIDByName($name)
                {

                        $qry = \Drupal::database()->select('rwPubgoldPublicationPlaces', 't')->fields('t', ['id']);
                        $qry->condition('name', $name, '=');

                        return $qry->execute()->fetchField();
                }
        
                /**
                 * @param $name
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 * @throws \Exception
                 */
                function createPublicationPlace($name)
                {
                        if (\Drupal::database()->select('rwPubgoldPublicationPlaces', 't')->fields('t', [])->condition('name', $name, '=')->countQuery()->execute()->fetchField() == 0)
                                return \Drupal::database()->insert('rwPubgoldPublicationPlaces')->fields(['name' => $name])->execute();
                        else
                                return \Drupal::database()->select('rwPubgoldPublicationPlaces', 't')->fields('t', ['id'])->condition('name', $name, '=')->execute()->fetchField();
                }
        
                /**
                 * @return mixed
                 */
                function getPublishers()
                {
                        return \Drupal::database()->select('rwPubgoldPublishers', 't')->fields('t', ['id', 'name'])->execute()->fetchAllKeyed();
                }
        
                /**
                 * @param $pp
                 * @return mixed
                 */
                function getPublisher($pp)
                {

                        $qry = \Drupal::database()->select('rwPubgoldPublishers', 't')->fields('t', ['name']);
                        $condgroup = $qry->orConditionGroup()->condition('id', $pp, '=')->condition('name', $pp, '=');
                        $qry->condition($condgroup);

                        return $qry->execute()->fetchField();
                }
        
                /**
                 * @param $name
                 * @return mixed
                 */
                function getPublisherIDByName($name)
                {

                        $qry = \Drupal::database()->select('rwPubgoldPublishers', 't')->fields('t', ['id']);
                        $qry->condition('name', $name, '=');

                        return $qry->execute()->fetchField();
                }
        
                /**
                 * @param $name
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 * @throws \Exception
                 */
                function createPublisher($name)
                {
                        if (\Drupal::database()->select('rwPubgoldPublishers', 't')->fields('t', [])->condition('name', $name, '=')->countQuery()->execute()->fetchField() == 0)
                                return \Drupal::database()->insert('rwPubgoldPublishers')->fields(['name' => $name])->execute();
                        else
                                return \Drupal::database()->select('rwPubgoldPublishers', 't')->fields('t', ['id'])->condition('name', $name, '=')->execute()->fetchField();
                }
        
                /**
                 * @return mixed
                 */
                function getCategories()
                {
                        return \Drupal::database()->select('rwPubgoldCategories', 't')->fields('t', ['id', 'name'])->execute()->fetchAllKeyed();
                }
        
                /**
                 * @param $pp
                 * @return mixed
                 */
                function getCategory($pp)
                {

                        $qry = \Drupal::database()->select('rwPubgoldCategories', 't')->fields('t', ['name']);
                        $condgroup = $qry->orConditionGroup()->condition('id', $pp, '=')->condition('name', $pp, '=');
                        $qry->condition($condgroup);

                        return $qry->execute()->fetchField();
                }
        
                /**
                 * @param $name
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 * @throws \Exception
                 */
                function createCategory($name)
                {
                        if (\Drupal::database()->select('rwPubgoldCategories', 't')->fields('t', [])->condition('name', $name, '=')->countQuery()->execute()->fetchField() == 0)
                                return \Drupal::database()->insert('rwPubgoldCategories')->fields(['name' => $name])->execute();
                }
        
                /**
                 * @param $pp
                 * @return mixed
                 */
                function getArticleType($pp)
                {

                        $qry = \Drupal::database()->select('rwPubgoldArticleTypes', 't')->fields('t', ['name']);
                        $condgroup = $qry->orConditionGroup()->condition('id', $pp, '=')->condition('name', $pp, '=');
                        $qry->condition($condgroup);

                        return $qry->execute()->fetchField();
                }
        
                /**
                 * @param $name
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 * @throws \Exception
                 */
                function getArticleTypeIDByName($name)
                {

                        $qry = \Drupal::database()->select('rwPubgoldArticleTypes', 't')->fields('t', ['id']);
                        $qry->condition('name', $name, '=');;

                        if (!$qry->countQuery()->fetchField()) {
                                return \Drupal::database()->insert('rwPubgoldArticleTypes')->fields('name', $name)->execute();
                        } else {
                                return $qry->execute()->fetchField();
                        }
                }
        
                /**
                 * @param $jrn_id
                 * @return array
                 */
                function getJournalEditors($jrn_id)
                {

                        $result = \Drupal::database()
                                ->select('rwpgvwJournalEditors', 'je')
                                ->fields('je', [])
                                ->condition('jrn_id', $jrn_id, '=')
                                ->execute()
                                ->fetchAll();

                        $editors = [];

                        foreach ($result as $_)
                                $editors[]['up_uid'] = $_->up_uid;

                        return $editors;
                }
        
                /**
                 * @param $id
                 * @return string|null
                 */
                function getCountry($id)
                {

                        if (preg_match('/^\d+$/', $id)) {
                                $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ascension", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Democratic Republic of the Congo (Kinshasa)", "Congo, Republic of (Brazzaville)", "Cook Islands", "Costa Rica", "Ivory Coast", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor Timor-Leste", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Metropolitan Areas", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Great Britain", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Rep. (North Korea)", "Korea, Republic of (South Korea)", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia, Rep. of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federal States of", "Moldova, Republic of", "Monaco", "Montenegro", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar, Burma", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestinian National Authority", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Island", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion Island", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Príncipe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Sri Lanka", "Saint Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syria, Syrian Arab Republic", "Taiwan, Republic of China", "Tajikistan", "Tanzania", "Thailand", "Tibet", "Timor-Leste (East Timor)", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "U.S. Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City State (Holy See)", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");
                        } else {
                                $countries = \Drupal::service('country_manager')->getStandardList();
                        }

                        return array_key_exists($id, $countries) ? (string)$countries[$id] : null;
                }
        
                /**
                 * @return mixed
                 */
                function getCountrylist()
                {

                        $list = \Drupal::service('country_manager')->getStandardList();

                        $mh = \Drupal::service('module_handler')->getModule('publisso_gold')->getPath();


                        /*
                                          foreach($list as $k => $v){

                                                  if(!is_file('/var/www/pubgold_dev/modules/custom/publisso_gold/images/cf/'.strtolower($k).'.png')){

                                                          $c = 'flag-of-'.str_replace(' ', '-', $v).'.png';
                                                          $curlSession = curl_init();
                                                          curl_setopt($curlSession, CURLOPT_URL, 'https://www.countries-ofthe-world.com/flags-normal/'.$c);
                                                          curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
                                                          curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
                                                          curl_setopt($curlSession, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

                                                          $cont = curl_exec($curlSession);
                                                          curl_close($curlSession);
                                                          //echo $cont;
                                                          if(preg_match('/Forbidden/', $cont)){

                                                                  echo "$k -- ($v): Forbidden\n";
                                                          }
                                                          else{
                                                                  file_put_contents('/var/www/pubgold_dev/modules/custom/publisso_gold/images/cf/'.strtolower($k).'.png', $cont);
                                                          }
                                                          //$cont = file_get_contents('https://www.countries-ofthe-world.com/flags-normal/'.$c);
                                                          //var_dump($cont);
                                                  }
                                          }
                                          */
                        foreach ($list as $k => $v) {

                                if (is_file(DRUPAL_ROOT . '/' . $mh . '/images/cf/' . strtolower($k) . '.png')) {
                                        $list[$k] = '<img src="/' . $mh . '/images/cf/' . strtolower($k) . '.png' . '" width="20">&nbsp;' . $v;
                                }
                        }

                        return \Drupal::service('country_manager')->getStandardList();
                }
        
                /**
                 * @param $id
                 * @return string|void
                 */
                function getSalutation($id)
                {

                        $salutation = [

                                0 => 'Mr',
                                1 => 'Ms'
                        ];

                        return t((string)$salutation[$id])->__toString();
                }
        
                /**
                 * @param false $lang_id
                 * @return array|mixed|string
                 */
                function getCorrespondenceLanguages($lang_id = false)
                {

                        $lang = [];

                        foreach (\Drupal::service('language_manager')->getStandardLanguageList() as $k => $v) {
                                $lang[$k] = "$v[0] ($v[1]])";
                        }

                        if ($lang_id === false)
                                return $lang;
                        elseif ($lang_id === null)
                                return '';
                        else
                                return $lang[$lang_id];
                }
        
                /**
                 * @param $text
                 * @param $references
                 * @param string $startMarker
                 * @param string $endMarker
                 * @return string|string[]
                 */
                function mapReferences($text, $references, $startMarker = "[", $endMarker = "]")
                {

                        $reference = $refindexes = [];
                        $startMarker = preg_quote($startMarker);
                        $endMarker = preg_quote($endMarker);
                        $regex = "/$startMarker([\d,]+)+?$endMarker/";

                        foreach (preg_split('/\n/', $references) as $line) {
                                
                                if (preg_match($regex, $line, $matches)) {
                                        Publisso::log($line);
                                        $ref = new \Drupal\publisso_gold\Controller\Reference();
                                        $ref->create(trim(str_replace($matches[0], "", $line)));
                                        $anchor = "rwPubRef~" . $ref->getElement('id');
                                        Publisso::log($anchor);
                                        $reference[$matches[1]] = '[<a name="' . $anchor . '">' . $matches[1] . '</a>]' . ' ' . $ref->getElement('reference');
                                        $refindexes[$matches[1]] = $anchor;
                                }
                        }
                        
                        Publisso::log(print_r($reference, 1));
                        Publisso::log(print_r($refindexes, 1));
                        
                        Publisso::log($regex);
                        
                        if (preg_match_all($regex, $text, $matches)) {
                                Publisso::log(print_r($matches, 1));
                                $strings = $matches[0];
                                $references = $matches[1];

                                foreach ($references as $index => $_) {

                                        $refstring = '';

                                        foreach (explode(',', $_) as $__) {
                                                $refstring .= (!empty($refstring) ? ', ' : '') . '<a href="#' . $refindexes[$__] . '">' . $__ . '</a>';
                                        }

                                        $text = str_replace($strings[$index], "[$refstring]", $text);
                                }
                        }
                        else{
                                Publisso::log('No matches!');
                        }


                        return $text;
                }
        
                /**
                 * @param string $paramText
                 * @param string $paramReferences
                 * @return string
                 */
                function getReferences(string $paramText, string $paramReferences)
                {
                        $paramReferences = preg_replace('/\r\n?/', "\n", $paramReferences);
                        $references = array_filter(explode("\n", $paramReferences));

                        $doc = new \DOMDocument();
                        $doc->loadHTML($paramText, LIBXML_NOERROR | LIBXML_NOWARNING);
                        $xpath = new \DOMXpath($doc);

                        $usedRefs = [];
                        $usedRefIDs = [];

                        foreach ($xpath->evaluate("//a[starts-with(@href, '#rwPubRef~')]") as $node) {

                                $refNr = $node->nodeValue;
                                $href = $node->getAttribute('href');
                                $refID = explode("~", $href)[1];

                                if (!in_array($href, $usedRefs)) {

                                        $objReference = new \Drupal\publisso_gold\Controller\Reference($refID);
                                        $objReference->setDOILinkHTML();

                                        $search = "[$refNr]";
                                        $replace = '<a id="' . $refID . '" name="' . substr($href, 1, strlen($href)) . '"></a>[' . $refNr . '] ';

                                        foreach ($references as $index => $reference) {
                                                if (substr($reference, 0, strlen($search)) == $search) {
                                                        $references[$index] = $replace . $objReference->getElement('reference');

                                                        if (!in_array($index, $usedRefIDs)) $usedRefIDs[] = $index;
                                                }
                                        }
                                }
                        }

                        foreach ($references as $index => $reference) {

                                if (!in_array($index, $usedRefIDs)) {

                                        $objReference = new \Drupal\publisso_gold\Controller\Reference();
                                        $objReference->tempReference(trim($reference));
                                        $objReference->setDOILinkHTML();
                                        $objReference->setLinkHTML();
                                        $references[$index] = $objReference->getElement('reference');
                                }
                        }

                        return implode("\n", $references);
                }
        
                /**
                 * @param $text
                 * @return array
                 */
                function getReferencesFromText($text)
                {

                        $references = [];

                        if (preg_match_all('/<a href="#rwPubRef~(\d+)">(.+?)<\/a>/im', $text, $matches)) {
                                //echo '<pre>'.print_r($matches, 1).'</pre>';
                                $links = $matches[0];
                                $refIds = $matches[1];
                                $refNrs = $matches[2];
                                $refNr = 1;
                                foreach ($links as $index => $link) {

                                        $ref = new \Drupal\publisso_gold\Controller\Reference($refIds[$index]);
                                        $ref->setDOILinkHTML();
                                        $ref->setLinkHTML();
                                        $anchor = "rwPubRef~" . $ref->getElement('id');

                                        if (!in_array('[<a name="' . $anchor . '">' . ($refNrs[$index]) . '</a>] ' . $ref->getElement('reference'), $references))
                                                $references[] = '[<a name="' . $anchor . '">' . ($refNrs[$index]) . '</a>] ' . $ref->getElement('reference');

                                        $text = str_replace($link, "<a href=\"#$anchor\">" . ($refNrs[$index]) . "</a>", $text);
                                        $refNr++;
                                }
                        }

                        usort($references, "self::sortReferences");
                        #echo '<pre>'.print_r($references, 1).'</pre>';exit();
                        return [$text, implode("\n", $references)];
                }
        
                /**
                 * @param $a
                 * @param $b
                 * @return bool|int
                 */
                private function sortReferences($a, $b)
                {

                        $a = preg_replace('/^(\[(\<a(.+?)\>)?)(\d+?)(\<\/a\>)?\](.*)$/', '$4', $a);
                        $b = preg_replace('/^(\[(\<a(.+?)\>)?)(\d+?)(\<\/a\>)?\](.*)$/', '$4', $b);
                        return $a == $b ? 0 : $a > $b;
                }
        
                /**
                 * @param $activeReferences
                 * @param $origReferences
                 * @return string
                 */
                function getInactiveReferences($activeReferences, $origReferences)
                {

                        if (empty($activeReferences)) $activeReferences = []; else $activeReferences = array_filter(explode("\n", $activeReferences));
                        if (empty($origReferences)) $origReferences = []; else $origReferences = array_filter(explode("\n", $origReferences));

                        for ($i = 0; $i < count($activeReferences); $i++) {

                                if (preg_match('/^\[.+?\](.+)/im', $activeReferences[$i], $matches)) {

                                        $activeReferences[$i] = trim($matches[1]);
                                }
                        }

                        $nr = count($activeReferences);

                        for ($i = 0; $i < count($origReferences); $i++) {

                                if (preg_match('/^\[.+?\](.+)/im', $origReferences[$i], $matches)) {

                                        $origReferences[$i] = trim($matches[1]);
                                }
                        }

                        $references = [];
                        #echo '<pre>'.print_r($origReferences, 1).'</pre>';
                        foreach ($origReferences as $reference) {

                                $ref = new \Drupal\publisso_gold\Controller\Reference();
                                $ref->create($reference);

                                $ref->setDOILinkHTML();
                                $ref->setAvailableFromLinkHTML();

                                /*
                                                        if(preg_match('/^Dellinger/', $ref->getElement('reference'))){
                                                                var_dump($ref->getElement('reference') == $activeReferences[1]);
                                                                echo '<pre>'.print_r(str_split(utf8_decode($ref->getElement('reference'))), 1).'</pre>';
                                                                echo "<pre>".print_r(str_split($activeReferences[33]), 1)."</pre>";
                                                        }
                                                        */
                                if (!in_array($ref->getElement('reference'), $activeReferences)) {

                                        if (mb_detect_encoding($ref->getElement('reference'), 'UTF-8, ISO-8859-1') === 'UTF-8') {
                                                $str = utf8_decode($ref->getElement('reference'));
                                        } else {
                                                $str = $ref->getElement('reference');
                                        }

                                        $nr++;
                                        $references[] = "[" . ($nr) . "] " . $str;
                                }
                        }


                        /*
                                          foreach(array_diff($origReferences, $activeReferences) as $reference){
                                                  $ref = new \Drupal\publisso_gold\Controller\Reference();
                                                  $ref->create($reference);
                                                  $ref->setDOILinkHTML();
                                                  $references[] = "[".(++$nr)."] ".$ref->getElement('reference');
                                          }
                                          */
                        return implode("\n", $references);
                }
        
                /**
                 * @return array
                 */
                function getModuleAccessPermissions()
                {

                        $modaccessweights = [];
                        foreach (\Drupal::database()->select('rwPubgoldModuleaccess', 't')->fields('t', [])->execute()->fetchAll() as $res) {
                                $modaccessweights[$res->mod_name] = $res->mod_minaccessweight;
                        }
                        return $modaccessweights;
                }
        
                /**
                 * @param $modname
                 * @param false $userweight
                 * @return bool
                 */
                function userHasAccessRight($modname, $userweight = false)
                {

                        $perms = $this->getModuleAccessPermissions();

                        if ($userweight === false) { //if no weight delivered, take current session
                                $s = \Drupal::service('session');
                                $u = $s->get('user');

                                $userweight = $u && $u['weight'] ? $u['weight'] : 0;
                        }

                        $userweight = (int)$userweight;

                        if (!array_key_exists($modname, $perms)) {
                                return false;
                        }

                        return $perms[$modname] <= $userweight;
                }
        
                /**
                 * @param string $msg
                 */
                function accessDenied($msg = 'You can not access this page. You may need to log in first.')
                {
                        throw new AccessDeniedHttpException($msg);
                }
        
                /**
                 * @param $uid
                 * @return mixed
                 */
                function isUserBookEditor($uid)
                {

                        return \Drupal::database()->select('rwPubgoldBookEditors', 't')
                                ->fields('t', [])
                                ->condition('be_uid', $uid, '=')
                                ->countQuery()
                                ->execute()
                                ->fetchField();
                }
        
                /**
                 * @param $uid
                 * @return mixed
                 */
                function isUserBookEiC($uid)
                {

                        return \Drupal::database()->select('rwPubgoldBookEditorsInChief', 't')
                                ->fields('t', [])
                                ->condition('eic_uid', $uid, '=')
                                ->countQuery()
                                ->execute()
                                ->fetchField();
                }
        
                /**
                 * @param $uid
                 * @return mixed
                 */
                function isUserBookEO($uid)
                {

                        return \Drupal::database()->select('rwPubgoldBookEditorialOffice', 't')
                                ->fields('t', [])
                                ->condition('beo_uid', $uid, '=')
                                ->countQuery()
                                ->execute()
                                ->fetchField();
                }
        
                /**
                 * @param $uid
                 * @return mixed
                 */
                function isUserJournalEditor($uid)
                {

                        return \Drupal::database()->select('rwPubgoldJournalEditors', 't')
                                ->fields('t', [])
                                ->condition('je_uid', $uid, '=')
                                ->countQuery()
                                ->execute()
                                ->fetchField();
                }
        
                /**
                 * @param $uid
                 * @return mixed
                 */
                function isUserJournalEiC($uid)
                {

                        return \Drupal::database()->select('rwPubgoldJournalsEditorsInChief', 't')
                                ->fields('t', [])
                                ->condition('jeic_uid', $uid, '=')
                                ->countQuery()
                                ->execute()
                                ->fetchField();
                }
        
                /**
                 * @param $uid
                 * @return mixed
                 */
                function isUserJournalEO($uid)
                {

                        return \Drupal::database()->select('rwPubgoldJournalsEditorialOffice', 't')
                                ->fields('t', [])
                                ->condition('jeo_uid', $uid, '=')
                                ->countQuery()
                                ->execute()
                                ->fetchField();
                }
        
                /**
                 * @param $uid
                 * @return int
                 */
                function isUserConferenceEditor($uid)
                {

                        return 0;

                        /*
                                          $uid = false;

                                          if(array_key_exists('user', $_SESSION)){
                                                  if(array_key_exists('id', $_SESSION['user'])){
                                                          $uid = $_SESSION['user']['id'];
                                                  }
                                          }

                                          if($uid === false) return 0;

                                          return \Drupal::database()->select('rwPubgoldBookEditors', 't')
                                                                                          ->fields('t', [])
                                                                                          ->condition('be_uid', $uid, '=')
                                                                                          ->countQuery()
                                                                                          ->execute()
                                                                                          ->fetchField();
                                          */
                }
        
                /**
                 * @param $uid
                 * @return mixed
                 */
                function isUserConferenceEiC($uid)
                {

                        return \Drupal::database()->select('rwPubgoldConferencesEditorsInChief', 't')
                                ->fields('t', [])
                                ->condition('ceic_uid', $uid, '=')
                                ->countQuery()
                                ->execute()
                                ->fetchField();
                }
        
                /**
                 * @param $uid
                 * @return mixed
                 */
                function isUserConferenceEO($uid)
                {

                        return \Drupal::database()->select('rwPubgoldConferencesEditorialOffice', 't')
                                ->fields('t', [])
                                ->condition('ceo_uid', $uid, '=')
                                ->countQuery()
                                ->execute()
                                ->fetchField();
                }
        
                /**
                 * @param $uid
                 * @param int $userweight
                 * @param int $role_id
                 * @param null $wf_id
                 * @return mixed
                 */
                function getMyTodo($uid, $userweight = 10, $role_id = 7, $wf_id = null)
                {

                        $ret = array();

                        //get workflows with direct association
                        $where = "";

                        if ($userweight < 70) { //not Publisso or Administrator
                                $where = "(
                                FIND_IN_SET('u:" . $uid . "', `wf_assigned_to`)
                                OR
                                FIND_IN_SET('r:" . $uid . "', `wf_assigned_to`)
                                OR
                                (FIND_IN_SET('" . $uid . "', `wf_assigned_to_eo`) AND `wf_state` NOT IN('ready for publication', 'running submission'))
                        )";
                        }

                        $sql = "
                                SELECT
                                wf_id
                                FROM
                                {rwPubgoldWorkflow}
                                WHERE
                                " . (!empty($where) ? $where . ' AND ' : '') . "
                                `wf_id` LIKE '" . ($wf_id ? $wf_id : '%') . "'
                                AND wf_state NOT IN('published', 'rejected', 'running submission')
                        ";

                        return \Drupal::database()->query(
                                $sql,
                                [
                                        ':uid' => 'u:' . $uid,
                                        ':rid' => 'r:' . $role_id,
                                        ':wf_id' => $wf_id ? $wf_id : "'%'"
                                ]
                        )->fetchCol();
                }
        
                /**
                 * @param $uid
                 * @return mixed
                 */
                function getMyEiCSubmissions($uid)
                {

                        $sql = "
                                SELECT
                                        `wf_id`
                                FROM
                                        {rwPubgoldWorkflow}
                                WHERE
                                        FIND_IN_SET(:uid, `wf_assigned_to_eic`) AND
                                        `wf_state` NOT IN('published', 'rejected')
                        ";

                        return \Drupal::database()->query($sql, [':uid' => $uid])->fetchCol();
                }
        
                /**
                 * @param $uid
                 * @return mixed
                 */
                function getMyEditorSubmissions($uid)
                {

                        $sql = "
                                SELECT
                                        `wf_id`
                                FROM
                                        {rwPubgoldWorkflow}
                                WHERE
                                        FIND_IN_SET(:uid, `wf_assigned_to_editor`)AND
                                        `wf_state` NOT IN('published', 'rejected')
				ORDER BY `wf_id` ASC
                        ";

                        return \Drupal::database()->query($sql, [':uid' => $uid])->fetchCol();
                }
        
                /**
                 * @param $uid
                 * @return mixed
                 */
                public function getMyReviewerSubmissions($uid)
                {

                        $sql = "
                                SELECT
                                        `wf_id`
                                FROM
                                        {rwPubgoldWorkflow}
                                WHERE
                                        FIND_IN_SET(:uid, `wf_assigned_to_reviewer`)AND
                                        `wf_state` NOT IN('published', 'rejected')
                        ";

                        return \Drupal::database()->query($sql, [':uid' => $uid])->fetchCol();
                }
        
                /**
                 * @param $uid
                 * @return array
                 */
                function getMyRunningSubmissions($uid)
                {

                        $submission = [];
                        foreach (\Drupal::database()->select('rwPubgoldBookchapter_temp', 't')
                                         ->fields('t', ['id'])
                                         ->condition('uid', $uid, '=')
                                         ->execute()
                                         ->fetchCol() as $id) {

                                $submission[] = [
                                        'type' => null,
                                        'id' => $id
                                ];
                        }

                        foreach (\Drupal::database()->select('rwPubgoldWorkflowTemp', 't')
                                         ->fields('t', ['id', 'type'])
                                         ->condition('uid', $uid, '=')
                                         ->execute()
                                         ->fetchAll() as $_) {

                                $submission[] = [
                                        'type' => $_->type,
                                        'id' => $_->id
                                ];
                        }

                        return $submission;
                }
        
                /**
                 * @param $uid
                 * @return mixed
                 */
                public function getMyWorkflowItems($uid)
                {
                        return \Drupal::database()->select('rwPubgoldWorkflow', 'wf')
                                ->fields('wf', ['wf_id'])
                                ->condition('wf_created_by_uid', $uid, '=')
                                ->execute()
                                ->fetchCol();
                }
        
                /**
                 * @param int $role_id
                 * @return array
                 */
                function getPublishedBookChapters_Workflow($role_id = 7)
                {

                        if (empty($role_id)) $role_id = 7;

                        if ($role_id > 2) return []; //Only PUBLISSO and Administrators

                        $qry = \Drupal::database()->select('rwPubgoldBookChapters', 'c');
                        $qry->join('rwPubgoldWorkflow', 'w', 'c.cp_wfid = w.wf_id');
                        $qry->addField('w', 'wf_id');
                        $qry->condition('w.wf_state', 'published');
                        $qry->isNull('c.cp_next_version');
                        return $qry->execute()->fetchCol();
                        return \Drupal::database()->select('rwPubgoldBookChapters', 't')->fields('t', ['cp_wfid'])
                                ->isNull('cp_next_version')->execute()->fetchCol();
                }
        
                /**
                 * @param $cp_wfid
                 * @return mixed
                 */
                function getBookchapterIDFromWorkflow($cp_wfid)
                {
                        return \Drupal::database()->select('rwPubgoldBookChapters', 't')->fields('t', ['cp_id'])
                                ->condition('cp_wfid', $cp_wfid, '=')->execute()->fetchField();
                }
        
                /**
                 * @param $uid
                 * @param $wf_id
                 * @return bool
                 */
                public function isUserReviewing($uid, $wf_id)
                {

                        $ret = \Drupal::database()->select('rwPubgoldWorkflowCache', 't')->fields('t', [])
                                ->condition('wc_uid', $uid, '=')
                                ->condition('wc_wfid', $wf_id, '=')
                                ->countQuery()
                                ->execute()
                                ->fetchField();

                        $ret1 = \Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', [])
                                ->condition('wfid', $wf_id, '=')
                                ->condition('uid', $uid, '=')
                                ->condition('state', 'progress', '=')
                                ->countQuery()
                                ->execute()
                                ->fetchField();

                        return !!$ret || !!$ret1;
                }
        
                /**
                 * @param $uid
                 * @param null $password
                 */
                public function exportUser($uid, $password = null)
                {

                        $session = \Drupal::service('session');
                        $user = $session->get('user');

                        if (strtolower(\Drupal::service('publisso_gold.setup')->getValue('system.sso.active')) != 'yes') {
                                return;
                        }

                        $user_local = new \Drupal\publisso_gold\Controller\User($uid);

                        if (!$user_local->getElement('initial_email'))
                                $user_local->setElement('initial_email', $user_local->profile->email);

                        $data = [
                                'institut' => $user_local->profile->getElement('institute'),
                                'LKURZ' => preg_match('/^\d+$/', $user_local->profile->getElement('country')) ? array_search(getCountry($user_local->profile->getElement('country')), getCountrylist()) : $user_local->profile->getElement('country'),
                                'LORT' => $user_local->profile->getElement('city'),
                                'name' => implode(', ', [$user_local->profile->getElement('lastname'), $user_local->profile->getElement('firstname')]),
                                'LSTRAS' => $user_local->profile->getElement('street'),
                                'LPZAHL' => $user_local->profile->getElement('postal_code'),
                                'LTELNR' => $user_local->profile->getElement('telephone'),
                                'LSPRACHE' => $user_local->profile->getElement('correspondence_language'),
                                'LLAND' => getCountry($user_local->profile->getElement('country')),
                                'PRGRP' => 'z2',
                                'LEMAILADR' => $user_local->profile->getElement('email')
                        ];

                        //Dummyeinträge, bis Schnittstelle angepasst
                        if (empty($data['LKURZ'])) $data['LKURZ'] = 'DE';
                        if (empty($data['LLAND'])) $data['LLAND'] = 'Deutschland';
                        if (empty($data['LORT'])) $data['LORT'] = 'Stadt';

                        foreach ($data as $k => $v) {
                                if ( !$v ) unset( $data[ $k ] );
                        }
                        
                        $sso = \Drupal\publisso_sso\Classes\SSO::registerUser($user_local->getElement('initial_email') ? $user_local->getElement('initial_email') : $user_local->getElement('user'), $password ? $password : $user['password'], $data);
                        
                        if ($sso->getLastCode() == 200) {

                                preg_match('/K\d+$/', $sso->getLastMessage(), $matches);
                                $kennung_zbmed = $matches[0];
                                $user_local->setElement('zbmed_kennung', $kennung_zbmed);
                                $user_local->setElement('initial_email', $user_local->getElement('user'));
                                Publisso::log('Mapping local user ' . $user_local->getElement('id') . ' to ZBMED ' . $kennung_zbmed);
                        }
                        else{
                                Publisso::log('Cant register user: '.print_r($sso, 1));
                        }
                }
        
                /**
                 * @param $password
                 * @return object
                 */
                public function validatePasswordStrength($password)
                {

                        if (strlen($password) < 8) {
                                return (object)['code' => 300, 'msg' => 'The password must have at least 8 characters!'];
                        }

                        if (!preg_match('/[a-zA-Z]/', $password)) {
                                return (object)['code' => 301, 'msg' => 'The entered password must contain letters!'];
                        }

                        if (!preg_match('/[0-9]/', $password)) {
                                return (object)['code' => 302, 'msg' => 'The entered password must conain numbers!'];
                        }

                        if (!preg_match('/[,.;:-_!?&%$=#+*~]/', $password)) {
                                return (object)['code' => 303, 'msg' => 'The entered password must conain not-alphanumeric characters [,.;:-_!?&%$=#+*~]!'];
                        }

                        return (object)['code' => 200, 'msg' => 'OK'];
                }
        
                /**
                 * @return mixed
                 */
                public function getUnpublishedWorkflowIDs()
                {

                        $sql = 'SELECT `wf_id` FROM {rwPubgoldWorkflow} WHERE FROM_BASE64(`wf_data`) NOT LIKE \'%"state":"published"%\'';
                        $result = \Drupal::database()->query($sql)->fetchCol();
                        return $result;
                }
        
                /**
                 * @param Workflow $workflow
                 * @return Book|Conference|Journal
                 */
                public function getWorkflowMedium(\Drupal\publisso_gold\Controller\Workflow &$workflow)
                {

                        switch (substr($workflow->getElement('type'), 0, 1)) {

                                case 'b':
                                        return new \Drupal\publisso_gold\Controller\Book($workflow->getElement('id'));
                                        break;

                                case 'j':
                                        return new \Drupal\publisso_gold\Controller\Journal($workflow->getElement('id'));
                                        break;

                                case 'c':
                                        return new \Drupal\publisso_gold\Controller\Conference($workflow->getElement('id'));
                                        break;
                        }
                }
        
                /**
                 * @param $route_name
                 * @param array $route_param
                 * @param string $fallback_route
                 * @param array $fallback_route_param
                 * @return Response
                 */
                public function redirect($route_name, $route_param = [], $fallback_route = 'publisso_gold.content', $fallback_route_param = [])
                {

                        try {
                                $url = Url::fromRoute($route_name, $route_param);
                        } catch (\Symfony\Component\Routing\Exception\ExceptionInterface $e) {

                                $url = Url::fromRoute($fallback_route, $fallback_route_param);
                                \Drupal::service('messenger')->addError($e->getMessage());
                        } finally {
                                $response = new Response();
                                $response->isRedirection();
                                $response->setStatusCode(301);
                                $response->headers->set('LOCATION', $url->toString());
                                return $response;
                        }
                }
        
                /**
                 * @param $name
                 * @return mixed
                 */
                public function getMailtemplate($name)
                {

                        return \Drupal::database()->select('rwPubgoldMailtemplates', 't')->fields('t', [])
                                ->condition('name', $name, '=')->execute()->fetchAll()[0];
                }
        
                /**
                 * @param $workflow
                 * @param $process
                 * @param array $vars
                 * @param bool $autosend
                 * @param null $uid
                 * @return false
                 * @throws \Exception
                 */
                public function sendWorkflowInfoMail(&$workflow, $process, $vars = [], $autosend = true, $uid = null)
                {

                        if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];

                        switch ($workflow->getDataElement('type')) {
                                case 'bookchapter':
                                        $medium = new \Drupal\publisso_gold\Controller\Book($workflow->getDataElement('bk_id'));
                                        break;

                                case 'journalarticle':
                                        $medium = new \Drupal\publisso_gold\Controller\Journal($workflow->getDataElement('jrn_id'));
                                        break;

                                case 'conferencepaper':
                                        $medium = new \Drupal\publisso_gold\Controller\Conference($workflow->getDataElement('cf_id'));
                                        break;

                                default:
                                        return false;
                        }

                        $mailout = $medium->getMailout($process);

                        if ($mailout === false) {
                                error_log('No mailtemplate for process "' . $process . '" found!');
                                return false;
                        }

                        $user = new \Drupal\publisso_gold\Controller\User($uid);
                        $url_login = \Drupal\Core\Url::fromRoute('publisso_gold.login')->setAbsolute()->toString();

                        $template_text = ['subject', 'template'];

                        foreach ($template_text as $template) {

                                $sysvars = [
                                        '::submitted_by::' => $user->profile->getReadableName('FL', ' '),
                                        '::submission_type::' => (string)t($workflow->getDataElement('type')),
                                        '::submission_type::' => (string)t(ucfirst($workflow->getDataElement('type'))),
                                        '::submission_title::' => $workflow->getDataElement('title'),
                                        '::parent_title::' => $medium->getElement('title'),
                                        '::parent_signature::' => $medium->getElement('mail_signature'),
                                        '::link.login::' => $url_login,
                                        '::mail_signature_publisso::' => \Drupal::service('publisso_gold.setup')->getValue('mail_signature_publisso')
                                ];

                                $mailout[$template] = str_replace(array_keys($sysvars), array_values($sysvars), $mailout[$template]);
                                $mailout[$template] = str_replace(array_keys($vars), array_values($vars), $mailout[$template]);

                                foreach ($medium->getControlKeys() as $key) {
                                        $mailout[$template] = str_replace("::parent.control.$key::", $medium->getControlElement($key), $mailout[$template]);
                                }

                                foreach ($medium->getDataKeys() as $key) {
                                        $mailout[$template] = str_replace("::parent.data.$key::", $medium->getElement($key), $mailout[$template]);
                                }

                                switch (substr($workflow->getElement('type'), 0, 1)) {
                                        case 'b':
                                                $mailout[$template] = str_replace('::media_type::', t('Book'), $mailout[$template]);
                                                break;

                                        case 'j':
                                                $mailout[$template] = str_replace('::media_type::', t('Journal'), $mailout[$template]);
                                                break;

                                        case 'c':
                                                $mailout[$template] = str_replace('::media_type::', t('Conference'), $mailout[$template]);
                                                break;
                                }
                        }

                        $mailout['subject'] = (string)t($mailout['subject']);

                        $recipient = $cc = $bcc = [];

                        error_log("Workflow #" . $workflow->getElement('id'));
                        error_log(print_r($mailout, 1));

                        foreach (['recipient', 'cc', 'bcc'] as $mailTo) {

                                foreach (explode(',', $mailout[$mailTo]) as $_) {

                                        switch (substr($_, 0, 2)) {

                                                case 'g:': //groups
                                                        $group = explode(':', $_);
                                                        $group = $group[1];

                                                        switch ($group) {

                                                                case 'editorial_office':
                                                                        $users = $medium->readEditorialOffice();
                                                                        break;

                                                                case 'editors_in_chief':
                                                                        foreach (explode(',', $workflow->getElement('assigned_to_eic')) as $uid) {
                                                                                $users[] = new \Drupal\publisso_gold\Controller\User($uid);
                                                                        }
                                                                        $users = array_merge($users, $medium->readEditorsInChief());
                                                                        break;

                                                                case 'reviewers':
                                                                        foreach (explode(',', $workflow->getElement('assigned_to_reviewer')) as $uid) {
                                                                                $users[] = new \Drupal\publisso_gold\Controller\User($uid);
                                                                        }
                                                                        break;

                                                                case 'editors':
                                                                        foreach (explode(',', $workflow->getElement('assigned_to_editor')) as $uid) {
                                                                                $users[] = new \Drupal\publisso_gold\Controller\User($uid);
                                                                        }
                                                                        break;
                                                        }

                                                        foreach ($users as $_user) {

                                                                if (!in_array($_user->profile->getElement('email'), $$mailTo))
                                                                        array_push($$mailTo, $_user->profile->getElement('email'));
                                                        }

                                                        break;

                                                case 'u:': //user
                                                        $_user = explode(':', $_);

                                                        if ($_user[1] == 'submitting_author')
                                                                $_user[1] = $workflow->getElement('created_by_uid');

                                                        $_user = new \Drupal\publisso_gold\Controller\User($_user[1]);

                                                        if (!in_array($_user->profile->getElement('email'), $$mailTo))
                                                                array_push($$mailTo, $_user->profile->getElement('email'));
                                                        break;

                                                case 'r:': //users by role
                                                        $role = explode(':', $_);
                                                        $role = $role[1];
                                                        $users = getUsersByRole(\Drupal::database(), [$role]);

                                                        foreach ($users as $uid => $_user) {

                                                                $_user = new \Drupal\publisso_gold\Controller\User($uid);

                                                                if (!in_array($_user->profile->getElement('email'), $$mailTo))
                                                                        array_push($$mailTo, $_user->profile->getElement('email'));
                                                        }
                                                        break;

                                                default:
                                                        foreach (array_filter(preg_split('/[;, ]/', $_)) as $___)
                                                                array_push($$mailTo, $___);
                                        }
                                }
                        }

                        error_log('Count recipients: ' . count($recipient) . ' -- ' . implode(', ', $recipient));
                        error_log('Fallback-recipient: ' . \Drupal::service('publisso_gold.setup')->getValue('system.email.fallback.recipient'));

                        if (!count($recipient) && \Drupal::service('publisso_gold.setup')->getValue('system.email.fallback.recipient')) {

                                error_log('No emailrecipient found! Using fallback!');
                                //set fallback emailrecipient if none found/set
                                $recipient[] = \Drupal::service('publisso_gold.setup')->getValue('system.email.fallback.recipient');

                                //remove fallback emailrecipient from (b)cc-lists
                                if (array_search($recipient[0], $cc)) array_splice($cc, array_search($recipient[0], $cc), 1);
                                if (array_search($recipient[0], $bcc)) array_splice($bcc, array_search($recipient[0], $bcc), 1);

                                $mailout['template'] .= "\n\n######################################################\n" . ((string)t('You receive this email because no other recipient could be detected') . "\n######################################################");
                        }

                        foreach ($recipient as $_) {

                                //set recipient-depending variables

                                $_mail = $mailout;

                                if (\Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->condition('up_email', $_, '=')->countQuery()->execute()->fetchField()) {
                                        $_user = new \Drupal\publisso_gold\Controller\User(\Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', ['up_uid'])->condition('up_email', $_, '=')->execute()->fetchField());
                                }

                                foreach ($_user->getElementKeys() as $key) {
                                        $_mail['template'] = str_replace("::recipient.$key::", $_user->getElement($key), $_mail['template']);
                                }

                                foreach ($_user->profile->getElementKeys() as $key) {
                                        $_mail['template'] = str_replace("::recipient.profile.$key::", $_user->profile->getElement($key), $_mail['template']);
                                }

                                $sender = new \Drupal\publisso_gold\Controller\User(\Drupal::service('session')->get('user')['id']);

                                foreach ($sender->getElementKeys() as $key) {
                                        $_mail['template'] = str_replace("::sender.$key::", $sender->getElement($key), $_mail['template']);
                                }

                                foreach ($sender->profile->getElementKeys() as $key) {
                                        $_mail['template'] = str_replace("::sender.profile.$key::", $sender->profile->getElement($key), $_mail['template']);
                                }

                                if ($autosend == true)
                                        $this->sendMail($_, $_mail['subject'], $_mail['template'], ['CC' => implode(',', $cc), 'BCC' => implode(',', $bcc)], $_mail['attachment']);
                                else
                                        $spooler_id = $this->saveMail2Spooler($_, $_mail, ['CC' => implode(',', $cc), 'BCC' => implode(',', $bcc)], json_encode($_mail['attachment']));

                                //save Mail in getWorkflowHistory
                                $data = [
                                        'wf_id' => $workflow->getElement('id'),
                                        'wf_type' => $workflow->getElement('type'),
                                        'wf_state' => $workflow->getDataElement('state'),
                                        'wf_data' => $workflow->getElement('data'),
                                        'mail_from' => 'PUBLISSO <livingbooks@zbmed.de>',
                                        'mail_to' => $_,
                                        'mail_cc' => serialize($cc),
                                        'mail_bcc' => serialize($bcc),
                                        'mail_text' => $_mail['template'],
                                        'mail_attachment' => serialize($_mail['attachment']),
                                        'mail_autosend' => $autosend ? 1 : 0,
                                        'init_uid' => $user->getElement('id')
                                ];

                                $insert = \Drupal::database()->insert('rwPubgoldWorkflowMailHistory');

                                if ($spooler_id) $data['spooler_id'] = $spooler_id;
                                else $data['mail_sent'] = date('Y-m-d H:i:s');

                                $insert->fields($data);
                                $insert->execute();
                        }
                }
        
                /**
                 * @param $to
                 * @param $subject
                 * @param $body
                 * @param array $add_header
                 * @param array $attachments
                 */
                public function sendMail($to, $subject, $body, $add_header = [], $attachments = [])
                {

                        $uuid = md5((int)uniqid() * time());
                        $mail_system = \Drupal::service('plugin.manager.mail')->getInstance(array('module' => 'publisso_gold', 'key' => $uuid));

                        $ctype = "text/plain; charset=UTF-8;";
                        $boundary = md5(uniqid(mt_rand(), 1));

                        if (count($attachments)) {

                                $eol = PHP_EOL;
                                $ctype = "multipart/mixed; boundary=\"$boundary\"";

                                $body = ""
                                        . "--$boundary" . $eol
                                        . "Content-Type: text/plain; charset=UTF-8;$eol"
                                        . "Content-Transfer-Encoding: 8BIT$eol$eol"
                                        . $body . $eol;

                                foreach ($attachments as $file) {

                                        $path = $_SERVER['DOCUMENT_ROOT'] . $file;

                                        if (!is_readable($path)) {
                                                error_log("File \"$path\" is not readable!");
                                                continue;
                                        }

                                        $type = mime_content_type($path);
                                        $name = basename($path);
                                        $cont = chunk_split(base64_encode(file_get_contents($path)));
                                        $size = filesize($path);

                                        $body .= "--$boundary$eol";
                                        $body .= "Content-Type: $type; name=\"$name\"$eol";
                                        $body .= "Content-Disposition: attachment; filename=\"$name\";$eol";
                                        $body .= "Content-Length: $size$eol";
                                        $body .= "Content-Transfer-Encoding: base64$eol$eol";
                                        $body .= $cont;
                                }

                                $body .= "$eol--$boundary--";
                        }


                        $params = [
                                'headers' => [
                                        'MIME-Version' => '1.0',
                                        'Content-Type' => $ctype,
                                        'Content-Transfer-Encoding' => '8BIT',
                                        'X-Mailer' => 'Publisso Mailer',
                                        'From' => 'PUBLISSO <livingbooks@zbmed.de>'
                                ],
                                'to' => $to,
                                'body' => ($body),
                                'subject' => $subject
                        ];

                        if (!empty($add_header['cc']))
                                $params['cc'] = $add_header['cc'];

                        if (!empty($add_header['bcc']))
                                $params['bcc'] = $add_header['bcc'];

                        foreach ($add_header as $_k => $_v) {
                                $params['headers'][$_k] = $_v;
                        }

                        $mail_system->mail($params);
                }
        
                /**
                 * @param $recipient
                 * @param array $mailout
                 * @param array $header
                 * @param $attachment
                 * @param null $autosend_parent
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 * @throws \Exception
                 */
                public function saveMail2Spooler($recipient, $mailout = [], $header = [], $attachment, $autosend_parent = null)
                {

                        return \Drupal::database()->insert('rwPubgoldMailout')->fields([
                                'recipient' => $recipient,
                                'subject' => $mailout['subject'],
                                'body' => $mailout['template'],
                                'header' => json_encode($header),
                                'ready_to_send' => 0,
                                'created_by_uid' => \Drupal::service('session')->get('user')['id'],
                                'attachment' => $attachment,
                                'autosend_parent' => $autosend_parent
                        ])->execute();
                }
        
                /**
                 * @param $rid
                 * @return |null
                 */
                public function getRoleByRoleID($rid)
                {

                        return \Drupal::database()->select('rwPubgoldRoles', 't')->fields('t', ['name'])->condition('id', $rid, '=')->execute()->fetchField() ?? null;
                }
        
                /**
                 * @param null $role
                 * @return array
                 */
                public function getUserIDsByRole($role = null)
                {

                        $users = [];

                        if (!preg_match('/^\d+$/', $role)) {

                                $role = \Drupal::database()->select('rwPubgoldRoles', 't')
                                        ->fields('t', ['id'])
                                        ->condition('name', $role, '=')
                                        ->execute()
                                        ->fetchField();

                        }

                        if (preg_match('/^\d+$/', $role)) {

                                $users = \Drupal::database()->select('rwPubgoldUserroles', 't')
                                        ->fields('t', ['user_id'])
                                        ->condition('role_id', $role, '=')
                                        ->execute()
                                        ->fetchCol();

                        }

                        //get all users without role - they will be default users (author)
                        //only if this role is requested
                        //compare role with setup value default.user.role.id

                        if ($role == \Drupal::service('publisso_gold.setup')->getValue('default.user.role.id')) {

                                //get all userids with assigned role
                                $uids = \Drupal::database()->select('rwPubgoldUserroles', 't')
                                        ->fields('t', ['user_id'])
                                        ->condition('role_id', $role, '=')
                                        ->execute()
                                        ->fetchCol();
                                $uids = implode(',', array_filter($uids));

                                $res = \Drupal::database()->select('rwPubgoldUsers', 't')
                                        ->fields('t', ['id'])
                                        ->condition('id', $uids, 'NOT IN');

                                foreach ($res->execute()->fetchCol() as $uid) {
                                        $users[] = $uid;
                                }
                        }

                        $users = array_values(array_unique($users, SORT_NUMERIC));
                        return $users;
                }
        
                /**
                 * @return null
                 */
                public function forceUserAction()
                {

                        $noRedirect = [
                                'publisso_gold.userprofile.delete'
                        ];

                        $session = \Drupal::service('session');
                        $currentUrl = \Drupal::routeMatch()->getRouteName();
                        $currentParams = \Drupal::routeMatch()->getRawParameters()->all();

                        if (in_array($currentUrl, $noRedirect)) return null;

                        if ($session->get('logged_in') === true) {

                                $user = new \Drupal\publisso_gold\Controller\User($session->get('user')['id']);
                                $ret = null;

                                //Datenschutzerklärung
                                if (($ret == null && $user->profile->agreedLatestPrivacyProtection() === false) && $this->systemHasPrivacyProtectInformation() && !$session->has('orig_user')) {
                                        $ret = ['route' => 'publisso_gold.privacy_protection', 'params' => ['action' => 'agree']];
                                }

                                //unsent mails in spooler?
                                if ($ret == null) {
                                        if (\Drupal::database()->select('rwPubgoldMailout', 't')->fields('t', [])->condition('ready_to_send', 0, '=')->condition('created_by_uid', \Drupal::service('session')->get('user')['id'])->countQuery()->execute()->fetchField()) {

                                                $ret = [
                                                        'route' => 'publisso_gold.mailspool',
                                                        'params' => []
                                                ];
                                        }
                                }

                                //if password change required and called path is not password-change-form -> redirect
                                if ($ret == null) {

                                        $unprivUser = $session->get('user')['weight'] < 70;

                                        if ($session->has('orig_user')) {
                                                $orig_user = $session->get('orig_user');
                                                $unprivUser = !!($unprivUser & $orig_user['weight'] < 70);
                                        }

                                        if ($session->has('user') && $session->get('user')['pw_change'] == 1 && $unprivUser) {

                                                $ret = [
                                                        'route' => 'publisso_gold.change_password',
                                                        'params' => []
                                                ];

                                        }
                                }

                                //has user to update profile
                                if ($ret == null) {

                                        if (\Drupal::service('session')->has('user') && \Drupal::service('session')->get('user')['id']) {
                                                $this->currentUser = new User(\Drupal::service('session')->get('user')['id']);
                                                if ($this->currentUser->profile->getElement('edited') == -1) {

                                                        $ret = [
                                                                'route' => 'publisso_gold.userprofile_edit',
                                                                'params' => []
                                                        ];
                                                }
                                        }
                                }

                                //verhindere Endlosschleife
                                if ($ret != null && $ret['route'] != $currentUrl) {

                                        $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
                                        $tempstore->set('back_url', ['url' => $currentUrl, 'params' => $currentParams]);

                                        $url = Url::fromRoute($ret['route'], $ret['params']);
                                        $url->setAbsolute();

                                        $response = new RedirectResponse($url->toString(), 302);
                                        $response->send();
                                }
                        }

                        return null;
                }
        
                /**
                 * @return bool
                 */
                public function systemHasPrivacyProtectInformation()
                {

                        return !!\Drupal::database()->select('rwPubgoldPrivacyProtection', 't')
                                ->fields('t', [])->countQuery()->execute()->fetchField();
                }
        
                /**
                 * @param null $uid
                 * @return string|null
                 */
                public function getUserHomeDir($uid = null)
                {

                        if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];

                        if ($uid) {
                                return \Drupal::service('module_handler')->getModule('publisso_gold')->getPath() . '/var/uhd/' . $uid;
                        }

                        return null;
                }
        
                /**
                 * @return string
                 */
                public function getPDFAGenDir()
                {
                        return \Drupal::service('module_handler')->getModule('publisso_gold')->getPath() . '/inc/pdfa/';
                }
        
                /**
                 * @param null $uid
                 * @return string
                 */
                public function getVarDirAbsolute($uid = null)
                {
                        return DRUPAL_ROOT . '/' . \Drupal::service('module_handler')->getModule('publisso_gold')->getPath() . '/var';
                }
        
                /**
                 * @param null $aoe_id
                 * @return array|mixed
                 */
                public function getAreasOfExpertise($aoe_id = NULL)
                {

                        $aoe = \Drupal::database()->select('rwPubgoldAreasOfExpertise', 'aoe')
                                ->fields('aoe', [])
                                ->execute()
                                ->fetchAll();

                        $ret = array();

                        foreach ($aoe as $_) {
                                $ret[$_->aoe_id] = $_->aoe_name;
                        }

                        if ($aoe_id === NULL)
                                return $ret;
                        else
                                return $ret[$aoe_id];
                }

                public function destroyUserSession()
                {

                        \Drupal::service('session')->remove('logged_in');
                        \Drupal::service('session')->remove('user');
                        \Drupal::service('session')->remove('orig_user');
                        \Drupal::service('session')->clear();
                }
        
                /**
                 * @param $fid
                 * @return array|false
                 */
                public function processUploadedUserAvatar($fid)
                {

                        $file = File::load($fid);
                        $type = $file->getMimeType();
                        $uri = preg_replace('/^temporary:\//', '/tmp', $file->getFileUri());

                        if (false !== ($size = getimagesize($uri))) {

                                $type = $size[2];
                                $w = $size[0];
                                $h = $size[1];
                                $max = 400;

                                if ($w > $max || $h > $max) {

                                        if ($w >= $h) {

                                                $h = $h / ($w / $max);
                                                $w = $max;
                                        } else {

                                                $w = $w / ($h / $max);
                                                $h = $max;
                                        }

                                        $img = null;
                                        if ($type == IMG_JPG || $type == IMG_JPEG) {
                                                $img = imagecreatefromjpeg($uri);
                                        } elseif ($type == IMG_PNG) {
                                                $img = imagecreatefrompng($uri);
                                        } elseif ($type == IMG_GIF) {
                                                $img = imagecreatefromgif($uri);
                                        }

                                        if (!is_resource($img)) {
                                                \Drupal::service('messenger')->addError('Can\'t process uploaded image - possible broken file');
                                                return false;
                                        } else {
                                                $newimg = imagecreatetruecolor($w, $h);
                                                imagecopyresized($newimg, $img, 0, 0, 0, 0, $w, $h, $size[0], $size[1]);
                                        }

                                        if ($type == IMG_JPG || $type == IMG_JPEG) {
                                                $img = imagejpeg($newimg, $uri);
                                        } elseif ($type == IMG_PNG) {
                                                $img = imagepng($newimg, $uri);
                                        } elseif ($type == IMG_GIF) {
                                                $img = imagegif($newimg, $uri);
                                        }

                                        if ($img == false) {
                                                \Drupal::service('messenger')->addError('Can\'t process uploaded image - possible broken file');
                                                return false;
                                        }
                                }

                                return [$file->getMimeType(), file_get_contents($uri)];
                        } else {
                                \Drupal::service('messenger')->addError('Can\'t process uploaded image - possible broken file');
                                return false;
                        }
                }
        
                /**
                 * @param $string
                 * @return bool
                 */
                public function isJSON($string)
                {

                        json_decode($string);
                        return json_last_error() == JSON_ERROR_NONE;
                }
        
                /**
                 * @param string|null $string
                 * @return bool
                 */
                public function isSerialized(string $string = null){
                        $string = @unserialize($string);
                        return ($string !== false || $string === 'b:0;' || $string === 'N;');
                }
        
                /**
                 * @param $msg
                 */
                public function logMsg($msg)
                {

                        $logPath = $this->getVarDirAbsolute() . '/log';
                        if (!is_dir($logPath)) @mkdir($logPath, 0755, true);

                        $lastLogsStanding = [
                                date('Y-m-d', time()) . '.log',
                                date('Y-m-d', time() - 86400) . '.log',
                                date('Y-m-d', time() - 86400 * 2) . '.log',
                                date('Y-m-d', time() - 86400 * 3) . '.log',
                                date('Y-m-d', time() - 86400 * 4) . '.log',
                        ];

                        foreach (glob($logPath . '/*.log') as $file) {
                                if (!in_array(basename($file), $lastLogsStanding)) unlink($file);
                        }

                        $logFile = $logPath . '/' . date('Y-m-d', time()) . '.log';

                        if ($fh = fopen($logFile, 'a+')) {
                                fwrite($fh, '==========' . "\n" . date('H:i:s') . "\n" . $msg . "\n==========\n\n");
                                fclose($fh);
                        }
                }
        
                /**
                 * @param string $password
                 * @return mixed
                 */
                public function getDBEncPassword(string $password)
                {
                        return \Drupal::database()->query('SELECT PASSWORD(:password)', [':password' => $password])->fetchField();
                }
        
                /**
                 * @return string
                 */
                public function uniqid()
                {
                        return base64_encode(uniqid(md5(time()), true) . range('A', 'z')[array_rand(range('A', 'z'))] . range('A', 'z')[array_rand(range('A', 'z'))]);
                }
        
                /**
                 * @param null $uid
                 * @return bool
                 */
                public function systemHasHigherUsers($uid = null)
                {

                        if ($uid === null) $uid = \Drupal::service('session')->get('user')['id'];
                        $user = new User($uid);

                        $weight = $user->role['weight'];

                        $qry = \Drupal::database()->select('rwPubgoldUsers', 'u');
                        $qry->leftJoin('rwPubgoldUserroles', 'ur', 'u.id = ur.user_id');
                        $qry->leftJoin('rwPubgoldRoles', 'r', 'ur.role_id = r.id');
                        $qry->fields('u', []);
                        $qry->fields('r', []);
                        $qry->fields('ur', []);
                        $qry->condition('weight', $weight, '>');
                        return !!$qry->countQuery()->execute()->fetchField();
                }
        
                /**
                 * @param null $uid
                 * @return bool
                 */
                public function systemHasEqualUsers($uid = null)
                {

                        if ($uid === null) $uid = \Drupal::service('session')->get('user')['id'];
                        $user = new User($uid);

                        $weight = $user->role['weight'];

                        $qry = \Drupal::database()->select('rwPubgoldUsers', 'u');
                        $qry->leftJoin('rwPubgoldUserroles', 'ur', 'u.id = ur.user_id');
                        $qry->leftJoin('rwPubgoldRoles', 'r', 'ur.role_id = r.id');
                        $qry->fields('u', []);
                        $qry->fields('r', []);
                        $qry->fields('ur', []);
                        $qry->condition('weight', $weight, '=');
                        return !!$qry->countQuery()->execute()->fetchField();
                }
        
                /**
                 * @param string $string
                 * @return string
                 */
                public function translateToDefault(string $string)
                {

                        $lm = \Drupal::service('language_manager');
                        $currlang = $lm->getCurrentLanguage()->getId();
                        $deflang = $lm->getDefaultLanguage()->getId();

                        if ($deflang == $currlang) return $string;

                        $qry = \Drupal::database()->select('locales_source', 's');
                        $qry->fields('s', ['source']);
                        $qry->leftJoin('locales_target', 't', 's.lid = t.lid');
                        $qry->condition('t.translation', $string, '=');
                        $qry->condition('t.language', $currlang, '=');
                        $result = $qry->execute()->fetchField();

                        return $result ? $result : $string;
                }
        
                /**
                 * @param $chapter
                 * @param $book
                 * @return array
                 */
                public function getChapterDCMeta($chapter, $book)
                {

                        $ret = [];

                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Format', 'content' => 'text/html']], 'DC.Format'];
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Type', 'content' => 'Text']], 'DC.Type'];
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Doc-type', 'content' => 'bookPart']], 'DC.Doc-type'];
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Status-type', 'content' => empty($chapter->getElement('previous_version')) ? 'publishedVersion' : 'updatedVersion']], 'DC.Status-type'];
                        $url = $chapter->getChapterLink('url');
                        $url->setAbsolute();
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Source', 'content' => $url->toString()]], 'DC.Source_0'];
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Source', 'content' => 'http://dx.doi.org/' . $chapter->getElement('doi')]], 'DC.Source_1'];
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Source', 'content' => 'Chapter of ' . $book->getElement('title')]], 'DC.Source_2'];

                        //(dc:relations)
                        $references = array_filter(explode("\n", base64_decode($chapter->getElement('references'))));
                        foreach ($references as $_ => $reference) {
                                $reference = trim(preg_replace('/^\[\d+?\]/', '', strip_tags(trim($reference))));
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Relation', 'content' => $reference]], 'DC.Relation_' . $_];
                        }

                        //(dc:creator)
                        $authors = [];
                        if (!empty($chapter->readAuthors())) {
                                foreach ($chapter->readAuthors() as $author) {
                                        $authors[] = implode(', ', array_filter([$author->profile->getElement('lastname'), substr($author->profile->getElement('firstname'), 0, 1)]));
                                }
                        }

                        foreach (array_filter(json_decode($chapter->getElement('corporation') ?? '[]')) as $corporation) {
                                $authors[] = $corporation;
                        }

                        foreach ($authors as $_ => $author) {
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Creator', 'content' => $author]], 'DC.Creator_' . $_];
                        }
                        //echo '<pre>'.print_r($authors, 1).'</pre>'; exit();

                        //(dc:publisher)
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Publisher', 'content' => implode(';', array_filter([$book->getElement('publisher'), $book->getElement('publication_place')]))]], 'DC.Publisher'];
                        //(dc:coverage)
                        if (!empty($book->getElement('ddc')))
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Coverage', 'content' => 'ddc:' . $book->getElement('ddc')]], 'DC.Coverage'];

                        //(dc:language)
                        if (!empty($book->getElement('language')))
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Language', 'content' => $book->getElement('language')]], 'DC.Language'];

                        //(dc:date)
                        if (!empty($chapter->getElement('published')))
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Date', 'content' => $chapter->getElement('published')]], 'DC.Date'];

                        //(dc:description)
                        if (!empty($chapter->getElement('abstract')))
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Description', 'content' => str_replace('"', '\"', strip_tags(base64_decode($chapter->getElement('abstract'))))]], 'DC.Description'];


                        //(dc:identifier)
                        if (!empty($chapter->getElement('doi')))
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Identifier', 'content' => $chapter->getElement('doi')]], 'DC.Identifier'];

                        //(dc:title)
                        if (!empty($chapter->getElement('title')))
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Title', 'content' => $chapter->getElement('title')]], 'DC.Title'];

                        //(dc:license)
                        if (!empty($chapter->getElement('license'))) {

                                $license = new License($chapter->getElement('license'));
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Rights', 'content' => $license->getElement('url')]], 'DC.Rights'];
                        }

                        //(dc:subject)
                        $subject = json_decode($chapter->getElement('keywords'));
                        $subject[] = 'ddc:' . $book->getElement('ddc');

                        foreach (array_filter($subject) as $_ => $kw) {
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Subject', 'content' => $kw]], 'DC.Subject_' . $_];
                        }

                        //echo '<pre>'.print_r($ret, 1).print_r($head, 1).'</pre>';exit();

                        return $ret;
                }
        
                /**
                 * @param $article
                 * @param $journal
                 * @return array
                 */
                public function getArticleDCMeta($article, $journal)
                {

                        $ret = [];

                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Format', 'content' => 'text/html']], 'DC.Format'];
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Type', 'content' => 'Text']], 'DC.Type'];
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Doc-type', 'content' => 'article']], 'DC.Doc-type'];
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Status-type', 'content' => empty($article->getElement('previous_version')) ? 'publishedVersion' : 'updatedVersion']], 'DC.Status-type'];
                        $url = $article->getArticleLink('url');
                        $url->setAbsolute();
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Source', 'content' => $url->toString()]], 'DC.Source_0'];
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Source', 'content' => 'http://dx.doi.org/' . $article->getElement('doi')]], 'DC.Source_1'];
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Source', 'content' => 'Chapter of ' . $journal->getElement('title')]], 'DC.Source_2'];

                        //(dc:relations)
                        $references = array_filter(explode("\n", base64_decode($article->getElement('references'))));
                        foreach ($references as $_ => $reference) {
                                $reference = trim(preg_replace('/^\[\d+?\]/', '', strip_tags(trim($reference))));
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Relation', 'content' => $reference]], 'DC.Relation_' . $_];
                        }

                        //(dc:creator)
                        $authors = [];
                        foreach ($article->readAuthors() as $author) {
                                $authors[] = implode(', ', array_filter([$author->profile->getElement('lastname'), substr($author->profile->getElement('firstname'), 0, 1)]));
                        }

                        foreach (array_filter(json_decode($article->getElement('corporation') ?? '[]')) as $corporation) {
                                $authors[] = $corporation;
                        }

                        foreach ($authors as $_ => $author) {
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Creator', 'content' => $author]], 'DC.Creator_' . $_];
                        }
                        //echo '<pre>'.print_r($authors, 1).'</pre>'; exit();

                        //(dc:publisher)
                        $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Publisher', 'content' => implode(';', array_filter([$journal->getElement('publisher'), $journal->getElement('publication_place')]))]], 'DC.Publisher'];
                        //(dc:coverage)
                        if (!empty($journal->getElement('ddc')))
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Coverage', 'content' => 'ddc:' . $journal->getElement('ddc')]], 'DC.Coverage'];

                        //(dc:language)
                        if (!empty($journal->getElement('language')))
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Language', 'content' => $journal->getElement('language')]], 'DC.Language'];

                        //(dc:date)
                        if (!empty($article->getElement('published')))
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Date', 'content' => $article->getElement('published')]], 'DC.Date'];

                        //(dc:description)
                        if (!empty($article->getElement('abstract')))
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Description', 'content' => str_replace('"', '\"', strip_tags(base64_decode($article->getElement('abstract'))))]], 'DC.Description'];


                        //(dc:identifier)
                        if (!empty($article->getElement('doi')))
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Identifier', 'content' => $article->getElement('doi')]], 'DC.Identifier'];

                        //(dc:title)
                        if (!empty($article->getElement('title')))
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Title', 'content' => $article->getElement('title')]], 'DC.Title'];

                        //(dc:license)
                        if (!empty($article->getElement('license'))) {

                                $license = new License($article->getElement('license'));
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Rights', 'content' => $license->getElement('url')]], 'DC.Rights'];
                        }

                        //(dc:subject)
                        $subject = json_decode($article->getElement('keywords'));
                        $subject[] = 'ddc:' . $journal->getElement('ddc');

                        foreach (array_filter($subject) as $_ => $kw) {
                                $ret[] = [['#tag' => 'meta', '#attributes' => ['name' => 'DC.Subject', 'content' => $kw]], 'DC.Subject_' . $_];
                        }

                        //echo '<pre>'.print_r($ret, 1).print_r($head, 1).'</pre>';exit();

                        return $ret;
                }
        
                /**
                 * @param $wf_id
                 * @return Workflow\ArticleWorkflow|Workflow\ChapterErratumWorkflow|Workflow\ChapterWorkflow|Workflow\PaperWorkflow|null
                 */
                public function getWorkflowItem($wf_id)
                {

                        switch (\Drupal::database()->select('rwPubgoldWorkflow', 't')->fields('t', ['wf_type'])->condition('wf_id', $wf_id, '=')->execute()->fetchField()) {

                                case 'bcs': //bookchapter-submission
                                        return new \Drupal\publisso_gold\Controller\Workflow\ChapterWorkflow($wf_id);
                                        break;

                                case 'jas': //journalarticle-submission
                                        return new \Drupal\publisso_gold\Controller\Workflow\ArticleWorkflow($wf_id);
                                        break;

                                case 'cps': //conferencepaper-submission
                                        return new \Drupal\publisso_gold\Controller\Workflow\PaperWorkflow($wf_id);
                                        break;

                                case 'bce': //bookchapter-erratum
                                        return new \Drupal\publisso_gold\Controller\Workflow\ChapterErratumWorkflow($wf_id);
                                        break;

                                default:
                                        return null;
                        }
                }
        
                /**
                 * @param int $length
                 * @return string
                 */
                public function genTempPassword($length = 8)
                {

                        $password = '';
                        $pot = array(
                                range('A', 'Z'),
                                range('0', '9'),
                                range('a', 'z')
                        );

                        while (strlen($password) < $length) {

                                $index = rand(0, count($pot) - 1);
                                $password .= trim($pot[$index][rand(0, count($pot[$index]) - 1)]);
                        }

                        return $password;
                }
        
                /**
                 * @param $tmpl_name
                 * @param array $vars
                 */
                public function sendRegistrationInfoMail($tmpl_name, $vars = [])
                {

                        $mailout = $this->getMailtemplate($tmpl_name);

                        if (count($mailout)) {

                                $mailout['template'] = t($mailout['template']);

                                $delta = [
                                        'template',
                                        'recipient',
                                        'subject',
                                        'bcc',
                                        'cc'
                                ];

                                foreach ($delta as $key) {
                                        $mailout[$key] = str_replace(array_keys($vars), array_values($vars), $mailout[$key]);
                                }
                        }

                        sendMail(
                                $mailout['recipient'],
                                $mailout['subject'],
                                $mailout['template'],
                                [
                                        'cc' => $mailout['cc'],
                                        'bcc' => $mailout['bcc']
                                ]
                        );
                }
        
                /**
                 * @param $mime
                 * @return string|null
                 */
                public function mime2ext($mime)
                {
                        $mime2ext = [
                                "application/andrew-inset" => "ez",
                                "application/applixware" => "aw",
                                "application/atom+xml" => "atom",
                                "application/atomcat+xml" => "atomcat",
                                "application/atomsvc+xml" => "atomsvc",
                                "application/ccxml+xml" => "ccxml",
                                "application/cdmi-capability" => "cdmia",
                                "application/cdmi-container" => "cdmic",
                                "application/cdmi-domain" => "cdmid",
                                "application/cdmi-object" => "cdmio",
                                "application/cdmi-queue" => "cdmiq",
                                "application/cu-seeme" => "cu",
                                "application/davmount+xml" => "davmount",
                                "application/docbook+xml" => "dbk",
                                "application/dssc+der" => "dssc",
                                "application/dssc+xml" => "xdssc",
                                "application/ecmascript" => "ecma",
                                "application/emma+xml" => "emma",
                                "application/epub+zip" => "epub",
                                "application/exi" => "exi",
                                "application/font-tdpfr" => "pfr",
                                "application/gml+xml" => "gml",
                                "application/gpx+xml" => "gpx",
                                "application/gxf" => "gxf",
                                "application/hyperstudio" => "stk",
                                "application/inkml+xml" => "ink",
                                "application/ipfix" => "ipfix",
                                "application/java-archive" => "jar",
                                "application/java-serialized-object" => "ser",
                                "application/java-vm" => "class",
                                "application/javascript" => "js",
                                "application/json" => "json",
                                "application/jsonml+json" => "jsonml",
                                "application/lost+xml" => "lostxml",
                                "application/mac-binhex40" => "hqx",
                                "application/mac-compactpro" => "cpt",
                                "application/mads+xml" => "mads",
                                "application/marc" => "mrc",
                                "application/marcxml+xml" => "mrcx",
                                "application/mathematica" => "ma",
                                "application/mathml+xml" => "mathml",
                                "application/mbox" => "mbox",
                                "application/mediaservercontrol+xml" => "mscml",
                                "application/metalink+xml" => "metalink",
                                "application/metalink4+xml" => "meta4",
                                "application/mets+xml" => "mets",
                                "application/mods+xml" => "mods",
                                "application/mp21" => "m21",
                                "application/mp4" => "mp4s",
                                "application/msword" => "doc",
                                "application/mxf" => "mxf",
                                "application/octet-stream" => "bin",
                                "application/oda" => "oda",
                                "application/oebps-package+xml" => "opf",
                                "application/ogg" => "ogx",
                                "application/omdoc+xml" => "omdoc",
                                "application/onenote" => "onetoc",
                                "application/oxps" => "oxps",
                                "application/patch-ops-error+xml" => "xer",
                                "application/pdf" => "pdf",
                                "application/pgp-encrypted" => "pgp",
                                "application/pgp-signature" => "asc",
                                "application/pics-rules" => "prf",
                                "application/pkcs10" => "p10",
                                "application/pkcs7-mime" => "p7m",
                                "application/pkcs7-signature" => "p7s",
                                "application/pkcs8" => "p8",
                                "application/pkix-attr-cert" => "ac",
                                "application/pkix-cert" => "cer",
                                "application/pkix-crl" => "crl",
                                "application/pkix-pkipath" => "pkipath",
                                "application/pkixcmp" => "pki",
                                "application/pls+xml" => "pls",
                                "application/postscript" => "ai",
                                "application/prs.cww" => "cww",
                                "application/pskc+xml" => "pskcxml",
                                "application/rdf+xml" => "rdf",
                                "application/reginfo+xml" => "rif",
                                "application/relax-ng-compact-syntax" => "rnc",
                                "application/resource-lists+xml" => "rl",
                                "application/resource-lists-diff+xml" => "rld",
                                "application/rls-services+xml" => "rs",
                                "application/rpki-ghostbusters" => "gbr",
                                "application/rpki-manifest" => "mft",
                                "application/rpki-roa" => "roa",
                                "application/rsd+xml" => "rsd",
                                "application/rss+xml" => "rss",
                                "application/rtf" => "rtf",
                                "application/sbml+xml" => "sbml",
                                "application/scvp-cv-request" => "scq",
                                "application/scvp-cv-response" => "scs",
                                "application/scvp-vp-request" => "spq",
                                "application/scvp-vp-response" => "spp",
                                "application/sdp" => "sdp",
                                "application/set-payment-initiation" => "setpay",
                                "application/set-registration-initiation" => "setreg",
                                "application/shf+xml" => "shf",
                                "application/smil+xml" => "smi",
                                "application/sparql-query" => "rq",
                                "application/sparql-results+xml" => "srx",
                                "application/srgs" => "gram",
                                "application/srgs+xml" => "grxml",
                                "application/sru+xml" => "sru",
                                "application/ssdl+xml" => "ssdl",
                                "application/ssml+xml" => "ssml",
                                "application/tei+xml" => "tei",
                                "application/thraud+xml" => "tfi",
                                "application/timestamped-data" => "tsd",
                                "application/vnd.3gpp.pic-bw-large" => "plb",
                                "application/vnd.3gpp.pic-bw-small" => "psb",
                                "application/vnd.3gpp.pic-bw-var" => "pvb",
                                "application/vnd.3gpp2.tcap" => "tcap",
                                "application/vnd.3m.post-it-notes" => "pwn",
                                "application/vnd.accpac.simply.aso" => "aso",
                                "application/vnd.accpac.simply.imp" => "imp",
                                "application/vnd.acucobol" => "acu",
                                "application/vnd.acucorp" => "atc",
                                "application/vnd.adobe.air-application-installer-package+zip" => "air",
                                "application/vnd.adobe.formscentral.fcdt" => "fcdt",
                                "application/vnd.adobe.fxp" => "fxp",
                                "application/vnd.adobe.xdp+xml" => "xdp",
                                "application/vnd.adobe.xfdf" => "xfdf",
                                "application/vnd.ahead.space" => "ahead",
                                "application/vnd.airzip.filesecure.azf" => "azf",
                                "application/vnd.airzip.filesecure.azs" => "azs",
                                "application/vnd.amazon.ebook" => "azw",
                                "application/vnd.americandynamics.acc" => "acc",
                                "application/vnd.amiga.ami" => "ami",
                                "application/vnd.android.package-archive" => "apk",
                                "application/vnd.anser-web-certificate-issue-initiation" => "cii",
                                "application/vnd.anser-web-funds-transfer-initiation" => "fti",
                                "application/vnd.antix.game-component" => "atx",
                                "application/vnd.apple.installer+xml" => "mpkg",
                                "application/vnd.apple.mpegurl" => "m3u8",
                                "application/vnd.aristanetworks.swi" => "swi",
                                "application/vnd.astraea-software.iota" => "iota",
                                "application/vnd.audiograph" => "aep",
                                "application/vnd.blueice.multipass" => "mpm",
                                "application/vnd.bmi" => "bmi",
                                "application/vnd.businessobjects" => "rep",
                                "application/vnd.chemdraw+xml" => "cdxml",
                                "application/vnd.chipnuts.karaoke-mmd" => "mmd",
                                "application/vnd.cinderella" => "cdy",
                                "application/vnd.claymore" => "cla",
                                "application/vnd.cloanto.rp9" => "rp9",
                                "application/vnd.clonk.c4group" => "c4g",
                                "application/vnd.cluetrust.cartomobile-config" => "c11amc",
                                "application/vnd.cluetrust.cartomobile-config-pkg" => "c11amz",
                                "application/vnd.commonspace" => "csp",
                                "application/vnd.contact.cmsg" => "cdbcmsg",
                                "application/vnd.cosmocaller" => "cmc",
                                "application/vnd.crick.clicker" => "clkx",
                                "application/vnd.crick.clicker.keyboard" => "clkk",
                                "application/vnd.crick.clicker.palette" => "clkp",
                                "application/vnd.crick.clicker.template" => "clkt",
                                "application/vnd.crick.clicker.wordbank" => "clkw",
                                "application/vnd.criticaltools.wbs+xml" => "wbs",
                                "application/vnd.ctc-posml" => "pml",
                                "application/vnd.cups-ppd" => "ppd",
                                "application/vnd.curl.car" => "car",
                                "application/vnd.curl.pcurl" => "pcurl",
                                "application/vnd.dart" => "dart",
                                "application/vnd.data-vision.rdz" => "rdz",
                                "application/vnd.dece.data" => "uvf",
                                "application/vnd.dece.ttml+xml" => "uvt",
                                "application/vnd.dece.unspecified" => "uvx",
                                "application/vnd.dece.zip" => "uvz",
                                "application/vnd.denovo.fcselayout-link" => "fe_launch",
                                "application/vnd.dna" => "dna",
                                "application/vnd.dolby.mlp" => "mlp",
                                "application/vnd.dpgraph" => "dpg",
                                "application/vnd.dreamfactory" => "dfac",
                                "application/vnd.ds-keypoint" => "kpxx",
                                "application/vnd.dvb.ait" => "ait",
                                "application/vnd.dvb.service" => "svc",
                                "application/vnd.dynageo" => "geo",
                                "application/vnd.ecowin.chart" => "mag",
                                "application/vnd.enliven" => "nml",
                                "application/vnd.epson.esf" => "esf",
                                "application/vnd.epson.msf" => "msf",
                                "application/vnd.epson.quickanime" => "qam",
                                "application/vnd.epson.salt" => "slt",
                                "application/vnd.epson.ssf" => "ssf",
                                "application/vnd.eszigno3+xml" => "es3",
                                "application/vnd.ezpix-album" => "ez2",
                                "application/vnd.ezpix-package" => "ez3",
                                "application/vnd.fdf" => "fdf",
                                "application/vnd.fdsn.mseed" => "mseed",
                                "application/vnd.fdsn.seed" => "seed",
                                "application/vnd.flographit" => "gph",
                                "application/vnd.fluxtime.clip" => "ftc",
                                "application/vnd.framemaker" => "fm",
                                "application/vnd.frogans.fnc" => "fnc",
                                "application/vnd.frogans.ltf" => "ltf",
                                "application/vnd.fsc.weblaunch" => "fsc",
                                "application/vnd.fujitsu.oasys" => "oas",
                                "application/vnd.fujitsu.oasys2" => "oa2",
                                "application/vnd.fujitsu.oasys3" => "oa3",
                                "application/vnd.fujitsu.oasysgp" => "fg5",
                                "application/vnd.fujitsu.oasysprs" => "bh2",
                                "application/vnd.fujixerox.ddd" => "ddd",
                                "application/vnd.fujixerox.docuworks" => "xdw",
                                "application/vnd.fujixerox.docuworks.binder" => "xbd",
                                "application/vnd.fuzzysheet" => "fzs",
                                "application/vnd.genomatix.tuxedo" => "txd",
                                "application/vnd.geogebra.file" => "ggb",
                                "application/vnd.geogebra.tool" => "ggt",
                                "application/vnd.geometry-explorer" => "gex",
                                "application/vnd.geonext" => "gxt",
                                "application/vnd.geoplan" => "g2w",
                                "application/vnd.geospace" => "g3w",
                                "application/vnd.gmx" => "gmx",
                                "application/vnd.google-earth.kml+xml" => "kml",
                                "application/vnd.google-earth.kmz" => "kmz",
                                "application/vnd.grafeq" => "gqf",
                                "application/vnd.groove-account" => "gac",
                                "application/vnd.groove-help" => "ghf",
                                "application/vnd.groove-identity-message" => "gim",
                                "application/vnd.groove-injector" => "grv",
                                "application/vnd.groove-tool-message" => "gtm",
                                "application/vnd.groove-tool-template" => "tpl",
                                "application/vnd.groove-vcard" => "vcg",
                                "application/vnd.hal+xml" => "hal",
                                "application/vnd.handheld-entertainment+xml" => "zmm",
                                "application/vnd.hbci" => "hbci",
                                "application/vnd.hhe.lesson-player" => "les",
                                "application/vnd.hp-hpgl" => "hpgl",
                                "application/vnd.hp-hpid" => "hpid",
                                "application/vnd.hp-hps" => "hps",
                                "application/vnd.hp-jlyt" => "jlt",
                                "application/vnd.hp-pcl" => "pcl",
                                "application/vnd.hp-pclxl" => "pclxl",
                                "application/vnd.hydrostatix.sof-data" => "sfd-hdstx",
                                "application/vnd.ibm.minipay" => "mpy",
                                "application/vnd.ibm.modcap" => "afp",
                                "application/vnd.ibm.rights-management" => "irm",
                                "application/vnd.ibm.secure-container" => "sc",
                                "application/vnd.iccprofile" => "icc",
                                "application/vnd.igloader" => "igl",
                                "application/vnd.immervision-ivp" => "ivp",
                                "application/vnd.immervision-ivu" => "ivu",
                                "application/vnd.insors.igm" => "igm",
                                "application/vnd.intercon.formnet" => "xpw",
                                "application/vnd.intergeo" => "i2g",
                                "application/vnd.intu.qbo" => "qbo",
                                "application/vnd.intu.qfx" => "qfx",
                                "application/vnd.ipunplugged.rcprofile" => "rcprofile",
                                "application/vnd.irepository.package+xml" => "irp",
                                "application/vnd.is-xpr" => "xpr",
                                "application/vnd.isac.fcs" => "fcs",
                                "application/vnd.jam" => "jam",
                                "application/vnd.jcp.javame.midlet-rms" => "rms",
                                "application/vnd.jisp" => "jisp",
                                "application/vnd.joost.joda-archive" => "joda",
                                "application/vnd.kahootz" => "ktz",
                                "application/vnd.kde.karbon" => "karbon",
                                "application/vnd.kde.kchart" => "chrt",
                                "application/vnd.kde.kformula" => "kfo",
                                "application/vnd.kde.kivio" => "flw",
                                "application/vnd.kde.kontour" => "kon",
                                "application/vnd.kde.kpresenter" => "kpr",
                                "application/vnd.kde.kspread" => "ksp",
                                "application/vnd.kde.kword" => "kwd",
                                "application/vnd.kenameaapp" => "htke",
                                "application/vnd.kidspiration" => "kia",
                                "application/vnd.kinar" => "kne",
                                "application/vnd.koan" => "skp",
                                "application/vnd.kodak-descriptor" => "sse",
                                "application/vnd.las.las+xml" => "lasxml",
                                "application/vnd.llamagraphics.life-balance.desktop" => "lbd",
                                "application/vnd.llamagraphics.life-balance.exchange+xml" => "lbe",
                                "application/vnd.lotus-1-2-3" => "123",
                                "application/vnd.lotus-approach" => "apr",
                                "application/vnd.lotus-freelance" => "pre",
                                "application/vnd.lotus-notes" => "nsf",
                                "application/vnd.lotus-organizer" => "org",
                                "application/vnd.lotus-screencam" => "scm",
                                "application/vnd.lotus-wordpro" => "lwp",
                                "application/vnd.macports.portpkg" => "portpkg",
                                "application/vnd.mcd" => "mcd",
                                "application/vnd.medcalcdata" => "mc1",
                                "application/vnd.mediastation.cdkey" => "cdkey",
                                "application/vnd.mfer" => "mwf",
                                "application/vnd.mfmp" => "mfm",
                                "application/vnd.micrografx.flo" => "flo",
                                "application/vnd.micrografx.igx" => "igx",
                                "application/vnd.mif" => "mif",
                                "application/vnd.mobius.daf" => "daf",
                                "application/vnd.mobius.dis" => "dis",
                                "application/vnd.mobius.mbk" => "mbk",
                                "application/vnd.mobius.mqy" => "mqy",
                                "application/vnd.mobius.msl" => "msl",
                                "application/vnd.mobius.plc" => "plc",
                                "application/vnd.mobius.txf" => "txf",
                                "application/vnd.mophun.application" => "mpn",
                                "application/vnd.mophun.certificate" => "mpc",
                                "application/vnd.mozilla.xul+xml" => "xul",
                                "application/vnd.ms-artgalry" => "cil",
                                "application/vnd.ms-cab-compressed" => "cab",
                                "application/vnd.ms-excel" => "xls",
                                "application/vnd.ms-excel.addin.macroenabled.12" => "xlam",
                                "application/vnd.ms-excel.sheet.binary.macroenabled.12" => "xlsb",
                                "application/vnd.ms-excel.sheet.macroenabled.12" => "xlsm",
                                "application/vnd.ms-excel.template.macroenabled.12" => "xltm",
                                "application/vnd.ms-fontobject" => "eot",
                                "application/vnd.ms-htmlhelp" => "chm",
                                "application/vnd.ms-ims" => "ims",
                                "application/vnd.ms-lrm" => "lrm",
                                "application/vnd.ms-officetheme" => "thmx",
                                "application/vnd.ms-pki.seccat" => "cat",
                                "application/vnd.ms-pki.stl" => "stl",
                                "application/vnd.ms-powerpoint" => "ppt",
                                "application/vnd.ms-powerpoint.addin.macroenabled.12" => "ppam",
                                "application/vnd.ms-powerpoint.presentation.macroenabled.12" => "pptm",
                                "application/vnd.ms-powerpoint.slide.macroenabled.12" => "sldm",
                                "application/vnd.ms-powerpoint.slideshow.macroenabled.12" => "ppsm",
                                "application/vnd.ms-powerpoint.template.macroenabled.12" => "potm",
                                "application/vnd.ms-project" => "mpp",
                                "application/vnd.ms-word.document.macroenabled.12" => "docm",
                                "application/vnd.ms-word.template.macroenabled.12" => "dotm",
                                "application/vnd.ms-works" => "wps",
                                "application/vnd.ms-wpl" => "wpl",
                                "application/vnd.ms-xpsdocument" => "xps",
                                "application/vnd.mseq" => "mseq",
                                "application/vnd.musician" => "mus",
                                "application/vnd.muvee.style" => "msty",
                                "application/vnd.mynfc" => "taglet",
                                "application/vnd.neurolanguage.nlu" => "nlu",
                                "application/vnd.nitf" => "ntf",
                                "application/vnd.noblenet-directory" => "nnd",
                                "application/vnd.noblenet-sealer" => "nns",
                                "application/vnd.noblenet-web" => "nnw",
                                "application/vnd.nokia.n-gage.data" => "ngdat",
                                "application/vnd.nokia.n-gage.symbian.install" => "n-gage",
                                "application/vnd.nokia.radio-preset" => "rpst",
                                "application/vnd.nokia.radio-presets" => "rpss",
                                "application/vnd.novadigm.edm" => "edm",
                                "application/vnd.novadigm.edx" => "edx",
                                "application/vnd.novadigm.ext" => "ext",
                                "application/vnd.oasis.opendocument.chart" => "odc",
                                "application/vnd.oasis.opendocument.chart-template" => "otc",
                                "application/vnd.oasis.opendocument.database" => "odb",
                                "application/vnd.oasis.opendocument.formula" => "odf",
                                "application/vnd.oasis.opendocument.formula-template" => "odft",
                                "application/vnd.oasis.opendocument.graphics" => "odg",
                                "application/vnd.oasis.opendocument.graphics-template" => "otg",
                                "application/vnd.oasis.opendocument.image" => "odi",
                                "application/vnd.oasis.opendocument.image-template" => "oti",
                                "application/vnd.oasis.opendocument.presentation" => "odp",
                                "application/vnd.oasis.opendocument.presentation-template" => "otp",
                                "application/vnd.oasis.opendocument.spreadsheet" => "ods",
                                "application/vnd.oasis.opendocument.spreadsheet-template" => "ots",
                                "application/vnd.oasis.opendocument.text" => "odt",
                                "application/vnd.oasis.opendocument.text-master" => "odm",
                                "application/vnd.oasis.opendocument.text-template" => "ott",
                                "application/vnd.oasis.opendocument.text-web" => "oth",
                                "application/vnd.olpc-sugar" => "xo",
                                "application/vnd.oma.dd2+xml" => "dd2",
                                "application/vnd.openofficeorg.extension" => "oxt",
                                "application/vnd.openxmlformats-officedocument.presentationml.presentation" => "pptx",
                                "application/vnd.openxmlformats-officedocument.presentationml.slide" => "sldx",
                                "application/vnd.openxmlformats-officedocument.presentationml.slideshow" => "ppsx",
                                "application/vnd.openxmlformats-officedocument.presentationml.template" => "potx",
                                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" => "xlsx",
                                "application/vnd.openxmlformats-officedocument.spreadsheetml.template" => "xltx",
                                "application/vnd.openxmlformats-officedocument.wordprocessingml.document" => "docx",
                                "application/vnd.openxmlformats-officedocument.wordprocessingml.template" => "dotx",
                                "application/vnd.osgeo.mapguide.package" => "mgp",
                                "application/vnd.osgi.dp" => "dp",
                                "application/vnd.osgi.subsystem" => "esa",
                                "application/vnd.palm" => "pdb",
                                "application/vnd.pawaafile" => "paw",
                                "application/vnd.pg.format" => "str",
                                "application/vnd.pg.osasli" => "ei6",
                                "application/vnd.picsel" => "efif",
                                "application/vnd.pmi.widget" => "wg",
                                "application/vnd.pocketlearn" => "plf",
                                "application/vnd.powerbuilder6" => "pbd",
                                "application/vnd.previewsystems.box" => "box",
                                "application/vnd.proteus.magazine" => "mgz",
                                "application/vnd.publishare-delta-tree" => "qps",
                                "application/vnd.pvi.ptid1" => "ptid",
                                "application/vnd.quark.quarkxpress" => "qxd",
                                "application/vnd.realvnc.bed" => "bed",
                                "application/vnd.recordare.musicxml" => "mxl",
                                "application/vnd.recordare.musicxml+xml" => "musicxml",
                                "application/vnd.rig.cryptonote" => "cryptonote",
                                "application/vnd.rim.cod" => "cod",
                                "application/vnd.rn-realmedia" => "rm",
                                "application/vnd.rn-realmedia-vbr" => "rmvb",
                                "application/vnd.route66.link66+xml" => "link66",
                                "application/vnd.sailingtracker.track" => "st",
                                "application/vnd.seemail" => "see",
                                "application/vnd.sema" => "sema",
                                "application/vnd.semd" => "semd",
                                "application/vnd.semf" => "semf",
                                "application/vnd.shana.informed.formdata" => "ifm",
                                "application/vnd.shana.informed.formtemplate" => "itp",
                                "application/vnd.shana.informed.interchange" => "iif",
                                "application/vnd.shana.informed.package" => "ipk",
                                "application/vnd.simtech-mindmapper" => "twd",
                                "application/vnd.smaf" => "mmf",
                                "application/vnd.smart.teacher" => "teacher",
                                "application/vnd.solent.sdkm+xml" => "sdkm",
                                "application/vnd.spotfire.dxp" => "dxp",
                                "application/vnd.spotfire.sfs" => "sfs",
                                "application/vnd.stardivision.calc" => "sdc",
                                "application/vnd.stardivision.draw" => "sda",
                                "application/vnd.stardivision.impress" => "sdd",
                                "application/vnd.stardivision.math" => "smf",
                                "application/vnd.stardivision.writer" => "sdw",
                                "application/vnd.stardivision.writer-global" => "sgl",
                                "application/vnd.stepmania.package" => "smzip",
                                "application/vnd.stepmania.stepchart" => "sm",
                                "application/vnd.sun.xml.calc" => "sxc",
                                "application/vnd.sun.xml.calc.template" => "stc",
                                "application/vnd.sun.xml.draw" => "sxd",
                                "application/vnd.sun.xml.draw.template" => "std",
                                "application/vnd.sun.xml.impress" => "sxi",
                                "application/vnd.sun.xml.impress.template" => "sti",
                                "application/vnd.sun.xml.math" => "sxm",
                                "application/vnd.sun.xml.writer" => "sxw",
                                "application/vnd.sun.xml.writer.global" => "sxg",
                                "application/vnd.sun.xml.writer.template" => "stw",
                                "application/vnd.sus-calendar" => "sus",
                                "application/vnd.svd" => "svd",
                                "application/vnd.symbian.install" => "sis",
                                "application/vnd.syncml+xml" => "xsm",
                                "application/vnd.syncml.dm+wbxml" => "bdm",
                                "application/vnd.syncml.dm+xml" => "xdm",
                                "application/vnd.tao.intent-module-archive" => "tao",
                                "application/vnd.tcpdump.pcap" => "pcap",
                                "application/vnd.tmobile-livetv" => "tmo",
                                "application/vnd.trid.tpt" => "tpt",
                                "application/vnd.triscape.mxs" => "mxs",
                                "application/vnd.trueapp" => "tra",
                                "application/vnd.ufdl" => "ufd",
                                "application/vnd.uiq.theme" => "utz",
                                "application/vnd.umajin" => "umj",
                                "application/vnd.unity" => "unityweb",
                                "application/vnd.uoml+xml" => "uoml",
                                "application/vnd.vcx" => "vcx",
                                "application/vnd.visio" => "vsd",
                                "application/vnd.visionary" => "vis",
                                "application/vnd.vsf" => "vsf",
                                "application/vnd.wap.wbxml" => "wbxml",
                                "application/vnd.wap.wmlc" => "wmlc",
                                "application/vnd.wap.wmlscriptc" => "wmlsc",
                                "application/vnd.webturbo" => "wtb",
                                "application/vnd.wolfram.player" => "nbp",
                                "application/vnd.wordperfect" => "wpd",
                                "application/vnd.wqd" => "wqd",
                                "application/vnd.wt.stf" => "stf",
                                "application/vnd.xara" => "xar",
                                "application/vnd.xfdl" => "xfdl",
                                "application/vnd.yamaha.hv-dic" => "hvd",
                                "application/vnd.yamaha.hv-script" => "hvs",
                                "application/vnd.yamaha.hv-voice" => "hvp",
                                "application/vnd.yamaha.openscoreformat" => "osf",
                                "application/vnd.yamaha.openscoreformat.osfpvg+xml" => "osfpvg",
                                "application/vnd.yamaha.smaf-audio" => "saf",
                                "application/vnd.yamaha.smaf-phrase" => "spf",
                                "application/vnd.yellowriver-custom-menu" => "cmp",
                                "application/vnd.zul" => "zir",
                                "application/vnd.zzazz.deck+xml" => "zaz",
                                "application/voicexml+xml" => "vxml",
                                "application/widget" => "wgt",
                                "application/winhlp" => "hlp",
                                "application/wsdl+xml" => "wsdl",
                                "application/wspolicy+xml" => "wspolicy",
                                "application/x-7z-compressed" => "7z",
                                "application/x-abiword" => "abw",
                                "application/x-ace-compressed" => "ace",
                                "application/x-apple-diskimage" => "dmg",
                                "application/x-authorware-bin" => "aab",
                                "application/x-authorware-map" => "aam",
                                "application/x-authorware-seg" => "aas",
                                "application/x-bcpio" => "bcpio",
                                "application/x-bittorrent" => "torrent",
                                "application/x-blorb" => "blb",
                                "application/x-bzip" => "bz",
                                "application/x-bzip2" => "bz2",
                                "application/x-cbr" => "cbr",
                                "application/x-cdlink" => "vcd",
                                "application/x-cfs-compressed" => "cfs",
                                "application/x-chat" => "chat",
                                "application/x-chess-pgn" => "pgn",
                                "application/x-conference" => "nsc",
                                "application/x-cpio" => "cpio",
                                "application/x-csh" => "csh",
                                "application/x-debian-package" => "deb",
                                "application/x-dgc-compressed" => "dgc",
                                "application/x-director" => "dir",
                                "application/x-doom" => "wad",
                                "application/x-dtbncx+xml" => "ncx",
                                "application/x-dtbook+xml" => "dtb",
                                "application/x-dtbresource+xml" => "res",
                                "application/x-dvi" => "dvi",
                                "application/x-envoy" => "evy",
                                "application/x-eva" => "eva",
                                "application/x-font-bdf" => "bdf",
                                "application/x-font-ghostscript" => "gsf",
                                "application/x-font-linux-psf" => "psf",
                                "application/x-font-otf" => "otf",
                                "application/x-font-pcf" => "pcf",
                                "application/x-font-snf" => "snf",
                                "application/x-font-ttf" => "ttf",
                                "application/x-font-type1" => "pfa",
                                "application/x-font-woff" => "woff",
                                "application/x-freearc" => "arc",
                                "application/x-futuresplash" => "spl",
                                "application/x-gca-compressed" => "gca",
                                "application/x-glulx" => "ulx",
                                "application/x-gnumeric" => "gnumeric",
                                "application/x-gramps-xml" => "gramps",
                                "application/x-gtar" => "gtar",
                                "application/x-hdf" => "hdf",
                                "application/x-install-instructions" => "install",
                                "application/x-iso9660-image" => "iso",
                                "application/x-java-jnlp-file" => "jnlp",
                                "application/x-latex" => "latex",
                                "application/x-lzh-compressed" => "lzh",
                                "application/x-mie" => "mie",
                                "application/x-mobipocket-ebook" => "prc",
                                "application/x-ms-application" => "application",
                                "application/x-ms-shortcut" => "lnk",
                                "application/x-ms-wmd" => "wmd",
                                "application/x-ms-wmz" => "wmz",
                                "application/x-ms-xbap" => "xbap",
                                "application/x-msaccess" => "mdb",
                                "application/x-msbinder" => "obd",
                                "application/x-mscardfile" => "crd",
                                "application/x-msclip" => "clp",
                                "application/x-msdownload" => "exe",
                                "application/x-msmediaview" => "mvb",
                                "application/x-msmetafile" => "wmf",
                                "application/x-msmoney" => "mny",
                                "application/x-mspublisher" => "pub",
                                "application/x-msschedule" => "scd",
                                "application/x-msterminal" => "trm",
                                "application/x-mswrite" => "wri",
                                "application/x-netcdf" => "nc",
                                "application/x-nzb" => "nzb",
                                "application/x-pkcs12" => "p12",
                                "application/x-pkcs7-certificates" => "p7b",
                                "application/x-pkcs7-certreqresp" => "p7r",
                                "application/x-rar-compressed" => "rar",
                                "application/x-research-info-systems" => "ris",
                                "application/x-sh" => "sh",
                                "application/x-shar" => "shar",
                                "application/x-shockwave-flash" => "swf",
                                "application/x-silverlight-app" => "xap",
                                "application/x-sql" => "sql",
                                "application/x-stuffit" => "sit",
                                "application/x-stuffitx" => "sitx",
                                "application/x-subrip" => "srt",
                                "application/x-sv4cpio" => "sv4cpio",
                                "application/x-sv4crc" => "sv4crc",
                                "application/x-t3vm-image" => "t3",
                                "application/x-tads" => "gam",
                                "application/x-tar" => "tar",
                                "application/x-tcl" => "tcl",
                                "application/x-tex" => "tex",
                                "application/x-tex-tfm" => "tfm",
                                "application/x-texinfo" => "texinfo",
                                "application/x-tgif" => "obj",
                                "application/x-ustar" => "ustar",
                                "application/x-wais-source" => "src",
                                "application/x-x509-ca-cert" => "der",
                                "application/x-xfig" => "fig",
                                "application/x-xliff+xml" => "xlf",
                                "application/x-xpinstall" => "xpi",
                                "application/x-xz" => "xz",
                                "application/x-zmachine" => "z1",
                                "application/xaml+xml" => "xaml",
                                "application/xcap-diff+xml" => "xdf",
                                "application/xenc+xml" => "xenc",
                                "application/xhtml+xml" => "xhtml",
                                "application/xml" => "xml",
                                "application/xml-dtd" => "dtd",
                                "application/xop+xml" => "xop",
                                "application/xproc+xml" => "xpl",
                                "application/xslt+xml" => "xslt",
                                "application/xspf+xml" => "xspf",
                                "application/xv+xml" => "mxml",
                                "application/yang" => "yang",
                                "application/yin+xml" => "yin",
                                "application/zip" => "zip",
                                "audio/adpcm" => "adp",
                                "audio/basic" => "au",
                                "audio/midi" => "mid",
                                "audio/mp4" => "m4a",
                                "audio/mpeg" => "mpga",
                                "audio/ogg" => "oga",
                                "audio/s3m" => "s3m",
                                "audio/silk" => "sil",
                                "audio/vnd.dece.audio" => "uva",
                                "audio/vnd.digital-winds" => "eol",
                                "audio/vnd.dra" => "dra",
                                "audio/vnd.dts" => "dts",
                                "audio/vnd.dts.hd" => "dtshd",
                                "audio/vnd.lucent.voice" => "lvp",
                                "audio/vnd.ms-playready.media.pya" => "pya",
                                "audio/vnd.nuera.ecelp4800" => "ecelp4800",
                                "audio/vnd.nuera.ecelp7470" => "ecelp7470",
                                "audio/vnd.nuera.ecelp9600" => "ecelp9600",
                                "audio/vnd.rip" => "rip",
                                "audio/webm" => "weba",
                                "audio/x-aac" => "aac",
                                "audio/x-aiff" => "aif",
                                "audio/x-caf" => "caf",
                                "audio/x-flac" => "flac",
                                "audio/x-matroska" => "mka",
                                "audio/x-mpegurl" => "m3u",
                                "audio/x-ms-wax" => "wax",
                                "audio/x-ms-wma" => "wma",
                                "audio/x-pn-realaudio" => "ram",
                                "audio/x-pn-realaudio-plugin" => "rmp",
                                "audio/x-wav" => "wav",
                                "audio/xm" => "xm",
                                "chemical/x-cdx" => "cdx",
                                "chemical/x-cif" => "cif",
                                "chemical/x-cmdf" => "cmdf",
                                "chemical/x-cml" => "cml",
                                "chemical/x-csml" => "csml",
                                "chemical/x-xyz" => "xyz",
                                "image/bmp" => "bmp",
                                "image/cgm" => "cgm",
                                "image/g3fax" => "g3",
                                "image/gif" => "gif",
                                "image/ief" => "ief",
                                "image/jpeg" => "jpeg",
                                "image/ktx" => "ktx",
                                "image/png" => "png",
                                "image/prs.btif" => "btif",
                                "image/sgi" => "sgi",
                                "image/svg+xml" => "svg",
                                "image/tiff" => "tiff",
                                "image/vnd.adobe.photoshop" => "psd",
                                "image/vnd.dece.graphic" => "uvi",
                                "image/vnd.dvb.subtitle" => "sub",
                                "image/vnd.djvu" => "djvu",
                                "image/vnd.dwg" => "dwg",
                                "image/vnd.dxf" => "dxf",
                                "image/vnd.fastbidsheet" => "fbs",
                                "image/vnd.fpx" => "fpx",
                                "image/vnd.fst" => "fst",
                                "image/vnd.fujixerox.edmics-mmr" => "mmr",
                                "image/vnd.fujixerox.edmics-rlc" => "rlc",
                                "image/vnd.ms-modi" => "mdi",
                                "image/vnd.ms-photo" => "wdp",
                                "image/vnd.net-fpx" => "npx",
                                "image/vnd.wap.wbmp" => "wbmp",
                                "image/vnd.xiff" => "xif",
                                "image/webp" => "webp",
                                "image/x-3ds" => "3ds",
                                "image/x-cmu-raster" => "ras",
                                "image/x-cmx" => "cmx",
                                "image/x-freehand" => "fh",
                                "image/x-icon" => "ico",
                                "image/x-mrsid-image" => "sid",
                                "image/x-pcx" => "pcx",
                                "image/x-pict" => "pic",
                                "image/x-portable-anymap" => "pnm",
                                "image/x-portable-bitmap" => "pbm",
                                "image/x-portable-graymap" => "pgm",
                                "image/x-portable-pixmap" => "ppm",
                                "image/x-rgb" => "rgb",
                                "image/x-tga" => "tga",
                                "image/x-xbitmap" => "xbm",
                                "image/x-xpixmap" => "xpm",
                                "image/x-xwindowdump" => "xwd",
                                "message/rfc822" => "eml",
                                "model/iges" => "igs",
                                "model/mesh" => "msh",
                                "model/vnd.collada+xml" => "dae",
                                "model/vnd.dwf" => "dwf",
                                "model/vnd.gdl" => "gdl",
                                "model/vnd.gtw" => "gtw",
                                "model/vnd.mts" => "mts",
                                "model/vnd.vtu" => "vtu",
                                "model/vrml" => "wrl",
                                "model/x3d+binary" => "x3db",
                                "model/x3d+vrml" => "x3dv",
                                "model/x3d+xml" => "x3d",
                                "text/cache-manifest" => "appcache",
                                "text/calendar" => "ics",
                                "text/css" => "css",
                                "text/csv" => "csv",
                                "text/html" => "html",
                                "text/n3" => "n3",
                                "text/plain" => "txt",
                                "text/prs.lines.tag" => "dsc",
                                "text/richtext" => "rtx",
                                "text/sgml" => "sgml",
                                "text/tab-separated-values" => "tsv",
                                "text/troff" => "t",
                                "text/turtle" => "ttl",
                                "text/uri-list" => "uri",
                                "text/vcard" => "vcard",
                                "text/vnd.curl" => "curl",
                                "text/vnd.curl.dcurl" => "dcurl",
                                "text/vnd.curl.scurl" => "scurl",
                                "text/vnd.curl.mcurl" => "mcurl",
                                "text/vnd.dvb.subtitle" => "sub",
                                "text/vnd.fly" => "fly",
                                "text/vnd.fmi.flexstor" => "flx",
                                "text/vnd.graphviz" => "gv",
                                "text/vnd.in3d.3dml" => "3dml",
                                "text/vnd.in3d.spot" => "spot",
                                "text/vnd.sun.j2me.app-descriptor" => "jad",
                                "text/vnd.wap.wml" => "wml",
                                "text/vnd.wap.wmlscript" => "wmls",
                                "text/x-asm" => "s",
                                "text/x-c" => "c",
                                "text/x-fortran" => "f",
                                "text/x-java-source" => "java",
                                "text/x-opml" => "opml",
                                "text/x-pascal" => "p",
                                "text/x-nfo" => "nfo",
                                "text/x-setext" => "etx",
                                "text/x-sfv" => "sfv",
                                "text/x-uuencode" => "uu",
                                "text/x-vcalendar" => "vcs",
                                "text/x-vcard" => "vcf",
                                "video/3gpp" => "3gp",
                                "video/3gpp2" => "3g2",
                                "video/h261" => "h261",
                                "video/h263" => "h263",
                                "video/h264" => "h264",
                                "video/jpeg" => "jpgv",
                                "video/jpm" => "jpm",
                                "video/mj2" => "mj2",
                                "video/mp4" => "mp4",
                                "video/mpeg" => "mpeg",
                                "video/ogg" => "ogv",
                                "video/quicktime" => "qt",
                                "video/vnd.dece.hd" => "uvh",
                                "video/vnd.dece.mobile" => "uvm",
                                "video/vnd.dece.pd" => "uvp",
                                "video/vnd.dece.sd" => "uvs",
                                "video/vnd.dece.video" => "uvv",
                                "video/vnd.dvb.file" => "dvb",
                                "video/vnd.fvt" => "fvt",
                                "video/vnd.mpegurl" => "mxu",
                                "video/vnd.ms-playready.media.pyv" => "pyv",
                                "video/vnd.uvvu.mp4" => "uvu",
                                "video/vnd.vivo" => "viv",
                                "video/webm" => "webm",
                                "video/x-f4v" => "f4v",
                                "video/x-fli" => "fli",
                                "video/x-flv" => "flv",
                                "video/x-m4v" => "m4v",
                                "video/x-matroska" => "mkv",
                                "video/x-mng" => "mng",
                                "video/x-ms-asf" => "asf",
                                "video/x-ms-vob" => "vob",
                                "video/x-ms-wm" => "wm",
                                "video/x-ms-wmv" => "wmv",
                                "video/x-ms-wmx" => "wmx",
                                "video/x-ms-wvx" => "wvx",
                                "video/x-msvideo" => "avi",
                                "video/x-sgi-movie" => "movie",
                                "video/x-smv" => "smv",
                                "x-conference/x-cooltalk" => "ice"
                        ];

                        return $mime2ext[$mime] ?? null;
                }
        
                /**
                 * @param $str
                 * @param string $subject
                 * @return string
                 */
                public function encryptHTMLEmailaddresses($str, $subject = '')
                {

                        $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
                        $html = new \DOMDocument();
                        $html->loadHTML($str, LIBXML_NOWARNING | LIBXML_NOERROR);
                        $xpath = new \DOMXPath($html);
                        $nodesReplace = [];

                        foreach ($xpath->evaluate('//a') as $nodeA) {

                                if ($nodeA->hasAttribute('href') && substr($nodeA->getAttribute('href'), 0, 7) == 'mailto:') {
                                        $nodesReplace[] = $nodeA;
                                }
                        }

                        #return $html;

                        foreach ($nodesReplace as $nodeA) {

                                $parentNode = $nodeA->parentNode;
                                $nodeValue = $nodeA->nodeValue;
                                $nodeHref = $nodeA->getAttribute('href');

                                if (substr($nodeHref, 0, 7) == 'mailto:') {

                                        preg_match('/\?.+/', $nodeHref, $matches);
                                        $ext = $matches[0];
                                        $email = explode('?', str_replace('mailto:', '', $nodeHref))[0];
                                        $hash = md5($email);

                                        $anchorNode = $html->createElement('a');
                                        $anchorNode->setAttribute('class', 'encmailadd');
                                        $anchorNode->setAttribute('data', $hash);

                                        if ($ext) {
                                                $ext = substr($ext, 1, strlen($ext)); //eliminate "?"
                                                parse_str($ext, $ext);
                                        }

                                        if (!array_key_exists('subject', $ext) && $subject) {
                                                $ext['subject'] = $subject;
                                        }

                                        if (count($ext)) {
                                                $anchorNode->setAttribute('href', '?' . http_build_query($ext, null, '&', PHP_QUERY_RFC3986));
                                        }

                                        $anchorNode->nodeValue = $nodeValue;

                                        if (null === ($encdata = $tempstore->get('encdata'))) {
                                                $encdata = serialize([]);
                                        }

                                        if (isset($encdata)) {
                                                $encdata = unserialize($encdata);
                                                $encdata[$hash] = $email;

                                                $tempstore->set('encdata', serialize($encdata));

                                                $parentNode->replaceChild($anchorNode, $nodeA);
                                        }
                                }
                        }

                        return $html->saveHTML();
                }
        
                /**
                 * @param $text
                 * @return string|string[]
                 */
                public function normalizeInlineImageLinks($text)
                {

                        return $text;

                        $replace = array();
                        preg_match_all('/^(.*)<img.+?src="(.+?)".*?>(.*)$/m', $text, $all_matches);

                        foreach ($all_matches[2] as $match) {

                                if (preg_match('/^.*\/(.*)$/', $match, $matches)) {
                                        $replace[$match] = '/publisso_gold/pic?pth=' . $matches[1];
                                }
                        }
                        return str_replace(array_keys($replace), array_values($replace), $text);
                }
        
                /**
                 * @param string $string
                 * @return bool
                 */
                public function isBase64(string $string){
                        
                        if (($decoded = base64_decode($string, true)) === false) return false;
        
                        $encoding = mb_detect_encoding($decoded);
                        if (in_array($encoding, array('UTF-8', 'ASCII'))) return true;
                        else return false;
                }
        
                /**
                 * @param $hexCode
                 * @param $adjustPercent
                 * @return string
                 */
                public function adjustBrightness($hexCode, $adjustPercent) {
                        
                        $hexCode = ltrim($hexCode, '#');
        
                        if (strlen($hexCode) == 3)
                                $hexCode = $hexCode[0] . $hexCode[0] . $hexCode[1] . $hexCode[1] . $hexCode[2] . $hexCode[2];
        
                        $hexCode = array_map("hexdec", str_split($hexCode, 2));
        
                        foreach ($hexCode as & $color) {
                                
                                $adjustableLimit = $adjustPercent < 0 ? $color : 255 - $color;
                                $adjustAmount = ceil($adjustableLimit * $adjustPercent);
                                $color = str_pad(dechex($color + $adjustAmount), 2, '0', STR_PAD_LEFT);
                        }
        
                        return '#' . implode($hexCode);
                }
        }

?>
