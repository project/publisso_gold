<?php
        
        /**
         * @file
         * Contains \Drupal\publisso_gold\Controller\Issue.
         */
        
        namespace Drupal\publisso_gold\Controller;
        
        /**
         * Class Issue
         * @package Drupal\publisso_gold\Controller
         */
        class Issue
        {
                
                private $id;
                private $elements;
                private $table;
                private $articles;
        
                /**
                 * @return string
                 */
                public function __toString() {
                        return "class issue";
                }
        
                /**
                 * Issue constructor.
                 * @param null $id
                 */
                public function __construct($id = null) {
                        
                        $this->id       = $id;
                        $this->elements = (object)[];
                        $this->table    = 'rwPubgoldJournalIssue';
                        
                        if ($id) $this->load($id);
                }
        
                /**
                 * @param $jrn_id
                 * @param $vol_id
                 * @param array $elements
                 * @throws \Exception
                 */
                public function create($jrn_id, $vol_id, $elements = []) {
                        
                        if (array_key_exists('number', $elements)) $this->id = \Drupal::database()->select($this->table, 't')->fields('t', ['id'])->condition('jrn_id', $jrn_id, '=')->condition('vol_id', $vol_id, '=')->condition('number', $elements['number'], '=')->execute()->fetchField();
                        
                        if (!$this->id) $this->id = \Drupal::database()->insert($this->table)->fields(['jrn_id' => $jrn_id, 'vol_id' => $vol_id])->execute();
                        
                        $this->load($this->id);
                        
                        foreach ($elements as $k => $v) {
                                $this->setElement($k, $v, false);
                        }
                        
                        $this->save();
                        $this->load();
                }
        
                /**
                 * @param null $id
                 */
                public function load($id = null) {
                        
                        if ($this->id) $id = $this->id;
                        $res = \Drupal::database()->select($this->table, 't')->fields('t', [])->condition('id', $id, '=')->execute()->fetchAssoc();
                        
                        if(is_array($res)) {
                                foreach ($res as $k => $v) {
                                        if ($k != 'id') $this->elements->$k = $v;
                                }
                        }
                        
                        $this->id = $id;
                        
                        $this->loadArticles();
                }
                
                private function loadArticles() {
                        $this->articles = [];
                        $this->articles = \Drupal::database()->select('rwPubgoldJournalArticles', 't')->fields('t', ['jrna_id'])->condition('jrna_issue', $this->id, '=')->orderBy('jrna_id', 'ASC')->execute()->fetchCol();
                }
                
                public function getArticles() {
                        return $this->articles;
                }
        
                /**
                 * @return bool
                 */
                public function hasArticles() {
                        return !!count($this->articles);
                }
        
                /**
                 * @param $jrna_id
                 * @return Journalarticle
                 */
                public function getArticle($jrna_id) {
                        return new \Drupal\publisso_gold\Controller\Journalarticle($jrna_id);
                }
        
                /**
                 * @param $jrna_id
                 * @return bool
                 */
                public function hasArticle($jrna_id) {
                        return in_array($jrna_id, $this->articles);
                }
        
                /**
                 * @param $name
                 * @param $value
                 * @param bool $autosave
                 */
                public function setElement($name, $value, $autosave = true) {
                        
                        if (property_exists($this->elements, $name)) $this->elements->$name = $value;
                        
                        if ($autosave) $this->save();
                }
                
                private function save() {
                        \Drupal::database()->update($this->table)->fields((array)$this->elements)->condition('id', $this->id, '=')->execute();
                }
        
                /**
                 * @param $name
                 * @return false|mixed|null
                 */
                public function getElement($name) {
                        if ($name == 'id') return $this->id;
                        if (property_exists($this->elements, $name)) return $this->elements->$name;
                        return false;
                }
        
                /**
                 * @param $id
                 */
                public function delete($id) {
                        
                        \Drupal::database()->delete($this->table)->condition('id', $id, '=')->execute();
                }
        
                /**
                 * @return mixed|null
                 */
                public function getId() {
                        return $this->id;
                }
        
                /**
                 * @return array
                 */
                public function getKeys() {
                        return get_object_vars($this->elements);
                }
        }

?>
