<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\Reference.
 */

namespace Drupal\publisso_gold\Controller;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Zend\Feed\Uri;
        
        /**
         * Class Reference
         * @package Drupal\publisso_gold\Controller
         */
        class Reference {
    
        private $id;
        private $elements;
        private $authors;
        private $editors;
        private $items;
        private $sources;
        
        private $authorsLoaded;
        private $itemsLoaded;
        private $editorsLoaded;
        private $sourcesLoaded;
        
        const TABLE             = 'rwPubgoldReferences';
        const TABLE_ITEMS       = 'rwPubgoldReferences_Items';
        const TABLE_SOURCES     = 'rwPubgoldReferences_Sources';
        const TABLE_EDITORS     = 'rwPubgoldReferences_Editors';
        const TABLE_AUTHORS     = 'rwPubgoldReferences_Authors';
        
        /**
         * @return string
         */
        public function __toString(){
                return "class reference";
        }
        
        /**
         * Reference constructor.
         * @param null $id
         */
        public function __construct($id = null){
            
                $this->id = $id;
                $this->elements = (object)[];
                $this->authors  = (array) [];
                $this->editors  = (array) [];
                $this->items    = (object)[];
                $this->sources  = (array) [];
                
                $this->authorsLoaded = $this->editorsLoaded = $this->sourcesLoaded = $this->itemsLoaded = false;
                
                if($id) $this->load($id);
        }
        
        /**
         * @param $reference
         * @return \Drupal\Core\Database\StatementInterface|int|string|null
         * @throws \Exception
         */
        public function create($reference){
                
                //if exists reference
                $refHash = md5($reference);
                $refName = "REF-".time()."-MD5:$refHash";
                $qry = \Drupal::database()->select(self::TABLE, 't')->fields('t', ['id'])->condition('hash', $refHash, '=');
                
                if($qry->countQuery()->execute()->fetchField()){
                        
                        $this->id = $qry->execute()->fetchField();
                        Publisso::log('Reference exists!' . "[$this->id || $refHash]");
                }
                else{
                        Publisso::log('Reference does not exist!');
                        $this->id = \Drupal::database()->insert(self::TABLE)->fields(['name' => $refName, 'reference' => $reference, 'hash' => $refHash])->execute();
                }
                
                $this->load($this->id);
                return $this->id;
        }
        
        /**
         * @param $name
         * @return Reference
         */
        public function fromName($name){
                
                $id = \Drupal::database()->select(self::TABLE, 't')->fields('t', ['id'])->condition('name', $name, '=')->execute()->fetchField();
                
                if($id){
                        return new self($id);
                }
                
                error_log('Failed to load reference by name "'.$name.'"');
                return new self();
        }
        
        /**
         * @param $hash
         * @return Reference
         */
        public function fromHash($hash){
        
                $id = \Drupal::database()->select(self::TABLE, 't')->fields('t', ['id'])->condition('hash', $hash, '=')->execute()->fetchField();
                
                if($id){
                        return new self($id);
                }
                
                error_log('Failed to load reference by hash "'.$hash.'"');
                return new self();
        }
        
        /**
         * @param $id
         */
        public function load($id){
                
                $res = \Drupal::database()->select(self::TABLE, 't')->fields('t', [])->condition('id', $id, '=')->execute()->fetchAssoc();
                
                foreach($res as $k => $v){
                        if($k != 'id') $this->elements->$k = $v;
                }
                
                foreach(['authors', 'items', 'editors', 'sources'] as $_){
                        
                        if($this->{$_.'Loaded'} === false){
                                
                                $table = constant("self::TABLE_".strtoupper($_));

                                $res = \Drupal::database()->select($table, 't')->fields('t', [])->condition('reference_id', $id, '=')->execute()->fetchAll();
                                
                                foreach($res as $__){
                                        
                                        if(is_array($this->$_)){
                                                $this->$_[] = (array)$__;
                                        }
                                        
                                        if(is_object($this->$_)){
                                                $this->$_ = $__;
                                        }
                                }
                                
                                $this->{$_.'Loaded'} = true;
                        }
                }
        }
        
        /**
         * @param $element
         * @return false|mixed|null
         */
        public function getElement($element){
                if($element == 'id') return $this->id;
                if(property_exists($this->elements, $element)) return $this->elements->$element;
                return false;
        }
        
        /**
         * @param $id
         */
        public function delete($id){
                
                \Drupal::database()->delete($this->table)->condition('id', $id, '=')->execute();
                \Drupal::database()->delete('rwPubgoldReferences_Authors')->condition('reference_id', $id, '=')->execute();
                \Drupal::database()->delete('rwPubgoldReferences_Editors')->condition('reference_id', $id, '=')->execute();
                \Drupal::database()->delete('rwPubgoldReferences_Sources')->condition('reference_id', $id, '=')->execute();
                \Drupal::database()->delete('rwPubgoldReferences_Items')->condition('reference_id', $id, '=')->execute();
        }
        
        /**
         * @return mixed|null
         */
        public function getId(){
                return $this->id;
        }
        
        /**
         * @param $reference
         */
        public function tempReference($reference){
                
                $this->id = 0;
                $refHash = md5($reference);
                $refName = "REF-".time()."-MD5:$refHash";
                $this->elements->name = $refName;
                $this->elements->hash = $refHash;
                $this->elements->reference = $reference;
        }
        
        /**
         * @return \Drupal\Core\GeneratedLink|false|mixed
         */
        public function getDOILinkHTML(){
                
                if(($link = $this->getDOILink()) !== false){
                        
                        $uri = new Uri($link);
                        
                        if($uri->isValid()){
                                $link = new Link(
                                        $this->getDOI(),
                                        Url::fromUri($link, ['attributes' => ['target' => '_blank']])
                                );
                                $link = $link->toString();
                        }
                        else{
                                $link = $this->getDOI();
                        }
                        
                        return $link;
                }
                
                return false;
        }
        
        /**
         * @return false|string
         */
        public function getDOILink(){
                
                if(($doi = $this->getDOI()) !== false){
                        return "https://dx.doi.org/$doi";
                }
                
                return false;
        }
        
        /**
         * @return false|mixed
         */
        public function getDOI(){
                
                if(preg_match('/doi:(.+)$/i', $this->getElement('reference'), $matches)){
                        return $matches[1];
                }
                
                return false;
        }
        
        public function setDOILinkHTML(){
                $this->elements->reference = str_replace($this->getDOI(), $this->getDOILinkHTML(), $this->elements->reference);
        }
        
        /**
         * @param string $target
         */
        public function setLinkHTML($target = '_blank'){
                
                if(property_exists($this->items, 'link')){
                        
                        if($this->items->link !== null){
                                $this->elements->reference = str_replace($this->items->link, '<a target="'.$target.'" href="'.$this->items->link.'">'.$this->items->link.'</a>', $this->elements->reference);
                        }
                }
        }
        
        public function setAvailableFromLinkHTML(){
                
                if(preg_match('/Available from:\s(.+)$/', $this->getElement('reference'), $matches) && !empty($this->getItem('link'))){
                        $this->elements->reference = str_replace($matches[0], 'Available from: <a target="_blank" href="'.$matches[1].'">'.$matches[1].'</a>', $this->elements->reference);
                }
                
                
        }
        
        //handle the authors
        private function loadAuthors(){
        	$this->authors = \Drupal::database()->select('rwPubgoldReferences_Authors', 't')->fields('t', [])->condition('reference_id', $this->id, '=')->execute()->fetchAll();
        	$this->authorsLoaded = true;
        }
        
        /**
         * @return array
         */
        public function getAuthors(){
        	
        	if(!$this->authorsLoaded) $this->loadAuthors();
		return $this->authors;
        }
        
        /**
         * @return int|void
         */
        public function countAuthors(){
        	if(!$this->authorsLoaded) $this->loadAuthors();
        	return count((array)$this->authors);
        }
        
        /**
         * @param int $index
         * @return mixed|null
         */
        public function getAuthor(int $index){
        	return array_key_exists($index, ((array)$this->authors)) ? ((array)$this->authors)[$index] : null;
        }
        
        /**
         * @param $index
         * @return bool
         */
        public function removeAuthor($index){
        	
        	$author = $this->getAuthor($index);
        	
        	$ret = false;
        	
        	if($author)
        		$ret = !!\Drupal::database()->delete('rwPubgoldReferences_Authors')->condition('id', $author->id, '=')->execute();
        	
        	$this->loadAuthors();
        	return $ret;
        }
        
        /**
         * @param null $fname
         * @param null $lname
         * @param null $initials
         * @return bool
         * @throws \Exception
         */
        public function addAuthor($fname = null, $lname = null, $initials = null){
        	
        	if(!$this->id) return false;
        	
        	if(empty($fname)) $fname = null; if(empty($lname)) $lname = null; if(empty($initials)) $initials = null;
        	
        	if(!$lname){
        		\Drupal::service('messenger')->addError('Can\'t add reference-author for reference #'.$this->id.'! You have to provide at least lastname!');
        		return false;
        	}
        
        	if(!$initials && $fname){
        		
        		$names = array_filter(preg_split('/([\\s-]+)/', $fname, -1));
        		$ret = '';
        		for($i = 0; $i < 2; $i++){
        			$ret .= strtoupper(substr($names[$i], 0, 1));
        		}
        		$initials = $ret ? $ret : null;
        	}
        	
        	$ret =  !!\Drupal::database()->insert('rwPubgoldReferences_Authors')->fields([
        		'reference_id' => $this->id,
        		'fname' => $fname,
        		'lname' => $lname,
        		'initials' => $initials
        	])->execute();
        	
        	$this->loadAuthors();
        	return $ret;
        }
        
        //handle the editors
        private function loadEditors(){
        	$this->editors = \Drupal::database()->select('rwPubgoldReferences_Editors', 't')->fields('t', [])->condition('reference_id', $this->id, '=')->execute()->fetchAll();
        	$this->editorsLoaded = true;
        }
        
        /**
         * @return array
         */
        public function getEditors(){
        	
        	if(!$this->editorsLoaded) $this->loadEditors();
        	return $this->editors;
        }
        
        /**
         * @return int|void
         */
        public function countEditors(){
        	if(!$this->editorsLoaded) $this->loadEditors();
        	return count((array)$this->editors);
        }
        
        /**
         * @param int $index
         * @return mixed|null
         */
        public function getEditor(int $index){
        	return array_key_exists($index, ((array)$this->editors)) ? ((array)$this->editors)[$index] : null;
        }
        
        /**
         * @param $index
         * @return bool
         */
        public function removeEditor($index){
        	
        	$editor = $this->getEditor($index);
        	
        	$ret = false;
        	
        	if($editor)
        		$ret = !!\Drupal::database()->delete('rwPubgoldReferences_Editors')->condition('id', $editor->id, '=')->execute();
        		
        		$this->loadEditors();
        		return $ret;
        }
        
        /**
         * @param null $fname
         * @param null $lname
         * @param null $initials
         * @return bool
         * @throws \Exception
         */
        public function addEditor($fname = null, $lname = null, $initials = null){
        	
        	if(!$this->id) return false;
        	
        	if(empty($fname)) $fname = null; if(empty($lname)) $lname = null; if(empty($initials)) $initials = null;
        	
        	if(!$lname && ($fname || $initials)){
        		\Drupal::service('messenger')->addError('Can\'t add reference-editor! You have to provide at least lastname and firstname(s) or firstname-initial(s)!');
        		return false;
        	}
        	
        	if(!$initials){
        		
        		$names = array_filter(preg_split('/([\\s-]+)/', $fname, -1));
        		$ret = '';
        		for($i = 0; $i < 2; $i++){
        			$ret .= strtoupper(substr($names[$i], 0, 1));
        		}
        		$initials = $ret ? $ret : null;
        	}
        	
        	$ret =  !!\Drupal::database()->insert('rwPubgoldReferences_Editors')->fields([
        		'reference_id' => $this->id,
        		'fname' => $fname,
        		'lname' => $lname,
        		'initials' => $initials
        	])->execute();
        	
        	$this->loadEditors();
        	return $ret;
        }
        
        //handle the source
        private function loadSources(){
        	$this->sources = \Drupal::database()->select('rwPubgoldReferences_Sources', 't')->fields('t', [])->condition('reference_id', $this->id, '=')->execute()->fetchAll();
        	$this->sourcesLoaded = true;
        }
        
        /**
         * @return array
         */
        public function getSources(){
        	
        	if(!$this->sourcesLoaded) $this->loadSources();
        	return $this->sources;
        }
        
        /**
         * @return int|void
         */
        public function countSources(){
        	if(!$this->sourcesLoaded) $this->loadSources();
        	return count((array)$this->sources);
        }
        
        /**
         * @param int $index
         * @return mixed|null
         */
        public function getSource(int $index){
        	return array_key_exists($index, ((array)$this->sources)) ? ((array)$this->sources)[$index] : null;
        }
        
        /**
         * @param $index
         * @return bool
         */
        public function removeSource($index){
        	
        	$source = $this->getSource($index);
        	
        	$ret = false;
        	
        	if($source)
        		$ret = !!\Drupal::database()->delete('rwPubgoldReferences_Sources')->condition('id', $source->id, '=')->execute();
        		
        		$this->loadSources();
        		return $ret;
        }
        
        /**
         * @param string $source
         * @return bool
         * @throws \Exception
         */
        public function addSource(string $source){
        	
        	if(!$this->id) return false;
        	
        	if(!$source){
        		\Drupal::service('messenger')->addError('Can\'t add empty reference-source!');
        		return false;
        	}
        	
        	$ret =  !!\Drupal::database()->insert('rwPubgoldReferences_Sources')->fields([
        		'reference_id' => $this->id,
        		'source' => $source
        	])->execute();
        	
        	$this->loadSources();
        	return $ret;
        }
        
        //handle the items
        private function loadItems(){
        	
        	$qry = \Drupal::database()->select('rwPubgoldReferences_Items', 't')->fields('t', [])->condition('reference_id', $this->id, '=');
        	
        	if($qry->countQuery()->execute()->fetchField() > 0){
        		
        		foreach($qry->execute()->fetchAssoc() as $k => $v){
        			if($v != null) $this->items->$k = $v;
        		}
        	}
        	
        	$this->itemsLoaded = true;
        }
        
        /**
         * @return bool
         */
        private function hasItemSet(){
        	return !!\Drupal::database()->select('rwPubgoldReferences_Items', 't')->fields('t', [])->condition('reference_id', $this->id, '=')->countQuery()->execute()->fetchField();
        }
        
        /**
         * @return object
         */
        public function getItems(){
        	
        	if(!$this->itemsLoaded) $this->loadItems();
        	return $this->items;
        }
        
        /**
         * @param $item
         * @return bool
         */
        public function hasItem($item){
        	if(!$this->itemsLoaded) $this->loadItems();
        	return (property_exists($this->items, $item));
        }
        
        /**
         * @param $item
         * @return |null
         */
        public function getItem($item){
        	if(!$this->itemsLoaded) $this->loadItems();
        	if(property_exists($this->items, $item)) return $this->items->$item;
        	return null;
        }
        
        /**
         * @param $item
         * @param $value
         * @return bool
         * @throws \Exception
         */
        public function setItem($item, $value){
        	
        	$field = [$item => $value];
        	$db = \Drupal::database();
        	
        	if($this->hasItemSet()){
        		
        		try{
        			$ret = !!$db->update('rwPubgoldReferences_Items')->fields($field)->condition('reference_id', $this->id, '=')->execute();
        		}
        		catch(\Drupal\Core\Database\DatabaseException $e){
        			
        			error_log($e->getMessage());
        			\Drupal::service('messenger')->addError("Can't set value \"$value\" for reference-item \"$item\" for reference#$this->id. See error-log for details.");
        			return false;
        		}
        	}
        	else{
        		try{
        			$ret = !!$db->insert('rwPubgoldReferences_Items')->fields(array_merge(['reference_id' => $this->id], $field))->execute();
        		}
        		catch(\Drupal\Core\Database\DatabaseException $e){
        			
        			\Drupal::service('messenger')->addError("Can't set value \"$value\" for reference-item \"$item\" for reference#$this->id. See error-log for details.");
        			error_log($e->getMessage());
        			return false;
        		}
        	}
        	
        	$this->loadItems();
        	return $ret;
        }
}

?>
