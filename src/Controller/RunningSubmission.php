<?php
        
        /**
         * @file
         * Contains \Drupal\publisso_gold\Controller\RunningSubmission.
         */
        
        namespace Drupal\publisso_gold\Controller;
        
        /**
         * Class RunningSubmission
         * @package Drupal\publisso_gold\Controller
         */
        class RunningSubmission {
                
                private $id;
                private $elements          = [];
                private $data              = [];
                private $tbl_column_prefix = '';
                private $table;
        
                /**
                 * @return string
                 */
                public function __toString () {
                        return "class RunningSubmission";
                }
        
                /**
                 * RunningSubmission constructor.
                 * @param null $id
                 * @param null $type
                 */
                public function __construct ($id = null, $type = null ) {
                        
                        $this->id = $id;
                        
                        if ( !$type ) {
                                $this->table = 'rwPubgoldBookchapter_temp';
                        }
                        else {
                                $this->table = 'rwPubgoldWorkflowTemp';
                                $this->elements[ 'type' ] = $type;
                        }
                        
                        $this->load( $this->id );
                }
        
                /**
                 * @param $elemName
                 * @param null $elemValue
                 * @param bool $storeOnDB
                 * @return bool
                 */
                public function setElement ($elemName, $elemValue = null, $storeOnDB = true ) {
                        
                        $oldValue = $this->elements[ $elemName ];
                        $this->elements[ $elemName ] = $elemValue;
                        
                        if ( $storeOnDB === false )
                                return true;
                        
                        if ( $this->save( $elemName ) === false ) {
                                $this->elements[ $elemName ] = $oldValue;
                                return false;
                        }
                        else {
                                return true;
                        }
                }
        
                /**
                 * @param $creator_uid
                 * @param null $type
                 * @throws \Exception
                 */
                public function create ($creator_uid, $type = null ) {
                        
                        $fields = [];
                        $fields[ $this->tbl_column_prefix . 'uid' ] = $creator_uid;
                        
                        if ( $type ) $fields[ $this->tbl_column_prefix . 'type' ] = $type;
                        
                        $this->id = \Drupal::database()
                                           ->insert( $this->table )
                                           ->fields( $fields )
                                           ->execute()
                        ;
                        
                        $this->load( $this->id );
                }
        
                /**
                 * @return mixed|null
                 */
                public function getId () {
                        return $this->id;
                }
        
                /**
                 * @param $elemName
                 * @return mixed|null
                 */
                public function getElement ($elemName ) {
                        if ( $elemName == 'id' ) return $this->id;
                        return $this->elements[ $elemName ] ?? null;
                }
        
                /**
                 * @param null $elemName
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 */
                public function save ($elemName = null ) {
                        
                        $fields = $this->elements;
                        
                        if ( $elemName !== null ) { //if specific element selected
                                $fields = [ $elemName => $fields[ $elemName ] ];
                        }
                        
                        return \Drupal::database()->update( $this->table )
                                      ->fields( $fields )
                                      ->condition( 'id', $this->id )
                                      ->execute()
                                ;
                }
                
                private function load () {
                        
                        if ( $this->id ) {
                                
                                $items = \Drupal::database()->select( $this->table, 'u' )
                                                ->fields( 'u', [] )
                                                ->condition( 'id', $this->id, '=' )
                                                ->execute()
                                                ->fetchAssoc()
                                ;
                                
                                foreach ( $items as $k => $v ) {
                                        
                                        if ( $k != 'id' ) //store except id
                                                $this->elements[ $k ] = $v;
                                }
                                
                                $this->data = json_decode( base64_decode( $this->getElement( 'data' ) ), true );
                        }
                }
        
                /**
                 * @param $elemName
                 * @param $elemValue
                 * @param bool $storeOnDB
                 * @param false $debug
                 */
                public function setDataElement ($elemName, $elemValue, $storeOnDB = true, $debug = false ) {
                        $this->data[ $elemName ] = $elemValue;
                        $this->setElement( 'data', base64_encode( json_encode( $this->data ) ), $storeOnDB );
                }
        
                /**
                 * @param $elemName
                 * @return mixed|null
                 */
                public function getDataElement ($elemName ) {
                        return $this->data[ $elemName ] ?? null;
                }
        
                /**
                 * @return array
                 */
                public function getDataKeys () {
                        return array_keys( $this->data );
                }
        }

?>
