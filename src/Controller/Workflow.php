<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\Workflow.
 */

namespace Drupal\publisso_gold\Controller;

use DOMElement;
use DOMXpath;
use \Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Database\StatementInterface;
use \Drupal\Core\Url;
use \Drupal\publisso_gold\Controller\WorkflowInfoMail;
use Exception;

/**
         * Class workflow
         * @package Drupal\ publisso_gold\Controller
         */
        class workflow {

        public $medium;
        private $id;
        private $tbl_column_prefix = 'wf_';
        private $elements = [];
        private $data;
        private $reviewers = [];
        private $editors_in_chief = [];
        private $editors = [];
        private $editorial_office = [];
        private $history = [];
        private $db_table;
        private $reviewpages = [];
        private $session;
        private $stateChain;

        /**
         * workflow constructor.
         * @param null $id
         * @param string $tbl_column_prefix
         * @param bool $loadMedium
         */
        public
        function __construct($id = null, $tbl_column_prefix = 'wf_', $loadMedium = true) {

                $this->session = \Drupal::service('session');
                $this->medium = null;
                $this->stateChain = null;

                if ($id !== null) {

                        $this->tbl_column_prefix = $tbl_column_prefix;

                        if ($tbl_column_prefix == 'wf_')
                                $this->db_table = 'rwPubgoldWorkflow';
                        elseif ($tbl_column_prefix == 'wfh_')
                                $this->db_table = 'rwPubgoldWorkflowHistory';

                        $this->id = $id;
                        $this->load($this->id);
                        //if($loadMedium) $this->loadMedium();
                } else {
                        $this->db_table = 'rwPubgoldWorkflow';
                }
        }

        private
        function load() {

                if ($this->id) {

                        $book = \Drupal::database()->select($this->db_table, 'b')->fields('b', [])->condition($this->tbl_column_prefix .
                                'id', $this->id, '=')->execute()->fetchAssoc();

                        foreach ($book as $k => $v) {

                                if ($k != 'wf_id') //store except id
                                        $this->elements[str_replace($this->tbl_column_prefix, '', $k)] = $v;
                        }

                        $this->data = json_decode(base64_decode($this->getElement('data')));
                        $this->setReviewers();
                        $this->setEditorsInChief();
                        $this->setEditors();
                        $this->setEditorialOffice();
                        $this->loadReviewpages();
                        $this->loadHistory();

                } else {
                        $this->data = (object)[];
                }
        }

        /**
         * @param $elemName
         * @return mixed
         */
        public
        function getElement($elemName) {
                if ($elemName == 'id') return $this->id;
                return $this->elements[$elemName];
        }

        public
        function setReviewers() {

                $this->reviewers = [];

                foreach (array_filter(explode(',', $this->getElement('assigned_to_reviewer'))) as $uid) {
                        $this->reviewers[] = new User($uid);
                }
        }

        public
        function setEditorsInChief() {

                $this->editors_in_chief = [];

                foreach (array_filter(explode(',', $this->getElement('assigned_to_eic'))) as $uid) {
                        $this->editors_in_chief[] = new User($uid);
                }
        }

        public
        function setEditors() {

                $this->editors = [];

                foreach (array_filter(explode(',', $this->getElement('assigned_to_editor'))) as $uid) {
                        $this->editors[] = new User($uid);
                }
        }

        public
        function setEditorialOffice() {

                $this->editorial_office = [];

                foreach (array_filter(explode(',', $this->getElement('assigned_to_eo'))) as $uid) {
                        $this->editorial_office[] = new User($uid);
                }
        }

        private
        function loadReviewpages() {

                $this->review_pages = [];

                $pages = json_decode(base64_decode($this->getDataElement('review_pages')), true);

                if (is_array($pages)) {

                        foreach ($pages as $uid => $reviewpages) {

                                $this->reviewpages[$uid] = [];

                                foreach ($reviewpages as $reviewpage) {
                                        $this->review_pages[$uid][] = json_decode(base64_decode($reviewpage), true);
                                }
                        }
                }
        }

        /**
         * @param $elemName
         * @return false|string|null
         */
        public
        function getDataElement($elemName) {

                if ($elemName == ':orig_text') { //like in CSS ":" identicate pseudo-element

                        switch (substr($this->getElement('type'), 0, 1)) {
                                case 'b':
                                        return base64_decode($this->getDataElement('chapter_text'));
                                        break;

                                case 'j':
                                        return base64_decode($this->getDataElement('article_text'));
                                        break;

                                case 'c':
                                        return base64_decode($this->getDataElement('paper_text'));
                                        break;
                        }
                }

                return $this->data->$elemName ?? null;
        }

        /**
         * @return null
         */
        private
        function loadHistory() {

                if (!$this->id) return null;

                $this->history = [];

                foreach (\Drupal::database()->select('rwPubgoldWorkflowHistory', 't')->fields('t', ['wfh_id'])->condition('wfh_wfid', $this->id, '=')->execute()->fetchAll() as $_) {
                        $this->history[] = $_->wfh_id;
                }
        }

        /**
         * @return string
         */
        public
        function __toString() {
                return "class workflow";
        }

        /**
         * @param null $creator_uid
         * @param array $fields
         * @return StatementInterface|int|string|null
         * @throws Exception
         */
        public
        function create($creator_uid = null, array $fields = []) {

                if (!$creator_uid) $creator_uid = $this->session->get('user')['id'];

                $fields['created_by_uid'] = $creator_uid;

                foreach ($fields as $k => $v) {
                        $fields[$this->tbl_column_prefix . $k] = $v;
                        unset($fields[$k]);
                }

                $this->id = \Drupal::database()->insert($this->db_table)->fields($fields)->execute();

                $this->load($this->id);
                return $this->id;
        }

        /**
         * @param null $uid
         * @return array|mixed
         */
        public
        function getReviewpages($uid = null) {

                if ($uid === null) return $this->review_pages;
                elseif (array_key_exists($uid, $this->review_pages)) return $this->review_pages[$uid];
                else return [];
        }

        /**
         * @param $uid
         * @return bool
         */
        public
        function getLock($uid) {

                if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];

                if ($this->id && $uid) {

                        //try to lock data
                        $sql = "
                update
                                {" . $this->db_table .
                                "}
                set
                        wf_locked = case when wf_locked_by_uid is null then 1 else wf_locked end,
                        wf_locked_by_uid = case when wf_locked_by_uid is null then :uid else wf_locked_by_uid end
                where
                        wf_id = :wf_id
                ";

                        $data = [
                                ':wf_id' => $this->getElement('id'),
                                ':uid' => $uid
                        ];

                        \Drupal::database()->query($sql, $data);
                        $this->load();

                        return $this->getElement('locked_by_uid') == $uid;
                } else {
                        return false;
                }
        }

        /**
         * @return bool
         */
        public
        function locked() {
                return !!$this->getElement('locked');
        }

        /**
         * @param false $force
         * @return bool
         * @throws Exception
         */
        public
        function unlock($force = false) {

                if ($this->getElement('locked_by_uid') != $this->session->get('user')['id'] && $force === false) {
                        return false;
                }

                $this->setElement('locked', null);
                $this->setElement('locked_by_uid', null);
                return true;
        }

        /**
         * @param $elemName
         * @param null $elemValue
         * @param bool $storeOnDB
         * @param false $selfcall
         * @return bool
         * @throws Exception
         */
        public
        function setElement($elemName, $elemValue = null, $storeOnDB = true, $selfcall = false) {

                if (array_key_exists($elemName, $this->elements)) {

                        $oldValue = $this->elements[$elemName];
                        $this->elements[$elemName] = $elemValue;

                        if (!$selfcall) {
                                if ($elemName == 'state') {

                                        $this->newStateChainElement($elemValue);
                                        $this->setDataElement($elemName, $elemValue, $storeOnDB, true); //um die Abwärtskompatibilität zu wahren wird der Status noch bei den Daten gespeichert
                                }
                        }

                        if ($storeOnDB === false)
                                return true;

                        if ($this->save($elemName) === false) {
                                $this->elements[$elemName] = $oldValue;
                                return false;
                        } else {
                                return true;
                        }
                } else {
                        return false;
                }
        }

        /**
         * @param string $state_new
         * @param string|null $state_old
         * @throws Exception
         */
        public function newStateChainElement(string $state_new, string $state_old = null) {

                if (\Drupal::service('session')->get('logged_in') === true) {

                        $uid = \Drupal::service('session')->get('user')['id'];

                        if (!$state_old && !$this->getCurrentState())
                                $state_old = 'in submission';

                        if ($state_new != $state_old) {
                                $data = [
                                        'uid' => $uid,
                                        'state_old' => $state_old,
                                        'state_new' => $state_new,
                                        'wf_id' => $this->id
                                ];

                                \Drupal::database()->insert('rwPubgoldWorkflowStateChain')->fields($data)->execute();
                        }
                }
        }

        /**
         * @return mixed
         */
        public function getCurrentState() {

                if ($this->stateChain === null) {
                        $this->loadStateChain();
                }

                return end($this->stateChain)->state_new;
        }

        private function loadStateChain() {

                $this->stateChain = [];

                $qry = \Drupal::database()->select('rwPubgoldWorkflowStateChain', 't')->fields('t', [])->condition
                ('wf_id', $this->id, "=");

                if ($qry->countQuery()->execute()->fetchField()) {
                        $this->stateChain = $qry->execute()->fetchAll();
                }
        }

        /**
         * @param $elemName
         * @param $elemValue
         * @param bool $storeOnDB
         * @param false $selfcall
         * @throws Exception
         */
        public
        function setDataElement($elemName, $elemValue, $storeOnDB = true, $selfcall = false) {

                $oldValue = $this->data->$elemName ?? null;
                $this->data->$elemName = $elemValue;

                if (!$selfcall) {
                        if ($elemName == 'state') {
                                $this->setElement($elemName, $elemValue, $storeOnDB, true);

                                if ($oldValue != $elemName) {
                                        $fields = [
                                                'uid' => \Drupal::service('session')->get('user')['id'],
                                                'wf_id' => $this->getElement('id'),
                                                'state_old' => $oldValue,
                                                'state_new' => $elemValue
                                        ];

                                        \Drupal::database()->insert('rwPubgoldWorkflowStateChain')->fields($fields)->execute();
                                }
                        }
                }

                $this->setElement('data', base64_encode(json_encode($this->data)), $storeOnDB);
        }

        /**
         * @param null $elemName
         * @return StatementInterface|int|string|null
         */
        public
        function save($elemName = null) {

                if ($this->id) {
                        $uid = null;
                        if (\Drupal::service('session')->has('user')) $this->elements['uid_last_modified'] = \Drupal::service('session')->get('user')['id'];

                        $fields = $this->elements;

                        if ($elemName !== null) { //if specific element selected
                                $fields = [$elemName => $fields[$elemName]];
                        }

                        foreach ($fields as $k => $var) {
                                $fields[$this->tbl_column_prefix . $k] = $var;
                                unset($fields[$k]);
                        }

                        return Drupal::database()->update($this->db_table)->fields($fields)->condition($this->tbl_column_prefix .
                                'id', $this->id)->execute();
                }
        }

        /**
         * @return array
         */
        public
        function getKeys() {
                return array_keys((array)$this->elements);
        }

        /**
         * @param $uid
         * @return bool
         */
        public
        function isEditorInChief($uid) {

                foreach ($this->editors_in_chief as $user) {
                        if ($user->getElement('id') == $uid) return true;
                }

                return false;
        }

        /**
         * @param $uid
         * @return bool
         */
        public
        function isEditorialOffice($uid) {

                foreach ($this->editorial_office as $user) {
                        if ($user->getElement('id') == $uid) return true;
                }

                return false;
        }

        /**
         * @param $uid
         * @return bool
         */
        public
        function isEditor($uid) {

                foreach ($this->editors as $user) {
                        if ($user->getElement('id') == $uid) return true;
                }

                return false;
        }

        public
        function reload() {
                $this->load();
                $this->loadMedium();
        }

        private
        function loadMedium() {

                if (!$this->medium) $this->medium = $this->getParentMedium();
        }

        /**
         * @return Book|Conference|Journal
         */
        public
        function getParentMedium() {

                switch ($this->getDataElement('type')) {

                        case 'bookchapter':
                                return new Book($this->getDataElement('bk_id'), true);
                                break;

                        case 'journalarticle':
                                return new Journal($this->getDataElement('jrn_id'), true);
                                break;

                        case 'conferencepaper':
                                return new Conference($this->getDataElement('cf_id'), true);
                                break;
                }
        }

        /**
         * @return array
         */
        public function readAllReviewerUIDs() {

                $uids = $this->readCurrentReviewerUIDs();

                foreach (\Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', ['uid'])->condition('wfid', $this->id, '=')->execute()->fetchCol() as $_) {
                        $uids[] = $_;
                }

                return array_map('trim', array_unique(array_filter($uids)));
        }

        /**
         * @return array
         */
        public function readCurrentReviewerUIDs() {

                $uids = [];

                foreach (explode(',', $this->getElement('assigned_to_reviewer')) as $_) {
                        $uids[] = $_;
                }

                return array_map('trim', array_unique(array_filter($uids)));
        }

        /**
         * @return StatementInterface|int|string|null
         * @throws Exception
         */
        public
        function historize() {

                if (!$this->id) return null;

                $tbl_column_prefix = 'wfh_';
                $fields = [];

                foreach ($this->elements as $k => $v) {
                        $fields[$tbl_column_prefix . $k] = $v;
                }

                $fields[$tbl_column_prefix .
                'wfid'] = $this->id;

                $wfh_id = \Drupal::database()->insert('rwPubgoldWorkflowHistory')->fields($fields)->execute();
                $this->loadHistory();
                return $wfh_id;
        }

        /**
         * @param int $id
         * @return workflow
         */
        public
        function History(int $id) {
                return new Workflow($id, 'wfh_');
        }

        /**
         * @return array
         */
        public function getHistory() {
                return $this->history;
        }

        /**
         * @return null
         */
        public
        function getMedium() {
                if (!$this->medium) $this->loadMedium();
                return $this->medium;
        }

        /**
         * @return bool
         * @throws Exception
         */
        function newStateEditorchangerequest() {

                $this->setDataElement('state', 'editor change request');
                $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('created_by_uid')))));
                $this->load();

                if ($this->getElement('type') == 'bcs') {

                        $mail = new WorkflowInfoMail($this, 'editor change request');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'editor change request');
                } elseif ($this->getElement('type') == 'bce') {

                        $mail = new WorkflowInfoMail($this, 'erratum editor change request');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'erratum editor change request');
                }

                return true;
        }

        /**
         * @return bool
         * @throws Exception
         */
        function newStateClearedforpublication() {

                $this->setDataElement('state', 'cleared for publication');

                //if workflow assigned to eo
                if (!empty($this->getElement('assigned_to_eo'))) {
                        $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('assigned_to_eo')))));
                } //redirect to book-eo
                else {
                        $eo = $this->medium->readEditorialOffice();
                        $ary = [];

                        foreach ($eo as $user) {
                                $ary[] = $user->getElement('id');
                        }

                        $this->setElement('assigned_to', 'u:' . implode(',u:', $ary));
                }


                $this->load();
                if ($this->getElement('type') == 'bcs') {

                        $mail = new WorkflowInfoMail($this, 'cleared for publication');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'cleared for publication');
                } elseif ($this->getElement('type') == 'bce') {

                        $mail = new WorkflowInfoMail($this, 'erratum cleared for publication');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'erratum cleared for publication');
                }
                return true;
        }

        /**
         * @return bool
         * @throws Exception
         */
        function newStateReadyforpublication() {

                $this->setDataElement('state', 'ready for publication');
                $this->setElement('assigned_to', 'r:2');

                $mail = new WorkflowInfoMail($this, 'ready for publication');
                if ($mail) $mail->send();
                //sendWorkflowInfoMail($this, 'ready for publication');
                return true;
        }

        /**
         * @param bool $sendInfoMail
         * @param false $preliminary
         * @return bool|StatementInterface|int|string|null
         * @throws Exception
         */
        public
        function publish($sendInfoMail = true, $preliminary = false) {

                \Drupal::database()->delete('rwPubgoldWorkflowStateChain')->condition('wf_id', $this->id, '=')->execute();

                switch ($this->getElement('type')) {

                        case 'bcs':
                                return $this->publishBookchapter($sendInfoMail, $preliminary);
                                break;

                        case 'jas':
                                return $this->publishJournalarticle($sendInfoMail, $preliminary);
                                break;

                        case 'cps':
                                return $this->publishConferencepaper($sendInfoMail, $preliminary);
                                break;

                        case 'bce':
                                return $this->publishErratum($sendInfoMail, $preliminary);
                                break;

                        default:
                                return false;
                }
        }

        /**
         * @param bool $sendInfoMail
         * @param false $preliminary
         * @return StatementInterface|false|int|string|null
         * @throws Exception
         */
        private
        function publishBookchapter($sendInfoMail = true, $preliminary = false) {

                if (!$this->id) return null;

                //create profile-archive to save author-data on publishing-time
                $authors = [];
                foreach (json_decode($this->getDataElement('authors'), true) as $_k => $_v) {

                        if ($_v['uid']) {

                                $author = new User($_v[uid]);
                                $profile = [];

                                foreach ($author->profile->getElementKeys() as $key) {
                                        $profile[$key] = $author->profile->getElement($key);
                                }

                                $profile = base64_encode(json_encode($profile));

                                $ary = $_v;

                                if (!$preliminary) {
                                        $ary['profile_archive_id'] = \Drupal::database()->insert('rwPubgoldUserProfiles_Archive')->fields([
                                                'uid' => $author->getElement('id'),
                                                'data' => $profile
                                        ])->execute();
                                }

                                $authors[] = $ary;
                        } else {
                                $authors[] = $_v;
                        }
                }

                $this->setDataElement('authors', json_encode($authors));
                // -- profile-archive

                //save chapter
                $data = [
                        'cp_abstract' => $this->getDataElement('abstract'),
                        'cp_author_uid' => $this->getElement('created_by_uid'),
                        'cp_bkid' => $this->getDataElement('bk_id'),
                        'cp_chapter_text' => $this->getDataElement('chapter_text'),
                        'cp_correction' => $this->getDataElement('correction'),
                        'cp_corresponding_author' => $this->getDataElement('corresponding_authors'),
                        'cp_ddc' => $this->getDataElement('ddc'),
                        'cp_doi' => $this->getDataElement('doi'),
                        'cp_errata' => $this->getDataElement('errata'),
                        'cp_funding_id' => $this->getDataElement('funding_id'),
                        'cp_funding_number' => $this->getDataElement('funding_number'),
                        'cp_funding_name' => $this->getDataElement('funding_name'),
                        'cp_keywords' => $this->getDataElement('keywords'),
                        'cp_license' => $this->getDataElement('license'),
                        'cp_publication_place' => $this->getDataElement('publication_place'),
                        'cp_publication_year' => $this->getDataElement('publication_year'),
                        'cp_publisher' => $this->getDataElement('publisher'),
                        'cp_title' => $this->getDataElement('title'),
                        'cp_urn' => $this->getDataElement('urn'),
                        'cp_version' => $this->getDataElement('version'),
                        'cp_patients_rights' => $this->getDataElement('patients_rights'),
                        'cp_author_contract' => $this->getDataElement('author_contract'),
                        'cp_conflict_of_interest' => $this->getDataElement('conflict_of_interest'),
                        'cp_conflict_of_interest_text' => $this->getDataElement('conflict_of_interest_text'),
                        'cp_corporation' => $this->getDataElement('corporations'),
                        'cp_sub_category' => $this->getDataElement('sub_category'),
                        'cp_more_authors' => $this->getDataElement('more_authors'),
                        'cp_wfid' => $this->getElement('id'),
                        'cp_authors' => $this->getDataElement('authors'),
                        'cp_policy_accept' => $this->getDataElement('accept_policy'),
                        'cp_published' => $this->getDataElement('published') ? $this->getDataElement('published') : date("Y-m-d"),
                        'cp_files' => $this->getDataElement('files') ? implode(',', unserialize($this->getDataElement('files'))) : null,
                        'cp_title_short' => $this->getDataElement('title_short'),
                        'cp_references' => null,
                        'cp_received' => !empty($this->getDataElement('received')) ? $this->getDataElement
                        ('received') : null,
                        'cp_revised' => !empty($this->getDataElement('revised')) ? $this->getDataElement('revised') :
                                                                                   null,
                        'cp_accepted' => !empty($this->getDataElement('accepted')) ? $this->getDataElement('accepted')
                                                                                     :
                                                                                     null
                ];

                if (!$preliminary) {
                        //any references?
                        if (!empty($this->getDataElement('references'))) {

                                $data['cp_chapter_text'] = base64_encode(\Drupal::service('publisso_gold.tools')->mapReferences(
                                        base64_decode($this->getDataElement('chapter_text')), $this->getDataElement('references')));
                        }
                }

                if (!$this->getDataElement('cp_id')) {
                        $cp_id = \Drupal::database()->insert('rwPubgoldBookChapters')->fields($data)->execute();
                        $this->setDataElement('cp_id', $cp_id);
                } else {
                        $cp_id = $this->getDataElement('cp_id');
                        error_log(print_r($data, 1));
                        \Drupal::database()->update('rwPubgoldBookChapters')->fields($data)->condition('cp_id', $cp_id, '=')->execute();
                }


                if (!$preliminary) {
                        $this->setDataElement('state', 'published');

                        $this->load();
                        if (!$this->medium) $this->loadMedium();

                        $vars = [
                                '::lnk.submission::' => Url::fromRoute('publisso_gold.book.chapter', ['bk_id' => $this->medium->getElement('id'), 'cp_id' => $cp_id])->setAbsolute()->toString()
                        ];

                        if ($sendInfoMail == true) {

                                $mail = new WorkflowInfoMail($this, 'publish submission');
                                $mail->addVars($vars);
                                if ($mail) $mail->send();

                                $mail = new WorkflowInfoMail($this, 'publication info eic');
                                $mail->addVars($vars);
                                if ($mail) $mail->send();
                                //sendWorkflowInfoMail($this, 'publish submission', $vars);
                                //sendWorkflowInfoMail($this, 'publication info eic', $vars);
                        }
                }

                if ($this->getElement('state') == 'published') {

                        $chapter = new Bookchapter($cp_id);
                        $chapter->setElement('citation_note', null);
                        $urlChapter = $chapter->getChapterLink('url');
                        $urlChapter->setAbsolute();
                        \Drupal::service('messenger')->addMessage(
                                new FormattableMarkup(
                                        (string)t('The chapter has been published successfully. You can see the chapter by visiting <a href="@link">@link</a>'),
                                        ['@link' => $urlChapter->toString()]
                                )
                        );
                }
                return $cp_id;
        }

        /**
         * @param bool $sendInfoMail
         * @return bool|null
         * @throws Exception
         */
        private
        function publishJournalArticle($sendInfoMail = true) {

                if (!$this->id) return null;

                //create profile-archive to save author-data on publishing-time
                $authors = [];
                foreach (json_decode($this->getDataElement('authors'), true) as $_k => $_v) {

                        if ($_v['uid']) {

                                $author = new User($_v[uid]);
                                $profile = [];

                                foreach ($author->profile->getElementKeys() as $key) {
                                        $profile[$key] = $author->profile->getElement($key);
                                }

                                $profile = base64_encode(json_encode($profile));

                                $ary = $_v;
                                $ary['profile_archive_id'] = \Drupal::database()->insert('rwPubgoldUserProfiles_Archive')->fields([
                                        'uid' => $author->getElement('id'),
                                        'data' => $profile
                                ])->execute();

                                $authors[] = $ary;
                        } else {
                                $authors[] = $_v;
                        }
                }

                $this->setDataElement('authors', json_encode($authors));
                // -- profile-archive

                //save chapter
                $data = [
                        'jrna_abstract' => $this->getDataElement('abstract'),
                        'jrna_author_uid' => $this->getElement('created_by_uid'),
                        'jrna_jrnid' => $this->getDataElement('jrn_id'),
                        'jrna_article_text' => $this->getDataElement('article_text'),
                        'jrna_article_type' => $this->getDataElement('article_type'),
                        'jrna_correction' => $this->getDataElement('correction'),
                        'jrna_corresponding_author' => $this->getDataElement('corresponding_authors'),
                        'jrna_ddc' => $this->getDataElement('ddc'),
                        'jrna_doi' => $this->getDataElement('doi'),
                        'jrna_funding_id' => $this->getDataElement('funding_id'),
                        'jrna_funding_name' => $this->getDataElement('funding_name'),
                        'jrna_volume' => $this->getDataElement('volume'),
                        'jrna_issue' => $this->getDataElement('issue'),
                        'jrna_keywords' => $this->getDataElement('keywords'),
                        'jrna_license' => $this->getDataElement('license'),
                        'jrna_publication_place' => $this->getDataElement('publication_place'),
                        'jrna_publication_year' => $this->getDataElement('publication_year'),
                        'jrna_publisher' => $this->getDataElement('publisher'),
                        'jrna_title' => $this->getDataElement('title'),
                        'jrna_urn' => $this->getDataElement('urn'),
                        'jrna_version' => $this->getDataElement('version'),
                        'jrna_patients_rights' => $this->getDataElement('patients_rights'),
                        'jrna_author_contract' => $this->getDataElement('author_contract'),
                        'jrna_conflict_of_interest' => $this->getDataElement('conflict_of_interest'),
                        'jrna_conflict_of_interest_text' => $this->getDataElement('conflict_of_interest_text'),
                        'jrna_corporation' => $this->getDataElement('corporations'),
                        'jrna_sub_category' => $this->getDataElement('sub_category'),
                        'jrna_more_authors' => $this->getDataElement('more_authors'),
                        'jrna_docno' => $this->getDataElement('docno'),
                        'jrna_wfid' => $this->getElement('id'),
                        'jrna_authors' => $this->getDataElement('authors'),
                        'jrna_policy_accept' => $this->getDataElement('accept_policy'),
                        'jrna_published' => date("Y-m-d"),
                        'jrna_files' => $this->getDataElement('files') ? implode(',', unserialize($this->getDataElement('files'))) : null,
                        'jrna_received' => !empty($this->getDataElement('received')) ? $this->getDataElement('received') : null,
                        'jrna_revised' => !empty($this->getDataElement('revised')) ? $this->getDataElement('revised') : null,
                        'jrna_accepted' => !empty($this->getDataElement('accepted')) ? $this->getDataElement('accepted') : null
                ];


                //any references?
                if (!empty($this->getDataElement('references'))) {

                        $data['jrna_article_text'] = base64_encode(\Drupal::service('publisso_gold.tools')->mapReferences(
                                base64_decode($this->getDataElement('article_text')), $this->getDataElement('references')));
                }

                $cp_id = \Drupal::database()->insert('rwPubgoldJournalArticles')->fields($data)->execute();


                $this->setDataElement('state', 'published');

                $this->load();
                if (!$this->medium) $this->loadMedium();
                $vars = [
                        '::lnk.submission::' => Url::fromRoute('publisso_gold.journals_journal', ['jrn_id' => $this->medium->getElement('id'), 'jrna_id' => $cp_id])->setAbsolute()->toString()
                ];

                if ($sendInfoMail == true) {

                        $mail = new WorkflowInfoMail($this, 'publish submission');
                        $mail->addVars($vars);
                        if ($mail) $mail->send();

                        $mail = new WorkflowInfoMail($this, 'publication info eic');
                        $mail->addVars($vars);
                        if ($mail) $mail->send();
                }
                return true;
        }

        /**
         * @param bool $sendInfoMail
         * @param false $preliminary
         * @return StatementInterface|false|int|string|null
         * @throws Exception
         */
        public
        function publishErratum($sendInfoMail = true, $preliminary = false) {

                if (!$this->id) return null;
                if (!$this->medium) $this->loadMedium();

                $oldChapter = new Bookchapter($this->getDataElement('previous_version'));
                if (!$oldChapter->getElement('id')) return false;

                $data = [
                        'cp_abstract' => $this->getDataElement('abstract'),
                        'cp_author_uid' => $oldChapter->getElement('author_uid'),
                        'cp_bkid' => $this->getDataElement('bk_id'),
                        'cp_chapter_text' => $this->getDataElement('chapter_text'),
                        'cp_correction' => $this->getDataElement('correction'),
                        'cp_corresponding_author' => $this->getDataElement('corresponding_authors'),
                        'cp_ddc' => $oldChapter->getElement('ddc'),
                        'cp_doi' => $this->getDataElement('doi'),
                        'cp_errata' => $this->getDataElement('erratum'),
                        'cp_funding_id' => $this->getDataElement('funcing_id'),
                        'cp_funding_number' => $this->getDataElement('funding_number'),
                        'cp_funding_name' => $this->getDataElement('funding_name'),
                        'cp_keywords' => $this->getDataElement('keywords'),
                        'cp_license' => $this->getDataElement('license'),
                        'cp_publication_place' => $this->getDataElement('publication_place'),
                        'cp_publication_year' => $this->getDataElement('publication_year'),
                        'cp_publisher' => $oldChapter->getElement('publisher'),
                        'cp_title' => $this->getDataElement('title'),
                        'cp_urn' => $oldChapter->getElement('urn'),
                        'cp_version' => $this->getDataElement('version'),
                        'cp_patients_rights' => $oldChapter->getElement('patients_rights'),
                        'cp_author_contract' => $oldChapter->getElement('author_contract'),
                        'cp_conflict_of_interest' => $this->getDataElement('conflict_of_interest'),
                        'cp_conflict_of_interest_text' => $this->getDataElement('conflict_of_interest_text'),
                        'cp_corporation' => $this->getDataElement('corporations'),
                        'cp_sub_category' => $this->getDataElement('sub_category'),
                        'cp_more_authors' => $oldChapter->getElement('more_authors'),
                        'cp_wfid' => $this->getElement('id'),
                        'cp_authors' => $this->getDataElement('authors'),
                        'cp_policy_accept' => $this->getDataElement('accept_policy'),
                        'cp_published' => $this->getDataElement('published') ? $this->getDataElement('published') : date("Y-m-d"),
                        'cp_files' => $oldChapter->getElement('files'),
                        'cp_previous_version' => $oldChapter->getElement('id'),
                        'cp_title_short' => $this->getDataElement('title_short'),
                        'cp_received' => $this->getDataElement('received') ?? null,
                        'cp_revised' => $this->getDataElement('revised') ?? null,
                        'cp_accepted' => $this->getDataElement('accepted') ?? null
                ];

                if (!$this->getDataElement('cp_id')) {
                        $cp_id = \Drupal::database()->insert('rwPubgoldBookChapters')->fields($data)->execute();
                        $this->setDataElement('cp_id', $cp_id);
                } else {
                        $cp_id = $this->getDataElement('cp_id');
                        \Drupal::database()->update('rwPubgoldBookChapters')->fields($data)->condition('cp_id', $cp_id, '=')->execute();
                }

                if (!$preliminary) {

                        $oldTociLink = "intern://chapter/" . $oldChapter->getElement('id');
                        $toci_id = \Drupal::database()->select('rwPubgoldTableOfContentItem', 't')->fields('t', ['toci_id'])->condition('toci_link', $oldTociLink, '=')->execute()->fetchField();

                        if ($toci_id) {
                                //recreate structure
                                $newTociLink = "intern://chapter/$cp_id";
                                \Drupal::database()->update('rwPubgoldTableOfContentItem')->fields(['toci_link' => $newTociLink])->condition('toci_link', $oldTociLink, '=')->execute();
                                $toc_item = new TableOfContentItem($toci_id);
                                $toc = new TableOfContent($toc_item->getElement('tocid'));
                                $toc->recreateStructure();
                        }
                        $oldChapter->setElement('next_version', $cp_id);
                        $oldChapter->setElement('pending_erratum', 0);
                        $this->setDataElement('state', 'published');

                        $vars = [
                                '::lnk.submission::' => Url::fromRoute('publisso_gold.book.chapter', ['bk_id' => $this->medium->getElement('id'), 'cp_id' => $cp_id])->setAbsolute()->toString()
                        ];

                        if ($sendInfoMail == true) {

                                $mail = new WorkflowInfoMail($this, 'erratum publish submission');
                                $mail->addVars($vars);
                                if ($mail) $mail->send();

                                $mail = new WorkflowInfoMail($this, 'erratum publication info eic');
                                $mail->addVars($vars);
                                if ($mail) $mail->send();
                        }
                }

                if ($this->getElement('state') == 'published') {

                        $chapter = new Bookchapter($cp_id);
                        $urlChapter = $chapter->getChapterLink('url');
                        $urlChapter->setAbsolute();
                        \Drupal::service('messenger')->addMessage(
                                new FormattableMarkup(
                                        (string)t('The chapter has been published successfully. You can see the chapter by visiting <a href="@link">@link</a>'),
                                        ['@link' => $urlChapter->toString()]
                                )
                        );
                }
                return $cp_id;
        }

        /**
         * @param $recommendation
         * @return null
         * @throws Exception
         */
        public
        function setRecommendation($recommendation) {

                if (!$this->id) return null;

                \Drupal::database()->insert('rwPubgoldWorkflowRecommendations')->fields([
                                'created_by_uid' => $this->session->get('user')['id'],
                                'recommendation' => $recommendation,
                                'wf_id' => $this->id
                        ])->execute();
        }

        /**
         * @param null $src
         * @return false
         */
        public
        function forward($src = null) {

                $dest = [
                        'accepted' => 'author clearing request',
                        'editor change request' => 'editor clearing request',
                        'author change request' => 'author clearing request'
                ];

                $currState = $src ? $src : $this->getDataElement('state');

                if (array_key_exists($currState, $dest)) {
                        return $this->setState($dest[$currState]);
                }
        }

        /**
         * @param null $state
         * @param array $args
         * @return false
         */
        public
        function setState($state = null, $args = []) {

                if (!$this->medium) $this->loadMedium();
                $method = 'newState' . ucfirst(strtolower(str_replace(' ', '', $state)));

                if (!method_exists($this, $method))
                        return false;

                #$this->setElement('state', $state);
                return $this->$method($args);
        }

        /**
         * @param $data
         */
        public
        function makeTemporary($data) {

                $this->elements['type'] = $data['type'];

                //abstract
                $element = 'abstract';
                $$element = $data[$element];
                $this->setDataElement($element, base64_encode($$element), false);

                //chapter_text
                $element = 'chapter_text';
                $$element = $data[$element];
                $this->setDataElement($element, base64_encode($$element), false);

                //keywords
                $keywords = array_filter(array_map('trim', explode(';', $data['keywords'])));
                $this->setDataElement('keywords', json_encode($keywords), false);

                //corporations
                $this->setDataElement('corporations', json_encode(array_map('trim', $data['corporation'])), false);

                //falls vorhanden - erratum
                if (array_key_exists('erratum', $data))
                        $this->setDataElement('errata', $data['erratum'], false);

                $values = [
                        'bk_id', 'data_type', 'title', 'conflict_of_interest', 'conflict_of_interest_text', 'funding', 'funding_name', 'funding_id', 'reviewer_suggestion', 'accept_policy',
                        'references', 'doi', 'license', 'version', 'prvious_version', 'publication_place', 'publication_year'
                ];

                foreach ($values as $_) {
                        if (array_key_exists($_, $data)) $this->setDataElement($_, $data[$_]);
                }

                //authors
                $authors = [];

                foreach ($data['author_db'] as $item) {

                        $author = $item['author'];

                        if (array_key_exists('raw', $author)) { //kommt beim Setzen des Erratums vor

                                if (!($author['raw']['data']['delete'])) {

                                        $fname = $author['raw']['data']['fields']['firstname'];
                                        $lname = $author['raw']['data']['fields']['lastname'];
                                        $affiliation = $author['raw']['data']['fields']['affiliation'];
                                        $name = implode(' ', [$fname, $lname]);
                                        $author = implode('; ', [$name, $affiliation]);

                                }
                        }

                        $is_corresponding = $item['is-corresponding'];
                        $weight = $item['weight'];

                        list($author, $author_data) = preg_split('/;\s{1,}/', $author, 2);

                        if (!empty($author)) {
                                $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                $found = 0;
                                $last_id = null;
                                $string = '';

                                foreach ($res as $v) {
                                        $string .= ' || ' . implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, \Drupal::service('publisso_gold.tools')->getCountry($v->up_country)])) .
                                                ' | ' . implode('; ', [$author, $author_data]);
                                        if (
                                                implode('; ', [$author, $author_data]) ==
                                                implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, \Drupal::service('publisso_gold.tools')->getCountry($v->up_country)]))) {
                                                $found++;
                                                $last_id = $v->up_uid;
                                        }
                                }

                                if ($found == 1) {

                                        $user = new User($last_id);

                                        $authors[] = [
                                                'weight' => $weight,
                                                'is_corresponding' => $is_corresponding,
                                                'firstname' => $user->profile->getElement('firstname'),
                                                'lastname' => $user->profile->getElement('lastname'),
                                                'affiliation' => $user->profile->getElement('institute'),
                                                'uid' => $user->getElement('id'),
                                                'profile_archive_id' => $user->profile->makeSnapshot(\Drupal::service('session')->get('user')['id'])
                                        ];
                                }
                        }
                }

                foreach ($data['author_add'] as $item) {
                        $authors[] = array_map('trim', $item);
                }

                $this->setDataElement('authors', json_encode($authors), false);
                $this->elements['locked_by_uid'] = \Drupal::service('session')->get('user')['id'];
                if (!$this->medium) $this->loadMedium();
        }

        /**
         * @param null $uid
         * @throws Exception
         */
        public
        function finishReview($uid = null) {

                if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];
                $id = $this->getReviewsheet($uid, 'id');

                $qry = \Drupal::database()->update('rwPubgoldReviewerReviewsheet')->fields([
                        'state' => 'finished'
                ])->expression('modified', 'NOW()')->condition('id', $id, "=")->execute();

                $this->removeReviewer($uid);

                $mail = new WorkflowInfoMail($this, 'reviewer finished');
                if ($mail) $mail->send();

                if (!count($this->readReviewers())) {

                        $this->setDataElement('state', 'review finished');
                        $this->setElement('state', 'review finished');
                        $this->setElement('assigned_to', 'u:' . implode(',u:', explode(',', $this->getElement('assigned_to_editor'))));

                        $mail = new WorkflowInfoMail($this, 'review finished');
                        if ($mail) $mail->send();
                }
        }

        /**
         * @param null $uid
         * @param string $field
         * @return mixed
         * @throws Exception
         */
        public
        function getReviewsheet($uid = null, $field = 'reviewsheet') {

                if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];
                if (!$this->medium) $this->loadMedium();

                $qry = \Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', []);
                $cnd = $qry->andConditionGroup();
                $cnd1 = $qry->orConditionGroup();

                $cnd->condition('wfid', $this->id, '=');
                $cnd->condition('uid', $uid, '=');

                $cnd1->condition('state', 'progress', '=')->condition('state', 'pending', '=');
                $cnd->condition($cnd1);
                $qry->condition($cnd);

                if (!$qry->countQuery()->execute()->fetchField()) {

                        $rp_id = $this->medium->getElement('rpid');
                        $rp = new Reviewsheet($rp_id);

                        \Drupal::database()->insert('rwPubgoldReviewerReviewsheet')->fields([
                                        'uid' => $uid,
                                        'reviewsheet' => $rp->getElement('sheet'),
                                        'wfid' => $this->id
                                ])->execute();
                }

                $res = $qry->execute()->fetchAll();
                return $res[0]->$field;
        }

        /**
         * @param array $uids
         */
        public
        function removeReviewer($uids = []) {

                if ($this->id) {

                        if (!is_array($uids))
                                $uids = [$uids];

                        foreach ($uids as $uid) {

                                \Drupal::database()->query("
                                UPDATE {" . $this->db_table .
                                                "}
                                        SET
                                        `" . $this->tbl_column_prefix .
                                                "assigned_to_reviewer` =
                                                TRIM(BOTH ',' FROM
                                                REPLACE(
                                                        REPLACE(CONCAT(',',REPLACE(`" . $this->tbl_column_prefix .
                                                "assigned_to_reviewer`, ',', ',,'), ','),'," . $uid .
                                                ",', ''), ',,', ',')
                                                )
                                        WHERE
                                        `" . $this->tbl_column_prefix .
                                                "id` = " . $this->getElement('id') .
                                                "
                                ");

                                if ($this->getDataElement('state') == 'in review') {

                                        \Drupal::database()->query("
                                                UPDATE {" . $this->db_table .
                                                        "}
                                                        SET
                                                        `" . $this->tbl_column_prefix .
                                                        "assigned_to` =
                                                                TRIM(BOTH ',' FROM
                                                                REPLACE(
                                                                        REPLACE(CONCAT(',',REPLACE(`" . $this->tbl_column_prefix .
                                                        "assigned_to`, ',', ',,'), ','),',u:" . $uid .
                                                        ",', ''), ',,', ',')
                                                                )
                                                        WHERE
                                                        `" . $this->tbl_column_prefix .
                                                        "id` = " . $this->getElement('id') .
                                                        "
                                                ");
                                }

                                \Drupal::database()->delete('rwPubgoldReviewerReviewsheet')->condition('wfid', $this->id, '=')->condition('uid', $uid, '=')->condition('state', ['pending', 'progress'], 'IN')->execute();
                        }

                        $this->load();
                }
        }

        /**
         * @return array
         */
        public
        function readReviewers() {
                return array_filter($this->reviewers);
        }

        /**
         * @param null $uid
         */
        public
        function rejectReview($uid = null) {

                if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];
                $id = $this->getReviewsheet($uid, 'id');

                $qry = \Drupal::database()->update('rwPubgoldReviewerReviewsheet')->fields([
                        'state' => 'rejected'
                ])->expression('modified', 'NOW()')->condition('id', $id, "=")->execute();
        }

        /**
         * @param $reviewsheet
         * @param null $uid
         */
        public
        function updateReview($reviewsheet, $uid = null) {

                if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];
                $id = $this->getReviewsheet($uid, 'id');
                $qry = \Drupal::database()->update('rwPubgoldReviewerReviewsheet')->fields([
                        'reviewsheet' => $reviewsheet
                ])->expression('modified', 'NOW()')->condition('id', $id, "=")->execute();
        }

        /**
         * @param $state
         * @param null $uid
         */
        public function setReviewState($state, $uid = null) {

                if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];
                $id = $this->getReviewsheet($uid, 'id');
                $qry = \Drupal::database()->update('rwPubgoldReviewerReviewsheet')->fields([
                        'state' => $state
                ])->expression('modified', 'NOW()')->condition('id', $id, "=")->execute();
        }

        /**
         * @param $template
         * @param array $args
         * @return |null
         */
        public
        function getRenderableTemplate($template, $args = []) {

                $method = "getRenderableTemplate_" . strtoupper($template);

                if (method_exists($this, $method))
                        return $this->$method($args);
                else
                        return null;
        }

        /**
         * @return bool
         */
        public
        function hasCompletedReviewsheets() {

                return !!\Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', [])->condition('wfid', $this->id, '=')->condition('state', 'finished', '=')->countQuery()->execute()->fetchField();
        }

        /**
         * @return null
         */
        public function getStateChain() {

                if ($this->stateChain === null) {
                        $this->loadStateChain();
                }

                return $this->stateChain;
        }

        /**
         * @param string|null $state
         * @return bool
         */
        public function hasStateInChain(string $state = null) {

                if ($this->stateChain === null) {
                        $this->loadStateChain();
                }

                foreach ($this->stateChain as $_) {
                        if ($_->state_old == $state || $_->state_new == $state) {
                                return true;
                                break;
                        }
                }

                return false;
        }

        /**
         * @return mixed
         */
        public function getPreviousState() {

                if ($this->stateChain === null) {
                        $this->loadStateChain();
                }

                return end($this->stateChain)->state_old;
        }

        /**
         * @param string $elemName
         * @return |null
         */
        public function getStateChainElement(string $elemName) {

                if ($this->stateChain === null) {
                        $this->loadStateChain();
                }

                foreach (array_reverse($this->stateChain) as $state) {
                        if ($state->state_new == $elemName) {
                                return $state->time;
                                break;
                        }
                }

                return null;
        }

        /**
         * @return mixed|string
         */
        public function getLastEditorRole() {

                if ($this->stateChain === null) {
                        $this->loadStateChain();
                }

                $role = 'eo';

                foreach (array_reverse($this->stateChain) as $_) {

                        $state = $_->state_old;

                        if (preg_match('/assigned to (eo|eic|editor)/', $state, $matches)) {
                                $role = $matches[1];
                                break;
                        }
                }

                return $role;
        }

        /**
         * @param $args
         * @return false
         * @throws Exception
         */
        private
        function newStateAssignedtoeic($args) {

                if (!is_array($args)) return false;

                $this->setDataElement('state', 'assigned to eic');
                $this->setElement('state', 'assigned to eic');
                $this->setElement('assigned_to', 'u:' . implode(',u:', $args));
                $this->setElement('assigned_to_eic', implode(',', $args));
                $this->load();

                $mail = new WorkflowInfoMail($this, 'assigned to eic');
                if ($mail) $mail->send();
        }

        /**
         * @param $args
         * @return false
         * @throws Exception
         */
        private
        function newStateAssignedtoeo($args) {

                if (!is_array($args)) return false;

                $this->setDataElement('state', 'assigned to eo');
                $this->load();

                $mail = new WorkflowInfoMail($this, 'assigned to eo');
                if ($mail) $mail->send();
        }

        /**
         * @param $args
         * @return false
         * @throws Exception
         */
        private
        function newStateAssignedtoeditor($args) {

                if (!is_array($args)) return false;

                $this->setDataElement('state', 'assigned to editor');
                $this->setElement('assigned_to', 'u:' . implode(',u:', $args));
                $this->setElement('assigned_to_editor', implode(',', $args));
                $this->load();
                $mail = new WorkflowInfoMail($this, 'assigned to editor');
                if ($mail) $mail->send();
                //sendWorkflowInfoMail($this, 'assigned to editor');
        }

        /**
         * @return bool
         * @throws Exception
         */
        private
        function newStateInrevision() {

                switch ($this->getElement('state')) {

                        case 'submission finished':
                        case 'accepted':
                                $this->setElement('revision_ordered_by', 'eo');
                                break;

                        case 'assigned to eic':
                        case 'decision eic':
                                $this->setElement('revision_ordered_by', 'eic');
                                break;

                        case 'assigned to editor':
                        case 'review finished':
                                $this->setElement('revision_ordered_by', 'editor');
                                break;

                }

                $this->setDataElement('state', 'in revision', false);
                $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('created_by_uid')))), false);

                $this->setElement('editor_last_edited', \Drupal::service('session')->get('user')['id'], false);
                $this->save();
                $this->load();

                $process = ($this->getElement('revision_ordered_by') != 'editor' ? 'editor ' : '') . 'in revision' . ($this->getElement('revision_ordered_by') != 'editor' && $this->getDataElement('revised') ? ' again ' : '');

                $mail = new WorkflowInfoMail($this, $process);
                if ($mail) $mail->send($this->getElement('revision_ordered_by') != 'editor');
                //sendWorkflowInfoMail($this, 'in revision', [], false);
                return true;
        }

        /**
         * @return bool
         * @throws Exception
         */
        private
        function newStateRevisionfinished() {

                $this->setDataElement('state', 'revision finished');

                if ($this->getElement('revision_ordered_by') == 'eo') {
                        $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('assigned_to_eo')))));
                } else {
                        $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('editor_last_edited')))));
                }

                $this->load();

                $mail = new WorkflowInfoMail($this, ($this->getElement('revision_ordered_by') != 'editor' ? 'eic ' : '') . 'revision finished');
                if ($mail) $mail->send();
                //sendWorkflowInfoMail($this, 'revision finished');
                return true;
        }

        /**
         * @param $args
         * @return false
         * @throws Exception
         */
        private
        function newStateInreview($args) {

                if (!is_array($args)) return false;

                if (!$this->medium) $this->loadMedium();

                $this->setDataElement('state', 'in review');
                $this->setElement('state', 'in review');
                $this->setElement('assigned_to', 'u:' . implode(',u:', $args));
                $this->setElement('assigned_to_reviewer', implode(',', $args));
                $this->setElement('reviewer_edited', '');
                $this->load();

                if (!empty($this->medium->getElement('rpid'))) {

                        foreach ($args as $uid) {
                                $this->newUserReview($uid);
                        }
                }
        }

        /**
         * @param int $uid
         * @return bool
         * @throws Exception
         */
        public function newUserReview(int $uid) {

                if (!$this->medium) $this->loadMedium();

                $qry = \Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', []);
                $cnd = $qry->andConditionGroup();
                $cnd1 = $qry->orConditionGroup();

                $cnd->condition('wfid', $this->id, '=');
                $cnd->condition('uid', $uid, '=');

                $cnd1->condition('state', 'progress', '=')->condition('state', 'pending', '=');
                $cnd->condition($cnd1);

                $qry->condition($cnd);

                if (!$qry->countQuery()->execute()->fetchField()) {

                        $this->addReviewer([$uid]);

                        $rp_id = $this->medium->getElement('rpid');
                        $rp = new Reviewsheet($rp_id);

                        \Drupal::database()->insert('rwPubgoldReviewerReviewsheet')->fields([
                                        'uid' => $uid,
                                        'reviewsheet' => $rp->getElement('sheet'),
                                        'wfid' => $this->id,
                                        'state' => 'pending'
                                ])->execute();

                        if (!array_key_exists('revised', $this->getDataKeys())) {
                                $mail = new WorkflowInfoMail($this, 'in review');
                                if ($mail) $mail->send();
                                //sendWorkflowInfoMail($this, 'in review');
                        } else {
                                $mail = new WorkflowInfoMail($this, 'in review after revision');
                                if ($mail) $mail->send();
                                //sendWorkflowInfoMail($this, 'in review after revision');
                        }

                        return true;
                }

                return false;
        }

        /**
         * @param array $uids
         */
        public
        function addReviewer($uids = []) {

                if (!is_array($uids))
                        $uids = [$uids];

                foreach ($uids as $uid) {

                        if (!$this->isReviewer($uid)) {

                                $reviewers = array_filter(explode(',', $this->getElement('assigned_to_reviewer')));
                                array_push($reviewers, $uid);

                                $this->setElement('assigned_to_reviewer', implode(',', $reviewers));

                                if ($this->getDataElement('state') == 'in review') {
                                        $reviewers = array_filter(explode(',', $this->getElement('assigned_to')));
                                        array_push($reviewers, "u:$uid");
                                        $this->setElement('assigned_to', implode(',', $reviewers));
                                }
                        }
                }

                $this->load();
        }

        /**
         * @param $uid
         * @return bool
         */
        public
        function isReviewer($uid) {

                foreach ($this->reviewers as $user) {
                        if ($user->getElement('id') == $uid) return true;
                }

                return false;
        }

        /**
         * @return array
         */
        public
        function getDataKeys() {
                return array_keys((array)$this->data);
        }

        /**
         * @return bool
         * @throws Exception
         */
        private
        function newStateDecisioneic() {
                $this->setDataElement('state', 'decision eic');
                $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('assigned_to_eic')))));
                $this->load();

                $mail = new WorkflowInfoMail($this, 'decision eic');
                if ($mail) $mail->send();
                //sendWorkflowInfoMail($this, 'decision eic');
                return true;
        }

        /**
         * @return bool
         * @throws Exception
         */
        private
        function newStateRejected() {

                $this->setDataElement('state', 'rejected');
                $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('created_by_uid')))));
                $this->load();

                $mail = new WorkflowInfoMail($this, 'submission rejected');
                if ($mail) $mail->send(true);
                //sendWorkflowInfoMail($this, 'submission rejected', [], false);
                return true;
        }

        /**
         * @return bool
         * @throws Exception
         */
        private
        function newStateAccepted() {

                $this->setDataElement('state', 'accepted');

                //if workflow assigned to eo
                if (!empty($this->getElement('assigned_to_eo'))) {
                        $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('assigned_to_eo')))));
                } //redirect to book-eo
                else {
                        $eo = $this->medium->readEditorialOffice();
                        $ary = [];

                        foreach ($eo as $user) {
                                $ary[] = $user->getElement('id');
                        }

                        $this->setElement('assigned_to', 'u:' . implode(',u:', $ary));
                }


                $this->load();

                $mail = new WorkflowInfoMail($this, 'submission accepted');
                if ($mail) $mail->send();
                //sendWorkflowInfoMail($this, 'submission accepted');
                return true;
        }

        /**
         * @return bool
         * @throws Exception
         */
        private
        function newStateAuthorclearingrequest() {

                $this->setDataElement('state', 'author clearing request');
                $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('created_by_uid')))));
                $this->load();
                if ($this->getElement('type') == 'bcs') {

                        $mail = new WorkflowInfoMail($this, 'author clearing request');
                        if ($mail) $mail->send(true);
                        //sendWorkflowInfoMail($this, 'author clearing request', [], false);
                } elseif ($this->getElement('type') == 'bce') {

                        $mail = new WorkflowInfoMail($this, 'erratum author clearing request');
                        if ($mail) $mail->send(true);
                        //sendWorkflowInfoMail($this, 'erratum author clearing request', [], false);
                }
                return true;
        }

        /**
         * @return bool
         * @throws Exception
         */
        private
        function newStateEditorclearingrequest() {

                $this->setDataElement('state', 'editor clearing request');
                $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('assigned_to_eic')))));
                $this->load();

                $mail = new WorkflowInfoMail($this, 'editor clearing request');
                if ($mail) $mail->send();
                //sendWorkflowInfoMail($this, 'editor clearing request');
                return true;
        }

        /**
         * @return bool
         * @throws Exception
         */
        private
        function newStateAuthorchangerequest() {

                $this->setDataElement('state', 'author change request');

                //if workflow assigned to eo
                if (!empty($this->getElement('assigned_to_eo'))) {
                        $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('assigned_to_eo')))));
                } //redirect to book-eo
                else {
                        $eo = $this->medium->readEditorialOffice();
                        $ary = [];

                        foreach ($eo as $user) {
                                $ary[] = $user->getElement('id');
                        }

                        $this->setElement('assigned_to', 'u:' . implode(',u:', $ary));
                }


                $this->load();
                if ($this->getElement('type') == 'bcs') {

                        $mail = new WorkflowInfoMail($this, 'author change request');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'author change request', [], true);
                } elseif ($this->getElement('type') == 'bce') {

                        $mail = new WorkflowInfoMail($this, 'erratum author change request');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'erratum author change request', [], true);
                }
                return true;
        }

        /**
         * @param $args
         * @return array|null
         */
        private
        function getRenderableTemplate_FILES($args) {

                $files_tmpl = [];
                $fids = $this->getDataElement('files');
                if ($fids) $fids = unserialize($fids);

                if (is_array($fids) && count($fids)) {

                        foreach ($fids as $fid) {

                                $blob = new Blob($fid);

                                $files_tmpl[] = [
                                        '#type' => 'fieldset',
                                        'content' => [
                                                '#type' => 'link',
                                                '#title' => (string)t('Download "@name"', ['@name' => $blob->meta['name']]),
                                                '#url' => Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()], ['attributes' => ['target' => '_blank']]),
                                                '#suffix' => '<br><i>' . $blob->meta['description'] .
                                                        '</i>'
                                        ],
                                        'preview' => preg_match('/^(image|video|audio)\//', $blob->type, $matches) ? [
                                                '#type' => 'inline_template',
                                                '#template' => (
                                                $matches[1] == 'image' ? '<br><img height="100" src="' . Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString() .
                                                        '">' : (
                                                $matches[1] == 'video' ? '<br><video height="100" controls><source src="' . Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString() .
                                                        '" type="' . $blob->type .
                                                        '"></video>' : (
                                                $matches[1] == 'audio' ? '<br><audio controls><source src="' . Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString() .
                                                        '" type="' . $blob->type .
                                                        '"></audio>' : (
                                                ''))))
                                        ] : ''
                                ];
                        }

                        return [
                                '#type' => 'details',
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.files', 'fc')),
                                '#open' => false,
                                'content' => $files_tmpl,];
                }

                return null;
        }

        /**
         * @param $args
         * @return array
         */
        private
        function getRenderableTemplate_META($args) {

                $metaTemplate = new Template();
                $metaTemplate->get('workflow_item_infobox_metadata');

                $rows = [];
                $bool = ['accept_policy', 'conflict_of_interest'];
                $base64 = [];
                $sort = [
                        'authors' => 1,
                        'corporations' => 2,
                        'conflict_of_interest' => 3,
                        'conflict_of_interest_text' => 4,
                        'keywords' => 5,
                        'funding_name' => 6,
                        'funding_id' => 7
                ];

                $keys = [
                        'corporations', 'keywords', 'accept_policy', 'authors', 'conflict_of_interest', 'conflict_of_interest_text', 'funding_name', 'funding_id',
                        'title_short', 'doi', 'publication_place', 'publication_year', 'sub_category'
                ];
                $unsortedCounter = count($keys);

                foreach ($keys as $key) {

                        $value = in_array($key, $base64) ? base64_decode($this->getDataElement($key)) : $this->getDataElement($key);

                        if (in_array($key, $bool)) {
                                $value = \Drupal::service('publisso_gold.texts')->get("global.bool_value." . (!!$value ? 'yes' : 'no'));
                        }

                        /**
                         * treat special fields (serialized structures ...
                         **/
                        //keywords
                        if ($key == 'keywords') $value = implode('; ', array_filter(json_decode($value, true)));

                        //authors
                        if ($key == 'authors') {

                                $authors = [];

                                foreach (array_filter(json_decode($value, true)) as $id => $author) {

                                        $id = $author['weight'] ? $author['weight'] : count(array_filter(json_decode($value, true))) + $id;
                                        $authors[$id] = implode(' ', array_filter([$author['firstname'], $author['lastname']]));

                                        if ($author['is_corresponding'])
                                                $authors[$id] .= ((string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.corresponding.suffix')));
                                }

                                ksort($authors);
                                $value = implode("\n", $authors);
                        }

                        //corporations
                        if ($key == 'corporations') {
                                $value = implode("\n", array_filter(json_decode($value, true)));
                        }

                        //put values to template (table)
                        if ($value) {
                                $metaTemplate->setVar("workflow.item.infobox.$key.txt", \Drupal::service('publisso_gold.texts')->get("workflow.item.infobox.$key", 'fc'));
                                $metaTemplate->setVar("workflow.item.infobox.$key.val", nl2br($value));

                                $rowID = array_key_exists($key, $sort) ? $sort[$key] : $unsortedCounter++;

                                $rows[$rowID] = [\Drupal::service('publisso_gold.texts')->get("workflow.item.infobox.$key", 'fc'),
                                        new FormattableMarkup(nl2br($value), [])
                                ];
                        }
                }

                ksort($rows, SORT_NUMERIC);

                return [
                        '#type' => 'details',
                        '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.meta', 'fc')),
                        '#open' => false,
                        'content' => [
                                '#type' => 'table',
                                '#rows' => $rows
                        ]];
        }

        /**
         * @param $args
         * @return array|null
         */
        private
        function getRenderableTemplate_ABSTRACT($args) {

                $abstract = $this->getDataElement('abstract');

                if ($abstract) {

                        $abstract = base64_decode($abstract);

                        return [
                                '#type' => 'details',
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.abstract', 'fc')),
                                '#open' => false,
                                'content' => [
                                        '#type' => 'inline_template',
                                        '#template' => $abstract
                                ]];
                }

                return null;
        }

        /**
         * @param $args
         * @return array|null
         */
        private
        function getRenderableTemplate_ERRATUM($args) {

                $erratum = $this->getDataElement('erratum');

                if ($erratum) {

                        return [
                                '#type' => 'details',
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.erratum', 'fc')),
                                '#open' => false,
                                'content' => [
                                        '#type' => 'inline_template',
                                        '#template' => $erratum
                                ]];
                }

                return null;
        }

        /**
         * @param $args
         * @return array|null
         */
        private
        function getRenderableTemplate_ORIGTEXT($args) {

                $text = '';

                switch (substr($this->getElement('type'), 0, 1)) {

                        case 'b':
                                $text = base64_decode($this->getDataElement('chapter_text'));
                                break;

                        case 'j':
                                $text = base64_decode($this->getDataElement('article_text'));
                                break;

                        case 'c':
                                $text = base64_decode($this->getDataElement('paper_text'));
                                break;
                }

                if ($text) {

                        return [
                                '#type' => 'details',
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.orig_text', 'fc')),
                                '#open' => false,
                                'content' => [
                                        '#type' => 'inline_template',
                                        '#template' => $text,
                                        '#prefix' => '<div id="chapter-content">',
                                        '#suffix' => '</div>'
                                ]];
                }

                return null;
        }

        /**
         * @param $args
         * @return array|null
         */
        private
        function getRenderableTemplate_RECOMMENDATIONS($args) {

                $result = \Drupal::database()->select('rwPubgoldWorkflowRecommendations', 't')->fields('t', [])->condition('wf_id', $this->getElement('id'), '=')->execute()->fetchAll();


                if (count($result)) {

                        $ary = [
                                '#type' => 'details',
                                '#open' => false,
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.recommendations', 'fc')),
                                'content' => [
                                        '#type' => 'table',
                                        '#header' => [
                                                'user' => (string)t(\Drupal::service('publisso_gold.texts')->get('global.user', 'fc')),
                                                'date' => (string)t(\Drupal::service('publisso_gold.texts')->get('global.date', 'fc')),
                                                'recommendation' => (string)t(\Drupal::service('publisso_gold.texts')->get('global.recommendation', 'fc')),
                                        ],
                                        '#rows' => [],
                                        '#empty' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.recommendations.emtpy', 'fc')),
                                ],
                        ];

                        foreach ($result as $_) {

                                $__user = new User($_->created_by_uid);

                                $ary['content']['#rows'][] = [
                                        'user' => $__user->profile->getReadableFullName(),
                                        'date' => $_->created,
                                        'recommendation' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.recommendations.recommendation_' . $_->recommendation, 'fc')),
                                ];
                        }

                        return $ary;
                }

                return null;
        }

        /**
         * @param $args
         * @return array|null
         */
        private
        function getRenderableTemplate_COMMENTSFORAUTHOR($args) {

                $comments = $this->getCommentsForAuthor();

                if (count($comments)) {

                        $ary = [
                                '#type' => 'details',
                                '#open' => false,
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.comments_for_author', 'fc')),
                                'content' => [
                                        '#type' => 'table',
                                        '#header' => [
                                                'user' => '',
                                                'comment' => '',
                                        ],
                                        '#rows' => [],
                                        '#empty' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.comments_for_author.emtpy', 'fc')),
                                ],
                        ];

                        foreach ($comments as $_) {

                                $__user = new User($_->cfa_created_by_uid);

                                $ary['content']['#rows'][] = [
                                        'user' => $__user->profile->getReadableFullName(),
                                        'comment' => base64_decode($_->cfa_comment)
                                ];
                        }

                        return $ary;
                }

                return null;
        }

        /**
         * @return bool
         */
        public function hasEditors(){
                return !!count($this->editors);
        }

        /**
         * @param $args
         * @return array|null
         */
        private
        function getRenderableTemplate_COMMENTSFORAUTHOR_ANON($args) {

                $comments = $this->getCommentsForAuthor();

                if (count($comments)) {

                        $ary = [
                                '#type' => 'details',
                                '#open' => false,
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.comments_for_author', 'fc')),
                                'content' => [
                                        '#type' => 'table',
                                        '#header' => [
                                                'user' => '',
                                                'comment' => '',
                                        ],
                                        '#rows' => [],
                                        '#empty' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.comments_for_author.emtpy', 'fc')),
                                ],
                        ];
                        $c = 1;
                        foreach ($comments as $_) {

                                $__user = new User($_->cfa_created_by_uid);

                                $ary['content']['#rows'][] = [
                                        'user' => (string)t('Comment #@nr', ['@nr' => $c++]),
                                        'comment' => base64_decode($_->cfa_comment)
                                ];
                        }

                        return $ary;
                }

                return null;
        }


        /**
         * @return mixed
         */
        public
        function getCommentsForAuthor() {
                return \Drupal::database()->select('rwPubgoldWorkflowCommentsForAuthors', 'cfa')->fields('cfa', [])->condition('cfa_wfid', $this->id, '=')->execute()->fetchAll();
        }

        /**
         * @param $args
         * @return array|null
         */
        private
        function getRenderableTemplate_COMMENTS($args) {

                $comments = $this->getComments();

                if (count($comments)) {

                        $ary = [
                                '#type' => 'details',
                                '#open' => false,
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.comments', 'fc')),
                                'content' => [
                                        '#type' => 'table',
                                        '#header' => [
                                                'user' => (string)t(\Drupal::service('publisso_gold.texts')->get('global.user', 'fc')),
                                                'comment' => (string)t(\Drupal::service('publisso_gold.texts')->get('global.comment', 'fc')),
                                        ],
                                        '#rows' => [],
                                        '#empty' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.comments.emtpy', 'fc')),
                                ],
                        ];

                        foreach ($comments as $_) {

                                $__user = new User($_->wfc_created_by_uid);

                                $ary['content']['#rows'][] = [
                                        'user' => $__user->profile->getReadableFullName(),
                                        'comment' => base64_decode($_->wfc_comment)
                                ];
                        }

                        return $ary;
                }

                return null;
        }

        /**
         * @return mixed
         */
        public
        function getComments() {
                return \Drupal::database()->select('rwPubgoldWorkflowComments', 'wfc')->fields('wfc', [])->condition('wfc_wfid', $this->id, '=')->execute()->fetchAll();
        }

        /**
         * @param $args
         * @return array|null
         */
        private
        function getRenderableTemplate_REFERENCES($args) {

                $references = $this->getDataElement('references');

                if ($references) {

                        $references = nl2br($references);

                        return [
                                '#type' => 'details',
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.references', 'fc')),
                                '#open' => false,
                                'content' => [
                                        '#type' => 'inline_template',
                                        '#template' => $references
                                ]];
                }

                return null;
        }

        /**
         * @param $args
         * @return array
         */
        private
        function getRenderableTemplate_REVIEWSHEETS($args) {
                return \Drupal::formBuilder()->getForm( "Drupal\\publisso_gold\\Form\\displayReviewsheetSkel", [ 'wf_id' => $this->id ] );
        }

        /**
         * @param $args
         * @return array|null
         */
        public function getReviewsheetForm($args){
                $rsIDs = $this->getUserReviewsheets($args);

                $sheets = [];
                $users = [];

                if (array_key_exists('uid', $args) && !preg_match('/^(\d+)$/', $args['uid'])) {

                        foreach ($rsIDs as $_uid => $_sheets) {

                                $user = new User($_uid);
                                $sheets[$_uid] = [
                                        '#type' => 'details',
                                        '#collapsed' => true,
                                        '#title' => $user->profile->getReadableFullName(),
                                        'content' => []
                                ];

                                foreach ($_sheets as $_sheet) {
                                        $rs = $this->getReviewsheetByID($_sheet);
                                        $sheets[$_uid]['content'][] = $this->getRenderableReviewsheet($rs->reviewsheet, $rs->state, $rs->modified);
                                }
                        }


                } else {
                        if ( array_key_exists( 'uid', $args ) ) {
                                foreach ( $rsIDs[ $args[ 'uid' ] ] as $id ) {

                                        $rs = $this->getReviewsheetByID( $id );
                                        $sheets[] = $this->getRenderableReviewsheet( $rs->reviewsheet, $rs->state, $rs->modified );
                                }
                        }
                        else {

                                foreach ( $rsIDs as $uid => $ids ) {

                                        $user = new User($uid);

                                        $ary = [
                                                '#type' => 'details',
                                                '#title' => $user->profile->getReadableFullname(),
                                                '#collapsed' => true,
                                                'content' => []
                                        ];

                                        foreach ( $ids as $id ) {
                                                $rs = $this->getReviewsheetByID( $id );
                                                $ary['content'][] = $this->getRenderableReviewsheet( $rs->reviewsheet, $rs->state, $rs->modified );
                                        }

                                        $sheets[] = $ary;
                                }
                        }
                }

                if (count($sheets)) {

                        return [
                                '#type' => 'details',
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.reviewsheets', 'fc')),
                                '#collapsed' => true,
                                'content' => $sheets
                        ];
                }

                return null;
        }

        /**
         * @param $args
         * @return array
         */
        public
        function getUserReviewsheets($args) {

                if (!$this->medium) $this->loadMedium();

                $qry = \Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', ['uid', 'id']);
                $cnd = $qry->andConditionGroup();
                $cnd->condition('wfid', $this->id, '=');

                if (isset($args['uid']))
                        $cnd->condition('uid', $args['uid'], '=');

                if (isset($args['state'])) {

                        if (is_string($args['state']) && strtolower($args['state']) != 'all')
                                $cnd->condition('state', $args['state'], '=');
                        elseif (is_array($args['state']))
                                $cnd->condition('state', $args['state'], 'IN');
                }

                $qry->condition($cnd);
                $res = $qry->orderBy('id', 'ASC')->execute()->fetchAll();

                $rs = [];

                if (array_key_exists('nogroup', $args) && $args['nogroup'] == true) {
                        foreach ($res as $_) $rs[] = $_->id;
                } else {
                        foreach ($res as $_) $rs[$_->uid][] = $_->id;
                }

                return $rs;
        }

        /**
         * @param $id
         * @param null $field
         * @return mixed|null
         */
        public
        function getReviewsheetByID($id, $field = null) {

                $qry = \Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', []);
                $cnd = $qry->andConditionGroup();
                $cnd->condition('id', $id, '=');
                $qry->condition($cnd);
                $result = $qry->execute()->fetchAll()[0];

                if ($field) {

                        if (property_exists($result, $field)) return $result->$field;
                        else return null;
                } else {
                        return $result;
                }
        }

        /**
         * @param $sheet
         * @param $state
         * @param $modified
         * @return array
         */
        private
        function getRenderableReviewsheet($sheet, $state, $modified) {

                $doc = new \DOMDocument();
                $doc->loadXML($sheet);
                $xpath = new DOMXpath($doc);
                $ary = [];

                foreach ($xpath->evaluate('/reviewsheet/elements/element') as $element) {
                        $ary[] = $this->parseReviewsheetElement($element, $xpath, 0);
                }

                $recommendation = '';
                $recommendation = $xpath->evaluate('/reviewsheet/comments/recommendation');

                if ($recommendation->length) {
                        $recommendation = (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.review.field.recommendation.option.' . $recommendation[0]->nodeValue, 'fc'));
                } else {
                        $recommendation = \Drupal::service('string_translation')->translate('none');
                }

                $comment = null;
                $comment = $xpath->evaluate('/reviewsheet/comments/comment_for_author');

                if ($comment->length) {

                        $comment = [
                                '#type' => 'container',
                                'content' => [
                                        '#markup' => $comment[0]->nodeValue,
                                        '#prefix' => '<strong>' . ((string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.review.field.comment_for_author.title', 'fc'))) .
                                                '</strong><br><br>'
                                ],
                                '#prefix' => '<hr>'
                        ];
                } else {
                        $comment = null;
                }

                return [

                        '#type' => 'details',
                        '#collapsed' => true,
                        'content' => $ary,
                        'comment' => $comment,
                        '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.reviewsheets.sheet'), [
                                '@recommendation' => $recommendation,
                                '@state' => $state,
                                '@date' => $modified
                        ])];
        }

        /**
         * @param DOMElement $element
         * @param DOMXpath $xpath
         * @return array|string[]
         */
        private
        function parseReviewsheetElement(DOMElement & $element, DOMXpath & $xpath) {

                $ary = [];

                switch ($element->getAttribute('type')) {

                        case 'item':
                                $ary = [
                                        '#type' => 'markup',
                                        '#prefix' => '<p>',
                                        '#suffix' => '</p>'
                                ];
                                break;

                        case 'heading':
                                $ary = [
                                        '#type' => 'markup',
                                        '#prefix' => '<h2>',
                                        '#suffix' => '</h2>'
                                ];
                                break;

                        case 'subheading':
                                $ary = [
                                        '#type' => 'markup',
                                        '#prefix' => '<h3>',
                                        '#suffix' => '</h3>'
                                ];
                                break;

                        case 'checkbox':
                                $ary = [
                                        '#type' => 'checkbox',
                                        '#disabled' => true
                                ];
                                break;

                        case 'textfield':
                                $ary = [
                                        '#type' => 'textfield',
                                        '#disabled' => true
                                ];
                                break;
                }

                $text = $xpath->evaluate('text', $element)[0];

                if ($xpath->evaluate('value', $element)->length) {
                        $ary['#default_value'] = $xpath->evaluate('value', $element)[0]->nodeValue;
                }

                if ($ary['#type'] == 'markup')
                        $ary['#markup'] = $text->nodeValue;
                else
                        $ary['#title'] = $text->nodeValue;

                if ($xpath->evaluate('subelements/subelement', $element)->length) {

                        $ary['subelements'] = [];

                        foreach ($xpath->evaluate('subelements/subelement', $element) as $subnode) {
                                $ary['subelements'][] = $this->parseReviewsheetElement($subnode, $xpath);
                        }
                }

                return $ary;
        }
}

?>
