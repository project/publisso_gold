<?php

	/**
	 * @file
	 * Contains \Drupal\publisso_gold\Controller\UserTable.
	 */

	namespace Drupal\publisso_gold\Controller;
	use Drupal\Core\Url;
	use Drupal\Core\Link;
	use Drupal\publisso_gold\Controller\Template;
	use Drupal\Component\Render\FormattableMarkup;
	use Symfony\Component\HttpFoundation\JsonResponse;
        
        /**
         * Class UserTable
         * @package Drupal\publisso_gold\Controller
         */
        class UserTable{
		
                private $modname;
                private $session;
                private $setup;
                private $tools;
                private $tables;
                private $route;
                private $path;
                private $userPath;
                
                /**
                 * @return string
                 */
                public function __toString(){
			return 'UserTable';
		}
		
		public function __construct(){
                        
                        \Drupal::service('publisso_gold.tools')->forceUserAction();
                        
                        $this->modname = 'usertable';
                        $this->session = \Drupal::service('session');
                        $this->setup   = \Drupal::service('publisso_gold.setup');
                        $this->tools   = \Drupal::service('publisso_gold.tools');
                        $this->tables  = [];
                        
                        $this->route['name'] = \Drupal::routeMatch()->getRouteName();
                        $this->route['param'] = \Drupal::routeMatch()->getRawParameters()->all();
                        
                        $this->path = DRUPAL_ROOT.'/'.\Drupal::service('module_handler')->getModule('publisso_gold')->getPath().'/var/userTables';
                        $this->userPath = $this->path.'/'.$this->session->get('user')['id'];
                        
                        $this->loadTables();
		}
                
                /**
                 * @param $action
                 * @param $name
                 * @return array
                 */
                public function main($action, $name){
                        
                        if(!$this->tools->userHasAccessRight($this->modname, $this->session->get('user')['weight'])){
                                return $this->tools->accessDenied();
                        }
                        
                        if(!method_exists($this, $action)){
                                return array(
                                        '#type' => 'markup',
                                        '#markup' => (string)t('This function ist not implemented!'),
                                        '#attached' => [
                                                'library' => [
                                                'publisso_gold/default'
                                                ]
                                        ],
                                        '#cache' => [
                                                'max-age' => 0
                                        ]
                                );
                        }
                        
                        return $this->$action($name);
		}
                
                /**
                 * @return array
                 */
                public function create(){
                        return \Drupal::formBuilder()->getForm("Drupal\publisso_gold\Form\addUserTable");
		}
                
                /**
                 * @param $name
                 * @return array
                 */
                private function edit($name){
                        return \Drupal::formBuilder()->getForm('Drupal\publisso_gold\Form\editUserTable', ['tableName' => $name]);
		}
                
                /**
                 * @param $name
                 * @return JsonResponse
                 */
                public function item($name){
                        
                        if(!(preg_match('/(.+)\|{2}(\d+)?\|{2}(.+)?\|{2}(.+)\[(.+)?\]/', $name, $matches))){
                                
                                $response = new JsonResponse();
                                $response->setContent((string)t('Error: Invalid pointer!'));
                                return $response;
                        }
                        
                        $tableName      = $matches[1];
                        $tableVersion   = $matches[2];
                        $searchColumn   = $matches[3];
                        $columnName     = $matches[4];
                        $columnValue    = $matches[5];
                        
                        if(!$this->tableExists($tableName)){
                                
                                $response = new JsonResponse();
                                $response->setContent((string)t('Error: Table @name not found!', ['@name' => $tableName]));
                                return $response;
                        }
                        
                        $table = $this->getTable($tableName);
                        $xpathTable = new \DOMXpath($table);
                        
                        $maxVersion = $xpathTable->evaluate('//meta//maxversion')[0]->nodeValue;
                        if($tableVersion < 0 || $tableVersion > $maxVersion){
                                $response = new JsonResponse();
                                $response->setContent((string)t('Error: Invalid version @version!', ['@version' => $tableVersion]));
                                return $response;
                        }
                        
                        if($xpathTable->evaluate('//datas//data[@version="'.$tableVersion.'"]')->length == 0){
                                $response = new JsonResponse();
                                $response->setContent((string)t('Error: Version @version not found!', ['@version' => $tableVersion]));
                                return $response;
                        }
                        
                        if($xpathTable->evaluate('//headers//header[@id="'.md5($columnName).'"]')->length == 0){
                                $response = new JsonResponse();
                                $response->setContent((string)t('Error: Column @name not found!', ['@name' => $columnName]));
                                return $response;
                        }
                        
                        if($xpathTable->evaluate('//headers//header[@id="'.md5($searchColumn).'"]')->length == 0){
                                $response = new JsonResponse();
                                $response->setContent((string)t('Error: Column @name not found!', ['@name' => $searchColumn]));
                                return $response;
                        }
                        
                        foreach($xpathTable->evaluate('//datas//data[@version="'.$tableVersion.'"]//row//cell[@for="'.md5($columnName).'"]') as $node){
                                
                                if($node->nodeValue == $columnValue){
                                        
                                        $weight = $node->parentNode->getAttribute('weight');
                                        $element = $xpathTable->evaluate('//datas//data[@version="'.$tableVersion.'"]//row[@weight="'.$weight.'"]//cell[@for="'.md5($searchColumn).'"]')[0];
                                        $response = new JsonResponse();
                                        $response->setContent($element->nodeValue);
                                        return $response;
                                }
                        }
                        
                        $response = new JsonResponse();
                        $response->setContent('n.a.');
                        return $response;
		}
                
                /**
                 * @param string $name
                 * @return array
                 */
                public function show($name = ''){
                        
                        if(!empty($name)) return $this->showSpecific($name);
                        
                        $param = $this->route['param'];
                        $param['action'] = 'create';
                        $url = Url::fromRoute($this->route['name'], $param);
                        $rows = $this->tables;
                        
                        foreach($rows as $id => $row){
                                
                                $url = Url::fromRoute($this->route['name'], ['name' => $row['name']]);
                                $rows[$id]['name'] = new FormattableMarkup('<a href=":link">@name</a>', [':link' => $url->toString(), '@name' => $row['name']]);
                        }
                        
                        return [
                                'table' => [
                                        '#type'         => 'table',
                                        '#empty'        => (string)t('No tables found!'),
                                        '#header'       => [
                                                'name'          => (string)t('Table'),
                                                'created'       => (string)t('Created'),
                                                'modified'      => (string)t('Modified'),
                                                'version'       => (string)t('Version')
                                        ],
                                        '#rows' => $rows
                                ],
                                'linkCreate'    => [
                                        '#type'         => 'link',
                                        '#title'        => (string)t('Create new table'),
                                        '#url'          => Url::fromRoute($this->route['name'], ['action' => 'create']),
                                        '#suffix'       => '<br><br><br>',
                                        '#attributes'   => [    
                                                'class'=> [
                                                        'btn', 'btn-warning'
                                                ]
                                        ]
                                ],
                                '#attached'     => [
                                        'library'       => [
                                                'publisso_gold/default'
                                        ]
                                ],
                                '#cache'        => [
                                        'max-age'       => 0
                                ]
                        ];
		}
                
                /**
                 * @param $name
                 * @return array
                 */
                public function showSpecific($name){
                        
                        if((!($table = $this->getTable($name)) === false) && (!($meta = $this->getTableMeta($name)) === false)){
                                
                                $tblAry = $this->getTableArray($table, $meta);
                                
                                $headers = [];
                                foreach($tblAry['headers'] as $id => $header){
                                        $headers[$id] = $header['title'];
                                }
                                
                                $rows = [];
                                
                                foreach($tblAry['rows'] as $row){
                                        $rows[] = $row;
                                }
                                
                                return array(
                                        'table' => [
                                                '#type' => 'table',
                                                '#header' => $headers,
                                                '#rows' => $rows,
                                                '#caption' => (string)t('Table @table', ['@table' => '»'.$name.'«'])
                                        ],
                                        'linkBack'    => [
                                                '#type'         => 'link',
                                                '#title'        => (string)t('Overview tables'),
                                                '#url'          => Url::fromRoute($this->route['name'], ['action' => 'show']),
                                                '#suffix'       => '&nbsp;&nbsp;&nbsp;',
                                                '#attributes'   => [
                                                        'class' => [
                                                                'btn', 'btn-warning'
                                                        ]
                                                ]
                                        ],
                                        'linkEdit'    => [
                                                '#type'         => 'link',
                                                '#title'        => (string)t('Edit table @table', ['@table' => '»'.$name.'«']),
                                                '#url'          => Url::fromRoute($this->route['name'], ['action' => 'edit', 'name' => $name]),
                                                '#suffix'       => '<br><br><br>',
                                                '#attributes'   => [
                                                        'class' => [
                                                                'btn', 'btn-warning'
                                                        ]
                                                ]
                                        ],
                                        '#attached' => [
                                                'library' => [
                                                'publisso_gold/default'
                                                ]
                                        ],
                                        '#cache' => [
                                                'max-age' => 0
                                        ]
                                );
                        }
                        
                        return array(
                                '#type' => 'markup',
                                '#markup' => (string)t('The table "@name" could not be found!', ['@name' => $name]),
                                '#attached' => [
                                        'library' => [
                                        'publisso_gold/default'
                                        ]
                                ],
                                '#cache' => [
                                        'max-age' => 0
                                ]
                        );
		}
                
                /**
                 * @param $name
                 * @return \DOMDocument|false
                 */
                public function getTable($name){
                        
                        if(is_file($this->userPath.'/tables.xml')){
                                
                                $tables = new \DOMDocument();
                                $tables->loadXML(file_get_contents($this->userPath.'/tables.xml'));
                                $xpath = new \DOMXpath($tables);
                                
                                $meta = null;
                                
                                foreach($this->tables as $_){
                                        
                                        if($_['name'] == $name) $meta = $_;
                                }
                                
                                $element = $xpath->evaluate('//object[@name="'.$name.'"]')[0];
                                
                                if($element && $meta){
                                        
                                        $table = new \DOMDocument('1.0', 'utf-8');
                                        $element = $table->importNode($element, true);
                                        $table->appendChild($element);
                                        return $table;
                                }
                        }
                        
                        return false;
		}
                
                /**
                 * @param $name
                 * @return mixed|null
                 */
                public function getTableMeta($name){
                        $meta = null;
                        
                        foreach($this->tables as $_){
                                
                                if($_['name'] == $name) $meta = $_;
                        }
                        
                        return $meta;
		}
                
                /**
                 * @param $table
                 * @param $meta
                 * @return array
                 */
                public function getTableArray($table, $meta){
                        
                        $xpath = new \DOMXpath($table);
                        $tblAry = [];
                        
                        foreach($xpath->evaluate('//headers/header') as $node){
                                $tblAry['headers'][$node->getAttribute('weight')]['id'] = $node->getAttribute('id');
                                $tblAry['headers'][$node->getAttribute('weight')]['title'] = trim($node->nodeValue);
                        }
                        
                        foreach($xpath->evaluate('//datas//data[@version="'.$meta['version'].'"]//row') as $node){
                                
                                $weight = $node->getAttribute('weight');
                                
                                foreach($xpath->evaluate('//datas//data[@version="'.$meta['version'].'"]//row[@weight="'.$weight.'"]/cell') as $cell){
                                        //echo '<pre>'.print_r($cell, 1).'</pre>';
                                        foreach($tblAry['headers'] as $index => $header){
                                                if($header['id'] == $cell->getAttribute('for')){
                                                        $tblAry['rows'][$weight][$index] = $cell->nodeValue;
                                                }
                                        }
                                }
                                
                                ksort($tblAry['rows'][$weight]);
                        }
                        
                        ksort($tblAry['headers']);
                        ksort($tblAry['rows']);
                        return $tblAry;
		}
                
                /**
                 * @return bool
                 */
                public function loadTables(){
                        
                        if(!is_dir($this->path)) mkdir($this->path, 0750, true);
                        if(!is_dir($this->path)) { error_log('Can\'t create directory "'.$this->path.'"'); return false; }
                        
                        if(is_dir($this->userPath) && is_file($this->userPath.'/tables.xml')){
                                
                                $tables = new \DOMDocument();
                                $tables->loadXML(file_get_contents($this->userPath.'/tables.xml'));
                                $xpath = new \DOMXpath($tables);
                                
                                foreach($xpath->evaluate('/objects/object[@name]') as $node){
                                        
                                        $name     = $node->getAttribute('name');
                                        $created  = $xpath->evaluate('/objects//object[@name="'.$name.'"]//meta//created'   )[0]->nodeValue;
                                        $modified = $xpath->evaluate('/objects//object[@name="'.$name.'"]//meta//created'   )[0]->nodeValue;
                                        $version  = $xpath->evaluate('/objects//object[@name="'.$name.'"]//meta//maxversion')[0]->nodeValue;
                                        
                                        $this->tables[] = [
                                                'name'     => trim($name),
                                                'created'  => trim($created),
                                                'modified' => trim($modified),
                                                'version'  => trim($version)
                                        ];
                                }
                        }
                        
                        return true;
		}
                
                /**
                 * @param $node
                 * @return false
                 */
                public function addTable($node){
                        
                        $table = new \DOMDocument();
                        $table->loadXML($node);
                        $xpathTable = new \DOMXpath($table);
                        $table = $xpathTable->evaluate('/object')[0];
                        
                        if(!is_file($this->userPath.'/tables.xml')){
                                
                                if(!is_dir($this->userPath)) mkdir($this->userPath, 0750, true);
                                if(!is_dir($this->userPath)) { error_log('Can\'t create directory "'.$this->userPath.'"'); return false; }
                                
                                $doc = new \DOMDocument('1.0', 'utf-8');
                                $doc->appendChild($doc->createElement('objects'));
                                $doc->normalizeDocument();
                                file_put_contents($this->userPath.'/tables.xml', $doc->saveXML());
                        }
                        
                        $doc = new \DOMDocument();
                        $doc->loadXML(file_get_contents($this->userPath.'/tables.xml'));
                        $xpath = new \DOMXpath($doc);
                        
                        $element = $xpath->evaluate('/objects');
                        $element[0]->appendChild($doc->importNode($table, true));
                        
                        $doc->normalizeDocument();
                        file_put_contents($this->userPath.'/tables.xml', $doc->saveXML());
		}
                
                /**
                 * @param $name
                 * @param $node
                 */
                public function modifyTable($name, $node){
                        
                        $meta = $this->getTableMeta($name);
                        
                        $tables = new \DOMDocument();
                        $tables->loadXML(file_get_contents($this->userPath.'/tables.xml'));
                        $xpath = new \DOMXpath($tables);
                        
                        $newNode = new \DOMDocument();
                        $newNode->loadXML($node);
                        $newXpath = new \DOMXpath($newNode);
                        
                        $element = $xpath->evaluate('/objects/object[@name="'.$name.'"]//meta/maxversion')[0];
                        $element->nodeValue = $meta['version'] + 1;
                        
                        $element = $xpath->evaluate('/objects/object[@name="'.$name.'"]//meta/modified')[0];
                        $element->nodeValue = date('Y-m-d H:i:s');
                        
                        $element = $newXpath->evaluate('/data')[0];
                        $element->setAttribute('version', $meta['version'] + 1);
                        
                        $datas = $xpath->evaluate('/objects/object[@name="'.$name.'"]//datas')[0];
                        $datas->appendChild($tables->importNode($element, 1));
                        
                        $tables->normalizeDocument();
                        file_put_contents($this->userPath.'/tables.xml', $tables->saveXML());
		}
                
                /**
                 * @param $name
                 * @return bool
                 */
                public function tableExists($name){
                        
                        foreach($this->tables as $table){
                                
                                if($table['name'] == $name){
                                        return true;
                                        break;
                                }
                        }
                        
                        return false;
                }
	}
?>
