<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\Autocomplete.
 */

namespace Drupal\publisso_gold\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Unicode;
use Drupal;

/**
 * Returns autocomplete responses.
 */
class Autocomplete {

	/**
	* Returns response for the country name autocompletion.
	*
	* @param \Symfony\Component\HttpFoundation\Request $request
	*   The current request object containing the search string.
	*
	* @return \Symfony\Component\HttpFoundation\JsonResponse
	*   A JSON response containing the autocomplete suggestions for countries.
	*/
	public function country(Request $request) {
		
		$matches = array();
		$string = $request->query->get('q');
		
		if($string) {
			
			$countries = \Drupal::service('country_manager')->getList();
			
			foreach ($countries as $iso2 => $country) {
				
				if (strpos(Unicode::strtolower($country), Unicode::strtolower($string)) !== FALSE) {
					$matches[] = array('value' => $country, 'label' => $country);
				}
			}
		}
		return new JsonResponse($matches);
	}
        
        /**
         * @param Request $request
         * @return JsonResponse
         */
        public function user(Request $request){
	
                $matches = array();
                $string = $request->query->get('q');
                
                if(strlen($string) > 2){
                        
                        $select = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', ['up_lastname', 'up_firstname', 'up_uid']);
                        
                        $condition = $select->orConditionGroup()
                                ->condition('up_lastname', $string.'%', 'LIKE')
                                ->condition('up_firstname', $string.'%', 'LIKE');
                                
                        $result = $select->condition($condition)->execute()->fetchAll();
                        
                        foreach($result as $k => $v){
                                array_push($matches, (object)['value' => strtoupper(base_convert($v->up_uid, 10, 36)) . ' | ' . $v->up_firstname.' '.$v->up_lastname, 'label' => $v->up_firstname.' '.$v->up_lastname]);
                        }
                }
                return new JsonResponse($matches);
	}
        
        /**
         * @param Request $request
         * @return JsonResponse
         */
        public function user_public(Request $request){
		
                \Drupal::moduleHandler()->loadInclude('php', 'inc/publisso_gold.lib.inc', 'publisso_gold');
                require_once(getcwd().'/'.drupal_get_path('module', 'publisso_gold').'/inc/publisso_gold.lib.inc.php');
		$matches = array();
		$string = $request->query->get('q');
		
		if(strlen($string) > 2){
			
			$select = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', ['up_lastname', 'up_firstname', 'up_uid', 'up_department', 'up_institute', 'up_country']);
			
			$condition = $select->orConditionGroup()
				->condition('up_lastname', $string.'%', 'LIKE')
				->condition('up_firstname', $string.'%', 'LIKE');
				
			$result = $select->condition($condition)->execute()->fetchAll();
			
			foreach($result as $k => $v){
                                $text = implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)]));
				array_push($matches, (object)['value' => $text, 'label' => $text]);
			}
		}
		return new JsonResponse($matches);
	}
        
        /**
         * @param Request $request
         * @return JsonResponse
         */
        public function user_public_new(Request $request){
		
		$matches = array();
		$string = $request->query->get('q');
		
		if(strlen($string) > 2){
			
			$select = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', ['up_lastname', 'up_firstname', 'up_uid', 'up_department', 'up_institute', 'up_country']);
			
			$condition = $select->orConditionGroup()
					    ->condition('up_lastname', $string.'%', 'LIKE')
					    ->condition('up_firstname', $string.'%', 'LIKE');
			
			$result = $select->condition($condition)->execute()->fetchAll();
			
			foreach($result as $k => $v){
				
				$name = implode(', ', [$v->up_lastname, $v->up_firstname]); //use a non-printable zero-width non-joiner for split in first- and lastname
				$text = implode('; ', array_filter([$name, $v->up_department, $v->up_institute, Publisso::tools()->getCountry($v->up_country)]));
				array_push($matches, (object)['value' => $text, 'label' => $text]);
			}
		}
		return new JsonResponse($matches);
	}
}
?>
