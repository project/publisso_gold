<?php

	/**
	 * @file
	 * Contains \Drupal\publisso_gold\Controller\FormFields.
	 */

	namespace Drupal\publisso_gold\Controller;
	use Drupal\Core\StringTranslation\StringTranslationTrait;
        use Drupal\Core\StringTranslation\TranslationInterface;
        use \Drupal\publisso_gold\Controller\Texts;
        
        /**
         * Class FormFields
         * @package Drupal\publisso_gold\Controller
         */
        class FormFields{
                
                use StringTranslationTrait;
		
		public function __construct(TranslationInterface $string_translation, Texts $texts) {
                        $this->stringTranslation = $string_translation;
                        $this->texts = $texts;
                }
                
                /**
                 * @return string
                 */
                public function __toString(){
			return 'FormFields';
		}
                
                /**
                 * @param $field
                 * @param array $args
                 * @return string[]
                 */
                public function getField($field, $args = []){
                        
                        if(!is_array($args['strings'])) $args['strings'] = [];
                        
                        list($form, $field) = explode('.', $field);
                        
                        if(method_exists($this, $form)){
                                return $this->$form($field, $args);
                        }
                        else{
                                return [
                                        '#markup' => (string)$this->t($this->texts->get('global.message.system.missing_formfield'), ['@form' => $form, '@field' => $field]),
                                        '#prefix' => '<div class="alert alert-danger">',
                                        '#suffix' => '</div>'
                                ];
                        }
		}
                
                /**
                 * @param $field
                 * @param $args
                 * @return array
                 */
                private function submitchapter($field, $args){
                        
                        switch($field){
                                
                                case 'author_add':
                                        return [
                                                '#type'  => 'fieldset',
                                                '#tree'  => true,
                                                '#title' => (string)$this->t($this->texts->get('form.submitchapter.author_add.title', 'fc'), $args['strings'])
                                        ];
                                        break;
                                
                                case 'author_add_firstname':
                                        return [
                                                '#type' => 'textfield',
                                                '#title' => (string)$this->t($this->texts->get('form.submitchapter.author_add_firstname.title', 'fc'), $args['strings']),
                                                '#placeholder' => (string)$this->t($this->texts->get('form.submitchapter.author_add_firstname.placeholder', 'fc'), $args['strings']),
                                                '#ajax'         => [
                                                        'event'         => 'change',
                                                        'prevent'       => 'blur',
                                                        'progress'      => [
                                                                'type'          => 'none'
                                                        ],
                                                        'callback'      => '::autosave',
                                                        'disable-refocus'       => true
                                                ]
                                        ];
                                        break;
                                
                                case 'author_add_lastname':
                                        return [
                                                '#type' => 'textfield',
                                                '#title' => (string)$this->t($this->texts->get('form.submitchapter.author_add_lastname.title', 'fc'), $args['strings']),
                                                '#placeholder' => (string)$this->t($this->texts->get('form.submitchapter.author_add_lastname.placeholder', 'fc'), $args['strings']),
                                                '#ajax'         => [
                                                        'event'         => 'change',
                                                        'prevent'       => 'blur',
                                                        'progress'      => [
                                                                'type'          => 'none'
                                                        ],
                                                        'callback'      => '::autosave',
                                                        'disable-refocus'       => true
                                                ]
                                        ];
                                        break;
                                
                                case 'author_add_affiliation':
                                        return [
                                                '#type' => 'textfield',
                                                '#title' => (string)$this->t($this->texts->get('form.submitchapter.author_add_affiliation.title', 'fc'), $args['strings']),
                                                '#placeholder' => (string)$this->t($this->texts->get('form.submitchapter.author_add_affiliation.placeholder', 'fc'), $args['strings']),
                                                '#maxlength' => 300,
                                                '#ajax'         => [
                                                        'event'         => 'change',
                                                        'prevent'       => 'blur',
                                                        'progress'      => [
                                                                'type'          => 'none'
                                                        ],
                                                        'callback'      => '::autosave',
                                                        'disable-refocus'       => true
                                                ]
                                        ];
                                        break;
                                
                                case 'author_add_weight':
                                        return [
                                                '#type'          => 'number',
                                                '#size'          => 3,
                                                '#title'         => (string)$this->t($this->texts->get('form.submitchapter.author_add_weight.title', 'fc'), $args['strings']),
                                                '#ajax'         => [
                                                        'event'         => 'change',
                                                        'prevent'       => 'blur',
                                                        'progress'      => [
                                                                'type'          => 'none'
                                                        ],
                                                        'callback'      => '::autosave',
                                                        'disable-refocus'       => true
                                                ]
                                        ];
                                        break;
                                
                                default:
                                        \Drupal::service('messenger')->addError(
                                                (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.message.system.missing_formfield'), ['@form' => __FUNCTION__, '@field' => $field])
                                        );
                                        
                                        return [];
                        }
		}
                
                /**
                 * @param $field
                 * @param $args
                 * @return array|string[]
                 */
                private function add_pp_information($field, $args){
                        
                        switch($field){
                                
                                case 'orig_text':
                                        $pp = new \Drupal\publisso_gold\Controller\PrivacyProtection(
                                                \Drupal::Service('session'),
                                                \Drupal::service('publisso_gold.texts'),
                                                \Drupal::service('publisso_gold.tools')
                                        );
                                        
                                        return [
                                                '#type' => 'details',
                                                '#title' => (string)$this->t(
                                                        $this->texts->get(
                                                                'form.add_pp_information.orig_text.title',
                                                                'fc'
                                                        ),
                                                        $args['strings']
                                                ),
                                                'content' => [
                                                        '#type' => 'inline_template',
                                                        '#template' => $pp->getHTMLFromDate($args['date'])
                                                ]
                                        ];
                                        break;
                                
                                case 'language':
                                        $languages = [];
                                        foreach(\Drupal::service('language_manager')->getLanguages() as $key => $lang)
                                                $languages[$key] = (string)$this->t($lang->getName());
                                        
                                        return [
                                                '#type' => 'select',
                                                '#title' => (string)$this->t(
                                                        $this->texts->get(
                                                                'form.add_pp_information.language.title',
                                                                'fc'
                                                        ),
                                                        $args['strings']
                                                ),
                                                '#required' => true,
                                                '#options' => $languages
                                        ];
                                        break;
                                
                                case 'valid_from':
                                        return [
                                                '#type'  => 'date',
                                                '#title' => (string)$this->t($this->texts->get('form.add_pp_information.valid_from.title', 'fc'), $args['strings']),
                                                '#required' => true
                                        ];
                                        break;
                                
                                case 'description':
                                        return [
                                                '#type' => 'textfield',
                                                '#title' => (string)$this->t($this->texts->get('form.add_pp_information.description.title', 'fc'), $args['strings']),
                                                '#placeholder' => (string)$this->t($this->texts->get('form.add_pp_information.description.placeholder', 'fc'), $args['strings'])
                                        ];
                                        break;
                                
                                case 'text':
                                        return [
                                                '#type' => 'textarea',
                                                '#title' => (string)$this->t($this->texts->get('form.add_pp_information.text.title', 'fc'), $args['strings']),
                                                '#required' => true,
                                                '#attributes' => [
                                                        'class' => [
                                                        	\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
                                                        ]
                                                ]
                                        ];
                                        break;
                                
                                default:
                                        \Drupal::service('messenger')->addError(
                                                (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.message.system.missing_formfield'), ['@form' => __FUNCTION__, '@field' => $field])
                                        );
                                        
                                        return [];
                        }
		}
                
                /**
                 * @param $field
                 * @param $args
                 * @return array
                 */
                private function agree_pp_information($field, $args){
                        
                        switch($field){
                                
                                case 'agree':
                                        return [
                                                '#type'  => 'checkbox',
                                                '#title' => (string)$this->t($this->texts->get('form.agree_pp_information.agree.title', 'fc'), $args['strings']),
                                                '#required' => true
                                        ];
                                        break;
                                
                                default:
                                        \Drupal::service('messenger')->addError(
                                                (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.message.system.missing_formfield'), ['@form' => __FUNCTION__, '@field' => $field])
                                        );
                                        
                                        return [];
                        }
                }
                
                /**
                 * @param $field
                 * @param $args
                 * @return array
                 */
                private function add_newsletter($field, $args){
                        
                        switch($field){
                                
                                case 'recipients':
                                        
                                        return [
                                                '#type'     => 'select',
                                                '#title'    => (string)$this->t($this->texts->get('form.'.__FUNCTION__.'.recipients.title', 'fc')),
                                                '#options'  => [
                                                        'all'            => (string)$this->t($this->texts->get('form.'.__FUNCTION__.'.recipients.all'           , 'fco')),
                                                        'administrators' => (string)$this->t($this->texts->get('form.'.__FUNCTION__.'.recipients.administrators', 'fco')),
                                                        'publisso'       => (string)$this->t($this->texts->get('form.'.__FUNCTION__.'.recipients.publisso'      , 'fco')),
                                                        'authors'        => (string)$this->t($this->texts->get('form.'.__FUNCTION__.'.recipients.authors'       , 'fco')),
                                                        'manual'         => (string)$this->t($this->texts->get('form.'.__FUNCTION__.'.recipients.manual'        , 'fco'))
                                                ],
                                                '#required' => true
                                        ];
                                        break;
                                
                                case 'recipients-extra':
                                        return [
                                                '#type'        => 'textarea',
                                                '#rows'        => 2,
                                                '#title'       => (string)$this->t($this->texts->get('form.'.__FUNCTION__.'.recipients-extra.title', 'fco')),
                                                '#placeholder' => (string)$this->t($this->texts->get('form.'.__FUNCTION__.'.recipients-extra.placeholder', 'fco')),
                                        ];
                                        break;
                                
                                case 'subject':
                                        return [
                                                '#type'        => 'textfield',
                                                '#maxlength'    => 256,
                                                '#title'       => (string)$this->t($this->texts->get('form.'.__FUNCTION__.'.subject.title', 'fco')),
                                                '#required'    => true
                                        ];
                                        break;
                                
                                case 'body':
                                        return [
                                                '#type'     => 'textarea',
                                                '#required' => true,
                                                '#rows'     => 20,
                                                '#title'    => (string)$this->t($this->texts->get('form.'.__FUNCTION__.'.body.title', 'fco')),
                                                
                                        ];
                                        break;
                                
                                default:
                                        \Drupal::service('messenger')->addError(
                                                (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.message.system.missing_formfield'), ['@form' => __FUNCTION__, '@field' => $field])
                                        );
                                        
                                        return [];
                        }
                }
                
                /**
                 * @param $field
                 * @param $args
                 * @return array|string[]
                 */
                private function userprofile_delete($field, $args){
                        
                        switch($field){
                                
                                case 'message_active_processes':
                                        return [
                                                '#markup' => $this->t(
                                                        \Drupal::service('publisso_gold.texts')->get('form.userprofile.delete.msg.active_processes')
                                                ),
                                                '#prefix' => '<div class="alert alert-danger">',
                                                '#suffix' => '</div>'
                                        ];
                                        break;
                                
                                case 'message_warning_delete':
                                        return [
                                                '#markup' => $this->t(
                                                        \Drupal::service('publisso_gold.texts')->get('form.userprofile.delete.msg.warning_delete')
                                                ),
                                                '#prefix' => '<div class="alert alert-warning">',
                                                '#suffix' => '</div>'
                                        ];
                                        break;
                                
                                default:
                                        return [
                                                '#markup' => (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.message.system.missing_formfield'), ['@form' => __FUNCTION__, '@field' => $field]),
                                                '#prefix' => '<div class="alert alert-danger">',
                                                '#suffix' => '</div>'
                                        ];
                        }
                }
        }
?>
