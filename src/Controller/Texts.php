<?php

	/**
	 * @file
	 * Contains \Drupal\publisso_gold\Controller\Texts.
	 */

	namespace Drupal\publisso_gold\Controller;

	use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Link;
        use Drupal\Core\Render\Markup;
        use Drupal\Core\Url;
        
        /**
         * Class Texts
         * @package Drupal\publisso_gold\Controller
         */
        class Texts extends ControllerBase {
		
		private $texts;
        
                /**
                 * @return string
                 */
                public function __toString(){
			return 'Texts';
		}
        
                /**
                 * @param $text
                 * @return bool
                 */
                private function textExists($text){
                        
                        return !!\Drupal::database()->select('rwPubgoldTexts', 't')->fields('t', ['id'])
                                ->condition('id', $text, '=')->countQuery()->execute()->fetchField();
		}
        
                /**
                 * @param $text
                 * @return bool
                 */
                private function textIsset($text){
		        return !!\Drupal::database()->select('rwPubgoldTexts', 't')->fields('t', [])->condition('id', $text, '=')->isNotNull('text')->countQuery()->execute()->fetchField();
                }
        
                /**
                 * @param $text
                 * @throws \Exception
                 */
                private function createText($text){
                        
                        \Drupal::database()->insert('rwPubgoldTexts')->fields(['id' => $text])->execute();
                        $this->texts[$text] = $text;
		}
		
		public function __construct(){
                        
                       $this->texts = []; 
                       
                       $res = \Drupal::database()->select('rwPubgoldTexts', 't')->fields('t', [])->execute()->fetchAll();
                       
                       foreach($res as $_){ $this->texts[$_->id] = $_->text; }
		}
        
                /**
                 * @param $text
                 * @param string $mod
                 * @return mixed|string
                 */
                public function get($text, $mod = ''){
                        
                        $account = \Drupal::currentUser();
                        $session = \Drupal::service('session');
                        
                        if(!$this->textExists($text)){
                                $this->createText($text);
                                
                                if($account->getAccount()->hasPermission('administer content') || ($session->has('user') && $session->get('user')['weight'] > 70))
                                        \Drupal::service('messenger')->addWarning($this->t('Missing text "@text", created new one! Please set the desired content in @link section!', ['@text' => $text, '@link' => Link::fromTextAndUrl('Admin', Url::fromRoute('publisso_gold.siteadmin'))->toString()]));
                        }
                        elseif(!$this->textIsset($text)){
        
                                if($account->getAccount()->hasPermission('administer content') || ($session->has('user') && $session->get('user')['weight'] > 70))
                                        \Drupal::service('messenger')->addWarning($this->t('Missing text "@text"! Please set the desired content in @link section!', ['@text' => $text, '@link' => Link::fromTextAndUrl('Admin', Url::fromRoute('publisso_gold.siteadmin'))->toString()]));
                        }
                        
                        $text =  array_key_exists($text, $this->texts) && $this->texts[$text] !== null ? $this->texts[$text] : $text;
                        
                        switch(strtoupper($mod)){
                                
                                case 'AC': //capitals
                                        $text = strtoupper($text);
                                        break;
                                
                                case 'AL': //lower
                                        $text = strtolower($text);
                                        break;
                                
                                case 'FC': //first capital, others lowercase
                                        $text = explode(' ', $text);
                                        $text[0] = ucfirst($text[0]);
                                        for($i = 1; $i < count($text); $i++) $text[$i] = strtolower($text[$i]);
                                        $text = implode(' ', $text);
                                        break;
                                
                                case 'FCO': //first capital, others as is
                                        $text = explode(' ', $text);
                                        $text[0] = ucfirst($text[0]);
                                        $text = implode(' ', $text);
                                        break;
                                
                                case 'AFC': //all first capital
                                        $text = explode(' ', $text);
                                        $text = array_map('ucfirst', $text);
                                        $text = implode(' ', $text);
                                        break;
                        }
                        
                        return $text;
		}
        
                /**
                 * @return array
                 */
                public function getKeys(){
                        return array_keys($this->texts);
		}
		
		public function debug(){
                        echo '<pre>'.print_r($this, 1).'</pre>'; exit();
		}
        
                /**
                 * @param $key
                 * @param $value
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 */
                public function update($key, $value){
                        
                        return \Drupal::database()->update('rwPubgoldTexts')->fields([
                                'text' => $value
                        ])->condition('id', $key, '=')->execute();
		}
	}
?>
