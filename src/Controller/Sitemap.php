<?php
        
        /**
         * @file
         * Contains \Drupal\publisso_gold\Controller\Sitemap.
         */
        
        namespace Drupal\publisso_gold\Controller;
        
        /**
         * Class Sitemap
         * @package Drupal\publisso_gold\Controller
         */
        class Sitemap {
                
                private $id                        = false;
                private $tbl_column_prefix         = 'sm_';
                private $items                     = [];
                private $structure                 = [];
                private $structure_processed_items = 1; //first Level always set
                private $path                      = [];
                
                public function __construct () {
                        $select = \Drupal::database()->select( 'rwPubgoldSitemap', 'sm' )->fields( 'sm', [] );
                        $result = $select->orderBy( $this->tbl_column_prefix . 'parent' )->execute()->fetchAll();
                        
                        if ( !count( $result ) ) return false;
                        
                        foreach ( $result as $item ) {
                                $this->items[] = new SitemapItem( $item );
                        }
                        
                        $this->createStructure();
                }
                
                private function createStructure () {
                        
                        $i = 0;
                        $rootAdded = false;
                        
                        while ( $this->structure_processed_items < count( $this->items ) && $i < count( $this->items ) * 10 ) {
                                foreach ( $this->items as $item ) {
                                        
                                        if ( empty( $item->getElement( 'parent' ) ) ) {
                                                
                                                if ( $rootAdded === false ) {
                                                        $this->structure[] = [ 'item' => $item, 'childs' => [] ];
                                                        $rootAdded = true;
                                                }
                                        }
                                        else {
                                                
                                                $this->addChild( $item, $this->structure, 1 );
                                        }
                                }
                                
                                $i++;
                        }
                }
        
                /**
                 * @param $item
                 * @param $array
                 * @param $level
                 */
                private function addChild (&$item, &$array, $level ) {
                        
                        foreach ( $array as $k => $v ) {
                                
                                if ( $v[ 'item' ]->getElement( 'id' ) == $item->getElement( 'parent' ) ) {
                                        
                                        $item->level = $level;
                                        
                                        $hasChild = false;
                                        
                                        foreach ( $array[ $k ][ 'childs' ] as $_ ) {
                                                if ( $_[ 'item' ]->getElement( 'id' ) == $item->getElement( 'id' ) ) {
                                                        $hasChild = true;
                                                        break;
                                                }
                                        }
                                        
                                        if ( $hasChild === false && !empty( $item->getElement( 'parent' ) ) ) {
                                                $array[ $k ][ 'childs' ][] = [ 'item' => $item, 'childs' => [] ];
                                                
                                                $this->structure_processed_items++;
                                        }
                                        
                                        break;
                                }
                                
                                if ( is_array( $v[ 'childs' ] ) ) {
                                        $this->addChild( $item, $array[ $k ][ 'childs' ], $level + 1 );
                                }
                        }
                }
        
                /**
                 * @return string
                 */
                public function __toString () {
                        return "class Sitemap";
                }
        
                /**
                 * @return array
                 */
                public function readStructure () {
                        return $this->structure;
                }
        
                /**
                 * @param $url
                 * @return mixed|null
                 */
                public function getPath ($url ) {
                        
                        $this->seekUrl( $url, $this->structure, 1 );
                        //array_unshift($this->path['path'], $this->structure[0]['item']);
                        return $this->path[ 'path' ] ?? null;
                }
        
                /**
                 * @param $url
                 * @param $structure
                 * @param $level
                 */
                private function seekUrl ($url, $structure, $level ) {
                        
                        foreach ( $structure as $item ) {
                                
                                if ( $level >= 2 ) {
                                        //$this->path['tmp'] = [];
                                        array_splice( $this->path[ 'tmp' ], $level - 1 );
                                }
                                
                                if ( $url == $item[ 'item' ]->getElement( 'url' ) ) {
                                        $this->path[ 'tmp' ][] = $item[ 'item' ];
                                        $this->path[ 'path' ] = $this->path[ 'tmp' ];
                                }
                                else {
                                        
                                        if ( count( $item[ 'childs' ] ) > 0 ) {
                                                $this->path[ 'tmp' ][] = $item[ 'item' ];
                                                $this->seekUrl( $url, $item[ 'childs' ], $level + 1 );
                                        }
                                }
                        }
                }
        }

?>
