<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\LoginProvider.
 */

namespace Drupal\publisso_gold\Controller;
use Drupal\publisso_gold\Controller\Manager\UserManager;

/**
 * Class LoginProvider
 * @package Drupal\publisso_gold\Controller
 */
class LoginProvider{
        
        private $stringTranslation;
        private $session;
        private $statusMessage;
        
        public function __construct() {
                $this->stringTranslation = \Drupal::translation();
                $this->session = \Drupal::service('session');
                if(!$this->session->isStarted()) $this->session->start();
                $this->statusMessage = null;
        }
        
        /**
         * @param string $username
         * @param string $password
         * @return bool
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        public function login(string $username, string $password){
                
                Publisso::tools()->logMsg("Login attempt [$username:********]");
                
                if(Publisso::setup()->getValue('system.sso.active') && \Drupal\publisso_sso\Classes\SSO::ssoAvailable()){

                        $ssoUD = \Drupal\publisso_sso\Classes\SSO::getUserData($username, $password);
                
                        if($ssoUD->getLastCode() != 200){
                                
                                $this->statusMessage = $ssoUD->getLastMessage();
                                Publisso::tools()->logMsg("SSO-Login failed [$username:********]: ".$this->statusMessage);
                                
                                switch(strtoupper($ssoUD->getLastMessage())){
                                        
                                        case 'BAD_PASSWORD':
                                                Publisso::tools()->logMsg("SSO-Login failed [$username:********]: ".$ssoUD->getLastMessage());
                                                return false;
                                                break;
                                                
                                        case 'BAD_USER_ID':
                                                Publisso::tools()->logMsg('SSO-user "'.$username.'" does not exists! Try to guess local-user...');
                                                $ids = UserManager::getIDsByField(['user' => $username]);
                                                
                                                if(!count($ids) == 1){ Publisso::tools()->logMsg('Can\'t guess user! Result gives none or more than one ID\'s'); return false; }
                                                
                                                $user = UserManager::getUserFromID($ids[0]);
                                                
                                                if($user->getElement('zbmed_kennung')){ //try to auth witch 'zbmed_kennung' if available
                                                        
                                                        Publisso::tools()->logMsg('Try to auth against SSO with '.$user->getElement('zbmed_kennung'));
                                                        $ssoUD = \Drupal\publisso_sso\Classes\SSO::getUserData($user->getElement('zbmed_kennung'), $password);
                                                        $this->statusMessage = $ssoUD->getLastMessage();
                                                        
                                                        if($ssoUD->getLastCode() == 200){
                                                                $this->updateUserData($user, $ssoUD->getLastDatas(), $password);
                                                                $this->createSession($user, $password);
                                                                return true;
                                                        }
                                                        else{
                                                                $this->statusMessage = $ssoUD->getLastMessage();
                                                                Publisso::tools()->logMsg("SSO-Login failed [$username:********]: ".$this->statusMessage);
                                                        }
                                                }
                                                else{
                                                        //try to get local user
                                                        //if none exists - return false
                                                        //otherwise try to create at sso
                                                        Publisso::tools()->logMsg('Try to authenticate local...');
                                                        $user = UserManager::getUserFromNameAndPassword($username, $password);
                                                        
                                                        if(!$user){
                                                                Publisso::tools()->logMsg("Login failed [$username:********]: Bad Password");
                                                                return false;
                                                        }
        
                                                        $ssoCU = \Drupal\publisso_sso\Classes\SSO::createUser($user->getElement('user'), $password, [
                                                                'institut' => $user->profile->getElement('institute'),
                                                                'LKURZ' => preg_match('/^\d+$/', $user->profile->getElement('country')) ? array_search(Publisso::tools()->getCountry($user->profile->getElement('country')), Publisso::tools()->getCountrylist()) : $user->profile->getElement('country'),
                                                                'LORT' => $user->profile->getElement('city'),
                                                                'name' => $user->profile->getReadableName(),
                                                                'LSTRAS' => $user->profile->getElement('street'),
                                                                'LPZAHL' => $user->profile->getElement('postal_code'),
                                                                'LTELNR' => $user->profile->getElement('telephone'),
                                                                'LSPRACHE' => $user->profile->getElement('correspondence_language'),
                                                                'LLAND' => Publisso::tools()->getCountry($user->profile->getElement('country')),
                                                                'PRGRP' => 'z2',
                                                                'LEMAILADR' => $user->profile->getElement('email')
                                                        ]);
                                                        
                                                        if($ssoCU->getLastCode() != 200){
                                                                Publisso::tools()->logMsg('Can\'t create SSO user from local. ErrMsg: '.$ssoCU->getLastMessage());
                                                        }
                                                        else {
                                                                preg_match( '/K\d+$/', $ssoCU->getLastMessage(), $matches );
                                                                $user->setElement( 'zbmed_kennung', $matches[ 0 ] );
                                                                $user->setElement( 'initial_email', $user->getElement( 'user' ) );
                                                        }
                                                        $this->createSession($user, $password);
                                                }
                                                break;
                                                
                                        default:
                                                $this->statusMessage = $ssoUD->getLastMessage();
                                                Publisso::log('Nix is...');
                                                return false;
                                }
                        }
                        else{ //sso-login successful
                                
                                $ids = UserManager::getIDsByField(['zbmed_kennung' => $ssoUD->getLastData('KENNUNG')]);
                                
                                switch(count($ids)){
                                        case 0:
                                                $names = explode(' ', $ssoUD->getLastData('name'));
                                                $lastname = array_pop($names);
                                                $firstname = implode(' ', $names);
                                                
                                                $user = UserManager::createUser($username, $password, $firstname, $lastname, $ssoUD->getLastData('LEMAILADR') ?? $ssoUD->getLastData('remote_id'), [
                                                        'initial_email' => $ssoUD->getLastData('remote_id'),
                                                        'zbmed_kennung' => $ssoUD->getLastData('KENNUNG')
                                                ]);
                                                
                                                if($user) $this->createSession($user, $password);
                                                break;
                                                
                                        case 1:
                                                $user = UserManager::getUserFromID($ids[0]);
                                                $this->updateUserData($user, $ssoUD->getLastDatas(), $password);
                                                $this->createSession($user, $password);
                                                break;
                                                
                                        default:
                                                Publisso::tools()->logMsg('Can\'t guess user for update from sso! Result gives more than one ID\'s');
                                }
                                
                                
                                
                                
                        }
                }
                else{
                        Publisso::tools()->logMsg("Login locally...");
                        
                        if(null === ($user = UserManager::getUserFromNameAndPassword($username, $password))){
                                Publisso::tools()->logMsg("Login failed [$username:$password]: Bad username/password");
                                return false;
                        }
                        
                        $this->createSession($user, $password);
                }
                
                return true;
        }
        
        /**
         * @param User $user
         * @param array $data
         * @param string $password
         */
        private function updateUserData(User $user, array $data, string $password){
                
                $user->setElement('initial_email', $data['remote_id'], false);
                $user->setElement('locked', $data['SPERR'] == 'n' ? '0' : '1', false);
                
                $user->profile->setElement('city', $data['LORT'], false);
                
                list($lastname, $firstname) = preg_split('/\s*,\s*/', $data['name']);
                foreach(['lastname', 'firstname'] as $_) $user->profile->setElement($_, $$_, false);
                
                if(array_key_exists('LEMAILADR', $data)) $user->profile->setElement('email'                     , $data['LEMAILADR'     ], false);
                if(array_key_exists('LSPRACHE' , $data)) $user->profile->setElement('correspondence_language'   , $data['LSPRACHE'      ], false);
                if(array_key_exists('LKURZ'    , $data)) $user->profile->setElement('country'                   , $data['LKURZ'         ], false);
                if(array_key_exists('LPZAHL'   , $data)) $user->profile->setElement('postal_code'               , $data['LPZAHL'        ], false);
                if(array_key_exists('LSTRAS'   , $data)) $user->profile->setElement('street'                    , $data['LSTRAS'        ], false);
                if(array_key_exists('LTELNR'   , $data)) $user->profile->setElement('telephone'                 , $data['LTELNR'        ], false);
                
                $user->save();
                $user->profile->save();
                
                $user->setPassword($password);
        }
        
        /**
         * @return null
         */
        public function getLastMessage(){
                return $this->statusMessage;
        }
        
        /**
         * @param User $user
         * @param false $password
         * @return bool
         */
        private function createSession(\Drupal\publisso_gold\Controller\User $user, $password = false){
                
                if($user->getElement('locked') == 1){
                        $this->statusMessage = 'Account locked!';
                        Publisso::tools()->logMsg("Login failed: Account locked");
                        return false;
                }
                
                if($user->getElement('deleted') == 1){
                        $this->statusMessage = 'Account not available';
                        Publisso::tools()->logMsg("Login failed: Account deleted");
                        return false;
                }
                
                $sUser = array();
                
                $sUser['name'     ] = $user->getElement('user');
                $sUser['role'     ] = $user->role['name'];
                $sUser['id'       ] = $user->getElement('id');
                $sUser['weight'   ] = $user->role['weight'];
                $sUser['pw_change'] = $user->getElement('pw_change_required');
                $sUser['role_id'  ] = $user->role['id'];
                
                if($password)
                $sUser['password' ] = $password;
                
                $sUser['isBookEditor']       = Publisso::tools()->isUserBookEditor($user->getElement('id'));
                $sUser['isBookEiC']          = Publisso::tools()->isUserBookEiC($user->getElement('id'));
                $sUser['isBookEO']           = Publisso::tools()->isUserBookEO($user->getElement('id'));
                
                $sUser['isJournalEditor']    = Publisso::tools()->isUserJournalEditor($user->getElement('id'));
                $sUser['isJournalEiC']       = Publisso::tools()->isUserJournalEiC($user->getElement('id'));
                $sUser['isJournalEO']        = Publisso::tools()->isUserJournalEO($user->getElement('id'));
                
                $sUser['isConferenceEditor'] = Publisso::tools()->isUserConferenceEditor($user->getElement('id'));
                $sUser['isConferenceEiC']    = Publisso::tools()->isUserConferenceEiC($user->getElement('id'));
                $sUser['isConferenceEO']     = Publisso::tools()->isUserConferenceEO($user->getElement('id'));
                
                $sUser['isEditor']   = $sUser['isBookEditor'] | $sUser['isJournalEditor'] | $sUser['isConferenceEditor'];
                $sUser['isEiC']      = $sUser['isBookEiC'] | $sUser['isJournalEiC'] | $sUser['isConferenceEiC'];
                $sUser['isEO']       = $sUser['isBookEO'] | $sUser['isJournalEO'] | $sUser['isConferenceEO'];
                
                $_SESSION['user'] = $sUser;
                $_SESSION['logged_in'] = true;
                
                $this->session->set('user', $sUser);
                $this->session->set('logged_in', true);
                Publisso::tools()->logMsg('Session startet for '.$user->getElement('id').': '.$user->profile->getReadableFullName());
                return true;
        }
        
        public function logout(){
                
                $user = new User($this->session->get('user')['id']);
                Publisso::tools()->logMsg('Session terminated for '.$user->getElement('id').': '.$user->profile->getReadableFullName());
                Publisso::tools()->destroyUserSession();
        }
        
        /**
         * @param string $string
         * @param array $excluded
         * @return bool
         */
        public function emailExists(string $string, array $excluded = []){
                
                if(is_string($excluded)) $excluded = [$excluded];
                $excluded = array_map('strtolower', $excluded);
                
                if(!in_array('pending', $excluded)){
                        
                        //String as username or email in pending registrations?
                        $qry = \Drupal::database()->select('rwPubgoldRegisterPending', 't')->fields('t', []);
                        $condition = $qry->orConditionGroup()->condition('rp_username', $string, '=')->condition('rp_email', $string, '=');
                        if($qry->condition($condition)->countQuery()->execute()->fetchField()) return true;
                }
                
                if(!in_array('users', $excluded)){
                        
                        //String as username, zbmed_kennung or initial_email in user logins?
                        $qry = \Drupal::database()->select('rwPubgoldUsers', 't')->fields('t', []);
                        $condition = $qry->orConditionGroup()->condition('user', $string, '=')->condition('initial_email', $string, '=')->condition('zbmed_kennung', $string, '=');
                        if($qry->condition($condition)->countQuery()->execute()->fetchField()) return true;
                }
                
                if(!in_array('profiles', $excluded)){
                        
                        //Email in user profiles?
                        $qry = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', []);
                        error_log($qry->condition('up_email', $string, '=')->countQuery()->execute()->fetchField());
                        if($qry->condition('up_email', $string, '=')->countQuery()->execute()->fetchField()) return true;
                }
                
                if(!in_array('preview', $excluded)){
                        
                        //String as email or username in administrative created profiles?
                        $qry = \Drupal::database()->select('rwPubgoldUserprofilePreview', 't')->fields('t', []);
                        $condition = $qry->orConditionGroup()->condition('pv_username', $string, '=')->condition('pv_email', $string, '=');
                        if($qry->condition($condition)->countQuery()->execute()->fetchField()) return true;
                }
                
                if(!in_array('invitations', $excluded)){
                        
                        //String as email or username in pending invitations?
                        $qry = \Drupal::database()->select('rwPubgoldUserInvitations', 't')->fields('t', []);
                        $condition = $qry->orConditionGroup()->condition('user', $string, '=')->condition('email', $string, '=');
                        if($qry->condition($condition)->countQuery()->execute()->fetchField()) return true;
                }
                
                return false;
        }
}
