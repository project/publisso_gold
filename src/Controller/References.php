<?php

	/**
	 * @file
	 * Contains \Drupal\publisso_gold\Controller\References
         * .
	 */

	namespace Drupal\publisso_gold\Controller;
	use Drupal\Core\Url;
	use Drupal\Core\Link;
	use Drupal\Core\Controller\ControllerBase;
	use Drupal\Component\Render\FormattableMarkup;
        
        /**
         * Class References
         * @package Drupal\publisso_gold\Controller
         */
        class References extends ControllerBase{
		
                private $modname;
                private $session;
                private $setup;
                private $tools;
                private $route;
                private $texts;
                private $baseTable;
                private $fieldPrefix;
                private $pageIDs;
                
                /**
                 * @return string
                 */
                public function __toString(){
			return 'References';
		}
		
		public function __construct(){
                        
                        \Drupal::service('publisso_gold.tools')->forceUserAction();
                        
                        $this->modname     = 'referencemgmt';
                        $this->session     = \Drupal::service('session');
                        $this->setup       = \Drupal::service('publisso_gold.setup');
                        $this->tools       = \Drupal::service('publisso_gold.tools');
                        $this->texts       = \Drupal::service('publisso_gold.texts');
                        $this->baseTable   = 'rwPubgoldReferences';
                        $this->fieldPrefix = '';
                        $this->pageIDs     = [];
                        
                        $this->route['name'] = \Drupal::routeMatch()->getRouteName();
                        $this->route['param'] = \Drupal::routeMatch()->getRawParameters()->all();
		}
                
                /**
                 * @param $action
                 * @return mixed
                 */
                public function main($action){
                        
                        \Drupal::service('page_cache_kill_switch')->trigger();
                        
                        if(!method_exists($this, $action)){
                                return  $this->tools->accessDenied((string)t($this->texts->get('global.message.system.function_not_implemented', 'fc')));
                        }
                        
                        return $this->$action($name);
		}
		
		private function list(){
                        
                        $qry = \Drupal::database()->select($this->baseTable, 't');
                        $qry->fields('t', []);
                        
                        $res = $qry->execute()->fetchAll();
                        
                        $table = [
                                '#type' => 'table',
                                '#header' => [
                                        'id' => 'ID',
                                        'reference' => 'Text'
                                ],
                                '#rows' => [],
                                '#emtpy' => 'No references found'
                        ];
                        
                        foreach($res as $_){
                                $table['#rows'][] = [
                                        $_->id,
                                        html_entity_decode($_->reference)
                                ];
                        }
                        
                        return [
                                $table,
                                $lnkAdd,
                                'cache' => [
                                        'max-age' => 0
                                ]
                        ];
		}
                
                /**
                 * @param $action
                 * @return mixed
                 */
                public function title($action){
                        return $this->texts->get('references.route.action.'.$action, 'afc');
		}
	}
