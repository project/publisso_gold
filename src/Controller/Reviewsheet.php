<?php

	/**
	 * @file
	 * Contains \Drupal\publisso_gold\Controller\Reviewsheet.
	 */

	namespace Drupal\publisso_gold\Controller;
	use Drupal\Core\Url;
	use Drupal\Core\Link;
	use Drupal\Core\Controller\ControllerBase;
	use Drupal\publisso_gold\Controller\Template;
	use Drupal\publisso_gold\Controller\UserTable;
	use Drupal\Component\Render\FormattableMarkup;
	use Symfony\Component\HttpFoundation\JsonResponse;
        
        /**
         * Class Reviewsheet
         * @package Drupal\publisso_gold\Controller
         */
        class Reviewsheet extends ControllerBase{
		
                private $modname;
                private $session;
                private $setup;
                private $tools;
                private $route;
                private $texts;
                private $baseTable;
                private $elements;
                private $fieldPrefix;
                private $id;
                
                /**
                 * @return string
                 */
                public function __toString(){
			return 'Reviewsheet';
		}
                
                /**
                 * Reviewsheet constructor.
                 * @param null $id
                 */
                public function __construct($id = null){
                        
                        \Drupal::service('publisso_gold.tools')->forceUserAction();
                        
                        $this->modname     = 'reviewpagemgmt';
                        $this->session     = \Drupal::service('session');
                        $this->setup       = \Drupal::service('publisso_gold.setup');
                        $this->tools       = \Drupal::service('publisso_gold.tools');
                        $this->texts       = \Drupal::service('publisso_gold.texts');
                        $this->baseTable   = 'rwPubgoldReviewPage';
                        $this->fieldPrefix = 'rp_';
                        $this->elements    = [];
                        
                        $this->route['name'] = \Drupal::routeMatch()->getRouteName();
                        $this->route['param'] = \Drupal::routeMatch()->getRawParameters()->all();
                        
                        if($id){
                                $this->id = $id;
                                $this->load();
                        }
		}
                
                /**
                 * @param $name
                 * @param $xml
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 * @throws \Exception
                 */
                public function createNew($name, $xml){
                        
                        $this->id = \Drupal::database()->insert($this->baseTable)->fields([$this->fieldPrefix.'name' => $name, $this->fieldPrefix.'sheet' => $xml])->execute();
                        $this->load();
                        return $this->id;
		}
		
		private function load(){
                        
                        if($this->id){
                                
                                $res = \Drupal::database()->select($this->baseTable, 't')->fields('t', [])->condition($this->fieldPrefix.'id', $this->id, '=')->execute()->fetchAll();
                                
                                if(count($res) == 0) $this->id = null;
                                
                                foreach($res[0] as $k => $v){
                                        if($k != $this->fieldPrefix.'id'){
                                                $this->elements[str_replace($this->fieldPrefix, '', $k)] = $v;
                                        }
                                }
                        }
		}
                
                /**
                 * @param $action
                 * @param null $id
                 * @return mixed
                 */
                public function main($action, $id = null){
                        
                        if(preg_match('/^(.+)\|(\d+)/', $action, $matches)){
                                $action = $matches[1];
                                $id = $matches[2];
                        }
		        
                        if(!$this->tools->userHasAccessRight($this->modname, $this->session->get('user')['weight'])){
                                return $this->tools->accessDenied();
                        }
                        
                        if(!method_exists($this, $action)){
                                return  $this->tools->accessDenied((string)t($this->texts->get('global.message.system.function_not_implemented', 'fc')));
                        }
                        
                        return $this->$action($id);
		}
                
                /**
                 * @return array
                 */
                private function add(){
                        return [
                                'head' => [
                                        '#markup' => (string)t($this->texts->get('reviewsheet.add.headline', 'fc')),
                                        '#prefix' => '<h1>',
                                        '#suffix' => '</h1>'
                                ],
                                'form' => \Drupal::formBuilder()->getForm('Drupal\publisso_gold\Form\addReviewsheet')
                        ];
		}
                
                /**
                 * @param $id
                 * @return array
                 */
                private function edit($id){
                        return [
                                'head' => [
                                        '#markup' => (string)t($this->texts->get('reviewsheet.edit.headline', 'fc')),
                                        '#prefix' => '<h1>',
                                        '#suffix' => '</h1>'
                                ],
                                'form' => \Drupal::formBuilder()->getForm('Drupal\publisso_gold\Form\editReviewsheet', ['id' => $id])
                        ];
		}
                
                /**
                 * @param $id
                 * @return array
                 */
                private function copy($id){
                        return [
                                'head' => [
                                        '#markup' => (string)t($this->texts->get('reviewsheet.copy.headline', 'fc')),
                                        '#prefix' => '<h1>',
                                        '#suffix' => '</h1>'
                                ],
                                'form' => \Drupal::formBuilder()->getForm('Drupal\publisso_gold\Form\editReviewsheet', ['id' => $id, 'copy' => 1])
                        ];
		}
                
                /**
                 * @param $id
                 * @return array
                 */
                private function remove($id){
                        
                        $this->id = $id;
                        $this->load();
                        
                        return [
                                'head' => [
                                        '#markup' => (string)t($this->texts->get('reviewsheet.remove.headline', 'fc'), ['@id' => $id, '@name' => $this->getElement('name')]),
                                        '#prefix' => '<h1>',
                                        '#suffix' => '</h1>'
                                ],
                                'form' => \Drupal::formBuilder()->getForm('Drupal\publisso_gold\Form\deleteReviewsheet', ['id' => $id])
                        ];
		}
                
                /**
                 * @param $action
                 * @return string
                 */
                public function title($action){
                        
                        if(preg_match('/^(.+)\|(\d+)/', $action, $matches)){
                                $action = $matches[1];
                        }
                        
                        return (string)t($this->texts->get("reviewsheet.title.$action", 'afc'));
		}
                
                /**
                 * @param $element
                 * @return mixed
                 */
                public function getElement($element){
                        if($element == 'id') return $this->id;
                        return $this->elements[$element];
		}
                
                /**
                 * @param $elemName
                 * @param null $elemValue
                 * @param bool $storeOnDB
                 * @return bool
                 */
                public function setElement($elemName, $elemValue = null, $storeOnDB = true){

                        if(array_key_exists($elemName, $this->elements)){

                                $oldValue = $this->elements[$elemName];
                                $this->elements[$elemName] = $elemValue;

                                if($storeOnDB === false)
                                        return true;

                                if($this->save($elemName) === false){
                                        $this->elements[$elemName] = $oldValue;
                                        return false;
                                }
                                else{
                                        return true;
                                }
                        }
                        else{
                                return false;
                        }
                }
                
                /**
                 * @param null $elemName
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 */
                public function save($elemName = null){

                        $fields = $this->elements;

                        if($elemName !== null){ //if specific element selected
                                $fields = [$elemName => $fields[$elemName]];
                        }

                        foreach($fields as $k => $var){
                                $fields[$this->fieldPrefix.$k] = $var;
                                unset($fields[$k]);
                        }

                        return \Drupal::database()->update($this->baseTable)
                        ->fields($fields)
                        ->condition($this->fieldPrefix.'id', $this->id)
                        ->execute();
                }
                
                public function delete(){
                        $this->setElement('active', 0);
                }
	}
?>
