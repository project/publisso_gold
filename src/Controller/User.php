<?php
        
        /**
         * @file
         * Contains \Drupal\publisso_gold\Controller\User.
         */
        
        namespace Drupal\publisso_gold\Controller;
        
        /**
         * Class User
         * @package Drupal\publisso_gold\Controller
         */
        class User {
                
                private static $id;
                private $uid;
                private $elements = [];
                var     $role     = [];
                var     $profile  = null;
                var     $tempInfo = [];
        
                /**
                 * @return string
                 */
                public function __toString () {
                        return __CLASS__;
                }
        
                /**
                 * User constructor.
                 * @param null $id
                 */
                public function __construct ($id = null ) {
                        
                        $this->uid = $id;
                        self::$id = $id;
                        $this->load();
                }
        
                /**
                 * @return Userprofile
                 */
                public static function profile(){
                        return new Userprofile(null, \Drupal::service('session')->get('user')['id']);
                }
        
                /**
                 * @return int|mixed
                 */
                public function getWeight(){
                        return $this->role['weight'] ?? 0;
                }
        
                /**
                 * @param $elemName
                 * @param null $elemValue
                 * @param bool $storeOnDB
                 * @return bool
                 */
                public function setElement ($elemName, $elemValue = null, $storeOnDB = true ) {
                        
                        if ( array_key_exists( $elemName, $this->elements ) ) {
                                
                                $oldValue = $this->elements[ $elemName ];
                                $this->elements[ $elemName ] = $elemValue;
                                
                                if ( $storeOnDB === false )
                                        return true;
                                
                                if ( $this->save( $elemName ) === false ) {
                                        $this->elements[ $elemName ] = $oldValue;
                                        return false;
                                }
                                else {
                                        return true;
                                }
                        }
                        else {
                                return false;
                        }
                }
        
                /**
                 * @return mixed|null
                 */
                public function getId () {
                        
                        try{
                                return $this->uid ?? self::$id;
                        }
                        catch(\Error $e){
                                return self::$id;
                        }
                }
        
                /**
                 * @param $elemName
                 * @return mixed|null
                 */
                public function getRoleElement ($elemName ) {
                        return $this->role[ $elemName ] ?? null;
                }
        
                /**
                 * @param $elemName
                 * @return mixed|null
                 */
                public function getElement ($elemName ) {
                        if ( $elemName == 'id' ) return $this->getId();
                        return $this->elements[ $elemName ];
                }
        
                /**
                 * @param $username
                 * @param $password
                 * @param $firstname
                 * @param $lastname
                 * @param $email
                 * @throws \Exception
                 */
                public function create ($username, $password, $firstname, $lastname, $email ) {
                        
                        self::$id = \Drupal::database()->insert( 'rwPubgoldUsers' )->fields( [
                                                                                                     'user'     => $username,
                                                                                                     'password' => \Drupal::service( 'publisso_gold.tools' )->getDBEncPassword( $password ),
                                                                                                     'active'   => 1,
                                                                                             ]
                        )->execute()
                        ;
                        
                        if ( $this->getId() ) {
                                
                                \Drupal::database()->insert( 'rwPubgoldUserroles' )->fields( [
                                                                                                     'user_id' => $this->getId(),
                                                                                                     'role_id' => \Drupal::service( 'publisso_gold.setup' )->getValue( 'default.user.role.id' ),
                                                                                             ]
                                )->execute()
                                ;
                                
                                \Drupal::database()->insert( 'rwPubgoldUserProfiles' )->fields( [
                                                                                                        'up_uid'       => $this->getId(),
                                                                                                        'up_email'     => $email,
                                                                                                        'up_firstname' => $firstname,
                                                                                                        'up_lastname'  => $lastname,
                                                                                                ]
                                )->execute()
                                ;
                                
                                //lösche Datensätze mit dieser Email aus den Temporären Tabellen
                                \Drupal::database()->delete( 'rwPubgoldRegisterPending' )->condition( 'rp_email', $email, '=' )->execute();
                                \Drupal::database()->delete( 'rwPubgoldUserInvitations' )->condition( 'email', $email, '=' )->execute();
                                \Drupal::database()->delete( 'rwPubgoldUserprofilePreview' )->condition( 'pv_email', $email, '=' )->execute();
                        }
                        
                        $this->load();
                }
        
                /**
                 * @param null $elemName
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 */
                public function save ($elemName = null ) {
                        
                        $fields = $this->elements;
                        
                        if ( $elemName !== null ) { //if specific element selected
                                $fields = [ $elemName => $fields[ $elemName ] ];
                        }
                        
                        return \Drupal::database()->update( 'rwPubgoldUsers' )
                                      ->fields( $fields )
                                      ->condition( 'id', $this->getId() )
                                      ->execute()
                                ;
                }
        
                /**
                 * @param string $password
                 * @return \Drupal\Core\Database\StatementInterface|int|string|null
                 */
                public function setPassword (string $password ) {
                        return \Drupal::database()->update('rwPubgoldUsers')->expression('password', 'PASSWORD(:password)', [':password' => $password])->condition('id', $this->getId(), '=')->execute();
                }
                
                private function load () {
                        
                        if ( self::$id ) {
                                
                                $user = \Drupal::database()->select( 'rwPubgoldUsers', 'u' )
                                               ->fields( 'u', [] )
                                               ->condition( 'id', $this->getId(), '=' )
                                               ->execute()
                                               ->fetchAssoc()
                                ;
                                
                                if ( is_array( $user ) ) {
                                        foreach ( $user as $k => $v ) {
                                                
                                                if ( $k != 'id' ) //store book except id
                                                        $this->elements[ $k ] = $v;
                                        }
                                }
                                
                                $this->profile = new Userprofile( null, self::$id );
                                
                                //getting role-info
                                $result = \Drupal::database()->select( 'rwPubgoldUserroles', 't' )->fields( 't', [ 'role_id' ] )->condition( 'user_id', self::$id, '=' )->execute()->fetchAssoc();
                                $this->role = \Drupal::database()->select( 'rwPubgoldRoles', 't' )->fields( 't', [] )->condition( 'id', $result[ 'role_id' ], '=' )->execute()->fetchAssoc();
                        }
                        else {
                                $this->profile = new Userprofile();
                        }
                }
        
                /**
                 * @return mixed
                 */
                public function isBookEditor(){
                        return \Drupal::database()->select('rwPubgoldBookEditors', 't')
                                      ->fields('t', [])
                                      ->condition('be_uid', $this->getId(), '=')
                                      ->countQuery()
                                      ->execute()
                                      ->fetchField();
                }
        
                /**
                 * @return mixed
                 */
                public function isBookEiC(){
                        return \Drupal::database()->select('rwPubgoldBookEditorsInChief', 't')
                                      ->fields('t', [])
                                      ->condition('eic_uid', $this->getId(), '=')
                                      ->countQuery()
                                      ->execute()
                                      ->fetchField();
                }
        
                /**
                 * @return mixed
                 */
                public function isBookEO(){
                        return \Drupal::database()->select('rwPubgoldBookEditorialOffice', 't')
                                      ->fields('t', [])
                                      ->condition('beo_uid', $this->getId(), '=')
                                      ->countQuery()
                                      ->execute()
                                      ->fetchField();
                }
        
                /**
                 * @return mixed
                 */
                public function isJournalEditor(){
                        return \Drupal::database()->select('rwPubgoldJournalEditors', 't')
                                      ->fields('t', [])
                                      ->condition('je_uid', $this->getId(), '=')
                                      ->countQuery()
                                      ->execute()
                                      ->fetchField();
                }
        
                /**
                 * @return mixed
                 */
                public function isJournalEiC(){
                        return \Drupal::database()->select('rwPubgoldJournalsEditorsInChief', 't')
                                      ->fields('t', [])
                                      ->condition('jeic_uid', $this->getId(), '=')
                                      ->countQuery()
                                      ->execute()
                                      ->fetchField();
                }
        
                /**
                 * @return mixed
                 */
                public function isJournalEO(){
                        return \Drupal::database()->select('rwPubgoldJournalsEditorialOffice', 't')
                                      ->fields('t', [])
                                      ->condition('jeo_uid', $this->getId(), '=')
                                      ->countQuery()
                                      ->execute()
                                      ->fetchField();
                }
        
                /**
                 * @return mixed
                 */
                public function isConferenceEiC(){
                        return \Drupal::database()->select('rwPubgoldConferencesEditorsInChief', 't')
                                      ->fields('t', [])
                                      ->condition('ceic_uid', $this->getId(), '=')
                                      ->countQuery()
                                      ->execute()
                                      ->fetchField();
                }
        
                /**
                 * @return mixed
                 */
                public function isConferenceEO(){
                
                        return \Drupal::database()->select('rwPubgoldConferencesEditorialOffice', 't')
                                      ->fields('t', [])
                                      ->condition('ceo_uid', $this->getId(), '=')
                                      ->countQuery()
                                      ->execute()
                                      ->fetchField();
                }
        
                /**
                 * @return false
                 */
                public function isConferenceEditor(){ return false;}
        
                /**
                 * @return int
                 */
                public function isEditor(){
                        return $this->isBookEditor() | $this->isJournalEditor() | $this->isConferenceEditor();
                }
        
                /**
                 * @return int
                 */
                public function isEiC(){
                        return $this->isBookEiC() | $this->isJournalEiC() | $this->isConferenceEiC();
                }
        
                /**
                 * @return int
                 */
                public function isEO(){
                        return $this->isBookEO() | $this->isJournalEO() | $this->isConferenceEO();
                }
        
                /**
                 * @return mixed
                 */
                public function isOverridden(){
                        return \Drupal::service('session')->has('orig_user');
                }
        
                /**
                 * @return bool
                 */
                public function isAnonymous(){
                        return !$this->getId();
                }
        
                /**
                 * @return array
                 */
                public function getElementKeys () {
                        return array_keys( $this->elements );
                }
        
                /**
                 * @return array
                 */
                public function getKeys () {
                        return array_keys( $this->elements );
                }
        
                /**
                 * @param $role
                 * @return bool
                 */
                public function setRole ($role ) {
                        
                        if ( !preg_match( '/^\d+$/', $role ) ) {
                                $role = \Drupal::database()->select( 'rwPubgoldRoles', 't' )->fields( 't', [ 'id' ] )->condition( 'name', $role, '=' )->execute()->fetchField();
                        }
                        
                        if ( !( $role && \Drupal::database()->select( 'rwPubgoldRoles', 't' )->fields( 't', [] )->condition( 'id', $role, '=' )->countQuery()->execute()->fetchField() ) ) {
                                error_log( 'User.php: Can\'t find role "' . $role . '"' );
                                return false;
                        }
                        
                        \Drupal::database()->update( 'rwPubgoldUserroles' )
                               ->fields( [ 'role_id' => $role ] )
                               ->condition( 'user_id', $this->getId(), '=' )
                               ->execute()
                        ;
                        
                        $this->role = \Drupal::database()->select( 'rwPubgoldRoles', 't' )->fields( 't', [] )->condition( 'id', $role, '=' )->execute()->fetchAssoc();
                        return true;
                }
                
                public function lock () {
                        $this->setElement( 'locked', 1 );
                }
                
                public function unlock () {
                        $this->setElement( 'locked', 0 );
                        $this->setElement( 'active', 1 );
                }
        
                /**
                 * @return bool
                 */
                public function hasActiveProcesses () {
                        
                        $processes = 0;
                        
                        $processes += \Drupal::database()->select( 'rwPubgoldReviewerReviewsheet', 't' )->fields( 't', [] )
                                             ->condition( 'uid', $this->getId(), '=' )
                                             ->condition( 'state', [ 'rejected', 'finished' ], 'NOT IN' )
                                             ->countQuery()->execute()->fetchField()
                        ;
                        
                        $processes += \Drupal::database()->select( 'rwPubgoldWorkflow', 't' )->fields( 't', [] )
                                             ->where( '
                                (FIND_IN_SET(' . $this->getId() . ', wf_assigned_to_eo) OR
                                FIND_IN_SET(' . $this->getId() . ', wf_assigned_to_reviewer) OR
                                FIND_IN_SET(' . $this->getId() . ', wf_assigned_to_editor) OR
                                FIND_IN_SET(' . $this->getId() . ', wf_assigned_to_eic) OR
                                wf_locked_by_uid = ' . $this->getId() . ' OR
                                FIND_IN_SET(\'u:' . $this->getId() . '\', wf_assigned_to) OR
                                (wf_created_by_uid = ' . $this->getId() . ' AND wf_state NOT IN (\'published\', \'rejected\'))) AND
                                (not isnull(wf_state) and wf_state != \'published\' )
                        '
                                             )
                                             ->countQuery()->execute()->fetchField()
                        ;
                        
                        return !!$processes;
                }
                
                public function delete () {
                        
                        $vars = [];
                        foreach ( $this->getKeys() as $_ ) $vars[ "user.$_" ] = $this->getElement( $_ );
                        foreach ( $this->profile->getKeys() as $_ ) $vars[ "::user.profile.$_::" ] = $this->profile->getElement( $_ );
                        
                        $tables = [
                                'BookAuthors'                => 'ba_uid',
                                'BookEditors'                => 'be_uid',
                                'BookEditorialOffice'        => 'beo_uid',
                                'BookEditorsInChief'         => 'eic_uid',
                                'BookReviewers'              => 'br_uid',
                                'Bookchapter_temp'           => 'uid',
                                'ConferencesEditorialOffice' => 'ceo_uid',
                                'ConferencesEditorsInChief'  => 'ceic_uid',
                                'JournalEditors'             => 'je_uid',
                                'JournalReviewers'           => 'jr_uid',
                                'Journalarticle_temp'        => 'uid',
                                'JournalsEditorialOffice'    => 'jeo_uid',
                                'JournalsEditorsInChief'     => 'jeic_uid',
                                'PWReset'                    => 'pwr_loginid',
                                'UserAreasOfExpertise'       => 'uaoe_uid',
                                'UserDeletePending'          => 'uid',
                                'Userroles'                  => 'user_id',
                                'WorkflowCache'              => 'wc_uid',
                        ];
                        
                        foreach ( $tables as $table => $field ) {
                                \Drupal::database()->delete( 'rwPubgold' . $table )->condition( $field, $this->getId(), '=' )->execute();
                        }
                        
                        
                        $fields = [
                                'firstname'               => 'n.a.',
                                'lastname'                => 'n.a.',
                                'picture'                 => null,
                                'picture_type'            => null,
                                'graduation'              => null,
                                'institute'               => null,
                                'country'                 => null,
                                'postal_code'             => null,
                                'city'                    => null,
                                'street'                  => null,
                                'salutation'              => null,
                                'department'              => null,
                                'telephone'               => null,
                                'area_of_expertise'       => null,
                                'orcid'                   => null,
                                'correspondence_language' => null,
                                'graduation_suffix'       => null,
                                'show_data_in_boards'     => 0,
                        ];
                        
                        foreach ( $fields as $field => $value ) {
                                $this->profile->setElement( $field, $value, false );
                        }
                        
                        $this->profile->save();
                        
                        //$this->setElement('initial_email', null                                     , false);
                        $this->setElement( 'active', 0, false );
                        $this->setElement( 'locked', 1, false );
                        $this->setElement( 'deleted', 1, false );
                        //$this->setElement('user'         , 'deleted:'.md5($this->getElement('user')), false);
                        $this->save();
                        
                        if ( null !== ( $template = \Drupal::service( 'publisso_gold.tools' )->getMailtemplate( 'userprofile delete info' ) ) ) {
                                
                                $to = str_replace( array_keys( $vars ), array_values( $vars ), $template->recipient );
                                $subject = (string)\Drupal::service( 'string_translation' )->translate( $template->subject, $vars );
                                $body = (string)\Drupal::service( 'string_translation' )->translate( $template->template, $vars );
                                
                                \Drupal::service( 'publisso_gold.tools' )->sendMail( $to, $subject, $body );
                                
                                \Drupal::database()->insert( 'rwPubgoldMaillog' )->fields( [
                                                                                                   'recipient'      => $to,
                                                                                                   'subject'        => $subject,
                                                                                                   'body'           => $body,
                                                                                                   'init_process'   => 'profiledelete',
                                                                                                   'created_by_uid' => $this->getElement( 'id' ),
                                                                                           ]
                                )->execute()
                                ;
                        }
                }
        
                /**
                 * @param string $username
                 * @param string $password
                 */
                public function loadByUserPass (string $username, string $password ) {
                        
                        self::$id = \Drupal::database()->select( 'rwPubgoldUsers', 't' )->fields( 't', [ 'id' ] )
                                           ->where( '(user = :user OR initial_email = :user OR zbmed_kennung = :user) AND password = PASSWORD(:pass)', [
                                                   ':user' => $username,
                                                   ':pass' => $password,
                                           ]
                                           )->execute()->fetchField()
                        ;
                        
                        $this->load();
                }
        
                /**
                 * @param array $fields
                 */
                public function loadByFieldValue (array $fields ) {
                        
                        $qry = \Drupal::database()->select( 'rwPubgoldUsers', 't' )->fields( 't', [ 'id' ] );
                        
                        foreach ( $fields as $field => $value ) {
                                $qry->condition( $field, $value, '=' );
                        }
                        
                        if ( $qry->countQuery()->execute()->fetchField() == 1 ) {
                                self::$id = $qry->execute()->fetchField();
                                $this->load();
                        }
                }
        
                /**
                 * @return bool
                 */
                public function isAdministrator(){
                        return $this->getRoleElement('id') == \Drupal::configFactory()->getEditable('publisso_gold.config')->get('admin_role');
                }
        
                /**
                 * @return bool
                 */
                public function isPublisso(){
                        return $this->getRoleElement('id') == \Drupal::configFactory()->getEditable('publisso_gold.config')->get('publisso_role');
                }
        }

?>
