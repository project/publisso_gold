<?php
        
        /**
         * @file
         * Contains \Drupal\publisso_gold\Controller\Journalarticle.
         */
        namespace Drupal\publisso_gold\Controller;
        
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\JournalManager;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Symfony\Component\HttpFoundation\Response;
        use Symfony\Component\HttpFoundation\ResponseHeaderBag;
        
        /**
         * Class Journalarticle
         * @package Drupal\publisso_gold\Controller
         */
        class Journalarticle {
                private $id;
                private $elements                = [];
                private $elements_translated     = [];
                private $tbl_column_prefix       = 'jrna_';
                private $authors = [];
                private $authors_translated      = [];
                private $files                   = [];
                private $files_translated        = [];
                private $table                   = 'rwPubgoldJournalArticles';
                private $translationsLoaded      = [];
                private $parent                  = null;
                private $availTranslations       = [];
                private $availTranslationsLoaded = false;
                private $origLanguage            = null;
        
        
                /**
                 * Journalarticle constructor.
                 * @param null $id
                 */
                public function __construct ($id = null ) {
                        
                        if($id) {
                                $this->id = $id;
                                $this->load();
                        }
                }
        
                /**
                 * @param string $type
                 * @return \Drupal\Core\GeneratedUrl|Url|false|string|null
                 */
                public function getLink($type = 'link'){
                        
                        if(!$this->getElement('vol_id')){
                                $this->setElement(
                                        'volume',
                                        $this->getMedium()->getCurrentVolume()->getElement('id'),
                                        true
                                );
                        }
                        
                        $params = [
                                'jrn_id' => $this->getElement('jrnid'),
                                'jrna_id' => $this->id,
                                'vol_id' => $this->getElement('volume')
                        ];
                        
                        if($this->getElement('issue')){
                                $routeName = 'publisso_gold.journals_journal.volume.article';
                                $params['iss_id'] = $this->getElement('issue');
                        }
                        elseif($this->getElement('volume')){
                                $routeName = 'publisso_gold.journals_journal.volume.article';
                        }
                        else{
                                return null;
                        }
                        
                        try {
                                $url = Url::fromRoute( $routeName, $params );
                                $url->setAbsolute();
                                return !$url ? false : ( $type == 'link' ? $url->toString() : ( $type == 'url' ? $url : false ) );
                        }
                        catch ( \Error $e ) {
                                error_log($e->getMessage());
                                return null;
                        }
                }
        
                /**
                 * @param null $tableName
                 * @return bool
                 */
                private function load ($tableName = null ) {
                        
                        if ( !$tableName ) $tableName = $this->table;
                        
                        if ( $tableName != $this->table && in_array( substr( $tableName, -2 ), $this->translationsLoaded ) ) return true;
                        if ( $tableName != $this->table ) $this->translationsLoaded[] = substr( $tableName, -2 );
                        
                        if ( $this->id ) {
                                
                                $result = \Drupal::database()->select( $tableName, 'bc' )->fields( 'bc', [] )->condition( $this->tbl_column_prefix . 'id', $this->id, '=' )->execute()->fetchAssoc();
                                if(!is_array($result)) return false;
                                
                                foreach ( $result as $k => $v ) {
                                        
                                        if ( $k != $this->tbl_column_prefix . 'id' ) { // store chapter except id
                                                
                                                if ( $tableName == $this->table ) $this->elements [ str_replace( $this->tbl_column_prefix, '', $k ) ] = $v;
                                                else
                                                        $this->elements_translated[ substr( $tableName, -2 ) ][ str_replace( $this->tbl_column_prefix, '', $k ) ] = $v;
                                        }
                                }
                                
                                // files
                                if ( array_key_exists('files', $result )) {
                                        
                                        foreach ( explode( ',', $result [ 'files' ] ) as $fid ) {
                                                
                                                if ( $tableName == $this->table ) $this->files = new \Drupal\publisso_gold\Controller\Blob ( $fid );
                                                else
                                                        $this->files_translated[ substr( $tableName, -2 ) ] = new \Drupal\publisso_gold\Controller\Blob ( $fid );
                                        }
                                }
                                
                                // parse the authors
                                $_tmp = [];
                                
                                $authors = json_decode( $this->getElement( 'authors', $tableName != $this->table ? substr( $tableName, -2 ) : null ) );
                                
                                usort( $authors, [ 'self', 'sortAuthors' ] );
                                
                                foreach ( $authors as $author ) {
                                        
                                        if(property_exists($author, 'uid')) {
                                                $_ = new User ($author->uid );
        
                                                if ( !$_->getElement( 'id' ) ) {
                
                                                        foreach ( $author as $k => $v ) {
                                                                $_->profile->setElement( $k, $v, false );
                                                        }
                                                }
                                        }
                                        else{
                                                $_ = new User ();
                                                
                                                foreach($author as $k => $v){
                                                        $_->profile->setElement($k, html_entity_decode($v), false);
                                                }
                                        }
                                        
                                        if ( property_exists($author, 'profile_archive_id')) {
                                                
                                                $result = \Drupal::database()->select( 'rwPubgoldUserProfiles_Archive', 't' )->fields( 't', [ 'data' ] )->condition( 'uid', $author->uid, '=' )->execute()->fetchAssoc();
                                                
                                                $result [ 'data' ] = json_decode( base64_decode( $result [ 'data' ], true ) );
                                                
                                                foreach ( $result [ 'data' ] as $k => $v ) {
                                                        $_->profile->setElement( $k, $v, false );
                                                }
                                        }
                                        
                                        if ( $_->profile->getElement( 'affiliation' ) == '' && $_->profile->getElement( 'institute' ) != '' ) {
                                                $_->profile->setElement( 'affiliation', $_->profile->getElement( 'institute' ), false );
                                        }
                                        
                                        $_tmp [] = $_;
                                }
                                
                                $tableName == $this->table ? $this->authors = $_tmp : $this->authors_translated[ substr( $tableName, -2 ) ] = $_tmp;
                                
                                foreach ( $this->getTranslations() as $lang ) $this->loadTranslation( $lang );
                                
                                $this->firstLoadWFStateChain();
                                $this->parent = $this->loadParent();
                                $this->origLanguage = $this->getOrigLanguage();
                        }
                }
        
                /**
                 * @param $elemName
                 * @param null $langCode
                 * @param bool $fallback
                 * @return mixed|null
                 */
                public function getElement ($elemName, $langCode = null, $fallback = true ) {
                        if ( $elemName == 'id' ) return $this->id;
                        
                        if ( $langCode == $this->origLanguage ) $langCode = null;
                        
                        if ( $langCode && array_key_exists( $langCode, $this->elements_translated ) && array_key_exists( $elemName, $this->elements_translated[ $langCode ] ) && $this->elements_translated[ $langCode ][ $elemName ] != null ) {

                                return $this->elements_translated[ $langCode ][ $elemName ];
                        }
                        elseif ( $fallback || $langCode === null ) {

                                if ( $elemName == 'references' && $this->elements [ 'references' ] === null ) {
                                        $origWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $this->getElement( 'wfid' ) );
                                        $_references = Publisso::tools()->getReferences( base64_decode( $this->elements [ 'article_text' ] ), ( string )$origWorkflow->getDataElement( 'references' ) );
                                        $this->setElement( 'references', base64_encode( $_references ) );
                                }

                                return $this->elements [ $elemName ] ?? null;
                        }
                        else {
                                return null;
                        }
                }
        
                /**
                 * @param $langCode
                 * @return bool
                 */
                public function hasTranslation ($langCode ) {
                        
                        $tablePrefix = \Drupal::database()->getConnectionOptions()[ 'prefix' ][ 'default' ];
                        $tableSuffix = '_' . strtolower( $langCode );
                        $tableName = $this->table;
                        $res = \Drupal::database()->query( "SHOW TABLES LIKE '$tablePrefix$tableName$tableSuffix'" )->fetchAssoc();
                        
                        if ( ( is_array( $res ) && !!count( $res ) ) ) {
                                return true;
                        }
                        
                        return false;
                }
        
                /**
                 * @param $langCode
                 */
                public function loadTranslation ($langCode ) {
                        
                        $tableSuffix = '_' . strtolower( $langCode );
                        $tableName = $this->table;
                        if ( $this->hasTranslation( $langCode ) ) $this->load( $tableName . $tableSuffix );
                }
        
                /**
                 * @return array
                 */
                public function getTranslations () {
                        
                        if ( $this->availTranslationsLoaded === false ) {
                                
                                $tablePrefix = \Drupal::database()->getConnectionOptions()[ 'prefix' ][ 'default' ];
                                $tableName = $this->table;
                                $table = "$tablePrefix$tableName" . '_%';
                                
                                $res = [];
                                
                                foreach ( \Drupal::database()->query( "SHOW TABLES LIKE '$table'" )->fetchCol() as $table ) {
                                        $res[ explode( '_', $table )[ 2 ] ] = $table;
                                }
                                
                                $translations = [];
                                
                                foreach ( $res as $lang => $table ) {
                                        
                                        $table = substr( $table, strlen( $tablePrefix ) );
                                        
                                        if ( !!\Drupal::database()->select( $table, 't' )->fields( 't', [ $this->tbl_column_prefix . 'id' ] )->condition( $this->tbl_column_prefix . 'id', $this->id, '=' )->countQuery()->execute()->fetchField() ) {
                                                $this->availTranslations[] = $lang;
                                        }
                                }
                                
                                $this->availTranslationsLoaded = true;
                                $this->availTranslations = array_filter($this->availTranslations);
                        }
                        
                        return $this->availTranslations;
                }
        
                /**
                 * @param null $lang
                 * @param false $recursion
                 * @return bool
                 */
                private function firstLoadWFStateChain ($lang = null, $recursion = false ) {
                        
                        if ( $recursion === false ) {
                                
                                foreach ( $this->getTranslations() as $_lang ) {
                                        $this->firstLoadWFStateChain( $_lang, true );
                                }
                        }
                        
                        if ( !$this->getElement( 'wfid', $lang ) ) return false;
                        
                        if ( !array_key_exists( 'wf_state_chain', $this->elements ) || $this->getElement( 'wf_state_chain', $lang, false ) !== null ) return false;
                        
                        //nur, wenn dieser Artikel bereits publiziert wurde und entsprechende Elemente in der
                        // WF-Chain hat
                        $qry = \Drupal::database()->select( 'rwPubgoldWorkflowStateChain', 't' );
                        $qry->fields( 't', [] );
                        $qry->condition( 'wf_id', $this->getElement( 'wfid', $lang ), '=' );
                        
                        $res = $qry->orderBy( 'time', 'ASC' )->execute()->fetchAll();
                        
                        if ( !count( $res ) ) return false;
                        
                        $states = [];
                        foreach ( $res as $row ) {
                                $states[] = $row;
                        }
                        
                        $this->setElement( 'wf_state_chain', serialize( $states ), true, $lang );
                        
                        //lösche aus der Verlaufstabelle
                        \Drupal::database()->delete( 'rwPubgoldWorkflowStateChain' )->condition( 'wf_id', $this->getElement( 'wfid', $lang ), '=' )->execute();
                        
                        return true;
                }
        
                /**
                 * @param $elemName
                 * @param null $elemValue
                 * @param bool $storeOnDB
                 * @param null $lang
                 */
                public function setElement ($elemName, $elemValue = null, $storeOnDB = true, $lang = null ) {

                        $elemValue = utf8_encode($elemValue);

                        if ( $lang && $this->hasTranslation( $lang ) && $lang != $this->getOrigLanguage() ) {
                                $this->loadTranslation( $lang );
                                if ( array_key_exists($lang, $this->elements_translated) && array_key_exists(
                                        $elemName,
                                                                                                     $this->elements_translated[ $lang ] ) ) {
                                        $this->elements_translated[ $lang ][ $elemName ] = $elemValue;
                                }
                        }
                        else {
                                if ( array_key_exists( $elemName, $this->elements ) ) {
                                        $this->elements[ $elemName ] = $elemValue;
                                }
                        }
                        
                        if ( $storeOnDB ) $this->save( null, $lang );
                }
        
                /**
                 * @param null $elemName
                 * @param null $lang
                 * @return bool
                 */
                public function save ($elemName = null, $lang = null ) {
                        
                        $this->getTranslations();
                        $fields = null;
                        if ( $lang !== null && array_key_exists($lang, $this->availTranslations) ) {
                                
                                $fields = $this->elements_translated[ $lang ];
                                $table = $this->table . '_' . $lang;
                        }
        
                        if ( $lang === null || $lang == $this->getOrigLanguage() ) {
                                $fields = $this->elements;
                                $table = $this->table;
                        }
                        
                        if($fields == null){
                                return false;
                        }
                        
                        if ( $elemName !== null ) { // if specific element selected
                                $fields = [ $elemName => $fields [ $elemName ] ];
                        }
                        
                        foreach ( $fields as $k => $v ) {
                                $fields [ $this->tbl_column_prefix . $k ] = $v;
                                unset ( $fields [ $k ] );
                        }
                        
                        try{
                                \Drupal::database()->update( $table )->fields( $fields )->condition(
                                        $this->tbl_column_prefix . 'id', $this->id )->execute();
                        }
                        catch(\Drupal\Core\Database\DatabaseExceptionWrapper $e){
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                                \Drupal::service( 'messenger' )->addError( 'An error occured. Please inform the site-admin.' . $e->getMessage() );
                                return false;
                        }
                        catch(\Error $e){
                                \Drupal::service( 'publisso_gold.tools' )->logMsg( __METHOD__ . ' (Line ' . __LINE__ . '): ' . $e->getMessage() );
                                \Drupal::service( 'messenger' )->addError( 'An error occured. Please inform the site-admin.' . $e->getMessage() );
                                return false;
                        }
                        
                        return true;
                }
        
                /**
                 * @param $a
                 * @param $b
                 * @return int
                 */
                private static function sortAuthors ($a, $b ) {
                        if ( $a->weight == $b->weight ) return 0;
                        return $b->weight > $a->weight ? -1 : +1;
                }
        
                /**
                 * @return string
                 */
                public function __toString () {
                        return "class journalarticle";
                }
        
                /**
                 * @param null $langCode
                 * @return array|mixed
                 */
                public function readAuthors ($langCode = null ) {
                        
                        if ( $langCode && array_key_exists( $langCode, $this->authors_translated ) ) {
                                return $this->authors_translated[ $langCode ];
                        }
                        else {
                                return $this->authors;
                        }
                }
        
                /**
                 * @return array
                 */
                public function getKeys () {
                        return array_keys( $this->elements );
                }
        
                /**
                 * @param null $lang
                 * @return false
                 * @throws \Exception
                 */
                public function createTOCStructure ($lang = null) {
                        
                        if(!$this->getElement('title', $lang)) return false;
                        $toc_id = \Drupal::database()->insert( 'rwPubgoldTableOfContent' )->fields( [ 'toc_title' => $this->getElement( 'title', $lang, false ), 'toc_medium' => 'article', 'toc_medium_id' => $this->getElement( 'id' ), 'toc_name' => 'article__' . str_replace( ' ', '_', $this->getElement( 'title', $lang ) ) . '__autogen', 'toc_description' => 'Autogenerated TOC for Article: ' . $this->getElement( 'title', $lang ), 'toc_lang' => $lang ] )->execute();
                        
                        $html = '<html>' . base64_decode( $this->getElement( 'article_text' , $lang, false) ) . '</html>';

                        $tagname = 'h2';
                        
                        // create DOM
                        $doc = new \DOMDocument ();
                        $doc->loadHTML( $html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD | LIBXML_NOWARNING | LIBXML_NOERROR );

                        $domx = new \DOMXPath ( $doc );

                        //delete old anchors
                        $oldAnchors = [];
                        foreach($domx->query('//a[@for="toc"]') as $node){
                                $oldAnchors = $node;
                        }
                        foreach($oldAnchors as $node) $node->parentNode->removeChild($node);

                        // get headers
                        $headers = $domx->query( "//$tagname" );
                        
                        for ( $i = $headers->length; --$i >= 0; ) {
                                
                                // extract header data
                                $header = $headers->item( $i );
                                
                                
                                $headerText = $header->textContent;
                                
                                // create anchor
                                $anchor = $doc->createElement( 'a', '' );
                                
                                // save table-of-content-item
                                $id = \Drupal::database()->insert( 'rwPubgoldTableOfContentItem' )->fields( [ 'toci_title' => $headerText, 'toci_link' => 'intern://anchor/article_header', 'toci_weight' => $i, 'toci_tocid' => $toc_id ] )->execute();
                                
                                // set correct id in link
                                \Drupal::database()->update( 'rwPubgoldTableOfContentItem' )->fields( [ 'toci_link' => 'intern://anchor/article_header_' . $id ] )->condition( 'toci_id', $id, '=' )->execute();
                                
                                // set anchor-name
                                $anchor->setAttribute( 'name', 'article_header_' . $id );
                                $anchor->setAttribute('for', 'toc');
                                
                                // prepend anchor before header
                                $header->parentNode->insertBefore( $anchor, $header );
                        }
                        
                        // update article-textContent
                        $this->setElement( 'article_text', base64_encode( str_replace( [ '<html>', '</html>' ], '', $doc->saveHTML($doc->documentElement) ) ), true, $lang);
                }
        
                /**
                 * @param $element
                 * @return mixed
                 */
                public function getParentElement ($element ) {
                        $this->loadParent();
                        return $this->parent->getElement( $element );
                }
                
                public function getMedium() : \Drupal\publisso_gold\Controller\Medium\Journal{
                        $this->loadParent();
                        return $this->parent;
                }
                
                private function loadParent () {
                        if ( $this->parent === null ) $this->parent = new \Drupal\publisso_gold\Controller\Medium\Journal( $this->getElement( 'jrnid' ), true );
                }
        
                /**
                 * @return mixed|null
                 */
                public function getOrigLanguage () {
                        
                        if ( !$this->getElement( 'language' ) ) {
                                return $this->getParentElement( 'language' );
                        }
                        
                        return $this->getElement( 'language' );
                }
        
                /**
                 * @param $action
                 * @param null $lang
                 * @return false|string|null
                 */
                public function getDate ($action, $lang = null ) {

                        if($this->getElement($action, $lang)) return date("Y-m-d", strtotime($this->getElement($action)));
                        $sc = unserialize( $this->getElement( 'wf_state_chain', $lang ) );
                        
                        $time = null;
                        
                        if(is_array($sc)) {
                                foreach ( $sc as $_ ) {
                                        if ( $_->state_new == $action ) $time = $_->time;
                                }
                        }
                        if ( $time !== null ) $time = date( "Y-m-d", strtotime( $time ) );
                        
                        return $time;
                }
        
                /**
                 * @param null $lang
                 * @return mixed|string
                 */
                public function getDOIPrefix ($lang = null ) {
                        return explode( '/', $this->getElement( 'doi', $lang ) )[ 0 ];
                }
        
                /**
                 * @param null $lang
                 * @return mixed|string
                 */
                public function getDOISuffix ($lang = null ) {
                        return explode( '/', $this->getElement( 'doi', $lang ) )[ 1 ];
                }
        
                /**
                 * @param null $lang
                 * @return false|string|string[]
                 */
                public function getCitationNote($lang = null){
                        
                        if($this->getElement('citation_note', $lang, true )) return base64_decode($this->getElement('citation_note', $lang, true));
                        
                        $this->loadParent();
                        $cnstring = '~AUTHORLIST~. ~TITLE~. ~YEAR~;~VOLNO~~ISSNO~:Doc~DOCNO~.';
        
                        //set title
                        $cnstring = str_replace( '~TITLE~', implode( '. ', array_filter( [
                                                                                                 $this->getElement( 'title', $lang, true ), $this->parent->getElement( 'title_nlm' ),
                                                                                         ]
                                                                         )
                        ), $cnstring
                        );
                        $cnstring = str_replace( '~JOURABBRNLM~', $this->parent->getElement( 'title_nlm', $lang ) . '.',
                                                 $cnstring );
        
                        $cnVol = new \Drupal\publisso_gold\Controller\Volume( $this->getElement( 'volume' ) );
                        $cnstring = str_replace( '~VOLNO~', $cnVol->getElement( 'number' ), $cnstring );
                        $cnstring = str_replace( '~YEAR~', $cnVol->getElement( 'year' ), $cnstring );
        
                        $issno = '';
                        if ( $this->getElement('issue', $lang, true) ) {
                                $cnIssue = new \Drupal\publisso_gold\Controller\Issue( $this->getElement( 'issue',
                                                                                                          $lang
                                ) );
                                $issno = '(' . $cnIssue->getElement( 'number' ) . ')';
                        }
                        $cnstring = str_replace( '~ISSNO~', $issno, $cnstring );
                        $cnstring = str_replace( '~DOCNO~', $this->getElement( 'docno', $lang ), $cnstring );
        
                        //set version
                        $version = $this->getElement( 'version', $lang, true );
        
                        if ( !empty( $version ) ) $cnstring = str_replace( '~VERSION~', $version . '. ', $cnstring );
                        else
                                $cnstring = str_replace( '~VERSION~', '', $cnstring );
        
                        //set cnstring
                        $citation_note = $this->parent->getElement( 'citation_note' );
                        if ( !empty( $citation_note ) ) $citation_note = "IN: $citation_note. ";
                        $cnstring = str_replace( '~CNSTRING~', $citation_note, $cnstring );
        
                        //set begin publication
                        $_ = !empty( $this->getElement( 'publication_year', $lang ) ) ? $this->getElement( 'publication_year', $lang, true ) : $this->parent->getElement( 'begin_publication', $lang );
                        $cnstring = str_replace( '~BEGINPUBLICATION~', $this->parent->getElement( 'begin_publication'
                        ),
                                                 $cnstring );
        
                        //set publication place
                        $_ = !empty( $this->getElement( 'publication_place', $lang ) ) ? $this->getElement( 'publication_place', $lang ) : $this->parent->getElement( 'publication_place', $lang );
                        $cnstring = str_replace( '~PUBLICATIONPLACE~', \Drupal::service( 'publisso_gold.tools' )->getPublicationPlace( $_ ), $cnstring );
        
                        //set publisher
                        $_ = !empty( $this->getElement( 'publisher', $lang ) ) ? $this->getElement( 'publisher',
                                                                                                       $lang ) :
                                $this->parent->getElement( 'publisher', $lang );
                        $cnstring = str_replace( '~PUBLISHER~', \Drupal::service( 'publisso_gold.tools' )->getPublisher( $_ ), $cnstring );
        
                        //set authors
                        $authors = [];
                        $authors_for_cn = [];
                        $corporations_for_cn = [];
        
                        $article_authors = json_decode( $this->getElement( 'authors', $lang, true ) );
        
                        if ( !is_array( $article_authors ) ) {
                
                                //first author who's written publication
                                #$authors[] = $chapter->getElement('author_uid');
                
                                //second authors which additional selected
                                if ( !empty( $this->getElement( 'authors', $lang, true ) ) ) {
                                        foreach ( explode( ',', $this->getElement( 'authors', $lang, true
                                        )
                                                  ) as $_ ) {
                                                if ( !in_array( $_, $authors ) ) {
                                                        $authors[] = $_;
                                                }
                                        }
                                }
                
                                //generate author string
                                foreach ( $authors as $uid ) {
                                        $_user = new User($uid );
                                        $authors_for_cn[] = $_user->profile->getElement( 'lastname' ) . ' ' . substr( $_user->profile->getElement( 'firstname' ), 0, 1 );
                                }
                
                                //third more authors
                                if ( !empty( $this->getElement( 'more_authors', $lang, true ) ) ) {
                                        foreach ( json_decode( $this->getElement( 'more_authors' ), true ) as $_ ) {
                                                $authors_for_cn[] = $_[ 'more_author_lastname' ] . ' ' . substr( $_[ 'more_author_name' ], 0, 1 );
                                        }
                                }
                        }
                        else {
                                $corporations_for_cn = [];
                                $authors = [];
                                foreach ( $article_authors as $_ ) $authors[ $_->weight ] = $_;
                                ksort( $authors );
                
                                foreach ( $authors as $_ ) {
                                        $initials = '';
                                        $firstnames = preg_split( '/([\\s-]+)/', $_->firstname, -1 );
                        
                                        $i = 0;
                                        foreach ( $firstnames as $name ) {
                                                if ( !empty( $name ) ) $initials .= strtoupper( substr( $name, 0, 1 ) );
                                                $i++;
                                                if ( $i == 2 ) break;
                                        }
                        
                                        $authors_for_cn[] = $_->lastname . ' ' . $initials;
                                }
                
                                //fourth corporations
                                if ( !empty( $this->getElement( 'corporation', $lang, true ) ) ) {
                                        foreach ( json_decode( $this->getElement( 'corporation', $lang ), true ) as $_
                                        ) {
                                                $corporations_for_cn[] = $_;
                                        }
                                }
                
                                $authors_for_cn = array_filter( $authors_for_cn );
                                $corporations_for_cn = array_filter( $corporations_for_cn );
                        }
        
                        $cnstring .= '<br>DOI: <a target="_blank" href="https://dx.doi.org/' . $this->getElement( 'doi', $lang ) . '">' . $this->getElement( 'doi', $lang ) . '</a>';
        
                        if ( $this->getElement( 'doi', $lang ) ) {
                                $cnstring .= '<br>URN: <a target="_blank" href="https://nbn-resolving.org/' .
                                             $this->getElement( 'urn', $lang ) . '">' . $this->getElement( 'urn',
                                                                                                           $lang ) . '</a>';
                        }
        
                        $author_string_for_cn = '';
                        $author_string_for_cn .= implode( ', ', $authors_for_cn );
                        $author_string_for_cn .= ( !empty( $author_string_for_cn ) ? ( count( $corporations_for_cn ) ? '; ' : '' ) : '' ) . implode( '; ', $corporations_for_cn );
        
                        $cnstring = str_replace( '~AUTHORLIST~', $author_string_for_cn, $cnstring );
                        
                        $this->setElement('citation_note', base64_encode($cnstring),true, $lang);
                        return $cnstring;
                }
        
                /**
                 * @param $jrn_id
                 * @param $jrna_id
                 * @param $action
                 * @return Response
                 * @throws \Exception
                 */
                public function exportXML($jrn_id, $jrna_id, $action){
        
                        $this->id = $jrna_id;
                        $this->load();
        
                        if($this->getElement('jrnid') != $jrn_id){
                                \Drupal::service('publisso_gold.tools')->accessDenied();
                        }
                        
                        $generate = $store = false;
                        $response = new Response();
                        $headers = new ResponseHeaderBag();
                        $headers->set( 'Content-Type', 'text/xml' );
                        $response->headers = $headers;
                        
                        switch($action){
                                
                                case 'show':
                                        
                                        if(!$this->getElement('xml_blob_id')){
                                                $generate = $store = true;
                                        }
                                        break;
                                        
                                case 'renew':
                                        $generate = true;
                                        if(\Drupal::service('session')->get('user')['weight'] >= 70){
                                                $store = true;
                                        }
                                        break;
                                        
                                case 'volatile':
                                        if(\Drupal::service('session')->get('user')['weight'] >= 70){
                                                $generate = true;
                                        }
                                        
                        }
                        
                        if($generate) {
                                $origLang = $this->getOrigLanguage();
                                
        
                                $implementation = new \DOMImplementation();
                                $dtd = $implementation->createDocumentType( 'GmsArticle', '', 'http://www.egms.de/dtd/2.0.34/GmsArticle.dtd' );
                                $xml = $implementation->createDocument( '', '', $dtd );
                                $xml->encoding = 'UTF-8';
                                $xml->xmlStandalone = true;
                                $xml->formatOutput = true;
                                #$xml->namespaceURI = 'http://www.w3.org/1999/xlink';
                                $xpath = new \DOMXPath( $xml );
        
                                $rootNode = $xpath->evaluate( '/' )->item( 0 );
                                $nodeArticle = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'GmsArticle', '', [ 'xmlns:xlink' => 'http://www.w3.org/1999/xlink' ], $xml, $rootNode);
                                $nodeMetadata = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'MetaData', '', [], $xml, $nodeArticle );
                                $nodeOrigdata = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'OrigData', '', [], $xml, $nodeArticle );
                                $nodeReferences = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'References', '', [], $xml, $nodeOrigdata );
                                $nodeMedia = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Media', '', [], $xml, $nodeOrigdata );
                                $nodeTitlegroup = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'TitleGroup', '', [], $xml, $nodeMetadata );
                                $nodeCreatorlist = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'CreatorList', '', [], $xml, $nodeMetadata );
                                $nodeSubjectgroup = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'SubjectGroup', '', [], $xml, $nodeMetadata );
                                $nodePublisherlist = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'PublisherList', '', [], $xml, $nodeMetadata );
                                $nodeSourcegroup = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'SourceGroup', '', [], $xml, $nodeMetadata );
        
                                //ID
                                $_node = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Identifier', $this->id, [], $xml, $nodeMetadata, true );
        
                                //DOI
                                $_node = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'IdentifierDoi', $this->getElement( 'doi' ), [], $xml, $nodeMetadata, true );
        
                                //URN
                                $_node = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'IdentifierUrn', $this->getElement( 'urn' ), [], $xml, $nodeMetadata, true );
        
                                //ArticleType
                                $_node = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'ArticleType', $this->getElement( 'article_type' ), [ 'language' => $origLang ], $xml, $nodeMetadata, true );
        
                                foreach ( $this->getTranslations() as $lang ) {
                                        $_node = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'ArticleType', $this->getElement( 'article_type', $lang ), [ 'language' => $lang ], $xml, $nodeMetadata, true );
                                }
        
                                //Title
                                $_node = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Title', $this->getElement( 'title' ), [ 'language' => $origLang ], $xml, $nodeTitlegroup, true );
        
                                foreach ( $this->getTranslations() as $lang ) {
                                        $_node = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Title', $this->getElement( 'title', $lang ), [ 'language' => $lang ], $xml, $nodeTitlegroup, true );
                                }
        
                                //Authors
        
                                foreach ( $this->readAuthors() as $author ) {
                
                                        $affiliation = $author->profile->getAffiliation();
                
                                        if ( empty( trim( $affiliation ) ) ) {
                                                $affiliation = $author->profile->getElement( 'affiliation' );
                                        }
                
                                        $nodeCreator = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Creator', '', [], $xml, $nodeCreatorlist );
                                        $nodePersonnames = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'PersonNames', '', [], $xml, $nodeCreator );
                                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Lastname', $author->profile->getElement( 'lastname' ), [], $xml, $nodePersonnames, true );
                                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'LastnameHeading', $author->profile->getElement( 'lastname' ), [], $xml, $nodePersonnames, true );
                                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Firstname', $author->profile->getElement( 'firstname' ), [], $xml, $nodePersonnames, true );
                                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Initials', $author->profile->getFirstnameInitials(), [], $xml, $nodePersonnames, true );
                                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'AcademicTitle', $author->profile->getElement( 'graduation' ), [], $xml, $nodePersonnames, true );
                                        $nodeAddress = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Address', '', [], $xml, $nodeCreator );
                                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Affiliation', $affiliation, [], $xml, $nodeAddress, true );
                                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Email', $author->profile->getElement( 'email' ), [], $xml, $nodeCreator, true );
                                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Creatorrole', 'author', [ 'corresponding' => $author->profile->getElement( 'is_corresponding' ) ? 'yes' : 'no', 'presenting' => $author->profile->getElement( 'is_presenting' ) ? 'yes' : 'no' ], $xml, $nodeCreator, true );
                                }
        
                                //Keywords
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'SubjectHeadingDDB', $this->getElement( 'ddc' ), [], $xml, $nodeSubjectgroup, true );
        
                                foreach ( json_decode( $this->getElement( 'keywords' ) ) as $kw ) {
                                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Keyword', $kw, [ 'language' => $origLang ], $xml, $nodeSubjectgroup, true );
                                }
        
                                foreach ( $this->getTranslations() as $lang ) {
                
                                        foreach ( json_decode( $this->getElement( 'keywords', $lang ) ) as $kw ) {
                                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Keyword', $kw, [ 'language' => $lang ], $xml, $nodeSubjectgroup, true );
                                        }
                                }
        
                                $nodePublisher = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Publisher', '', [], $xml, $nodePublisherlist );
                                $nodePublisherCorporation = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Corporation', '', [], $xml, $nodePublisher );
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Corporatename', $this->getParentElement( 'publisher' ), [], $xml, $nodePublisherCorporation, true );
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Address', $this->getParentElement( 'publication_place' ), [], $xml, $nodePublisher, true );
        
                                //Dates
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'DateReceived', !empty( $this->getElement( 'receiced' ) ) ? date( 'Ymd', strtotime( $this->getElement( 'reveived' ) ) ) : '', [], $xml, $nodeMetadata, true );
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'DateRevised', !empty( $this->getElement( 'revised' ) ) ? date( 'Ymd', strtotime( $this->getElement( 'revised' ) ) ) : '', [], $xml, $nodeMetadata, true );
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'DateAccepted', !empty( $this->getElement( 'accepted' ) ) ? date( 'Ymd', strtotime( $this->getElement( 'accepted' ) ) ) : '', [], $xml, $nodeMetadata, true );
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'DatePublished', !empty( $this->getElement( 'published' ) ) ? date( 'Ymd', strtotime( $this->getElement( 'published' ) ) ) : '', [], $xml, $nodeMetadata, true );
        
                                //Parent (Source)
                                $nodeJournal = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Journal', '', [], $xml, $nodeSourcegroup );
                                $volume = new Volume( $this->getElement( 'volume' ) );
                                $issue = new Issue( $this->getElement( 'issue' ) );
        
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'ISSN', $this->getParentElement( 'issn' ), [], $xml, $nodeJournal, true );
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Volume', $volume->getElement( 'number' ), [], $xml, $nodeJournal, true );
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Issue', $issue->getElement( 'number' ), [], $xml, $nodeJournal, true );
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'JournalTitle', $this->getParentElement( 'title' ), [], $xml, $nodeJournal, true );
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'JournalTitleAbbr', $this->getParentElement( 'title_nlm' ) ?? $this->getParentElement( 'title_abbr' ), [], $xml, $nodeJournal, true );
        
                                //DocNo
                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'ArticleNo', $this->getElement( 'docno' ), [], $xml, $nodeMetadata, true );
        
                                //Abstract
                                $abstract = base64_decode( $this->getElement( 'abstract' ) ) ?? '';
        
                                if ( substr( $abstract, 0, 9 ) == '<!DOCTYPE' ) {
                                        $_xml = new \DOMDocument();
                                        $_xml->loadHTML( $abstract );
                                        $_xpt = new \DOMXpath( $_xml );
                                        $abstract = preg_replace( '/\n|\r|\<body\>|\<\/body\>/', '', $_xml->saveHTML( $_xpt->query( '//html/body' )->item( 0 ) ) );
                                }
        
                                $nodeAbstract = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Abstract', '', [ 'language' => $origLang ], $xml, $nodeOrigdata, true );
                                \Drupal::service( 'publisso_gold.tools' )->AppendCdata( $xml, $nodeAbstract, $abstract );
        
                                foreach ( $this->getTranslations() as $lang ) {
                                        $abstract = base64_decode( $this->getElement( 'abstract', $lang ) ) ?? '';
                
                                        if ( substr( $abstract, 0, 9 ) == '<!DOCTYPE' ) {
                                                $_xml = new \DOMDocument();
                                                $_xml->loadHTML( $abstract );
                                                $_xpt = new \DOMXpath( $_xml );
                                                $abstract = preg_replace( '/\n|\r|\<body\>|\<\/body\>/', '', $_xml->saveHTML( $_xpt->query( '//html/body' )->item( 0 ) ) );
                                        }
                
                                        $nodeAbstract = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Abstract', '', [ 'language' => $lang ], $xml, $nodeOrigdata, true );
                                        \Drupal::service( 'publisso_gold.tools' )->AppendCdata( $xml, $nodeAbstract, $abstract );
                                }
        
                                //OrigText
                                $origText = base64_decode( $this->getElement( 'article_text' ) ) ?? '';
        
                                if ( substr( $origText, 0, 9 ) == '<!DOCTYPE' ) {
                                        $_xml = new \DOMDocument();
                                        $_xml->loadHTML( $origText );
                                        $_xpt = new \DOMXpath( $_xml );
                                        $origText = preg_replace( '/\n|\r|\<body\>|\<\/body\>/', '', $_xml->saveHTML( $_xpt->query( '//html/body' )->item( 0 ) ) );
                                }
        
                                $nodeOrigtext = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'FullText', '', [ 'language' => $origLang ], $xml, $nodeOrigdata, true );
                                \Drupal::service( 'publisso_gold.tools' )->AppendCdata( $xml, $nodeOrigtext, $origText );
        
                                foreach ( $this->getTranslations() as $lang ) {
                                        $origText = base64_decode( $this->getElement( 'articleText', $lang ) ) ?? '';
                
                                        if ( substr( $origText, 0, 9 ) == '<!DOCTYPE' ) {
                                                $_xml = new \DOMDocument();
                                                $_xml->loadHTML( $origText );
                                                $_xpt = new \DOMXpath( $_xml );
                                                $origText = preg_replace( '/\n|\r|\<body\>|\<\/body\>/', '', $_xml->saveHTML( $_xpt->query( '//html/body' )->item( 0 ) ) );
                                        }
                
                                        $nodeOrigtext = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'FullText', '', [ 'language' => $lang ], $xml, $nodeOrigdata, true );
                                        \Drupal::service( 'publisso_gold.tools' )->AppendCdata( $xml, $nodeOrigtext, $origText );
                                }
        
                                //References
                                $refList = [];
                                $origText = base64_decode( $this->getElement( 'article_text' ) ) ?? '';
        
                                $origTextXML = new \DOMDocument();
                                $origTextXML->loadHTML( $origText );
                                $origTextXPATH = new \DOMXpath( $origTextXML );
        
                                foreach ( $origTextXPATH->evaluate( '//a' ) as $nodeA ) {
                
                                        if ( $nodeA->hasAttribute( 'href' ) && substr( $nodeA->getAttribute( 'href' ), 0, 10 ) == '#rwPubRef~' ) {
                                                $refList[ $nodeA->nodeValue ] = new Reference( explode( '~', $nodeA->getAttribute( 'href' ) )[ 1 ] );
                                        }
                                }
        
                                foreach ( $refList as $nr => $reference ) {
                
                                        $nodeReference = \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Reference', '', [ 'refNo' => $nr ], $xml, $nodeReferences );
                                        $authorsUsed = [];
                
                                        foreach ( $reference->getAuthors() as $author ) {
                        
                                                $_author = $author[ 'lname' ] . ' ' . ( $author[ 'initials' ] ?? $author[ 'firstname' ] );
                        
                                                if ( !in_array( $_author, $authorsUsed ) ) {
                                
                                                        $authorsUsed[] = $_author;
                                                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'RefAuthor', $_author, [], $xml, $nodeReference, true );
                                                }
                                        }
                
                                        if ( $reference->getItem( 'title' ) ) \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'RefTitle', $reference->getItem( 'title' ), [], $xml, $nodeReference, true );
                                        if ( $reference->getItem( 'type' ) && $reference->getItem( 'parent_title' ) ) \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'Ref' . ucfirst( $reference->getItem( 'type' ) ), $reference->getItem( 'parent_title' ), [], $xml, $nodeReference, true );
                                        if ( $reference->getItem( 'pages' ) ) \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'RefPage', $reference->getItem( 'pages' ), [], $xml, $nodeReference, true );
                                        if ( $reference->getItem( 'link' ) ) \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'RefLink', $reference->getItem( 'link' ), [], $xml, $nodeReference, true );
                                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendChild( 'RefTotal', $reference->getElement( 'reference' ), [], $xml, $nodeReference, true );
                                }
        
                                //Attachments
        
                                if ( !empty( $this->getElement( 'files' ) ) ) {
                
                                        $nodeAttachments = \Drupal::service( 'publisso_gold.tools' )->DOMAppendchild( 'Attachments', '', [], $xml, $nodeMedia );
                                        $files = explode( ',', $this->getElement( 'files' ) );
                                        $MediaNo = 1;
                
                                        foreach ( $files as $file ) {
                        
                                                $nodeAttachment = \Drupal::service( 'publisso_gold.tools' )->DOMAppendchild( 'Attachments', '', [], $xml, $nodeAttachments );
                                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendchild( 'MediaNo', $MediaNo++, [], $xml, $nodeAttachment, true );
                        
                                                $file = new Blob( $file );
                        
                                                $url = Url::fromRoute( 'publisso_gold.getFile', [ 'fid' => $file->getId() ] );
                                                $url->setAbsolute();
                                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendchild( 'MediaID', $file->getId(), [ 'filename' => $file->meta[ 'name' ], 'mimeType' => $file->meta[ 'mime' ], 'size' => strlen( $file->data ), 'url' => $url->toString() ], $xml, $nodeAttachment, true );
                                                \Drupal::service( 'publisso_gold.tools' )->DOMAppendchild( 'AttachmentTitle', $file->meta[ 'description' ], [], $xml, $nodeAttachment, true );
                                        }
                
                                        \Drupal::service( 'publisso_gold.tools' )->DOMAppendchild( 'NoOfAttachments', --$MediaNo, [], $xml, $nodeAttachments, true );
                                }
                                
                                if($store && !$this->getElement('xml_blob_id')){
                                        
                                        $name = explode('.', $this->getElement('doi') ?? $this->getElement('id'));
                                        $name = $name[count($name)];
                                        $blob = new Blob();
                                        $blob->create($xml->saveXML, 'text/xml', ['name' => "$name.xml"]);
                                        $this->setElement('xml_blob_id', $blob->getId());
                                }
                                elseif($store && $this->getElement('xml_blob_id')){
                                        $blob = new Blob($this->getElement('xml_blob_id'));
                                        $blob->setContent($xml->saveXML());
                                }
                                
                                $response->setContent( $xml->saveXML() );
                        }
                        else{
                                
                                $blob = new Blob($this->getElement('xml_blob_id'));
                                $response->setContent($blob->data);
                        }
                        
                        
                        return $response;
                }
        
                /**
                 * @param $jrn_id
                 * @param $jrna_id
                 * @return string
                 */
                public function exportXML_Title($jrn_id, $jrna_id){
                        return '';
                }
        
                /**
                 * @param string $type
                 * @return \Drupal\Core\GeneratedUrl|Url|false|string|null
                 */
                public function getArticleLink ($type = 'link' ) {
                
                        /**
                         * use title only if this title is unique
                         */
                        $journal = $this->getMedium();
                        $url = null;
                        
                        if(!empty($this->getElement('volume'))){
                                $route = 'publisso_gold.journals_journal.volume.article';
                                $vol_id = $this->getElement('volume');
                                $url = Url::fromRoute($route, ['jrn_id' => $journal->getElement('id'), 'jrna_id' => $this->getElement('id'), 'vol_id' => $vol_id]);
                        }
        
                        if(!empty($this->getElement('issue'))){
                                $route = 'publisso_gold.journals_journal.volume.article';
                                $vol_id = $this->getElement('volume');
                                $iss_id = $this->getElement('issue');
                                $url = Url::fromRoute($route, ['jrn_id' => $journal->getElement('id'), 'jrna_id' => $this->getElement('id'), 'vol_id' => $vol_id, 'iss_id' => $iss_id]);
                        }
                
                        if(isset($url)){
                                $url->setAbsolute();
                                return $type == 'link' ? $url->toString() : ( $type == 'url' ? $url : false );
                        }
                
                        return null;
                }

                public function getXML4PDF() :\DOMDocument{

                        $journal        = JournalManager::getJournal($this->getElement('jrnid'), false);
                        $workflow       = WorkflowManager::getItem($this->getElement('wfid'));
                        $article        = $this;

                        $xml = new \DOMDocument('1.0', 'UTF-8');
                        $rootNode = $xml->appendChild($xml->createElement('record'));
                        $rootNode->setAttribute('type', 'article');

                        $_fulltext   = base64_decode( $article->getElement( 'article_text' ) );
                        $_references = base64_decode( $article->getElement( 'references' ) );

                        $rootNode->appendChild($xml->createElement('id', $article->getElement('id')))->setAttribute('title', 'ID');
                        $rootNode->appendChild($xml->createElement('parent-id', $journal->getElement('id')))->setAttribute('title', 'Parent ID');

                        $titleNode = $rootNode->appendChild($xml->createElement('title'));
                        $titleNode->setAttribute('title', 'Title');
                        $titleNode->appendChild($xml->createCDATASection($article->getElement('title')));

                        $titleTranslatedNode = $rootNode->appendChild($xml->createElement('title-translated'));
                        $titleTranslatedNode->setAttribute('title', 'Titel');
                        if(!empty($article->getElement('title_translated'))) $titleTranslatedNode->appendChild($xml->createCDATASection($article->getElement('title_tranlated')));

                        $keywordsNode = $rootNode->appendChild($xml->createElement('keywords'));
                        $keywordsNode->setAttribute('title', 'Keywords');

                        $keywords = json_decode($article->getElement('keywords')) ?? unserialize($article->getElement('keywords'));
                        $keywords = array_filter($keywords);
                        foreach($keywords as $keyword) $keywordsNode->appendChild($xml->createElement('keyword'))->appendChild($xml->createCDATASection($keyword));

                        $CAsNode = $rootNode->appendChild($xml->createElement('corresponding-authors'));
                        $CAsNode->setAttribute('title', 'Corresponding authors');

                        foreach ( $this->getCorrespondingAuthors() as $author ) {
                                $CANode = $CAsNode->appendChild($xml->createElement('author'));
                                $CANode->setAttribute('title', 'Corresponding author');
                                $CANode->appendChild($xml->createElement('graduation-suffix'))->appendChild($xml->createCDATASection($author->profile->getElement('graduation_suffix')));
                                $CANode->appendChild($xml->createElement('graduation'))->appendChild($xml->createCDATASection($author->profile->getElement('graduation')));
                                $CANode->appendChild($xml->createElement('country'))->appendChild($xml->createCDATASection(Publisso::tools()->getCountry($author->profile->getElement('country'))));
                                $CANode->appendChild($xml->createElement('department'))->appendChild($xml->createCDATASection($author->profile->getElement('department')));
                                $CANode->appendChild($xml->createElement('email'))->appendChild($xml->createCDATASection($author->profile->getElement('email')));
                                $CANode->appendChild($xml->createElement('telephone'))->appendChild($xml->createCDATASection($author->profile->getElement('telephone')));
                                $CANode->appendChild($xml->createElement('postal-code'))->appendChild($xml->createCDATASection($author->profile->getElement('postal_code')));
                                $CANode->appendChild($xml->createElement('institute'))->appendChild($xml->createCDATASection($author->profile->getElement('institute')));
                                $CANode->appendChild($xml->createElement('street'))->appendChild($xml->createCDATASection($author->profile->getElement('street')));
                                $CANode->appendChild($xml->createElement('city'))->appendChild($xml->createCDATASection($author->profile->getElement('city')));
                                $CANode->appendChild($xml->createElement('firstname'))->appendChild($xml->createCDATASection($author->profile->getElement('firstname')));
                                $CANode->appendChild($xml->createElement('lastname'))->appendChild($xml->createCDATASection($author->profile->getElement('lastname')));
                                $CANode->appendChild($xml->createElement('firstname'))->appendChild($xml->createCDATASection($author->profile->getElement('firstname')));
                                $CANode->appendChild($xml->createElement('initials'))->appendChild($xml->createCDATASection($author->profile->getFirstnameInitials()));
                                $CANode->appendChild($xml->createElement('affiliation'))->appendChild($xml->createCDATASection($author->profile->getAffiliation()));
                        }

                        $AsNode = $rootNode->appendChild($xml->createElement('authors'));
                        $AsNode->setAttribute('title', 'Authors');

                        $AffsNode = $rootNode->appendChild($xml->createElement('affiliations'));
                        $AffsNode->setAttribute('title', 'Affiliations');

                        $authors = [];
                        $affiliations = [];

                        $article_authors = json_decode( $article->getElement( 'authors' ) );

                        foreach ( array_filter($article_authors) as $_ ){
                                $weight = $_->weight ? $_->weight : 0;
                                while ( array_key_exists( $weight, $authors ) ) $weight++; //falls Autoren mit gleicher Gewichtung angegeben sind, damit sie sich nicht gegenseitig überschreiben
                                $authors[ $weight ] = $_;
                        }
                        ksort( $authors );

                        foreach ( $authors as $_ ) {

                                if ( property_exists($_, 'uid') && $_->uid && $_->profile_archive_id ) {
                                        $user = new User ($_->uid );
                                        $user->profile->loadSnapshot( $_->profile_archive_id );
                                        $affiliations [] = $user->profile->getAffiliation();
                                }
                                else {
                                        $affiliations [] = $_->affiliation;
                                }

                                if(property_exists($_, 'add_affiliations')){

                                        $addAffiliations = (array)$_->add_affiliations;
                                        usort($addAffiliations, function($a, $b){
                                                return $a->weight <=> $b->weight;
                                        });

                                        foreach($addAffiliations as $addAffiliation) $affiliations[] = $addAffiliation->affiliation;
                                }
                        }

                        $affiliations = array_keys( array_flip( $affiliations ) ); // make affs unique

                        for ( $c = 1; $c <= count( $affiliations ); $c++ ) {
                                if ( $affiliations [ $c - 1 ] ) {
                                        $affNode = $AffsNode->appendChild($xml->createElement('affiliation'));
                                        $affNode->setAttribute('number', $c);
                                        $affNode->setAttribute('title', 'Affiliation');
                                        $affNode->appendChild($xml->createCDATASection($affiliations[$c - 1]));
                                }
                        }

                        foreach ( $authors as $_ ) {

                                if ( $_->uid && $_->profile_archive_id ) {
                                        $user = new User ($_->uid );
                                        $user->profile->loadSnapshot( $_->profile_archive_id );
                                        $affiliation = $user->profile->getAffiliation();
                                }
                                else {
                                        $affiliation = $_->affiliation;
                                }

                                $affKey = [];

                                if(property_exists($_, 'add_affiliations')){
                                        foreach ($_->add_affiliations ?? [] as $addAffiliation){
                                                $affKey[] = array_search($addAffiliation->affiliation, $affiliations) + 1;
                                        }
                                }

                                $affKey[] = array_search( $affiliation, $affiliations ) + 1;
                                sort($affKey);

                                $ANode = $AsNode->appendChild($xml->createElement('author'));
                                $ANode->setAttribute('title', 'Author');
                                $ANode->appendChild($xml->createElement('affiliation-number', implode(',', $affKey)));
                                foreach ( ( array )$_ as $k => $v ) {
                                        if(is_scalar($v))
                                                $ANode->appendChild(($xml->createElement($k)))->appendChild($xml->createCDATASection($v));
                                }
                        }

                        if(!empty($article->getElement('license'))){
                                $license = new License($article->getElement('license'));
                                $licNode = $rootNode->appendChild($xml->createElement('license'));
                                $licNode->setAttribute('title', 'License');
                                $licNameNode = $licNode->appendChild($xml->createElement('name'));
                                $licNameNode->setAttribute('title', 'Name');
                                $licNameNode->appendChild($xml->createCDATASection($license->getElement('name')));
                                $licDescNode = $licNode->appendChild($xml->createElement('description'));
                                $licDescNode->setAttribute('title', 'Description');
                                $licDescNode->appendChild($xml->createCDATASection($license->getElement('description')));
                                $licUrlNode = $licNode->appendChild($xml->createElement('url'));
                                $licUrlNode->setAttribute('title', 'URL');
                                $licUrlNode->appendChild($xml->createCDATASection($license->getElement('url')));
                                $licImgNode = $licNode->appendChild($xml->createElement('img'));
                                $licImgNode->setAttribute('src', $license->getInlineImageData());
                        }

                        $abstractNode = $rootNode->appendChild($xml->createElement('abstract'));
                        $abstractNode->setAttribute('title', 'Abstract');
                        if(!empty($article->getElement('abstract'))) $abstractNode->appendChild($xml->createCDATASection(base64_decode($article->getElement('abstract'))));

                        $abstractTranslatedNode = $rootNode->appendChild($xml->createElement('abstract-translated'));
                        $abstractTranslatedNode->setAttribute('title', 'Zusammenfassung');
                        if(!empty($article->getElement('abstract_translated'))) $abstractTranslatedNode->appendChild($xml->createCDATASection(base64_decode($article->getElement('abstract_translated'))));

                        $rootNode->appendChild($xml->createElement('publication-year', $article->getElement('publication_year')))->setAttribute('title', 'Publication year');

                        $pubPlaceNode = $rootNode->appendChild($xml->createElement('publication-place'));
                        $pubPlaceNode->setAttribute('title', 'Publication place');
                        $pubPlaceNode->appendChild($xml->createCDATASection($article->getElement('publication_place')));

                        $publisher = $article->getElement('publisher') ?? $journal->getElement('publisher');
                        $pubNode = $rootNode->appendChild($xml->createElement('publisher'));
                        $pubNode->setAttribute('title', 'Publisher');
                        $pubNode->appendChild($xml->createCDATASection($publisher));

                        $chapterNode = $rootNode->appendChild($xml->createElement('fulltext'));
                        $chapterNode->setAttribute('title', 'Chapter text');
                        $chapterNode->appendChild($xml->createCDATASection($_fulltext));

                        $rootNode->appendChild($xml->createElement('doi', $article->getElement('doi')))->setAttribute('title', 'DOI');
                        $rootNode->appendChild($xml->createElement('urn', $article->getElement('urn')))->setAttribute('title', 'URN');

                        $fundingNode = $rootNode->appendChild($xml->createElement('funding'));
                        $fundingNode->setAttribute('title', 'Funding');

                        if(!(empty($article->getElement('funding_id')) || empty($article->getElement('funding_name')) || empty($article->getElement('funding_number')))){
                                $_ = $fundingNode->appendChild($xml->createElement('funding-id'));
                                $_->setAttribute('title', 'Funding ID');
                                $_->appendChild($xml->createCDATASection($article->getElement('funding_id')));

                                $_ = $fundingNode->appendChild($xml->createElement('funding-number'));
                                $_->setAttribute('title', 'Funding number');
                                $_->appendChild($xml->createCDATASection($article->getElement('funding_number')));

                                $_ = $fundingNode->appendChild($xml->createElement('funding-name'));
                                $_->setAttribute('title', 'Funding name');
                                $_->appendChild($xml->createCDATASection($article->getElement('funding_name')));
                        }

                        $errNode = $rootNode->appendChild($xml->createElement('erratum'));
                        $errNode->setAttribute('title', 'Erratum');
                        if(!empty($article->getElement('erratum'))) $errNode->appendChild($xml->createCDATASection($article->getElement('erratum')));

                        $corrNode = $rootNode->appendChild($xml->createElement('correction'));
                        $corrNode->setAttribute('title', 'Correction');
                        if(!empty($article->getElement('correction'))) $corrNode->appendChild($xml->createCDATASection($article->getElement('correction')));

                        $rootNode->appendChild($xml->createElement('author-uid', $article->getElement('author_uid')))->setAttribute('title', 'Author UID');
                        $rootNode->appendChild($xml->createElement('version', $article->getElement('version')))->setAttribute('title', 'Version');
                        $rootNode->appendChild($xml->createElement('ddc', Publisso::tools()->getDDC($article->getElement('author_uid'), 'name')))->setAttribute('title', 'DDC');

                        $corpNode = $rootNode->appendChild($xml->createElement('corporations'));
                        $corpNode->setAttribute('title', 'Corporations');

                        $corporations = json_decode($article->getElement('corporation')) ?? unserialize($article->getElement('corporation'));
                        $corporations = array_filter($corporations);

                        foreach($corporations as $_){
                                $corpNode->appendChild($xml->createElement('corporation'))->appendChild($xml->createCDATASection($_));
                        }

                        $coiNode = $rootNode->appendChild($xml->createElement('conflict-of-interest'));
                        $coiNode->setAttribute('title', 'Conflict of interest');
                        if(!empty($article->getElement('conflict_of_interest'))) $coiNode->appendChild($xml->createCDATASection($article->getElement('conflict_of_interest')));

                        $coiNode = $rootNode->appendChild($xml->createElement('conflict-of-interest-text'));
                        $coiNode->setAttribute('title', 'Conflict of interest text');
                        if(!empty($article->getElement('conflict_of_interest_text'))) $coiNode->appendChild($xml->createCDATASection($article->getElement('conflict_of_interest_text')));

                        $subCatNode = $rootNode->appendChild($xml->createElement('subcategory'));
                        $subCatNode->setAttribute('title', 'Subcategory');
                        if(!empty($article->getElement('sub_category'))) $subCatNode->appendChild($xml->createCDATASection($article->getElement('sub_category')));

                        $rootNode->appendChild($xml->createElement('published', $article->getElement('published')))->setAttribute('title', 'Published');

                        $refsNode = $rootNode->appendChild($xml->createElement('references'));
                        $refsNode->setAttribute('title', 'References');
                        foreach(array_filter( explode( "\n", $_references ) ) as $ref){
                                $refsNode->appendChild($xml->createElement('reference'))->appendChild($xml->createCDATASection(preg_replace('/^(\<a.+?\<\/a\>){0,1}\s*(\[.+?\])\s*(.+$)/', '$1$3', $ref)));
                        }

                        // citation_note
                        $cn = $this->getCitationNote( $journal, 'text' );
                        $cn = (str_replace('<br>', ' ', $cn));
                        $cnNode = $rootNode->appendChild($xml->createElement('citation-note'));
                        $cnNode->setAttribute('title', 'Citation note');
                        $cnNode->appendChild($xml->createCDATASection($cn));

                        // append license-information
                        $copyNode = $rootNode->appendChild($xml->createElement('copyright'));
                        $copyNode->setAttribute('title', 'Copyright');

                        reset($authors);
                        $name = implode(' ', [current($authors)->firstname, current($authors)->lastname]);
                        if(count($authors) > 1) $name .= ' et al.';
                        $copyNode->appendChild($xml->createElement('name'))->appendChild($xml->createCDATASection('© ' . date( 'Y' ).' '.$name));
                        $copyNode->appendChild($xml->createElement('description'))->appendChild($xml->createCDATASection(( ( string )t( ' This is an Open Access publication distributed under the terms of the ' ) ) . ($license ? $license->getElement( 'description' ) : null) . '. ' . ( ( string )t( 'See license information at ' ) ) . ($license ? $license->getElement( 'url' ) : '')));

                        //attachments
                        $attsNode = $rootNode->appendChild($xml->createElement('attachments'));
                        $attsNode->setAttribute('title', 'Attachments');

                        foreach(array_filter(explode(',', $article->getElement('files'))) as $blobID){

                                $blob = new Blob($blobID);
                                $attNode = $attsNode->appendChild($xml->createElement('attachment'));
                                foreach(array_keys($blob->meta) as $_){
                                        $attNode->appendChild($xml->createElement($_))->appendChild($xml->createCDATASection($blob->meta[$_]));
                                }
                                $attNode->appendChild($xml->createElement('type'))->appendChild($xml->createCDATASection($blob->type));
                                $attNode->appendChild($xml->createElement('url'))->appendChild($xml->createCDATASection($blob->getUrl()->setAbsolute()->toString()));
                        }

                        $xml->formatOutput = true;
                        return $xml;
                }

                /**
                 * @return array
                 */
                public function getCorrespondingAuthors () {
                        $authors = [];

                        foreach ( json_decode( $this->getElement( 'authors' ), true ) as $_ ) {

                                if ( array_key_exists('is_corresponding', $_) && $_ [ 'is_corresponding' ] == 1 && $_ [ 'uid' ] ) {
                                        $authors [] = new User ( $_ [ 'uid' ] );
                                }
                        }

                        return $authors;
                }
        }


