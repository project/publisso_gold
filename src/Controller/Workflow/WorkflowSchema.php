<?php
        /**
         * Created by PhpStorm.
         * User: sseliger
         * Date: 25.02.19
         * Time: 13:35
         */
        
        namespace Drupal\publisso_gold\Controller\Workflow;
        use Drupal\Core\Controller\ControllerBase;

        /**
         * Class WorkflowSchema
         * @package Drupal\publisso_gold\Controller\Workflow
         */
        class WorkflowSchema extends ControllerBase{
                
                private $elements;
                private $name;
                private $meta;
                
                public function __construct(string $name = null){
                        
                        $this->name = $name;
                        $this->elements = [];
                        
                        if($name) $this->load();
                }
        
                /**
                 * @param string $name
                 * @return false
                 * @throws \Exception
                 */
                public function createNew(string $name){
                        
                        $qry = \Drupal::database()->select('rwPubgoldWorkflowSchema', 't')->fields('t', [])->condition('name', $name, '=')->countQuery()->execute();
                        if($qry->fetchField()){
                                error_log('ERROR: Workflow-schema "'.$name.'" already exists!');
                                return false;
                        }
                        
                        $this->name = \Drupal::database()->insert('rwPubgoldWorkflowSchema')->fields(['name' => $name])->execute();
                }
        
                /**
                 * @param $elemName
                 */
                public function getElement($elemName) {
                
                }
        
                /**
                 * @return int|string|null
                 */
                public function getEntryPointID(){
                        
                        foreach($this->schema as $k => $v){
                                if($v['schema_entry_point']) return $k;
                                break;
                        }
                        
                        return null;
                }
        
                /**
                 * @return mixed|null
                 */
                public function getEntryPointIdentifier(){
        
                        foreach($this->schema as $k => $v){
                                if($v['schema_entry_point']) return $v['schema_identifier'];
                                break;
                        }
        
                        return null;
                }
        
                /**
                 * @return false
                 */
                public function load(){
                        
                        if(!$this->name) return false;
        
                        $qry = \Drupal::database()->select('rwPubgoldWorkflowSchema', 's');
                        $qry->leftJoin('rwPubgoldWorkflowSchema_Boxmapping', 'bm', 's.identifier = bm.schema_identifier AND bm.schema_name = s.name');
                        $qry->leftJoin('rwPubgoldWorkflowSchema_Boxes', 'b', 'bm.box_id = b.id');
                        $qry->addField('s', 'id', 'schema_id');
                        $qry->addField('s', 'name', 'schema_name');
                        $qry->addField('s', 'action', 'schema_action');
                        $qry->addField('s', 'identifier', 'schema_identifier');
                        $qry->addField('s', 'follower', 'schema_follower');
                        $qry->addField('s', 'currentrole', 'schema_currentrole');
                        $qry->addField('s', 'followerrole', 'schema_followerrole');
                        $qry->addField('s', 'followerrole_replacement', 'schema_followerrole_replacement');
                        $qry->addField('s', 'mailtemplate_enter', 'schema_mailtemplate_enter');
                        $qry->addField('s', 'mailtemplate_leave', 'schema_mailtemplate_leave');
                        $qry->addField('s', 'description', 'schema_description');
                        $qry->addField('s', 'status', 'schema_status');
                        $qry->addField('s', 'select_action', 'schema_select_action');
                        $qry->addField('s', 'select_action_required_field', 'schema_select_action_required_field');
                        $qry->addField('s', 'default_action', 'schema_default_action');
                        $qry->addField('s', 'is_alternative_submit', 'schema_is_alternative_submit');
                        $qry->addField('s', 'entry_point', 'schema_entry_point');
                        $qry->addField('s', 'comment_required', 'schema_comment_required');
                        $qry->addField('b', 'id', 'box_id');
                        $qry->addField('b', 'name', 'box_name');
                        $qry->addField('b', 'type', 'box_type');
                        $qry->addField('b', 'description', 'box_description');
                        $qry->addField('bm', 'weight', 'box_weight');
                        $qry->addField('bm', 'hideonempty', 'box_hideonempty');
                        $qry->addField('bm', 'only_show_on_empty_fields', 'box_only_show_on_empty_fields');
                        $qry->addField('bm', 'schema_follower', 'box_follower_overwrite');
                        $qry->condition('s.name', $this->name, '=');
                        $qry->orderBy('s.identifier', 'ASC');
                        $qry->orderBy('bm.weight', 'ASC');
                        
                        $ret = [];
                        foreach($qry->execute()->fetchAll() as $_){
                                
                                $_ = (array) $_;
                                
                                foreach($_ as $k => $v){
                                        
                                        if(substr($k, 0, 6) == 'schema'){
                                                $ret[$_['schema_id']][$k] = $v;
                                        }
        
                                        if(substr($k, 0, 3) == 'box' && !empty($_['box_id'])){
                                                $ret[$_['schema_id']]['boxes'][$_['box_id']][$k] = $v;
                                        }
                                }
                        }
        
                        $this->schema = $ret;
                        $this->meta = \Drupal::database()->select('rwPubgoldWorkflowSchemaMeta', 't')->fields('t', [])->condition('schema_name', $this->name, '=')->execute()->fetchAssoc();
                }
        
                /**
                 * @param array $schema
                 */
                public function import(array $schema){
                        $this->schema = $schema;
                        
                }
        
                /**
                 * @return mixed
                 */
                public function export(){
                        return $this->schema;
                }
        
                /**
                 * @param null $schema_id
                 * @return mixed|null
                 */
                public function getSchemaElement($schema_id = null){
                        
                        if(!$schema_id){
                                reset($this->schema);
                                return current($this->schema);
                        }
                        else{
                                return $this->schema[$schema_id] ?? null;
                        }
                }
        
                /**
                 * @param $schema_id
                 * @return mixed
                 */
                public function getStateFromSchema($schema_id){
                        
                        return $this->schema[$schema_id]['schema_status'];
                }
        }
