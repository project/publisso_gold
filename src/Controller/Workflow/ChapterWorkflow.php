<?php
        /**
         * Created by PhpStorm.
         * User: sseliger
         * Date: 11.02.19
         * Time: 11:12
         */

        namespace Drupal\publisso_gold\Controller\Workflow;


        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\UserManager;
        use Drupal\publisso_gold\Controller\User;
        use Drupal\Component\Render\FormattableMarkup;
        use Drupal\publisso_gold\Controller\Manager\BookManager;

        /**
         * Class ChapterWorkflow
         * @package Drupal\publisso_gold\Controller\Workflow
         */
        class ChapterWorkflow extends Workflow {

                /**
                 * ChapterWorkflow constructor.
                 * @param null $id
                 * @param string $tbl_column_prefix
                 * @param bool $loadMedium
                 */
                public function __construct($id = null, $tbl_column_prefix = 'wf_', $loadMedium = true) {

                        $this->session           = \Drupal::service('session');
                        $this->medium            = null;
                        $this->stateChain        = null;
                        $this->reviewpages       = $this->history = $this->editorial_office = $this->editors = $this->editors_in_chief = $this->reviewers = $this->elements = [];
                        $this->data              = (object)[];
                        $this->tbl_column_prefix = $tbl_column_prefix;
                        $this->type              = 'bcs';

                        if ($id !== null) {

                                if ($tbl_column_prefix == 'wf_')
                                        $this->db_table = 'rwPubgoldWorkflow';
                                elseif ($tbl_column_prefix == 'wfh_')
                                        $this->db_table = 'rwPubgoldWorkflowHistory';

                                $this->id = $id;
                                $this->load($this->id);
                        } else {
                                $this->db_table = 'rwPubgoldWorkflow';
                        }
                }

                /**
                 * @param $bk_id
                 * @param $type
                 * @param null $schema_id
                 * @return false
                 * @throws \Exception
                 */
                public function init($bk_id, $type, $schema_id = null){

                        if($this->id || !\Drupal::service('session')->get('user')['id']) return false;

                        $this->id = \Drupal::database()->insert('rwPubgoldWorkflow')->fields([
                                'wf_created_by_uid' => \Drupal::service('session')->get('user')['id']
                        ])->execute();
                        $this->load();

                        if(!$schema_id){
                                $book = BookManager::getBook($bk_id, false);

                                if($book->getElement('wfschema')){
                                        $schema_id = \Drupal::database()->select('rwPubgoldWorkflowSchema', 's')->fields('s', ['id'])->condition('name', $book->getElement('wfschema'), '=')->condition('entry_point', 1, '=')->execute()->fetchField();
                                }
                        }

                        if($schema_id){
                                $this->initSchema($schema_id);
                        }

                        $this->type = $type ?? $this->type;
                        $this->setElement('type', $this->type);
                        $this->setDataElement('bk_id', $bk_id);
                        $this->assignToUser(\Drupal::service('session')->get('user')['id']);
                        $this->load();
                }

                /**
                 * @param bool $sendInfoMail
                 * @param false $preliminary
                 * @return \Drupal\Core\Database\StatementInterface|false|int|string|null
                 * @throws \Exception
                 */
                public function publish($sendInfoMail = true, $preliminary = false) {

                        if (!$this->id) return null;

                        //create profile-archive to save author-data on publishing-time
                        $authors = [];

                        $authorsDB = json_decode($this->getDataElement('author_db'), true) ?? [];

                        foreach($authorsDB as $_){

                                if(!array_key_exists('uid', $_)) continue;

                                $author = UserManager::getUserFromID($_['uid']);

                                $authors[] = array_merge([
                                                                 'firstname' => $author->profile->getElement('firstname'),
                                                                 'lastname' => $author->profile->getElement('lastname'),
                                                                 'affiliation' => $author->profile->getAffiliation()
                                                         ], $_);
                        }

                        $authorsAdd = json_decode($this->getDataElement('author_add'), true) ?? [];

                        foreach($authorsAdd as $k => &$author){
                                if(empty($author['firstname']) || empty($author['lastname'])) unset($authorsAdd[$k]);
                                if(array_key_exists('add_affiliations', $author)){
                                        $affs = $author['add_affiliations'];

                                        $c = 0;
                                        foreach($affs as $aff) {
                                                $author['add_affiliations'][$c++] = (object)[
                                                        'affiliation' => $aff,
                                                        'weight' => $c,
                                                        'show' => 1
                                                ];
                                        }
                                }
                        }

                        $authors = array_merge($authors, $authorsAdd);

                        $this->setDataElement('authors', json_encode($authors));
                        // -- profile-archive

                        //save chapter
                        $data = [
                                'cp_abstract' => $this->getDataElement('abstract'),
                                'cp_author_uid' => $this->getElement('created_by_uid'),
                                'cp_bkid' => $this->getDataElement('bk_id'),
                                'cp_chapter_text' => $this->getDataElement('chapter_text'),
                                'cp_correction' => $this->getDataElement('correction'),
                                'cp_corresponding_author' => $this->getDataElement('corresponding_authors'),
                                'cp_ddc' => $this->getDataElement('ddc'),
                                'cp_doi' => $this->getDataElement('doi'),
                                'cp_errata' => $this->getDataElement('errata'),
                                'cp_funding_id' => $this->getDataElement('funding_id'),
                                'cp_funding_number' => $this->getDataElement('funding_number'),
                                'cp_funding_name' => $this->getDataElement('funding_name'),
                                'cp_keywords' => $this->getDataElement('keywords'),
                                'cp_license' => $this->getDataElement('license'),
                                'cp_publication_place' => $this->getDataElement('publication_place'),
                                'cp_publication_year' => !empty($this->getDataElement('publication_year')) ? $this->getDataElement('publication_year') : null,
                                'cp_publisher' => $this->getDataElement('publisher'),
                                'cp_title' => $this->getDataElement('title'),
                                'cp_urn' => $this->getDataElement('urn'),
                                'cp_version' => $this->getDataElement('version'),
                                'cp_patients_rights' => $this->getDataElement('patients_rights'),
                                'cp_author_contract' => $this->getDataElement('author_contract'),
                                'cp_conflict_of_interest' => $this->getDataElement('conflict_of_interest'),
                                'cp_conflict_of_interest_text' => $this->getDataElement('conflict_of_interest_text'),
                                'cp_corporation' => $this->getDataElement('corporations'),
                                'cp_sub_category' => $this->getDataElement('sub_category'),
                                'cp_more_authors' => $this->getDataElement('more_authors'),
                                'cp_wfid' => $this->getElement('id'),
                                'cp_authors' => $this->getDataElement('authors'),
                                'cp_policy_accept' => $this->getDataElement('accept_policy'),
                                'cp_published' => $this->getDataElement('published') ? $this->getDataElement('published') : date("Y-m-d"),
                                'cp_files' => $this->getDataElement('files') ? implode(',', unserialize($this->getDataElement('files'))) : null,
                                'cp_title_short' => $this->getDataElement('title_short'),
                                'cp_references' => null,
                                'cp_received' => !empty($this->getDataElement('received')) ? $this->getDataElement
                                ('received') : null,
                                'cp_revised' => !empty($this->getDataElement('revised')) ? $this->getDataElement('revised') :
                                        null,
                                'cp_accepted' => !empty($this->getDataElement('accepted')) ? $this->getDataElement('accepted')
                                        :
                                        null
                        ];

                        if (!$preliminary) {
                                //any references?
                                if (!empty($this->getDataElement('references'))) {

                                        $data['cp_chapter_text'] = base64_encode(\Drupal::service('publisso_gold.tools')->mapReferences(
                                                base64_decode($this->getDataElement('chapter_text')), $this->getDataElement('references')));
                                }
                        }

                        if (!$this->getDataElement('cp_id')) {
                                $cp_id = \Drupal::database()->insert('rwPubgoldBookChapters')->fields($data)->execute();
                                $this->setDataElement('cp_id', $cp_id);
                        } else {
                                $cp_id = $this->getDataElement('cp_id');
                                \Drupal::database()->update('rwPubgoldBookChapters')->fields($data)->condition('cp_id', $cp_id, '=')->execute();
                        }


                        if (!$preliminary) {
                                $this->setDataElement('state', 'published');

                                $this->load();
                                if (!$this->medium) $this->loadMedium();

                                $vars = [
                                        '::lnk.submission::' => Url::fromRoute('publisso_gold.book.chapter', ['bk_id' => $this->medium->getElement('id'), 'cp_id' => $cp_id])->setAbsolute()->toString()
                                ];
                        }

                        if ($this->getElement('state') == 'published') {

                                $chapter = new \Drupal\publisso_gold\Controller\Bookchapter($cp_id);
                                $urlChapter = $chapter->getChapterLink('url');
                                $urlChapter->setAbsolute();
                                \Drupal::service('messenger')->addMessage(
                                        new FormattableMarkup(
                                                (string)t('The chapter has been published successfully. You can see the chapter by visiting <a href="@link">@link</a>'),
                                                ['@link' => $urlChapter->toString()]
                                        )
                                );
                        }
                        return $cp_id;
                }
        }
