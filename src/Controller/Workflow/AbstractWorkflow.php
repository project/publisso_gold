<?php
        /**
         * Created by PhpStorm.
         * User: sseliger
         * Date: 11.02.19
         * Time: 11:12
         */
        
        namespace Drupal\publisso_gold\Controller\Workflow;
        
        
        use Drupal\Component\Render\FormattableMarkup;
        use Drupal\publisso_gold\Controller\Manager\AbstractManager;
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\SubmissionFundingManager;
        use Drupal\publisso_gold\Controller\Medium\Conference;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class AbstractWorkflow
         * @package Drupal\publisso_gold\Controller\Workflow
         */
        class AbstractWorkflow extends Workflow {
        
                /**
                 * AbstractWorkflow constructor.
                 * @param null $id
                 * @param string $tbl_column_prefix
                 * @param bool $loadMedium
                 */
                public function __construct($id = null, $tbl_column_prefix = 'wf_', $loadMedium = true) {
                
                        $this->session           = \Drupal::service('session');
                        $this->medium            = null;
                        $this->stateChain        = null;
                        $this->reviewpages       = $this->history = $this->editorial_office = $this->editors = $this->editors_in_chief = $this->reviewers = $this->elements = [];
                        $this->data              = (object)[];
                        $this->tbl_column_prefix = $tbl_column_prefix;
                        $this->type              = 'cas';
                
                        if ($id !== null) {
                                
                                if ($tbl_column_prefix == 'wf_')
                                        $this->db_table = 'rwPubgoldWorkflow';
                                elseif ($tbl_column_prefix == 'wfh_')
                                        $this->db_table = 'rwPubgoldWorkflowHistory';
                        
                                $this->id = $id;
                                $this->load($this->id);
                        } else {
                                $this->db_table = 'rwPubgoldWorkflow';
                        }
                }
        
                /**
                 * @param $cf_id
                 * @param $type
                 * @param null $schema_id
                 * @return false
                 * @throws \Exception
                 */
                public function init($cf_id, $type, $schema_id = null){
                        
                        if($this->id || !\Drupal::service('session')->get('user')['id']) return false;
                        
                        if(null === ($ca_id = AbstractManager::create($cf_id))){
                                \Drupal::service('messenger')->addError((string)$this->t('Cant create Item. Error during init submedium.'));
                                return false;
                        }
                        
                        $abstract = AbstractManager::getAbstract($ca_id);
                        
                        $this->id = \Drupal::database()->insert('rwPubgoldWorkflow')->fields([
                                'wf_created_by_uid' => \Drupal::service('session')->get('user')['id']
                        ])->execute();
                        $this->load();
                        
                        $abstract->setElement('wfid', $this->id);
                        
                        if(!$schema_id){
                                
                                $conference = ConferenceManager::getConference($cf_id, false);
                                if($conference->getElement('wfschema')){
                                        $schema_id = \Drupal::database()->select('rwPubgoldWorkflowSchema', 's')->fields('s', ['id'])->condition('name', $conference->getElement('wfschema'), '=')->condition('entry_point', 1, '=')->execute()->fetchField();
                                }
                        }
                        
                        if($schema_id){
                                $this->initSchema($schema_id);
                        }
                        
                        if($conference){
                                $abstract->setElement('publisher_abbr', $conference->getElement('publisher_abbr'));
                                $abstract->setElement('publisher_name', $conference->getElement('publisher_name'));
                                $abstract->setElement('publisher_city', $conference->getElement('publisher_city'));
                                
                                $this->setDataElement('publisher_abbr', $conference->getElement('publisher_abbr'));
                                $this->setDataElement('publisher_name', $conference->getElement('publisher_name'));
                                $this->setDataElement('publisher_city', $conference->getElement('publisher_city'));
                        }
                        
                        $this->type = $type ?? $this->type;
                        $this->setElement('type', $this->type);
                        $this->setDataElement('cf_id', $cf_id);
                        $this->setDataElement('ca_id', $ca_id);
                        $this->setDataElement('doc_no', $abstract->getElement('doc_no'));
                        $this->assignToUser(\Drupal::service('session')->get('user')['id']);
                        $abstract->save();
                        $this->load();
                }
                
                public function transferData2Medium(){
                        
                        $workflow = &$this;
        
                        $abstract = $workflow->getAbstract();
                        $conference = $workflow->getConference();
        
                        if($abstract) {
        
                                foreach ( $workflow->getDataKeys() as $key ) {
                                        if ( $abstract->hasElement( $key ) ) {
                                                $abstract->setElement( $key, !empty($workflow->getDataElement( $key )) ? $workflow->getDataElement( $key ) : null );
                                        }
                                }
        
                                //authors_db - split name
                                if ( false === ( $authors = @unserialize( $workflow->getDataElement( 'authors_db' ) ) ) ) $authors = [];
        
                                foreach ( $authors as $i => $author ) {
                                        $name = explode( ';', $author[ 'author' ], 2 );
                                        [ $lastname, $firstname ] = explode( ',', $name[ 0 ] );
                                        $affiliation = $name[ 1 ] ?? '';
                                        $authors[ $i ] = array_merge( $author, [ 'firstname' => trim( $firstname ), 'lastname' => trim( $lastname ), 'affiliation' => trim( $affiliation ) ] );
                                }
                                $abstract->setElement( 'authors_db', serialize( $authors ) );
        
                                //authors contract
                                $abstract->setElement( 'authors_contract', $conference->getElement( 'authors_contract' ) );
        
                                //translations
                                $translationMapping = [
                                        'title'    => 'title',
                                        'keywords' => 'keywords',
                                        'text'     => 'main_text',
                                        'coi'      => 'conflict_of_interest'
                                ];
        
                                foreach ( $translationMapping as $identifierPrefix => $field ) {
                
                                        if ( $workflow->hasDataElement( $identifierPrefix . '_translations' ) ) {
                        
                                                $translations = @unserialize( $workflow->getDataElement( $identifierPrefix . '_translations' ) );
                                                if ( !$translations ) $translations = [];
                                                
                                                foreach($translations as $k => $v){
                                                        if(!$v['language']) unset($translations[$k]);
                                                }
                                                
                                                if ( !count( $translations ) ) {
                                                        $abstract->deleteElementTranslations( $field );
                                                }
                                                else {
                                                        foreach ( $translations as $translation ) {
                                                                $abstract->setElementTranslation( $field, is_scalar( $translation[ $field ] ) ? $translation[ $field ] : serialize( $translation[ $field ] ), $translation[ 'language' ] );
                                                        }
                                                }
                                        }
                                        else {
                                                $abstract->deleteElementTranslations( $field );
                                        }
                                }
        
                                //fundings
                                if ( $workflow->hasDataElement( 'ctrlFunding' ) && !!$workflow->getDataElement( 'ctrlFunding' ) === true ) {
                                        SubmissionFundingManager::delete( $abstract->getId(), 'abstract' );
                                        $fundings = @unserialize( $workflow->getDataElement( 'funding' ) );
                
                                        if ( !$fundings ) $fundings = [];
                                        $number = 1;
                                        foreach ( $fundings as $f ) {
                        
                                                $funding = SubmissionFundingManager::create( $abstract->getId(), 'abstract', $number++ );
                        
                                                if ( $funding ) {
                                
                                                        switch ( $f[ 'funding_type' ] ) {
                                                                case 'manual':
                                                                        $funding->setElement( 'funder_name', $f[ 'funder_name' ] ?? null );
                                                                        $funding->setElement( 'funder_id', $f[ 'funder_id' ] ?? null );
                                                                        break;
                                        
                                                                case 'stream':
                                                                        $funding->setElement( 'funder', $f[ 'funder' ] ?? null );
                                                                        $funding->setElement( 'funder_award_number', $f[ 'funder_award_number' ] ?? null );
                                                                        $funding->setElement( 'funder_award_uri', $f[ 'funder_award_uri' ] ?? null );
                                                                        $funding->setElement( 'funder_award_identifier', $f[ 'funder_identifier' ] ?? null );
                                                                        $funding->setElement( 'funder_funding_stream', $f[ 'funder_funding_stream' ] ?? null );
                                                                        $funding->setElement( 'funder_award_title', $f[ 'funder_award_title' ] ?? null );
                                                                        break;
                                                        }
                                
                                                        $funding->setElement( 'type', $f[ 'funding_type' ] );
                                                        $funding->save();
                                                }
                                        }
                                }
                                
                                $abstract->save();
                        }
                }
        
                /**
                 * @param bool $sendInfoMail
                 * @param false $preliminary
                 * @throws \Exception
                 */
                public function publish($sendInfoMail = true, $preliminary = false) {
                        
                        if(!$this->getDataElement('published')) $this->setDataElement('published', date('Y-m-d'));
                        $this->transferData2Medium();
                        $this->getParentMedium()->renewTOC();
                }
        
                /**
                 * @param array $ids
                 * @throws \Exception
                 */
                public function setConferenceManagerIDs(array $ids = []){
                        $this->setElement('assigned_to_cm', implode(',', array_filter($ids)));
                }
                
                public function getMedium() :Conference{
                        return ConferenceManager::getConference($this->getDataElement('cf_id'));
                }
        
                /**
                 * @return \Drupal\publisso_gold\Controller\Submedium\ConferenceAbstract
                 */
                public function getAbstract(){
                        return AbstractManager::getAbstract($this->getDataElement('ca_id'));
                }
        
                /**
                 * @return Conference|null
                 */
                public function getConference(){
                        return ConferenceManager::getConference($this->getDataElement('cf_id'));
                }
        }
