<?php
        /**
         * Created by PhpStorm.
         * User: sseliger
         * Date: 11.02.19
         * Time: 11:12
         */

        namespace Drupal\publisso_gold\Controller\Workflow;


        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\UserManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\User;
        use Drupal\Component\Render\FormattableMarkup;
        use Drupal\publisso_gold\Controller\Manager\ArticleManager;
        use Drupal\publisso_gold\Controller\Manager\JournalManager;

        /**
         * Class ArticleWorkflow
         * @package Drupal\publisso_gold\Controller\Workflow
         */
        class ArticleWorkflow extends Workflow {

                /**
                 * ArticleWorkflow constructor.
                 * @param null $id
                 * @param string $tbl_column_prefix
                 * @param bool $loadMedium
                 */
                public function __construct($id = null, $tbl_column_prefix = 'wf_', $loadMedium = true) {

                        $this->session           = \Drupal::service('session');
                        $this->medium            = null;
                        $this->stateChain        = null;
                        $this->reviewpages       = $this->history = $this->editorial_office = $this->editors = $this->editors_in_chief = $this->reviewers = $this->elements = [];
                        $this->data              = (object)[];
                        $this->tbl_column_prefix = $tbl_column_prefix;
                        $this->type              = 'jas';

                        if ($id !== null) {

                                if ($tbl_column_prefix == 'wf_')
                                        $this->db_table = 'rwPubgoldWorkflow';
                                elseif ($tbl_column_prefix == 'wfh_')
                                        $this->db_table = 'rwPubgoldWorkflowHistory';

                                $this->id = $id;
                                $this->load($this->id);
                        } else {
                                $this->db_table = 'rwPubgoldWorkflow';
                        }
                }

                /**
                 * @param $jrn_id
                 * @param $type
                 * @param null $schema_id
                 * @return false
                 * @throws \Exception
                 */
                public function init($jrn_id, $type, $schema_id = null){

                        if($this->id || !\Drupal::service('session')->get('user')['id']) return false;

                        $this->id = \Drupal::database()->insert('rwPubgoldWorkflow')->fields([
                             'wf_created_by_uid' => \Drupal::service('session')->get('user')['id']
                        ])->execute();
                        $this->load();

                        if(!$schema_id){
                                $journal = JournalManager::getJournal($jrn_id, false);

                                if($journal->getElement('wfschema')){
                                        $schema_id = \Drupal::database()->select('rwPubgoldWorkflowSchema', 's')->fields('s', ['id'])->condition('name', $journal->getElement('wfschema'), '=')->condition('entry_point', 1, '=')->execute()->fetchField();
                                }
                        }

                        if($schema_id){
                                $this->initSchema($schema_id);
                        }

                        $this->type = $type ?? $this->type;
                        $this->setElement('type', $this->type, false);
                        $this->setDataElement('jrn_id', $jrn_id, false);
                        $this->setDataElement('volume', $this->getMedium()->getCurrentVolume()->getElement('id'), false);
                        $this->save();
                        $this->assignToUser(\Drupal::service('session')->get('user')['id']);
                        $this->load();
                }

                /**
                 * @param bool $sendInfoMail
                 * @param false $preliminary
                 * @return \Drupal\Core\Database\StatementInterface|false|int|string|void|null
                 * @throws \Exception
                 */
                public function publish($sendInfoMail = true, $preliminary = false) {

                        if (!$this->id) return null;

                        //create profile-archive to save author-data on publishing-time
                        $authors = [];

                        $authorsDB = json_decode($this->getDataElement('author_db'), true) ?? [];

                        foreach($authorsDB as $_){

                                if(!array_key_exists('uid', $_)) continue;

                                $author = UserManager::getUserFromID($_['uid']);

                                $authors[] = array_merge([
                                        'firstname' => $author->profile->getElement('firstname'),
                                        'lastname' => $author->profile->getElement('lastname'),
                                        'affiliation' => $author->profile->getAffiliation()
                                ], $_);
                        }

                        $authorsAdd = json_decode($this->getDataElement('author_add'), true) ?? [];

                        foreach($authorsAdd as $k => &$author){
                                if(empty($author['firstname']) || empty($author['lastname'])) unset($authorsAdd[$k]);
                                if(array_key_exists('add_affiliations', $author)){
                                        $affs = $author['add_affiliations'];

                                        $c = 0;
                                        foreach($affs as $aff) {
                                                $author['add_affiliations'][$c++] = (object)[
                                                        'affiliation' => $aff,
                                                        'weight' => $c,
                                                        'show' => 1
                                                ];
                                        }
                                }
                        }

                        $authors = array_merge($authors, $authorsAdd);

                        $this->setDataElement('authors', json_encode($authors));
                        // -- profile-archive

                        //save chapter
                        $data = [
                                'jrna_abstract' => $this->getDataElement('abstract'),
                                'jrna_author_uid' => $this->getElement('created_by_uid'),
                                'jrna_jrnid' => $this->getDataElement('jrn_id'),
                                'jrna_article_text' => $this->getDataElement('article_text'),
                                'jrna_article_type' => $this->getDataElement('article_type'),
                                'jrna_correction' => $this->getDataElement('correction'),
                                'jrna_corresponding_author' => $this->getDataElement('corresponding_authors'),
                                'jrna_ddc' => $this->getDataElement('ddc'),
                                'jrna_doi' => $this->getDataElement('doi'),
                                'jrna_funding_id' => $this->getDataElement('funding_id'),
                                'jrna_funding_name' => $this->getDataElement('funding_name'),
                                'jrna_volume' => $this->getDataElement('volume'),
                                'jrna_issue' => $this->getDataElement('issue'),
                                'jrna_keywords' => $this->getDataElement('keywords'),
                                'jrna_license' => $this->getDataElement('license'),
                                'jrna_publication_place' => $this->getDataElement('publication_place'),
                                'jrna_publication_year' => !empty($this->getDataElement('publication_year')) ? $this->getDataElement('publication_year') : null,
                                'jrna_publisher' => $this->getDataElement('publisher'),
                                'jrna_title' => $this->getDataElement('title'),
                                'jrna_urn' => $this->getDataElement('urn'),
                                'jrna_version' => $this->getDataElement('version'),
                                'jrna_patients_rights' => $this->getDataElement('patients_rights'),
                                'jrna_author_contract' => $this->getDataElement('author_contract'),
                                'jrna_conflict_of_interest' => $this->getDataElement('conflict_of_interest'),
                                'jrna_conflict_of_interest_text' => $this->getDataElement('conflict_of_interest_text'),
                                'jrna_corporation' => json_encode(array_filter(json_decode($this->getDataElement('corporations'), true) ?? [])),
                                'jrna_sub_category' => $this->getDataElement('sub_category'),
                                'jrna_more_authors' => $this->getDataElement('more_authors'),
                                'jrna_docno' => $this->getDataElement('doc_no'),
                                'jrna_wfid' => $this->getElement('id'),
                                'jrna_authors' => $this->getDataElement('authors'),
                                'jrna_policy_accept' => $this->getDataElement('accept_policy'),
                                'jrna_published' => !$preliminary ? date("Y-m-d") : null,
                                'jrna_files' => $this->getDataElement('files') ? implode(',', unserialize($this->getDataElement('files'))) : null,
                                'jrna_received' => !empty($this->getDataElement('received')) ? $this->getDataElement('received') : null,
                                'jrna_revised' => !empty($this->getDataElement('revised')) ? $this->getDataElement('revised') : null,
                                'jrna_accepted' => !empty($this->getDataElement('accepted')) ? $this->getDataElement('accepted') : null,
                                'jrna_citation_note' => null
                        ];

                        //any references?
                        if(!$preliminary) {
                                if ( !empty( $this->getDataElement( 'references' ) ) ) {

                                        $data[ 'jrna_article_text' ] = base64_encode( \Drupal::service( 'publisso_gold.tools' )->mapReferences(
                                                base64_decode( $this->getDataElement( 'article_text' ) ), $this->getDataElement( 'references' )
                                        )
                                        );
                                }
                        }

                        if (!$this->getDataElement('jrna_id')) {
                                $jrna_id = \Drupal::database()->insert('rwPubgoldJournalArticles')->fields($data)->execute();
                                $this->setDataElement('jrna_id', $jrna_id);
                        } else {
                                $jrna_id = $this->getDataElement('jrna_id');
                                \Drupal::database()->update('rwPubgoldJournalArticles')->fields($data)->condition('jrna_id', $jrna_id, '=')->execute();
                        }

                        if(!$preliminary) {

                                $this->setDataElement( 'state', 'published' );

                                $this->load();
                                if ( !$this->medium ) $this->loadMedium();
                                $vars = [
                                        '::lnk.submission::' => Url::fromRoute( 'publisso_gold.journals_journal', [ 'jrn_id' => $this->medium->getElement( 'id' ), 'jrna_id' => $jrna_id ] )->setAbsolute()->toString()
                                ];
                        }

                        if ($this->getElement('state') == 'published') {

                                $article = ArticleManager::getArticle($jrna_id);

                                if(null !== ($urlArticle = $article->getArticleLink('url'))) {

                                        $urlArticle->setAbsolute();
                                        \Drupal::service('messenger')->addMessage(
                                                new FormattableMarkup(
                                                        (string)t('The article has been published successfully. You can see the article by visiting <a href="@link">@link</a>'),
                                                        ['@link' => $urlArticle->toString()]
                                                )
                                        );
                                }
                        }

                        return $jrna_id;
                }
        }
