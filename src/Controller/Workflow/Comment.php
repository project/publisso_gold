<?php
        /**
         * Created by PhpStorm.
         * User: sseliger
         * Date: 05.03.19
         * Time: 07:29
         */
        
        namespace Drupal\publisso_gold\Controller\Workflow;
        
        
        use Drupal\Core\Controller\ControllerBase;

        /**
         * Class Comment
         * @package Drupal\publisso_gold\Controller\Workflow
         */
        class Comment extends ControllerBase {
                
                private $elements;
                private $id;
                
                const ROLES = [
                        'administrator'         => 70,
                        'publisso'              => 60,
                        'editorial_office'      => 50,
                        'editors_in_chief'      => 40,
                        'editors'               => 30,
                        'reviewer'              => 20,
                        'submitting_author'     => 10
                ];
        
                /**
                 * Comment constructor.
                 * @param null $id
                 */
                public function __construct($id = null){

                        $this->elements = [];
                        if($id) $this->loadFromDB($id);
                }
        
                /**
                 * @param $id
                 * @return \Symfony\Component\HttpFoundation\RedirectResponse
                 */
                public function delete($id){

                        list($wf_id, $this->id) = explode('|', $id);
                        $this->loadFromDB($this->id);
                        $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($wf_id);

                        if($this->getElement('creator_uid') == \Drupal::service('session')->get('user')['id'] &&
                                $this->getElement('wf_schema_id') == $workflow->getElement('schema_identifier')){
                                \Drupal::database()->delete('rwPubgoldComment')->condition('id', $this->id, '=')
                                        ->execute();
                                \Drupal::service('messenger')->addMessage((string)$this->t('Comment deleted!'));
                        }
                        else{
                                \Drupal::service('messenger')->addError((string)$this->t('You don\'t have permissions to remove this comment!'));
                        }

                        return $this->redirect('publisso_gold.workflow.item', ['wf_id' => $wf_id]);
                }
        
                /**
                 * @param $id
                 */
                private function loadFromDB($id){

                        $res = \Drupal::database()->select('rwPubgoldComment', 't')->fields('t', [])->condition('id', $id)->execute()->fetchAssoc();
        
                        $ary = [];

                        if($res) {
                                foreach ($res as $k => $v) {
                                        if ($k != 'id') $ary[$k] = $v;
                                        else $this->id = $v;
                                }
                        }
                        $this->elements = $ary;
                }
        
                /**
                 * @param $id
                 * @return static|null
                 */
                public static function load($id){
                        
                        $res = \Drupal::database()->select('rwPubgoldComment', 't')->fields('t', [])->condition('id', $id)->countQuery()->execute()->fetchField();
                        return $res ? new static($id) : null;
                }
                
                public static function new(int $wf_id, $schema_state_identifier, string $text, int $creator_uid = null, int $receiver_uid = null, string $creator_role = null, string $receiver_role = null){
                        
                        if(!$creator_uid) $creator_uid = \Drupal::service('session')->get('user')['id'];
                        if(!$creator_uid) return null;
                        
                        $id = \Drupal::database()->insert('rwPubgoldComment')->fields([
                                'wf_id'         => $wf_id,
                                'wf_schema_id'  => $schema_state_identifier,
                                'body'          => $text,
                                'creator_uid'   => $creator_uid,
                                'receiver_uid'  => $receiver_uid,
                                'creator_role'  => $creator_role,
                                'receiver_role' => $receiver_role
                        ])->execute();
                        
                        return $id ? new static($id) : null;
                }
        
                /**
                 * @param $wf_id
                 * @return array
                 */
                public static function loadByWorkflowItem($wf_id){
                        
                        $res = [];
                        foreach(\Drupal::database()->select('rwPubgoldComment', 't')->fields('t', ['id'])->condition('wf_id', $wf_id, "=")->orderBy('created', 'ASC')->execute()->fetchCol() as $_){
                                $res[] = new static($_);
                        }
                        return $res;
                }
        
                /**
                 * @return mixed
                 */
                public function getElementKeys(){ return $this->keys; }
        
                /**
                 * @return array
                 */
                public function getKeys(){
                        return array_keys($this->elements);
                }
        
                /**
                 * @param $key
                 * @return mixed|null
                 */
                public function getElement($key){
                        if($key == 'id') return $this->id;
                        return $this->elements[$key] ?? null;
                }
        
                /**
                 * @param $uid
                 * @param $role
                 * @return false|mixed|string
                 */
                public function userHasAccess($uid, $role){
                        $role = trim($role);
                        $receiver_role = $this->getElement('receiver_role');
                        $receiver = $this->getElement('receiver_uid');
                        
                        if($uid == $this->getElement('creator_uid')) return 'own';
        
                        if($receiver){
                                if(!$receiver_role) {
                                        if ( self::ROLES[ $role ] != $receiver ) {
                                                if ( self::ROLES[ strtolower( \Drupal::service( 'session' )->get( 'user' )[ 'role' ] ) ] >= 60 )
                                                        return 'overridden';
                                                else
                                                        return false;
                                        };
                                }
                                else{
                                        
                                        $userweight = self::ROLES[ $role ];
                                        $commentweight = self::ROLES[ $receiver_role ];
                                        
                                        if($userweight == $commentweight){
                                                return 'regular';
                                        }
                                        
                                        if(self::ROLES[ strtolower(\Drupal::service('session')->get('user')['role']) ] >= 60)
                                                return 'overridden';
                                        else
                                                return false;

                                }
                        }
                        
                        if($receiver_role) {
                                
                                $receiver_role = array_filter(array_map('trim', explode(',', $receiver_role)));
                                
                                $return = false;
                                $roles = self::ROLES;
                                $userweight = $roles[ $role ];
                                
                                foreach ($receiver_role as $_){
                                        
                                        if($return === false) {
                                                
                                                $commentweight = self::ROLES[ $_ ];
        
                                                if ( $userweight > $commentweight ) {
                                                        $return = 'elevated';
                                                }
                                                elseif ( $userweight == $commentweight ) {
                                                        $return = 'regular';
                                                }
                                                if ( $userweight < $commentweight ) {
                                                        if ( in_array(strtolower( \Drupal::service( 'session' )->get( 'user' )[ 'role' ]), self::ROLES) && self::ROLES[ strtolower( \Drupal::service( 'session' )->get( 'user' )[ 'role' ] ) ] >= 60 )
                                                                $return = 'overridden';
                                                };
                                        }
                                }
                                
                                return $return;
                        }
                        
                        if(preg_match('/_author|reviewer$/', $role)){
                                if($receiver_role != $role) return false;
                        }
                        
                        return 'regular';
                }
        }
