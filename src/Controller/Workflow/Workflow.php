<?php
        /**
         * Created by PhpStorm.
         * User: sseliger
         * Date: 11.02.19
         * Time: 10:20
         */

        namespace Drupal\publisso_gold\Controller\Workflow;

        use Drupal\Core\Database\StatementInterface;
        use Drupal\Core\Form\FormState;
        use Drupal\Core\Link;
        use Drupal\publisso_gold\Controller\Blob;
        use Drupal\publisso_gold\Controller\Bookchapter;
        use \Drupal\publisso_gold\Controller\Interfaces\WorkflowInterface;
        use Drupal\publisso_gold\Controller\Issue;
        use Drupal\publisso_gold\Controller\Journalarticle;
        use Drupal\publisso_gold\Controller\Manager\BlobManager;
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\JournalManager;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\Reviewsheet;
        use Drupal\publisso_gold\Controller\Volume;
        use \Drupal\publisso_gold\Controller\Template;
        use Drupal\publisso_gold\Controller\User;
        use Drupal\Core\Controller\ControllerBase;
        use \Drupal\Component\Render\FormattableMarkup;
        use Drupal\Core\Url;

        /**
         * Class Workflow
         * @package Drupal\publisso_gold\Controller\Workflow
         */
        class Workflow extends ControllerBase implements WorkflowInterface {

                public    $medium;
                protected $id;
                protected $tbl_column_prefix;
                protected $elements;
                protected $data;
                protected $reviewers;
                protected $editors_in_chief;
                protected $editors;
                protected $editorial_office;
                protected $history;
                protected $db_table;
                protected $reviewpages;
                protected $session;
                protected $stateChain;
                protected $type;
                protected $schema;
                protected $ts;

                /**
                 * Workflow constructor.
                 * @param null $id
                 * @param string $tbl_column_prefix
                 * @param bool $loadMedium
                 */
                public function __construct($id = null, $tbl_column_prefix = 'wf_', $loadMedium = true) {
                        return false;
                }

                protected function load() {

                        if ($this->id) {

                                $book = \Drupal::database()->select($this->db_table, 'b')->fields('b', [])->condition($this->tbl_column_prefix .
                                                                                                                      'id', $this->id, '=')->execute()->fetchAssoc();

                                foreach ($book as $k => $v) {

                                        if ($k != 'wf_id') //store except id
                                                $this->elements[str_replace($this->tbl_column_prefix, '', $k)] = $v;
                                }

                                $this->data = json_decode(base64_decode($this->getElement('data')));
                                $this->setReviewers();
                                $this->setEditorsInChief();
                                $this->setEditors();
                                $this->setEditorialOffice();
                                $this->loadReviewpages();
                                $this->loadHistory();

                                if($this->getElement('schema')) $this->schema = unserialize($this->getElement('schema'));
                                if($this->getElement('reinit_schema')) $this->reinitSchema();
                        }
                }

                /**
                 * @param $elemName
                 * @return mixed
                 */
                public
                function getElement($elemName) {
                        if ($elemName == 'id') return $this->id;
                        return $this->elements[$elemName];
                }

                public
                function setReviewers() {

                        $this->reviewers = [];

                        foreach (array_filter(explode(',', $this->getElement('assigned_to_reviewer'))) as $uid) {
                                $this->reviewers[] = new User($uid);
                        }
                }

                public
                function setEditorsInChief() {

                        $this->editors_in_chief = [];

                        foreach (array_filter(explode(',', $this->getElement('assigned_to_eic'))) as $uid) {
                                $this->editors_in_chief[] = new User($uid);
                        }
                }

                public
                function setEditors() {

                        $this->editors = [];

                        foreach (array_filter(explode(',', $this->getElement('assigned_to_editor'))) as $uid) {
                                $this->editors[] = new User($uid);
                        }
                }

                public function setEditorialOffice() {

                        $this->editorial_office = [];

                        foreach (array_filter(explode(',', $this->getElement('assigned_to_eo'))) as $uid) {
                                $this->editorial_office[] = new User($uid);
                        }
                }

                protected function loadReviewpages() {

                        $this->review_pages = [];

                        $pages = json_decode(base64_decode($this->getDataElement('review_pages')), true);

                        if (is_array($pages)) {

                                foreach ($pages as $uid => $reviewpages) {

                                        $this->reviewpages[$uid] = [];

                                        foreach ($reviewpages as $reviewpage) {
                                                $this->review_pages[$uid][] = json_decode(base64_decode($reviewpage), true);
                                        }
                                }
                        }
                }

                /**
                 * @param $elemName
                 * @return false|string|null
                 */
                public function getDataElement($elemName) {

                        if ($elemName == ':orig_text') { //like in CSS ":" identicate pseudo-element

                                switch (substr($this->getElement('type'), 0, 1)) {
                                        case 'b':
                                                return base64_decode($this->getDataElement('chapter_text'));
                                                break;

                                        case 'j':
                                                return base64_decode($this->getDataElement('article_text'));
                                                break;

                                        case 'c':
                                                return base64_decode($this->getDataElement('paper_text'));
                                                break;
                                }
                        }

                        return $this->data->$elemName ?? null;
                }

                /**
                 * @return |null
                 */
                protected function loadHistory() {

                        if (!$this->id) return null;

                        $this->history = [];

                        foreach (\Drupal::database()->select('rwPubgoldWorkflowHistory', 't')->fields('t', ['wfh_id'])->condition('wfh_wfid', $this->id, '=')->execute()->fetchAll() as $_) {
                                $this->history[] = $_->wfh_id;
                        }
                }

                /**
                 * @return string
                 */
                public function __toString() {
                        return "class workflow";
                }

                /**
                 * @param null $creator_uid
                 * @param array $fields
                 * @return StatementInterface|int|string|null
                 * @throws \Exception
                 */
                public function __create($creator_uid = null, array $fields = []) {

                        if (!$creator_uid) $creator_uid = $this->session->get('user')['id'];

                        $fields['created_by_uid'] = $creator_uid;
                        $fields['type'] = $this->type;
                        $fields['assigned_to'] = 'u:'.\Drupal::service('session')->get('user')['id'];
                        $fields['data'] = base64_encode(json_encode([]));

                        foreach ($fields as $k => $v) {
                                $fields[$this->tbl_column_prefix . $k] = $v;
                                unset($fields[$k]);
                        }

                        $this->id = \Drupal::database()->insert($this->db_table)->fields($fields)->execute();

                        $this->load($this->id);
                        return $this->id;
                }

                /**
                 * @param null $uid
                 * @return array|mixed
                 */
                public function getReviewpages($uid = null) {

                        if ($uid === null) return $this->review_pages;
                        elseif (array_key_exists($uid, $this->review_pages)) return $this->review_pages[$uid];
                        else return [];
                }

                /**
                 * @param null $uid
                 * @return bool
                 */
                public function getLock($uid = null) {

                        if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];

                        if ($this->id && $uid) {

                                //try to lock data
                                $sql = "
                update
                                {" . $this->db_table .
                                       "}
                set
                        wf_locked = case when wf_locked_by_uid is null then 1 else wf_locked end,
                        wf_locked_by_uid = case when wf_locked_by_uid is null then :uid else wf_locked_by_uid end
                where
                        wf_id = :wf_id
                ";

                                $data = [
                                        ':wf_id' => $this->getElement('id'),
                                        ':uid' => $uid
                                ];

                                \Drupal::database()->query($sql, $data);
                                $this->load();

                                return $this->getElement('locked_by_uid') == $uid;
                        } else {
                                return false;
                        }
                }

                /**
                 * @return bool
                 */
                public function locked() {
                        return !!$this->getElement('locked');
                }

                /**
                 * @param false $force
                 * @return bool
                 * @throws \Exception
                 */
                public function unlock($force = false) {

                        if ($this->getElement('locked_by_uid') != $this->session->get('user')['id'] && $force === false) {
                                return false;
                        }

                        $this->setElement('locked', null);
                        $this->setElement('locked_by_uid', null);
                        return true;
                }

                /**
                 * @param $elemName
                 * @param null $elemValue
                 * @param bool $storeOnDB
                 * @param false $selfcall
                 * @return bool
                 * @throws \Exception
                 */
                public function setElement($elemName, $elemValue = null, $storeOnDB = true, $selfcall = false) {

                        if (array_key_exists($elemName, $this->elements)) {

                                if($elemName == 'schema_identifier' && !$selfcall){
                                        $this->newSchemaState($elemValue);
                                        $this->elements[$elemName] = $elemValue;
                                        $this->save();
                                        return true;
                                }

                                $oldValue = $this->elements[$elemName];
                                $this->elements[$elemName] = $elemValue;

                                if (!$selfcall) {
                                        if ($elemName == 'state') {

                                                $this->newStateChainElement($elemValue);
                                                $this->setDataElement($elemName, $elemValue, $storeOnDB, true); //um die Abwärtskompatibilität zu wahren wird der Status noch bei den Daten gespeichert
                                        }
                                }

                                if ($storeOnDB === false)
                                        return true;

                                if ($this->save($elemName) === false) {
                                        $this->elements[$elemName] = $oldValue;
                                        return false;
                                } else {
                                        return true;
                                }
                        } else {
                                return false;
                        }
                }

                /**
                 * @param $schemaID
                 * @return bool
                 */
                public function newSchemaState($schemaID){

                        $currentIdentifier = $this->getSchemaCurrentIdentifier();
                        $nextIdentifier = $schemaID;

                        $itemCurrent = $this->getSchemaItemByIdentifierAndFollower($currentIdentifier, $nextIdentifier);
                        $mailtemplates = array_filter(array_map('trim', explode(',', $itemCurrent['schema_mailtemplate_leave'])) ?? []);

                        $newStatus = $this->getSchemaItemsByIdentifier($nextIdentifier)[0]['schema_status'];

                        if(!$newStatus){
                                \Drupal::service('messenger')->addError('ERROR: Can\'t determine new status identifier!');
                                return false;
                        }

                        $nextRole = trim($itemCurrent['schema_followerrole']);
                        $assignment = $this->getAssignmentByRole($nextRole);

                        if(empty($assignment)){

                                $nextRoleReplacement = $itemCurrent['schema_followerrole_replacement'];
                                $nextRoleReplacement = explode(',', $nextRoleReplacement);
                                $nextRoleReplacement = array_map('trim', $nextRoleReplacement);
                                $nextRoleReplacement = array_filter($nextRoleReplacement ?? []);

                                foreach($nextRoleReplacement as $_){
                                        $assignment = $this->getAssignmentByRole($_);
                                        if(!empty($assignment)) break;
                                }
                        }

                        if(empty($assignment) && !($this->getElement('state') == 'in submission' || empty
                                        ($this->getElement('state')))){
                                \Drupal::service('messenger')->addError((string)$this->t('Cant assign any users to next step!'));
                                return false;
                        }

                        $run = unserialize($this->elements['run']);

                        $run[date('Y-m-d H:i:s')] = [
                                'old_identifier' => $this->getSchemaCurrentRun()['new_identifier'] ?? null,
                                'new_identifier' => $schemaID,
                                'initiator_uid' => \Drupal::service('session')->get('user')['id']
                        ];

                        $this->setElement('run', serialize($run));

                        foreach($mailtemplates as $id){
                                $mail = new WorkflowInfoMail($this, $id);
                                if($mail) $mail->send();
                        }


                        $this->setElement('state', $newStatus);
                        $this->setElement('assigned_to', $assignment);
                        $this->setElement('schema_identifier', $schemaID, true, true);
                        return true;
                }

                /**
                 * @param $role
                 * @return string
                 */
                protected function getAssignmentByRole($role){

                        switch($role){

                                case 'r:2':
                                        return $role;
                                        break;

                                case 'g:editorial_office':
                                        $eo = explode(',', $this->getElement('assigned_to_eo'));
                                        $eo = array_map('trim', $eo);
                                        $eo = array_filter($eo ?? []);
                                        $eo = implode(',u:', $eo);
                                        if($eo) $eo = "u:$eo";
                                        return $eo ?? '';
                                        break;

                                case 'g:conference_manager':
                                        $cm = explode(',', $this->getElement('assigned_to_cm'));
                                        $cm = array_map('trim', $cm);
                                        $cm = array_filter($cm ?? []);
                                        $cm = implode(',u:', $cm);
                                        if($cm) $cm = "u:$cm";
                                        return $cm ?? '';
                                        break;

                                case 'g:editors_in_chief':
                                        $eo = explode(',', $this->getElement('assigned_to_eic'));
                                        $eo = array_map('trim', $eo);
                                        $eo = array_filter($eo ?? []);
                                        $eo = implode(',u:', $eo);
                                        if($eo) $eo = "u:$eo";
                                        return $eo ?? '';
                                        break;

                                case 'g:editors':
                                        $eo = explode(',', $this->getElement('assigned_to_editor'));
                                        $eo = array_map('trim', $eo);
                                        $eo = array_filter($eo ?? []);
                                        $eo = implode(',u:', $eo);
                                        if($eo) $eo = "u:$eo";
                                        return $eo ?? '';
                                        break;

                                case 'u:submitting_author':
                                        return 'u:'.$this->getElement('created_by_uid');
                                        break;

                                case 'u:corresponding_author':
                                        $eo = explode(',', $this->getElement('corresponding_authors'));
                                        $eo = array_map('trim', $eo);
                                        $eo = array_filter($eo ?? []);
                                        $eo = implode(',u:', $eo);
                                        if($eo) $eo = "u:$eo";
                                        return $eo ?? '';
                                        break;

                                case 'u:reviewer':
                                        $eo = explode(',', $this->getElement('assigned_to_reviewer'));
                                        $eo = array_map('trim', $eo);
                                        $eo = array_filter($eo ?? []);
                                        $eo = implode(',u:', $eo);
                                        if($eo) $eo = "u:$eo";
                                        return $eo ?? '';
                                        break;
                        }
                }

                /**
                 * @return array|mixed
                 */
                protected function getSchemaCurrentRun(){

                        if(false !== ($run = unserialize($this->elements['run']))) {

                                if ( is_array( $run ) ) {
                                        uksort( $run, [ 'self', 'orderSchemaRun' ] );
                                }

                                return end( $run );
                        }

                        return [];
                }

                /**
                 * @return mixed
                 */
                public function getSchemaCurrentIdentifier(){
                        return $this->getElement('schema_identifier');
                }

                /**
                 * @param $a
                 * @param $b
                 * @return int
                 */
                protected function orderSchemaRun($a, $b){
                        $a = strtotime($a);
                        $b = strtotime($b);
                        return $a <=> $b;
                }

                /**
                 * @param string $state_new
                 * @param string|null $state_old
                 * @throws \Exception
                 */
                public function newStateChainElement(string $state_new, string $state_old = null) {

                        if (\Drupal::service('session')->get('logged_in') === true) {

                                $uid = \Drupal::service('session')->get('user')['id'];

                                if (!$state_old && !$this->getCurrentState())
                                        $state_old = 'in submission';

                                if ($state_new != $state_old) {
                                        $data = [
                                                'uid' => $uid,
                                                'state_old' => $state_old,
                                                'state_new' => $state_new,
                                                'wf_id' => $this->id
                                        ];

                                        \Drupal::database()->insert('rwPubgoldWorkflowStateChain')->fields($data)->execute();
                                }
                        }
                }

                /**
                 * @return |null
                 */
                public function getCurrentState() {

                        if ($this->stateChain === null) {
                                $this->loadStateChain();
                        }

                        return end($this->stateChain)->state_new ?? null;
                }

                protected function loadStateChain() {

                        $this->stateChain = [];

                        $qry = \Drupal::database()->select('rwPubgoldWorkflowStateChain', 't')->fields('t', [])->condition
                        ('wf_id', $this->id, "=");

                        if ($qry->countQuery()->execute()->fetchField()) {
                                $this->stateChain = $qry->execute()->fetchAll();
                        }
                }

                /**
                 * @param $elemName
                 * @param $elemValue
                 * @param bool $storeOnDB
                 * @param false $selfcall
                 * @throws \Exception
                 */
                public function setDataElement($elemName, $elemValue, $storeOnDB = true, $selfcall = false) {

                        if(!is_object($this->data)) $this->data = (object)[];

                        $oldValue = $this->data->$elemName ?? null;
                        $this->data->$elemName = $elemValue;

                        if (!$selfcall) {
                                if ($elemName == 'state') {
                                        $this->setElement($elemName, $elemValue, $storeOnDB, true);

                                        if ($oldValue != $elemName) {
                                                $fields = [
                                                        'uid' => \Drupal::service('session')->get('user')['id'],
                                                        'wf_id' => $this->getElement('id'),
                                                        'state_old' => $oldValue,
                                                        'state_new' => $elemValue
                                                ];

                                                \Drupal::database()->insert('rwPubgoldWorkflowStateChain')->fields($fields)->execute();
                                        }
                                }
                        }

                        $this->setElement('data', base64_encode(json_encode($this->data)), $storeOnDB);
                }

                /**
                 * @param null $elemName
                 * @return StatementInterface|int|string|null
                 */
                public function save($elemName = null) {

                        if ($this->id) {
                                $uid = null;
                                if (\Drupal::service('session')->has('user')) $this->elements['uid_last_modified'] = \Drupal::service('session')->get('user')['id'];

                                $fields = $this->elements;

                                if ($elemName !== null) { //if specific element selected
                                        $fields = [$elemName => $fields[$elemName]];
                                }

                                foreach ($fields as $k => $var) {
                                        $fields[$this->tbl_column_prefix . $k] = $var;
                                        unset($fields[$k]);
                                }

                                return \Drupal::database()->update($this->db_table)->fields($fields)->condition($this->tbl_column_prefix .
                                                                                                                 'id', $this->id)->execute();
                        }
                }

                /**
                 * @return array
                 */
                public function getKeys() {
                        return array_keys((array)$this->elements);
                }

                /**
                 * @param $uid
                 * @return bool
                 */
                public function isEditorInChief($uid) {

                        foreach ($this->editors_in_chief as $user) {
                                if ($user->getElement('id') == $uid) return true;
                        }

                        return false;
                }

                /**
                 * @param $uid
                 * @return bool
                 */
                public function isEditorialOffice($uid) {

                        foreach ($this->editorial_office as $user) {
                                if ($user->getElement('id') == $uid) return true;
                        }

                        return false;
                }

                /**
                 * @param $uid
                 * @return bool
                 */
                public function isEditor($uid) {

                        foreach ($this->editors as $user) {
                                if ($user->getElement('id') == $uid) return true;
                        }

                        return false;
                }

                public function reload() {
                        $this->load(); $this->loadMedium();
                }

                protected function loadMedium() {

                        if (!$this->medium) $this->medium = $this->getParentMedium();
                }

                /**
                 * @return \Drupal\publisso_gold\Controller\Medium\Book|\Drupal\publisso_gold\Controller\Medium\Conference|\Drupal\publisso_gold\Controller\Medium\Journal|null
                 */
                public function getParentMedium() {

                        switch (substr($this->type, 0, 1)) {

                                case 'b':
                                        return BookManager::getBook($this->getDataElement('bk_id'), true);
                                        break;

                                case 'j':
                                        return JournalManager::getJournal($this->getDataElement('jrn_id'), true);
                                        break;

                                case 'c':
                                        return ConferenceManager::getConference($this->getDataElement('cf_id'));
                                        break;
                        }
                }

                /**
                 * @return array
                 */
                public function readAllReviewerUIDs() {

                        $uids = $this->readCurrentReviewerUIDs();

                        foreach (\Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', ['uid'])->condition('wfid', $this->id, '=')->execute()->fetchCol() as $_) {
                                $uids[] = $_;
                        }

                        return array_map('trim', array_unique(array_filter($uids)));
                }

                public function readCurrentReviewerUIDs() :array{

                        $uids = [];

                        foreach (explode(',', $this->getElement('assigned_to_reviewer')) as $_) {
                                $uids[] = $_;
                        }

                        return array_map('trim', array_unique(array_filter($uids)));
                }

                public function countCurrentReviewers() :int{
                        return count($this->readCurrentReviewerUIDs());
                }

                /**
                 * @return StatementInterface|int|string|null
                 * @throws \Exception
                 */
                public function historize() {

                        if (!$this->id) return null;

                        $tbl_column_prefix = 'wfh_';
                        $fields = [];

                        foreach ($this->elements as $k => $v) {
                                $fields[$tbl_column_prefix . $k] = $v;
                        }

                        $fields[$tbl_column_prefix .
                                'wfid'] = $this->id;

                        $wfh_id = \Drupal::database()->insert('rwPubgoldWorkflowHistory')->fields($fields)->execute();
                        $this->loadHistory();
                        return $wfh_id;
                }

                /**
                 * @param int $id
                 * @return Workflow
                 */
                public function History(int $id) {
                        return new Workflow($id, 'wfh_');
                }

                /**
                 * @return mixed
                 */
                public function getHistory() {
                        return $this->history;
                }

                /**
                 * @return mixed
                 */
                public function getMedium() {
                        if (!$this->medium) $this->loadMedium();
                        return $this->medium;
                }

                /**
                 * @return bool
                 */
                public function newStateEditorchangerequest() {

                        $this->setDataElement('state', 'editor change request');
                        $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('created_by_uid')))));
                        $this->load();

                        if ($this->getElement('type') == 'bcs') {

                                $mail = new WorkflowInfoMail($this, 'editor change request');
                                if ($mail) $mail->send();
                                //sendWorkflowInfoMail($this, 'editor change request');
                        } elseif ($this->getElement('type') == 'bce') {

                                $mail = new WorkflowInfoMail($this, 'erratum editor change request');
                                if ($mail) $mail->send();
                                //sendWorkflowInfoMail($this, 'erratum editor change request');
                        }

                        return true;
                }

                /**
                 * @return bool
                 */
                public function newStateClearedforpublication() {

                        $this->setDataElement('state', 'cleared for publication');

                        //if workflow assigned to eo
                        if (!empty($this->getElement('assigned_to_eo'))) {
                                $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('assigned_to_eo')))));
                        } //redirect to book-eo
                        else {
                                $eo = $this->medium->readEditorialOffice();
                                $ary = [];

                                foreach ($eo as $user) {
                                        $ary[] = $user->getElement('id');
                                }

                                $this->setElement('assigned_to', 'u:' . implode(',u:', $ary));
                        }


                        $this->load();
                        if ($this->getElement('type') == 'bcs') {

                                $mail = new WorkflowInfoMail($this, 'cleared for publication');
                                if ($mail) $mail->send();
                        } elseif ($this->getElement('type') == 'bce') {

                                $mail = new WorkflowInfoMail($this, 'erratum cleared for publication');
                                if ($mail) $mail->send();
                        }
                        return true;
                }

                /**
                 * @return Bookchapter|Journalarticle|Conferencepaper
                 */
                public function getPublishedEntity(){

                        switch(substr($this->getElement('type'), 0, 1)){
                                case 'b':
                                        $entity = new Bookchapter($this->getDataElement('cp_id'));
                                        break;

                                case 'j':
                                        $entity = new Journalarticle($this->getDataElement('jrna_id'));
                                        break;

                                case 'c':
                                        $entity = new Conferencepaper($this->getDataElement('cp_id'));
                                        break;
                        }

                        return $entity;
                }

                /**
                 * @return bool
                 */
                public function newStateReadyforpublication() {

                        $this->setDataElement('state', 'ready for publication');
                        $this->setElement('assigned_to', 'r:2');

                        $mail = new WorkflowInfoMail($this, 'ready for publication');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'ready for publication');
                        return true;
                }

                /**
                 * @param $recommendation
                 * @return |null
                 * @throws \Exception
                 */
                public function setRecommendation($recommendation) {

                        if (!$this->id) return null;

                        \Drupal::database()->insert('rwPubgoldWorkflowRecommendations')->fields([
                                                                                                       'created_by_uid' => $this->session->get('user')['id'],
                                                                                                       'recommendation' => $recommendation,
                                                                                                       'wf_id' => $this->id
                                                                                               ])->execute();
                }

                /**
                 * @param null $src
                 * @return false
                 */
                public function forward($src = null) {

                        $dest = [
                                'accepted' => 'author clearing request',
                                'editor change request' => 'editor clearing request',
                                'author change request' => 'author clearing request'
                        ];

                        $currState = $src ? $src : $this->getDataElement('state');

                        if (array_key_exists($currState, $dest)) {
                                return $this->setState($dest[$currState]);
                        }
                }

                /**
                 * @param null $state
                 * @param array $args
                 * @return false
                 */
                public function setState($state = null, $args = []) {

                        if (!$this->medium) $this->loadMedium();
                        $method = 'newState' . ucfirst(strtolower(str_replace(' ', '', $state)));

                        if (!method_exists($this, $method))
                                return false;

                        return $this->$method($args);
                }

                /**
                 * @param $data
                 */
                public function makeTemporary($data) {

                        $this->elements['type'] = $data['type'];

                        //abstract
                        $element = 'abstract';
                        $$element = $data[$element];
                        $this->setDataElement($element, base64_encode($$element), false);

                        //chapter_text
                        $element = 'chapter_text';
                        $$element = $data[$element];
                        $this->setDataElement($element, base64_encode($$element), false);

                        //keywords
                        $keywords = array_filter(array_map('trim', explode(';', $data['keywords'])));
                        $this->setDataElement('keywords', json_encode($keywords), false);

                        //corporations
                        $this->setDataElement('corporations', json_encode(array_map('trim', $data['corporation'])), false);

                        //falls vorhanden - erratum
                        if (array_key_exists('erratum', $data))
                                $this->setDataElement('errata', $data['erratum'], false);

                        $values = [
                                'bk_id', 'data_type', 'title', 'conflict_of_interest', 'conflict_of_interest_text', 'funding', 'funding_name', 'funding_id', 'reviewer_suggestion', 'accept_policy',
                                'references', 'doi', 'license', 'version', 'prvious_version', 'publication_place', 'publication_year'
                        ];

                        foreach ($values as $_) {
                                if (array_key_exists($_, $data)) $this->setDataElement($_, $data[$_]);
                        }

                        //authors
                        $authors = [];

                        foreach ($data['author_db'] as $item) {

                                $author = $item['author'];

                                if (array_key_exists('raw', $author)) { //kommt beim Setzen des Erratums vor

                                        if (!($author['raw']['data']['delete'])) {

                                                $fname = $author['raw']['data']['fields']['firstname'];
                                                $lname = $author['raw']['data']['fields']['lastname'];
                                                $affiliation = $author['raw']['data']['fields']['affiliation'];
                                                $name = implode(' ', [$fname, $lname]);
                                                $author = implode('; ', [$name, $affiliation]);

                                        }
                                }

                                $is_corresponding = $item['is-corresponding'];
                                $weight = $item['weight'];

                                [$author, $author_data] = preg_split('/;\s{1,}/', $author, 2);

                                if (!empty($author)) {
                                        $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                        $found = 0;
                                        $last_id = null;
                                        $string = '';

                                        foreach ($res as $v) {
                                                $string .= ' || ' . implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, \Drupal::service('publisso_gold.tools')->getCountry($v->up_country)])) .
                                                           ' | ' . implode('; ', [$author, $author_data]);
                                                if (
                                                        implode('; ', [$author, $author_data]) ==
                                                        implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, \Drupal::service('publisso_gold.tools')->getCountry($v->up_country)]))) {
                                                        $found++;
                                                        $last_id = $v->up_uid;
                                                }
                                        }

                                        if ($found == 1) {

                                                $user = new User($last_id);

                                                $authors[] = [
                                                        'weight' => $weight,
                                                        'is_corresponding' => $is_corresponding,
                                                        'firstname' => $user->profile->getElement('firstname'),
                                                        'lastname' => $user->profile->getElement('lastname'),
                                                        'affiliation' => $user->profile->getElement('institute'),
                                                        'uid' => $user->getElement('id'),
                                                        'profile_archive_id' => $user->profile->makeSnapshot(\Drupal::service('session')->get('user')['id'])
                                                ];
                                        }
                                }
                        }

                        foreach ($data['author_add'] as $item) {
                                $authors[] = array_map('trim', $item);
                        }

                        $this->setDataElement('authors', json_encode($authors), false);
                        $this->elements['locked_by_uid'] = \Drupal::service('session')->get('user')['id'];
                        if (!$this->medium) $this->loadMedium();
                }

                /**
                 * @param null $uid
                 * @param string $withState
                 * @throws \Exception
                 */
                public function finishReview($uid = null, $withState = 'finished') {

                        if (!$uid) $uid = Publisso::currentUser()->getId();
                        $id = $this->getReviewsheet($uid, 'id');

                        $qry = \Drupal::database()->update('rwPubgoldReviewerReviewsheet')->fields([
                                'state' => $withState
                        ])->expression('modified', 'NOW()')->condition('id', $id, "=")->execute();

                        $this->removeReviewer($uid);


                        if (!count($this->readReviewers())) {

                                $newIdentifier = $this->getSchemaDefaultFollower('schema_follower');

                                if(!WorkflowManager::countFinishedReviews($this->id) && $this->countCurrentReviewers() < 1){
                                        $newIdentifier = $this->getSchemaPreviousIdentifier();
                                }

                                $this->newSchemaState($newIdentifier);
                        }
                }

                /**
                 * @param null $uid
                 * @param string $field
                 * @return mixed
                 * @throws \Exception
                 */
                public function getReviewsheet($uid = null, $field = 'reviewsheet') {

                        if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];
                        if (!$this->medium) $this->loadMedium();

                        $qry = \Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', []);
                        $cnd = $qry->andConditionGroup();
                        $cnd1 = $qry->orConditionGroup();

                        $cnd->condition('wfid', $this->id, '=');
                        $cnd->condition('uid', $uid, '=');

                        $cnd1->condition('state', 'progress', '=')->condition('state', 'pending', '=');
                        $cnd->condition($cnd1);
                        $qry->condition($cnd);

                        if (!$qry->countQuery()->execute()->fetchField()) {

                                $rp_id = $this->medium->getElement('rpid');
                                $rp = new Reviewsheet($rp_id);

                                \Drupal::database()->insert('rwPubgoldReviewerReviewsheet')->fields([
                                                                                                           'uid' => $uid,
                                                                                                           'reviewsheet' => $rp->getElement('sheet'),
                                                                                                           'wfid' => $this->id
                                                                                                   ])->execute();
                        }

                        $res = $qry->execute()->fetchAll();
                        return $res[0]->$field;
                }

                /**
                 * @param array $uids
                 */
                public function removeReviewer($uids = []) {

                        if ($this->id) {

                                if (!is_array($uids))
                                        $uids = [$uids];

                                foreach ($uids as $uid) {

                                        \Drupal::database()->query("
                                UPDATE {" . $this->db_table .
                                                                  "}
                                        SET
                                        `" . $this->tbl_column_prefix .
                                                                  "assigned_to_reviewer` =
                                                TRIM(BOTH ',' FROM
                                                REPLACE(
                                                        REPLACE(CONCAT(',',REPLACE(`" . $this->tbl_column_prefix .
                                                                  "assigned_to_reviewer`, ',', ',,'), ','),'," . $uid .
                                                                  ",', ''), ',,', ',')
                                                )
                                        WHERE
                                        `" . $this->tbl_column_prefix .
                                                                  "id` = " . $this->getElement('id') .
                                                                  "
                                ");

                                        if ($this->getDataElement('state') == 'in review') {

                                                \Drupal::database()->query("
                                                UPDATE {" . $this->db_table .
                                                                          "}
                                                        SET
                                                        `" . $this->tbl_column_prefix .
                                                                          "assigned_to` =
                                                                TRIM(BOTH ',' FROM
                                                                REPLACE(
                                                                        REPLACE(CONCAT(',',REPLACE(`" . $this->tbl_column_prefix .
                                                                          "assigned_to`, ',', ',,'), ','),',u:" . $uid .
                                                                          ",', ''), ',,', ',')
                                                                )
                                                        WHERE
                                                        `" . $this->tbl_column_prefix .
                                                                          "id` = " . $this->getElement('id') .
                                                                          "
                                                ");
                                        }

                                        \Drupal::database()->delete('rwPubgoldReviewerReviewsheet')->condition('wfid', $this->id, '=')->condition('uid', $uid, '=')->condition('state', ['pending', 'progress'], 'IN')->execute();
                                }

                                $this->load();
                        }
                }

                /**
                 * @return array
                 */
                public function readReviewers() {
                        return array_filter($this->reviewers);
                }

                /**
                 * @param null $uid
                 */
                public function rejectReview($uid = null) {

                        if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];
                        $id = $this->getReviewsheet($uid, 'id');

                        $qry = \Drupal::database()->update('rwPubgoldReviewerReviewsheet')->fields([
                                                                                                           'state' => 'rejected'
                                                                                                   ])->expression('modified', 'NOW()')->condition('id', $id, "=")->execute();
                }

                /**
                 * @param $reviewsheet
                 * @param null $uid
                 */
                public function updateReview($reviewsheet, $uid = null) {

                        if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];
                        $id = $this->getReviewsheet($uid, 'id');
                        $qry = \Drupal::database()->update('rwPubgoldReviewerReviewsheet')->fields([
                                                                                                           'reviewsheet' => $reviewsheet
                                                                                                   ])->expression('modified', 'NOW()')->condition('id', $id, "=")->execute();
                }

                /**
                 * @param $state
                 * @param null $uid
                 */
                public function setReviewState($state, $uid = null) {

                        if (!$uid) $uid = \Drupal::service('session')->get('user')['id'];
                        $id = $this->getReviewsheet($uid, 'id');
                        $qry = \Drupal::database()->update('rwPubgoldReviewerReviewsheet')->fields([
                                                                                                           'state' => $state
                                                                                                   ])->expression('modified', 'NOW()')->condition('id', $id, "=")->execute();
                }

                /**
                 * @param $template
                 * @param array $args
                 * @return |null
                 */
                public function getRenderableTemplate($template, $args = []) {

                        $method = "getRenderableTemplate_" . strtoupper($template);

                        if (method_exists($this, $method))
                                return $this->$method($args);
                        else
                                return null;
                }

                /**
                 * @return bool
                 */
                public function hasCompletedReviewsheets() {

                        return !!\Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', [])->condition('wfid', $this->id, '=')->condition('state', 'finished', '=')->countQuery()->execute()->fetchField();
                }

                /**
                 * @return mixed
                 */
                public function getStateChain() {

                        if ($this->stateChain === null) {
                                $this->loadStateChain();
                        }

                        return $this->stateChain;
                }

                /**
                 * @param string|null $state
                 * @return bool
                 */
                public function hasStateInChain(string $state = null) {

                        if ($this->stateChain === null) {
                                $this->loadStateChain();
                        }

                        foreach ($this->stateChain as $_) {
                                if ($_->state_old == $state || $_->state_new == $state) {
                                        return true;
                                        break;
                                }
                        }

                        return false;
                }

                /**
                 * @return mixed
                 */
                public function getPreviousState() {

                        if ($this->stateChain === null) {
                                $this->loadStateChain();
                        }

                        return end($this->stateChain)->state_old;
                }

                /**
                 * @param string $elemName
                 * @return |null
                 */
                public function getStateChainElement(string $elemName) {

                        if ($this->stateChain === null) {
                                $this->loadStateChain();
                        }

                        foreach (array_reverse($this->stateChain) as $state) {
                                if ($state->state_new == $elemName) {
                                        return $state->time;
                                        break;
                                }
                        }

                        return null;
                }

                /**
                 * @return mixed|string
                 */
                public function getLastEditorRole() {

                        if ($this->stateChain === null) {
                                $this->loadStateChain();
                        }

                        $role = 'eo';

                        foreach (array_reverse($this->stateChain) as $_) {

                                $state = $_->state_old;

                                if (preg_match('/assigned to (eo|eic|editor)/', $state, $matches)) {
                                        $role = $matches[1];
                                        break;
                                }
                        }

                        return $role;
                }

                /**
                 * @param $args
                 * @return false
                 */
                protected function newStateAssignedtoeic($args) {

                        if (!is_array($args)) return false;

                        $this->setDataElement('state', 'assigned to eic');
                        $this->setElement('state', 'assigned to eic');
                        $this->setElement('assigned_to', 'u:' . implode(',u:', $args));
                        $this->setElement('assigned_to_eic', implode(',', $args));
                        $this->load();

                        $mail = new WorkflowInfoMail($this, 'assigned to eic');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'assigned to eic');
                }

                /**
                 * @param $args
                 * @return false
                 */
                protected function newStateAssignedtoeo($args) {

                        if (!is_array($args)) return false;

                        $this->setDataElement('state', 'assigned to eo');
                        $this->load();

                        $mail = new WorkflowInfoMail($this, 'assigned to eo');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'assigned to eic');
                }

                /**
                 * @param $args
                 * @return false
                 */
                protected function newStateAssignedtoeditor($args) {

                        if (!is_array($args)) return false;

                        $this->setDataElement('state', 'assigned to editor');
                        $this->setElement('assigned_to', 'u:' . implode(',u:', $args));
                        $this->setElement('assigned_to_editor', implode(',', $args));
                        $this->load();
                        $mail = new WorkflowInfoMail($this, 'assigned to editor');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'assigned to editor');
                }

                /**
                 * @return bool
                 */
                protected function newStateInrevision() {

                        switch ($this->getElement('state')) {

                                case 'submission finished':
                                case 'accepted':
                                        $this->setElement('revision_ordered_by', 'eo');
                                        break;

                                case 'assigned to eic':
                                case 'decision eic':
                                        $this->setElement('revision_ordered_by', 'eic');
                                        break;

                                case 'assigned to editor':
                                case 'review finished':
                                        $this->setElement('revision_ordered_by', 'editor');
                                        break;

                        }

                        $this->setDataElement('state', 'in revision', false);
                        $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('created_by_uid')))), false);

                        $this->setElement('editor_last_edited', \Drupal::service('session')->get('user')['id'], false);
                        $this->save();
                        $this->load();

                        $process = ($this->getElement('revision_ordered_by') != 'editor' ? 'editor ' : '') . 'in revision' . ($this->getElement('revision_ordered_by') != 'editor' && $this->getDataElement('revised') ? ' again ' : '');

                        $mail = new WorkflowInfoMail($this, $process);
                        if ($mail) $mail->send($this->getElement('revision_ordered_by') != 'editor');
                        //sendWorkflowInfoMail($this, 'in revision', [], false);
                        return true;
                }

                /**
                 * @return bool
                 */
                protected function newStateRevisionfinished() {

                        $this->setDataElement('state', 'revision finished');

                        if ($this->getElement('revision_ordered_by') == 'eo') {
                                $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('assigned_to_eo')))));
                        } else {
                                $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('editor_last_edited')))));
                        }

                        $this->load();

                        $mail = new WorkflowInfoMail($this, ($this->getElement('revision_ordered_by') != 'editor' ? 'eic ' : '') . 'revision finished');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'revision finished');
                        return true;
                }

                /**
                 * @param $args
                 * @return false
                 * @throws \Exception
                 */
                protected function newStateInreview($args) {

                        if (!is_array($args)) return false;

                        if (!$this->medium) $this->loadMedium();

                        $this->setDataElement('state', 'in review');
                        $this->setElement('state', 'in review');
                        $this->setElement('assigned_to', 'u:' . implode(',u:', $args));
                        $this->setElement('assigned_to_reviewer', implode(',', $args));
                        $this->setElement('reviewer_edited', '');
                        $this->load();
                        /*
                        if (!array_key_exists('revised', $this->getDataKeys())) {
                                $mail = new WorkflowInfoMail($this, 'in review');
                                if ($mail) $mail->send();
                                //sendWorkflowInfoMail($this, 'in review');
                        } else {
                                $mail = new WorkflowInfoMail($this, 'in review after revision');
                                if ($mail) $mail->send();
                                //sendWorkflowInfoMail($this, 'in review after revision');
                        }
                        */
                        if (!empty($this->medium->getElement('rpid'))) {
                                $rp_id = $this->medium->getElement('rpid');
                                $rp = new Reviewsheet($rp_id);

                                foreach ($args as $uid) {
                                        $this->newUserReview($uid);
                                }
                        }
                }

                /**
                 * @param int $uid
                 * @return bool
                 * @throws \Exception
                 */
                public function newUserReview(int $uid) {

                        if (!$this->medium) $this->loadMedium();

                        $qry = \Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', []);
                        $cnd = $qry->andConditionGroup();
                        $cnd1 = $qry->orConditionGroup();

                        $cnd->condition('wfid', $this->id, '=');
                        $cnd->condition('uid', $uid, '=');

                        $cnd1->condition('state', 'progress', '=')->condition('state', 'pending', '=');
                        $cnd->condition($cnd1);

                        $qry->condition($cnd);

                        if (!$qry->countQuery()->execute()->fetchField()) {

                                $this->addReviewer([$uid]);

                                $rp_id = $this->medium->getElement('rpid');
                                $rp = new Reviewsheet($rp_id);

                                \Drupal::database()->insert('rwPubgoldReviewerReviewsheet')->fields([
                                                                                                           'uid' => $uid,
                                                                                                           'reviewsheet' => $rp->getElement('sheet'),
                                                                                                           'wfid' => $this->id,
                                                                                                           'state' => 'pending'
                                                                                                   ])->execute();
                                /*
                                if (!array_key_exists('revised', $this->getDataKeys())) {
                                        $mail = new WorkflowInfoMail($this, 'in review');
                                        if ($mail) $mail->send();
                                        //sendWorkflowInfoMail($this, 'in review');
                                } else {
                                        $mail = new WorkflowInfoMail($this, 'in review after revision');
                                        if ($mail) $mail->send();
                                        //sendWorkflowInfoMail($this, 'in review after revision');
                                }
                                */
                                return true;
                        }

                        return false;
                }

                /**
                 * @param array $uids
                 */
                public function addReviewer($uids = []) {

                        if (!is_array($uids))
                                $uids = [$uids];

                        foreach ($uids as $uid) {

                                if (!$this->isReviewer($uid)) {

                                        $reviewers = array_filter(explode(',', $this->getElement('assigned_to_reviewer')));
                                        array_push($reviewers, $uid);

                                        $this->setElement('assigned_to_reviewer', implode(',', $reviewers));

                                        if ($this->getDataElement('state') == 'in review') {
                                                $reviewers = array_filter(explode(',', $this->getElement('assigned_to')));
                                                array_push($reviewers, "u:$uid");
                                                $this->setElement('assigned_to', implode(',', $reviewers));
                                        }
                                }
                        }

                        $this->load();
                }

                /**
                 * @param $uid
                 * @return bool
                 */
                public function isReviewer($uid) {

                        foreach ($this->reviewers as $user) {
                                if ($user->getElement('id') == $uid) return true;
                        }

                        return false;
                }

                /**
                 * @param null $uid
                 * @return bool
                 */
                public function userIsAssigned($uid = null){

                        if($uid === null) $uid = \Drupal::service('session')->get('user')['id'];

                        $user = new User($uid);

                        $assigned = $this->getElement('assigned_to');
                        $assigned = explode(',', $assigned);

                        $owner = false;

                        foreach($assigned as $_){

                                $_ = explode(':', $_);
                                if($_[0] == 'u' && $_[1] == $uid) $owner = true;
                                if($_[0] == 'r' && $user->getRoleElement('id') == $_[1]) $owner = true;
                        }

                        return $owner;
                }

                /**
                 * @return array
                 */
                public function getDataKeys() {
                        return array_keys((array)$this->data);
                }

                /**
                 * @param $elemName
                 * @return bool
                 */
                public function hasDataElement($elemName){
                        return in_array($elemName, $this->getDataKeys(), true);
                }

                /**
                 * @return bool
                 */
                protected function newStateDecisioneic() {
                        $this->setDataElement('state', 'decision eic');
                        $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('assigned_to_eic')))));
                        $this->load();

                        $mail = new WorkflowInfoMail($this, 'decision eic');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'decision eic');
                        return true;
                }

                /**
                 * @return bool
                 */
                protected function newStateRejected() {

                        $this->setDataElement('state', 'rejected');
                        $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('created_by_uid')))));
                        $this->load();

                        $mail = new WorkflowInfoMail($this, 'submission rejected');
                        if ($mail) $mail->send(true);
                        //sendWorkflowInfoMail($this, 'submission rejected', [], false);
                        return true;
                }

                /**
                 * @return bool
                 */
                protected function newStateAccepted() {

                        $this->setDataElement('state', 'accepted');

                        //if workflow assigned to eo
                        if (!empty($this->getElement('assigned_to_eo'))) {
                                $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('assigned_to_eo')))));
                        } //redirect to book-eo
                        else {
                                $eo = $this->medium->readEditorialOffice();
                                $ary = [];

                                foreach ($eo as $user) {
                                        $ary[] = $user->getElement('id');
                                }

                                $this->setElement('assigned_to', 'u:' . implode(',u:', $ary));
                        }


                        $this->load();

                        $mail = new WorkflowInfoMail($this, 'submission accepted');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'submission accepted');
                        return true;
                }

                /**
                 * @return bool
                 */
                protected function newStateAuthorclearingrequest() {

                        $this->setDataElement('state', 'author clearing request');
                        $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('created_by_uid')))));
                        $this->load();
                        if ($this->getElement('type') == 'bcs') {

                                $mail = new WorkflowInfoMail($this, 'author clearing request');
                                if ($mail) $mail->send(true);
                                //sendWorkflowInfoMail($this, 'author clearing request', [], false);
                        } elseif ($this->getElement('type') == 'bce') {

                                $mail = new WorkflowInfoMail($this, 'erratum author clearing request');
                                if ($mail) $mail->send(true);
                                //sendWorkflowInfoMail($this, 'erratum author clearing request', [], false);
                        }
                        return true;
                }

                /**
                 * @return bool
                 */
                protected function newStateEditorclearingrequest() {

                        $this->setDataElement('state', 'editor clearing request');
                        $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('assigned_to_eic')))));
                        $this->load();

                        $mail = new WorkflowInfoMail($this, 'editor clearing request');
                        if ($mail) $mail->send();
                        //sendWorkflowInfoMail($this, 'editor clearing request');
                        return true;
                }

                /**
                 * @return bool
                 */
                protected function newStateAuthorchangerequest() {

                        $this->setDataElement('state', 'author change request');

                        //if workflow assigned to eo
                        if (!empty($this->getElement('assigned_to_eo'))) {
                                $this->setElement('assigned_to', 'u:' . implode(',u:', array_filter(explode(',', $this->getElement('assigned_to_eo')))));
                        } //redirect to book-eo
                        else {
                                $eo = $this->medium->readEditorialOffice();
                                $ary = [];

                                foreach ($eo as $user) {
                                        $ary[] = $user->getElement('id');
                                }

                                $this->setElement('assigned_to', 'u:' . implode(',u:', $ary));
                        }


                        $this->load();
                        if ($this->getElement('type') == 'bcs') {

                                $mail = new WorkflowInfoMail($this, 'author change request');
                                if ($mail) $mail->send();
                                //sendWorkflowInfoMail($this, 'author change request', [], true);
                        } elseif ($this->getElement('type') == 'bce') {

                                $mail = new WorkflowInfoMail($this, 'erratum author change request');
                                if ($mail) $mail->send();
                                //sendWorkflowInfoMail($this, 'erratum author change request', [], true);
                        }
                        return true;
                }

                /**
                 * @param $args
                 * @return array|null
                 */
                protected function getRenderableTemplate_FILES($args) {

                        $files_tmpl = [];
                        $fids = $this->getDataElement('files');
                        if ($fids) $fids = unserialize($fids);
                        if(!is_array($fids)) $fids = [];

                        if(array_key_exists('hideonempty', $args)){
                                if($args['hideonempty'] && !count($fids)){
                                        return null;
                                }
                        }

                        if (is_array($fids)) {

                                foreach ($fids as $fid) {

                                        $blob = BlobManager::get($fid);

                                        $files_tmpl[] = [
                                                '#type' => 'fieldset',
                                                'content' => [
                                                        '#type' => 'link',
                                                        '#title' => (string)t('Download "@name"', ['@name' => $blob->meta['name']]),
                                                        '#url' => Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()], ['attributes' => ['target' => '_blank']]),
                                                        '#suffix' => '<br><i>' . $blob->meta['description'] .
                                                                     '</i>'
                                                ],
                                                'preview' => preg_match('/^(image|video|audio)\//', $blob->type, $matches) ? [
                                                        '#type' => 'inline_template',
                                                        '#template' => (
                                                        $matches[1] == 'image' ? '<br><img height="100" src="' . Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString() .
                                                                                 '">' : (
                                                        $matches[1] == 'video' ? '<br><video height="100" controls><source src="' . Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString() .
                                                                                 '" type="' . $blob->type .
                                                                                 '"></video>' : (
                                                        $matches[1] == 'audio' ? '<br><audio controls><source src="' . Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString() .
                                                                                 '" type="' . $blob->type .
                                                                                 '"></audio>' : (
                                                        ''))))
                                                ] : ''
                                        ];
                                }

                                return [
                                        '#type' => 'details',
                                        '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.files', 'fc')),
                                        '#open' => false,
                                        'content' => $files_tmpl,];
                        }

                        return null;
                }

                /**
                 * @param $args
                 * @return array
                 */
                protected function getRenderableTemplate_META($args) {

                        $metaTemplate = new Template();
                        $metaTemplate->get('workflow_item_infobox_metadata');

                        $rows = [];
                        $bool = ['accept_policy', 'conflict_of_interest'];
                        $base64 = [];
                        $sort = [
                                'authors' => 1,
                                'corporations' => 2,
                                'conflict_of_interest' => 3,
                                'conflict_of_interest_text' => 4,
                                'keywords' => 5,
                                'funding_name' => 6,
                                'funding_id' => 7,
                                'reviewer_suggestion'
                        ];

                        $keys = [
                                'corporations', 'keywords', 'accept_policy', 'authors', 'conflict_of_interest', 'conflict_of_interest_text', 'funding_name', 'funding_id',
                                'title_short', 'doi', 'publication_place', 'publication_year', 'sub_category', 'reviewer_suggestion', 'volume', 'issue'
                        ];
                        $unsortedCounter = count($keys);

                        foreach ($keys as $key) {

                                $value = in_array($key, $base64) ? base64_decode($this->getDataElement($key)) : $this->getDataElement($key);

                                if (in_array($key, $bool)) {
                                        $value = \Drupal::service('publisso_gold.texts')->get("global.bool_value." . (!!$value ? 'yes' : 'no'));
                                }

                                /**
                                 * treat special fields (serialized structures ...)
                                 **/
                                //keywords
                                if ($key == 'keywords') $value = implode('; ', array_filter(json_decode($value, true) ?? []));

                                //authors
                                if ($key == 'authors') {

                                        $authors = [];

                                        foreach (array_filter(json_decode($value, true) ?? []) as $id => $author) {

                                                $id = $author['weight'] ? $author['weight'] : count(array_filter(json_decode($value, true))) + $id;
                                                $authors[$id] = implode(' ', array_filter([$author['firstname'], $author['lastname']]));

                                                if (array_key_exists('is_corresponding', $author) && $author['is_corresponding'])
                                                        $authors[$id] .= ((string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.corresponding.suffix')));
                                        }

                                        ksort($authors);
                                        $value = implode("\n", $authors);
                                }

                                //corporations
                                if ($key == 'corporations') {
                                        $value = implode("\n", array_filter(json_decode($value, true) ?? []));
                                }

                                //volume
                                if($key == 'volume' && $this->getDataElement('volume')){
                                        $volume = new Volume($this->getDataElement('volume'));
                                        $value = implode('/', [$volume->getElement('number'), $volume->getElement('year')]);
                                }

                                //issue
                                if($key == 'issue' && $this->getDataElement('issue')){
                                        $issue = new Issue($this->getDataElement('issue'));
                                        $value = implode(' - ', array_filter([$issue->getElement('number'), $issue->getElement('title')]));
                                }

                                //put values to template (table)
                                if ($value) {
                                        $metaTemplate->setVar("workflow.item.infobox.$key.txt", \Drupal::service('publisso_gold.texts')->get("workflow.item.infobox.$key", 'fc'));
                                        $metaTemplate->setVar("workflow.item.infobox.$key.val", nl2br($value));

                                        $rowID = array_key_exists($key, $sort) ? $sort[$key] : $unsortedCounter++;

                                        $rows[$rowID] = [\Drupal::service('publisso_gold.texts')->get("workflow.item.infobox.$key", 'fc'),
                                                         new FormattableMarkup(nl2br($value), [])
                                        ];
                                }
                        }

                        ksort($rows, SORT_NUMERIC);

                        return [
                                '#type' => 'details',
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.meta', 'fc')),
                                '#open' => false,
                                'content' => [
                                        '#type' => 'table',
                                        '#rows' => $rows
                                ]];
                }

                /**
                 * @param $args
                 * @return array|null
                 */
                protected function getRenderableTemplate_ABSTRACT($args) {

                        $abstract = $this->getDataElement('abstract');

                        if ($abstract) {
                                $abstract = base64_decode( $abstract );
                        }

                        if(array_key_exists('hideonempty', $args)){
                                if($args['hideonempty'] && empty($abstract)){
                                        return null;
                                }
                        }

                        return [
                                '#type' => 'details',
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.abstract', 'fc')),
                                '#open' => false,
                                'content' => [
                                        '#type' => 'inline_template',
                                        '#template' => $abstract
                                ]];
                }

                /**
                 * @param $args
                 * @return array|null
                 */
                protected function getRenderableTemplate_ERRATUM($args) {

                        $erratum = $this->getDataElement('erratum');

                        if ($erratum) {

                                return [
                                        '#type' => 'details',
                                        '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.erratum', 'fc')),
                                        '#open' => false,
                                        'content' => [
                                                '#type' => 'inline_template',
                                                '#template' => $erratum
                                        ]];
                        }

                        return null;
                }

                /**
                 * @param $args
                 * @return array|null
                 */
                protected function getRenderableTemplate_ORIGTEXT($args) {

                        $text = '';

                        switch (substr($this->getElement('type'), 0, 1)) {

                                case 'b':
                                        $text = base64_decode($this->getDataElement('chapter_text'));
                                        break;

                                case 'j':
                                        $text = base64_decode($this->getDataElement('article_text'));
                                        break;

                                case 'c':
                                        $text = base64_decode($this->getDataElement('paper_text'));
                                        break;
                        }

                        if ($text) {

                                return [
                                        '#type' => 'details',
                                        '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.orig_text', 'fc')),
                                        '#open' => false,
                                        'content' => [
                                                '#type' => 'inline_template',
                                                '#template' => $text,
                                                '#prefix' => '<div id="chapter-content">',
                                                '#suffix' => '</div>'
                                        ]];
                        }

                        return null;
                }

                /**
                 * @param $args
                 * @return array|null
                 */
                protected function getRenderableTemplate_RECOMMENDATIONS($args) {

                        $result = \Drupal::database()->select('rwPubgoldWorkflowRecommendations', 't')->fields('t', [])->condition('wf_id', $this->getElement('id'), '=')->execute()->fetchAll();


                        if (count($result)) {

                                $ary = [
                                        '#type' => 'details',
                                        '#open' => false,
                                        '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.recommendations', 'fc')),
                                        'content' => [
                                                '#type' => 'table',
                                                '#header' => [
                                                        'user' => (string)t(\Drupal::service('publisso_gold.texts')->get('global.user', 'fc')),
                                                        'date' => (string)t(\Drupal::service('publisso_gold.texts')->get('global.date', 'fc')),
                                                        'recommendation' => (string)t(\Drupal::service('publisso_gold.texts')->get('global.recommendation', 'fc')),
                                                ],
                                                '#rows' => [],
                                                '#empty' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.recommendations.emtpy', 'fc')),
                                        ],
                                ];

                                foreach ($result as $_) {

                                        $__user = new User($_->created_by_uid);

                                        $ary['content']['#rows'][] = [
                                                'user' => $__user->profile->getReadableFullName(),
                                                'date' => $_->created,
                                                'recommendation' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.recommendations.recommendation_' . $_->recommendation, 'fc')),
                                        ];
                                }

                                return $ary;
                        }

                        return null;
                }

                /**
                 * @param $args
                 * @return array|null
                 */
                protected function getRenderableTemplate_COMMENTSFORAUTHOR($args) {

                        $comments = $this->getCommentsForAuthor();

                        if (count($comments)) {

                                $ary = [
                                        '#type' => 'details',
                                        '#open' => false,
                                        '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.comments_for_author', 'fc')),
                                        'content' => [
                                                '#type' => 'table',
                                                '#header' => [
                                                        'user' => '',
                                                        'comment' => '',
                                                ],
                                                '#rows' => [],
                                                '#empty' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.comments_for_author.emtpy', 'fc')),
                                        ],
                                ];

                                foreach ($comments as $_) {

                                        $__user = new User($_->cfa_created_by_uid);

                                        $ary['content']['#rows'][] = [
                                                'user' => $__user->profile->getReadableFullName(),
                                                'comment' => base64_decode($_->cfa_comment)
                                        ];
                                }

                                return $ary;
                        }

                        return null;
                }

                /**
                 * @param $args
                 * @return array|null
                 */
                protected function getRenderableTemplate_COMMENTSFORAUTHOR_ANON($args) {

                        $comments = $this->getCommentsForAuthor();

                        if (count($comments)) {

                                $ary = [
                                        '#type' => 'details',
                                        '#open' => false,
                                        '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.comments_for_author', 'fc')),
                                        'content' => [
                                                '#type' => 'table',
                                                '#header' => [
                                                        'user' => '',
                                                        'comment' => '',
                                                ],
                                                '#rows' => [],
                                                '#empty' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.comments_for_author.emtpy', 'fc')),
                                        ],
                                ];
                                $c = 1;
                                foreach ($comments as $_) {

                                        $__user = new User($_->cfa_created_by_uid);

                                        $ary['content']['#rows'][] = [
                                                'user' => (string)t('Comment #@nr', ['@nr' => $c++]),
                                                'comment' => base64_decode($_->cfa_comment)
                                        ];
                                }

                                return $ary;
                        }

                        return null;
                }

                /**
                 * @return bool
                 */
                public function hasEditors(){
                        return !!count($this->editors);
                }

                /**
                 * @return mixed
                 */
                protected function getCommentsForAuthor() {
                        return \Drupal::database()->select('rwPubgoldWorkflowCommentsForAuthors', 'cfa')->fields('cfa', [])->condition('cfa_wfid', $this->id, '=')->execute()->fetchAll();
                }

                /**
                 * @param $args
                 * @return array|null
                 */
                protected function getRenderableTemplate_COMMENTS($args) {

                        $comments = Comment::loadByWorkflowItem($this->id);

                        if(array_key_exists('hideonempty', $args)){
                                if($args['hideonempty'] && !count($comments)){
                                        return null;
                                }
                        }

                        $rows = [];
                        foreach($comments as $comment){
                                if(false !== ($access = $comment->userHasAccess(\Drupal::service('session')->get('user')['id'], $this->getSchemaCurrentRole()))){

                                        $user = new user($comment->getElement('creator_uid'));

                                        $link = null;
                                        if($comment->getElement('wf_schema_id') == $this->getElement('schema_identifier') && $comment->getElement('creator_uid') == \Drupal::service('session')->get('user')['id']){
                                                $link = Link::fromTextAndUrl('delete', Url::fromRoute('publisso_gold.comment.delete', ['id' => $this->id.'|'.$comment->getElement('id')], ['attributes' => ['onclick' => 'return confirm("'.((string)$this->t('Do you really want to delete this comment?')).'")']]));
                                        }

                                        $rows[] = [
                                                'created'       => $comment->getElement('created'),
                                                'user'          => $user->profile->getReadableFullname(),
                                                'role'          => \Drupal::service('publisso_gold.texts')->get('global.role.'.trim($comment->getElement('creator_role')), 'fc'),
                                                'comment'       => $comment->getElement('body'),
                                                'access'        => \Drupal::service('publisso_gold.texts')->get('comment.access.'.$access, 'fc'),
                                                'delete'        => $link
                                        ];
                                }
                        }

                        $comments = $this->getComments();

                        return [
                                '#type' => 'details',
                                '#open' => false,
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.comments', 'fc')),
                                'content' => [
                                        '#type' => 'table',
                                        '#header' => [
                                                'created' => (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.created', 'fc')),
                                                'user' => (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.user', 'fc')),
                                                'role' => (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.role', 'fc')),
                                                'comment' => (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.comment', 'fc')),
                                                'access' => (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.access', 'fc')),
                                                'delete' => ''
                                        ],
                                        '#rows' => $rows,
                                        '#empty' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.comments.emtpy', 'fc')),
                                ],
                        ];

                }

                /**
                 * @param $args
                 * @return array|null
                 */
                protected function getRenderableTemplate_COMMENTSANON($args) {

                        $comments = Comment::loadByWorkflowItem($this->id);

                        if(array_key_exists('hideonempty', $args)){
                                if($args['hideonempty'] && !count($comments)){
                                        return null;
                                }
                        }

                        $rows = [];
                        foreach($comments as $comment){
                                if(false !== ($access = $comment->userHasAccess(\Drupal::service('session')->get('user')['id'], $this->getSchemaCurrentRole()))){

                                        $user = new user($comment->getElement('creator_uid'));

                                        $rows[] = [
                                                'created'       => $comment->getElement('created'),
                                                'role'          => \Drupal::service('publisso_gold.texts')->get('global.role.'.$comment->getElement('creator_role'), 'fc'),
                                                'comment'       => $comment->getElement('body'),
                                                'access'        => \Drupal::service('publisso_gold.texts')->get('comment.access.'.$access, 'fc'),
                                        ];
                                }
                        }

                        $comments = $this->getComments();



                        return [
                                '#type' => 'details',
                                '#open' => false,
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.comments', 'fc')),
                                'content' => [
                                        '#type' => 'table',
                                        '#header' => [
                                                'created' => (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.created', 'fc')),
                                                'role' => (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.role', 'fc')),
                                                'comment' => (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.comment', 'fc')),
                                                'access' => (string)$this->t(\Drupal::service('publisso_gold.texts')->get('global.access', 'fc')),
                                        ],
                                        '#rows' => $rows,
                                        '#empty' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.comments.emtpy', 'fc')),
                                ],
                        ];

                }

                /**
                 * @return mixed
                 */
                protected function getComments() {
                        return \Drupal::database()->select('rwPubgoldWorkflowComments', 'wfc')->fields('wfc', [])->condition('wfc_wfid', $this->id, '=')->execute()->fetchAll();
                }

                /**
                 * @param $args
                 * @return array|null
                 */
                protected function getRenderableTemplate_REFERENCES($args) {

                        $references = $this->getDataElement('references');

                        if ($references) {

                                $references = nl2br($references);

                                return [
                                        '#type' => 'details',
                                        '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.references', 'fc')),
                                        '#open' => false,
                                        'content' => [
                                                '#type' => 'inline_template',
                                                '#template' => $references
                                        ]];
                        }

                        return null;
                }

                /**
                 * @param $args
                 * @return array
                 */
                protected function getRenderableTemplate_REVIEWSHEETS($args) {
                        return \Drupal::formBuilder()->getForm( "Drupal\\publisso_gold\\Form\\Workflow\\displayReviewsheetSkel", [ 'wf_id' => $this->id , 'args' => ['state' => ['finished', 'rejected']]] );
                }

                /**
                 * @param $args
                 * @return array
                 */
                protected function getRenderableTemplate_USERREVIEWSHEETS($args){
                        return \Drupal::formBuilder()->getForm( "Drupal\\publisso_gold\\Form\\Workflow\\displayReviewsheetSkel", [ 'wf_id' => $this->id, 'args' => [ 'uid' => \Drupal::service('session')->get( 'user' )[ 'id' ], 'state' => ['finished', 'rejected'] ] ] );
                }

                /**
                 * @param $args
                 * @return array|null
                 */
                public function getReviewsheetForm($args){

                        $rsIDs = $this->getUserReviewsheets($args);
                        $sheets = [];
                        $users = [];

                        if (array_key_exists('uid', $args) && !preg_match('/^(\d+)$/', $args['uid'])) {

                                foreach ($rsIDs as $_uid => $_sheets) {

                                        $user = new User($_uid);
                                        $sheets[$_uid] = [
                                                '#type' => 'details',
                                                '#collapsed' => true,
                                                '#title' => $user->profile->getReadableFullName(),
                                                'content' => []
                                        ];

                                        foreach ($_sheets as $_sheet) {
                                                $rs = $this->getReviewsheetByID($_sheet);
                                                $sheets[$_uid]['content'][] = $this->getRenderableReviewsheet($rs->reviewsheet, $rs->state, $rs->modified);
                                        }
                                }


                        } else {
                                if ( array_key_exists( 'uid', $args ) ) {

                                        if(array_key_exists($args['uid'], $rsIDs) && is_array($rsIDs[ $args[ 'uid' ] ])) {
                                                foreach ( $rsIDs[ $args[ 'uid' ] ] as $id ) {

                                                        $rs = $this->getReviewsheetByID( $id );
                                                        $sheets[] = $this->getRenderableReviewsheet( $rs->reviewsheet, $rs->state, $rs->modified );
                                                }
                                        }
                                }
                                else {
                                        foreach ( $rsIDs as $uid => $ids ) {

                                                $user = new User($uid);

                                                $ary = [
                                                        '#type' => 'details',
                                                        '#title' => $user->profile->getReadableFullname(),
                                                        '#collapsed' => true,
                                                        'content' => []
                                                ];

                                                foreach ( $ids as $id ) {
                                                        $rs = $this->getReviewsheetByID( $id );
                                                        $ary['content'][] = $this->getRenderableReviewsheet( $rs->reviewsheet, $rs->state, $rs->modified );
                                                }

                                                $sheets[] = $ary;
                                        }
                                }
                        }

                        if (count($sheets)) {

                                return [
                                        '#type' => 'details',
                                        '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.reviewsheets', 'fc')),
                                        '#collapsed' => true,
                                        'content' => $sheets
                                ];
                        }

                        return null;
                }

                /**
                 * @param $args
                 * @return array
                 */
                public function getUserReviewsheets($args) {

                        if (!$this->medium) $this->loadMedium();

                        $qry = \Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', ['uid', 'id']);
                        $cnd = $qry->andConditionGroup();
                        $cnd->condition('wfid', $this->id, '=');

                        if (isset($args['uid']))
                                $cnd->condition('uid', $args['uid'], '=');

                        if (isset($args['state'])) {

                                if (is_string($args['state']) && strtolower($args['state']) != 'all')
                                        $cnd->condition('state', $args['state'], '=');
                                elseif (is_array($args['state'])) {

                                        $cnd->condition( 'state', $args[ 'state' ], 'IN' );
                                }
                        }

                        $qry->condition($cnd);
                        $res = $qry->orderBy('id', 'ASC')->execute()->fetchAll();

                        $rs = [];

                        if (array_key_exists('nogroup', $args) && $args['nogroup'] == true) {
                                foreach ($res as $_) $rs[] = $_->id;
                        } else {
                                foreach ($res as $_) $rs[$_->uid][] = $_->id;
                        }

                        return $rs;
                }

                /**
                 * @param $id
                 * @param null $field
                 * @return mixed|null
                 */
                public function getReviewsheetByID($id, $field = null) {

                        $qry = \Drupal::database()->select('rwPubgoldReviewerReviewsheet', 't')->fields('t', []);
                        $cnd = $qry->andConditionGroup();
                        $cnd->condition('id', $id, '=');
                        $qry->condition($cnd);
                        $result = $qry->execute()->fetchAll()[0];

                        if ($field) {

                                if (property_exists($result, $field)) return $result->$field;
                                else return null;
                        } else {
                                return $result;
                        }
                }

                /**
                 * @param $sheet
                 * @param $state
                 * @param $modified
                 * @return array
                 */
                protected function getRenderableReviewsheet($sheet, $state, $modified) {

                        $doc = new \DOMDocument();
                        $doc->loadXML($sheet);
                        $xpath = new \DOMXpath($doc);
                        $ary = [];

                        foreach ($xpath->evaluate('/reviewsheet/elements/element') as $element) {
                                $ary[] = $this->parseReviewsheetElement($element, $xpath, 0);
                        }

                        $recommendation = '';
                        $recommendation = $xpath->evaluate('/reviewsheet/comments/recommendation');

                        if ($recommendation->length) {
                                $recommendation = (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.review.field.recommendation.option.' . $recommendation[0]->nodeValue, 'fc'));
                        } else {
                                $recommendation = \Drupal::service('string_translation')->translate('none');
                        }

                        $comment = null;
                        $comment = $xpath->evaluate('/reviewsheet/comments/comment_for_author');

                        if ($comment->length) {

                                $comment = [
                                        '#type' => 'container',
                                        'content' => [
                                                '#markup' => $comment[0]->nodeValue,
                                                '#prefix' => '<strong>' . ((string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.review.field.comment_for_author.title', 'fc'))) .
                                                             '</strong><br><br>'
                                        ],
                                        '#prefix' => '<hr>'
                                ];
                        } else {
                                $comment = null;
                        }
                        /*
                        $diff = new Diff('<a href="http://www.google.de">Google.de</a>', '<a href="http://www.google.com">Google.com</a>');
                        $diff->build();*/
                        //echo $diff->getDifference(); exit();

                        return [

                                '#type' => 'details',
                                '#collapsed' => true,
                                'content' => $ary,
                                'comment' => $comment,
                                '#title' => (string)t(\Drupal::service('publisso_gold.texts')->get('workflow.item.infobox.headline.reviewsheets.sheet'), [
                                        '@recommendation' => $recommendation,
                                        '@state' => $state,
                                        '@date' => $modified
                                ])];
                }

                /**
                 * @param \DOMElement $element
                 * @param \DOMXpath $xpath
                 * @return array|string[]
                 */
                protected function parseReviewsheetElement(\DOMElement & $element, \DOMXpath & $xpath) {

                        $ary = [];

                        switch ($element->getAttribute('type')) {

                                case 'item':
                                        $ary = [
                                                '#type' => 'markup',
                                                '#prefix' => '<p>',
                                                '#suffix' => '</p>'
                                        ];
                                        break;

                                case 'heading':
                                        $ary = [
                                                '#type' => 'markup',
                                                '#prefix' => '<h2>',
                                                '#suffix' => '</h2>'
                                        ];
                                        break;

                                case 'subheading':
                                        $ary = [
                                                '#type' => 'markup',
                                                '#prefix' => '<h3>',
                                                '#suffix' => '</h3>'
                                        ];
                                        break;

                                case 'checkbox':
                                        $ary = [
                                                '#type' => 'checkbox',
                                                '#disabled' => true
                                        ];
                                        break;

                                case 'textfield':
                                        $ary = [
                                                '#type' => 'textfield',
                                                '#disabled' => true
                                        ];
                                        break;
                        }

                        $text = $xpath->evaluate('text', $element)[0];

                        if ($xpath->evaluate('value', $element)->length) {
                                $ary['#default_value'] = $xpath->evaluate('value', $element)[0]->nodeValue;
                        }

                        if ($ary['#type'] == 'markup')
                                $ary['#markup'] = $text->nodeValue;
                        else
                                $ary['#title'] = $text->nodeValue;

                        if ($xpath->evaluate('subelements/subelement', $element)->length) {

                                $ary['subelements'] = [];

                                foreach ($xpath->evaluate('subelements/subelement', $element) as $subnode) {
                                        $ary['subelements'][] = $this->parseReviewsheetElement($subnode, $xpath);
                                }
                        }

                        return $ary;
                }

                /**
                 * @param bool $sendInfoMail
                 * @param false $preliminary
                 */
                public function publish($sendInfoMail = true, $preliminary = false) {}

                /**
                 * @param null $id
                 * @return false
                 */
                public function initSchema($id = null){

                        if(!$id && !$this->getElement('schema')) return false;

                        $name = \Drupal::database()->select('rwPubgoldWorkflowSchema', 't')->fields('t', ['name'])->condition('id', $id, '=')->execute()->fetchField();

                        if(!$name) return false;

                        $schema = new WorkflowSchema($name);



                        $this->setElement('schema', serialize($schema->export()));
                        $this->schema = $schema->export();
                        $this->setElement('schema_identifier', $schema->getSchemaElement()['schema_identifier'], true, true);
                        $this->setElement('state', $schema->getStateFromSchema($this->getSchemaEntryPointID()));

                        $run = [
                                date('Y-m-d H:i:s') => [
                                        'old_identifier' => null,
                                        'new_identifier' => $this->getElement('schema_identifier'),
                                        'initiator_uid' => \Drupal::service('session')->get('user')['id']
                                ]
                        ];

                        $this->setElement('run', serialize($run));
                }

                /**
                 * @return false
                 */
                private function reinitSchema(){

                        #if(!$this->schema) return false;
                        #$id = $this->schema[array_keys($this->schema)[0]]['schema_id'];
                        #$name = \Drupal::database()->select('rwPubgoldWorkflowSchema', 't')->fields('t', ['name'])->condition('id', $id, '=')->execute()->fetchField();
                        $name = $this->getParentMedium()->getElement('wfschema');
                        if(!$name) return false;

                        $schema = new WorkflowSchema($name);
                        $this->setElement('schema', serialize($schema->export()), false);
                        $this->schema = $schema->export();
                        #$this->setElement('reinit_schema', 0, false);
                        $this->save();
                }

                /**
                 * @param null $id
                 * @return mixed
                 */
                public function getSchema($id = null){
                        return $id ? $this->schema[$id] : $this->schema;
                }

                /**
                 * @return mixed|null
                 */
                public function getSchemaEntryPoint(){

                        if(!$this->schema) return null;

                        foreach($this->schema as $item){

                                if($item['schema_entry_point']) return $item;
                        }

                        return null;
                }

                /**
                 * @return mixed|null
                 */
                public function getSchemaEntryPointIdentifier(){

                        if(null !== ($item = $this->getSchemaEntryPoint())){
                                return $item['schema_identifier'];
                        }

                        return null;
                }

                /**
                 * @return mixed|null
                 */
                public function getSchemaEntryPointID(){

                        if(null !== ($item = $this->getSchemaEntryPoint())){
                                return $item['schema_id'];
                        }

                        return null;
                }

                /**
                 * @param null $schema_identifier
                 * @return array
                 */
                public function getSchemaFollowers($schema_identifier = null){

                        if(!$this->schema) return [];

                        $subschema = [];


                        foreach($this->schema as $id => $schema){

                                if($schema['schema_identifier'] == $this->getElement('schema_identifier')) {

                                        $follower = $schema['schema_follower'];

                                        foreach($this->schema as $_){

                                                if($_['schema_identifier'] == $follower)
                                                        $subschema[] = $_;
                                        }
                                }
                        }

                        return $subschema;
                }

                /**
                 * @param null $identifier
                 * @param false $withDefault
                 * @return array
                 */
                public function getSchemaItemActions($identifier = null, $withDefault = false){

                        $identifier = $identifier ?? $this->getSchemaCurrentIdentifier();

                        $items = [];

                        foreach($this->schema as $_){
                                if($_['schema_identifier'] == $identifier){

                                        if(($withDefault && $_['schema_default_action']) || !$_['schema_default_action'])
                                                $items[] = $_;
                                }
                        }

                        return $identifier ? $items : [];
                }

                /**
                 * @param null $key
                 * @return mixed
                 */
                public function getSchemaDefaultFollower($key = null){

                        $item = $this->getSchemaItemsByIdentifier();

                        $item = array_filter($item, ['self', 'filterSchemaDefaultFollower']);
                        return $key ? array_shift($item)[$key] : array_shift($item);
                }

                /**
                 * @return mixed|null
                 */
                public function getSchemaPreviousIdentifier(){

                        $identifier = $this->getSchemaCurrentIdentifier();

                        if($identifier){

                                $parents = $this->getSchemaItemsByFollower($identifier);

                                if(is_array($parents) && count($parents)){

                                        foreach($parents as $_){
                                                if($_['schema_default_action']) return $_['schema_identifier'];
                                        }

                                        return $parents[0]['schema_identifier'];
                                }
                        }

                        return null;
                }

                /**
                 * @param null $identifier
                 * @return array
                 */
                public function getSchemaItemsByIdentifier($identifier = null){

                        $identifier = $identifier ?? $this->getSchemaCurrentIdentifier();

                        $items = [];

                        foreach($this->getSchema() as $item){
                                if($item['schema_identifier'] == $identifier){
                                        $items[] = $item;
                                }
                        }


                        return $items;
                }

                /**
                 * @param null $identifier
                 * @return array
                 */
                public function getSchemaItemsByFollower($identifier = null){

                        $identifier = $identifier ?? $this->getSchemaCurrentIdentifier();

                        $items = [];

                        foreach($this->getSchema() as $item){
                                if($item['schema_follower'] == $identifier){
                                        $items[] = $item;
                                }
                        }
                        return $items;
                }

                /**
                 * @param $var
                 * @return bool
                 */
                protected function filterSchemaDefaultFollower($var){

                        return (array_key_exists('schema_default_action', $var) && $var['schema_default_action'] == 1);
                }

                /**
                 * @return mixed|string
                 */
                public function getSchemaCurrentRole(){

                        $id = $this->getElement('schema_identifier');
                        $item = null;

                        foreach($this->schema as $schemaItem){
                                if($schemaItem['schema_identifier'] == $id){
                                        if(array_key_exists('boxes', $schemaItem) && is_array($schemaItem['boxes'])){
                                                $item = $schemaItem;
                                                break;
                                        }
                                }
                        }

                        $item = explode(':', $item['schema_currentrole']);

                        switch($item[0]){

                                case 'r':
                                        return strtolower(\Drupal::service('publisso_gold.tools')->getRoleByRoleID($item[1]));
                                        break;

                                default: return $item[1];

                        }
                }

                /**
                 * @param $id
                 * @return mixed|null
                 */
                public function getSchemaItem($id){
                        return $this->schema[$id] ?? null;
                }

                /**
                 * @param $identifier
                 * @param $follower
                 * @return mixed|null
                 */
                public function getSchemaItemByIdentifierAndFollower($identifier, $follower){

                        foreach($this->schema as $item){
                                if($item['schema_identifier'] == $identifier && $item['schema_follower'] == $follower){
                                        return $item;
                                        break;
                                }
                        }

                        return null;
                }

                /**
                 * @param $id
                 * @return mixed|null
                 */
                public function getSchemaItemSelectAction($id){
                        return $this->schema[$id]['schema_select_action'] ?? null;
                }

                /**
                 * @param null $id
                 * @return array
                 */
                public function getSchemaBoxes($id = null){

                        if(!$id) $id = $this->getElement('schema_identifier');

                        $boxes = [];

                        foreach($this->schema as $schemaItem){

                                if($schemaItem['schema_identifier'] == $id){

                                        if(array_key_exists('boxes', $schemaItem) && is_array($schemaItem['boxes'])){
                                                $boxes = $schemaItem['boxes'];
                                                break;
                                        }
                                }
                        }

                        usort($boxes, ['self', 'sortSchemaBoxes']);
                        return $boxes;
                }

                /**
                 * @param $a
                 * @param $b
                 * @return int
                 */
                protected function sortSchemaBoxes($a, $b){
                        return $a['box_weight'] <=> $b['box_weight'];
                }

                /**
                 * @param array $box
                 * @param array $args
                 * @return array|null
                 */
                public function renderSchemaBox(array &$box, &$args = []){

                        $args = array_merge($args, ['wf_id' => $this->id]);

                        $show = false;

                        if(!empty($box['box_only_show_on_empty_fields'])){

                                $checkFields = array_filter(array_map('trim', explode(',', $box['box_only_show_on_empty_fields'])) ?? []);

                                foreach($checkFields as $field){

                                        if(in_array($field, $this->getKeys()))
                                                if(empty($this->getElement($field))) $show = true;
                                }
                        }
                        else{
                                $show = true;
                        }

                        if(!$show) return null;

                        switch($box['box_type']){

                                case 'form':

                                        $ts = \Drupal::service('tempstore.private')->get('publisso_gold');
                                        $formState = new FormState();
                                        $formID = $this->formBuilder()->getFormId("\Drupal\publisso_gold\\".$box['box_name'], $formState);
                                        $form = $ts->get("overwrite-".$formID) ?? $box['box_name'];
                                        return \Drupal::formBuilder()->getForm("\Drupal\publisso_gold\\".$form, $args);
                                        break;

                                case 'box':
                                        return $this->getRenderableTemplate($box['box_name'], $args);
                                        break;
                        }
                }

                /**
                 * @param $uid
                 */
                public function assignToUser($uid){

                        if(!is_array($uid)) $uid = [$uid];
                        $uid = array_filter(array_map("trim", $uid));
                        $uid = 'u:'.implode(',u:', $uid);
                        $this->setElement('assigned_to', $uid);
                }

                /**
                 * @param $uid
                 */
                public function assignToGroup($uid){

                        if(!is_array($uid)) $uid = [$uid];
                        $uid = array_filter(array_map("trim", $uid));
                        $uid = 'g:'.implode(',g:', $uid);
                        $this->setElement('assigned_to', 'g:'.$uid);
                }

                public function getHTMLPageTitle(){

                }
        }
