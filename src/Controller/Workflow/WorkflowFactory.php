<?php
        /**
         * Created by PhpStorm.
         * User: sseliger
         * Date: 11.02.19
         * Time: 11:24
         */
        
        namespace Drupal\publisso_gold\Controller\Workflow;
        
        
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class WorkflowFactory
         * @package Drupal\publisso_gold\Controller\Workflow
         */
        class WorkflowFactory {
        
                /**
                 * WorkflowFactory constructor.
                 * -- deprecated --
                 * @param null   $id
                 * @param string $tbl_column_prefix
                 * @param bool   $loadMedium
                 */
                public function __construct($id = null, $tbl_column_prefix = 'wf_', $loadMedium = true) {
                        
                        if ($id !== null) {
                                
                                if ($tbl_column_prefix == 'wf_')
                                        $db_table = 'rwPubgoldWorkflow';
                                elseif ($tbl_column_prefix == 'wfh_')
                                        $db_table = 'rwPubgoldWorkflowHistory';
                        
                                $type = \Drupal::database()->select($db_table, 't')->fields('t', [$tbl_column_prefix.'type'])->condition($tbl_column_prefix.'id', $id, '=')->execute()->fetchField();
                                
                                if(!$type) return false;
                                
                                switch($type){
                                        case 'bcs': return new ChapterWorkflow($id, $tbl_column_prefix, $loadMedium); break;
                                        case 'jas': return new ArticleWorkflow($id, $tbl_column_prefix, $loadMedium); break;
                                        case 'bce': return new ChapterErratumWorkflow($id, $tbl_column_prefix, $loadMedium); break;
                                }
                        }
                        else{
                                return null;
                        }
                }
        
                /**
                 * @param string $type
                 * @param int $mediumID
                 * @return ArticleWorkflow|ChapterErratumWorkflow|ChapterWorkflow|null
                 */
                public static function create(string $type, int $mediumID){
                        
                        switch($type){
                                case 'bcs':
                                        $workflow = self::ChapterWorkflow();
                                        break;
                                case 'jas':
                                        $workflow = self::ArticleWorkflow();
                                        break;
                                case 'bce':
                                        $workflow = self::ChapterErratumWorkflow();
                                        break;
                                default:
                                        Publisso::tools()->logMsg("ERROR in file ".__FILE__.' in line '.__LINE__.": Workflow-type not implemented!");
                                        return null;
                        }
                        
                        $workflow->init($mediumID, $type);
                        return $workflow;
                }
        
                /**
                 * @return ChapterWorkflow
                 */
                public static function ChapterWorkflow(){
                        return new ChapterWorkflow();
                }
        
                /**
                 * @return ArticleWorkflow
                 */
                public static function ArticleWorkflow(){
                        return new ArticleWorkflow();
                }
        
                /**
                 * @return ChapterErratumWorkflow
                 */
                public static function ChapterErratumWorkflow(){
                        return new ChapterErratumWorkflow();
                }
        }
