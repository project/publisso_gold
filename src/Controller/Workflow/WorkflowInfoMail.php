<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Controller\Workflow\WorkflowInfoMail.
 */

namespace Drupal\publisso_gold\Controller\Workflow;
use Drupal\Core\Url;
        
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\JournalManager;
        use \Drupal\publisso_gold\Controller\Medium\Book;
use \Drupal\publisso_gold\Controller\Medium\Journal;
use \Drupal\publisso_gold\Controller\Medium\Conference;
        
        use Drupal\publisso_gold\Controller\Publisso;
        use \Drupal\publisso_gold\Controller\User;
        
        /**
         * Class WorkflowInfoMail
         * @package Drupal\publisso_gold\Controller\Workflow
         */
        class WorkflowInfoMail {
    
        private $workflow;
        private $process;
        private $tools;
        private $setup;
        private $session;
        private $texts;
        private $template;
        private $global_vars;
        private $recipient_vars;
        private $mediumInfo;
        private $medium;
        private $recipients;
        private $cc;
        private $bcc;
        private $override_recipient;
        
        private $pecipient_vars;
        private $additional_vars;
        
        /**
         * WorkflowInfoMail constructor.
         * @param $workflow
         * @param int $mailID
         * @param null $override_recipient
         */
        public function __construct(&$workflow, int $mailID, $override_recipient = null){
            
                $this->workflow                 = $workflow;
                $this->mailID                   = $mailID;
                $this->override_recipient       = $override_recipient;
                $this->global_vars              = [];
                $this->recipient_vars           = [];
                $this->mediumInfo               = [];
                $this->recipients               = [];
                $this->cc                       = [];
                $this->bcc                      = [];
                $this->recipient_vars           = [];
                $this->additional_vars          = [];
                
                /**
                 * Lade benötigte Services für den schnelleren Zugriff
                 *
                 *  Publisso - Tools
                 *  Publisso - Setup
                 *  Publisso - Texts
                 *  Drupal - Session
                 */
                
                foreach(['publisso_gold.tools', 'publisso_gold.setup', 'publisso_gold.texts', 'session'] as $service){
                        
                        $name = $service;
                        if(preg_match('/^.+\.(.+)$/', $service, $matches)){
                                $name = $matches[1];
                        }
                        
                        $this->$name = \Drupal::service($service);
                }
                
                //lade Mailtemplate
                if($this->loadTemplate() === false) return false;
                
                /**
                 * Laden der Eckdaten des Mediums (Typ und ID), da sie
                 * in diversen Methoden genutzt werden
                 */
                if($this->loadMediumInfo() === false) return false;
                
                /**
                 * Laden des Mediums OHNE Kindelemente
                 */
                if($this->loadMedium() === false) return false;
                
                /**
                 * Laden der Variablen aus Workflow, Workflow-Daten, Medium als
                 *
                 * ::workflow.[var]::
                 * ::workflow.data.[var]::
                 * ::medium.[var]::
                                  *
                 * Der sendende User (der den Versand veranlasst hat), wird als Sender gespeichert
                 * ::sender.[var]::
                 * ::sender.profile.[var]::
                 *
                 * Die Daten werden aus der Datenbank nativ genommen und nicht weiter aufbereitet,
                 * d.h. JSONs, serialisierte Daten, Binarys usw. werden nicht gefiltert
                 *
                 * Weiterhin werden bestimmte Werte aus dem Setup hinzugefügt (z.B. Signatur Publisso)
                 *
                 * Daten zum Empfänger werden während des Parsens der Empfänger selbst gesetzt (::parseRecipients)
                 *
                 */
                
                $this->loadGlobalVars();
                
                /**
                 * Parsen der Empfänger - derzeit unterstützt sind angaben wie z.B.:
                 * g:editorial_office - Alle User, welche als EO diesem Workflow zugeordnet sind
                 * r:2 - Alle User, welche die Rolle PUBLISSO haben
                 * u:1 - User mit der ID 1
                 *
                 * Sollte der Eintrag keinem der Muster entsprechen, dann wird angenommen,
                 * dass es sich um eine Emailadresse handelt und ungeprüft als Empfänger gesetzt
                 *
                 */
                $this->loadRecipients();
        }
        
        /**
         * @param array $vars
         */
        public function addVars(array $vars){
                $this->additional_vars = $vars;
        }
        
        /**
         * @return bool
         */
        private function loadMedium(){
                
                switch($this->mediumInfo['type']){
                        
                        case 'book':
                                $this->medium = BookManager::getBook($this->mediumInfo['id'], false);
                                $this->global_vars['::lnk.medium::'] = $this->medium->getUrl()->toString();
                                break;
                        
                        case 'journal':
                                $this->medium = JournalManager::getJournal($this->mediumInfo['id'], true);
                                $this->medium->getUrl();
                                $this->global_vars['::lnk.medium::'] = Url::fromRoute('publisso_gold.journals_journal', ['jrn_id' => $this->mediumInfo['id']])->setAbsolute()->toString();
                                break;
                        
                        case 'conference':
                                $this->medium = new Conference($this->mediumInfo['id'], true);
                                $this->global_vars['::lnk.medium::'] = Url::fromRoute('publisso_gold.conferences_conference', ['cf_id' => $this->mediumInfo['id']])->setAbsolute()->toString();
                                break;
                        
                        default:
                                return false;
                                break;
                }
                
                return true;
        }
        
        private function loadRecipients(){
                
                $recipientVars = [];
                
                foreach(['recipient', 'cc', 'bcc'] as $mailTo){
                        
                        $users = [];
                        
                        if($mailTo == 'recipient' && $this->override_recipient !== null)
                                $this->template[$mailTo] = $this->override_recipient;
                        
                        foreach(array_filter(array_map('trim', explode(',', $this->template[$mailTo]))) as $_){

                                switch(substr($_, 0, 2)){

                                        case 'g:': //groups
                                                $group = explode(':', $_);
                                                $group = $group[1];

                                                switch($group){

                                                        case 'editorial_office':
                                                                
                                                                foreach(array_filter(explode(',', $this->workflow->getElement('assigned_to_eo'))) as $uid){
                                                                        $users[] = new User($uid);
                                                                }
                                                                
                                                                if(!count($users) && $mailTo == 'recipient'){ //try Publisso
                                                                        
                                                                        foreach($this->tools->getUserIDsByRole($this->setup->getValue('default.admin.role.name')) as $uid){
                                                                                $users[] = new User($uid);
                                                                        }
                                                                }
                                                                break;

                                                        case 'editors_in_chief':
                                                                foreach(array_filter(explode(',', $this->workflow->getElement('assigned_to_eic'))) as $uid){
                                                                        $users[] = new User($uid);
                                                                }
                                                                
                                                                if(!count($users) && $mailTo == 'recipient'){ //try EO
                                                                        foreach(array_filter(explode(',', $this->workflow->getElement('assigned_to_eo'))) as $uid){
                                                                                $users[] = new User($uid);
                                                                        }
                                                                }
                                                                
                                                                if(!count($users) && $mailTo == 'recipient'){ //try Publisso
                                                                        
                                                                        foreach($this->tools->getUserIDsByRole($this->setup->getValue('default.admin.role.name')) as $uid){
                                                                                $users[] = new User($uid);
                                                                        }
                                                                }
                                                                
                                                                break;

                                                        case 'reviewers':
                                                                foreach(array_filter(explode(',', $this->workflow->getElement('assigned_to_reviewer'))) as $uid){
                                                                        $users[] = new User($uid);
                                                                }
                                                                
                                                                break;

                                                        case 'editors':
                                                                foreach(array_filter(explode(',', $this->workflow->getElement('assigned_to_editor'))) as $uid){
                                                                        $users[] = new User($uid);
                                                                }
                                                                
                                                                if(!count($users) && $mailTo == 'recipient'){ //try EiC
                                                                        foreach(array_filter(explode(',', $this->workflow->getElement('assigned_to_eic'))) as $uid){
                                                                                $users[] = new User($uid);
                                                                        }
                                                                }
                                                                
                                                                if(!count($users) && $mailTo == 'recipient'){ //try EO
                                                                        foreach(array_filter(explode(',', $this->workflow->getElement('assigned_to_eo'))) as $uid){
                                                                                $users[] = new User($uid);
                                                                        }
                                                                }
                                                                
                                                                if(!count($users) && $mailTo == 'recipient'){ //try Publisso
                                                                        
                                                                        foreach($this->tools->getUserIDsByRole($this->setup->getValue('default.admin.role.name')) as $uid){
                                                                                $users[] = new User($uid);
                                                                        }
                                                                }
                                                                break;
                                                }
                                                
                                                foreach($users as $_user){
                                                        
                                                        if(!in_array($_user->profile->getElement('email'), $$mailTo)){
                                                                ${$mailTo}[] = $_user->profile->getElement('email');
                                                                end($$mailTo);
                                                                $index = key($$mailTo);
                                                                $recipientVars[$mailTo][$index] = [];
                                                                
                                                                foreach($_user->getKeys() as $key){
                                                                        $recipientVars[$mailTo][$index]["::recipient.$key::"] = $_user->getElement($key);
                                                                }
                                                                
                                                                foreach($_user->profile->getKeys() as $key){
                                                                        $recipientVars[$mailTo][$index]["::recipient.profile.$key::"] = $_user->profile->getElement($key);
                                                                }
                                                        }
                                                }

                                                break;

                                        case 'u:': //user
                                                $_user = explode(':', $_);
                                                
                                                if($_user[1] == 'submitting_author')
                                                        $_user[1] = $this->workflow->getElement('created_by_uid');

                                                $_user = new User($_user[1]);

                                                if(!in_array($_user->profile->getElement('email'), ${$mailTo} ?? [])){
                                                        
                                                        ${$mailTo}[] = $_user->profile->getElement('email');
                                                        end($$mailTo);
                                                        $index = key($$mailTo);
                                                        $recipientVars[$mailTo][$index] = [];
                                                        
                                                        foreach($_user->getKeys() as $key){
                                                                $recipientVars[$mailTo][$index]["::recipient.$key::"] = $_user->getElement($key);
                                                        }
                                                        
                                                        foreach($_user->profile->getKeys() as $key){
                                                                $recipientVars[$mailTo][$index]["::recipient.profile.$key::"] = $_user->profile->getElement($key);
                                                        }
                                                }
                                                break;

                                        case 'r:': //users by role
                                                $role = explode(':', $_);
                                                $role = $role[1];

                                                foreach($this->tools->getUserIDsByRole($role) as $uid){
                                                        
                                                        $_user = new User($uid);
                                                        
                                                        if(!in_array($_user->profile->getElement('email'), $$mailTo)){
                                                                
                                                                ${$mailTo}[] = $_user->profile->getElement('email');
                                                                end($$mailTo);
                                                                $index = key($$mailTo);
                                                                $recipientVars[$mailTo][$index] = [];
                                                                
                                                                foreach($_user->getKeys() as $key){
                                                                        $recipientVars[$mailTo][$index]["::recipient.$key::"] = $_user->getElement($key);
                                                                }
                                                                
                                                                foreach($_user->profile->getKeys() as $key){
                                                                        $recipientVars[$mailTo][$index]["::recipient.profile.$key::"] = $_user->profile->getElement($key);
                                                                }
                                                        }
                                                }
                                                break;

                                        default:
                                                foreach(array_filter(preg_split('/[;, ]/', $_)) as $___) ${$mailTo}[] = $___;
                                }
                        }
                }
                
                if(isset($recipient) && !count($recipient) && \Drupal::service('publisso_gold.setup')->getValue('system.email.fallback.recipient')){
                        
                        //set fallback emailrecipient if none found/set
                        $recipient[] = $this->setup->getValue('system.email.fallback.recipient');

                        //remove fallback emailrecipient from (b)cc-lists
                        if(array_search($recipient[0], $cc ?? '' )) array_splice($cc , array_search($recipient[0], $cc ), 1);
                        if(array_search($recipient[0], $bcc ?? '')) array_splice($bcc, array_search($recipient[0], $bcc), 1);

                        $this->template['template'] .= "\n\n######################################################\n".((string)t('You receive this email because no other recipient could be detected')."\n######################################################");
                }
                
                $this->recipients     = $recipient;
                $this->cc             = $cc;
                $this->bcc            = $bcc;
                $this->recipient_vars = $recipientVars;
        }
        
        /**
         * @return bool
         */
        private function loadMediumInfo(){
                
                switch(substr($this->workflow->getElement('type'), 0, 1)){
                        
                        case 'b':
                                $this->mediumInfo['type'] = 'book';
                                $this->mediumInfo['id'  ] = $this->workflow->getDataElement('bk_id');
                                break;
                        
                        case 'j':
                                $this->mediumInfo['type'] = 'journal';
                                $this->mediumInfo['id'  ] = $this->workflow->getDataElement('jrn_id');
                                break;
                        
                        case 'c':
                                $this->mediumInfo['type'] = 'conference';
                                $this->mediumInfo['id'  ] = $this->workflow->getDataElement('cf_id');
                                break;
                        
                        default:
                                return false;
                }
                
                return true;
        }
        
        /**
         * @return bool
         */
        private function loadTemplate(){
                
                $qry = \Drupal::database()->select('rwPubgoldMailtemplates', 't')->fields('t', ['template', 'cc', 'bcc', 'subject', 'recipient', 'attachments'])->condition('id', $this->mailID, '=');
                
                if(!$qry->countQuery()->execute()->fetchField()) return false;
                
                $result = $qry->execute()->fetchAssoc();
                
                if(array_key_exists('attachment', $result) && $this->tools->isJSON($result['attachment']))
                        $result['attachment'] = json_decode($result['attachment'], true);
                else{
                        $result['attachment'] = [];
                }
                
                $this->template = $result;
                return true;
        }
        
        private function loadGlobalVars(){
                
                if($this->session->has('user') && array_key_exists('id', $this->session->get('user'))){
                        
                        $user = new User($this->session->get('user')['id']);
                        
                        //Sender
                        foreach($user->getKeys() as $key) $this->global_vars["::sender.$key::"] = $user->getElement($key);
                        foreach($user->profile->getKeys() as $key) $this->global_vars["::sender.profile.$key::"] = $user->profile->getElement($key);
                }
                
                foreach($this->workflow->getKeys() as $key)     $this->global_vars["::workflow.$key::"      ] = $this->workflow->getElement($key);
                foreach($this->workflow->getDataKeys() as $key) $this->global_vars["::workflow.data.$key::" ] = $this->workflow->getDataElement($key);
                foreach($this->medium->getKeys() as $key)       $this->global_vars["::medium.$key::"        ] = $this->medium->getElement($key);
                foreach($this->medium->getControlKeys() as $key)$this->global_vars["::medium.control.$key::"] = $this->medium->getControlElement($key);
                
                $creator = new User($this->workflow->getElement('created_by_uid'));
                foreach($creator->getKeys() as $key) $this->global_vars["::creator.$key::"] = $creator->getElement($key);
                foreach($creator->profile->getKeys() as $key) $this->global_vars["::creator.profile.$key::"] = $creator->profile->getElement($key);
                
                $this->global_vars['::lnk.login::'] = $url_login = \Drupal\Core\Url::fromRoute('publisso_gold.login')->setAbsolute()->toString();
                $this->global_vars['::setup.mail_signature_publisso'] = $this->setup->getValue('mail_signature_publisso');
        }
        
        /**
         * @param $recipient
         * @param array $mailout
         * @param array $header
         * @param $attachment
         * @param null $autosend_parent
         * @return \Drupal\Core\Database\StatementInterface|int|string|null
         * @throws \Exception
         */
        private function saveMail2Spooler($recipient, $mailout = [], $header = [], $attachment, $autosend_parent = null){
                
                return \Drupal::database()->insert('rwPubgoldMailout')->fields([
                        'recipient'             => $recipient,
                        'subject'               => $mailout['subject'],
                        'body'                  => $mailout['template'],
                        'header'                => json_encode($header),
                        'ready_to_send'         => 0,
                        'created_by_uid'        => $this->session->get('user')['id'],
                        'attachment'            => $attachment,
                        'autosend_parent'       => $autosend_parent
                ])->execute();
        }
        
        /**
         * @param $to
         * @param $subject
         * @param $body
         * @param array $add_header
         * @param array $attachments
         */
        private function sendMail($to, $subject, $body, $add_header = [], $attachments = []){
        
                $uuid = md5((int)uniqid() * time());
                $mail_system = \Drupal::service('plugin.manager.mail')->getInstance(array('module' => 'publisso_gold', 'key' => $uuid));
        
                $ctype = "text/plain; charset=UTF-8;";
                $boundary =  md5(uniqid(mt_rand(), 1));
        
                if(count($attachments)){
        
                        $eol    = PHP_EOL;
                        $ctype  = "multipart/mixed; boundary=\"$boundary\"";
        
                        $body   = ""
                        ."--$boundary".$eol
                        ."Content-Type: text/plain; charset=UTF-8;$eol"
                        ."Content-Transfer-Encoding: 8BIT$eol$eol"
                        .$body.$eol;
        
                        foreach($attachments as $file){
        
                                $path = DRUPAL_ROOT.$file;
        
                                if(!is_readable($path)){
                                        Publisso::log("File \"$path\" is not readable!");
                                        continue;
                                }
        
                                $type = mime_content_type($path);
                                $name = basename($path);
                                $cont = chunk_split(base64_encode(file_get_contents($path)));
                                $size = filesize($path);
        
                                $body .= "--$boundary$eol";
                                $body .= "Content-Type: $type; name=\"$name\"$eol";
                                $body .= "Content-Disposition: attachment; filename=\"$name\";$eol";
                                $body .= "Content-Length: $size$eol";
                                $body .= "Content-Transfer-Encoding: base64$eol$eol";
                                $body .= $cont;
                        }
        
                        $body .= "$eol--$boundary--";
                }
                
                $params = [
                        'headers' => [
                                'MIME-Version'                  => '1.0',
                                'Content-Type'                  => $ctype,
                                'Content-Transfer-Encoding'     => '8BIT',
                                'X-Mailer'                      => 'Publisso Mailer',
                                'From'                          => 'PUBLISSO <livingbooks@zbmed.de>'
                        ],
                        'to'      => $to,
                        'body'    => ($body),
                        'subject' => $subject
                ];

                if(!empty($add_header['cc']))
                        $params['cc'] = $add_header['cc'];

                if(!empty($add_header['bcc']))
                        $params['bcc'] = $add_header['bcc'];

                foreach($add_header as $_k => $_v){
                        $params['headers'][$_k] = $_v;
                }
                
                $mail_system->mail($params);
        }
        
        //alias for ::send
        
        /**
         * @param bool $spool
         * @throws \Exception
         */
        public function submit(bool $spool = false){
                $this->send($spool);
        }
        
        /**
         * @param bool $spool
         * @throws \Exception
         */
        public function send(bool $spool = false){ //Für den Produktivbetrieb wieder auf "false" setzen
                
                foreach($this->recipients as $index => $_){
                        
                        $rVars = [];
                        if(array_key_exists('recipient', $this->recipient_vars)){
                                if(array_key_exists($index, $this->recipient_vars['recipient'])){
                                        $rVars = $this->recipient_vars['recipient'][$index];
                                }
                        }
                        
                        $rVars = array_merge($rVars, $this->global_vars, $this->additional_vars);
                        
                        //set recipient-depending variables

                        $_mail = $this->template;
                        
                        $_mail['template'] = str_replace(array_keys($rVars), array_values($rVars), $_mail['template']);
                        $_mail['subject'] = str_replace(array_keys($rVars), array_values($rVars), $_mail['subject']);
                        $_mail['template'] = str_replace(array_keys($rVars), array_values($rVars), $_mail['template']);
                        
                        Publisso::log('Send "'.$this->process.'" - Mail to '.$_.'(CC: '.implode('; ', $this->cc ?? []).' | BCC: '.implode('; ', $this->bcc ?? []).')');
                        
                        if(!$spool)
                                $this->sendMail($_, $_mail['subject'], $_mail['template'], ['CC' => implode(',', $this->cc ?? []), 'BCC' => implode(',', $this->bcc ?? [])], $_mail['attachment']);
                        else
                                $spooler_id = $this->saveMail2Spooler($_, $_mail, ['CC' => implode(',', $this->cc ?? []), 'BCC' => implode(',', $this->bcc ?? [])], json_encode($_mail['attachment']));

                        //save Mail in getWorkflowHistory
                        $data = [
                                'wf_id'                 => $this->workflow->getElement('id'),
                                'wf_type'               => $this->workflow->getElement('type'),
                                'wf_state'              => $this->workflow->getDataElement('state'),
                                'wf_data'               => $this->workflow->getElement('data'),
                                'mail_from'             => 'PUBLISSO <livingbooks@zbmed.de>',
                                'mail_to'               => $_,
                                'mail_cc'               => serialize($this->cc),
                                'mail_bcc'              => serialize($this->bcc),
                                'mail_text'             => $_mail['template'],
                                'mail_attachment'       => serialize($_mail['attachment']),
                                'mail_autosend'         => $spool ? 0 : 1,
                                'init_uid'              => $this->session->get('user')['id']
                        ];
                        
                        
                        
                        $insert = \Drupal::database()->insert('rwPubgoldWorkflowMailHistory');

                        if($spooler_id) $data['spooler_id'] = $spooler_id;
                        else $data['mail_sent'] = date('Y-m-d H:i:s');
                        
                        $insert->fields($data);
                        $insert->execute();
                }
        }
}

?>
