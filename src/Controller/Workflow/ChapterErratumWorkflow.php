<?php
        /**
         * Created by PhpStorm.
         * User: sseliger
         * Date: 11.02.19
         * Time: 11:12
         */

        namespace Drupal\publisso_gold\Controller\Workflow;


        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Bookchapter;
        use Drupal\publisso_gold\Controller\TableOfContent;
        use Drupal\publisso_gold\Controller\TableOfContentItem;

        /**
         * Class ChapterErratumWorkflow
         * @package Drupal\publisso_gold\Controller\Workflow
         */
        class ChapterErratumWorkflow extends Workflow {

                /**
                 * ChapterErratumWorkflow constructor.
                 * @param null $id
                 * @param string $tbl_column_prefix
                 * @param bool $loadMedium
                 */
                public function __construct($id = null, $tbl_column_prefix = 'wf_', $loadMedium = true) {

                        $this->session           = \Drupal::service('session');
                        $this->medium            = null;
                        $this->stateChain        = null;
                        $this->reviewpages       = $this->history = $this->editorial_office = $this->editors = $this->editors_in_chief = $this->reviewers = $this->elements = [];
                        $this->data              = (object)[];
                        $this->tbl_column_prefix = $tbl_column_prefix;
                        $this->type              = 'bce';

                        if ($id !== null) {

                                if ($tbl_column_prefix == 'wf_')
                                        $this->db_table = 'rwPubgoldWorkflow';
                                elseif ($tbl_column_prefix == 'wfh_')
                                        $this->db_table = 'rwPubgoldWorkflowHistory';

                                $this->id = $id;
                                $this->load($this->id);
                        } else {
                                $this->db_table = 'rwPubgoldWorkflow';
                        }
                }

                /**
                 * @param bool $sendInfoMail
                 * @param false $preliminary
                 * @return \Drupal\Core\Database\StatementInterface|false|int|string|null
                 * @throws \Exception
                 */
                public function publishErratum($sendInfoMail = true, $preliminary = false) {

                        if (!$this->id) return null;
                        if (!$this->medium) $this->loadMedium();

                        $oldChapter = new Bookchapter($this->getDataElement('previous_version'));
                        if (!$oldChapter->getElement('id')) return false;

                        $data = [
                                'cp_abstract' => $this->getDataElement('abstract'),
                                'cp_author_uid' => $oldChapter->getElement('author_uid'),
                                'cp_bkid' => $this->getDataElement('bk_id'),
                                'cp_chapter_text' => $this->getDataElement('chapter_text'),
                                'cp_correction' => $this->getDataElement('correction'),
                                'cp_corresponding_author' => $this->getDataElement('corresponding_authors'),
                                'cp_ddc' => $oldChapter->getElement('ddc'),
                                'cp_doi' => $this->getDataElement('doi'),
                                'cp_errata' => $this->getDataElement('erratum'),
                                'cp_funding_id' => $this->getDataElement('funcing_id'),
                                'cp_funding_number' => $this->getDataElement('funding_number'),
                                'cp_funding_name' => $this->getDataElement('funding_name'),
                                'cp_keywords' => $this->getDataElement('keywords'),
                                'cp_license' => $this->getDataElement('license'),
                                'cp_publication_place' => $this->getDataElement('publication_place'),
                                'cp_publication_year' => $this->getDataElement('publication_year'),
                                'cp_publisher' => $oldChapter->getElement('publisher'),
                                'cp_title' => $this->getDataElement('title'),
                                'cp_urn' => $oldChapter->getElement('urn'),
                                'cp_version' => $this->getDataElement('version'),
                                'cp_patients_rights' => $oldChapter->getElement('patients_rights'),
                                'cp_author_contract' => $oldChapter->getElement('author_contract'),
                                'cp_conflict_of_interest' => $this->getDataElement('conflict_of_interest'),
                                'cp_conflict_of_interest_text' => $this->getDataElement('conflict_of_interest_text'),
                                'cp_corporation' => $this->getDataElement('corporations'),
                                'cp_sub_category' => $this->getDataElement('sub_category'),
                                'cp_more_authors' => $oldChapter->getElement('more_authors'),
                                'cp_wfid' => $this->getElement('id'),
                                'cp_authors' => $this->getDataElement('authors'),
                                'cp_policy_accept' => $this->getDataElement('accept_policy'),
                                'cp_published' => $this->getDataElement('published') ? $this->getDataElement('published') : date("Y-m-d"),
                                'cp_files' => $oldChapter->getElement('files'),
                                'cp_previous_version' => $oldChapter->getElement('id'),
                                'cp_title_short' => $this->getDataElement('title_short'),
                                'cp_received' => $this->getDataElement('received') ?? null,
                                'cp_revised' => $this->getDataElement('revised') ?? null,
                                'cp_accepted' => $this->getDataElement('accepted') ?? null
                        ];

                        if (!$this->getDataElement('cp_id')) {
                                $cp_id = \Drupal::database()->insert('rwPubgoldBookChapters')->fields($data)->execute();
                                $this->setDataElement('cp_id', $cp_id);
                        } else {
                                $cp_id = $this->getDataElement('cp_id');
                                \Drupal::database()->update('rwPubgoldBookChapters')->fields($data)->condition('cp_id', $cp_id, '=')->execute();
                        }

                        if (!$preliminary) {

                                $oldTociLink = "intern://chapter/" . $oldChapter->getElement('id');
                                $toci_id = \Drupal::database()->select('rwPubgoldTableOfContentItem', 't')->fields('t', ['toci_id'])->condition('toci_link', $oldTociLink, '=')->execute()->fetchField();

                                if ($toci_id) {
                                        //recreate structure
                                        $newTociLink = "intern://chapter/$cp_id";
                                        \Drupal::database()->update('rwPubgoldTableOfContentItem')->fields(['toci_link' => $newTociLink])->condition('toci_link', $oldTociLink, '=')->execute();
                                        $toc_item = new TableOfContentItem($toci_id);
                                        $toc = new TableOfContent($toc_item->getElement('tocid'));
                                        $toc->recreateStructure();
                                }
                                $oldChapter->setElement('next_version', $cp_id);
                                $oldChapter->setElement('pending_erratum', 0);
                                $this->setDataElement('state', 'published');

                                $vars = [
                                        '::lnk.submission::' => Url::fromRoute('publisso_gold.book.chapter', ['bk_id' => $this->medium->getElement('id'), 'cp_id' => $cp_id])->setAbsolute()->toString()
                                ];

                                if ($sendInfoMail == true) {

                                        $mail = new WorkflowInfoMail($this, 'erratum publish submission');
                                        $mail->addVars($vars);
                                        if ($mail) $mail->send();

                                        $mail = new WorkflowInfoMail($this, 'erratum publication info eic');
                                        $mail->addVars($vars);
                                        if ($mail) $mail->send();
                                }
                        }

                        if ($this->getElement('state') == 'published') {

                                $chapter = new Bookchapter($cp_id);
                                $urlChapter = $chapter->getChapterLink('url');
                                $urlChapter->setAbsolute();
                                \Drupal::service('messenger')->addMessage(
                                        new FormattableMarkup(
                                                (string)t('The chapter has been published successfully. You can see the chapter by visiting <a href="@link">@link</a>'),
                                                ['@link' => $urlChapter->toString()]
                                        )
                                );
                        }
                        return $cp_id;
                }
        }
