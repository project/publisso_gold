<?php
        namespace Drupal\publisso_gold\Theme;
        
        use Drupal\Core\Theme\ThemeNegotiatorInterface;
        use Drupal\Core\Routing\RouteMatchInterface;
        use Drupal\Core\Config\ConfigFactory;
        
        /**
         * Class ThemeNegotiator
         * @package Drupal\publisso_gold\Theme
         */
        class ThemeNegotiator implements ThemeNegotiatorInterface {
                
                /**
                 * The config factory.
                 *
                 * @var \Drupal\Core\Config\ConfigFactoryInterface
                 */
                protected $configFactory;
                
                public function __construct ( \Drupal\Core\Config\ConfigFactoryInterface $config_factory ) {
                        
                        $this->configFactory = $config_factory;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function applies ( RouteMatchInterface $route_match ) {
                        // Use this theme on a certain route.
                        // return $route_match->getRouteName() == 'example_route_name';
                        
                        // Or use this for more than one route:
                        $possible_routes = [
                                '^publisso_gold\.books_book(\..+)*$', //gilt nur für Bücher
                                '^publisso_gold\.book\.chapter$'
                        ];
                        
                        $ret = false;
                        foreach($possible_routes as $possible_route){
                                if(preg_match("/$possible_route/", $route_match->getRouteName())){
                                        $ret = true;
                                        break;
                                }
                        }
                        
                        return $ret;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function determineActiveTheme ( RouteMatchInterface $route_match ) {
                        // Here you return the actual theme name.
                        
                        $paramBag = $route_match->getParameters();
                        
                        //for books
                        if ( $paramBag->has( 'bk_id' ) ) {
                                
                                $medium = new \Drupal\publisso_gold\Controller\Book( $paramBag->get( 'bk_id' ) );
                                if ( $medium && $medium->getElement( 'theme' ) ) {
                                        return $medium->getElement( 'theme' );
                                        
                                }
                                else {
                                        return \Drupal::service( 'publisso_gold.setup' )->getValue( 'books.default_theme' );
                                }
                        }
                        
                        return 'publisso';
                }
                
        }

?>
