<?php
        
        
        namespace Drupal\publisso_gold\Views;
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Link;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\TemplateManager;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Symfony\Component\HttpFoundation\RequestStack;

        /**
         * Class ConferencesOverview
         * @package Drupal\publisso_gold\Views
         */
        class ConferencesOverview extends ControllerBase {
                
                protected $requestStack;
                protected $filterParams;
                protected $tmplVarConferences;
                protected $varsLoaded;
                
                public function __construct(RequestStack $requestStack){
                        $this->requestStack = $requestStack;
                        $this->filterParams = $this->requestStack->getCurrentRequest()->query->all();
                        $this->tmplVarConferences = [];
                        $this->varsLoaded = false;
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return ConferencesOverview
                 */
                public static function create(ContainerInterface $container){
                        return new self($container->get('request_stack'));
                }
        
                /**
                 * @return array
                 */
                public function main(){
                        
                        $this->loadVars();
                        
                        $filter = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('Status:'),
                                '#attributes' => [
                                        'style' => [
                                        
                                        ],
                                        'onchange' => [
                                                'javascript: var key = \'status\'; var s = new URLSearchParams(location.search); this.value ? s.set(key, this.value) : s.delete(key); location.search = s.toString() ? \'?\' + s.toString() : \'\';'
                                        ]
                                ],
                                '#options' => [
                                        '' => (string)$this->t('All'),
                                        'published' => (string)$this->t('Only published'),
                                        'submission_possible' => (string)$this->t('Submission possible'),
                                        'submission_closed' => (string)$this->t('Submission closed')
                                ],
                                '#value' => $this->filterParams['status'] ?? null,
                                '#prefix' => '<div class="col-sm-4">',
                                '#suffix' => '</div>'
                        ];
        
                        $filter1 = [
                                '#type' => 'textfield',
                                '#title' => (string)$this->t('Title contains:'),
                                '#attributes' => [
                                        'style' => [
                        
                                        ],
                                        'onchange' => [
                                                'javascript: var key = \'title_contains\'; var s = new URLSearchParams(location.search); this.value ? s.set(key, this.value) : s.delete(key); location.search = s.toString() ? \'?\' + s.toString() : \'\';'
                                        ]
                                ],
                                '#value' => $this->filterParams['title_contains'] ?? null,
                                '#prefix' => '<div class="col-sm-4">',
                                '#suffix' => '</div>'
                        ];
        
                        $filter2 = [
                                '#type' => 'textfield',
                                '#title' => (string)$this->t('Inviting society contains:'),
                                '#attributes' => [
                                        'style' => [
                        
                                        ],
                                        'onchange' => [
                                                'javascript: var key = \'society_contains\'; var s = new URLSearchParams(location.search); this.value ? s.set(key, this.value) : s.delete(key); location.search = s.toString() ? \'?\' + s.toString() : \'\';'
                                        ]
                                ],
                                '#value' => $this->filterParams['society_contains'] ?? null,
                                '#prefix' => '<div class="col-sm-4">',
                                '#suffix' => '</div>'
                        ];
                        
                        return [
                                'filter' => [
                                        '#type' => 'fieldset',
                                        '#collapsible' => true,
                                        '#collapsed' => !count($this->filterParams),
                                        '#title' => (string)$this->t('Filter'),
                                        '#prefix' => '<div class="col-sm-12">',
                                        '#suffix' => '</div><br clear="all">',
                                        'filter1' => $filter,
                                        'filter2' => $filter1,
                                        'filter3' => $filter2
                                ],
                                'content' =>[
                                        '#conferences' => $this->tmplVarConferences,
                                        '#theme' => 'public_conference_overview'
                                ],
                                '#cache' => ['max-age' => 0],
                                'pager' => ['#type' => 'pager']
                        ];
                }
        
                private function loadVars(){
                
                        if(!$this->varsLoaded) {
                        
                                $conference = [];
                        
                                //load the conference
                                foreach(ConferenceManager::getConferences($this->filterParams, 20) as $_){
                                        
                                        $conference = ConferenceManager::getConference($_);
        
                                        foreach ( $conference->getElementKeys() as $key ) {
                                                $this->tmplVarConferences[ $_ ][$key] = $conference->getElement( $key );
                                        }
        
                                        $this->tmplVarConferences[$_]['urlLogo'] = Url::fromRoute('publisso_gold.getPicture', ['type' => 'ul', 'id' => $conference->getElement('logo') ?? 0]);
                                        $this->tmplVarConferences[$_]['headline'] = Link::fromTextAndUrl($conference->getElement('title'), $conference->getUrl());
                                }
                                
                                $this->varsLoaded = true;
                        }
                }
        }
