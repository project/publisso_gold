<?php
        
        namespace Drupal\publisso_gold\Views;
        
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Link;
        use Drupal\Core\Render\Markup;
        use Drupal\Core\Render\Renderer;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Bookchapter;
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\TemplateManager;
        use Drupal\publisso_gold\Controller\TableOfContent;
        use Drupal\publisso_gold\Controller\Template;
        use Zend\Feed\Uri;

        /**
         * Class Book
         * @package Drupal\publisso_gold\Views
         */
        class Book extends ControllerBase {
        
                /**
                 * @param $bk_id
                 * @param null $tab
                 * @return array
                 */
                public function getBookHTML($bk_id, $tab = null) :array{
                        
                        $book = BookManager::getBook($bk_id);
                        $header = $this->getBookHead($book);
                        $body = $this->getBookBody($book, $tab);
                        
                        return [
                                'header' => $header,
                                'body' => $body,
                                '#cache' => ['max-age' => 0]
                        ];
                }
        
                /**
                 * @param $bk_id
                 * @param null $tab
                 * @return string
                 */
                public function getBookTitle($bk_id, $tab = null) :string{
                        
                        if(!$tab || strtolower($tab) == 'about') {
                                if(BookManager::bookExists($bk_id)) {
                                        $book = BookManager::getBook( $bk_id );
                                        $title = $book->getElement( 'title' );
                                }
                                else{
                                        $title = 'n.a.';
                                }
                        }
                        else{
                                $tabs = BookManager::getHTMLViewTabs($bk_id);
                                
                                $title = (string)$this->t($tabs[strtolower($tab)]['title']);
                        }
                        
                        return $title;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Book $book
                 * @param $tab
                 * @param array $addTabs
                 * @return array
                 */
                protected function getBookBody(\Drupal\publisso_gold\Controller\Medium\Book $book, $tab, $addTabs = []) :array{
        
                        $templateBody = TemplateManager::load($book->getElement('theme').'_book_body');
                        if(!$templateBody->isLoaded()) return [];
                        
                        $this->setBookTabs($book, $templateBody, $tab, $addTabs);
                        $this->setBookContent($book, $templateBody, $tab);
                        
                        return [
                                '#type' => 'inline_template',
                                '#template' => $templateBody->parse()
                        ];
                }
                
                protected function getBookHead(\Drupal\publisso_gold\Controller\Medium\Book $book) :array{
        
                        $templateHead = TemplateManager::load($book->getElement('theme').'_book_header');
                        if(!$templateHead->isLoaded()) return [];
        
                        $templateHead->setVar('bk_id', $book->getElement('id'));
        
                        foreach($book->getElementKeys() as $key){
                                $templateHead->setVar($key, $book->getElement($key));
                        }
        
                        $lnkCover = Url::fromRoute('publisso_gold.getPicture', ['type' => 'bc', 'id' => $book->getElement('id')]);
                        $templateHead->setVar('lnkCover', $lnkCover->toString());
        
                        $issbn = [];
                        foreach(['issn', 'isbn'] as $_){
                                if(!empty($book->getElement($_))) $issbn[] = $book->getElement($_);
                        }
        
                        if(count($issbn)) $templateHead->setVar('issn_isbn', '<p>'.implode('<br>', $issbn).'</p>');
        
                        //setzen der Links
                        switch ( $book->getElement( 'theme' ) ) {
                
                                case 'gms':
                                        $linkClasses = [ 'btnPublissoDefault' ];
                                        break;
                
                                default:
                                        $linkClasses = [ 'link--calltoaction', 'inline', 'btn' ];
                        }
                        
                        if(!count($book->getTemplates()))
                                $urlPublish = Url::fromRoute( 'publisso_gold.book.addchapter', ['bk_id' => $book->getElement('id')], ['attributes' => ['class' => $linkClasses]]);
                        else
                                $urlPublish = Url::fromRoute('publisso_gold.directsubmit', ['book' => $book->getElement('id')], ['attributes' => ['class' => $linkClasses]]);
        
                        if(\Drupal::service('session')->get('logged_in') === true) {
                
                                $uid = \Drupal::service('session')->get('user')['id'];
                                $weight = \Drupal::service('session')->get('user')['weight'];
                                
                                if($urlPublish->access()){
                                #if($book->hasAuthor($uid) || $book->countAuthors() == 0 || $weight >= 70) {
                                        $templateHead->setVar( 'lnk_publish_chapter', Link::fromTextAndUrl( Markup::create( '<span>' . (string)$this->t( 'Submit' ) . '</span>' ), $urlPublish)->toString());
                                }
                        }
        
                        if ( \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'bookmanagement' ) ) {
                
                                $urlEdit = Url::fromRoute( 'publisso_gold.book.edit', [ 'bk_id' => $book->getElement('id') ], [ 'attributes' => [ 'class' => $linkClasses ] ] );
                                $urlEditEB = Url::fromRoute( 'publisso_gold.bookmanagement_seteditorialboard', [ 'bk_id' => $book->getElement('id') ], [ 'attributes' => [ 'class' => $linkClasses ] ] );
                
                                $templateHead->setVar( 'lnk_edit_book', Link::fromTextAndUrl( Markup::create( '<span>' . (string)$this->t( 'Edit book' ) . '</span>' ), $urlEdit)->toString());
                                $templateHead->setVar( 'lnk_edit_book_eb', Link::fromTextAndUrl( Markup::create( '<span>' . (string)$this->t( 'Edit edit. board' ) . '</span>' ), $urlEditEB)->toString());
                        }
                        
                        return [
                                '#type' => 'inline_template',
                                '#template' => $templateHead->parse()
                        ];
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Book $book
                 * @param Template $templateBody
                 * @param null $tab
                 * @param array $addTabs
                 * @param array $addRouteParams
                 * @param string $routePrefix
                 * @return bool
                 */
                protected function setBookTabs(\Drupal\publisso_gold\Controller\Medium\Book $book, Template &$templateBody, $tab = null, $addTabs = [], array $addRouteParams = [], $routePrefix = 'publisso_gold.books_book') :bool{
        
                        $templateBody->setVar('tabs', '');
                        $tabs = BookManager::getHTMLViewTabs($book->getElement('id'));
                        
                        if(count($addTabs)){
                                $tabs = array_merge($tabs, $addTabs);
                        }
        
                        $parameters = ['bk_id' => $book->getElement('id')];
                        
                        if(count($addRouteParams)){
                                $parameters = array_merge($parameters, $addRouteParams);
                        }
                        
                        uasort($tabs, ['self', 'sortTabs']);
                        
                        $templateTab = TemplateManager::load($book->getElement('theme').'_book_body_tab');
        
                        foreach($tabs as $tabName => $tabData){
                                
                                $routeName = $routePrefix.'.'.strtolower($tabName);
                                
                                if(array_key_exists('route_parameters', $tabData) && is_array($tabData['route_parameters']))
                                        $parameters = array_merge($parameters, $tabData['route_parameters']);
                                
                                try {
                                        $url = Url::fromRoute( $routeName, $parameters );
                                        $link = Link::fromTextAndUrl( (string)$this->t( $tabData[ 'title' ] ), $url );
                                        $link = $link->toString();
                                }
                                catch(\Exception $e){
                                        error_log('ERROR: route "'.$routeName.'" not found!');
                                        $link = $tabData['title'];
                                }
                                
                                $templateTab->clear();
                                $templateTab->setVar('lnkTab', $link);
                                
                                if($tab == $tabData['title'] || strtolower($tab) == $tabName)
                                        $templateTab->setVar('tabClasses', 'active');
                                
                                $templateBody->appendToVar('tabs', $templateTab->parse());
                        }
                        
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Book $book
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setBookContent(\Drupal\publisso_gold\Controller\Medium\Book $book, Template &$templateBody, $tab = null) :bool{
                        
                        $method = 'setBookContent_'.str_replace([' ', '.', '-'], '_', strtolower($tab));
                        
                        try{
                                $this->$method($book, $templateBody, $tab = null);
                        }
                        catch(\Error $e){
                                \Drupal::service('messenger')->addError('ERROR: Can\'t load all parts for this view. Please contact support!');
                                error_log('Error in file '.$e->getFile().' line '.$e->getLine().': '.$e->getMessage());
                                return false;
                        }
                        
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Book $book
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setBookContent_about(\Drupal\publisso_gold\Controller\Medium\Book $book, Template &$templateBody, $tab = null) :bool{
                        
                        $templateBody->setVar('content', base64_decode($book->getElement('about')));
                        $this->setBookContent_Sidebars($book, $templateBody);
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Book $book
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setBookContent_media(\Drupal\publisso_gold\Controller\Medium\Book $book, Template &$templateBody, $tab = null) :bool{
                
                        $templateBody->setVar('content', base64_decode($book->getElement('media')));
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Book $book
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setBookContent_manuscript_guidelines(\Drupal\publisso_gold\Controller\Medium\Book $book, Template &$templateBody, $tab = null) :bool{
                
                        $templateBody->setVar('content', base64_decode($book->getElement('manuscript_guidelines')));
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Book $book
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setBookContent_imprint(\Drupal\publisso_gold\Controller\Medium\Book $book, Template &$templateBody, $tab = null) :bool{
                
                        $templateBody->setVar('content', base64_decode($book->getElement('imprint')));
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Book $book
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setBookContent_overview_chapters(\Drupal\publisso_gold\Controller\Medium\Book $book, Template &$templateBody, $tab = null) :bool{
        
                        $toc = new TableOfContent(false, ['medium' => 'book', 'id' => $book->getElement('id')], false);
                        $templateBody->setVar('content', '<div id="chapter-list">'.$this->parseChapterlistStructure( $toc->readStructure(), 1).'</div>');
                        return true;
                }
        
                protected function setBookContent_Sidebars(\Drupal\publisso_gold\Controller\Medium\Book $book, Template &$templateBody) :bool{
                        
                        $texts          = \Drupal::service('publisso_gold.texts');
                        $theme          = strtolower($book->getElement('theme'));
                        $renderer       = \Drupal::service('renderer');
        
                        $_tmpl = TemplateManager::load($book->getElement('theme').'_book_content_sidebar_block');
                        
                        if(strtolower($book->getElement('theme')) == 'gms'){
                
                                $_tmpl->setVar('sbb_title', (string)$this->t('Search'));
                                $_tmpl->setVar('sbb_content', '');
                                $_tmpl->setVar('view_name', 'partner-list');
                                $_tmpl->appendToVar('sbb_title', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAAbCAYAAACKlipAAAADlElEQVRogeWZMW6zQBCFfZEUKXyN3MRXyAnc+hiOI1G7p0C+ABIN5RZuoKJCQrTzVw89P+8CxjHwJyOt5GC8zM6382bYbGxlVjft0i4sapulHTAzu5ZVB4I//0VbHEjdtLZ9f7Pt+5vtDmfLXfGnoawKyPb9zZLU2bWs7FpWS7u2iK0WSO6KP5klqwMSxZkXSN20lqTOojizKM46cL8N2qqBQLZyV9jH57d37A7n7je/wVYLBLv/WlZBGDr2x8t/D+alQMbISR+Qa1nZ/njpMmEsmCjOXiZlr5bIzf54sd3hfDf2x4slqZs8MeaN4qz3vj4guStuQGA+QPJJF3/3jP8+S1LXxedVYDa6YN2J++Nl0sQIzKNATl+nIBAE2Cdj+K5u2pvrPylhSeq6eV8OZHc4d7qtgZiyqFcAyV1xN78GqG7a4G+etdmBhB7OO9P3fqBdkdktEG5Zk9TdtbNjgKhMcJaoNDEs3UzXsuqkDbIcCu61rCxJXTd43kWBIHOGAsBzDBVj3rkfn9+DQHzSiWdocNh3fk4UZ8HWmcHVTeutU7yeWYBgl/OCEIhngPC8DAiLGgPEV6BzV3ivc/Yg0AwDWaFdHPxR3/XeWYBoUUfXwto8BYjWEN/uZSC4rkB4B/tqmkqp/g5/qz9cc4aUYEwNQQ1Gt8jxg1zyyTY3I1Gc3WZI3bTd8D3oJ4BwADAPA8HIXXEDRIOtht2sz9cgo6XnweviUwEFPwQEPptZd8SDwON67opOdVCX2OdgDVGbGwikCwGEQY44+L5rCB6AcC1iSeSXUOzuUIc2BIR3P87aEAvOCi4DiA8an7sMGQNEH+CDOkay4PDp69QNPfnVOXBNd5beB0gqWb51siooPL6X65AvVnw/B1r94szhz3XTjgfCi+LU1w5Eg4RAoe3laz5DQJAl2gHxvOoDPx/zsFSwrypZnPXsJ56h3WJIsrR2cNAxN75DU4L6YmaPAeFswG+4P1cgIWhjjjS4yUDgojjzniZooPkIRaUt1M6qPGmLjHkVCJ80AwgbWmgE3ecPx33TV8j7wGjfrnPo50f/CwjJAQDtotChhAIcKszqz5BM+9bFf/OLJdp6PXWGrz6pVP8WP37vM0jWmMwNgZnrOJ7hojuccmyzaiBoQ8eaSupcQLROhK6NsVUDeVRKYaz1cwBBnYCsQoqm+L5qIM8YsuUnT3v7DO8R/P4xxX4tELPpGbak/QPYnkh3mayzNQAAAABJRU5ErkJggg=="><br>');
                
                                $form = \Drupal::formBuilder()->getForm('\\Drupal\\publisso_gold\\Form\\PubmedSearch', ['bk_id' => $book->getElement('id')]);
                                $_tmpl->setVar('sbb_content', $renderer->render($form).'');
                
                                $templateBody->appendToVar( 'sidebar', $_tmpl->parse() );
                        }
        
                        if(count($book->readSustainingMembers())){
                
                                $_tmpl->setVar('sbb_title', (string)$this->t( $texts->get( "book.content.sidebar.$theme.sustaining_members.title")));
                                $_tmpl->setVar('sbb_content', '');
                                $_tmpl->setVar('view_name', 'partner-list');
                
                                foreach($book->readSustainingMembers() as $bsm_id){
                        
                                        $urlPicture = Url::fromRoute('publisso_gold.getPicture', [ 'type' => 'bsm', 'id' => $bsm_id ]);
                                        $_tmpl->appendToVar('sbb_content', '<img src="' . $urlPicture->toString().'"><br>');
                                }
                
                                $templateBody->appendToVar('sidebar', $_tmpl->parse());
                        }
                        
                        return true;
                }
        
                /**
                 * @param $structure
                 * @param $level
                 * @return string
                 */
                protected function parseChapterlistStructure ($structure, $level) :string{
                
                        $markup = '';
                
                        if (count( $structure ) > 0 ) {
                        
                                $markup .= '<ul style="list-style: none;">';
                                
                                if (is_array($structure)){
                                        foreach ($structure as $item) {
                                        
                                                $childs = $item['childs'];
                                                $item   = $item['item'];
                                                $link   = null;
                                        
                                                $markup .= '<li>';
                                        
                                                if (!empty($item->getElement('link'))){
                                                
                                                        $link = $item->getElement('link');
                                                
                                                        if(substr($link, 0, 9) == 'intern://') {
                                                        
                                                                $link = str_replace('intern://', '', $link);
                                                                list( $sub_medium, $sub_medium_id ) = explode('/', $link);
                                                        
                                                                $chapter = new Bookchapter($sub_medium_id, true);
                                                                $url = $chapter->getChapterLink('url');
                                                                $link = Link::fromTextAndUrl($item->getElement('title'), $url);
                                                        }
                                                        else{
                                                                if(UrlHelper::isValid($link)){
                                                                        $url = Url::fromUri($link);
                                                                        $url->setOptions(['attributes' => ['target' => '_blank']]);
                                                                        $link = Link::fromTextAndUrl($link, $url);
                                                                }
                                                                else{
                                                                        $link = null;
                                                                }
                                                        }
                                                }
                                        
                                                $markup .= $link ? $link->toString() : $item->getElement('title');
                                        
                                                if($item->getElement('description') != '') {
                                                        $markup .= '<div class="description">' . $item->getElement('description') . '</div>';
                                                }
                                        
                                                if($item->getElement('authors') != '') {
                                                        $markup .= '<div class="authors">' . $item->getElement('authors') . '</div>';
                                                }
                                        
                                                if (count($childs) > 0) {
                                                        $markup .= $this->parseChapterlistStructure($childs, $level + 1);
                                                }
                                        
                                                $markup .= '</li>';
                                        }
                                }
                                $markup .= '</ul>';
                        }
                
                        return $markup;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Book $book
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setBookContent_authors(\Drupal\publisso_gold\Controller\Medium\Book $book, Template &$templateBody, $tab = null) :bool{
        
                        $tmplAuthors            = TemplateManager::load($book->getElement('theme').'_book_content_author');;
                        $content_authors        = '';
                        $c                      = 0;
                        $texts                  = \Drupal::service('publisso_gold.texts');
                        
                        if ( count( $book->readAuthors() ) == 0 ) {
                                $content_authors = '<p>' . ( (string)$this->t( 'Here the authors of the chapters will be listed.' ) ) . '</p>';
                        }
                        else {
                                $authors = [];
                
                                foreach ( $book->readAuthors() as $user ) {
                                        if ( !$user->profile->getElement( 'show_data_in_boards' ) ) continue;
                                        $authors[ $user->profile->getReadableName() ] = $user;
                                }
                
                                ksort( $authors );
                                
                                foreach ( $authors as $user ) {
                                        
                                        $lnkOrcid = null;
                        
                                        if ( !empty( $user->profile->getElement( 'orcid' ) ) ) {
                                
                                                $uri = new Uri( $texts->get( 'book.content.authors_board.lnk.orcid' ) . $user->profile->getElement( 'orcid' ) );
                                
                                                if ( $uri->isValid() ) {
                                                        $urlOrcid = Url::fromUri( $texts->get( 'book.content.authors_board.lnk.orcid' ) . $user->profile->getElement( 'orcid' ), [ 'attributes' => [ 'target' => '_blank' ] ] );
                                                        $lnkOrcid = Link::fromTextAndUrl( $user->profile->getElement( 'orcid' ), $urlOrcid );
                                                        $lnkOrcid = $lnkOrcid->toString();
                                                }
                                                else {
                                                        $lnkOrcid = $user->profile->getElement( 'orcid' );
                                                }
                                        }
                        
                                        $tmplAuthors->clear();
                                        $tmplAuthors->setVar( 'txt_more', (string)$this->t( 'more' ) );
                                        $tmplAuthors->setVar( 'txt_address', (string)$this->t( 'Address' ) );
                                        $tmplAuthors->setVar( 'txt_contact', (string)$this->t( 'Contact' ) );
                                        $tmplAuthors->setVar( 'modal_id', 'm_authors_' . ( $c + 1 ) );
                                        $tmplAuthors->setVar( 'author_name', $user->profile->getElement( 'graduation' ) . ' ' . $user->profile->getElement( 'firstname' ) . ' ' . $user->profile->getElement( 'lastname' ) . ' ' . $user->profile->getElement( 'graduation_suffix' ) );
                                        $tmplAuthors->setVar( 'author_institute', $user->profile->getElement( 'institute' ) );
                                        $tmplAuthors->setVar( 'author_department', !empty( $user->profile->getElement( 'department' ) ) ? '<br>' . $user->profile->getElement( 'department' ) : '' );
                                        $tmplAuthors->setVar( 'author_street', $user->profile->getElement( 'street' ) );
                                        $tmplAuthors->setVar( 'author_postal_code', $user->profile->getElement( 'postal_code' ) );
                                        $tmplAuthors->setVar( 'author_city', $user->profile->getElement( 'city' ) );
                                        $tmplAuthors->setVar( 'author_country', \Drupal::service( 'publisso_gold.tools' )->getCountry( $user->profile->getElement( 'country' ) ) );
                                        $tmplAuthors->setVar( 'author_telephone', $user->profile->getElement( 'telephone' ) );
                                        $tmplAuthors->setVar( 'author_email', $user->profile->getElement( 'email' ) );
                                        $tmplAuthors->setVar( 'author_pic_link', '/system/getPicture/up/' . $user->profile->getLoginId() );
                        
                                        $tmplAuthors->setVar( 'author_address', implode( ', ', [
                                                implode( ' ', [
                                                        $tmplAuthors->getVar( 'author_postal_code' ),
                                                        $tmplAuthors->getVar( 'author_city' )
                                                ]),
                                                $tmplAuthors->getVar( 'author_country' ),
                                                implode( ', ', [
                                                        implode( ' ', [
                                                                $tmplAuthors->getVar( 'author_postal_code' ),
                                                                $tmplAuthors->getVar( 'author_city' )
                                                        ]),
                                                        $tmplAuthors->getVar( 'author_country' )
                                                ])
                                        ]));
                        
                                        $tmplAuthors->setVar( 'author_orcid', $lnkOrcid ? '<hr>' . $texts->get( 'book.content.authors_board.orcid', 'ac' ) . ' ' . $lnkOrcid : '' );
                        
                                        if ( $c % 2 == 0 ) {
                                                $tmplAuthors->prependHTML( '<div class="row flex-row-sm">' );
                                        }else{
                                                $tmplAuthors->appendHTML( '</div><hr>' );
                                        }
                        
                                        $content_authors .= $tmplAuthors->parse();
                                        $c++;
                                }
                        }
        
                        if ( $c > 0 && $c % 2 != 0 ) {
                                $content_authors .= '</div>';
                        }
        
                        $templateBody->setVar( 'content', $content_authors );
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Book $book
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setBookContent_editorial_board(\Drupal\publisso_gold\Controller\Medium\Book $book, Template &$templateBody, $tab = null) :bool{
                        
                        $tmplEB         = TemplateManager::load($book->getElement('theme').'_book_content_author');;
                        $content_eb     = '';
                        $modal_id       = $c = 0;
        
                        $eb = $book->readEditorialBoard();
        
                        $content_eb .= $book->getEbAbAnnotation( 'main' ) ? '<kbd>' . $book->getEbAbAnnotation( 'main' ) . '</kbd>' : '';
        
                        foreach ( $eb as $uid => $user ) {
                
                                $lnkAvatar = Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'up', 'id' => $uid ] );
                                $modal_id++;
                                $tmplEB->clear();
                
                                $tmplEB->setVar( 'txt_more', (string)$this->t( 'more' ) );
                                $tmplEB->setVar( 'txt_address', (string)$this->t( 'Address' ) );
                                $tmplEB->setVar( 'txt_contact', (string)$this->t( 'Contact' ) );
                                $tmplEB->setVar( 'modal_id', 'm_editors_' . ( $modal_id ) );
                                $tmplEB->setVar( 'author_name', implode( ' ', [ $user->profile->getElement( 'graduation' ), $user->profile->getElement( 'firstname' ), $user->profile->getElement( 'lastname' ), $user->profile->getElement( 'graduation_suffix' ) ] ) );
                                $tmplEB->setVar( 'author_institute', $user->profile->getElement( 'institute' ) );
                                $tmplEB->setVar( 'author_department', $user->profile->getElement( 'department' ) );
                                $tmplEB->setVar( 'author_street', $user->profile->getElement( 'street' ) );
                                $tmplEB->setVar( 'author_notes', !empty( $user->tempInfo[ 'eb_notes' ] ) ? $user->tempInfo[ 'eb_notes' ] . '<br><br>' : '' );
                                $tmplEB->setVar( 'author_postal_code', $user->profile->getElement( 'postal_code' ) );
                                $tmplEB->setVar( 'author_city', $user->profile->getElement( 'city' ) );
                                $tmplEB->setVar( 'author_country', \Drupal::service( 'publisso_gold.tools' )->getCountry( $user->profile->getElement( 'country' ) ) );
                                $tmplEB->setVar( 'author_telephone', $user->profile->getElement( 'telephone' ) );
                                $tmplEB->setVar( 'author_email', $user->profile->getElement( 'email' ) );
                                $tmplEB->setVar( 'author_pic_link', $lnkAvatar->toString() );
                                $tmplEB->setVar( 'author_address', implode( ', ', [ implode( ' ', [ $tmplEB->getVar( 'author_postal_code' ), $tmplEB->getVar( 'author_city' ) ] ), $tmplEB->getVar( 'author_country' ) ] ) );
                
                                if ( $c % 2 == 0 ) {
                                        $tmplEB->prependHTML( '<div class="row flex-row-sm">' );
                                }
                                else {
                                        $tmplEB->appendHTML( '</div><hr>' );
                                }
                
                                $content_eb .= $tmplEB->parse();
                                $c++;
                        }
        
                        if ( $c > 0 && $c % 2 != 0 ) {
                                $content_eb .= '</div>';
                        }
        
                        //advisory boards
                        $advisoryBoards = $book->readAdvisoryBoard();
        
                        foreach ( $advisoryBoards as $boardName => $users ) {
                
                                $content_eb .= '<h4 style="text-align: center;"><i>' . $boardName . '</i></h4>';
                                $content_eb .= $book->getEbAbAnnotation( $boardName ) ? '<kbd>' . $book->getEbAbAnnotation( $boardName ) . '</kbd>' : '';
                                $c = 0;
                
                                foreach ( $users as $uid => $user ) {
                        
                                        $lnkAvatar = Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'up', 'id' => $uid ] );
                                        $modal_id++;
                                        $tmplEB->clear();
                        
                                        $tmplEB->setVar( 'txt_more', (string)$this->t( 'more' ) );
                                        $tmplEB->setVar( 'txt_address', (string)$this->t( 'Address' ) );
                                        $tmplEB->setVar( 'txt_contact', (string)$this->t( 'Contact' ) );
                                        $tmplEB->setVar( 'modal_id', 'm_editors_' . ( $modal_id ) );
                                        $tmplEB->setVar( 'author_name', implode( ' ', [ $user->profile->getElement( 'graduation' ), $user->profile->getElement( 'firstname' ), $user->profile->getElement( 'lastname' ), $user->profile->getElement( 'graduation_suffix' ) ] ) );
                                        $tmplEB->setVar( 'author_institute', $user->profile->getElement( 'institute' ) );
                                        $tmplEB->setVar( 'author_department', $user->profile->getElement( 'department' ) );
                                        $tmplEB->setVar( 'author_street', $user->profile->getElement( 'street' ) );
                                        $tmplEB->setVar( 'author_notes', $user->tempInfo[ 'eb_notes' ] . '<br><br>' );
                                        $tmplEB->setVar( 'author_postal_code', $user->profile->getElement( 'postal_code' ) );
                                        $tmplEB->setVar( 'author_city', $user->profile->getElement( 'city' ) );
                                        $tmplEB->setVar( 'author_country', \Drupal::service( 'publisso_gold.tools' )->getCountry( $user->profile->getElement( 'country' ) ) );
                                        $tmplEB->setVar( 'author_telephone', $user->profile->getElement( 'telephone' ) );
                                        $tmplEB->setVar( 'author_email', $user->profile->getElement( 'email' ) );
                                        $tmplEB->setVar( 'author_pic_link', $lnkAvatar->toString() );
                                        $tmplEB->setVar( 'author_address', implode( ', ', [ implode( ' ', [ $tmplEB->getVar( 'author_postal_code' ), $tmplEB->getVar( 'author_city' ) ] ), $tmplEB->getVar( 'author_country' ) ] ) );
                        
                                        if ( $c % 2 == 0 ) {
                                                $tmplEB->prependHTML( '<div class="row flex-row-sm">' );
                                        }
                                        else {
                                                $tmplEB->appendHTML( '</div><hr>' );
                                        }
                        
                                        $content_eb .= $tmplEB->parse();
                                        $c++;
                                }
                
                                if ( $c > 0 && $c % 2 != 0 ) {
                                        $content_eb .= '</div>';
                                }
                        }
        
                        $templateBody->setVar( 'content', $content_eb );
                        return true;
                }
        
                /**
                 * @param array $a
                 * @param array $b
                 * @return int
                 */
                protected function sortTabs(array $a, array $b){
                        return $a['weight'] <=> $b['weight'];
                }
        }
