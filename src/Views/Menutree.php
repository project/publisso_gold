<?php
        
        
        namespace Drupal\publisso_gold\Views;
        
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Menu\MenuTreeParameters;

        /**
         * Class Menutree
         * @package Drupal\publisso_gold\Views
         */
        class Menutree extends ControllerBase {
        
                /**
                 * @return array
                 */
                public function main(){
                        
                        $routeName      = \Drupal::routeMatch()->getRouteName();
                        $routeTitle     = \Drupal::routeMatch()->getRouteObject()->getDefault('_title');
                        $menu_tree      = \Drupal::menuTree();
                        $parameters     = new MenuTreeParameters();
                        $tree           = $menu_tree->load('publisso_gold', $parameters);
                        
                        $manipulators = [
                                ['callable' => 'menu.default_tree_manipulators:checkAccess'],
                                ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
                        ];
                        
                        $tree = $menu_tree->transform($tree, $manipulators);
                        
                        $subtree = $this->getMenuSubtree($routeName, $tree);
                        $menu = $menu_tree->build($subtree);
                        
                        foreach ($menu['#items'] as &$item) {
                                
                                $item['url']->setOption('attributes', ['class' => ['block']]);
                                $links[] = [
                                        '#type' => 'link',
                                        '#title' => $item['title'],
                                        '#url' => $item['url']
                                ];
                        }
                        
                        return [
                                '#type' => 'fieldset',
                                '#title' => (string)$this->t($routeTitle),
                                'content' => $links,
                                '#suffix' => '<br>'
                        ];
                }
        
                /**
                 * @param $routeName
                 * @param $tree
                 * @return mixed|null
                 */
                private function getMenuSubtree($routeName, $tree){
                        
                        $subtree = null;
                        
                        foreach(array_keys($tree) as $key){
                                
                                if(explode(' ', $key)[2] == $routeName) $subtree = $tree[$key]->subtree;
                                
                                if(!$subtree && $tree[$key]->hasChildren){
                                        $subtree = $this->getMenuSubtree($routeName, $tree[$key]->subtree);
                                }
                                
                                if($subtree) break;
                        }
                        
                        return $subtree;
                }
        }
