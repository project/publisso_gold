<?php
        
        
        namespace Drupal\publisso_gold\Views;
        
        use Drupal\Core\Link;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\AbstractManager;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class ConferenceAbstract
         * @package Drupal\publisso_gold\Views
         */
        class ConferenceAbstract extends Conference{
                
                private $conference;
                private $abstract;
                private $tmplVarConference;
                private $tmplVarAbstract;
                private $varsLoaded;
                private $cf_id;
                private $ca_id;
                private $tab;
                protected $currentUser;
                private $workflow;
                
                public function __construct(){
                        
                        $this->varsLoaded = false;
                        $this->cf_id = $this->ca_id = $this->tab = null;
                        $this->currentUser = Publisso::currentUser();
                }
                
                public function getConferenceAbstractHTML(int $cf_id, int $ca_id, string $tab): array{
                        
                        $this->cf_id = $cf_id; $this->ca_id = $ca_id; $this->tab = $tab; $this->loadVars();
                        
                        if(is_null($this->tmplVarAbstract)){
                                return [];
                        }
                        
                        if(!$this->abstract->getElement('published')) {
                                
                                $this->messenger()->addError((string)$this->t('This content has not yet been published'));
                                
                                if(!($this->currentUser->isAdministrator() || $this->currentUser->isPublisso() || $this->workflow->userIsAssigned($this->currentUser->getId()))){
                                        return [];
                                }

                                $workflow = WorkflowManager::getItem($this->abstract->getElement('wfid'));
                                $workflow->transferData2Medium();
                                $this->varsLoaded = false; $this->loadVars();
                        }
                        
                        $tabs = $this->getTabs();
                        $template = $this->getActiveTemplate($tabs);
                        
                        if(null !== ($url = $this->abstract->getPrevious())){
                                $linkPrevious = Link::fromTextAndUrl((string)$this->t('Previous'), $url);
                        }
                        else{
                                $linkPrevious = ['#markup' => (string)$this->t('Previous')];
                        }
        
                        if(null !== ($url = $this->abstract->getNext())){
                                $linkNext = Link::fromTextAndUrl((string)$this->t('Next'), $url);
                        }
                        else{
                                $linkNext = ['#markup' => (string)$this->t('Next')];
                        }
                        
                        return [
                                '#theme'        => 'public_abstract',
                                '#cache'        => ['max-age' => 0],
                                '#conference'   => $this->tmplVarConference,
                                '#abstract'     => $this->tmplVarAbstract,
                                '#tabs'         => $tabs,
                                '#title'        => $this->conference->getElement('title'),
                                '#template'     => $template,
                                '#authors'      => $this->getAuthors(),
                                '#attachments'  => $this->getAttachments('link', true, ['target' => '_blank']),
                                '#sideblocks'   => $this->getSideblocks(),
                                '#linkPrevious' => $linkPrevious ?? null,
                                '#linkNext'     => $linkNext ?? null,
                                '#conferenceTOC'=> [
                                        '#type' => 'inline_template',
                                        '#template' => $this->conference->getSessionTOC()
                                ]
                        ];
                }
                
                public function getConferenceAbstractTitle(int $cf_id, int $ca_id, string $tab) :string{
                        
                        $this->cf_id = $cf_id; $this->ca_id = $ca_id; $this->tab = $tab; $this->loadVars();
                        
                        if($tab != 'Abstract') return (string)$this->t($this->getTabs()[$tab]['title'] ?? 'n.a.');
                        else
                        return (string)$this->abstract->getElement('title') ?? 'n.a.';
                }
                
                private function loadVars(){
                        
                        if(!$this->varsLoaded) {
                                
                                $conference = $abstract = [];
                                
                                //load the conference
                                if($this->cf_id) {
                                        $this->conference = ConferenceManager::getConference( $this->cf_id );
                                        foreach ( $this->conference->getElementKeys() as $key ) {
                                                $this->tmplVarConference[ $key ] = $this->conference->getElement( $key );
                                        }
                                        $this->tmplVarConference['abstractList'] = $this->conference->getSessionsUL()->saveHTML();
                                }
                                
                                //load the abstract
                                if($this->ca_id) {
                                        $this->abstract = AbstractManager::getAbstract( $this->ca_id );
                                        foreach ( $this->abstract->getElementKeys() as $key ) {
                                                $this->tmplVarAbstract[$key] = $this->abstract->getElement( $key );
                                        }
                                }
                                
                                if($this->abstract->getElement('wfid'))
                                        $this->workflow = WorkflowManager::getItem($this->abstract->getElement('wfid'));
                                
                                $this->varsLoaded = true;
                        }
                }
        
                /**
                 * @return array
                 */
                private function getTabs(){
                        
                        $tabs = ConferenceManager::getHTMLViewTabs($this->cf_id);
                        
                        $tabs = array_merge($tabs, [
                                'abstract' => [
                                        'title' => 'Abstract',
                                        'weight' => 8,
                                        'show_on_empty' => 1,
                                        'template' => 'abstract-abstract'
                                ]
                        ]);
                        
                        foreach($tabs as $name => $tab){
                                
                                $param = \Drupal::routeMatch()->getParameters()->all();
                                
                                if($name != 'abstract') $param['tab'] = $name;
                                else unset($param['tab']);
                                
                                $tabs[$name]['url'] = Url::fromRoute(\Drupal::routeMatch()->getRouteName(), $param)->toString();
                                
                                if($this->tab == $tab['title'] || $this->tab == $name){
                                        $tabs[$name]['class'] = 'active';
                                        $tabs[$name]['bgcolor'] = $this->conference->getElement('color');
                                }
                        }
                        
                        return $tabs;
                }
        
                /**
                 * @param $tabs
                 * @return mixed
                 */
                private function getActiveTemplate(&$tabs){
                        
                        $template = null;
                        foreach($tabs as $name => $tab){
                                if(array_key_exists('class', $tab) && $tab['class'] == 'active') return $tab['template'];
                        }
                }
                
                private function getAuthors() :array{
                        
                        $ret = ['authors' => [], 'affiliations' => [], 'corresponding' => [], 'presenting' => []];
                        
                        $authors = array_merge($this->abstract->getElement('authors_db') ?? [], $this->abstract->getElement('authors_add') ?? []);
                        usort($authors, function($a, $b){ return $a['weight'] <=> $b['weight']; });
                        
                        $i = $ai = 1;
                        foreach($authors as $index => $author){
                                
                                $ret['authors'][$i]['author'] = implode(' ', array_filter([$author['firstname'], $author['lastname']]));
                                $ret['authors'][$i]['email'] = $author['email'] ?? '';
                                if(!in_array($author['affiliation'], $ret['affiliations'])) $ret['affiliations'][$ai++] = $author['affiliation'];
                                $ret['authors'][$i]['affiliation'] = array_search($author['affiliation'], $ret['affiliations']);
                                if($author['corresponding']) $ret['corresponding'][] = $i;
                                if($author['presenting']) $ret['presenting'][] = $i;
                                $i++;
                        }
                        
                        foreach($this->abstract->getElement('corporations') ?? [] as $corporation){
                                $ret['authors'][$i]['author'] = $corporation;
                                $i++;
                        }
                        
                        return $ret;
                }
        
                /**
                 * @param string $type
                 * @param false $toString
                 * @param array $attributes
                 * @return array
                 */
                private function getAttachments(string $type = 'id', $toString = false, array $attributes = []){
                        
                        $attachments = [];
                        
                        if(count($this->abstract->getElement('attachments'))){
                                $attachments = $this->abstract->getElement('attachments');
                        }
                        
                        if($type == 'url'){
                                foreach($attachments as $k => $v){
                                        $attachments[$k] = Url::fromRoute('publisso_gold.getPicture', ['type' => 'ul', 'id' => $v], ['attributes' => $attributes]);
                                        if($toString) $attachments[$k] = $attachments[$k]->toString();
                                }
                        }
        
                        if($type == 'link'){
                                $i = 1;
                                foreach($attachments as $k => $v){
                                        $attachments[$k] = Link::fromTextAndUrl((string)$this->t('Attachment #@nr', ['@nr' => $i++]), Url::fromRoute('publisso_gold.getPicture', ['type' => 'ul', 'id' => $v], ['attributes' => $attributes]));
                                        if($toString) $attachments[$k] = $attachments[$k]->toString();
                                }
                        }
                        
                        return $attachments;
                }
        
                /**
                 * @return array[]
                 */
                private function getSideblocks(){
                        
                        $licenseAry = null;
                        if($license = $this->abstract->getLicense()){
                                
                                $licenseAry['title'] = (string)$this->t('License');
                                $licenseAry['content'] = $license->getInlineImage(['width' => '150']);
        
                                $authors = array_merge($this->abstract->getElement('authors_db') ?? [], $this->abstract->getElement('authors_add') ?? []);
                                usort($authors, function($a, $b){ return $a['weight'] <=> $b['weight']; });
                                $authors = array_merge($authors, $this->abstract->getElement('corporations') ?? []);
                                
                                $licenseAry['content'] .= '<br>&copy; '.$authors[0]['lastname'].(count($authors) > 1 ? ' et al.' : '');
                                
                                $licenseAry['content'] .= '<br>'.((string)$this->t('This abstract is distributes under the terms of the license ')).$license->getLink($license->getElement('description', '_blank'));
                                $licenseAry['weight'] = 2;
                        }
                        
                        $ret = [
                                'citation_note' => [
                                        'title' => (string)$this->t('Citation note'),
                                        'content' => $this->abstract->getCitationNote(),
                                        'subcontent' => [
                                                $this->abstract->getElement('doi') ? 'DOI: '.$this->abstract->getDoiLink('link', true, ['target' => '_blank']) : null,
                                                $this->abstract->getElement('published') ? (string)$this->t('Published: @date', ['@date' => $this->abstract->getElement('published')]) : null
                                        ],
                                        'weight' => 1
                                ],
                                'sessions' => [
                                        'title' => (string)$this->t('Sessions'),
                                        'content' => $xml = $this->conference->getSessionsUL($this->abstract->getId(), 30)->saveXML(),
                                        'weight' => 3
                                ]
                        ];
                        
                        if($licenseAry) $ret[] = $licenseAry;
                        usort($ret, function($a, $b){ return $a['weight'] <=> $b['weight']; });
                        
                        return $ret;
                }
        }