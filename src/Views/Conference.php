<?php
        
        namespace Drupal\publisso_gold\Views;
        
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Link;
        use Drupal\Core\Render\Markup;
        use Drupal\Core\Render\Renderer;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Bookchapter;
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\TemplateManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\TableOfContent;
        use Drupal\publisso_gold\Controller\Template;
        use Zend\Feed\Uri;

        /**
         * Class Conference
         * @package Drupal\publisso_gold\Views
         */
        class Conference extends ControllerBase {
        
                /**
                 * @param $cf_id
                 * @param null $tab
                 * @return array
                 */
                public function getConferenceHTML($cf_id, $tab = null) :array{
                        
                        $conference = ConferenceManager::getConference($cf_id);
                        $header = $this->getConferenceHead($conference);
                        $body = $this->getConferenceBody($conference, $tab);
                        
                        return [
                                'header' => $header,
                                'body' => $body,
                                '#cache' => ['max-age' => 0]
                        ];
                }
        
                /**
                 * @param $cf_id
                 * @param null $tab
                 * @return string
                 */
                public function getConferenceTitle($cf_id, $tab = null) :string{
                        
                        if(!$tab || strtolower($tab) == 'welcome') {
                                if(ConferenceManager::conferenceExists($cf_id)) {
                                        $conference = ConferenceManager::getConference( $cf_id );
                                        $title = $conference->getElement( 'title' );
                                }
                                else{
                                        $title = 'n.a.';
                                }
                        }
                        else{
                                $tabs = ConferenceManager::getHTMLViewTabs($cf_id);
                                $title = (string)$this->t($tabs[strtolower($tab)]['title']);
                        }

                        return $title;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Conference $conference
                 * @param $tab
                 * @param array $addTabs
                 * @return array
                 */
                protected function getConferenceBody(\Drupal\publisso_gold\Controller\Medium\Conference $conference, $tab, $addTabs = []) :array{
        
                        $templateBody = TemplateManager::load('conference_body');
                        if(!$templateBody->isLoaded()) return [];
                        
                        $this->setConferenceTabs($conference, $templateBody, $tab, $addTabs);
                        $this->setConferenceContent($conference, $templateBody, $tab);
        
                        foreach($conference->getElementKeys() as $key){
                                $templateBody->setVar($key, $conference->getElement($key));
                        }
                        
                        return [
                                '#type' => 'inline_template',
                                '#template' => $templateBody->parse()
                        ];
                }
                
                protected function getConferenceHead(\Drupal\publisso_gold\Controller\Medium\Conference $conference) :array{
        
                        $templateHead = TemplateManager::load('conference_header');
                        if(!$templateHead->isLoaded()) return [];
        
                        $templateHead->setVar('cf_id', $conference->getElement('id'));
        
                        foreach($conference->getElementKeys() as $key){
                                $templateHead->setVar($key, $conference->getElement($key));
                        }
        
                        $linkClasses = [ 'link--calltoaction', 'inline', 'btn' ];
                        
                        if($conference->submissionPossible()){
                                
                                $color = $conference->getElement('color');
                                $color1 = Publisso::tools()->adjustBrightness($color, -0.25);
                                $style = 'background: linear-gradient(to bottom, '.$color.' 47%, '.$color1.' 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\''.$color.'\', endColorstr=\''.$color1.'\', GradientType=0);';
                                $url = Url::fromRoute('publisso_gold.conference.submit', ['param' => $conference->getId()], ['attributes' => ['class' => $linkClasses, 'style' => $style]]);
                                
                                if($url->access())
                                        $templateHead->setVar('lnkSubmit', Link::fromTextAndUrl( Markup::create( '<span>' . (string)$this->t( 'Submit' ) . '</span>' ), $url)->toString());
                        }
                        
                        $templateHead->setVar('TOC', $conference->getSessionToC());
                        
                        return [
                                '#type' => 'inline_template',
                                '#template' => $templateHead->parse()
                        ];
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Conference $conference
                 * @param Template $templateBody
                 * @param null $tab
                 * @param array $addTabs
                 * @param array $addRouteParams
                 * @param string $routePrefix
                 * @return bool
                 */
                protected function setConferenceTabs(\Drupal\publisso_gold\Controller\Medium\Conference $conference, Template &$templateBody, $tab = null, $addTabs = [], array $addRouteParams = [], $routePrefix = 'publisso_gold.conferences_conference') :bool{
        
                        $templateBody->setVar('tabs', '');
                        $tabs = ConferenceManager::getHTMLViewTabs($conference->getElement('id'));
                        
                        if(count($addTabs)){
                                $tabs = array_merge($tabs, $addTabs);
                        }
        
                        $parameters = ['cf_id' => $conference->getElement('id')];
                        
                        if(count($addRouteParams)){
                                $parameters = array_merge($parameters, $addRouteParams);
                        }
                        
                        uasort($tabs, ['self', 'sortTabs']);
                        
                        $templateTab = TemplateManager::load('conference_body_tab');
        
                        foreach($tabs as $tabName => $tabData){
                                
                                if($tabData['show_on_empty'] == 0){
                                        
                                        $template = TemplateManager::skel();
                                        $this->setConferenceContent($conference, $template, $tabName);
                                        if($template->getVar('content') == '') continue;
                                }
                                
                                $templateTab->clear();
                                $routeName = $routePrefix.'.'.strtolower($tabName);
                                
                                if(array_key_exists('route_parameters', $tabData) && is_array($tabData['route_parameters']))
                                        $parameters = array_merge($parameters, $tabData['route_parameters']);
                                
                                try {
                                        $linkColor = $conference->getElement('color');
                                        $bgColor = '#eeeeee';
                                        if($tab == $tabData['title'] || strtolower($tab) == $tabName) {
                                                $templateTab->setVar( 'tabClasses', 'active' );
                                                $bgColor = $linkColor;
                                                $linkColor = 'white';
                                        }
                                        
                                        $url = Url::fromRoute( $routeName, $parameters, ['attributes' => ['style' => 'color: '.$linkColor.'; background-color: '.$bgColor.'; border-color: '.$bgColor.';']] );
                                        $link = Link::fromTextAndUrl( (string)$this->t( $tabData[ 'title' ] ), $url );
                                        $link = $link->toString();
                                }
                                catch(\Exception $e){
                                        error_log('ERROR: route "'.$routeName.'" not found!');
                                        $link = $tabData['title'];
                                }
                                
                                $templateTab->setVar('lnkTab', $link);
                                
                                $templateBody->appendToVar('tabs', $templateTab->parse());
                        }
                        
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Conference $conference
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setConferenceContent(\Drupal\publisso_gold\Controller\Medium\Conference $conference, Template &$templateBody, $tab = null) :bool{
                        
                        $method = 'setConferenceContent_'.str_replace([' ', '.', '-'], '_', strtolower($tab));
                        
                        try{
                                $this->$method($conference, $templateBody, $tab = null);
                        }
                        catch(\Error $e){
                                \Drupal::service('messenger')->addError('ERROR: Can\'t load all parts for this view. Please contact support!');
                                error_log('Error in file '.$e->getFile().' line '.$e->getLine().': '.$e->getMessage());
                                return false;
                        }
                        
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Conference $conference
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setConferenceContent_welcome(\Drupal\publisso_gold\Controller\Medium\Conference $conference, Template &$templateBody, $tab = null) :bool{
                        
                        $templateBody->setVar('content', $conference->getElement('about'));
                        $this->setConferenceContent_Sidebars($conference, $templateBody);
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Conference $conference
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setConferenceContent_abstracts(\Drupal\publisso_gold\Controller\Medium\Conference $conference, Template &$templateBody, $tab = null) :bool{
                
                        $content = '';
                        
                        $xml = $conference->getSessionsUL();
                        $templateBody->setVar('content', $xml->saveHTML());
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Conference $conference
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setConferenceContent_hosts(\Drupal\publisso_gold\Controller\Medium\Conference $conference, Template &$templateBody, $tab = null) :bool{
                
                        $templateBody->setVar('content', base64_decode($conference->getElement('hosts')));
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Conference $conference
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setConferenceContent_venue(\Drupal\publisso_gold\Controller\Medium\Conference $conference, Template &$templateBody, $tab = null) :bool{
                
                        $templateBody->setVar('content', $conference->getElement('venue'));
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Conference $conference
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setConferenceContent_imprint(\Drupal\publisso_gold\Controller\Medium\Conference $conference, Template &$templateBody, $tab = null) :bool{
        
                        $templateBody->setVar('content', $conference->getElement('imprint'));
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Conference $conference
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setConferenceContent_manuscript_guidelines(\Drupal\publisso_gold\Controller\Medium\Conference $conference, Template &$templateBody, $tab = null) :bool{
                
                        $templateBody->setVar('content', $conference->getElement('manuscript_guidelines'));
                        return true;
                }
                
                protected function setConferenceContent_Sidebars(\Drupal\publisso_gold\Controller\Medium\Conference $conference, Template &$templateBody) :bool{
                        
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Conference $conference
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setConferenceContent_contact(\Drupal\publisso_gold\Controller\Medium\Conference $conference, Template &$templateBody, $tab = null) :bool{
                        
                        $content = '';
                        
                        if(!empty($conference->getElement('venue'))){
                                $content .= '<h2>'.((string)$this->t('Venue')).'</h2>';
                                $content .= $conference->getElement('venue');
                                
                                if(!empty($conference->getElement('venue_info'))){
                                        $content .= '<br>'.nl2br($conference->getElement('venue_info'));
                                }
                        }
        
                        if(!empty($conference->getElement('inviting_society'))){
                                
                                if(!empty($content)) $content .= '<br><br>';
                                
                                $content .= '<h2>'.((string)$this->t('Inviting society')).'</h2>';
                                $content .= $conference->getElement('inviting_society');
                
                                if(!empty($conference->getElement('inviting_society_info'))){
                                        $content .= '<br>'.nl2br($conference->getElement('inviting_society_info'));
                                }
                        }
                        
                        $templateBody->setVar('content', $content);
                        return true;
                }
        
                /**
                 * @param array $a
                 * @param array $b
                 * @return int
                 */
                protected function sortTabs(array $a, array $b){
                        return $a['weight'] <=> $b['weight'];
                }
        }
