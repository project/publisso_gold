<?php
        
        
        namespace Drupal\publisso_gold\Views;
        
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\TemplateManager;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class Bookmanagement
         * @package Drupal\publisso_gold\Views
         */
        class Bookmanagement extends ControllerBase {
        
                /**
                 * @return array
                 */
                public function main () {
        
                        $filter = null;
                        if(\Drupal::requestStack()->getCurrentRequest()->query->has('filter')){
                                $filter = \Drupal::requestStack()->getCurrentRequest()->query->get('filter');
                        }
                        
                        $user = Publisso::currentUser();
                        
                        if($user->getWeight() >= 70){ //alle Bücher für Account >= PUBLISSO
                                $qry = \Drupal::database()->select( 'rwPubgoldBooks', 'b' )->fields( 'b', [ 'bk_id' ] );
                                $pager = $qry->extend( '\Drupal\Core\Database\Query\PagerSelectExtender' )->limit( 10 );
                                $pager->orderBy( 'bk_title', 'ASC' );
                        }
                        else { //Account-bezogene Bücher
                                $qry = \Drupal::database()->select( 'rwPubgoldBooks', 'b' )->fields( 'b', [ 'bk_id' ] );
                                $qry->leftJoin( 'rwPubgoldBookEditors', 'be', 'b.bk_id = be.be_bookid AND be.be_uid = :uid', [ ':uid' => $user->getId() ] );
                                $qry->leftJoin( 'rwPubgoldBookEditorsInChief', 'beic', 'b.bk_id=beic.eic_bkid AND beic.eic_uid = :uid', [ ':uid' => $user->getId() ] );
                                $qry->leftJoin( 'rwPubgoldBookEditorialOffice', 'beo', 'b.bk_id=beo.beo_bkid AND beo.beo_uid = :uid', [ ':uid' => $user->getId() ] );
                                $cond = $qry->orConditionGroup();
                                $cond->condition( 'be.be_uid', $user->getId(), '=' );
                                $cond->condition( 'beic.eic_uid', $user->getId(), '=' );
                                $cond->condition( 'beo.beo_uid', $user->getId(), '=' );
                                $qry->condition( $cond );
                                $pager = $qry->extend( '\Drupal\Core\Database\Query\PagerSelectExtender' )->limit( 10 );
                                $pager->orderBy( 'bk_title', 'ASC' );
                        }
        
                        if($filter) $pager->condition('b.bk_title', "%$filter%", 'LIKE');
                        
                        $urlAddBook = Url::fromRoute('publisso_gold.bookmanagement_add', [], ['attributes' => ['class' => ['btn', 'btn-warning']]]);
        
                        $list = [
                                '#type' => 'table',
                                '#header' => [
                                        [],
                                        ['data' => (string)$this->t('Title')],
                                        ['data' => (string)$this->t('Title Abbr.')],
                                        ['data' => (string)$this->t('Public')],
                                        []
                                ],
                                '#rows' => [],
                                '#empty' => (string)$this->t('No books found...')
                        ];
        
                        foreach($pager->execute()->fetchCol() as $id){
                                
                                $book = BookManager::getBook($id, false);
                                
                                $urlViewBook            = Url::fromRoute('publisso_gold.books_book'                     , ['bk_id' => $id]);
                                $urlEditBook            = Url::fromRoute('publisso_gold.book.edit'                      , ['bk_id' => $id]);
                                $urlAssignRoles         = Url::fromRoute('publisso_gold.bookmanagement_book.roles'      , ['bk_id' => $id]);
                                $urlInviteUser          = Url::fromRoute('publisso_gold.book.invite_user'               , ['bk_id' => $id]);
                                $urlInvitedUsers        = Url::fromRoute('publisso_gold.book.invited_users'             , ['bk_id' => $id]);
                                $urlDelete              = Url::fromRoute('publisso_gold.bookmanagement_book.delete'     , ['bk_id' => $id]);
                                $links = [];
                                $links[] = [
                                        'title' => (string)$this->t('View Book'),
                                        'url' => $urlViewBook
                                ];
                                
                                if($urlEditBook->access()){
                                        $links[] = [
                                                'title' => (string)$this->t('Edit Book'),
                                                'url' => $urlEditBook
                                        ];
                                }
                                
                                if($urlAssignRoles->access()){
                                        $links[] = [
                                                'title' => (string)$this->t('Assign Roles'),
                                                'url' => $urlAssignRoles
                                        ];
                                }
        
                                if($urlInviteUser->access()){
                                        $links[] = [
                                                'title' => (string)$this->t('Invite User'),
                                                'url' => $urlInviteUser
                                        ];
                                }
        
                                if($urlInvitedUsers->access()){
                                        $links[] = [
                                                'title' => (string)$this->t('Invited Users'),
                                                'url' => $urlInvitedUsers
                                        ];
                                }
        
                                if($urlDelete->access()){
                                        $links[] = [
                                                'title' => (string)$this->t('Delete Book'),
                                                'url' => $urlDelete
                                        ];
                                }
                                
                                $list['#rows'][] = [
                                        ['data' => [
                                                '#markup' => '<img src="'.Url::fromRoute('publisso_gold.getPicture', ['type' => 'bc', 'id' => $book->getId()])->toString().'" width="30">'
                                        ]],
                                        ['data' => [
                                                '#markup' => $book->getElement('title')
                                        ]],
                                        ['data' => [
                                                '#markup' => $book->getElement('title_abbr')
                                        ]],
                                        ['data' => [
                                                '#markup' => (string)$this->t($book->getElement('public') ? 'Yes' : 'No')
                                        ]],
                                        ['data' => [
                                                '#type' => 'dropbutton',
                                                '#links' => $links
                                        ], 'width' => '150']
                                ];
                        }
                        
                        return [
                                'filter' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Title contains:'),
                                        '#attributes' => [
                                                'style' => [
                                                        'width: 200px;'
                                                ],
                                                'onkeyup' => [
                                                        'filterManagementMediumList(event, this)'
                                                ]
                                        ],
                                        '#value' => $filter ?? '',
                                        '#description' => (string)$this->t('Hit "Enter" to accept filter')
                                ],
                                'content' => $list,
                                'pager' => [
                                        '#type' => 'pager'
                                ],
                                'separator1' => [
                                        '#markup' => '<hr>'
                                ],
                                'add' => $urlAddBook->access() ? [
                                        '#type' => 'link',
                                        '#title' => (string)$this->t('Add Book'),
                                        '#url' => $urlAddBook
                                ] : [],
                                'separator2' => [
                                        '#markup' => '<hr>'
                                ],
                                '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        }
