<?php
        
        
        namespace Drupal\publisso_gold\Views;
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Language\LanguageManager;
        use Drupal\Core\Link;
        use Drupal\Core\StreamWrapper\StreamWrapperManager;
        use Drupal\Core\Url;
        use Drupal\file\Entity\File;
        use Drupal\media\Entity\Media;
        use Drupal\publisso_gold\Controller\Manager\AbstractManager;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\FunderManager;
        use Drupal\publisso_gold\Controller\Manager\SubmissionFundingManager;
        use Drupal\publisso_gold\Controller\Manager\UserManager;

        /**
         * Class AbstractsOverview
         * @package Drupal\publisso_gold\Views
         */
        class AbstractsOverview extends ControllerBase {
                
                private $conference;
        
                /**
                 * @param int $cf_id
                 * @return array
                 */
                public function main(int $cf_id){
                        
                        $this->conference = ConferenceManager::getConference($cf_id);
                        
                        return [
                                'head' => [
                                        '#markup' => $this->t((string)$this->t('Abstracts for @conference_title', ['@conference_title' => $this->conference->getElement('title')])),
                                        '#prefix' => '<h1>',
                                        '#suffix' => '</h1>'
                                ],
                                'filter' => [],
                                'list' => \Drupal::formBuilder()->getForm('\Drupal\publisso_gold\Form\Conferences\ListAbstracts', ['cf_id' => $cf_id, 'conference' => $this->conference]),
                                'pager' => ['#type' => 'pager']
                        ];
                }
        }