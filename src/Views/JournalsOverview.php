<?php
        
        
        namespace Drupal\publisso_gold\Views;
        
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Link;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\JournalManager;
        use Drupal\publisso_gold\Controller\Manager\TemplateManager;

        /**
         * Class JournalsOverview
         * @package Drupal\publisso_gold\Views
         */
        class JournalsOverview extends ControllerBase {
        
                /**
                 * @return array
                 */
                public function main () {
                        
                        $tmpl = TemplateManager::load('publisso_journals_journallist');
                        $tmpl_link = TemplateManager::load('publisso_journals_journallist_item');
                        
                        $qry = \Drupal::database()->select('rwPubgoldJournals', 'j')->fields('j', ['jrn_id']);
                        $pager = $qry->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit(20);
                        $pager->condition('jrn_public', 1, '=');
                        $pager->orderBy('jrn_title', 'ASC');
                        
                        foreach($pager->execute()->fetchCol() as $id){
                                
                                $tmpl_link->clear();
                                $journal = JournalManager::getJournal($id, true);
        
                                $tmpl_link->setVar('journals_journallist_cover', '<img class="media-object" src="/system/getPicture/ul/0">');
                                if(is_array($journal->getElement('cover')) && count($journal->getElement('cover'))){
                                        $tmpl_link->setVar('journals_journallist_cover', '');
                                        foreach($journal->getElement('cover') as $fid){
                                                $tmpl_link->appendToVar('journals_journallist_cover', (!empty($tmpl_link->getVar('journals_journallist_cover')) ? '<br>' : '').'<img class="media-object" src="/system/getPicture/ul/'.$fid.'">');
                                        }
                                }
        
                                $tmpl_link->setVar('journals_journallist_logo', '<img class="media-object" src="/system/getPicture/ul/0">');
                                if(is_array($journal->getElement('logo')) && count($journal->getElement('logo'))){
                                        $tmpl_link->setVar('journals_journallist_logo', '');
                                        foreach($journal->getElement('logo') as $fid){
                                                $tmpl_link->appendToVar('journals_journallist_logo', (!empty($tmpl_link->getVar('journals_journallist_logo')) ? '<br>' : '').'<img class="media-object" src="/system/getPicture/ul/'.$fid.'">');
                                        }
                                }
        
                                $tmpl_link->setVar( 'journals_journallist_link', $journal->getElement( 'title' ) );
                                $tmpl_link->setVar( 'lnk_view_journal', Link::fromTextAndUrl( (string)t( 'View journal' ), Url::fromRoute( 'publisso_gold.journals_journal', [ 'jrn_id' => $journal->getElement( 'id' ) ] ) )->toString());
                                
                                $tmpl->appendToVar('journals_journallist', '<li>'.$tmpl_link->parse().'</li>');
                        }
                        
                        return [
                                'content' => [
                                        '#type'     => 'markup', '#markup' => $tmpl->parse(),
                                ],
                                'pager' => [
                                        '#type' => 'pager'
                                ],
                                '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        }
