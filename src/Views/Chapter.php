<?php
        
        
        namespace Drupal\publisso_gold\Views;
        
        
        use Drupal\Core\Link;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\License;
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\ChapterManager;
        use Drupal\publisso_gold\Controller\Manager\TemplateManager;
        use Drupal\publisso_gold\Controller\User;
        use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
        use \Drupal\publisso_gold\Controller\Template;

        /**
         * Class Chapter
         * @package Drupal\publisso_gold\Views
         */
        class Chapter extends Book {
                
                protected $chapter;
        
                /**
                 * @param $bk_id
                 * @param $cp_id
                 * @param null $tab
                 * @return array
                 */
                public function getChapterHTML($bk_id, $cp_id, $tab = null) :array{
                        
                        $book = BookManager::getBook($bk_id, false);
                        
                        if(!$book->hasChapter($cp_id)) throw new NotFoundHttpException();
                        if(null === ($this->chapter = ChapterManager::getChapter($cp_id))) throw new NotFoundHttpException();
                        
                        $header = $this->getBookHead($book);
                        $body = $this->getBookBody($book, $tab);
                        
                        return [
                                'header' => $header,
                                'body' => $body,
                                '#cache' => ['max-age' => 0],
                                'page' => [
                                        '#attached' => [
                                                'html_head' => \Drupal::service('publisso_gold.tools')->getChapterDCMeta($this->chapter, $book)
                                        ]
                                ]
                        ];
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Book $book
                 * @param $tab
                 * @param array $addTabs
                 * @return array
                 */
                protected function getBookBody(\Drupal\publisso_gold\Controller\Medium\Book $book, $tab, $addTabs = []) :array{
                        
                        $templateBody = TemplateManager::load($book->getElement('theme').'_book_body');
                        if(!$templateBody->isLoaded()) return [];
                        
                        $addTabs = [
                                'chapter_content' => [
                                        'title' => 'Chapter Content',
                                        'weight' => 0,
                                        'show_on_empty' => 0,
                                        'route_prefix' => 'publisso_gold.books_book.chapter',
                                        'route_parameters' => [
                                                'cp_id' => $this->chapter->getElement('id')
                                        ]
                                ]
                        ];
                        
                        $this->setBookTabs($book, $templateBody, $tab, $addTabs, ['cp_id' => $this->chapter->getElement('id')], 'publisso_gold.books_book.chapter');
                        $this->setBookContent($book, $templateBody, $tab);
                
                        return [
                                '#type' => 'inline_template',
                                '#template' => $templateBody->parse()
                        ];
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Book $book
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setBookContent_chapter_content(\Drupal\publisso_gold\Controller\Medium\Book $book, Template &$templateBody, $tab = null) :bool{
        
                        $templateBody->setVar('content', '');
                        $tempstore = \Drupal::service( 'user.private_tempstore' )->get( 'publisso_gold' );
                        $_abstract = base64_decode( $this->chapter->getElement( 'abstract' ) );
                        $_erratum = $this->chapter->getElement( 'errata' );
                        $_fulltext = base64_decode( $this->chapter->getElement( 'chapter_text' ) );
                        $_references = base64_decode( $this->chapter->getElement( 'references' ) );
                        $_references = nl2br( $_references );

                        $authors = [];
                        $affiliations = [];
                        $strAuthors = '';
                        $strAffiliations = '';
                        $chapter_authors = json_decode( $this->chapter->getElement( 'authors' ) );

                        foreach ( $chapter_authors as $_ ) {

                                $weight = $_->weight ? $_->weight : 0;
                                while ( array_key_exists( $weight, $authors ) ) $weight++; //falls Autoren mit gleicher Gewichtung angegeben sind, damit sie sich nicht gegenseitig überschreiben
                                $authors[ $weight ] = $_;
                        }

                        ksort( $authors );

                        foreach ( $authors as $_ ) {

                                if ( property_exists( $_, 'uid' )) {
                                        $user = new User( $_->uid );
                                        $user->profile->loadSnapshot( $_->profile_archive_id );
                                        $affiliations[] = $user->profile->getAffiliation();
                                        $_->email = $user->profile->getElement('email');
                                }
                                else {
                                        $affiliations[] = $_->affiliation;
                                }

                                if(property_exists($_, 'add_affiliations')){

                                        $addAffiliations = (array)$_->add_affiliations;
                                        usort($addAffiliations, function($a, $b){
                                                return $a->weight <=> $b->weight;
                                        });

                                        $addAffiliations = array_filter($addAffiliations, function($v){
                                                if(property_exists($v, 'show')) return !!$v->show;
                                                return true;
                                        });

                                        foreach($addAffiliations as $addAffiliation) {

                                                if(is_object($addAffiliation))
                                                        $affiliations[] = $addAffiliation->affiliation;
                                                else
                                                        $affiliations[] = $addAffiliation;
                                        }
                                }
                        }

                        $affiliations = array_keys( array_flip( $affiliations ) ); //make affs unique


                        for ( $c = 1; $c <= count( $affiliations ); $c++ ) {
                                if ( $affiliations[ $c - 1 ] ) $strAffiliations .= ( !empty( $strAffiliations ) ? '<br>' : '' ) . '<sup>' . $c . '</sup>&nbsp;' . $affiliations[ $c - 1 ];
                        }

                        foreach ( $authors as $_ ) {

                                $affiliation = $_->affiliation;
                                $affKey = [];

                                if(property_exists($_, 'add_affiliations')){
                                        foreach ($_->add_affiliations as $addAffiliation){
                                                $affKey[] = array_search($addAffiliation->affiliation, $affiliations) + 1;
                                        }
                                }

                                $affKey[] = array_search( $affiliation, $affiliations ) + 1;
                                $affKey = array_unique($affKey);
                                sort($affKey);
                                $str_ = implode( ' ', [ $_->firstname, $_->lastname ] ) . ( count( $affKey ) ? " <sup>".implode(',', $affKey)."</sup>" : "" );
                                $isCorresponding = property_exists( $_, 'is_corresponding' ) ? !!$_->is_corresponding : false;

                                if ( $isCorresponding && property_exists($_, 'email')) {

                                        $data = md5( $_->email );

                                        if ( null === ( $encdata = $tempstore->get( 'encdata' ) ) ) {
                                                $encdata = serialize( [] );
                                        }
                                        $encdata = unserialize( $encdata );
                                        $encdata[ $data ] = $_->email;

                                        $tempstore->set( 'encdata', serialize( $encdata ) );
                                        $str_ = '<span class="glyphicon glyphicon-envelope"></span>&nbsp;<a class="encmailadd" data="' . $data . '" href="?subject=' . $this->chapter->getElement( 'title', $lang ?? null ) . '">' . $str_ . '</a>';
                                }

                                $strAuthors .= ( !empty( $strAuthors ) ? '<br>' : '' ) . '<span class="chapterAuthor">' . $str_ . '</span>';
                        }

                        $strAuthors .= '<br>' . implode( '<br>', array_filter( json_decode( $this->chapter->getElement( 'corporation' ), 1 ) ?? [] ) ) . '<br><br>' . $strAffiliations;

                        $templateBody->appendToVar( 'content', '<div id="chapter-text">
                                <div>
                                        <h1>
                                                ' . $this->chapter->getElement( 'title' ) . '
                                        </h1>
                                </div>
                                ' . $strAuthors . '
                                <div>
                                        <div id="abstract">' . ( !empty( $_abstract ) ? '<h2>' . t( 'Abstract' ) . '</h2>' . $_abstract . '<hr id="sepAbstractText">' : '<br>' ) . '</div>
                                        <div>' . $_fulltext . '</div>
                                        <div>' . ( !empty( $_references ) ? '<hr id="sepTextReferences"><h2>' . ( (string)t( 'References' ) ) . '</h2>' . $_references : '' ) . '</div>
                                        <div>' . ( !empty( $_erratum ) ? '<hr><h2>' . t( 'Erratum' ) . '</h2>' . $_erratum : '' ) . '</div>
                                </div></div>
                        '
                        );
        
        
                        if ( \Drupal::service( 'session' )->get( 'user' )[ 'weight' ] > 70 && !$this->chapter->getElement( 'next_version' ) ) {
                
                                switch ( $book->getElement( 'theme' ) ) {
                        
                                        case 'gms':
                                                $templateTheme = 'GMS';
                                                $linkClasses = [ 'btnPublissoDefault' ];
                                                break;
                        
                                        default:
                                                $templateTheme = 'publisso_gold';
                                                $linkClasses = [ 'link--calltoaction', 'inline', 'btn' ];
                                                $linkClasses = [ 'btn', 'btnPublissoDefault' ];
                                }
                
                                $_urlErratum = Url::fromRoute( 'publisso_gold.book.chapter.seterratum', [ 'cp_id' => $this->chapter->getElement( 'id' ) ], [ 'attributes' => [ 'class' => $linkClasses ] ] );
                                $_lnkErratum = Link::fromTextAndUrl( (string)$this->t( 'Set erratum' ), $_urlErratum );
                                $templateBody->appendToVar( 'content', '<hr>'.$_lnkErratum->toString() );
                        }
                        
                        $templateBody->setVar( 'book_chapter_navigation', $this->getBookChapterNavigation( $book ) );
                        
                        $this->setContent_Sidebars($book, $templateBody);
                        
                        return true;
                }
                
                protected function setContent_Sidebars(\Drupal\publisso_gold\Controller\Medium\Book $book, Template &$templateBody) :bool{
        
                        $texts          = \Drupal::service('publisso_gold.texts');
                        $theme          = strtolower($book->getElement('theme'));
                        $renderer       = \Drupal::service('renderer');
        
                        // create the sibebar-boxes
                        $__tmpl = new Template();
                        $__tmpl->get( 'publisso_gold_book_content_sidebar_block' );
        
                        //pubmed-search
                        if ( !!$book->getControlElement( 'search_pubmed_enabled' )) {
                
                                $authors = [];
                                foreach ( json_decode( $this->chapter->getElement( 'authors' ) ) as $_ ) $authors[ $_->weight ] = $_;
                                ksort( $authors );
                                reset( $authors );
                                $firstAuthor = current( $authors );
                                $firstAuthor = implode( ' ', array_filter( [ $firstAuthor->firstname, $firstAuthor->lastname ] ) );
                
                                $__tmpl->setVar( 'sbb_title', (string)$this->t( 'Search' ) );
                                $__tmpl->setVar( 'section-id', 'chapter-search' );
                                $__tmpl->setVar( 'view_name', 'search' );
                                $__tmpl->setVar( 'sbb_content', '' );
                                $__tmpl->setVar( 'view_name', 'partner-list' );
                                $__tmpl->appendToVar( 'sbb_title', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAAbCAYAAACKlipAAAADlElEQVRogeWZMW6zQBCFfZEUKXyN3MRXyAnc+hiOI1G7p0C+ABIN5RZuoKJCQrTzVw89P+8CxjHwJyOt5GC8zM6382bYbGxlVjft0i4sapulHTAzu5ZVB4I//0VbHEjdtLZ9f7Pt+5vtDmfLXfGnoawKyPb9zZLU2bWs7FpWS7u2iK0WSO6KP5klqwMSxZkXSN20lqTOojizKM46cL8N2qqBQLZyV9jH57d37A7n7je/wVYLBLv/WlZBGDr2x8t/D+alQMbISR+Qa1nZ/njpMmEsmCjOXiZlr5bIzf54sd3hfDf2x4slqZs8MeaN4qz3vj4guStuQGA+QPJJF3/3jP8+S1LXxedVYDa6YN2J++Nl0sQIzKNATl+nIBAE2Cdj+K5u2pvrPylhSeq6eV8OZHc4d7qtgZiyqFcAyV1xN78GqG7a4G+etdmBhB7OO9P3fqBdkdktEG5Zk9TdtbNjgKhMcJaoNDEs3UzXsuqkDbIcCu61rCxJXTd43kWBIHOGAsBzDBVj3rkfn9+DQHzSiWdocNh3fk4UZ8HWmcHVTeutU7yeWYBgl/OCEIhngPC8DAiLGgPEV6BzV3ivc/Yg0AwDWaFdHPxR3/XeWYBoUUfXwto8BYjWEN/uZSC4rkB4B/tqmkqp/g5/qz9cc4aUYEwNQQ1Gt8jxg1zyyTY3I1Gc3WZI3bTd8D3oJ4BwADAPA8HIXXEDRIOtht2sz9cgo6XnweviUwEFPwQEPptZd8SDwON67opOdVCX2OdgDVGbGwikCwGEQY44+L5rCB6AcC1iSeSXUOzuUIc2BIR3P87aEAvOCi4DiA8an7sMGQNEH+CDOkay4PDp69QNPfnVOXBNd5beB0gqWb51siooPL6X65AvVnw/B1r94szhz3XTjgfCi+LU1w5Eg4RAoe3laz5DQJAl2gHxvOoDPx/zsFSwrypZnPXsJ56h3WJIsrR2cNAxN75DU4L6YmaPAeFswG+4P1cgIWhjjjS4yUDgojjzniZooPkIRaUt1M6qPGmLjHkVCJ80AwgbWmgE3ecPx33TV8j7wGjfrnPo50f/CwjJAQDtotChhAIcKszqz5BM+9bFf/OLJdp6PXWGrz6pVP8WP37vM0jWmMwNgZnrOJ7hojuccmyzaiBoQ8eaSupcQLROhK6NsVUDeVRKYaz1cwBBnYCsQoqm+L5qIM8YsuUnT3v7DO8R/P4xxX4tELPpGbak/QPYnkh3mayzNQAAAABJRU5ErkJggg=="><br>' );
                
                                $renderer1 = \Drupal::service( 'renderer' );
                                $form = \Drupal::formBuilder()->getForm( '\\Drupal\\publisso_gold\\Form\\PubmedSearch', [ 'bk_id' => $book->getElement( 'id' ), 'term' => $firstAuthor ] );
                                $__tmpl->setVar( 'sbb_content', $renderer1->render( $form ) . '' );
                                $templateBody->appendToVar( 'sidebar', $__tmpl->parse('::', 0) );
                                $__tmpl->clear();
                        }
        
                        //livivo search
        
                        if(!!$book->getControlElement('search_livivo_enabled')) {
                
                                $renderer2 = \Drupal::service( 'renderer' );
                                $form = \Drupal::formBuilder()->getForm( '\\Drupal\\publisso_gold\\Form\\LivivoSearch', [ 'bk_id' => $book->getElement( 'id' ), 'term' => '' ] );
                                $__tmpl->clear();
                                $__tmpl->setVar( 'sbb_content', $renderer2->render( $form ) . '' );
                                $__tmpl->setVar( 'sbb_title', ( (string)$this->t( 'Search' ) ) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAAAjCAYAAAAnvgICAAAUCXpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarZppciM3toX/YxVvCZguhuVgjOgd9PLfdyBWuV2WHW1Hi1UklUwigTucASl3/v2v6/6Pn9zNu2y1lV6K5yf33OPgTfNfP1+vwef3/H7S/nwWfn/cxfT5IHJI7z+/l/M5f3DcfvtCzZ/j8/fHXV2fcdpnoM8HPwZMunLkzee89hkoxa/j4fO765/vjfwfy/n8nz+WYV8vv/6eK8HYxngpunhSSJ7npqskZpB6Grzae+5RRwrvE58OjrbvY+d+vv0leD1+Hzs/Pmek34fC+fI5ofwSo8/xYN/H7kXoP2cUfryNv/8g3fBjTn+I3b273Xu+VjdyIVLFfRblP0O8d5w4CWV6Xys8Kv+N9/U9Oo/GEhcZ22Rz8lgu9BCJ9g057DDCDee9rrCYYo4nVl5jXDG9Yy3V2ON6Scl6hBsr6dkuNbKxyFricPw5l/Cu29/1VmhceQfOjIHBAt/4w8N9d/CfPH4OdK9KNwQFs8cXK+YVFXCmoczpmbNISLifmNqL73u4n2n97UeJTWTQXpgbCxx+fg0xLfxWW+nlOXGe+ez8V2uEuj8DECKubUwmJDLgS0gWSvA1xhoCcWzkZzDzmHKcZCCYxR3cJTcpFZLToq7Nd2p450aLX4eBFhJhNEolNTQQycrZqJ+aGzU0LFl2ZlasWrNuo6SSi5VSahFGjZpqrlZLrbXVXkdLLTdrpdXWWm+jx56AMOulV9db730MLjoYevDtwRljzDjTzNNmmXW22edYlM/Ky1ZZdbXV19hxp03777Kr2233PU44lNLJx0459bTTz7jU2k03X7vl1ttuv+Nn1j5Z/X3Wwi+Z++ushU/WlLH8zqu/ZY3Dtf4YIghOTDkjYzEHMl6VAQo6Kme+hZyjMqecUXQ0hUWyFkzJ2UEZI4P5hGg3/Mzdb5n7y7w5y38rb/HPMueUuv9F5pxS98ncH/P2Tdb2eIySXoLUhYopMAiwcdKIjX/g8d97TX6ufEeotxxX+g57ciGmumfyPdWy9t6trWrhTGO9xTrgWlbcY9+19j2mNjtnlSRWKfVmN2bJJxLX2fuePSvws1nIFgx2+q/n6P7Zon4uDiDrsPvtrh5by+o41a9c6rBCIWdO29sAZNJXLWfKouwW2mRtROEsktdIUCo3keIak2sr3DxmHeX2ecjsKavZuh3kyVO0u8xGp2jjvJM4UkN1wHalryYum4HLruqo3zYaRbotrHMiwypAZaW/t0z3d+JSZhct3UIfsXzRYzgUOaEaznrn4iu3ldoF6oaZ75OGaScTDMq8DErBxtzLn0rDzdlHvHGWdWPkCDzVlP7DSvl1bat95jVTvwqXP2fPnU6J6Y5tg5auPK3Td+qrVFq5jjR9I9o37ONGr5MWHav4eWZrmaxQqCWBoJVp1rDq5vJ3BRquBqp0hVNJ9Yyn7GC0Tjs2XST/p3Y1n92V+rAzKNo117Sbcu39lJLWKsY0+6mQh3qUyabcJx1LKGys4BhmNyoonfrGTMTmTj8Oy6Vs6pplgxKhrk21zFMmbNOabTDt7g62Ebd+KEhDXlQf0WYATvUHWEUTDED1+Ako9XX2ldzNMBuT3YKsOS55ievOA9z3a8NJJYSaDzIWyARIyFLPl7gQrEzAW0eT5OpD26XP3RJJIo+zX4i7Tj2Pi87Wm5JZOdDK8i6gwhGQByzvNMDa0cCvnAcRRbkersX8QCgwnMiBDCPvk90NvUQwsY01ijpsB2JLayy/KI8R6YEO+AO86e7jE9CITPJx5q2xgJFCe9IifJqsrkK7rgrSTCQHTad5NaO2KIzSjToAklHAtzaLficK6GrO6YKdrAKEtMP4lbIGncuSRKvhnvVi4EdVXFq9VEO6sAry4XSLowgB2uKiOV2m4fZeCuBW59dDCPvkOEMn41BAX5zeqUfKYhJ7BX5sSvuGNdbsJaxGPkNzSdzNly6NeaoWqpmcuWlCACVPBgqTwqQWyCtDQYRg7gU8DKQBeGudPTqCAOsAJHOGEMZamfpljmDJHbUe4DknynF51gGYz4sK7WWztt27ndymBxAZqBIphSmcW0cPmkQeSxWwzcABzlw7MTEUL5ONo59DS1Ba9SJ612Cd9Lb7sRAghY7KRCooDy8nWCwvvBEgyT/98to7HMI7ij5W1IgJUU9vWgtRaZM622G2NSbXz9JXxAVw2WNk4IjY9FmLNWp/C2TMoB/X6wKGaPp1EIAkqdJNDOAbZD1TExSj4Qd6fgmdar+LpS1U/dp4zpDaXu2AkNakDWKfwAYylKo/l9kQld4oaxR810XgeewH1bSnIfVrgoN2AJKyvxGCcbjZxLyTLXLfAoB5Buwfdzwo2wn69kK0YZlWoaQpdKQCYftFsoMut+ac0am4EiVtXLZxOB4qdGI84OH2inOWGGiLuDWX0AGQiXUZh4j1ncvYw2a9iNEdTglHvqaVQe1kqIxYvr6LrCTTPtH8CbnQ+h1tEmza8UB12JevVLJlIORWVzAbhgvHxqDsKLQKyYCpuZ6Yr4dFAE9gmkwc64FaAv/iAH8XQE283ASuLnWNvCg70a8RdmzXT7B+U5yhXGOMTnPYmWsY7AAvxJ0XYg0yOuia8bofXQVk9UC5P4+GIqj7cLGzI+b8kMZKxaTL4haowhDt7GPEUZR+ygiIT6fZNYFQA9zB8irZAx6DdxAWDrDODVCUXe4Ei2EVgABE4Rz4BAG00xoRKwo1DH9yR1IhSgGVwXdav742gJJOYLqwLbjMGsSctBjtXg9FC2qejSRBqWJq4hhDWLa/uksysdPDD9RbuRvoDOVwjYMgRtQBq18KhqCAASPD93WR/no0D/gLhHhQA9zJv3JpKo0i1mf9AouI+ImuAv8b3KnEbWThTCjWwkAg1ixpVJ8N2GXNBgLy6SOau6r/Qu8kUC0gA0F4R46weCjQkExK7i2a+VPTq3n6nsjDd9iOIHIpp2FGuhhBFuOlDgneFuNTtQsSuvDxXQ6gAk6T9k+0FXJwktRchNXhmUgS6aChoO9qBYYC84ByOgVhD3YWBBGUuRBaCDa+7MMTat+8om4R00EluwF4EMpotBSpkkCVkSXqf5pD2GdNAYzMB7hHe1SJrFUyzOOvRBkdILtZJsIgU1BIBGQNXeZZWh5ileBgddYLxOMZhHoAJ6aGmsoWIYLw9p8WMAOUz419B182WIvxkgdNQBUMk6+L1OLyFaRvqFaDTCbtCJTGPRmYpNIOT8qc2Bgk8ozJ2fgc/FsEHqjIXsDsQXs1ATg6DcPQIRvmR+8yRkKZlIX6WQimfPOi4LERgOtKMnD7KbuSw1zobF5F0UynWkLZ3Q4gJFUrEaACw66duAuciNYDTUS0UUTIKvpHhTGPo7qQNzNLGZJ1oCah6v2U/EpYMQpxyEcTMMzL2FX+FNMn2whFEgrYHYHuzj10MVOdSSoP8qYpaGJVMiql9FHKoFuBu0UFgNsJkXYpRYWUuQJIcMdxWXL5YKkiIeoLq/ooE7o4qkP85S1R3cEJgCltD8dUooXG3RWMHRRXturgyxWgtp3J48qyxYhyhCLDo5grNo2CtEqsPBgDF/uA4EQvGZEwoAbVRBbdrheYIKF0SjKPni+A6ZblJyEBB0iMkNd864KWLHKAhSpLgOuugWQA1ry5gnKjell6h1Qlzeehv1pAx4dBXAigyADWPpsioLbQoquXLCFdITIA2eJ1cuogtpQscmYjjy8roWU6lh78R8WAuwgtFDwNqmJC/gUqA124wP4EeVjdDv01tpSzNBaNiIHpAN0Tg51lmYiSMVKNG3jpIKrOy9pLQCuFEdIqARHxTMoYUPmhxFg73GLdUPs0AdyJIusIsyJ3hzyB75BRq2fmMTFKrB0cXru4g4Ygr7ZZhxTerPQlKg+zQkmjefgV0JmUOjUP2oP86FfmQM9smSEmjbJ0lEQHsFJXfrgCdeKTSQVo+wuSprs6Y9CQQ/oltQTwX226wGwsDZua6J2n2JZKZzQa16N3sLSyi+pjuLgAlkaYuewUeg6cL8wdkYdytiwXrIjVO/AVmwCAX9/QmLG0DOppgwf6IOsMRNTpvqvaBj50RYr6JhJLygDRIdfjkCTgljpCUwxIDD+sEjncskWARRstaaP8EKUUOkK3wpM9eskvqQXoffvgQPNEma7IFTH1HevLGg+CmKhN2TWfgGjgSKRoAV3H07wZfo2eF5DU0xFuazsDwvoT7Ed8wnuMPJGNkkjwZypqG5AIhWlr0PyIQmfAAbgifbhVBdoRR3NsztiwA46cDEK9XltZ0o1kHFPX01hxHPoXcVmxNg5rRKkY2rovYAKZAY6I94MV/eIbi7we7AMiCBe2De6vuFWZiS1z8uyew7kixJocEcRCPio0MltFk4tnEao0FaAJZZ2Ne1h5P8pmsQEBcC+dD04M9NHCc9IvMISqnhLEJd2U+pcfo48GsSo3zyHRow6HLpB/kBwoTeWgB/EimhhUNLLUndQM1jVilWTYYUC07CDZSN8F7YPfZUidQtXaLUtwhpwtEtk19A5RQeqgPkeVOcfEdm3WDL8wzpLU8phIpCjlgg5HGDekN6MTiyTTfpIz2HPd8jYK5oqqNzQBHonhkeSwDYm7inAVWyG0ttS4dPvdcikeSxZ7c+pUaC1X+UYEAAcH0mpc6iuBOHSz2gMpeenoGIBsFPgO+7lU9PTBItDVwuxmjzlsYKu9vSA3Ghy8vDDAvAUi0F0FKh0xtWWQ65NQB4BJKwc8XyXYrCqsmJRqbN6FYbk0BhWpRGxAfGUVpSv1N1+fvfQjLCAJUO508HM7UJkMTMJwuDQ25C6Tmn/azAcvVwEhMQHmRTyTOPbiTG8+r1+xvANh7yhTZLL25VQnN7a0haZc6UYCfWjcXrXbw28w8zyVmVObNJO0NlnuchnbIRnAmK0PQWoMfQROSKD2c/UtyBJCRjNRJwCZkKCT/VK1O4jawbh11M9xUZtMqdCsGyPLOivFBIDuQfTRUlTWoa/xiep7FE1FJS2xdGsgz0KJVnkiJ8FEV2HZsJWZ+DC87ngM7TNj9ECGERF6QdljoVwUS6ybRwaCM+mndzHHAE5mldv6hnY4a2mHDAOHh4d0qCqKMkrT36x1462QEX2r2XH31pClnavgaSHpRiqDfB5lOwlDmEkg2yNXR5PIoHe/4p/5dr061ZU0RaX47vMj5TUiC8FtMrl0fT9jUvs41EsMqCUaOIJJcMCYzJgkUZDgN7yPggKnxW6yHfL5E6+5WlFpb21Q0RNwO+skWMhb/Px9e7Zj4vA7CAmnQaoHf45amAhA2P7tW1FMcvBywegCemKBKSkCNe9KK8LaaAHqkQoc1BGThStKVisF9GtUVqqyWKTRmVeOmexdWSVODx/IQ9ZP0q8ZQlbAyOMXmIhhkM4B4ocZ5QcFQxR7I+Q31ZywuWNp66fAUg0M662qd58rd5Q0rjsTEDJKJEKb0GQjoSfLXFwE0yODRJXOr80XQ2ZgOweST4G42gp1rIfulmoE5AkRp0tvIe6oeN2nxExp1wgNhYWlvAgPxWaQNf2NsULZau/Urd6QaXQ9ChrYH4STWpSOPU8jUtvUgXagprbUkJ5d/dq083KZ08DWHajDUdfp8RQKYY2aWdo6u1aBxG5QHdosvAWVBpvUgczQcrB4EnW6JyETissOTaAFPCP0sPnE/FIZOAv0je6nIERRmKEQPRUZJ2qfSNtVHjbs86uQi1vMZkui4Opx5EXKcD8qA08FH8Sn2Pl4XAqRy0UcASfN/npgvE8czY1SYi0S64o80768Fanv1zIYx8r4gEYgPqfhrBF3/GNV2KMr+DnBsQRVmFzrpGy5dCBS9BVfWqQbwQ2WqzI0JIul/C/9Ica847ytMW2eu5CPkd+IFFqmu2A7QMfgLLCatCtyjNWg3EaAg6onY9qzvfJ2lDEUKQ2KPJ6TulMhQyNMsl7BXIiEK9AQJ8q14Dl3K9DeuxFUvjXS7jt1RQsYkhsh9PR5kci4Rjk2okC8KwIXmVl0z53SIe94Eeu65xHlppQgbf6jhw9OaOWOr/V4HMJF9jMOdrSvv9xoROTr3tF5qh6dzfKwrWVLdOMSJhlHl5BY3erp06ZhtGiaGBr/+LJA1hpSg+lJw/nRDziJOYZvSShMRy9PdB7drv1tZB3Scuq2C5WJvzISmXRjMgSO02Wtm9dWLmuzhWJDLUMa9D3Kggwg0wfFr00e3ciMCHMsNZpj469G8qd1dRKcS2cShZEi6/fOyIvup2+CJlv5djFY5tS2MPl92wpA89nrWUxSqk3V+7m7FgUUJ68jzAZ5VS9wK3rNgCv9Bc3LoG4zoe080OyvR6ZyIgPjdPU3FPqzibIoCv2FhHa0pJapYQy9PMRZoViAEyOE6HmKBSBvmDQso1xsDgi0d5+IDJSu3UGoxOkiNDH6zo48GCHC7zd85WNurozApRKA+LZ0WzAlIDxxuO1OpI5az3x0A4OHUhK+ZKBJf64h2Y0gxx68HUM5bqg4BRi/i9dgN7wjchhr7rXvEigNxzPwqb9LoJegSepWwflrWv3m1f3dL/x4HahAZX+Ds9qNxtRs5UGcFjmE8NGeCv+HmrogSIADahoHg++inr2G8ZBFQ3kgXuEz1p1dhwc6tplFdeiRhGq7rmMqaUVsRbKiVA6UMm5taK+zIrQaFHZAJWKC6YVIHZxDDivJKtptw3klHHREAnZ4nfMSXiDm6skUC0NC8nJ0d8IvyBgMauvrrug/iM93r//VQNQhmCpx4rWlOSQuUCd2cPgriUd8djt97oxEEbb/3O39Eu2tafNmfG4BR/u6V05vf/Pq/uyD12lXO8DUyP8D2ftjWQj1LNcAAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQfjBQkOIDvH/eP6AAAISElEQVRo3u2Za4xcZRnHf+c658zOZZnZzi673ba0227ZdKAWFFGkCGi9BAX5Ihr9YDDRGPCDH0w0RaMfNFFiIAE1MUoCpIUYaAEhJEovAm2xpXtpl+52Zy/d2WG3M9s5cz33OX6Y7dQaY6Kwu6byfHvfc3mf/3mf5/9/nvcQrKDVTTv4zu5ng1K5FqymCUEQBKyglco1YtEwgiCwWrbioP8XTFzpBV3fIiC4ckFXrQInp56iEfgAFKuzvDJ4LwvF0SsXdEhu4+qrbkBYWkZTovQmvkw4lFxV0B/k9HKb32hQqZp4nr+qoOX3VY5qOWr2It2JNADzxVHOzu/jI30PElIivHVigl89eYxv3H0dn7n9ehYXF5mdnUWSZNrb4wiCiKLIzOVyaKEQnZ2dzM7O0t3dg+PYmKbJunXrEEUR13VRFAXf9wmCgGLRIBaLooZCjI2NkUwkaW+PoygKrutiWRaaplEsFt9f0H7DxfPN1thr2FjuPEHQWGJun1LdxXG9Zm4JAt3d3UxOTqFpIVQ1hGEY2JYNgCRJJBIJDMMgkUhQKpU5c+YMsqxQLpeJx2OEw2E8z8O2HQyjyIYNG3Bsh2q1QqlUolar0du7lkqlQrlSwayby5/TQdBAEJpZ5Hk+pUqdaJuOqspMT0+jaRqVSpVYPIaqNMHoepharUosFuf8+QV6enqwLJt6vYaqhhBFAcMwiEQipFIpstk5dF1DVVUikQh100TXNLLZOSKRNlKpFLlcjnrdbPqz0qAt20VVZVRF5uLS/1id/bM7F6/5vo8kSa173ktF956ILL9Yplypt8aj41kOvXlJg8dyr3Jw9CEuVGYAePq5I+z61lM89sRrNBoBk5NTVKtVLMuiXq/jeR5zc3PUajXGx89Sq9WwbZuFhQXGxsawLAvLsiiVytTrdWzbxlyac123Nec4zvIR2YnhKbrWxNm+bQMAx4em+evbWW69+VoEQSBfHqRQfZyKdReJ6HrO5Qz8IODcuxV838fzXHK5HL7v4/sNNm3aiG07lEolLMskk8mgaTrd3VezsHCe6ekZkskk42fHkUQJTQuhaTqCAJIkUygUiEQiRKNR1q9ftzI6bdkunucRadMBqJgLlM15UrF+FFljdDzL4OlZtm7qZMd1GykaBr7n4XkemqYTj8eYmTlHKKSiKMpFF1FVhXy+QDwea7G14zjoehhZkTHrdWRZxrJsQiEVSZLo6OhYneIkCAKCAESxmX+O41E3bXRNJRRqgmo0AgSBFe26/qPwdhwPRZFaDp7L5gmpCp2pdgICJucPYzqLbO35PJKo8vs9hxmfucC9u9Js37aB3Q+/yKHTBbatjfLI7ruZzuZ58vkT9HZG+eZXdyLLTR4QBJH+7l0IgshisYJRqrFpQ9dlEaWFlOUnskajwU8feZlzc4Wm5roeD//uEHte+NvFbWUk+xDvvHs/VSuP43jsOzzFodMFBkezWLZDJlcB4FS2Qt2yGZuY5+CpPC8fnaVSM7G9GqfmvsZobjeOVwPg1QMjfH33S60qrrBY5vs/24/jeMu/06Iosvu7n0VRmo8oisz37r/t0hcXBNJrf4LpLBLR1iCJMvfs3MjY9CLbB9aiaSp9PTGyxTzp3ihhPcTWvi5u27aGdV0xom06sgzpnqcQBBFVbgNg1yfT3LRjI7LclKuOZIxf/OBuVPW/5+CVzWnXo1630XWVkLp6OS3dd999P77IdI7jcPTYMYoXiniex+xsllRqTVOT8wUmJiYwDIPh4RGq1RoXLiziOA6xWAyATCZDuVwhHA5z5MhR3j55EkkUSSYTCILAmYk5Xj82ju/7dHcl8BsOC8Y7NAIXTW2+o163cT2/FVHL0mUNDQ23BoqiEDQCOjtTCILA3meexXXdpgYfP06hUCAajbL3mT8SDocpVyocOHCotav797+I53ns2fsM2bk5+rds5vFf/5aZmXMAPPfKEI8+d5qn9w/iuh6F8iRvnN3JW5mft7qwR//wGo89caDl08TUu+zdd3T5WktBEJBlGVlW6OxMYdsOxaJBo9Hg5OAQW7f2k0hcRSTSRkdHki2bN/PmkWN4nodlWQwNn6Kjo4Pn973EF79wFwMDA+z69J2MjIwAsL6nHUkQWN8dQ5JEwqEEqeiDXB3f2XRGEPjULf3ceUt/y6furgS3fax/ZSRLlmU+etOHyWQyKIqMaZqkUik87xJrxmJxent7mJ+fp1qtcusnPo5lmSSTV6GqIQDi7XEymUkAvnLPzXzpczeiqjKiKBLVU9x67UOwlM+CIHDD9Rsv8yOshwjroZXT6XR6G3/+y2vousb116VRFOUy0JIkcuMNOxgfP4thGHxox3YkScIyLVg6/HMdF03TmqDEBoJUQxQjly0tsLLHwf9Wp3t6eshm5zhw8DDpdPpf3jMwMMDrb7zJibdP0rdpE+3t7c26O5+n0WgwPn6WjddcA8DUwuu8OnInZ+b+1Pwgnsmh0R8xMX+w9b7chRHmjXeWtyI7fz7P4OAgAH19fczOZpFlmWQyQTgcZsuWzQwPn2Lt2h4ABgeHqFZrHD9+gjvuuH0p922SyWSLxR944Ns89vhvSCaTKIpCOr2tGRmiiix2IYmhlraHlA4USb9EprKOKEjLezBYLpdbOq3rOqZpIooibW3N4sCyLDzPJxJpjk3LwnNdJEkiHA4vyUwdQRDQ9UvOVyoVPM8jFou1+uBG4OO4NVQljCjIrJZ9cBq63GaUaux5/ggLeaM1l5map1I1r1zQlarJC4czFI1aa+70+Bylcv3KDm/Ldgmp8gd/La/onIbm/+lGI/j/AW1aDj/85ctUa+aqgv47z4zuLkRxSaQAAAAASUVORK5CYII=">' );
                                $templateBody->appendToVar( 'sidebar', $__tmpl->parse('::', 0) );
                                $__tmpl->clear();
                        }
                        
                        //citation note
                        $__tmpl->setVar( 'sbb_title', (string)$this->t( 'Citation note' ) );
                        $__tmpl->setVar( 'section-id', 'chapter-citationnote' );
                        $__tmpl->setVar( 'view_name', 'citationnote' );
                        $__tmpl->setVar( 'sbb_content', $this->chapter->getCitationNote( $this ) );
                        $templateBody->appendToVar('sidebar', $__tmpl->parse());
                        $__tmpl->clear();
                        
                        //sustaining members
                        if ( count( $book->readSustainingMembers() ) ) {
                
                                $__tmpl->clear();
                                $__tmpl->setVar( 'section-id', 'chapter-sustainingmembers' );
                                $__tmpl->setVar( 'sbb_title', (string)$this->t( \Drupal::service( 'publisso_gold.texts' )->get( "book.content.sidebar.$theme.sustaining_members.title", 'fc' ) ) );
                                $__tmpl->setVar( 'sbb_content', '' );
                                $__tmpl->setVar( 'view_name', 'partner-list' );
                
                                foreach ( $book->readSustainingMembers() as $bsm_id ) {
                        
                                        $urlPicture = Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'bsm', 'id' => $bsm_id ] );
                                        $__tmpl->appendToVar( 'sbb_content', '<img src="' . $urlPicture->toString() . '"><br>' );
                                }
        
                                $templateBody->appendToVar('sidebar', $__tmpl->parse());
                                $__tmpl->clear();
                        }
                        
                        //downloads
                        $urlExportPDF = Url::fromRoute( 'publisso_gold.export.chapter', [ 'cp_id' => $this->chapter->getElement( 'id' ) ] );
                        $urlRecreatePDF = Url::fromRoute( 'publisso_gold.export.chapter.force_new', [ 'cp_id' => $this->chapter->getElement( 'id' ) ] );
                        $urlPreviewPDF = Url::fromRoute( 'publisso_gold.export.chapter.volatile', [ 'cp_id' => $this->chapter->getElement( 'id' ) ] );
                        $urlDeletePDF = Url::fromRoute( 'publisso_gold.book.chapter.pdf', [ 'bk_id' => $book->getElement('id'), 'cp_id' => $this->chapter->getElement( 'id' ), 'action' => 'delete' ] );
                        $urlExportRIS = Url::fromRoute('publisso_gold.book.chapter.ris', ['bk_id' => $book->getElement('id'), 'cp_id' => $this->chapter->getElement('id')]);
        
                        if ( $this->chapter->getElement( 'pdf_blob_id' ) ) $__tmpl->appendToVar( 'sbb_content', '<div id="lnkChapterPDFDownload" ><a href="' . $urlExportPDF->toString() . '" class="download-pdf" id="lnkChapterPDFDownload" rel="nofollow" target="_self"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;Download PDF</a></div>' );
                        $__tmpl->appendToVar( 'sbb_content', '<div id="lnkChapterRIS" ><a href="'.$urlExportRIS->toString().'" class="download-ris" rel="nofollow" target="_self"><span class="glyphicon glyphicon-bookmark"></span>&nbsp;&nbsp;Download RIS</a><br></div>' );
        
                        if ( \Drupal::service( 'session' )->get( 'user' )[ 'weight' ] >= 70 ) {
                                $__tmpl->appendToVar( 'sbb_content', '<a href="' . $urlRecreatePDF->toString() . '" class="download-pdf" rel="nofollow" target="_self"><span class="glyphicon glyphicon-repeat"></span>&nbsp;&nbsp;(Re)Create PDF</a><br>' );
                                $__tmpl->appendToVar( 'sbb_content', '<a href="' . $urlPreviewPDF->toString() . '" class="download-pdf" rel="nofollow" target="_self"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;Preview PDF</a><br>' );
                
                                if ( $this->chapter->getElement( 'pdf_blob_id' ) ) $__tmpl->appendToVar( 'sbb_content', '<div id="lnkChapterPDFDelete" ><a href="' . $urlDeletePDF->toString() . '" class="download-pdf use-ajax" data-dialog-type="modal" rel="nofollow" target="_self"><span class="glyphicon glyphicon-trash"></span>&nbsp;&nbsp;Delete PDF</a><br></div>' );
                        }
        
                        $__tmpl->appendToVar( 'sbb_content', '<div id="lnkChapterPrint" ><a href="javascript:printChapter();" class="print-chapter use-ajax" data-dialog-type="modal" rel="nofollow" target="_self"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Print</a><br></div>' );

                        if ( !empty( $__tmpl->getVar( 'sbb_content' ) ) ) $templateBody->appendToVar( 'sidebar', $__tmpl->parse('::', 0) );
                        $__tmpl->clear();
        
                        //license
                        if ( !empty( $this->chapter->getElement( 'license' ) ) ) {
                
                                $__tmpl->clear();
                                $__tmpl->setVar( 'section-id', 'chapter-license' );
                                $__tmpl->setVar( 'view_name', 'license' );
                                $__tmpl->setVar( 'sbb_title', (string)$this->t( 'License' ) );
                
                                $license = new License( $this->chapter->getElement( 'license' ) );
                                $__tmpl->setVar( 'sbb_content', $license->getImageLink( [ 'height' => 40 ], '_blank' ) );
                                $__tmpl->appendToVar( 'sbb_content', '<br>&copy;&nbsp;' );
                                $__tmpl->appendToVar( 'sbb_content', $this->chapter->getAuthorsIntNames()[ 0 ] ?? '' );
                                $__tmpl->appendToVar( 'sbb_content', count( json_decode( $this->chapter->getElement( 'authors' ) ) ) > 1 ? ' et al.' : '' );
                                $__tmpl->appendToVar( 'sbb_content', '<br>' );
                                $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'This chapter is distributed under the terms of the license <a href="' . $license->getElement( 'url' ) . '" target="_blank">@license</a>.', [ '@license' => $license->getElement( 'description' ) ] ) );
                                $__tmpl->setVar( 'view_name', 'license' );
                                $templateBody->appendToVar( 'sidebar', $__tmpl->parse('::', 0) );
                                $__tmpl->clear();
                        }
        
                        //version
                        if ( $this->chapter->getElement( 'version' ) || $this->chapter->getElement( 'next_version' ) || $this->chapter->getElement( 'previous_version' ) ) {
                
                                $__tmpl->clear();
                                $__tmpl->setVar( 'section-id', 'chapter-version' );
                                $__tmpl->setVar( 'view_name', 'version' );
                                $__tmpl->setVar( 'sbb_title', (string)$this->t( 'Version' ) );
                                $__tmpl->setVar( 'sbb_content', $this->chapter->getElement( 'version' ) );
                                $__tmpl->setVar( 'view_name', 'version' );
                
                                $links = [];
                
                                if ( !empty( $this->chapter->getElement( 'next_version' ) ) ) {
                                        $route_param = \Drupal::routeMatch()->getRawParameters()->all();
                                        $route_param[ 'cp_id' ] = $this->chapter->getElement( 'next_version' );
                                        $url = Url::fromRoute( \Drupal::routeMatch()->getRouteName(), $route_param );
                        
                                        $__tmpl->appendToVar( 'sbb_content', '<div class="rwError">' . ( (string)$this->t( 'This version is not up-to-date' ) ) . '</div>' );
                        
                                        $publication_date_next_version = $this->chapter->getRawElement( 'published', '<next>' );
                                        $publication_date_next_version = implode( '-', array_reverse( explode( '-', $publication_date_next_version ) ) );
                        
                                        $links[] = Link::fromTextAndUrl( ( (string)$this->t( 'Version @date', [ '@date' => $publication_date_next_version ] ) ), $url )->toString();
                                }
                
                                if ( !empty( $this->chapter->getElement( 'previous_version' ) ) ) {
                                        $route_param = \Drupal::routeMatch()->getRawParameters()->all();
                                        $route_param[ 'cp_id' ] = $this->chapter->getElement( 'previous_version' );
                                        $url = Url::fromRoute( \Drupal::routeMatch()->getRouteName(), $route_param );
                        
                                        $publication_date_previous_version = $this->chapter->getRawElement( 'published', '<previous>' );
                                        $publication_date_previous_version = implode( '-', array_reverse( explode( '-', $publication_date_previous_version ) ) );
                        
                                        $links[] = Link::fromTextAndUrl( ( (string)$this->t( 'Version @date', [ '@date' => $publication_date_previous_version ] ) ), $url )->toString();
                                }
                
                                $__tmpl->appendToVar( 'sbb_content', '<div>' . implode( '<br>', $links ) . '</div>' );
                                $templateBody->appendToVar( 'search', $__tmpl->parse('::', 0) );
                                $__tmpl->clear();
                        }
        
                        if ( $book->getControlElement( 'show_submission_date' ) == 1 && $this->chapter->getDate( 'received', $lang ?? null ) ) {
                                $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'Received: @date', [ '@date' => date( 'Y-m-d', strtotime( $this->chapter->getDate( 'received', $lang ?? null ) ) ) ] ) );
                        }
        
                        if ( $book->getControlElement( 'show_revision_date' ) == 1 && $this->chapter->getDate( 'revised', $lang ?? null
                                ) ) {
                                if ( $__tmpl->getVar( 'sbb_content' ) ) $__tmpl->appendToVar( 'sbb_content', '<br>' );
                                $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'Revised: @date', [ '@date' => date( 'Y-m-d', strtotime( $this->chapter->getDate( 'revised', $lang ?? null ) ) ) ] ) );
                        }
        
                        if ( $book->getControlElement( 'show_acceptance_date' ) == 1 && $this->chapter->getDate( 'accepted', $lang ?? null ) ) {
                                if ( $__tmpl->getVar( 'sbb_content' ) ) $__tmpl->appendToVar( 'sbb_content', '<br>' );
                                $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'Accepted: @date', [ '@date' => date( 'Y-m-d', strtotime( $this->chapter->getDate( 'accepted', $lang ?? null ) ) ) ] ) );
                        }
        
                        if ( $book->getControlElement( 'show_publication_date' ) == 1 && $this->chapter->getDate( 'published', $lang ?? null ) ) {
                
                                if ( $__tmpl->getVar( 'sbb_content' ) ) $__tmpl->appendToVar( 'sbb_content', '<br>' );
                                $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'Published: @date', [ '@date' => date( 'Y-m-d', strtotime( $this->chapter->getDate( 'published', $lang ?? null ) ) ) ] ) );
                        }
        
                        if ( $__tmpl->getVar( 'sbb_content' ) ) {
                                $templateBody->appendToVar( 'sidebar', $__tmpl->parse('::', 0) );
                        }
                        
                        return true;
                }
        
                /**
                 * @param $bk_id
                 * @param $cp_id
                 * @param $tab
                 * @return false|mixed|string
                 */
                public function getPageTitle($bk_id, $cp_id, $tab){
                        
                        if(!$tab || strtolower($tab) == 'chapter_content') {
                                $chapter = ChapterManager::getChapter($cp_id, false);
                                $title = $chapter->getElement('title');
                        }
                        else{
                                $tabs = BookManager::getHTMLViewTabs($bk_id);
                                $title = (string)$this->t($tabs[strtolower($tab)]['title']);
                        }
        
                        return $title;
                }
        
                /**
                 * @param $book
                 * @return string|string[]|null
                 */
                private function getBookChapterNavigation (&$book) {
                        
                        $chapter = $this->chapter;
                        $template = new \Drupal\publisso_gold\Controller\Template();
                        $template->get( 'book_chapter_navigation' );
                
                        $nextChapterID = $book->getNextChapterID( $chapter->getElement( 'id' ) );
                
                        if ( $nextChapterID != $chapter->getElement( 'id' ) ) {
                                $urlNext = Url::fromRoute( 'publisso_gold.book.chapter', [ 'bk_id' => $book->getElement('id'), 'cp_id' => $book->getNextChapterID( $chapter->getElement( 'id' ) ) ] );
                                $template->setVar( 'lnkNextChapter', Link::fromTextAndUrl( (string)t( 'Go to next chapter' ), $urlNext )->toString() );
                        }
                        else {
                                $template->setVar( 'lnkNextChapter', (string)t( 'Go to next chapter' ) );
                        }
                
                        $prevChapterID = $book->getPreviousChapterID( $chapter->getElement( 'id' ) );
                
                        if ( $prevChapterID != $chapter->getElement( 'id' ) ) {
                                $urlPrev = Url::fromRoute( 'publisso_gold.book.chapter', [ 'bk_id' => $book->getElement('id'), 'cp_id' => $book->getPreviousChapterID( $chapter->getElement( 'id' ) ) ] );
                                $template->setVar( 'lnkPrevChapter', Link::fromTextAndUrl( (string)t( 'Go to previous chapter' ), $urlPrev )->toString() );
                        }
                        else {
                                $template->setVar( 'lnkPrevChapter', (string)t( 'Go to previous chapter' ) );
                        }
                
                        return $template->parse();
                }
        }
