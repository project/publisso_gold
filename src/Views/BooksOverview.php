<?php
        
        
        namespace Drupal\publisso_gold\Views;
        
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Database\Database;
        use Drupal\Core\Database\Schema;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\TemplateManager;

        /**
         * Class BooksOverview
         * @package Drupal\publisso_gold\Views
         */
        class BooksOverview extends ControllerBase {
        
                /**
                 * @return array
                 */
                public function main () {
                        
                        $tmpl = TemplateManager::load('publisso_books_booklist');
                        $tmpl_link = TemplateManager::load('publisso_books_booklist_item');
        
        
                        $qry = \Drupal::database()->select('rwPubgoldBooks', 'b')->fields('b', ['bk_id']);
                        $pager = $qry->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit(20);
                        $pager->condition('bk_public', 1, '=');
                        $pager->orderBy('bk_title', 'ASC');
        
                        foreach($pager->execute()->fetchCol() as $id){
                
                                $tmpl_link->clear();
                                $book = BookManager::getBook($id, false);
        
                                $tmpl_link->setVar( 'bk_id', $id );
                                $tmpl_link->setVar( 'books_booklist_link', $book->getElement( 'title' ) );
                                $tmpl_link->setVar( 'books_booklist_title', $book->getElement( 'title' ) );
                                $tmpl_link->setVar( 'books_booklist_description', base64_decode( $book->getElement( 'description' ) ) );
                                $tmpl_link->setVar( 'txt_lnk_view_book', (string)$this->t( 'View book' ) );
        
                                $urlCover = Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'bc', 'id' => $id ] );
                                $urlLogo = Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'bl', 'id' => $id ] );
        
                                if ( !empty( $link = $book->getCoverLink() ) ) {
        
                                        $tmpl_link->setVar( 'books_booklist_cover', '<a href="' . $link . '"><img class="media-object" src="' . $urlCover->toString() . '"></a>' );
                                        $tmpl_link->setVar( 'lnk_view_book', $link );
                                }
                                else {
        
                                        $tmpl_link->setVar( 'books_booklist_cover', '<img class="media-object" src="' . $urlCover->toString() . '"' );
                                        $urlBook = Url::fromRoute( 'publisso_gold.books_book', [ 'bk_id' => $id ] );
                                        $tmpl_link->setVar( 'lnk_view_book', $urlBook->toString() );
                                }
        
                                $tmpl_link->setVar( 'books_booklist_logo', $urlLogo->toString() );
                                $tmpl_link->setVar('books_booklist_logo_alt', $book->hasLogo() ? 'LOGO: '.$book->getElement('publisher') : '');
                                $tmpl->appendToVar('books_booklist', '<li>'.$tmpl_link->parse().'</li>');
                        }
                        
                        return [
                                'content' => [
                                        '#type'     => 'markup', '#markup' => $tmpl->parse(),
                                ],
                                'pager' => [
                                        '#type' => 'pager'
                                ],
                                '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        }
