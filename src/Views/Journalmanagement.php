<?php
        
        
        namespace Drupal\publisso_gold\Views;
        
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\JournalManager;
        use Drupal\publisso_gold\Controller\Manager\TemplateManager;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class Journalmanagement
         * @package Drupal\publisso_gold\Views
         */
        class Journalmanagement extends ControllerBase {
        
                /**
                 * @return array
                 */
                public function main () {
                        
                        $filter = null;
                        if(\Drupal::requestStack()->getCurrentRequest()->query->has('filter')){
                                $filter = \Drupal::requestStack()->getCurrentRequest()->query->get('filter');
                        }
                        
                        $user = Publisso::currentUser();
                        
                        if($user->getWeight() >= 70){ //alle Journals für Account >= PUBLISSO
                                $qry = \Drupal::database()->select( 'rwPubgoldJournals', 'j' )->fields( 'j', [ 'jrn_id' ] );
                                $pager = $qry->extend( '\Drupal\Core\Database\Query\PagerSelectExtender' )->limit( 10 );
                                $pager->orderBy( 'jrn_title', 'ASC' );
                        }
                        else { //Account-bezogene Bücher
                                $qry = \Drupal::database()->select( 'rwPubgoldJournals', 'j' )->fields( 'j', [ 'jrn_id' ] );
                                $qry->leftJoin( 'rwPubgoldJournalEditors', 'je', 'j.jrn_id = je.je_journalid AND je.je_uid = :uid', [ ':uid' => $user->getId() ] );
                                $qry->leftJoin( 'rwPubgoldJournalsEditorsInChief', 'jeic', 'j.jrn_id=jeic.jeic_jrnid AND jeic.jeic_uid = :uid', [ ':uid' => $user->getId() ] );
                                $qry->leftJoin( 'rwPubgoldJournalsEditorialOffice', 'jeo', 'j.jrn_id=jeo.jeo_jrnid AND jeo.jeo_uid = :uid', [ ':uid' => $user->getId() ] );
                                $cond = $qry->orConditionGroup();
                                $cond->condition( 'je.je_uid', $user->getId(), '=' );
                                $cond->condition( 'jeic.jeic_uid', $user->getId(), '=' );
                                $cond->condition( 'jeo.jeo_uid', $user->getId(), '=' );
                                $qry->condition( $cond );
                                $pager = $qry->extend( '\Drupal\Core\Database\Query\PagerSelectExtender' )->limit( 10 );
                                $pager->orderBy( 'jrn_title', 'ASC' );
                        }
        
                        if($filter) $pager->condition('j.jrn_title', "%$filter%", 'LIKE');
                        
                        $urlAddJournal = Url::fromRoute('publisso_gold.add_journal', [], ['attributes' => ['class' => ['btn', 'btn-warning']]]);
        
                        $list = [
                                '#type' => 'table',
                                '#header' => [
                                        [],
                                        ['data' => (string)$this->t('Title')],
                                        ['data' => (string)$this->t('Title Abbr.')],
                                        ['data' => (string)$this->t('Public')],
                                        []
                                ],
                                '#rows' => [],
                                '#empty' => (string)$this->t('No journals found...')
                        ];
        
                        foreach($pager->execute()->fetchCol() as $id){
                                
                                $journal = JournalManager::getJournal($id, true);
                                
                                $urlViewJournal         = Url::fromRoute('publisso_gold.journals_journal'                , ['jrn_id' => $id]);
                                $urlEditJournal         = Url::fromRoute('publisso_gold.edit_journal'                    , ['jrn_id' => $id]);
                                $urlAssignRoles         = Url::fromRoute('publisso_gold.journal.management.roles.assign' , ['jrn_id' => $id]);
                                $urlAddVolume           = Url::fromRoute('publisso_gold.journalmanagement.volume.add'    , ['jrn_id' => $id]);
                                $urlAddIssue            = Url::fromRoute('publisso_gold.journalmanagement.issue.add'     , ['jrn_id' => $id]);
                                
                                $links = [];
                                $links[] = [
                                        'title' => (string)$this->t('View Journal'),
                                        'url' => $urlViewJournal
                                ];
                                
                                if($urlEditJournal->access()){
                                        $links[] = [
                                                'title' => (string)$this->t('Edit Journal'),
                                                'url' => $urlEditJournal
                                        ];
                                }
                                
                                if($urlAssignRoles->access()){
                                        $links[] = [
                                                'title' => (string)$this->t('Assign Roles'),
                                                'url' => $urlAssignRoles
                                        ];
                                }
        
                                if($urlAddVolume->access()){
                                        $links[] = [
                                                'title' => (string)$this->t('Add Volume'),
                                                'url' => $urlAddVolume
                                        ];
                                }
        
                                if($urlAddIssue->access()){
                                        $links[] = [
                                                'title' => (string)$this->t('Add Issue'),
                                                'url' => $urlAddIssue
                                        ];
                                }
                                
                                $list['#rows'][] = [
                                        ['data' => [
                                                '#markup' => '<img src="'.Url::fromRoute('publisso_gold.getPicture', ['type' => 'ul', 'id' => $journal->getElement('logo')[0] ?? 0])->toString().'" width="30">'
                                        ]],
                                        ['data' => [
                                                '#markup' => $journal->getElement('title')
                                        ]],
                                        ['data' => [
                                                '#markup' => $journal->getElement('title_abbr')
                                        ]],
                                        ['data' => [
                                                '#markup' => (string)$this->t($journal->getElement('public') ? 'Yes' : 'No')
                                        ]],
                                        ['data' => [
                                                '#type' => 'dropbutton',
                                                '#links' => $links
                                        ], 'width' => '150']
                                ];
                        }
                        
                        return [
                                'filter' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Title contains:'),
                                        '#attributes' => [
                                                'style' => [
                                                        'width: 200px;'
                                                ],
                                                'onkeyup' => [
                                                        'filterManagementMediumList(event, this)'
                                                ]
                                        ],
                                        '#value' => $filter ?? '',
                                        '#description' => (string)$this->t('Hit "Enter" to accept filter')
                                ],
                                'content' => $list,
                                'pager' => ['#type' => 'pager'],
                                'separator1' => [
                                        '#markup' => '<hr>'
                                ],
                                'add' => $urlAddJournal->access() ? [
                                        '#type' => 'link',
                                        '#title' => (string)$this->t('Add Journal'),
                                        '#url' => $urlAddJournal
                                ] : [],
                                'separator2' => [
                                        '#markup' => '<hr>'
                                ],
                                '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        }
