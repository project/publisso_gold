<?php
        
        namespace Drupal\publisso_gold\Views;
        
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Link;
        use Drupal\Core\Render\Markup;
        use Drupal\Core\Render\Renderer;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Bookchapter;
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Manager\JournalManager;
        use Drupal\publisso_gold\Controller\Manager\TemplateManager;
        use Drupal\publisso_gold\Controller\TableOfContent;
        use Drupal\publisso_gold\Controller\Template;
        use Zend\Feed\Uri;

        /**
         * Class Journal
         * @package Drupal\publisso_gold\Views
         */
        class Journal extends ControllerBase {
        
                /**
                 * @param $jrn_id
                 * @param null $tab
                 * @return array
                 */
                public function getJournalHTML($jrn_id, $tab = null) :array{
                        
                        $journal = JournalManager::getJournal($jrn_id);
                        $header = $this->getJournalHead($journal);
                        $body = $this->getJournalBody($journal, $tab);
                        
                        return [
                                'header' => $header,
                                'body' => $body,
                                '#cache' => ['max-age' => 0]
                        ];
                }
        
                /**
                 * @param $jrn_id
                 * @param null $tab
                 * @return string
                 */
                public function getJournalTitle($jrn_id, $tab = null) :string{
                        
                        if(!$tab || strtolower($tab) == 'current_volume') {
                                $journal = JournalManager::getJournal( $jrn_id, true);
                                if($journal) $title = html_entity_decode($journal->getElement('title'));
                        }
                        else{
                                $tabs = JournalManager::getHTMLViewTabs($jrn_id);
                                
                                $title = (string)$this->t($tabs[strtolower($tab)]['title']);
                        }
                        
                        return $title ?? '';
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param $tab
                 * @param array $addTabs
                 * @return array
                 */
                protected function getJournalBody(\Drupal\publisso_gold\Controller\Medium\Journal $journal, $tab, $addTabs = []) :array{
        
                        $templateBody = TemplateManager::load('publisso_journal_body');
                        if(!$templateBody->isLoaded()) return [];
                        
                        $this->setJournalTabs($journal, $templateBody, $tab, $addTabs);
                        $this->setJournalContent($journal, $templateBody, $tab);
                        
                        return [
                                '#type' => 'inline_template',
                                '#template' => $templateBody->parse()
                        ];
                }
                
                protected function getJournalHead(\Drupal\publisso_gold\Controller\Medium\Journal $journal) :array{
        
                        $templateHead = TemplateManager::load('publisso_journal_header');
                        if(!$templateHead->isLoaded()) return [];
        
                        $templateHead->setVar('jrn_id', $journal->getElement('id'));
        
                        foreach($journal->getElementKeys() as $key){
                                $templateHead->setVar($key, $journal->getElement($key));
                        }
                        
                        $templateHead->setVar('logo', '');
                        $templateHead->setVar('cover', '');
                        
                        $covers = ($journal->getElement('cover'));
                        $logos  = ($journal->getElement('logo' ));
                        
                        foreach($covers as $_){
                                $lnk = Url::fromRoute('publisso_gold.getPicture', ['type' => 'ul', 'id' => $_]);
                                $templateHead->appendToVar('cover', (!empty($templateHead->getVar('cover')) ? '<br>' : '' ).'<img style="max-width: 150px;" src="'.$lnk->toString().'" alt="COVER['.$_.']" title="'.$journal->getElement('title').'">');
                        }
        
                        foreach($logos as $_){
                                $lnk = Url::fromRoute('publisso_gold.getPicture', ['type' => 'ul', 'id' => $_]);
                                $templateHead->appendToVar('logo', (!empty($templateHead->getVar('cover')) ? '<br>' : '' ).'<img style="max-width: 170px;" src="'.$lnk->toString().'" alt="LOGO['.$_.']" title="'.$journal->getElement('title').'">');
                        }
                        
                        $issbn = [];
                        foreach(['issn', 'isbn'] as $_){
                                if(!empty($journal->getElement($_))) $issbn[] = $journal->getElement($_);
                        }
        
                        if(count($issbn)) $templateHead->setVar('issn_isbn', '<p>'.implode('<br>', $issbn).'</p>');
                        $linkClasses = [ 'link--calltoaction', 'inline', 'btn' ];
        
                        $urlPublish = Url::fromRoute( 'publisso_gold.journal_addarticle', ['jrn_id' => $journal->getElement('id')], ['attributes' => ['class' => $linkClasses]]);
        
                        if(\Drupal::service('session')->get('logged_in') === true && count($journal->getVolumes())) {
                                $templateHead->setVar( 'lnk_publish_article', Link::fromTextAndUrl( Markup::create( '<span>' . (string)$this->t( 'Submit' ) . '</span>' ), $urlPublish)->toString());
                        }
        
                        if ( \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'journalmanagement' ) ) {
                
                                $urlEdit = Url::fromRoute( 'publisso_gold.edit_journal', [ 'jrn_id' => $journal->getElement('id') ], [ 'attributes' => [ 'class' => $linkClasses ] ] );
                                $urlEditEB = Url::fromRoute( 'publisso_gold.journalmanagement_seteditorialboard', [ 'jrn_id' => $journal->getElement('id') ], [ 'attributes' => [ 'class' => $linkClasses ] ] );
                
                                $templateHead->setVar( 'lnk_edit_journal', Link::fromTextAndUrl( Markup::create( '<span>' . (string)$this->t( 'Edit journal' ) . '</span>' ), $urlEdit)->toString());
                                $templateHead->setVar( 'lnk_edit_journal_eb', Link::fromTextAndUrl( Markup::create( '<span>' . (string)$this->t( 'Edit edit. board' ) . '</span>' ), $urlEditEB)->toString());
                        }
                        
                        return [
                                '#type' => 'inline_template',
                                '#template' => $templateHead->parse()
                        ];
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param Template $templateBody
                 * @param null $tab
                 * @param array $addTabs
                 * @param array $addRouteParams
                 * @param string $routePrefix
                 * @return bool
                 */
                protected function setJournalTabs(\Drupal\publisso_gold\Controller\Medium\Journal $journal, Template &$templateBody, $tab = null, $addTabs = [], array $addRouteParams = [], $routePrefix = 'publisso_gold.journals_journal') :bool{
        
                        $templateBody->setVar('tabs', '');
                        $tabs = JournalManager::getHTMLViewTabs($journal->getElement('id'));
                        
                        if(count($addTabs)){
                                $tabs = array_merge($tabs, $addTabs);
                        }
        
                        $parameters = ['jrn_id' => $journal->getElement('id')];
                        
                        if(count($addRouteParams)){
                                $parameters = array_merge($parameters, $addRouteParams);
                        }
                        
                        uasort($tabs, ['self', 'sortTabs']);
                        
                        $templateTab = TemplateManager::load('publisso_journal_body_tab');
        
                        foreach($tabs as $tabName => $tabData){
                                
                                $routeName = $tabData['route_prefix'] ?? $routePrefix.'.'.strtolower($tabName);
                                
                                if(array_key_exists('route_parameters', $tabData) && is_array($tabData['route_parameters']))
                                        $parameters = array_merge($parameters, $tabData['route_parameters']);
                                
                                try {
                                        $url = Url::fromRoute( $routeName, $parameters );
                                        $link = Link::fromTextAndUrl( (string)$this->t( $tabData[ 'title' ] ), $url );
                                        $link = $link->toString();
                                }
                                catch(\Exception $e){
                                        error_log('ERROR: Can\'t build route "'.$routeName.'"! ('.$e->getMessage().')');
                                        $link = $tabData['title'];
                                }
                                
                                $templateTab->clear();
                                $templateTab->setVar('lnkTab', $link);
                                
                                if($tab == $tabData['title'] || strtolower($tab) == $tabName)
                                        $templateTab->setVar('tabClasses', 'active');
                                
                                $templateBody->appendToVar('tabs', $templateTab->parse());
                        }
                        
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setJournalContent(\Drupal\publisso_gold\Controller\Medium\Journal $journal, Template &$templateBody, $tab = null) :bool{
                        
                        $method = 'setJournalContent_'.str_replace([' ', '.', '-'], '_', strtolower($tab));
                        
                        try{
                                $this->$method($journal, $templateBody, $tab = null);
                        }
                        catch(\Error $e){
                                \Drupal::service('messenger')->addError('ERROR: Can\'t load all parts for this view. Please contact support!');
                                error_log('Error in file '.$e->getFile().' line '.$e->getLine().': '.$e->getMessage());
                                return false;
                        }
                        
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setJournalContent_current_volume(\Drupal\publisso_gold\Controller\Medium\Journal
                                                                    $journal, Template &$templateBody, $tab = null)
                :bool{
                        $templateBody->setVar('content', $journal->getElement('welcome'));
                        $templateBody->appendToVar('content', '<br>');
                        $volume = $journal->getCurrentVolume();
                        $articles = [];
                        if ( !$volume->hasIssues() ) {
                                foreach ( array_reverse( $volume->getArticles() ) as $jrna_id ) {
                        
                                        $article = $volume->getArticle( $jrna_id );
                                        $section = (string)t( 'Volume @year - @number', [ '@number' => $volume->getElement( 'number' ), '@year' => $volume->getElement( 'year' ) ] );
                                        $articles[ $section ][] = [ 'title' => $article->getElement( 'title' ), 'jrna_id' => $article->getElement( 'id' ), 'volume' => $volume->getElement( 'id' ), 'docno' => $article->getElement( 'docno' ) ];
                                }
                        }
                        else {
                                foreach ( array_reverse( $volume->getIssues() ) as $iss_id ) {
                        
                                        $issue = $volume->getIssue( $iss_id );
                        
                                        foreach ( array_reverse( $issue->getArticles() ) as $jrna_id ) {
                                
                                                $article = $volume->getArticle( $jrna_id );
                                                $section = (string)t( 'Volume @year - @vol_number, Issue @iss_number@iss_title', [ '@year' => $volume->getElement( 'year' ), '@vol_number' => $volume->getElement( 'number' ), '@iss_number' => $issue->getElement( 'number' ), '@iss_title' => $issue->getElement('title') ? ': '.html_entity_decode($issue->getElement('title')) : '' ] );
                                                $articles[ $section ][] = [ 'title' => $article->getElement( 'title' ), 'jrna_id' => $article->getElement( 'id' ), 'volume' => $volume->getElement( 'id' ), 'issue' => $issue->getElement( 'id' ), 'docno' => $article->getElement( 'docno' ) ];
                                        }
                                }
                        }
        
                        $c = 0;
                        foreach ( $articles as $_section => $_articles ) {
                
                                $tmpl_section = new \Drupal\publisso_gold\Controller\Template();
                                $tmpl_section->get( 'volume_content_current_volume_section' );
                                $tmpl_section->setVar( 'section', $_section );
                                $tmpl_section->setVar( 'articles', '' );
                
                                $cc = 1;
                                foreach ( $_articles as $_article ) {
                        
                                        $url = Url::fromRoute( 'publisso_gold.journals_journal.volume.article', [ 'jrn_id' => $journal->getElement( 'id' ), 'vol_id' => $_article[ 'volume' ], 'jrna_id' => $_article[ 'jrna_id' ] ] );
                        
                                        $link = Link::fromTextAndUrl( (string)t( 'Article @cc', [ '@cc' => (int)$_article[ 'docno' ] ] ), $url );
                                        $tmpl_section->appendToVar( 'articles', '<li>' . $link->toString() . ': ' . $_article[ 'title' ] . '</li>' );
                                        $cc++;
                                }
                
                                $templateBody->appendToVar( 'content', $tmpl_section->parse() );
                
                                if ( $c ) $templateBody->appendToVar( 'content', '<hr>' );
                
                                $c++;
                        }
                        
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setJournalContent_about(\Drupal\publisso_gold\Controller\Medium\Journal $journal,
                                                           Template &$templateBody, $tab = null) :bool{
                        
                        $templateBody->setVar('content', ($journal->getElement('about')));
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setJournalContent_manuscript_guidelines(\Drupal\publisso_gold\Controller\Medium\Journal $journal, Template &$templateBody, $tab = null)
                :bool{
                
                        $templateBody->setVar('content', ($journal->getElement('manuscript_guidelines')));
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setJournalContent_imprint(\Drupal\publisso_gold\Controller\Medium\Journal $journal,
                                                             Template &$templateBody, $tab = null) :bool{
                
                        $templateBody->setVar('content', ($journal->getElement('imprint')));
                        return true;
                }
        
                protected function setBookContent_Sidebars(\Drupal\publisso_gold\Controller\Medium\Book $book, Template &$templateBody) :bool{
                        
                        $texts          = \Drupal::service('publisso_gold.texts');
                        $theme          = strtolower($book->getElement('theme'));
                        $renderer       = \Drupal::service('renderer');
        
                        $_tmpl = TemplateManager::load($book->getElement('theme').'_book_content_sidebar_block');
                        
                        if(strtolower($book->getElement('theme')) == 'gms'){
                
                                $_tmpl->setVar('sbb_title', (string)$this->t('Search'));
                                $_tmpl->setVar('sbb_content', '');
                                $_tmpl->setVar('view_name', 'partner-list');
                                $_tmpl->appendToVar('sbb_title', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAAbCAYAAACKlipAAAADlElEQVRogeWZMW6zQBCFfZEUKXyN3MRXyAnc+hiOI1G7p0C+ABIN5RZuoKJCQrTzVw89P+8CxjHwJyOt5GC8zM6382bYbGxlVjft0i4sapulHTAzu5ZVB4I//0VbHEjdtLZ9f7Pt+5vtDmfLXfGnoawKyPb9zZLU2bWs7FpWS7u2iK0WSO6KP5klqwMSxZkXSN20lqTOojizKM46cL8N2qqBQLZyV9jH57d37A7n7je/wVYLBLv/WlZBGDr2x8t/D+alQMbISR+Qa1nZ/njpMmEsmCjOXiZlr5bIzf54sd3hfDf2x4slqZs8MeaN4qz3vj4guStuQGA+QPJJF3/3jP8+S1LXxedVYDa6YN2J++Nl0sQIzKNATl+nIBAE2Cdj+K5u2pvrPylhSeq6eV8OZHc4d7qtgZiyqFcAyV1xN78GqG7a4G+etdmBhB7OO9P3fqBdkdktEG5Zk9TdtbNjgKhMcJaoNDEs3UzXsuqkDbIcCu61rCxJXTd43kWBIHOGAsBzDBVj3rkfn9+DQHzSiWdocNh3fk4UZ8HWmcHVTeutU7yeWYBgl/OCEIhngPC8DAiLGgPEV6BzV3ivc/Yg0AwDWaFdHPxR3/XeWYBoUUfXwto8BYjWEN/uZSC4rkB4B/tqmkqp/g5/qz9cc4aUYEwNQQ1Gt8jxg1zyyTY3I1Gc3WZI3bTd8D3oJ4BwADAPA8HIXXEDRIOtht2sz9cgo6XnweviUwEFPwQEPptZd8SDwON67opOdVCX2OdgDVGbGwikCwGEQY44+L5rCB6AcC1iSeSXUOzuUIc2BIR3P87aEAvOCi4DiA8an7sMGQNEH+CDOkay4PDp69QNPfnVOXBNd5beB0gqWb51siooPL6X65AvVnw/B1r94szhz3XTjgfCi+LU1w5Eg4RAoe3laz5DQJAl2gHxvOoDPx/zsFSwrypZnPXsJ56h3WJIsrR2cNAxN75DU4L6YmaPAeFswG+4P1cgIWhjjjS4yUDgojjzniZooPkIRaUt1M6qPGmLjHkVCJ80AwgbWmgE3ecPx33TV8j7wGjfrnPo50f/CwjJAQDtotChhAIcKszqz5BM+9bFf/OLJdp6PXWGrz6pVP8WP37vM0jWmMwNgZnrOJ7hojuccmyzaiBoQ8eaSupcQLROhK6NsVUDeVRKYaz1cwBBnYCsQoqm+L5qIM8YsuUnT3v7DO8R/P4xxX4tELPpGbak/QPYnkh3mayzNQAAAABJRU5ErkJggg=="><br>');
                
                                $form = \Drupal::formBuilder()->getForm('\\Drupal\\publisso_gold\\Form\\PubmedSearch', ['bk_id' => $book->getElement('id')]);
                                $_tmpl->setVar('sbb_content', $renderer->render($form).'');
                
                                $templateBody->appendToVar( 'sidebar', $_tmpl->parse() );
                        }
        
                        if(count($book->readSustainingMembers())){
                
                                $_tmpl->setVar('sbb_title', (string)$this->t( $texts->get( "book.content.sidebar.$theme.sustaining_members.title")));
                                $_tmpl->setVar('sbb_content', '');
                                $_tmpl->setVar('view_name', 'partner-list');
                
                                foreach($book->readSustainingMembers() as $bsm_id){
                        
                                        $urlPicture = Url::fromRoute('publisso_gold.getPicture', [ 'type' => 'bsm', 'id' => $bsm_id ]);
                                        $_tmpl->appendToVar('sbb_content', '<img src="' . $urlPicture->toString().'"><br>');
                                }
                
                                $templateBody->appendToVar('sidebar', $_tmpl->parse());
                        }
                        
                        return true;
                }
        
                /**
                 * @param $structure
                 * @param $level
                 * @return string
                 */
                protected function parseChapterlistStructure ($structure, $level) :string{
                
                        $markup = '';
                
                        if (count( $structure ) > 0 ) {
                        
                                $markup .= '<ul style="list-style: none;">';
                                
                                if (is_array($structure)){
                                        foreach ($structure as $item) {
                                        
                                                $childs = $item['childs'];
                                                $item   = $item['item'];
                                                $link   = null;
                                        
                                                $markup .= '<li>';
                                        
                                                if (!empty($item->getElement('link'))){
                                                
                                                        $link = $item->getElement('link');
                                                
                                                        if(substr($link, 0, 9) == 'intern://') {
                                                        
                                                                $link = str_replace('intern://', '', $link);
                                                                list( $sub_medium, $sub_medium_id ) = explode('/', $link);
                                                        
                                                                $chapter = new Bookchapter($sub_medium_id, true);
                                                                $url = $chapter->getChapterLink('url');
                                                                $link = Link::fromTextAndUrl($item->getElement('title'), $url);
                                                        }
                                                        else{
                                                                if(UrlHelper::isValid($link)){
                                                                        $url = Url::fromUri($link);
                                                                        $url->setOptions(['attributes' => ['target' => '_blank']]);
                                                                        $link = Link::fromTextAndUrl($link, $url);
                                                                }
                                                                else{
                                                                        $link = null;
                                                                }
                                                        }
                                                }
                                        
                                                $markup .= $link ? $link->toString() : $item->getElement('title');
                                        
                                                if($item->getElement('description') != '') {
                                                        $markup .= '<div class="description">' . $item->getElement('description') . '</div>';
                                                }
                                        
                                                if($item->getElement('authors') != '') {
                                                        $markup .= '<div class="authors">' . $item->getElement('authors') . '</div>';
                                                }
                                        
                                                if (count($childs) > 0) {
                                                        $markup .= $this->parseChapterlistStructure($childs, $level + 1);
                                                }
                                        
                                                $markup .= '</li>';
                                        }
                                }
                                $markup .= '</ul>';
                        }
                
                        return $markup;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param Template $templateBody
                 * @param null $tab
                 */
                protected function setJournalContent_archive(\Drupal\publisso_gold\Controller\Medium\Journal
                                                             $journal, Template &$templateBody, $tab = null){
                        
                        if ( count( $journal->getVolumes() ) > 1 || $journal->getControlElement('show_empty_tab_archive')) {
                        
                                $archiveVolumes = $journal->getVolumes();
                                unset($archiveVolumes[$journal->getCurrentVolumeID()]);
                                
                                foreach ( array_reverse( $archiveVolumes ) as $_volume ) {
                                
                                        
                                        #$_volume = $journal->getVolume( $_ );
                                        
                                        if ( $_volume->hasAnyArticles() ) {
                                                $templateBody->appendToVar( 'content', Link::fromTextAndUrl(
                                                        (string)$this->t( 'Volume @year - @number', [ '@year' => $_volume->getElement( 'year' ), '@number' => $_volume->getElement( 'number' ) ] ), Url::fromRoute( 'publisso_gold.journals_journal.volume', [ 'jrn_id' => $journal->getElement( 'id' ), 'vol_id' => $_volume->getElement('id') ]
                                                ))->toString());
                                        
                                                $templateBody->appendToVar( 'content', '<br>' );
                                        }
                                        
                                }
                        
                                if(!count($archiveVolumes)) $templateBody->appendToVar( 'content', (string)
                                $this->t
                                ($journal->getControlElement('empty_tab_archive_message')) );
                        }
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param Template $templateBody
                 * @param null $tab
                 */
                protected function setJournalContent_contact(\Drupal\publisso_gold\Controller\Medium\Journal
                                                                     $journal, Template &$templateBody, $tab = null){
                        $templateBody->setVar('content', ($journal->getElement('contact')));
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setJournalContent_editorial_board(\Drupal\publisso_gold\Controller\Medium\Journal
                                                                     $journal, Template &$templateBody, $tab = null)
                :bool{
                        
                        $content_eb = '<div class="row flex-row-sm">';
                        $modal_id = 0;
                        $c = 0;
        
                        $eb = $journal->readEditorialBoard();
                        if($journal->getEbAbAnnotation( 'main' )) $content_eb .= '<tt>' . $journal->getEbAbAnnotation( 'main' ) . '</tt>';
                        
                        $tmpl_eb = TemplateManager::load('publisso_journal_content_author');
        
                        foreach ( $eb as $uid => $user ) {
                                
                                $modal_id++;
                                $tmpl_eb->clear();
                
                                $tmpl_eb->setVar( 'txt_more', (string)t( 'more' ) );
                                $tmpl_eb->setVar( 'txt_address', (string)t( 'Address' ) );
                                $tmpl_eb->setVar( 'txt_contact', (string)t( 'Contact' ) );
                                $tmpl_eb->setVar( 'modal_id', 'm_editors_' . ( $modal_id ) );
                                $tmpl_eb->setVar( 'author_name', implode( ' ', [ $user->profile->getElement( 'graduation' ), $user->profile->getElement( 'firstname' ), $user->profile->getElement( 'lastname' ), $user->profile->getElement( 'graduation_suffix' ) ] ) );
                                $tmpl_eb->setVar( 'author_institute', $user->profile->getElement( 'institute' ) );
                                $tmpl_eb->setVar( 'author_department', $user->profile->getElement( 'department' ) );
                                $tmpl_eb->setVar( 'author_street', $user->profile->getElement( 'street' ) );
                                $tmpl_eb->setVar( 'author_notes', $user->tempInfo[ 'eb_notes' ] . '<br><br>' );
                                $tmpl_eb->setVar( 'author_postal_code', $user->profile->getElement( 'postal_code' ) );
                                $tmpl_eb->setVar( 'author_city', $user->profile->getElement( 'city' ) );
                                $tmpl_eb->setVar( 'author_country', \Drupal::service('publisso_gold.tools')->getCountry($user->profile->getElement( 'country' ) ) );
                                $tmpl_eb->setVar( 'author_telephone', $user->profile->getElement( 'telephone' ) );
                                $tmpl_eb->setVar( 'author_email', $user->profile->getElement( 'email' ) );
                                $tmpl_eb->setVar( 'author_pic_link', '/system/getPicture/up/' . $user->getId() );
                                $tmpl_eb->setVar( 'author_address', implode( ', ', array_filter( [ implode( ' ', array_filter( [ $tmpl_eb->getVar( 'author_postal_code' ), $tmpl_eb->getVar( 'author_city' ) ] ) ), $tmpl_eb->getVar( 'author_country' ) ] ) ) );
                                
                                if ( $c % 2 == 0 ) {
                                        $tmpl_eb->prependHTML( '<div class="row flex-row-sm">' );
                                }
                
                                if ( $c % 2 != 0 ) {
                                        $tmpl_eb->appendHTML( '</div><hr>' );
                                }
                
                                $content_eb .= $tmpl_eb->parse();
                                $c++;
                        }
        
                        if ( $c > 0 && $c % 2 != 0 ) {
                                $content_eb .= '</div>';
                        }
        
                        //advisory boards
                        $advisoryBoards = $journal->readAdvisoryBoard();
        
                        foreach ( $advisoryBoards as $boardName => $users ) {
                
                                $content_eb .= '<h4><center><i>' . $boardName . '</i></center></h4>';
                                $content_eb .= '<tt>' . $journal->getEbAbAnnotation( $boardName ) . '</tt>';
                                $c = 0;
                
                                foreach ( $users as $uid => $user ) {
                        
                                        $modal_id++;
                                        $tmpl_eb->clear();
                                        $tmpl_eb->get( 'publisso_gold_journal_content_author' );
                        
                                        $tmpl_eb->setVar( 'txt_more', (string)t( 'more' ) );
                                        $tmpl_eb->setVar( 'txt_address', (string)t( 'Address' ) );
                                        $tmpl_eb->setVar( 'txt_contact', (string)t( 'Contact' ) );
                        
                                        $tmpl_eb->setVar( 'modal_id', 'm_editors_' . ( $modal_id ) );
                        
                                        $tmpl_eb->setVar( 'author_name', implode( ' ', array_filter( [ $user->profile->getElement( 'graduation' ), $user->profile->getElement( 'firstname' ), $user->profile->getElement( 'lastname' ), $user->profile->getElement( 'graduation_suffix' ) ] ) ) );
                                        $tmpl_eb->setVar( 'author_institute', $user->profile->getElement( 'institute' ) );
                                        $tmpl_eb->setVar( 'author_department', $user->profile->getElement( 'department' ) );
                                        $tmpl_eb->setVar( 'author_street', $user->profile->getElement( 'street' ) );
                                        $tmpl_eb->setVar( 'author_notes', $user->tempInfo[ 'eb_notes' ] . '<br><br>' );
                                        $tmpl_eb->setVar( 'author_postal_code', $user->profile->getElement( 'postal_code' ) );
                                        $tmpl_eb->setVar( 'author_city', $user->profile->getElement( 'city' ) );
                                        $tmpl_eb->setVar( 'author_country', getCountry( $user->profile->getElement( 'country' ) ) );
                                        $tmpl_eb->setVar( 'author_telephone', $user->profile->getElement( 'telephone' ) );
                                        $tmpl_eb->setVar( 'author_email', $user->profile->getElement( 'email' ) );
                                        $tmpl_eb->setVar( 'author_pic_link', '/system/getPicture/up/' . $user->getId() );
                                        $tmpl_eb->setVar( 'author_address', implode( ', ', array_filter( [ implode( ' ', array_filter( [ $tmpl_eb->getVar( 'author_postal_code' ), $tmpl_eb->getVar( 'author_city' ) ] ) ), $tmpl_eb->getVar( 'author_country' ) ] ) ) );
                        
                                        if ( $c % 2 == 0 ) {
                                                $tmpl_eb->prependHTML( '<div class="row flex-row-sm">' );
                                        }
                        
                                        if ( $c % 2 != 0 ) {
                                                $tmpl_eb->appendHTML( '</div><hr>' );
                                        }
                        
                                        $content_eb .= $tmpl_eb->parse();
                                        $c++;
                                }
                
                                if ( $c > 0 && $c % 2 != 0 ) {
                                        $content_eb .= '</div>';
                                }
                        }
                        
                        #echo '<pre>'.$content_eb.'</pre>'; exit();
                        
                        $templateBody->setVar( 'content', $content_eb );
                        return true;
                }
        
                /**
                 * @param array $a
                 * @param array $b
                 * @return int
                 */
                protected function sortTabs(array $a, array $b){
                        return $a['weight'] <=> $b['weight'];
                }
        }
