<?php
        
        
        namespace Drupal\publisso_gold\Views;
        
        
        use Drupal\Core\Link;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\journalarticle;
        use Drupal\publisso_gold\Controller\Manager\ArticleManager;
        use Drupal\publisso_gold\Controller\Manager\JournalManager;
        use Drupal\publisso_gold\Controller\Manager\TemplateManager;
        use Drupal\publisso_gold\Controller\Template;
        use Drupal\publisso_gold\Controller\User;

        /**
         * Class Article
         * @package Drupal\publisso_gold\Views
         */
        class Article extends Volume {
                
                protected $article;
                protected $lang;
        
                /**
                 * Article constructor.
                 */
                public function __construct(){
                        
                        $this->lang = 'en';
                        
                        if(\Drupal::service('language_manager')->getCurrentLanguage()->getId()) $this->lang = \Drupal::service('language_manager')->getCurrentLanguage()->getId();
                        
                        $request = \Drupal::requestStack()->getCurrentRequest();
                        if($request->query->has('lang')) $this->lang = $request->query->get('lang');
                }
        
                /**
                 * @param $jrna_id
                 *
                 * @return string
                 */
                public function getArticleTitle($jrna_id){
                
                        if(null === ($article = ArticleManager::getArticle($jrna_id))) return 'NULL';
                        return $article->getElement('title', $this->lang);
                }
        
                /**
                 * @param      $jrn_id
                 * @param      $vol_id
                 * @param      $jrna_id
                 * @param null $tab
                 *
                 * @return array
                 */
                public function getArticleHTML($jrn_id, $vol_id, $jrna_id, $tab = null){
                        
                        if(null === ($this->article = ArticleManager::getArticle($jrna_id))) return [];
                        $journal = JournalManager::getJournal($jrn_id);
                        $this->volume = new \Drupal\publisso_gold\Controller\Volume($this->article->getElement('volume'));
                        
                        $header = $this->getJournalHead($journal);
                        $body = $this->getJournalBody($journal, $tab);
                
                        return [
                                'header' => $header,
                                'body' => $body,
                                '#cache' => ['max-age' => 0]
                        ];
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param $tab
                 * @param array $addTabs
                 * @return array
                 */
                protected function getJournalBody(\Drupal\publisso_gold\Controller\Medium\Journal $journal, $tab, $addTabs = []) :array{
                        
                        $templateBody = TemplateManager::load('publisso_journal_body');
                        if(!$templateBody->isLoaded()) return [];
                        
                        if($this->article) {
                                $addTabs = [
                                        'article' => [
                                                'title'            => (string)$this->t( 'Article' ),
                                                'weight'           => 0,
                                                'show_on_empty'    => 1,
                                                'route_prefix'     => 'publisso_gold.journals_journal.volume.article',
                                                'route_parameters' => [
                                                        'vol_id'  => $this->volume->getElement( 'id' ),
                                                        'jrna_id' => $this->article ? $this->article->getElement( 'id' ) : null
                                                ]
                                        ]
                                ];
                        }
                        
                        if($this->article->getElement('volume') != $journal->getCurrentVolume()->getElement('id')){
                                
                                $addTabs['volume'] = [
                                        'title' => implode('-', [$this->volume->getElement('year'),
                                                                 $this->volume->getElement
                                                                 ('number')]),
                                        'weight' => 1.5,
                                        'show_on_empty' => 0,
                                        'route_parameters' => [
                                                'vol_id' => $this->volume->getElement('id')
                                        ]
                                ];
                        }
                        
                        $this->setJournalTabs($journal, $templateBody, $tab, $addTabs, [], 'publisso_gold.journals_journal.volume.article');
                        $this->setJournalContent($journal, $templateBody, $tab);
                
                        return [
                                '#type' => 'inline_template',
                                '#template' => $templateBody->parse()
                        ];
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param Template $templateBody
                 */
                public function setJournalContent_article(\Drupal\publisso_gold\Controller\Medium\Journal $journal, Template &$templateBody){
                        
                        $article        = $this->article;
                        $lang           = $this->lang;
                        $origWorkflow   = \Drupal::service('publisso_gold.tools')->getWorkflowItem( $article->getElement( 'wfid', $lang ) );
                        $tempstore      = \Drupal::service( 'user.private_tempstore' )->get( 'publisso_gold' );
                        
                        if($article->getElement( 'article_type', $lang, false ) || $article->getElement( 'sub_category', $lang, false )) {
                                
                                $templateBody->appendToVar('content', '<div>');
        
                                if ( $article->getElement( 'article_type', $lang, false ) ) {
                                        $type = implode( ' ', array_map( 'ucfirst', explode( ' ', \Drupal::service( 'publisso_gold.tools' )->getArticleType( $article->getElement( 'article_type', $lang, false ) ) ) ) );
                                        $templateBody->appendToVar( 'content', '<div id="articleContainerArticleType">' . ( (string)$this->t( $type ) ) . '</div>' );
                                }
        
                                if ( $article->getElement( 'sub_category', $lang, false ) ) {
                                        $templateBody->appendToVar( 'content', '<div id="articleContainerSubCategory">' . ( (string)$this->t( $article->getElement( 'sub_category', $lang, false ) ) ) . '</div>' );
                                }
        
                                $templateBody->appendToVar('content', '</div><br clear="all">');
                        }
                        //title
                        $templateBody->appendToVar( 'content', '<h1>' . $article->getElement( 'title', $lang, false ) . '</h1>' );
                        
                        //authors
                        $authors = [];
                        $affiliations = [];
                        $strAuthors = '';
                        $strAffiliations = '';
                        $article_authors = json_decode( $this->article->getElement( 'authors' ) );

                        foreach ( $article_authors as $_ ) {

                                $weight = $_->weight ? $_->weight : 0;
                                while ( array_key_exists( $weight, $authors ) ) $weight++; //falls Autoren mit gleicher Gewichtung angegeben sind, damit sie sich nicht gegenseitig überschreiben
                                $authors[ $weight ] = $_;
                        }

                        ksort( $authors );

                        foreach ( $authors as $_ ) {

                                if ( property_exists( $_, 'uid' )) {
                                        $user = new User( $_->uid );
                                        $user->profile->loadSnapshot( $_->profile_archive_id );
                                        $affiliations[] = $user->profile->getAffiliation();
                                        $_->email = $user->profile->getElement('email');
                                }
                                else {
                                        $affiliations[] = $_->affiliation;
                                }

                                if(property_exists($_, 'add_affiliations')){

                                        $addAffiliations = (array)$_->add_affiliations;
                                        usort($addAffiliations, function($a, $b){
                                                return $a->weight <=> $b->weight;
                                        });

                                        $addAffiliations = array_filter($addAffiliations, function($v){
                                                if(property_exists($v, 'show')) return !!$v->show;
                                                return true;
                                        });

                                        foreach($addAffiliations as $addAffiliation) {

                                                if(is_object($addAffiliation))
                                                        $affiliations[] = $addAffiliation->affiliation;
                                                else
                                                        $affiliations[] = $addAffiliation;
                                        }
                                }
                        }

                        $affiliations = array_keys( array_flip( $affiliations ) ); //make affs unique


                        for ( $c = 1; $c <= count( $affiliations ); $c++ ) {
                                if ( $affiliations[ $c - 1 ] ) $strAffiliations .= ( !empty( $strAffiliations ) ? '<br>' : '' ) . '<sup>' . $c . '</sup>&nbsp;' . $affiliations[ $c - 1 ];
                        }

                        foreach ( $authors as $_ ) {

                                $affiliation = $_->affiliation;
                                $affKey = [];

                                if(property_exists($_, 'add_affiliations')){
                                        foreach ($_->add_affiliations as $addAffiliation){
                                                $affKey[] = array_search($addAffiliation->affiliation, $affiliations) + 1;
                                        }
                                }

                                $affKey[] = array_search( $affiliation, $affiliations ) + 1;
                                $affKey = array_unique($affKey);
                                sort($affKey);
                                $str_ = implode( ' ', [ $_->firstname, $_->lastname ] ) . ( count( $affKey ) ? " <sup>".implode(',', $affKey)."</sup>" : "" );
                                $isCorresponding = property_exists( $_, 'is_corresponding' ) ? !!$_->is_corresponding : false;

                                if ( $isCorresponding && property_exists($_, 'email')) {

                                        $data = md5( $_->email );

                                        if ( null === ( $encdata = $tempstore->get( 'encdata' ) ) ) {
                                                $encdata = serialize( [] );
                                        }
                                        $encdata = unserialize( $encdata );
                                        $encdata[ $data ] = $_->email;

                                        $tempstore->set( 'encdata', serialize( $encdata ) );
                                        $str_ = '<span class="glyphicon glyphicon-envelope"></span>&nbsp;<a class="encmailadd" data="' . $data . '" href="?subject=' . $this->article->getElement( 'title', $lang ?? null ) . '">' . $str_ . '</a>';
                                }

                                $strAuthors .= ( !empty( $strAuthors ) ? '<br>' : '' ) . '<span class="chapterAuthor">' . $str_ . '</span>';
                        }

                        $strAuthors .= '<br>' . implode( '<br>', array_filter( json_decode( $this->article->getElement( 'corporation' ), 1 ) ?? [] ) ) . '<br><br>' . $strAffiliations;

                        $templateBody->appendToVar( 'content', "$strAuthors<br><br>" );

                        //content
                        if(empty($article->getElement('references', $lang, true))) {
                                list( $_fulltext, $_references ) = \Drupal::service( 'publisso_gold.tools' )->getReferencesFromText( base64_decode( $article->getElement( 'article_text', $lang, false ) ) );
                                if($origWorkflow) $inactiveReferences = \Drupal::service( 'publisso_gold.tools' )->getInactiveReferences( $_references, $origWorkflow->getDataElement( 'references' ) );
                                $article->setElement('references',  implode("\n", [$_references, $inactiveReferences]), $lang);
                                $article->setElement('article_text', base64_encode($_fulltext), $lang);
                        }
                        else{
                                $_references = base64_decode($article->getElement('references', $lang, false));
                                $_fulltext = base64_decode($article->getElement('article_text', $lang, false));
                        }
        
                        //abstract
                        $templateBody->appendToVar( 'content', !empty( $article->getElement( 'abstract', $lang, false ) ) ? '<h3>' . ( (string)$this->t( 'Abstract' ) ) . '</h3>' . base64_decode( $article->getElement( 'abstract', $lang, false ) ) : '<br><br>' );
        
                        //keywords
                        $keywords = array_filter( json_decode( $article->getElement( 'keywords', $lang, false )) ?? []);
                        if ( count( $keywords ) ) {
                
                                $keywords = implode( ', ', $keywords );
                                $templateBody->appendToVar( 'content', "<hr><div id='keywords'><h3>" . ( htmlentities((string)$this->t( 'Keywords' ) )) . "</h3>$keywords</div>" );
                        }
        
                        $templateBody->appendToVar( 'content', '<hr>' );
                        $templateBody->appendToVar( 'content', $_fulltext );
        
        
                        if ( !empty( $_references . ($inactiveReferences ?? '') ) ) {
        
                                $templateBody->appendToVar( 'content', '<hr>' );
                                $templateBody->appendToVar( 'content', '<h3>' . ( (string)t( 'References' ) ) . '</h3>' );
                                $templateBody->appendToVar( 'content', nl2br( $_references ) . '<br>' . nl2br( $inactiveReferences ?? '' ) );
                        }
        
                        //attachments
                        $fileCnt = 0;
                        if ( !empty( $article->getElement( 'files', $lang, false ) ) ) {
        
                                $templateBody->appendToVar( 'content', '<hr>' );
                                $templateBody->appendToVar( 'content', '<h3>' . ( (string)t( 'Attachments' ) ) . '</h3>' );
                                $templateBody->appendToVar( 'content', '<table class="articlesAttachments">' );
                                $templateBody->appendToVar( 'content', '<tr>' );
                
                                foreach ( array_filter( explode( ',', $article->getElement( 'files', $lang, false ) ) ) as $bid ) {
                        
                        
                                        $blob = new \Drupal\publisso_gold\Controller\Blob( $bid );
                        
                                        if ( $blob->getId() ) {
                                
                                                $name = $article->getDOISuffix( $lang ) . '_att' . ( ++$fileCnt );
                                                $meta = $blob->meta;
                                
                                                if ( $blob->meta[ 'name' ] != $name ) {
                                                        $meta[ 'name' ] = $name;
                                                        $blob->updateMeta( $meta );
                                                }
                                
                                                //MainRow
                                                $templateBody->appendToVar( 'content', '<tr>' );
                                
                                                $link = Link::fromTextAndUrl( $blob->meta[ 'description' ], Url::fromRoute( 'publisso_gold.getFile', [ 'fid' => $blob->getId() ], [ 'attributes' => [ 'target' => '_blank' ] ] ) );
                                
                                                //Name
                                                $templateBody->appendToVar( 'content', '<td>' );
                                                $templateBody->appendToVar( 'content', '<strong>' . (string)$this->t( 'Attachment @attno', [ '@attno' => ++$fileCnt ] ) . '</strong>' );
                                                $templateBody->appendToVar( 'content', '</td>' );
                                
                                                //Link
                                                $templateBody->appendToVar( 'content', '<td>' );
                                                $templateBody->appendToVar( 'content', $link->toString() );
                                                $templateBody->appendToVar( 'content', '</td>' );
        
                                                $templateBody->appendToVar( 'content', '</tr>' );
                                
                                                //Name-Row
                                                $templateBody->appendToVar( 'content', '<tr>' );
                                                $templateBody->appendToVar( 'content', '<td>' );
                                                $templateBody->appendToVar( 'content', 'Name:' );
                                                $templateBody->appendToVar( 'content', '</td>' );
                                                $templateBody->appendToVar( 'content', '<td>' );
                                                $templateBody->appendToVar( 'content', $blob->meta[ 'name' ] );
                                                $templateBody->appendToVar( 'content', '</td>' );
                                                $templateBody->appendToVar( 'content', '</tr>' );
                                
                                                //Type-Row
                                                $templateBody->appendToVar( 'content', '<tr>' );
                                                $templateBody->appendToVar( 'content', '<td>' );
                                                $templateBody->appendToVar( 'content', 'Type' );
                                                $templateBody->appendToVar( 'content', '</td>' );
                                                $templateBody->appendToVar( 'content', '<td>' );
                                                $templateBody->appendToVar( 'content', $blob->type );
                                                $templateBody->appendToVar( 'content', '</td>' );
                                                $templateBody->appendToVar( 'content', '</tr>' );
                                
                                                //MD5-Row
                                                $templateBody->appendToVar( 'content', '<tr>' );
                                                $templateBody->appendToVar( 'content', '<td>' );
                                                $templateBody->appendToVar( 'content', 'MD5' );
                                                $templateBody->appendToVar( 'content', '</td>' );
                                                $templateBody->appendToVar( 'content', '<td>' );
                                                $templateBody->appendToVar( 'content', $blob->getMD5() );
                                                $templateBody->appendToVar( 'content', '</td>' );
                                                $templateBody->appendToVar( 'content', '</tr>' );
                                
                                                //SHA256-Row
                                                $templateBody->appendToVar( 'content', '<tr>' );
                                                $templateBody->appendToVar( 'content', '<td>' );
                                                $templateBody->appendToVar( 'content', 'SHA256' );
                                                $templateBody->appendToVar( 'content', '</td>' );
                                                $templateBody->appendToVar( 'content', '<td>' );
                                                $templateBody->appendToVar( 'content', $blob->getSHA256() );
                                                $templateBody->appendToVar( 'content', '</td>' );
                                                $templateBody->appendToVar( 'content', '</tr>' );
                                
                                
                                        }
                                }
        
                                $templateBody->appendToVar( 'content', '</tr>' );
                                $templateBody->appendToVar( 'content', '</table>' );
                        }
        
                        //liegt der Artikel in der Originalsprache vor? Wenn nicht, dann einen entsprechenden
                        // Hinweis mit dem zugehörigen Link einblenden
                        if ( $lang != $article->getOrigLanguage() ) {
                
                                $language = \Drupal::service( 'language_manager' );
                                $language = $language->getLanguage( $article->getOrigLanguage() ?? 'en' )->getName();
                
                                $url = Url::fromRoute( \Drupal::routeMatch()->getRouteName(), array_merge( \Drupal::routeMatch()->getParameters()->all(), [ 'lang' => $article->getOrigLanguage() ] ) );
                                $link = Link::fromTextAndUrl( (string)$this->t( $language ), $url );
                                $txt = (string)$this->t( 'The full/original article is available in @link', [ '@link' => $link->toString() ] );
                
                                $templateBody->appendToVar( 'content', "<div class='alertalert-success'>$txt</div>");
                        }
        
                        $this->setArticleContent_Sidebars($templateBody);
                }
        
                /**
                 * @param Template $templateBody
                 */
                public function setArticleContent_Sidebars(Template &$templateBody){
                        
                        $article = $this->article;
                        $journal = JournalManager::getJournal($article->getElement('jrnid'), true);
                        $__tmpl = TemplateManager::load('publisso_article_sidebar');
                        
                        if($journal->getControlElement('search_livivo_enabled')) {
                                //Livivo Search
                                $renderer1 = \Drupal::service( 'renderer' );
                                $form = \Drupal::formBuilder()->getForm( '\\Drupal\\publisso_gold\\Form\\LivivoSearch', [ 'bk_id' => $journal->getElement( 'id' ), 'term' => '' ] );
                                $__tmpl->clear();
                                $__tmpl->setVar( 'sbb_content', $renderer1->render( $form ) . '' );
                                $__tmpl->setVar( 'sbb_title', ( (string)$this->t( 'Search' ) ) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=" data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAAA0CAYAAAAKVi8oAAAY9XpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarZpnlmO3doX/YxQeAtJBGA7iWp6Bh+9vg+yW1JJesF+XVGSRl7jACTsAdOd//vu6/+Jf8S27bLWVXornX+65x8GT5j//Po/B5/f7/bPxfS/88XWX8veNyEuJx/T5s5zv9YPX7bcP1O/1Yf7xdVfXd5z2Hej7xo8Bk+4cefK9rn0HSvHzevj+7fr3cyP/bjnf/+f+vmafh1//zpVgbGO8FF08KSTP76a7JGaQeho8Fn7HVKJe0XNLid8p2V/Hzv18+kvwevrr2PnxvSL9MRTOl+8F5ZcYfV8P9texexH6/YzCj6fxj2+ME37c4k+xu3e3e89ndSMXIlXcd1H+O8R7xoWTUKb3scJP5X/jeX0/nZ/GEhcZ22Rz8rNc6CES7Rty2GGEG857XGExxRxPrDzGuGJ6r7VUY4/rJSXrJ9xYSc92qZGbRdYSL8efcwnvvv3db4XGnXfgyhgYLPCJP/24v3rx//Lzc6B7VbohKJg9vVjxO6qmmYYyp99cRULC/cbUXnzfj/uZ1t/+KbGJDNoLc2OBw8/PENPCb7WVXp4T15nPzn9aI9T9HYAQcW9jMiGRAV9CslCCrzHWEIhjIz+DmceU4yQDwSzu4C65SXRCjS3q3nymhndttPh5GWghEUajVFJDA5GsnI36qblRQ8OSZWdmxao16zZKKrlYKaUWYdSoqeZqtdRaW+11tNRys1Zaba31NnrsCQizXnp1vfXex+Cmg6EHnx5cMcaMM808bZZZZ5t9jkX5rLxslVVXW32NHXfatP8uu7rddt/0w6GUTj52yqmnnX7GpdZuuvnaLbfedvsdP7P2zeofsxZ+ydw/zlr4Zk0Zy++6+lvWeLnWH0MEwYkpZ2Qs5kDGqzJAQUflzLeQc1TmlDPfo0AqkrVgSs4OyhgZzCdEu+Fn7n7L3D/Mm7P8b+Ut/l3mnFL3n8icU+q+mftz3v4ia3s8Rvl0o7pQMfXpAmxccNqIbYiT/uXHM6yesG4bexGZ7MYCNcdtjQQDiGOstC3lPs8O5dyz5mg5nLz2rLXsffTB3M7wPewcxiqhdRLhqLoVS583lubPhal6sJvHzH7UaEV1cs/lxRZOyvf2GnTLWfq9M+6cygS9rwtpn006q5U7SrmtlGzURrnpxNCN0vGHj9XB3e8uc6U6JjmcLOXsPPOdiei4Ode8+8R0zp6NSzdosZEWNteObVu7M5Afu9uY6YVD2w7H+k221lDM4oypOv+eAov/z8e/Hkg1WqtRvHGtVaUUVlcQA2/ukK+/Z1qkUyzvcGnK5tallAlB5L3Uxjqjt3XA8GZ7+TskLsZj17THXEbkzhikmUDPMmijCQJW9NFgtTGP9U2/ft+jsF+iaaR29m1r7/eOqndSGWny9E7qg9+JqDa3N/dP/eXDr0n33jF3vaVTSGnTex443yc0DdRZVi56Bly1dG+2sSuFsFyp74IWK9z4nu9RbUGho131JBnNtNraNLO37KHcsGZs5mdDEtHEq8QxHVM7msGxN/XGuq2GmdJMZ3vbl84LfZVz+vb1mIGmNRYKFo6Zwe/NWlgqA91FN9PKk/Z/igCej5FyioncAAvprKDpp9NZjIJyT7Bz/BosjYoHpopb+lCpdHlbZLIwDm1wMx8kkvwGZ/3nAzTenfu9O1/E5iUwK3YPfAD+APC0vhTZFDofA73G/DEm8Xujev8dl2GpDPouNdqKcmi5qCDDzBaySZ5SXLfk2YGhxb3T2oATsz67kd9LDYSwhxoDbEizdtv5UrZRH3egr2UVly0YeA5r0drOhNc2MN/Bym+59xj58KjrIjJU6GmcRS2G299A9bcZ/fWjDSFY45M3KN4hI5TarbAU2on+AHjLRNYMA5VJzhrnhAlE1N0luuLqYSJRN7k8GeY5SdMbnfS3lAG8ABpk4lTAYldW5065TqgIsKDBMutXUG3cfu4qcxQAnZyx/kN1xDaXEAn82a1DCoUkDdcTywXgqbGjXK5+Az1ALTJatIECO7FMGgZeCnNuxj/GiKdX4LcDmJQE3W9LMle635/msUq727Hd2opmUN/IrHPXLCJhmAwgtETx9wCH2wS9H5QW11lfhErbLdJHudMPKY866460SwIvNrLKWOaGmZGwmxQ+jGhzHJ+WMeWR3a6Vkf0Z8PupQBWBXMNuotFDuCyfiXZJZqo52gob9G7TEikrDNkB4REgyJVCRArkSsESoTJAwwuY0dFxnkv77GgdzIxrhlzWXtWOpzWGlWCK+QDbd3Ex05UTojdqpmIqZ4u7HcI/GtEPadDfRHgBvsy4QCUTjDrwIGKUm9IkcPFwoQo4oAsSEFYePd9OQnNcp7RzytJtGiW4yu0LYzlXABYG7UDFdmjdU34BggS5mXJEeozdIBwERI5wKYhFN5errB7QG9pjYgkf8cp+F1FVQ1REpTC7szaD902UER2tIakMvEXobsMoU9kUNBkmbtTXJFadnM/QN8ntnjvCeucWKHtTik3r4AK/RPeMM+8B5i868iF0oA4o6nyERg+Ql96m7HPpHldDi5B3H6gm4s/seOSCOXafqRDyvApdSheVTfcuVQfCbRBsqEcDwkIJ4lvOIpNBOk7NON4H7adTwgFak+yoYOSA1RsoQA/d14coqQ0PEjNiFQhEdo2BmCvV2BnqEQ1CEZiFUhROkIGQA1hXk77wHURw0Xiw2JkUyUSvkgAXw0AE9yPwIYPpzxROi6BPFtksA3gGbsBIpAUMi05hvu/2jrZMyFqqJOZNcmICaKjOznwRTDgwGrByZ3oV/0oEqJbB7ABI2pBxkLHTYFrwBxYElRpwgdieSMlID9CzYDdiiHoFryesTqcBe7mA8wRmdeInWGw32HbCdCBKXVww1nSt5G+hDBur2Ua2mDHDnsNdYu0pTmSf9YYcN4lCrpsBnb0zLVS37meI64T9Ilz7ghc+R4ybGICVQ26NEKBmbhiUSQenZoVQn/ijaad8AfI+oXPg25JOAXJYzkXTI+xVgi/lq1Gbh+Z4Y/LWIIwLSX8HxMSMUPSFKIHQPNQO18fdERBLdXoCAhF8p4doZowH+EdGTqeDOmrAJI5Y0VTWkL8tMBtqm+gmWYEKSRyBObKKeyBpSaRxR7ASqlR7ASpp3x0H88R+uISHeWXoqcADNnyWAUxHIBSkpQh7pREyCcffAiJwG6rPLzoS1vI0OQjtUFdIPqTKDMAt4rDVNYghDFZS7Gt2kiCK2YMOnVyT1zlMdNF2xPJxBnBKsKm/UmfqlFGEtfErA4HMXXg7t5FzP2pbFJO0vgbnDh7GQN6g18gs5sc7JDmGa9WIREgYCe6QQUhYLB9QGy7txKol9N9NAzDCA8gSdSB0S8+BnWYp0bSbvAEo6F07ig+wVblPxTyDfTkh22azSOAK+gb9MbBgsD1ZCbsxYcxc3o56Zc1bnVgHrZmajEhCUE7lY931gY22n0yDnOpQbwIramSJXO7Vi5NfpN8mCxA630PVUPjcEGbVrdYHD3n4jkYLS1G+7CYM3UWT1uuaaaeR1mwNQY8AgefoQegkon88zTYDjbZZkz8Aeocdpb6ACZosnw0ZBYZ2Uqt/HJuhf7ew+nMigD91jRaeNA/mYiZqGQd3o2jePZWIamu7LcAU4MeMrwNrz1ood5qwefGYMSRUyZ1U1NDCKh9FmV4I3FOp7ROHQIsAilG7Bbh5OuzppPlEcU56U61c5DcOGMN0v6+P5X5G7q0NSfvL6rjB/V2gIQo8hToyQWh0OP1G6O5xKEJeNwH7L3cf69+5ifvchQ4d0D+6Y9/VseGyF20FUT2tBJublETFXVdL1CM4DebiXjy9BDMmN8XM5PSxCHLmT49lFO1sZBo0N7xv734lSck16DkT/4vYnRYJhONEztQaCSGCAuBS5x0BUegtr6sdZ2ICVGBt5IPGLCth1mPHTs3gcHW48jBRNLQIsEw2wRngkst3hz074UN4gtsVPYTNodsaoAjkEMQphQJ5OgQOQ4vQuXpO6KQfEbEsGguBwFJpFURfVgkHFA7ZgBhgGXxbkXAmYqBp98Cb9gr2QBxAIzKey9D1Ap4NpQ2B2eAuxeB18kgiIHVaFFH90BVBdR3+NQaK0j+xMkkWEhGiQFzMKzKBsk9rCGTEgfaagKbWWzmU0LlSN+sSNHNvAwarchAO/CcvSfDffeAJYkbkpETOwJOGO2Ej1HU6wAFuBu6CPaC16pCkdT/6HW+rB6zF36WLsQI5Ceh9u52XnEBfuWiruIJdhnvGwZPh6OUx3OpNNoOI4n+rzEjF10VKFszYIwXQvyL60Nm3I1rAbsbjRhVyFIaMWxvU4ahRsqG9nUZFrQUKYrDhZEN6oI659ezI2+G5/tBohJFix2JuXESG19VQ3Rxvo9mxHhnPvEtPMlrUUH2u22cYkLICnDGnu4OtQili9ow9A3ygZFQXa6tHWc5kYtPTCPpMP2xltfACRV1vCvQpc9u50N6gM94aqR0SBUOlWgMhY8KyYgrwC7yFBKuyEx3OxoaQEfpsSIIiUtrnwIYSYeVtqVzJHSthKNdG0uRpPAkjihpwXkP+gkUO4I1i1yDwGHehkFisfESJluilATIB3QkritqM7UAo2PQDwyb8zgiB/kSb7McqtF+8TymCpn7ax/MDpEeboREz0497BxA43whjIY88ShG903ZqMHDMOnQgcSWDDHzeMmFG59JgJFNylimvCAU4OQ2QMku+a6NlXCjG9vN8Hi13556UiVKyJpbrbcGPrnECSMQScRjoSxxkI0cdFtHFqAnDEVOpcWo/DiJpVCEUggINCFwPkWK476L7hicj6h9wojhMHbZniO0AborWB3RdwnpWQJ1yt1yDEKvIOJVOryyMBdIC5Za1YYGEwrcgaxZshR3eRB0cndrkwSBoe0qEmeyzq+TpgLszVQxY3rdFhIQD6Rfh50KYFt9W6MYWMKq1BiGwIcn7yBJ3VD0NRaldgffL3GOU8ri/MnF5pHOddm8bH8xgrnio47YOXUpOcGloZ8iQejHqY9P1uOVF6LThysyhHwGQzdPJmraW48eVgb3rB8AtBL3HEK89ji1CjG6iSNBKhdtpcyvsM0FwJTl9uH/J5YgYtT+GGkccYg2A2o14Myo766lJDcBj9FYn5sCSPF/fYCUl6N7WKkjfpUXxH3gnkIZ8B7+1KYmOTU9LxkDuc/EqQfwcshFCoVkIb0EoOjQvlhjwwjB+dnpo7V9pMuD56sDH7L6n8AA32xEtJWuj/uSj87XNbQ0RwxNbg6rv2DDKBLrGnj+9liHPZNItPhlBXGKKHCXyFzMmDSmC2Z7K70HHCySBZe5UYPsXu5isfiTeLg+955GwsfCprG3a4AFRYA13x9sM1L4FZY/speCpVMoWlJbRidlYQ98dMeLHEfuiXuGsDmpVLU/tmbUzKqkBzXio70pA14YJyxHKEd1TuRViPG/jKD4Iwe6rSl6ZMumZ1LQomvlzovfHRD/zzNqZ+qzus7a3stcteD6ADZve80Yzz+lawTtBUhEEbfjTQ0tgFcunFBY+GAWKYQAzFkSIB8EW+ARywx27VhUCbFIcjYMihv7SJ6jwNCYRNgH9MTG1Hmozg3dGLoDSRouYtsuiCPUO5uRpePAIbEDfwpakuSRUOy4OdcFMsCkQScMgIbqP0RQ0Vg74iYPgwH6tg/yQRESrOBin6GASzsZoATYHnT4DEcelINUgcTntlKiyPiEoHJj0w7kb3ETawjOEbjqDvjDZYz71DSx6OHmhrDflnwPthEcz6oPGBrrFKJO7BHh5Uu/UaaDIRZA0ZD8HzAxR1jgIlzOWiU4BfDeNQzp1kJ1EJZ/dLhRpB1lR+IzXM1dBRxSbF2fxwaKtc5amnYqpTTSaN2+jEugcFYGtV9MXirUj0SFflUn0vE47ApJBkIodhCWulp6Z8gooy8ByEbSXoBE9PlEj1HLQDitIicjGtHils+/hySiIrtddyAedVZRndCKw2/v39IE2ZXwqEawYED1FVlHWO2Fce2muy/HCBjngdsX7gOHAeAI3Q2XQFmnR9iP0pnMsyDZ/EtwfNNoRj8G0dBeyZ2JyITNaTBs9h9LVWWPQgQb9ery2xHz+WHmTF2Qd07Y3JCSqzJt3ZMtMm5f4bLQnBdat65Ap4f0BydzFi7y0nhUsOuZAe8L7cp5PJSHfgJEtFAjr1P5azbRdUMW3oqMJM6I8lzalNvIvaoOX6YNJOnWTI9POMlE6MC12WHiL4cnrczb499YEvVfmWyB50FFE81pmjnQ/bAPJGuJjdpVvK5QxRcnE/dJ+k/K8dAgUtCUlsntQopPNJjQlkQhChxxIQ+p6Nv4Eh+nohF4gEipbkOxoRxRKaJs3wU86TSYaMUdlRuL99rCcqmHiMUAAEoCSa+XqeJAMo+iRCblqY58xtFclaRNKk3yT5KMnLsxyektOQCh5tdAjOhqEZ0i19lt0ysBkU5fYQ9agw7ZOmUBESz/NtP+aaSc3XQGhoJ5HAUfBHrKvQn7X67QZ96SNXf4C/HgfzC6RazqAJFxCJFBVbqBOPBJCRJ4q/I6nwgIRF3Am7oyfQeiSo8wwTJJywS8aeIjiiGujYWnEcxwWUEdkGNwFn5q6dQrUThGr6txWCg99Uz8HZ/T9wIUORXyQuxq0ZZcCWUv2qQtkAbmbEpRx4sAOzCzvCswOvGskpFBs6ELpBIcFqg/4QucmkuGQBDicjTuI/uc2CwDqtTcLTaPr5NUkAXteI2BLUwTWUZ0Y50R0C0tAH5FYooq+Jy5vZtopXvJEoLnc78U6iihQUyAhIV87DJ0GqakkVIqOgzozojXlh+CWk+Ks+23SnNPlrbLHbOkbVygLfTWiyrronJXi8ZiGpi0teiGbQ/kk0tbGSDrXfX7jLMiReo2kTDuYRQdGsCg+WycHVE/TZiiclqkmUBXx5sjgVQnCT50Ig1iGQKDCrbbhMW110H+UHiW4KxNJgDP5zjRjvF32AAMzoCPCTpfB+xhyij1INC44XrteNjp6Oi5mNlvXhm7GHlH8GOWzWeHQcXyna4pjrVv72BY3DYi4RCtb7Wh5yppgw0ceFxhTC6rHAydFbHyS6co6VBH6rRicqu/Ay7Q7MRDC4tWljLWH3WT9qdNGEgFIUExnzhX3QK1SPjqmIfvAaydGGEfqGK9sb1Od7lVtUHGE5qiOcO9nbewlWkVApNN/S0pb0rYHsgdb5LQRgVmGmYSsmGT81wmYT31jRCcsJCvrv/yODvHOXXsE22vLODJ5bJT0Pd1vUpSxas+CWtjA1/s6gzaGyF4GlE/THgFinE7FoYF9EwRfOs6j1bAy3NrdJ4nxIDn8s+PR72PNas6ePwzStefMC47ym1+Vqvd45+2E6j3eqFh1k61N47udAuIhm58C0A7LO/REXwMjGC6IoUNA87zz4lb3JmR9b+CcD1Wkpr5EMWGCIFVpOuf/gAqy+krMZgoSLq1XCoq6et/coJuAFEY64PVAJBYdJ9CAnzX9nPl3Td8FuY50fes5n8v+sB4yNhVa8cLO74QKbTQPihSXto92k7UhaSu5g7wS0X3s3NI3Y3ROpu/7xPNKfV5Z4fPwjXxP7ZSiKMH4ovtiBMrUN1noBRaOTNQW6viGb2oD6BMV2XmEF5f0H5cQaC6hZCC8Y/q6lXZraJ0pV9JiCAO90DBHOlyhugAsqWAixLhjLQgRuaOT26j+3kVHpkcQGqLLYAKRBt9wsPF9qWhJkiEsCfNAvBmQgifhGh0N4Fo64hmlgkmnW+EeKKkkl2A6WoxaKAEvoLYDI8lUj/tZG7RZuFjkksVo9LL2rSD9SNWfi8J96Y6wCMg2ZQ4wWqCOfCSSAX0f6V97igxbSG11/CNlAp1gGhezqBKAFx7MdKhbnprj3oh/knC3jiSPtVlqmJlaIzwx39YvWL/BddyxpARvFbnl1iEDfT/FIdbljBC1akjQCJFV7DR95RNhNt/5OZyms+AY9T3hG/WiADyiolGxGdpOTmeuiLdM8HVwx5KIedPJEvCdvHY5gZyso3+dWwF2IAGxuOTa66yUQFoKDFRUspmSKIHiWimAhywv6qs46MaA/pAjua/uAW9gBSA7HbdHWjbNnIKOoIclWuJqPzmx6g0jHG3VtjLioMSDvhXFeAOJuHWIQyryRyGe3x95uv/M96H+NJB2vJPOJqWplAIaBLWug0590SQOHXR6rCeZOzEgajalCmhUB0mRLxKK3upNXYgxbFFOFBxKcC56GYoCdekljB19MHQUIR/w/GpdOY3ktvoa6wGeSWkKgAkFwWw3y+5PeZOJpoVRKMOEn+n63s9scwSRiL5YSMU6eFFnl/paaiOz1Je2jbp28Cf5HMAno+EJYGd94wGdAfOi8C/wXZJODpg+LNK1ndYv4UD4dIxSYAIReKrpo9L1NS3/t0cLPx/dP7vgX3okEq5LzS9oUcc4U1/VQqIyR21pfPaWmr7Z/CEffBnz34RX30ZBkIHuOvPsa7jP9I+O9/9VYvsT0XGn7v4X7eHhT8eiBc0AAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQfjBhoONSR4TIq6AAAPsUlEQVR42u2ae3SV1ZnGf8/+zknCJRAQvFXFW62ibfECaKEKSayg0o63ekGtV7D11mkdrWvNLJ1LV2tXx+rUthZ1ZlpBW/FWb6ASENESoiJeQBQVFAsSA4QEkpOc8+1n/jgHiLp0uVrF0MmzVtbK2d/Z39n73e9+3+d99hbdHIefMSVY3AGMBvZcMG1Sge0Qme4/xGgILwApYLZTdHtDt6xa7MpdD7pOiJjvjPSgBz3oBlB3GcivH6pk0OCMHOGV59p87SUdAPzxqX4oE3aC0Av7rW8fsX67jNOhuwxk0KBMgvmVxD8NPaz3FgcI2WQwaDF4MeLYnmT46eyuGuBlFf8veq7dC1RZGuuAHkP/zSTOhSANA2K+4C3sQtI7mDMMA4Pj/T3Rvgc9+H/JOk6ZnOHkc/rQsaKVs0/fWn/84al+hEwiDKs7O335UZsAOOTs2zKZNP2KIGuzpHn1K619W+vpX1UlDDgagiy8obmZqqqq0sQK2IkIMiBFbCGM1zY10W/HKrLOCOx33n6DZcuWfey4x1aPA0fmzHmMw0aMom9lpWQR09Rz5z72sX3H1Izf9ob+Q8OAXiFyCqahubVy6aRvvA3A9PkD9i9pGhsc/K1TRzS3jP761eT32Ot84KYSQ5pq+8I+jX8y+GRMLXYd0pHAO8DPEMdjKgOeFdGViDeBgYp62PL5gqWQ/DYq3VUwSWaF5FuiZQzI2C76oIrrGBSQqAG/Fu2VSAH0L4IITAeW2lBceYHYo6I8805HrlB6rSZu82QYzN7AbYjrKytbf7SZXVgMA74KRKXsDiz2HvsB+T2BilL3fQjSnFmPxOMnnHjvprb2vURYZHwQqLw4V/cV7G4YBV6L1Qc0x9Jq5OcwjVa6P5GEoDWSGyI6EjgU6XHwsVJ4FvMloEzidcQBhndBIyUW23pI8iZgvtFhggkS7wC9gJ2AfrnOwuMSXwQVZPXe5jza9quCbzhwQy4XtxYfVj0wE7ibmCwHWLO2FeAFYD2wEWgIUbG2tpa2tvavC9eDOg1vgTuD095FFUo5E3YAbRAUgFcEnUV/JatIAdGIuTeaf8DUGFKIQ0BrgcOKPuFbDXtjbgE6EPeAdkEINBjoI1gO/Bl7R6AvcKPgzUzI1Bs2AV8Gwjb36FMPby4Acz7YvrFRK/rt5AlW9H0txerv9ZmXMeD0KfeG4Lringwb6+9IXV2dDYaRktZbMQr2QrQUUtpDQpPwMmCooQnoL/nEGFko2A9YhfxgcNjXihMEKyw1yd4NaAb2AVpBq7HaJK/BnAksA9qAd0u78DnErECsjA7ngVZZ7gBy2G/k08Lhgt0R72Gv1/aYwaurq4HyxA4BCimEYCt94olHPGbMMaGiIjifR2kaCEHYTgrBaSYS2trWFxYsmM/ommPJQjYT8oV8oYCS8gwmX/T4JJUC+XwhVvRKVUhD4pS0UMhSVtapGIlSNtiOnWV5ymOScUFpKE/VmaaxTyarjs6QJIqAHWPc9gXLIcf/lEzf/tgFFj34Y/Jt77LPAeIn11dIVRV9ooiFmLRN/FoTo468msJuQ3ZA4aBSMlwR8fLZ0yZz5NHfCIrunWa14amZM7bQl969k5grUBGDcwlpWSajQmeBfgmsnTOn+L3Ro08IWXKDHbNNs5941GmaUl19XACYM/vh/AejHcXw0/UzFBPhZhQ+8OyDfT471jHirFt6yR4ivLx+6uQOgFGjriIdss9+FucCzwtPr5822XfXD1TElwFXA6nxOatzzY/fccuURPAAML401tUSQ7NrpjdnYvY88MIk+NWYZvJ5pUqIGVk5pEnAbcDQYjylxim/zCQxqagoy21qz58hJCOnSXpXNpK1k2FAg1BFdGcbSaYcW6nVnkh9+lRk2traOsqkJNrBUflszLe3P/nkk59vCS77JMyvIzobuB+gc6+9FcyFwBVAo9F9QD6VJXNsKWMjVDu4fPDjJc3joC4OMdhRuyYOG7D3RbyeRi6DdFPG6oeCJK7zVpayh8wAQ1TC5NTq37opXqcQWoV3g/hYkuqCiHqBW4SCFb8KmdWKjDVan0F1tvff1J5/C4VhJrYTYiqHbMj0/iXFk5/PUb2zFgJ3B/PS5qbKjQUDrwOrgRc3b7+SFZu6bLumfEfF5gebukoiiPYnZj1q0FtAJ4QGApZoM77e0V0nXo5ptL0Y8yVbGzJJDAE3Iu4HTgRlEH8Eyo0Ps2kBcobXsFYZviKYBmEtMNhoA9YLwG5Bn9x+n5lHL5h24RLgvK5ts++/mBFn3nyrrOmgnNyRAnS2FWJZRfZyxL3gDmD2OWNWMvL0QkEh8y+GH5XG+kCIfrtEyJeWZbIvdubzk7BeNjFKdNiy0CrgSvAiS+9gp8Bsiao0dYFAp9D35HC3FYfIOg60NMVvBPxtxDNYOYIjaCXme4L7Da8LN4KqgBV6f5zePoT/j8Kwc24gm1YmwVGio1A/9VK2xzPabWrokMBZl2XZbc8MuZz5/Q053lsNSuoYcdryCogDAachND037eL88DNuBDRUUaOMM1JYHEL69PzbL0qHn/tbhU4NtsjIbMyHtGXh7d+lrALO+0E5O+yU0Loh5dafd9DWst0bWgw/7bYkhLQstdqfvfMCAI44fQoxsCOwM3h1Nhfee+qeC5neMECOTAROA1Yq+spTvtbcOnzilGyAh4BDSy++E/tyArtjLQKqSu2dwJmFpDA9k2bOAn5eCinro0L1M1MvePvep/uFQsj8ADEW4iIrufbUkWvzACMnTuldKkg2orh8wdSLAKitvY72QVVl+WwgR0vni7f/8NN3sr+l82ETb1ZIClcgLwqZuOOWjBUIwK3F8lm/KlQ4ADgqAX4KHAdMIqi2xFB2AaqBHUp/JzkoOGp/oF+XnywDDs4UkgBMAHYEBgJ7BcevA+RDGIj8H+BjQVcG4gFd+p8HLAKeiy7bzExo3WlAUkh0j6L/0DutTD6T3fy3hYK4mSVYqbtyu/eR94+JqP7Ut6E/0U+461ukLQVI+llF/7/J0A2//65B/wkcHAONW2aROgIXFNU4XUwuFrOzYiq4CngY+E0h41kADloNzAbWAk2Y6SESEUuBDV1+shO8sBDykWIhswZYB7yB9CRAiF4ncTXoEcRPsF/Zyjj938Aw4DApn9vc3qbWVDGcLOuMHZrzaTeM0X9FMrw0y257ZWhvM/9zU471fykmw+GnvVmBPBCIQWpaMO2SwvAzbgSHA4RH2WQlvRTl+c9MnZyO/ub1yldWDracUWSjcUvDHZOLyfCHZeywY4aWDSm3/X0kw08H48aNI0kS5XI519XVve/Z6BOup1DRL0kJ2vDusMJrcw7dPund2JrxvwCumFM3I92qjo3b1wr/CGQM82SvQDrbcPMTdY8s3HpEM26oCJdbnirzNdAXS49eFn7J6FTk6+fMmrF0y7trxx9s6yLwLZnAc4Wo4eArgD2BJYIfp47L5s5+lOFnTyFEnYJ9FZAADzuk1zTc/t307ob+SYwaJYVdI15o67XTDl8HwMgzp2Qx/UzY1Lwh5l57aNLnbugAnPnBWB3FastLwDtJnkvgPfA5wqccefQxAhhTXYvQCeBzVYytCfhE8ARwEqEZfC7mhHHjJhSNXHOcbE4BfwfTWLAmgOuAQ4snGHzLMDeQDCnGWzLY/1aifcOAKxXDEIDocDholu07ZWYFnClROGGuKS52vLmqSlvmNvw7U8pHTpxy08iJU0444uwp29zQH8ITdTM3AW8B64L1TpDeAF4Bjs44FPuEbELxwsurRL8m+BmwEngDfIOjXgRWALUd+TQBSBKFUp9FCryH/RNgVQgMz0jfBE4FdkSeNGbseJUiRO/3hTqHihKVqSp5OcCgrWHQoijw7wwcKMett55SVQETgaMLadTnbugPIsY0BR4BDsJ8oehtoT8wAphhwocydRJUKB1NHe7oKoA0TXcGvoqYYbNvySD3OvW6WbMeAccngPeA0SEQVDT1S12038bgdFXR4nEhsBRoBO7boqJJkeKi/yv4amvrZZzO9kGNoOGIf+7M5NztDD3/6bkGzwASi+qxY4/DxKNKBcSMhvqnPzToRc8/49LilClw1IgRtRjGAsFmRqlCk2DlvHl1XYn5GmBXg3ZoIRWcTbFfNTDKgWaA7IZNq4k62MR9STjrvt9tjAALpk6is631WafptTHfNqth6kVbxvb8PSd4wbQLX18wddK6F373/W0aOj6RepfL5bBokGnE1ErxdosaYF1UqG9r2/ChPuvWNWKYJ2gBju5TWfYncA2wylGLQvDeLvKHtnw+v7mMcKnM7gfm4QcmUeLJH1LXTzimAKzrLH3/fXj+vh/S3fCJZVLF0IH8GHBMxBVC1UBdiO74aO7oNtBsoNZyhUw1MDMJ5I02lmhav6FDh7JkyRJKV1z6A+tFMMBdfx4QCPQver82rl20rvOiokTBtHn9lS1X2G9wPh2216ZuTe8+cWW4ZPELNswABkk6ubT1Z+Q7Wz9Sk23f1DuWwscQmZOAXYCZy99cZuRXS7F3/5133kMAIcR+wG7AYtv+r5lA4ETgTeAt4x8PGDZQAHctGBiy2XANUTOXrSk79v55W3PmH+sH97qrfsDxd80fOGC7M/SaNX9BMdaVtuo1QGqHx596ah5jasZh0avEAhJQrzFjjqG+/l4kPVZKVNcC7ZbnLF++jIrysjeBZ4FTI+GQsTXj+0eHq4CsxF19+2RiVZ9dKJbxVFG8MzFcKgpU2AE4F6g1fDOXregiXuTHAvcRfFp3Ch1lwC/G1ozf7JkLbT0l+3xgn4hPr64Zd+fsuplGYT0wF6gF5klpU3G1lNjcWio6DNykEM4Dos27wALgCGCmIi0AbRub05D0uhjpfslzS0dWfYGbCvnsjAdnPcDkyaJtUNVyIFdyijcUVbo3rYj8YClJznEp1JQYUb3lCwPM6j6GlsZ/QNdqCrjR0s9KOtyatCTHZZJCTJ29xLAL+N1UMS0x1wj6d+DGkrc1ly6wIZxaOh/YCbOqs0MRYO7cuYwaNfb5bHnFIRIHA73AK8rKsouXLV8WAY4/3txRz+8y4nlMmUJc/HJDIQK89ExHPGh42SWSZePTj2jaMoVHp7esqzmxz/+2rGynBz3owd+tevdxGHnGbwPiJNCAIN8xf+rkjVuenTklgykP0W3z75zcrSW90N0NbXkP0DTgNxF9a3P7IRfcDHApsDIGDuzu8+j2hiZk2ihWlznM2i3V6vq+YF4F6rq29xj6r0Qk32jFA8FDkR/dUkDdcybGjwDfJt+2uicL9KAHPayjC4aOuZTKXb8cwBQ62+Nzd39/uzR0t4/Rfb9woBBXIk3NlPdOtleP7vaGloJKwtJR28MO/Cj8Hzw1YqhGar+sAAAAAElFTkSuQmCC"><br>' );
                                $templateBody->appendToVar( 'sidebar', $__tmpl->parse() );
                        }
        
                        if($journal->getControlElement('search_pubmed_enabled')) {
                                //Pubmed Search
                                $renderer2 = \Drupal::service( 'renderer' );
                                $form = \Drupal::formBuilder()->getForm( '\\Drupal\\publisso_gold\\Form\\PubmedSearch', [ 'bk_id' => $journal->getElement( 'id' ), 'term' => '' ] );
                                $__tmpl->clear();
                                $__tmpl->setVar( 'sbb_content', $renderer1->render( $form ) . '' );
                                $__tmpl->setVar( 'sbb_title', (string)$this->t( 'Search' ) . ( '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAAbCAYAAACKlipAAAADlElEQVRogeWZMW6zQBCFfZEUKXyN3MRXyAnc+hiOI1G7p0C+ABIN5RZuoKJCQrTzVw89P+8CxjHwJyOt5GC8zM6382bYbGxlVjft0i4sapulHTAzu5ZVB4I//0VbHEjdtLZ9f7Pt+5vtDmfLXfGnoawKyPb9zZLU2bWs7FpWS7u2iK0WSO6KP5klqwMSxZkXSN20lqTOojizKM46cL8N2qqBQLZyV9jH57d37A7n7je/wVYLBLv/WlZBGDr2x8t/D+alQMbISR+Qa1nZ/njpMmEsmCjOXiZlr5bIzf54sd3hfDf2x4slqZs8MeaN4qz3vj4guStuQGA+QPJJF3/3jP8+S1LXxedVYDa6YN2J++Nl0sQIzKNATl+nIBAE2Cdj+K5u2pvrPylhSeq6eV8OZHc4d7qtgZiyqFcAyV1xN78GqG7a4G+etdmBhB7OO9P3fqBdkdktEG5Zk9TdtbNjgKhMcJaoNDEs3UzXsuqkDbIcCu61rCxJXTd43kWBIHOGAsBzDBVj3rkfn9+DQHzSiWdocNh3fk4UZ8HWmcHVTeutU7yeWYBgl/OCEIhngPC8DAiLGgPEV6BzV3ivc/Yg0AwDWaFdHPxR3/XeWYBoUUfXwto8BYjWEN/uZSC4rkB4B/tqmkqp/g5/qz9cc4aUYEwNQQ1Gt8jxg1zyyTY3I1Gc3WZI3bTd8D3oJ4BwADAPA8HIXXEDRIOtht2sz9cgo6XnweviUwEFPwQEPptZd8SDwON67opOdVCX2OdgDVGbGwikCwGEQY44+L5rCB6AcC1iSeSXUOzuUIc2BIR3P87aEAvOCi4DiA8an7sMGQNEH+CDOkay4PDp69QNPfnVOXBNd5beB0gqWb51siooPL6X65AvVnw/B1r94szhz3XTjgfCi+LU1w5Eg4RAoe3laz5DQJAl2gHxvOoDPx/zsFSwrypZnPXsJ56h3WJIsrR2cNAxN75DU4L6YmaPAeFswG+4P1cgIWhjjjS4yUDgojjzniZooPkIRaUt1M6qPGmLjHkVCJ80AwgbWmgE3ecPx33TV8j7wGjfrnPo50f/CwjJAQDtotChhAIcKszqz5BM+9bFf/OLJdp6PXWGrz6pVP8WP37vM0jWmMwNgZnrOJ7hojuccmyzaiBoQ8eaSupcQLROhK6NsVUDeVRKYaz1cwBBnYCsQoqm+L5qIM8YsuUnT3v7DO8R/P4xxX4tELPpGbak/QPYnkh3mayzNQAAAABJRU5ErkJggg=="><br>' )
                                );
                                $templateBody->appendToVar( 'sidebar', $__tmpl->parse() );
                        }
                        
                        //Citation Note
                        $__tmpl->clear();
                        $__tmpl->setVar( 'sbb_title', (string)$this->t('Citation note'));
                        $__tmpl->setVar( 'sbb_content', $article->getCitationNote($this->lang) );
                        $templateBody->appendToVar( 'sidebar', $__tmpl->parse() );
                        
                        //license
                        $article_authors = json_decode( $article->getElement( 'authors', $this->lang, false ) ) ?? [];
                        $corporations_for_cn = [];
                        $authors_for_cn = [];
                        $authors = [];
                        foreach ( $article_authors as $_ ) $authors[ $_->weight ] = $_;
                        ksort( $authors );
        
                        foreach ( $authors as $_ ) {
                                
                                $initials       = '';
                                $firstnames     = preg_split( '/([\\s-]+)/', $_->firstname, -1 );
                                $i              = 0;
                                
                                foreach ( $firstnames as $name ) {
                                        if ( !empty( $name ) ) $initials .= strtoupper( substr( $name, 0, 1 ) );
                                        $i++;
                                        if ( $i == 2 ) break;
                                }
                
                                $authors_for_cn[] = $_->lastname . ' ' . $initials;
                        }
        
                        //fourth corporations
                        if ( !empty( $article->getElement( 'corporation', $this->lang, false ) ) ) {
                                foreach ( json_decode( $article->getElement( 'corporation', $this->lang ), true ) as $_ ) {
                                        $corporations_for_cn[] = $_;
                                }
                        }
        
                        $authors_for_cn = array_filter( $authors_for_cn );
                        $corporations_for_cn = array_filter( $corporations_for_cn );
                        $__tmpl->clear();
                        $__tmpl->setVar( 'sbb_title', (string)t( 'License' ) );
                        $license = new \Drupal\publisso_gold\Controller\License( $article->getElement( 'license', $this->lang ) );
                        $__tmpl->setVar( 'sbb_content', $license->getImageLink( [ 'height' => 40 ], '_blank' ) );
                        $__tmpl->appendToVar( 'sbb_content', '<br>&copy;&nbsp;' );
                        //den ersten Autor zerlegen
                        $licAuthor = explode( ' ', $authors_for_cn[ 0 ] ?? '' );
                        //das letzte Element entfernen (Anfangsbuchstabe vom Vornamen)
                        array_pop( $licAuthor );
                        $__tmpl->appendToVar( 'sbb_content', implode( ' ', array_filter( $licAuthor ) ) );
                        count( $article_authors ) > 1 ? $__tmpl->appendToVar( 'sbb_content', ' et al.' ) : '';
                        $__tmpl->appendToVar( 'sbb_content', '<br>' );
                        $__tmpl->appendToVar( 'sbb_content', (string)t( 'This article is distributed under the terms of the license <a href="' . $license->getElement( 'url' ) . '" target="_blank">@license</a>.', [ '@license' => $license->getElement( 'description' ) ] ) );
                        $templateBody->appendToVar('sidebar', $__tmpl->parse());
                        
                        //Publication Data
                        $__tmpl->clear();
                        if ( $journal->getControlElement( 'show_submission_date' ) == 1 && $article->getDate( 'received', $this->lang ) ) {
                                if ( $__tmpl->getVar( 'sbb_content' ) ) $__tmpl->appendToVar( 'sbb_content', '<br>' );
                                $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'Received: @date', [ '@date' => date( 'Y-m-d', strtotime( $article->getDate( 'received', $this->lang ) ) ) ] ) );
                        }
        
                        if ( $journal->getControlElement( 'show_revision_date' ) == 1 && $article->getDate( 'revised', $this->lang ) ) {
                                if ( $__tmpl->getVar( 'sbb_content' ) ) $__tmpl->appendToVar( 'sbb_content', '<br>' );
                                $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'Revised: @date', [ '@date' => date( 'Y-m-d', strtotime( $article->getDate( 'revised', $this->lang ) ) ) ] ) );
                        }
        
                        if ( $journal->getControlElement( 'show_acceptance_date' ) == 1 && $article->getDate( 'accepted', $this->lang ) ) {
                                if ( $__tmpl->getVar( 'sbb_content' ) ) $__tmpl->appendToVar( 'sbb_content', '<br>' );
                                $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'Accepted: @date', [ '@date' => date( 'Y-m-d', strtotime( $article->getDate( 'accepted', $this->lang ) ) ) ] ) );
                        }
        
                        if ( $journal->getControlElement( 'show_publication_date' ) == 1 && $article->getDate( 'published', $this->lang ) ) {
                                if ( $__tmpl->getVar( 'sbb_content' ) ) $__tmpl->appendToVar( 'sbb_content', '<br>' );
                                $__tmpl->appendToVar( 'sbb_content', (string)$this->t( 'Published: @date', [ '@date' => date( 'Y-m-d', strtotime( $article->getDate( 'published', $this->lang ) ) ) ] ) );
                        }
        
                        if ( $__tmpl->getVar( 'sbb_content' ) ) {
                                $__tmpl->appendToVar( 'dates', $__tmpl->parse() );
                        }
                        $templateBody->appendToVar('sidebar', $__tmpl->parse());
        
                        //translations
                        $__tmpl->clear();
                        $translations = $article->getTranslations();
        
                        if ( !in_array( $article->getOrigLanguage(), $translations ) ) {
                                array_unshift( $translations, $article->getOrigLanguage() );
                        }
        
                        if ( count( $translations ) ) {
        
                                $__tmpl->setVar( 'sbb_title', (string)$this->t( 'Other languages' ) );
                                $__tmpl->setVar( 'sbb_content', (string)$this->t( 'This article is also available in:' ) . '<br>' );
                
                                foreach ( $translations as $langCode ) {
                        
                                        if ( $langCode != $this->lang ) {
                                
                                                $ary = [];
                                
                                                if ( $langCode != $this->lang ) $ary[ 'lang' ] = $langCode;
                                
                                                if(null !== ($langManager = \Drupal::service('language_manager')->getLanguage($langCode ?? 'en'))) {
                                        
                                                        $link = Url::fromRoute( \Drupal::routeMatch()->getRouteName(), array_merge( \Drupal::routeMatch()->getParameters()->all(), $ary ) );
                                                        $link = Link::fromTextAndUrl( (string)$this->t($langManager->getName() ?? 'en'), $link);
                                                        $__tmpl->appendToVar( 'sbb_content', $link->toString() );
                                                }
                                        }
                                }
                
                                $templateBody->appendToVar( 'sidebar', $__tmpl->parse() );
                        }

                        //Downloads
                        $urlExportPDF = Url::fromRoute( 'publisso_gold.export.pdf.article', [ 'jrna_id' => $this->article->getElement( 'id' ) ] );
                        $urlRecreatePDF = Url::fromRoute( 'publisso_gold.export.pdf.article.force_new', [ 'jrna_id' => $this->article->getElement( 'id' ) ] );
                        $urlPreviewPDF = Url::fromRoute( 'publisso_gold.export.pdf.article.volatile', [ 'jrna_id' => $this->article->getElement( 'id' ) ] );
                        $urlXML = Url::fromRoute('publisso_gold.journal.article.xml', ['jrn_id' => $journal->getElement('id'), 'jrna_id' => $article->getElement('id')]);

                        if ( $this->article->getElement( 'pdf_blob_id' ) ) $__tmpl->appendToVar( 'sbb_content', '<div id="lnkChapterPDFDownload" ><a href="' . $urlExportPDF->toString() . '" class="download-pdf" id="lnkChapterPDFDownload" rel="nofollow" target="_self"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;Download PDF</a></div>' );

                        if ( \Drupal::service( 'session' )->get( 'user' )[ 'weight' ] >= 70 ) {
                                $__tmpl->appendToVar( 'sbb_content', '<a href="' . $urlRecreatePDF->toString() . '" class="download-pdf" rel="nofollow" target="_self"><span class="glyphicon glyphicon-repeat"></span>&nbsp;&nbsp;(Re)Create PDF</a><br>' );
                                $__tmpl->appendToVar( 'sbb_content', '<a href="' . $urlPreviewPDF->toString() . '" class="download-pdf" rel="nofollow" target="_self"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;Preview PDF</a><br>' );
                        }

                        $__tmpl->appendToVar( 'sbb_content', '<div id="lnkChapterXMLDownload" ><a href="' . ( $urlXML->toString() ) . '" class="download-pdf" id="lnkChapterXMLDownload" rel="nofollow" target="_blank"><span class="glyphicon glyphicon-console"></span>&nbsp;&nbsp;Download XML</a></div>' );

                        if ( !empty( $__tmpl->getVar( 'sbb_content' ) ) ) $templateBody->appendToVar( 'sidebar', $__tmpl->parse('::', 0) );
                        $__tmpl->clear();

                }
        }
