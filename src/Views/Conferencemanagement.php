<?php
        
        
        namespace Drupal\publisso_gold\Views;
        
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Url;
        use Drupal\file\Entity\File;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class Conferencemanagement
         * @package Drupal\publisso_gold\Views
         */
        class Conferencemanagement extends ControllerBase {
        
                /**
                 * @return array
                 */
                public function main () {
        
                        $filter = null;
                        if(\Drupal::requestStack()->getCurrentRequest()->query->has('filter')){
                                $filter = \Drupal::requestStack()->getCurrentRequest()->query->get('filter');
                        }
                        
                        $user = Publisso::currentUser();
                        
                        if($user->getWeight() >= 70){ //alle Bücher für Account >= PUBLISSO
                                $qry = \Drupal::database()->select( 'rwPubgoldConferences', 'c' )->fields( 'c', [ 'cf_id' ] );
                                $pager = $qry->extend( '\Drupal\Core\Database\Query\PagerSelectExtender' )->limit( 10 );
                                $pager->orderBy( 'cf_title', 'ASC' );
                        }
                        else { //Account-bezogene Bücher
                                $qry = \Drupal::database()->select( 'rwPubgoldConferences', 'c' )->fields( 'c', [ 'cf_id' ] );
                                $qry->leftJoin( 'rwPubgoldConferencesEditors', 'ce', 'c.cf_id = ce.ce_conferenceid AND ce.ce_uid = :uid', [ ':uid' => $user->getId() ] );
                                $qry->leftJoin( 'rwPubgoldConferencesEditorsInChief', 'ceic', 'c.cf_id=ceic.ceic_cfid AND ceic.ceic_uid = :uid', [ ':uid' => $user->getId() ] );
                                $qry->leftJoin( 'rwPubgoldConferencesEditorialOffice', 'ceo', 'c.cf_id=ceo.ceo_cfid AND ceo.ceo_uid = :uid', [ ':uid' => $user->getId() ] );
                                $qry->leftJoin( 'rwPubgoldConferencesConferenceManager', 'cm', 'c.cf_id = cm.cm_cfid AND cm.cm_uid = :uid', [ ':uid' => $user->getId() ] );
                                $cond = $qry->orConditionGroup();
                                $cond->condition( 'ce.ce_uid', $user->getId(), '=' );
                                $cond->condition( 'ceic.ceic_uid', $user->getId(), '=' );
                                $cond->condition( 'ceo.ceo_uid', $user->getId(), '=' );
                                $cond->condition( 'cm.cm_uid', $user->getId(), '=' );
                                $qry->condition( $cond );
                                $pager = $qry->extend( '\Drupal\Core\Database\Query\PagerSelectExtender' )->limit( 10 );
                                $pager->orderBy( 'cf_title', 'ASC' );
                        }
        
                        if($filter) $pager->condition('c.cf_title', "%$filter%", 'LIKE');
                        
                        $urlAddConference = Url::fromRoute( 'publisso_gold.conferencemanagement', [ 'action' => 'add' ], ['attributes' => ['class' => ['btn', 'btn-success']]] );
        
                        $list = [
                                '#type' => 'table',
                                '#header' => [
                                        [],
                                        ['data' => (string)$this->t('Title')],
                                        ['data' => (string)$this->t('Creator')],
                                        ['data' => (string)$this->t('Edited')],
                                        ['data' => (string)$this->t('Public')],
                                        []
                                ],
                                '#rows' => [],
                                '#empty' => (string)$this->t('No conferences found...')
                        ];
        
                        foreach($pager->execute()->fetchCol() as $id){
                                
                                $conference = ConferenceManager::getConference($id);
                                $user = Publisso::User($conference->getElement('creator_uid'));
                                
                                $urlEditConference      = Url::fromRoute('publisso_gold.conferencemanagement', ['action' => 'edit|'.$id]);
                                $urlCompleteConference  = Url::fromRoute('publisso_gold.conferencemanagement', ['action' => 'complete|'.$id]);
                                $urlEditSessions        = Url::fromRoute('publisso_gold.conferencemanagement', ['action' => 'sessions|'.$id]);
                                $urlAbstracts           = Url::fromRoute('publisso_gold.conferencemanagement.abstracts.overview', ['cf_id' => $id]);
                                
                                $links = [];
                                
                                if($urlEditConference->access()){
                                        $links[] = [
                                                'title' => (string)$this->t('Edit'),
                                                'url' => $urlEditConference
                                        ];
                                }
        
                                if($urlCompleteConference->access()){
                                        $links[] = [
                                                'title' => (string)$this->t('Complete'),
                                                'url' => $urlCompleteConference
                                        ];
                                }
        
                                if($urlEditSessions->access()){
                                        $links[] = [
                                                'title' => (string)$this->t('Edit sessions'),
                                                'url' => $urlEditSessions
                                        ];
                                }
        
                                if($urlAbstracts->access()){
                                        $links[] = [
                                                'title' => (string)$this->t('Abstracts'),
                                                'url' => $urlAbstracts
                                        ];
                                }
                                
                                $list['#rows'][] = [
                                        ['data' => [
                                                '#markup' => '<img src="'.Url::fromRoute('publisso_gold.getPicture', ['type' => 'ul', 'id' => $conference->getElement('logo') ?? 0])->toString().'" width="30">'
                                        ]],
                                        ['data' => [
                                                '#markup' => $conference->getElement('title')
                                        ]],
                                        ['data' => [
                                                '#markup' => $user->profile()->getReadableName()
                                        ]],
                                        ['data' => [
                                                '#markup' => $conference->getElement('edited') == -1 ? (string)$this->t( 'Never' ) : ( $conference->getElement('edited') == 0 ? (string)$this->t( 'By manager' ) : (string)$this->t( 'By PUBLISSO' ) )
                                        ]],
                                        ['data' => [
                                                '#markup' => (string)$this->t($conference->getElement('published') ? 'Yes' : 'No')
                                        ]],
                                        ['data' => [
                                                '#type' => 'dropbutton',
                                                '#links' => $links
                                        ], 'width' => '150']
                                ];
                        }
                        
                        return [
                                'filter' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Title contains:'),
                                        '#attributes' => [
                                                'style' => [
                                                        'width: 200px;'
                                                ],
                                                'onkeyup' => [
                                                        'filterManagementMediumList(event, this)'
                                                ]
                                        ],
                                        '#value' => $filter ?? '',
                                        '#description' => (string)$this->t('Hit "Enter" to accept filter')
                                ],
                                'content' => $list,
                                'pager' => [
                                        '#type' => 'pager'
                                ],
                                'separator1' => [
                                        '#markup' => '<hr>'
                                ],
                                'add' => $urlAddConference->access() ? [
                                        '#type' => 'link',
                                        '#title' => (string)$this->t('Add Conference'),
                                        '#url' => $urlAddConference
                                ] : [],
                                'separator2' => [
                                        '#markup' => '<hr>'
                                ],
                                '#attached' => [ 'library' => [ 'publisso_gold/default' ] ], '#cache' => [ 'max-age' => 0 ],
                        ];
                }
        }
