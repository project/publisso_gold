<?php
        
        
        namespace Drupal\publisso_gold\Views;
        
        
        use Drupal\Core\Link;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\JournalManager;
        use Drupal\publisso_gold\Controller\Manager\TemplateManager;
        use Drupal\publisso_gold\Controller\Template;

        /**
         * Class Volume
         * @package Drupal\publisso_gold\Views
         */
        class Volume extends Journal {
                
                protected $volume;
        
                /**
                 * @param $jrn_id
                 * @param $vol_id
                 * @return string
                 */
                public function getVolumeTitle($jrn_id, $vol_id){
        
                        if(preg_match('/^journal\|(.+)$/', $jrn_id, $matches)){
                                $jrn_id = $matches[1];
                        }
        
                        if(preg_match('/^volume\|(.+)$/', $vol_id, $matches)){
                                $vol_id = $matches[1];
                        }
                        
                        $volume = new \Drupal\publisso_gold\Controller\Volume($vol_id);
                        $this->volume = $volume;
                        $title = implode('-', [$volume->getElement('year'), $volume->getElement('number')]);
                        
                        if(!empty($volume->getElement('title'))){
                                $title = implode(' ', [$title, '"'.$volume->getElement('title').'"']);
                        }
                        
                        return $title;
                }
        
                /**
                 * @param $jrn_id
                 * @param $vol_id
                 * @param null $tab
                 * @return array
                 */
                public function getVolumeHTML($jrn_id, $vol_id, $tab = null){
        
                        if(preg_match('/^journal\|(.+)$/', $jrn_id, $matches)){
                                $jrn_id = $matches[1];
                        }
        
                        if(preg_match('/^volume\|(.+)$/', $vol_id, $matches)){
                                $vol_id = $matches[1];
                        }
                        
                        $journal = JournalManager::getJournal($jrn_id);
                        $volume = new \Drupal\publisso_gold\Controller\Volume($vol_id);
                        $this->volume = $volume;
                        $header = $this->getJournalHead($journal);
                        $body = $this->getJournalBody($journal, $tab);
        
                        return [
                                'header' => $header,
                                'body' => $body,
                                '#cache' => ['max-age' => 0]
                        ];
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param Template $templateBody
                 * @param null $tab
                 * @return bool
                 */
                protected function setJournalContent_volume(\Drupal\publisso_gold\Controller\Medium\Journal $journal, Template &$templateBody, $tab = null){
                        
                        $volume = $this->volume;
                        $articles = [];
                        if ( !$volume->hasIssues() ) {
                                foreach ( array_reverse( $volume->getArticles() ) as $jrna_id ) {
                        
                                        $article = $volume->getArticle( $jrna_id );
                                        $section = (string)t( 'Volume @year - @number', [ '@number' => $volume->getElement( 'number' ), '@year' => $volume->getElement( 'year' ) ] );
                                        $articles[ $section ][] = [ 'title' => $article->getElement( 'title' ), 'jrna_id' => $article->getElement( 'id' ), 'volume' => $volume->getElement( 'id' ), 'docno' => $article->getElement( 'docno' ) ];
                                }
                        }
                        else {
                                foreach ( array_reverse( $volume->getIssues() ) as $iss_id ) {
                        
                                        $issue = $volume->getIssue( $iss_id );
                        
                                        foreach ( array_reverse( $issue->getArticles() ) as $jrna_id ) {
                                
                                                $article = $volume->getArticle( $jrna_id );
                                                $section = (string)t( 'Volume @year - @vol_number, Issue @iss_number@iss_title', [ '@year' => $volume->getElement( 'year' ), '@vol_number' => $volume->getElement( 'number' ), '@iss_number' => $issue->getElement( 'number' ), '@iss_title' => $issue->getElement('title') ? ': '.html_entity_decode($issue->getElement('title')) : '' ] );
                                                $articles[ $section ][] = [ 'title' => $article->getElement( 'title' ), 'jrna_id' => $article->getElement( 'id' ), 'volume' => $volume->getElement( 'id' ), 'issue' => $issue->getElement( 'id' ), 'docno' => $article->getElement( 'docno' ) ];
                                        }
                                }
                        }
        
                        $c = 0;
                        foreach ( $articles as $_section => $_articles ) {
                
                                $tmpl_section = new \Drupal\publisso_gold\Controller\Template();
                                $tmpl_section->get( 'volume_content_current_volume_section' );
                                $tmpl_section->setVar( 'section', $_section );
                                $tmpl_section->setVar( 'articles', '' );
                
                                $cc = 1;
                                foreach ( $_articles as $_article ) {
                        
                                        $url = Url::fromRoute( 'publisso_gold.journals_journal.volume.article', [ 'jrn_id' => $journal->getElement( 'id' ), 'vol_id' => $_article[ 'volume' ], 'jrna_id' => $_article[ 'jrna_id' ] ] );
                        
                                        $link = Link::fromTextAndUrl( (string)t( 'Article @cc', [ '@cc' => (int)$_article[ 'docno' ] ] ), $url );
                                        $tmpl_section->appendToVar( 'articles', '<li>' . $link->toString() . ': ' . $_article[ 'title' ] . '</li>' );
                                        $cc++;
                                }
                
                                $templateBody->appendToVar( 'content', $tmpl_section->parse() );
                
                                if ( $c ) $templateBody->appendToVar( 'content', '<hr>' );
                
                                $c++;
                        }
        
                        return true;
                }
        
                /**
                 * @param \Drupal\publisso_gold\Controller\Medium\Journal $journal
                 * @param $tab
                 * @param array $addTabs
                 * @return array
                 */
                protected function getJournalBody(\Drupal\publisso_gold\Controller\Medium\Journal $journal, $tab, $addTabs = []) :array{
                
                        $templateBody = TemplateManager::load('publisso_journal_body');
                        if(!$templateBody->isLoaded()) return [];
                        
                        if($this->volume->getElement('id') != $journal->getCurrentVolume()->getElement('id')) {
                                $addTabs = [
                                        'volume' => [
                                                'title'            => implode( '-', [
                                                        $this->volume->getElement( 'year' ),
                                                        $this->volume->getElement
                                                        ( 'number'
                                                        )
                                                ]
                                                ),
                                                'weight'           => 1.5,
                                                'show_on_empty'    => 0,
                                                'route_prefix'     => 'publisso_gold.journals_journal.volume',
                                                'route_parameters' => [
                                                        'vol_id' => $this->volume->getElement( 'id' )
                                                ]
                                        ]
                                ];
                        }
                        else{
                                if($tab == 'volume') $tab = 'current_volume';
                        }
                        
                        $this->setJournalTabs($journal, $templateBody, $tab, $addTabs, ['vol_id' => $this->volume->getElement('id')], 'publisso_gold.journals_journal.volume');
                        $this->setJournalContent($journal, $templateBody, $tab);
                
                        return [
                                '#type' => 'inline_template',
                                '#template' => $templateBody->parse()
                        ];
                }
        }
