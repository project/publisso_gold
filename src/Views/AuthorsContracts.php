<?php
        
        
        namespace Drupal\publisso_gold\Views;
        
        
        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Classes\AuthorsContract;

        /**
         * Class AuthorsContracts
         * @package Drupal\publisso_gold\Views
         */
        class AuthorsContracts extends ControllerBase {
        
                /**
                 * @return array
                 */
                public function main(){
                        
                        $qry = \Drupal::database()->select('rwPubgoldAuthorsContracts', 't');
                        $qry->addField('t', 'name');
                        $pager = $qry->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit(20);
                        
                        $rows = [];
                        
                        foreach($pager->execute()->fetchAll() as $_){
                                
                                $ac = AuthorsContract::load($_->name);
        
                                $delete = [];
                                $urlDelete = Url::fromRoute('publisso_gold.authorscontracts.delete', ['name' => $_->name]);
        
                                if($urlDelete->access()){
                                        $delete = [
                                                '#type' => 'link',
                                                '#title' => (string)$this->t('Delete authors contract'),
                                                '#url' => $urlDelete,
                                                '#prefix' => '<hr>',
                                                '#attributes' => [
                                                        'class' => 'btn btn-warning use-ajax',
                                                        'data-dialog-type' => 'modal',
                                                        'data-dialog-options' => '{"width":800, "title":"'.((string)$this->t('Delete authors contract')).'"}'
                                                ]
                                        ];
                                }
                                
                                $rows[] = [
                                        ['data' => ['#markup' => $ac->getName()]],
                                        ['data' => ['#markup' => $ac->getUploadDate()]],
                                        ['data' => ['#markup' => $ac->getDeacription()]],
                                        ['data' => [
                                                'view' => [
                                                        '#type' => 'link', '#url' => $ac->getBlob()->getUrl(), '#title' => '', '#attributes' => ['target' => '_blank', 'class' => 'icon-search', 'title' => (string)$this->t('View')]
                                                ],
                                                'delete' => $urlDelete->access() ? [
                                                        '#type' => 'link', '#url' => $urlDelete, '#title' => '', '#attributes' => ['class' => 'icon-cancel', 'title' => (string)$this->t('Delete')], '#prefix' => '&nbsp;&nbsp;&nbsp;'
                                                ] : [],
                                        ]],
                                ];
                        }
                        
                        $add = [];
                        $urlAdd = Url::fromRoute('publisso_gold.authorscontracts.add');
                        
                        if($urlAdd->access()){
                                $add = [
                                        '#type' => 'link',
                                        '#title' => (string)$this->t('Add authors contract'),
                                        '#url' => $urlAdd,
                                        '#prefix' => '<hr>',
                                        '#attributes' => [
                                                'class' => 'btn btn-warning use-ajax',
                                                'data-dialog-type' => 'modal',
                                                'data-dialog-options' => '{"width":800, "title":"'.((string)$this->t('Add authors contract')).'"}'
                                        ]
                                ];
                        }
                        
                        return [
                                'table' => [
                                        '#type' => 'table',
                                        '#header' => [
                                                ['data' => (string)$this->t('Name')],
                                                ['data' => (string)$this->t('Upload-date')],
                                                ['data' => (string)$this->t('Description')],
                                                []
                                        ],
                                        '#rows' => $rows,
                                        '#empty' => (string)$this->t('There are no authors contracts available')
                                ],
                                'pager' => [
                                        '#type' => 'pager'
                                ],
                                $add,
                                [
                                        '#markup' => '<hr>'
                                ]
                        ];
                }
        }
