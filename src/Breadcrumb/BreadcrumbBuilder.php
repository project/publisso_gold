<?php
        
        namespace Drupal\publisso_gold\Breadcrumb;
        
        use Drupal\Core\Routing\RouteMatchInterface;
        use Drupal\system\PathBasedBreadcrumbBuilder;
        
        /**
         * Adds the current page title to the breadcrumb.
         *
         * Extend PathBased Breadcrumbs to include the current page title as an unlinked
         * crumb. The module uses the path if the title is unavailable and it excludes
         * all admin paths.
         *
         * {@inheritdoc}
         */
        class BreadcrumbBuilder extends PathBasedBreadcrumbBuilder {
                
                /**
                 * {@inheritdoc}
                 */
                public function build(RouteMatchInterface $route_match) {
                        $breadcrumbs = parent::build($route_match);
                        return $breadcrumbs;
                }
                
        }
