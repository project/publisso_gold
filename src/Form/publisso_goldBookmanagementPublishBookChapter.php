<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldBookmanagementPublishBookChapter.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Drupal\publisso_gold\Controller\Workflow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;

/**
 * Provides a simple example form.
 */
class publisso_goldBookmanagementPublishBookChapter extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $num_corporations;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldBookmanagementPublishBookChapter|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldbookmanagementpublishbookchapter';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $texts = \Drupal::service('publisso_gold.texts');
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        $args = $form_state->getBuildInfo();
        
        if(count($args)){
            $wf_id = $args['args'][0]['wf_id'];
            $bk_id = $args['args'][0]['bk_id'];
        }
        
        if(!$wf_id)
            $wf_id = $form_state->get('wf_id');
        
        if(!$bk_id)
            $bk_id = $form_state->get('bk_id');
        
        if($wf_id && $bk_id){
            
            $form_state->set('wf_id', $wf_id);
            $form_state->set('bk_id', $bk_id);
            
            $workflow = getDashboard(\Drupal::database(), $wf_id);
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            
            $delta = ['workflow', 'workflow_data'];
            foreach($delta as $_) $form_state->set($_, $$_);
            
            $objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
            
            $form['pdf_container'] = [
                    '#type' => 'container',
                    '#attributes' => [
                            'class' => [
                                    'div-float-left'
                            ]
                    ],
                    '#suffix' => '<br clear="all">'
            ];
            
            if ($objWorkflow->getDataElement ( 'cp_id' )) {
                    
                    $url = Url::fromRoute ( 'publisso_gold.export.chapter', [
                            'cp_id' => $objWorkflow->getDataElement ( 'cp_id' )
                    ] );
                    
                    $form['pdf_container']['pdf'] = [
                            '#type' => 'link',
                            '#title' => ( string ) $this->t ( 'Download as PDF' ),
                            '#url' => $url,
                            '#prefix' => '<span class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;',
                            '#suffix' => '&nbsp;&nbsp;&nbsp;',
                            '#attributes' => [
                                    'class' => [
                                            'download-pdf'
                                    ]
                            ]
                    ];
            }
            
            $form['pdf_container']['create_pdf'] = [
                    
                    '#type' => 'button',
                    '#value' => (string)$this->t('Create final PDF'),
                    '#suffix' => '&nbsp;&nbsp;&nbsp;',
                    '#ajax' => [
                            'callback' => '::createFinalPDF',
                    ]
            ];
            
            
            $form['save_pdf'] = [
                    '#type' => 'checkbox',
                    '#default_value' => true,
                    '#title' => (string)t('Publish final created PDF or create one if not already done.'),
                    '#suffix' => '<hr>'
            ];
            
            $form['published'] = [
                    '#type'   => 'date',
                    '#title'  => (string)t($texts->get('workflow.publish_chapter.publish_date', 'fc')),
                    '#suffix' => '<hr>',
                    '#default_value' => $objWorkflow->getDataElement('published') ?? ''
            ];
            
            $form['send_mail'] = [
                '#type' => 'checkbox',
                '#default_value' => true,
                '#title' => (string)t('Send infomail on publish')
            ];
            
            $form['revert'] = [
                    '#type'         => 'submit',
                    '#value'        => (string)t('Revert to accepted'),
                    '#button_type'  => 'info',
                    '#submit'       => ['::revertToAccepted'],
                    '#suffix'       => '&nbsp;&nbsp;&nbsp;'
            ];
            
            $form['submit'] = [
                    '#type' => 'submit',
                    '#value' => (string)t('Publish Chapter'),
					'#button_type' => 'success',
					'#suffix' => '<br><br>'
            ];
        }
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $delta = ['workflow', 'workflow_data', 'wf_id', 'bk_id'];
        foreach($delta as $_) $$_ = $form_state->get($_);
        
		$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
		$objWorkflow->historize();
		$objWorkflow->setDataElement('published', $form_state->getValue('published'));
		$cp_id = $objWorkflow->getElement('type') == 'bcs' ? $objWorkflow->publish($form_state->getValue('send_mail') == 1) : $objWorkflow->publishErratum($form_state->getValue('send_mail') == 1);
		
		//Lösche alle PDF, die während des Workflows erstellt wurden
		if($objWorkflow->getDataElement('cp_id')){
		        $chapter = new \Drupal\publisso_gold\Controller\Bookchapter($objWorkflow->getDataElement('cp_id'));
		        
		        if($chapter->getElement('pdf_blob_id')){
		                $blob = new \Drupal\publisso_gold\Controller\Blob($chapter->getElement('pdf_blob_id'));
		                $blob->delete($blob->getId());
		                $chapter->setElement('pdf_blob_id', null);
		        }
		}
		
		//erstelle neues PDF oder speicher ein vorher erstelltes, wenn gewünscht
		if($form_state->getValue('save_pdf')){
		        
		        $tempstore = \Drupal::service('user.private_tempstore')->get('pg_pdf_bin');
		        
		        if($tempstore->get('name') && $tempstore->get('type') && $tempstore->get('data')){
		                
		                $chapter = new \Drupal\publisso_gold\Controller\Bookchapter($cp_id);
		                $blob = new \Drupal\publisso_gold\Controller\Blob();
		                $blob->create($tempstore->get('data'), $tempstore->get('type'), serialize(['name' => $tempstore->get('name')]));
		                $chapter->setElement('pdf_blob_id', $blob->getId());
		                $tempstore->delete('name');
		                $tempstore->delete('type');
		                $tempstore->delete('data');
		        }
		        else{
		                $url = \Drupal\Core\Url::fromRoute('publisso_gold.export.chapter', ['cp_id' => $cp_id, 'type' => 'pdf']);
		                $url->setAbsolute();
		                \Drupal::httpClient()->request('GET', $url->toString(), ['headers' => ['Content-Type' => 'application/text'], 'http_errors' => false]);
		        }
		}
		
		$objWorkflow->unlock();
        $form_state->setRedirect('publisso_gold.dashboard');
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function revertToAccepted(array &$form, FormStateInterface $form_state){
        
        $workflow = new Workflow($form_state->get('wf_id'));
        $workflow->setDataElement('state', 'accepted');
        $workflow->setElement('state', 'accepted');
        $form_state->setRedirect('publisso_gold.workflow_item', ['wf_id' => $workflow->getElement('id')]);
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return AjaxResponse
         */
        public function createFinalPDF(array &$form, FormStateInterface $form_state){
            
            $workflow = new Workflow($form_state->get('wf_id'));
            
            if(!$workflow->getDataElement('cp_id')){
                    $workflow->publish(false, true);
            }
            
            $chapter = new \Drupal\publisso_gold\Controller\Bookchapter($workflow->getDataElement('cp_id'));
            
            if(!empty($workflow->getDataElement('references'))){
                    
                    $chapter->setElement('chapter_text', base64_encode(\Drupal::service('publisso_gold.tools')->mapReferences(
                                    base64_decode($workflow->getDataElement('chapter_text')), $workflow->getDataElement('references'))));
            }
            
            $url = \Drupal\Core\Url::fromRoute('publisso_gold.export.chapter.volatile', ['cp_id' => $chapter->getElement('id')]);
            $url->setAbsolute();
            $response = new AjaxResponse();
            $command = new InvokeCommand(
                            'html',
                            'trigger',
                            [
                                    'openPDF',
                                    [
                                            'url' => $url->toString()
                                    ]
                            ]
                            );
            $response->addCommand($command);
            return $response;
    }
        
        /**
         * @param null $tmpl
         * @param null $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl = NULL, $vars = NULL){
        
        if($tmpl == NULL || $vars == NULL){
            //set Site-Vars
            $this->tmpl = str_replace(array_keys($this->tmpl_vars), array_values($this->tmpl_vars), $this->tmpl);
            
            //remove unused vars
            $this->tmpl = preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl);
        }
        else{
            //set Site-Vars
            $tmpl = str_replace(array_keys($vars), array_values($vars), $tmpl);
            
            //remove unused vars
            return preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $tmpl);
        }
    }
}
