<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldLogin.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Controller\ControllerBase;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Drupal\Core\Database\Connection;
        use Drupal\Core\Url;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldLogin extends FormBase {
                
                private $user    = null;
                private $database;
                private $modname = 'publisso_gold';
                private $modpath;
                
                public function __construct ( Connection $database ) {
                        $this->database = $database;
                        
                        if ( isset( $_SESSION[ 'user' ] ) )
                                $this->user = $this->database->select( 'rwpgvwUserProfiles', 'u' )->fields( 'u', [] )->condition( 'id', $_SESSION[ 'user' ][ 'id' ], '=' )->execute()->fetchAssoc();
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return publisso_goldLogin|static
                 */
                public static function create (ContainerInterface $container ) {
                        return new static( $container->get( 'database' ) );
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'publisso_goldlogin';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state ) {
                        
                        if ( !array_key_exists( 'setup', $_SESSION ) ) $_SESSION[ 'setup' ] = new \Drupal\publisso_gold\Controller\Setup();
                        $session = \Drupal::service( 'session' );
                        
                        $this->modpath = drupal_get_path( 'module', $this->modname );
                        require_once( $this->modpath . '/inc/publisso_gold.lib.inc.php' );
                        
                        //echo '<pre>'.print_r($_SESSION, 1).'</pre>';
                        
                        if ( ( $session->has( 'logged_in' ) && $session->get( 'logged_in' ) === true ) ) {
                                
                                $form[ 'login_info' ] = [
                                        '#type'   => 'markup',
                                        '#markup' => t( '<div class="rwLoginInfo">Logged in as @user</div>', [ '@user' => $this->user[ 'user' ] ] ),
                                        '#prefix' => '<div style="float: left;">',
                                ];
                                
                                $form[ 'submit' ] = [
                                        '#type'   => 'submit',
                                        '#value'  => t( 'Logout' ),
                                        '#suffix' => '</div><br clear="all">',
                                        '#prefix' => '<br>',
                                ];
                                
                                $form[ 'space' ] = [
                                        '#type'   => 'markup',
                                        '#markup' => '<br><br><br><br><br><br><br><br>',
                                ];
                        }
                        else {
                                
                                $form[ 'user' ] = [
                                        '#type'       => 'textfield',
                                        '#title'      => t( 'Username' ),
                                        '#required'   => true,
                                        '#attributes' => [
                                                'style' => 'display: block; width: 80%;',
                                        ],
                                ];
                                
                                $form[ 'password' ] = [
                                        '#type'       => 'password',
                                        '#title'      => t( 'Password' ),
                                        '#required'   => true,
                                        '#attributes' => [
                                                'style' => 'display: block; width: 80%;',
                                        ],
                                ];
                                
                                $form[ 'submit' ] = [
                                        '#type'   => 'submit',
                                        '#value'  => t( 'Login' ),
                                        '#suffix' => '<br><br>',
                                ];
                                
                                $form[ 'link' ] = [
                                        '#type'   => 'link',
                                        '#title'  => (string)t( 'Create account' ),
                                        '#url'    => Url::fromRoute( 'publisso_gold.register' ),
                                        '#prefix' => (string)t( 'You don\'t have an account? Please register here' ) . ': ',
                                ];
                                
                                $form[ 'link1' ] = [
                                        '#type'   => 'link',
                                        '#title'  => (string)t( 'Reset password' ),
                                        '#url'    => Url::fromRoute( 'publisso_gold.password.reset' ),
                                        '#prefix' => '&nbsp;&nbsp;&nbsp;&nbsp;' . (string)t( 'If you have forgotten your password, you can reset it here' ) . ': ',
                                ];
                                
                                $form[ 'space' ] = [
                                        '#type'   => 'markup',
                                        '#markup' => '<br><br><br><br><br><br><br><br>',
                                ];
                        }
                        
                        $form[ '#cache' ] = [
                                'max-age' => 0,
                        ];
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $session = \Drupal::service( 'session' );
                        
                        $lp = \Drupal::service( 'publisso_gold.login_provider' );
                        
                        if ( !( $session->has( 'logged_in' ) && $session->get( 'logged_in' ) === true ) ) {
                                
                                if ( !$lp->login( $form_state->getValue( 'user' ), $form_state->getValue( 'password' ) ) ) {
                                        $form_state->setErrorByName( '', (string)$this->t( 'Login failed with "'.$lp->getLastMessage().'"' ) );
                                }
                        }
                        else {
                                $lp->logout();
                        }
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $session = \Drupal::service( 'session' );
        
                        \Drupal::cache('menu')->invalidateAll(); // for clearing the menu cache
                        \Drupal::service('plugin.manager.menu.link')->rebuild(); // rebuild the menu
                        
                        
                        if ( !( $session->has( 'logged_in' ) && $session->get( 'logged_in' ) === true ) ) {
                                $form_state->setRedirect( 'publisso_gold.login' );
                        }
                        else {
                                $form_state->setRedirect( 'publisso_gold.dashboard' );
                        }
                        
                        return $form;
                }
        }






