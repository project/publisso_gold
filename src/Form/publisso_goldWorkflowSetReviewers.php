<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowSetReviewers.
 */
namespace Drupal\publisso_gold\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldWorkflowSetReviewers extends FormBase {
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function getFormId() {
		return 'publisso_goldworkflowsetreviewers';
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {
	        
	        $this->database = \Drupal::database();
	        $this->modname = 'publisso_gold';
		$this->modpath = drupal_get_path ( 'module', $this->modname );
		
		if ($this->modpath && ! $form_state->get ( 'modpath' )) {
			$form_state->set ( 'modpath', $this->modpath );
		}
		
		if (! $this->modpath && $form_state->get ( 'modpath' )) {
			$this->modpath = $form_state->get ( 'modpath' );
		}
		
		$args = $form_state->getBuildInfo ();
		
		if (count ( $args ))
			$wf_id = $args ['args'] [0] ['wf_id'];
		
		if (! $wf_id)
			$wf_id = $form_state->get ( 'wf_id' );
		
		if ($wf_id) {
			
			$form_state->set ( 'wf_id', $wf_id );
			
			// get the workflow-item
			$workflow = getDashboard ( \Drupal::Database (), $wf_id );
			$workflow = $workflow [0];
			$workflow_data = json_decode ( base64_decode ( $workflow->wf_data ) );
			$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
			
			// get predefined editors
			$editors = array ();
			
			switch ($workflow_data->type) {
				
				case 'bookchapter' :
					$medium = new \Drupal\publisso_gold\Controller\Book ( $objWorkflow->getDataElement ( 'bk_id' ) );
					$reviewers = getBookReviewers ( \Drupal::database (), $workflow_data->bk_id );
					
					foreach ( $reviewers as $index => $uid ) {
						
						$user = getAllUserData ( \Drupal::database (), $uid ['up_uid'] );
						$reviewers [$uid ['up_uid']] = (array_key_exists ( 'profile_graduation', $user ) ? $user ['profile_graduation'] . ' ' : '') . $user ['profile_lastname'] . ', ' . $user ['profile_firstname'] . ' ' . (array_key_exists ( 'profile_graduation_suffix', $user ) ? $user ['profile_graduation_suffix'] . ' ' : '');
						unset ( $reviewers [$index] );
					}
					break;
				
				case 'journalarticle' :
					$medium = new \Drupal\publisso_gold\Controller\Journal ( $objWorkflow->getDataElement ( 'jrn_id' ) );
					break;
				
				case 'conferencepaper' :
					$medium = new \Drupal\publisso_gold\Controller\Conference ( $objWorkflow->getDataElement ( 'cf_id' ) );
					break;
			}
			
			if (! count ( $reviewers ))
				$reviewers = getUsersByRole ( \Drupal::database (), [ 
					2,
					3,
					4,
					5,
					6,
					7 
				] );
			
			$form = [ 
				
				'reviewer-list' => [ 
					'#type' => 'fieldset',
					'#title' => t ( 'set reviewer(s)' ),
					'#prefix' => '<br>',
					'content' => [ 
						
						'reviewers_list' => [ 
							'#title' => t ( 'Available users for reviewer' ),
							'#type' => 'select',
							'#multiple' => true,
							'#options' => $reviewers,
							'#required' => true,
							'#default_value' => explode ( ',', $workflow->wf_assigned_to_reviewer ),
							'#size' => 20 
						],
						
						'invite_help' => [ 
							'#type' => 'markup',
							'#markup' => ( string ) t ( 'If you would like to assign someone not listed, choose “invite”. Please note, that you have to assign the reviewer after the invitation has been accepted. When the chapter is in review you can assign, invite or unassign reviewers via "Submissions where I am editor" in the dashboard.' ),
							'#prefix' => '<div>',
							'#suffix' => '</div>' 
						],
						
						'invite' => [ 
							'#type' => 'link',
							'#title' => t ( 'Invite user' ),
							'#url' => Url::fromRoute ( 'publisso_gold.workflow.invite_user', [ 
								'wf_id' => $form_state->get ( 'wf_id' ) 
							] ),
							'#suffix' => '<br><br>' 
						],
						
						'submit' => [ 
							'#type' => 'submit',
							'#value' => t ( 'Set reviewer(s) & close' ),
							'#submit' => [ 
								'::setReviewers' 
							],
							'#limit_validation_errors' => [ 
								[ 
									'reviewers_list' 
								] 
							],
							'#button_type' => 'success' 
						] 
					] 
				] 
			];
			
			$form ['action-list'] = [ 
				'#weight' => 100,
				'#type' => 'fieldset',
				'#collapsible' => true,
				'#collapsed' => true,
				'#title' => t ( 'Recommendation without review' ),
				'#prefix' => '<br>',
				'#suffix' => '<br>',
				'#tree' => false,
				'content' => [ 
					
					'comment' => [ 
						'#title' => t ( 'Comment' ),
						'#type' => 'textarea',
						'#suffix' => '<br>' 
					],
					
					'comment_for_author' => [ 
						'#title' => t ( 'Comment for author' ),
						'#type' => 'textarea',
						'#suffix' => '<br>' 
					],
					
					'recommendation' => [ 
						'#title' => t ( 'Recommendation' ),
						'#type' => 'select',
						'#multiple' => false,
						'#options' => [ 
							'accept' => t ( 'Accept Submission' ),
							'reject' => t ( 'Reject Submission' ),
							'revision' => t ( 'To author for revision' ) 
						],
						'#required' => true 
					],
					
					'submit' => [ 
						'#type' => 'submit',
						'#value' => t ( 'Set recommendation & close' ),
						'#submit' => [ 
							'::setDirectAction' 
						],
						'#limit_validation_errors' => [ 
							[ 
								'recommendation' 
							] 
						],
						'#button_type' => 'success' 
					] 
				] 
			];
			
			if ($objWorkflow->getDataElement ( 'state' ) != 'assigned to editor') {
				unset ( $form ['action-list'] );
			}
		}
		
		return $form;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {
		return $form;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {
		return $form;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function setReviewers(array &$form, FormStateInterface $form_state) {
		$session = \Drupal::service ( 'session' );
		$reviewers = $form_state->getValue ( 'reviewers_list' );
		$wf_id = $form_state->get ( 'wf_id' );
		$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
		$workflow = getDashboard ( $this->database, $wf_id );
		
		if (count ( $workflow )) {
			
			// inform users about registration if not done
			$assigned_users = [ ];
			
			foreach ( $reviewers as $uid ) {
				
				$_user = new \Drupal\publisso_gold\Controller\User ( $uid );
				
				if ($_user->getElement ( 'reg_notification_sent' ) == 0) {
					$assigned_users [] = $_user;
				}
			}
			
			foreach ( $assigned_users as $_ ) {
				
				// send Reg-Mail to user if not yet be done
				if ($_->getElement ( 'reg_notification_sent' ) == 0) {
					
					$port = \Drupal::request ()->getPort ();
					$host = \Drupal::request ()->getHttpHost ();
					$host = "http" . ($port == 443 ? 's' : '') . "://$host";
					$link = $host;
					$password ['clear'] = genTempPassword ();
					$password ['encrypted'] = '*' . strtoupper ( sha1 ( sha1 ( $user ['password_clear'], true ) ) );
					
					$_->setElement ( 'password', $password ['encrypted'] );
					
					foreach ( [ 
						'registration_password',
						'registration_info' 
					] as $name ) {
						
						$vars = [ 
							'::firstname::' => $_->profile->getElement ( 'firstname' ),
							'::lastname::' => $_->profile->getElement ( 'lastname' ),
							'::link::' => $link,
							'::email::' => $_->profile->getElement ( 'email' ),
							'::user::' => $_->getElement ( 'user' ),
							'::login_link::' => $link . '/publisso_gold/login',
							'::password::' => $password ['clear'] 
						];
						
						sendRegistrationInfoMail ( $name, $vars );
					}
					
					$_->setElement ( 'reg_notification_sent', 1 );
				}
				// - Reg-Mail --
				
				$objWorkflow->newUserReview($_);
			}
			// -- inform users --
			
			
			$objWorkflow->setState ( 'in review', $reviewers );
			$objWorkflow->setElement ( 'editor_last_edited', $session->get ( 'user' ) ['id'] );
			$objWorkflow->unlock ();
			$form_state->setRedirect ( 'publisso_gold.dashboard' );
			return $form;
		}
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function setDirectAction(array &$form, FormStateInterface $form_state) {
		$session = \Drupal::service ( 'session' );
		$workflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
		$recommendation = $form_state->getValue ( 'recommendation' );
		
		\Drupal::database ()->insert ( 'rwPubgoldWorkflowRecommendations' )->fields ( [ 
			'wf_id' => $workflow->getElement ( 'id' ),
			'created_by_uid' => $session->get ( 'user' ) ['id'],
			'recommendation' => $recommendation 
		] )->execute ();
		
		$workflow->setDataElement ( 'state', 'decision eic' );
		$workflow->setElement ( 'assigned_to', 'u:' . implode ( 'u:', explode ( ',', $workflow->getElement ( 'assigned_to_eic' ) ) ) );
		
		// comments
		$comment = $_REQUEST ['comment'];
		$comment_for_author = $_REQUEST ['comment_for_author'];
		
		$db_comment = $db_comment_for_author = false;
		
		if (! empty ( $comment )) {
			
			$comment = base64_encode ( $comment );
			$db_comment = \Drupal::database ()->insert ( 'rwPubgoldWorkflowComments' )->fields ( [ 
				'wfc_created_by_uid' => $session->get ( 'user' ) ['id'],
				'wfc_comment' => $comment,
				'wfc_wfid' => $workflow->getElement ( 'id' ) 
			] )->execute ();
		}
		
		if (! empty ( $comment_for_author )) {
			
			$comment_for_author = base64_encode ( $comment_for_author );
			$db_comment_for_author = \Drupal::database ()->insert ( 'rwPubgoldWorkflowCommentsForAuthors' )->fields ( [ 
				'cfa_created_by_uid' => $session->get ( 'user' ) ['id'],
				'cfa_comment' => $comment_for_author,
				'cfa_wfid' => $workflow->getElement ( 'id' ),
				'cfa_for_uid' => $workflow->getElement ( 'created_by_uid' ) 
			] )->execute ();
		}
		// -- comments --
		
		/**
		 * ****************************************
		 * unlock workflow *
		 * ****************************************
		 */
		$workflow->setElement ( 'locked', null );
		$workflow->setElement ( 'locked_by_uid', null );
		/**
		 * ***************************************
		 */
		/**
		 * ***************************************
		 */
		
		$form_state->setRedirect ( 'publisso_gold.dashboard' );
		return $form;
	}
        
        /**
         * @param null $tmpl
         * @param null $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl = NULL, $vars = NULL) {
		if ($tmpl == NULL || $vars == NULL) {
			// set Site-Vars
			$this->tmpl = str_replace ( array_keys ( $this->tmpl_vars ), array_values ( $this->tmpl_vars ), $this->tmpl );
			
			// remove unused vars
			$this->tmpl = preg_replace ( '(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl );
		} else {
			// set Site-Vars
			$tmpl = str_replace ( array_keys ( $vars ), array_values ( $vars ), $tmpl );
			
			// remove unused vars
			return preg_replace ( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
		}
	}
}
