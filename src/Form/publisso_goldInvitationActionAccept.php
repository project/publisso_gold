<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldInvitationActionAccept.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use \Drupal\publisso_gold\Controller\WorkflowInfoMail;

/**
 * Provides a simple example form.
 */
class publisso_goldInvitationActionAccept extends FormBase {

	private $current_user = NULL;
	private $modname = 'publisso_gold';
	private $modpath;

	/**
	* {@inheritdoc}
	*/
	public function getFormId() {
		return 'publisso_goldinvitationActionAccept';
	}

	/**
	* {@inheritdoc}
	*/
	public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
		
		$this->modpath = drupal_get_path('module', $this->modname);
		require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
		
		$this->current_user = new \Drupal\publisso_gold\Controller\User($_SESSION['user']['id']);
		
		$form_state->setCached(false); //don't cache this form
		$form = [];
		
		if(!$form_state->get('token') && array_key_exists('token', $args))
			$form_state->set('token', $args['token']);
		
		$result = \Drupal::database()->select('rwPubgoldUserInvitations', 't')->fields('t', [])->condition('token', $form_state->get('token'), '=')->execute()->fetchAssoc();
		
                if(usedLocal($result['user'], 'invitations') || usedLocal($result['email'], 'invitations')){
                        $form['info'] = [
                                '#type' => 'markup',
                                '#markup' => (string)t('The user name or email address has been used in the meantime for a different registration process. Please contact the administration.'),
                                '#suffix' => '<br><br>'
                        ];
                        
                        return $form;
                }
    
		$form['info'] = [
			'#type' => 'markup',
			'#markup' => (string)t('Thank you for accepting the invitation. Please check your personal data and, if necessary, complete your profile.'),
			'#suffix' => '<br><br>'
		];
		
		$form['data'] = [
			'#type' => 'markup',
			'#markup' => '
				<div class="rwTable">
					<div class="rwTablerow">
						<div class="rwTablecell head">
							'.t('Firstname').'
						</div>
						<div class="rwTablecell">
							'.$result['firstname'].'
						</div>
					</div>
					<div class="rwTablerow">
						<div class="rwTablecell head">
							'.t('Lastname').'
						</div>
						<div class="rwTablecell">
							'.$result['lastname'].'
						</div>
					</div>
					<div class="rwTablerow">
						<div class="rwTablecell head">
							'.t('Email').'
						</div>
						<div class="rwTablecell">
							'.$result['email'].'
						</div>
					</div>
				</div>
			'
		];
		
		$form['submit'] = [
			'#type' => 'submit',
			'#button_type' => 'success',
			'#value' => t('Accept invitation and create profile'),
			'#suffix' => '<br><br>'
		];
		
		return $form;
	}

	/**
	* {@inheritdoc}
	*/
	public function validateForm(array &$form, FormStateInterface $form_state) {
		
		return $form;
	}

	/**
	* {@inheritdoc}
	*/
	public function submitForm(array &$form, FormStateInterface $form_state) {
		
		$roles = [
			'edoff' => 'editorial office',
			'eic' => 'editor-in-chief',
			'editor' => 'editor',
			'reviewer' => 'reviewer',
			'author' => 'author'
		];
		
		$result = \Drupal::database()->select('rwPubgoldUserInvitations', 't')->fields('t', [])->condition('token', $form_state->get('token'), '=')->execute()->fetchAssoc();
		$mailvars = [];
		
		$result['role'] = $roles[$result['role']];
		
		foreach($result as $key => $val){
			$mailvars["::$key::"] = $val;
		}
		
		$recipient = new \Drupal\publisso_gold\Controller\User($result['created_by_uid']);
		
		foreach($recipient->getKeys() as $key){
			$mailvars["::recipient.$key::"] = $recipient->getElement($key);
		}
		
		foreach($recipient->profile->getKeys() as $key){
			$mailvars["::recipient.profile.$key::"] = $recipient->profile->getElement($key);
		}
		
		if($result['wf_id']){
			
			$workflow = new \Drupal\publisso_gold\Controller\Workflow($result['wf_id']);
			
			switch($workflow->getDataElement('type')){
				
				case 'bookchapter':
					$medium = new \Drupal\publisso_gold\Controller\Book($workflow->getDataElement('bk_id'));
					break;
				
				case 'journalarticle':
					$medium = new \Drupal\publisso_gold\Controller\Journal($workflow->getDataElement('jrn_id'));
					break;
				
				case 'conferencepaper':
					$medium = new \Drupal\publisso_gold\Controller\Conference($workflow->getDataElement('cf_id'));
					break;
			}
			
			foreach($workflow->getKeys() as $key){
				$mailvars["::workflow.$key::"] = $workflow->getElement($key);
			}
			
			foreach($workflow->getDataKeys() as $key){
				$mailvars["::workflow.data.$key::"] = $workflow->getElement($key);
			}
		}
		
		if(!$medium && $result['medium_type'] && $result['medium_id']){
			
			switch($result['medium_type']){
				
				case 'book':
					$medium = new \Drupal\publisso_gold\Controller\Book($result['medium_id']);
					break;
				
				case 'journal':
					$medium = new \Drupal\publisso_gold\Controller\Journal($result['medium_id']);
					break;
				
				case 'conference':
					$medium = new \Drupal\publisso_gold\Controller\Conference();
					break;
			}
		}
		
		if($medium){
			
			foreach($medium->getControlKeys() as $key){
				$mailvars["::medium.control.$key::"] = $medium->getControlElement($key);
			}
			
			foreach($medium->getKeys() as $key){
				$mailvars["::medium.$key::"] = $medium->getElement($key);
			}
		}
		
		
		//create user
		$fields = [
			'user' 					=> $result['user'],
			'password' 				=> $result['password'],
			'active' 				=> 1,
			'locked' 				=> 0,
			'pw_change_required' 	=> 0,
			'reg_notification_sent' => 1
		];
		
		$uid = \Drupal::database()->insert('rwPubgoldUsers')->fields($fields)->execute();
		
		if($uid){
			
			//set userrole
			$fields = [
				'user_id' => $uid,
				'role_id' => 7
			];
			
			\Drupal::database()->insert('rwPubgoldUserroles')->fields($fields)->execute();
			
			//set profile
			$fields = [
				'up_uid' => $uid,
				'up_firstname' 	=> $result['firstname'],
				'up_lastname' 	=> $result['lastname'],
				'up_email' 		=> $result['email']
			];
			
			\Drupal::database()->insert('rwPubgoldUserProfiles')->fields($fields)->execute();
			
			$user = new \Drupal\publisso_gold\Controller\User($uid);
			
			switch($result['role']){
				
				case 'reviewer':
					$medium->addReviewer($uid);
					
					if($workflow){
					        
						$workflow->setElement('assigned_to_reviewer', $workflow->getElement('assigned_to_reviewer').(!empty($workflow->getElement('assigned_to_reviewer')) ? ',' : '').$user->getElement('id'));
						
						
						if($workflow->getDataElement('state') == 'in review'){
							$workflow->setElement('assigned_to', $workflow->getElement('assigned_to').(!empty($workflow->getElement('assigned_to')) ? ',' : '').'u:'.$user->getElement('id'));
              $mail = new WorkflowInfoMail($workflow, 'in review');
              if($mail) $mail->send();
							//sendWorkflowInfoMail($workflow, 'in review');
						}
						if($workflow->getDataElement('state') == 'assigned to editor'){
							$workflow->setState('in review');
						}
						
						$workflow->newUserReview($uid);
					}
					break;
				
				case 'eic':
					$medium->addEditorsInChief($user->getElement('id'));
					break;
				
				case 'edoff':
					$medium->addEitorialOffice($user->getElement('id'));
					break;
				
				case 'editor':
					$medium->addEditor($user->getElement('id'));
					break;
				
				case 'author':
					$medium->addAuthor($user->getElement('id'));
					break;
			}
		}
		
		\Drupal::database()->update('rwPubgoldUserInvitations')->fields(['token' => md5(time()), 'status' => 'accepted'])->condition('token', $form_state->get('token'), '=')->execute();
		drupal_set_message(t('Your account has been successfully created. You can now login with the credentials you received via email.'));
		
		$mailout = getMailtemplate('info invitation accepted');
		$mailout['recipient'] = str_replace(array_keys($mailvars), array_values($mailvars), $mailout['recipient']);
		sendInfoMail($mailout, $mailvars);
		$form_state->setRedirect('publisso_gold.login');
		return $form;
	}
}






