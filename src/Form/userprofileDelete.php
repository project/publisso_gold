<?php
/**
* @file
* Contains \Drupal\publisso_gold\Form\userprofileDelete.
*/
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Url;

/**
* Provides a simple example form.
*/
class userprofileDelete extends FormBase {

        /**
        * {@inheritdoc}
        */
        public function getFormId() {
                return 'userprofileDelete';
        }

        /**
        * {@inheritdoc}
        */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                foreach(['texts', 'tools', 'setup'] as $_){
                        if(!$form_state->has($_)) $form_state->set($_, \Drupal::service("publisso_gold.$_"));
                }
                
                $fields = \Drupal::service('publisso_gold.form.fields');
                $user   = new \Drupal\publisso_gold\Controller\User(\Drupal::service('session')->get('user')['id']);
                
                $message = 'message_'.($user->hasActiveProcesses() ? 'active_processes' : 'warning_delete');
                $form['message'] = $fields->getField("userprofile_delete.$message", $args);
                
                $form['actions'] = [
                        '#type'   => 'container',
                        '#prefix' => '<hr>'
                ];
                
                $form['actions']['cancel'] = [
                        
                        '#type'        => 'button',
                        '#value'       => $this->t($form_state->get('texts')->get('form.userprofile.delete.btn.cancel', 'fc')),
                        '#button_type' => 'success',
                        '#ajax'        => [
                                'callback' => '::cancel',
                                'progress' => [
                                        '#type' => 'none'
                                ]
                        ],
                        '#suffix'      => '&nbsp;&nbsp;&nbsp;'
                ];
                
                if($user->hasActiveProcesses() === false){
                        
                        $form['actions']['delete'] = [
                                
                                '#type'        => 'button',
                                '#value'       => $this->t($form_state->get('texts')->get('form.userprofile.delete.btn.submit', 'fc')),
                                '#button_type' => 'danger',
                                '#suffix'      => '&nbsp;&nbsp;&nbsp;',
                                '#ajax'        => [
                                        'callback' => '::submitForm',
                                        'progress' => [
                                                '#type' => 'none'
                                        ]
                                ],
                        ];
                }
                
                $form['#cache']['max-age'] = 0;
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function validateForm(array &$form, FormStateInterface $form_state) {

                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                
                $user = new \Drupal\publisso_gold\Controller\User(\Drupal::service('session')->get('user')['id']);
                
                $user->setElement('active', 0);
                
                \Drupal::httpClient()->request(
                        'POST', 
                        'https://lvps46-163-114-198.dedicated.hosteurope.de/zbmedauth/sso', 
                        [
                                'headers' => [
                                        'Content-Type' => 'application/text'
                                ], 
                                'http_errors' => false, 
                                'verify' => false, 
                                'query' => [
                                        'token' => $token, 
                                        'delSession' => 1, 
                                        'apikey' => \Drupal::service('publisso_gold.setup')->getValue('system.sso.apikey'), 
                                        'sso_system' => \Drupal::service('publisso_gold.setup')->getValue('system.sso.name')
                                ]
                        ]
                );
                
                //send the Email
                if(null !== ($template = $form_state->get('tools')->getMailtemplate('userprofile delete'))){
                        
                        $token = uniqid(md5($user->profile->getReadableName()), true);
                        
                        \Drupal::database()->insert('rwPubgoldUserDeletePending')->fields([
                                'uid'   => $user->getElement('id'),
                                'token' => $token
                        ])->execute();
                        
                        $vars = [];
                        $vars['::link.delete.confirm::'] = Url::fromRoute('publisso_gold.userprofile.delete', ['token' => $token])->setAbsolute()->toString();
                        
                        foreach($user->getKeys() as $_) $vars["user.$_"] = $user->getElement($_);
                        foreach($user->profile->getKeys() as $_) $vars["::user.profile.$_::"] = $user->profile->getElement($_);
                        
                        $to      = str_replace(array_keys($vars), array_values($vars), $template->recipient);
                        
                        $subject = $this->t($template->subject , $vars);
                        $body    = $this->t($template->template, $vars);
                        
                        
                        $form_state->get('tools')->sendMail($to, $subject, $body);
                        
                        \Drupal::database()->insert('rwPubgoldMaillog')->fields([
                                'recipient'             => $to,
                                'subject'               => $subject,
                                'body'                  => $body,
                                'init_process'          => 'profiledelete',
                                'to_uid'                => $user->getElement('id'),
                                'created_by_uid'        => $user->getElement('id')
                        ])->execute();
                }
                
                \Drupal::service('publisso_gold.tools')->destroyUserSession();
                
                //close Dialog and redirect to Login
                \Drupal::service('messenger')->addMessage($this->t(
                        $form_state->get('texts')->get('form.userprofile.delete.msg.success')
                ));
                
                $response = new AjaxResponse;
                
                $command  = new CloseModalDialogCommand();
                $response->addCommand($command);
                
                $command  = new RedirectCommand(Url::fromRoute('publisso_gold.login')->toString());
                $response->addCommand($command);
                
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return AjaxResponse
         */
        public function cancel(array &$form, FormStateInterface $form_state){
                
                $response = new AjaxResponse;
                
                $command  = new CloseModalDialogCommand();
                $response->addCommand($command);
                
                return $response;
        }
}
