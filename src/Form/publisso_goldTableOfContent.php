<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldTableOfContent.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldTableOfContent extends FormBase {
    
    private $modname = 'publisso_gold';
    private $modpath;
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldtableofcontent';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        if($this->modname && !$form_state->get('modname')){
            $form_state->set('modname', $this->modname);
        }
        
        if(!$this->modname && $form_state->get('modname')){
            $this->modname = $form_state->get('modname');
        }
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        $select = \Drupal::database()->select('rwPubgoldTableOfContent', 'toc')->fields('toc', ['toc_id', 'toc_name', 'toc_description'])->condition('toc_name', 'article__%', 'NOT LIKE');
        
        if($form_state->get('filter') === false){
            //$select->condition('toc_medium', 'book', '=');
        }
        
        $result = $select->execute()->fetchAll();
        
        foreach($result as $_){
            $options[$_->toc_id] = [
                'name'          => $_->toc_name,
                'description'   => $_->toc_description
            ];
        }
        
        /**
         * Provides Filters
         
        $form['filter'] = [];
        
        $form['filter']['filter'] = [
            '#prefix'       => '<hr>',
            '#type'         => 'select',
            '#title'        => (string)t('Filter'),
            '#description'  => (string)t('please choose to narrow down the results'),
            '#required'     => false,
            
            '#options'      => [
                ''             => (string)t('not assigned'),
                'book'         => (string)t('Books'),
                'journal'      => (string)t('Journals'),
                'conference'   => (string)t('Conferences')
            ]
        ];
        
        if($form_state->get('filter')){
            $form['filter']['filter']['#default_value'] = $form_state->get('filter');
        }
        
        $form['filter']['submit'] = [
            '#type'     => 'submit',
            '#value'    => (string)t('apply filter'),
            '#submit'   => ['::apply_filter']
        ];
        */
        $markup = '<hr>';
        $markup .= '<h3>'.((string)t('Existing tables of content')).'</h3>';
        $markup .= '<div class="rwTable">
                        <div class="rwTablerow">
                            <div class="rwTablecell head">
                                Name
                            </div>
                            <div class="rwTablecell head">
                                Description
                            </div>
                            <div class="rwTablecell head">
                                &nbsp;
                            </div>
                            <div class="rwTablecell head">
                                &nbsp;
                            </div>
                        </div>';
        
        foreach($result as $_){
            
            $urlEdit = Url::fromRoute('publisso_gold.table_of_content.edit', ['toc_id' => $_->toc_id]);
            $lnkEdit = Link::fromTextAndUrl((string)t('edit'), $urlEdit);
            
            $urlDelete = Url::fromRoute('publisso_gold.table_of_content.delete', ['toc_id' => $_->toc_id]);
            $lnkDelete = Link::fromTextAndUrl((string)t('delete'), $urlEdit);
            
            $markup .= '<div class="rwTablerow">
                            <div class="rwTablecell">
                                '.$_->toc_name.'
                            </div>
                            <div class="rwTablecell">
                                '.$_->toc_description.'
                            </div>
                            <div class="rwTablecell">
                                '.$lnkEdit->toString().'
                            </div>
                            <div class="rwTablecell">
                                '.$lnkDelete->toString().'
                            </div>
                        </div>';
        }
        
        $markup .= '</div>';
        
        $form['result'] = [
            '#type'     => 'markup',
            '#markup'   => $markup
        ];
        
        $form['#cache']['max-age'] = 0;
        $form_state->setCached(false);
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function apply_filter(array &$form, FormStateInterface $form_state){
        
        $form_state->set('filter', $form_state->getValue('filter'));
        
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function add_toc(array &$form, FormStateInterface $form_state){
        
        $form_state->setRedirect('publisso_gold.table_of_content.add');
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        
        return $form;
    }
}
