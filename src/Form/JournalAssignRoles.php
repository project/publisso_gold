<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\JournalAssignRoles.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormState;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class JournalAssignRoles extends FormBase {
    
        const __ACCESS_DENIED__             = 'You can not access this page. You may need to log in first.';

        private $modname = 'publisso_gold';
        private $modpath;
        private $current_user    = null;
        private $modaccessweights;

        /**
        * {@inheritdoc}
        */
        public function getFormId() {
                return 'journalassignroles';
        }

        /**
        * {@inheritdoc}
        */
        public function buildForm(array $form, FormStateInterface $form_state, $args = null) {
        
                if(!$this->current_user)
                        $this->current_user = new \Drupal\publisso_gold\Controller\User(\Drupal::service('session')->get('user')['id']);
    
                $this->getModuleAccessPermissions();
                $this->modpath = drupal_get_path('module', $this->modname);
                require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');

// check various things
                if(!$form_state->get('jrn_id'))
                        $form_state->set('jrn_id', $args['jrn_id']);
    
                if(!$form_state->get('jrn_id')){
                        drupal_set_message(t('No Journal given!'), 'error');
                        return [];
                }
    
                $journal = new \Drupal\publisso_gold\Controller\Journal($form_state->get('jrn_id'));
        
                $user = \Drupal::service('session')->get('user');
        
                if(is_array($user)) {
                        $user_has_rights = $user[ 'isEditor' ] | $user[ 'isEiC' ] | $user[ 'isEO' ];
                }
                
                if(!($user_has_rights                                         ||
                     $journal->isUserEditorialOffice(\Drupal::service('session')->get('user')['id']) ||
		     $journal->isUserEditorInChief(\Drupal::service('session')->get('user')['id'])   ||
		     $journal->isUserEditor(\Drupal::service('session')->get('user')['id'])          ||
                     \Drupal::service('publisso_gold.tools')->userHasAccessRight('journalmanagement')                )){
                     
                        return array(
                                '#type' => 'markup',
                                '#markup' => self::__ACCESS_DENIED__,
                                '#attached' => [
                                        'library' => [
                                                'publisso_gold/default'
                                        ]
                                ]
                        );
                }
    
// -- check various things --



// Main Form
                $form['title'] = [
                        '#type'   => 'markup',
                        '#markup' => $journal->getElement('title'),
                        '#prefix' => '<h1>',
                        '#suffix' => '</h1>'
                ];
	
                $form['assign'] = [
                        'edoff'    => $this->getEdoff($form_state),
                        'eic'      => $this->getEiC($form_state),
                        'editor'   => $this->getEditor($form_state),
                        'reviewer' => $this->getReviewer($form_state)/*,
                        'author'   => $this->getAuthor($form_state)*/
                ];
    
                $weights = [
                        'edoff'    => 5,
                        'eic'      => 4,
                        'editor'   => 3,
                        'reviewer' => 2,
                        'author'   => 1
                ];
        
                $myWeight = $this->getRoleWeight($journal);
    
                foreach($form['assign'] as $_role => $_form){
                        if($myWeight < $weights[$_role])
                        unset($form['assign'][$_role]);
                }
    
                $form['assign']['#tree'] = true;
    
                $form['submit'] = [
                        '#type'   => 'submit',
                        '#value'  => (string)t('Save'),
                        '#suffix' => '<br><br>'
                ];
// -- Main Form --    
    
                $form['#cache'] = [
                        'max-age' => 0
                ];
    
                return $form;
        }
        
        /**
         * @param $journal
         * @return int
         */
        private function getRoleWeight(&$journal){
        
                $weight = 1; //author is default
        
		if($journal->isUserReviewer(\Drupal::service('session')->get('user')['id']))         $weight = 2;
		if($journal->isUserEditor(\Drupal::service('session')->get('user')['id']))           $weight = 3;
		if($journal->isUserEditorInChief(\Drupal::service('session')->get('user')['id']))    $weight = 4;
		if($journal->isUserEditorialOffice(\Drupal::service('session')->get('user')['id']))  $weight = 5;
        
                if(\Drupal::service('session')->get('user')['role_id'] == 2) $weight = 6; //PUBLISSO
                if(\Drupal::service('session')->get('user')['role_id'] == 1) $weight = 7; //Administrator
        
                return $weight;
        }
        
        /**
         * @param $tmpl
         * @param array $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl, $vars = array()){
        
                //set Site-Vars
                $tmpl = str_replace(array_keys($vars), array_values($vars), $tmpl);
        
                //remove unused vars
                $tmpl = preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $tmpl);
        
                return $tmpl;
        }

        /**
        * {@inheritdoc}
        */
        public function validateForm(array &$form, FormStateInterface $form_state) {
		
		$roles = [];
		
		foreach($form_state->getValue('assign') as $role => $users){
			$roles[$role] = count(array_filter($users['content']['values']));
		}
		
		if($roles['eic'] < 1){
			$form_state->setErrorByName('eic', (string)t('Please assign at least one editor-in-chief ('.$roles['eic'].')!'));
		}
		
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function submitForm(array &$form, FormStateInterface $form_state) {
        
                $values = $form_state->getValues();
                $journal = new \Drupal\publisso_gold\Controller\Journal($form_state->get('jrn_id'));
                error_log("================ Assign new roles ================\n", 0);
                        error_log(\Drupal::service('session')->get('user')['id'].': '.\Drupal::service('session')->get('user')['firstname'].' '.\Drupal::service('session')->get('user')['lastname']."\n", 0);
                        
                foreach($values['assign'] as $role => $users){
                
                        error_log($role, 0);
                        $uids = [];
                        
                        foreach($users['content']['values'] as $user){
                                
                                error_log("$user\n", 0);
                                $user = explode('|', $user);
                                $user = base_convert(trim($user[0]), 36, 10);
                                if(!empty($user)) $uids[] = $user;
                        }
                        
                        switch($role){
                                
                                case 'edoff':
                                        $journal->setEditorialOffice(array_unique($uids));
                                        break;
                                
                                case 'eic':
                                        $journal->setEditorsInChief(array_unique($uids));
                                        break;
                                
                                case 'editor':
                                        $journal->setEditors(array_unique($uids));
                                        break;
                                
                                case 'reviewer':
                                        $journal->setReviewers(array_unique($uids));
                                        break;
                                
                                case 'author':
                                        $journal->setAuthors(array_unique($uids));
                                        break;
                        }
                        //echo '<pre>'.print_r($book, 1).'</pre>';
                }
                
                error_log("==================================================\n", 0);
                drupal_set_message(t('Changes successfully saved!'));
                $form_state->setRedirect('publisso_gold.journalmanagement');
                //return $form;
        }   
    
// Sub Edoff    
        
        /**
         * @param FormStateInterface $form_state
         * @return array
         */
        private function getEdoff(FormStateInterface $form_state){
        
        $type = 'edoff';
        $title = 'Editorial Office';
        $journal = new \Drupal\publisso_gold\Controller\Journal($form_state->get('jrn_id'));
        $$type = $journal->readEditorialOffice();
        
        $element = [
            '#title' => t($title),
            '#type' => 'fieldset',
            '#collapsible' => false,
            '#collapsed' => false,
            '#tree' => true,
            'content' => [
                '#tree' => true,
                '#prefix' => '<div id="'.$type.'Wrapper">',
                '#suffix' => '</div>',
                'values' => []
            ],
        ];
        
        $rebuildInfo = $form_state->get('is_rebuild');
        
        if(!$form_state->get('cnt_'.$type))
            $form_state->set('cnt_'.$type, count($$type));
        
        for($c = 0; $c < $form_state->get('cnt_'.$type); $c++){
            
            if(array_key_exists($c, $$type)){
                
                $user = ${$type}[$c];
                $uid = strtoupper(base_convert($user->getElement('id'), 10, 36));
                $firstname = $user->profile->getElement('firstname');
                $lastname = $user->profile->getElement('lastname');
                $element_value = "$uid | $firstname $lastname";
            }
            else{
                $element_value = '';
            }
            
            $element['content']['values'][] = [
                '#type' => 'textfield',
				'#autocomplete_route_name' => 'autocomplete.user',
				'#autocomplete_route_parameters' => array(),
                '#default_value' => $element_value
            ];
        }
        
        $element['content'][$type.'submit'] = [
            '#type' => 'submit',
            '#value' => t('Add User as EO'),
			'#limit_validation_errors' => [],
            '#submit' => ['::Add'.ucfirst($type)],/*
            '#ajax' => array(
				'callback' => '::Add'.ucfirst($type).'Callback',
				'wrapper' => 'edoff_wrapper',
				'effect' => 'fade'
			)*/
        ];
        
        return $element;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function AddEdoff(array &$form, FormStateInterface $form_state){
        
        $type = 'edoff';
        $form[$type]['content']['values'][] = [
            '#type' => 'textfield',
            '#autocomplete_route_name' => 'autocomplete.user',
            '#autocomplete_route_parameters' => array()
        ];
        
        $form_state->set('cnt_'.$type, $form_state->get('cnt_'.$type) + 1);
        $form_state->set('is_rebuild', 1);
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function AddEdoffCallback(array &$form, FormStateInterface $form_state){
        $type = 'edoff';
        return $form[$type]['content']['values'];
    }
// -- Sub Edoff --   

// Sub EiC    
        /**
         * @param FormStateInterface $form_state
         * @return array
         */
        private function getEic(FormStateInterface $form_state){
        
        $type = 'eic';
        $title = 'Editors in Chief';
        $journal = new \Drupal\publisso_gold\Controller\Journal($form_state->get('jrn_id'));
        $$type = $journal->readEditorsInChief();
        
        $element = [
            '#title' => t($title),
            '#type' => 'fieldset',
            '#collapsible' => false,
            '#collapsed' => false,
            '#tree' => true,
            'content' => [
                '#tree' => true,
                '#prefix' => '<div id="'.$type.'Wrapper">',
                '#suffix' => '</div>',
                'values' => []
            ],
        ];
        
        $rebuildInfo = $form_state->getRebuildInfo();
        
        if(!$form_state->get('cnt_'.$type))
            $form_state->set('cnt_'.$type, count($$type));
        
        for($c = 0; $c < $form_state->get('cnt_'.$type); $c++){
            
            if(array_key_exists($c, $$type)){
                
                $user = ${$type}[$c];
                $uid = strtoupper(base_convert($user->getElement('id'), 10, 36));
                $firstname = $user->profile->getElement('firstname');
                $lastname = $user->profile->getElement('lastname');
                $element_value = "$uid | $firstname $lastname";
            }
            else{
                $element_value = '';
            }
            
            $element['content']['values'][] = [
                '#type' => 'textfield',
				'#autocomplete_route_name' => 'autocomplete.user',
				'#autocomplete_route_parameters' => array(),
                '#default_value' => $element_value
            ];
        }
        
        $element['content'][$type.'submit'] = [
            '#type' => 'submit',
            '#value' => t('Add User as EiC'),
			'#limit_validation_errors' => [],
            '#submit' => ['::Add'.ucfirst($type)],/*
            '#ajax' => array(
				'callback' => '::Add'.ucfirst($type).'Callback',
				'wrapper' => 'edoff_wrapper',
				'effect' => 'fade'
			)*/
        ];
        
        return $element;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function AddEic(array &$form, FormStateInterface $form_state){
        
        $type = 'eic';
        $form[$type]['content']['values'][] = [
            '#type' => 'textfield',
            '#autocomplete_route_name' => 'autocomplete.user',
            '#autocomplete_route_parameters' => array()
        ];
        
        $form_state->set('cnt_'.$type, $form_state->get('cnt_'.$type) + 1);
        $form_state->set('is_rebuild', 1);
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function AddEicCallback(array &$form, FormStateInterface $form_state){
        $type = 'eic';
        return $form[$type]['content']['values'];
    }
// -- Sub EiC --  

// Sub Editor   
        /**
         * @param FormStateInterface $form_state
         * @return array
         */
        private function getEditor(FormStateInterface $form_state){
        
        $type = 'editor';
        $title = 'Editors';
        $journal = new \Drupal\publisso_gold\Controller\journal($form_state->get('jrn_id'));
        $$type = $journal->readEditors();
        
        $element = [
            '#title' => t($title),
            '#type' => 'fieldset',
            '#collapsible' => false,
            '#collapsed' => false,
            '#tree' => true,
            'content' => [
                '#tree' => true,
                '#prefix' => '<div id="'.$type.'Wrapper">',
                '#suffix' => '</div>',
                'values' => []
            ],
        ];
        
        $rebuildInfo = $form_state->getRebuildInfo();
        
        if(!$form_state->get('cnt_'.$type))
            $form_state->set('cnt_'.$type, count($$type));
        
        for($c = 0; $c < $form_state->get('cnt_'.$type); $c++){
            
            if(array_key_exists($c, $$type)){
                
                $user = ${$type}[$c];
                $uid = strtoupper(base_convert($user->getElement('id'), 10, 36));
                $firstname = $user->profile->getElement('firstname');
                $lastname = $user->profile->getElement('lastname');
                $element_value = "$uid | $firstname $lastname";
            }
            else{
                $element_value = '';
            }
            
            $element['content']['values'][] = [
                '#type' => 'textfield',
				'#autocomplete_route_name' => 'autocomplete.user',
				'#autocomplete_route_parameters' => array(),
                '#default_value' => $element_value
            ];
        }
        
        $element['content'][$type.'submit'] = [
            '#type' => 'submit',
            '#value' => t('Add User as Editor'),
			'#limit_validation_errors' => [],
            '#submit' => ['::Add'.ucfirst($type)],/*
            '#ajax' => array(
				'callback' => '::Add'.ucfirst($type).'Callback',
				'wrapper' => 'edoff_wrapper',
				'effect' => 'fade'
			)*/
        ];
        
        return $element;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function AddEditor(array &$form, FormStateInterface $form_state){
        
        $type = 'editor';
        $form[$type]['content']['values'][] = [
            '#type' => 'textfield',
            '#autocomplete_route_name' => 'autocomplete.user',
            '#autocomplete_route_parameters' => array()
        ];
        
        $form_state->set('cnt_'.$type, $form_state->get('cnt_'.$type) + 1);
        $form_state->set('is_rebuild', 1);
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function AddEditorCallback(array &$form, FormStateInterface $form_state){
        $type = 'eic';
        return $form[$type]['content']['values'];
    }
// -- Sub Editor --  

// Sub Reviewer  
        /**
         * @param FormStateInterface $form_state
         * @return array
         */
        private function getReviewer(FormStateInterface $form_state){
        
        $type = 'reviewer';
        $title = 'Reviewers';
        $journal = new \Drupal\publisso_gold\Controller\Journal($form_state->get('jrn_id'));
        $$type = $journal->readReviewers();
        
        $element = [
            '#title' => t($title),
            '#type' => 'fieldset',
            '#collapsible' => false,
            '#collapsed' => false,
            '#tree' => true,
            'content' => [
                '#tree' => true,
                '#prefix' => '<div id="'.$type.'Wrapper">',
                '#suffix' => '</div>',
                'values' => []
            ],
        ];
        
        $rebuildInfo = $form_state->getRebuildInfo();
        
        if(!$form_state->get('cnt_'.$type))
            $form_state->set('cnt_'.$type, count($$type));
        
        for($c = 0; $c < $form_state->get('cnt_'.$type); $c++){
            
            if(array_key_exists($c, $$type)){
                
                $user = ${$type}[$c];
                $uid = strtoupper(base_convert($user->getElement('id'), 10, 36));
                $firstname = $user->profile->getElement('firstname');
                $lastname = $user->profile->getElement('lastname');
                $element_value = "$uid | $firstname $lastname";
            }
            else{
                $element_value = '';
            }
            
            $element['content']['values'][] = [
                '#type' => 'textfield',
				'#autocomplete_route_name' => 'autocomplete.user',
				'#autocomplete_route_parameters' => array(),
                '#default_value' => $element_value
            ];
        }
        
        $element['content'][$type.'submit'] = [
            '#type' => 'submit',
            '#value' => t('Add User as Reviewer'),
			'#limit_validation_errors' => [],
            '#submit' => ['::Add'.ucfirst($type)],/*
            '#ajax' => array(
				'callback' => '::Add'.ucfirst($type).'Callback',
				'wrapper' => 'edoff_wrapper',
				'effect' => 'fade'
			)*/
        ];
        
        return $element;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function AddReviewer(array &$form, FormStateInterface $form_state){
        
        $type = 'reviewer';
        $form[$type]['content']['values'][] = [
            '#type' => 'textfield',
            '#autocomplete_route_name' => 'autocomplete.user',
            '#autocomplete_route_parameters' => array()
        ];
        
        $form_state->set('cnt_'.$type, $form_state->get('cnt_'.$type) + 1);
        $form_state->set('is_rebuild', 1);
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function AddReviewerCallback(array &$form, FormStateInterface $form_state){
        $type = 'reviewer';
        return $form[$type]['content']['values'];
    }
// -- Sub Reviewer --  

// Sub Author    
        /**
         * @param FormStateInterface $form_state
         * @return array
         */
        private function getAuthor(FormStateInterface $form_state){
        
        $type = 'author';
        $title = 'Authors';
        $book = new \Drupal\publisso_gold\Controller\Book($form_state->get('bk_id'));
        $$type = $book->readAuthors();
        
        $element = [
            '#title' => t($title),
            '#type' => 'fieldset',
            '#collapsible' => false,
            '#collapsed' => false,
            '#tree' => true,
            'content' => [
                '#tree' => true,
                '#prefix' => '<div id="'.$type.'Wrapper">',
                '#suffix' => '</div>',
                'values' => []
            ],
        ];
        
        $rebuildInfo = $form_state->getRebuildInfo();
        
        if(!$form_state->get('cnt_'.$type))
            $form_state->set('cnt_'.$type, count($$type));
        
        for($c = 0; $c < $form_state->get('cnt_'.$type); $c++){
            
            if(array_key_exists($c, $$type)){
                
                $user = ${$type}[$c];
                $uid = strtoupper(base_convert($user->getElement('id'), 10, 36));
                $firstname = $user->profile->getElement('firstname');
                $lastname = $user->profile->getElement('lastname');
                $element_value = "$uid | $firstname $lastname";
            }
            else{
                $element_value = '';
            }
            
            $element['content']['values'][] = [
                '#type' => 'textfield',
				'#autocomplete_route_name' => 'autocomplete.user',
				'#autocomplete_route_parameters' => array(),
                '#default_value' => $element_value
            ];
        }
        
        $element['content'][$type.'submit'] = [
            '#type' => 'submit',
            '#value' => t('Add User as Author'),
			'#limit_validation_errors' => [],
            '#submit' => ['::Add'.ucfirst($type)],/*
            '#ajax' => array(
				'callback' => '::Add'.ucfirst($type).'Callback',
				'wrapper' => 'edoff_wrapper',
				'effect' => 'fade'
			)*/
        ];
        
        return $element;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function AddAuthor(array &$form, FormStateInterface $form_state){
        
        $type = 'author';
        $form[$type]['content']['values'][] = [
            '#type' => 'textfield',
            '#autocomplete_route_name' => 'autocomplete.user',
            '#autocomplete_route_parameters' => array()
        ];
        
        $form_state->set('cnt_'.$type, $form_state->get('cnt_'.$type) + 1);
        $form_state->set('is_rebuild', 1);
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function AddAuthorCallback(array &$form, FormStateInterface $form_state){
        $type = 'author';
        return $form[$type]['content']['values'];
    }
// -- Sub Author --  

    private function getModuleAccessPermissions(){
        
        $result = \Drupal::database()->select('rwPubgoldModuleaccess', 'ma')->fields('ma', [])->execute()->fetchAll();
        
        foreach($result as $res){
            $this->modaccessweights[$res->mod_name] = $res->mod_minaccessweight;
        }
    }
        
        /**
         * @param $modname
         * @return bool
         */
        private function userHasAccessRight($modname){
        
        if(!array_key_exists($modname, $this->modaccessweights)){
            return false;
        }
        return $this->modaccessweights[$modname] <= \Drupal::service('session')->get('user')['weight'];
    }
}
