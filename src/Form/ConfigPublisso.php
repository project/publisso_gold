<?php
        
        namespace Drupal\publisso_gold\Form;
        use Drupal\Core\Config\ConfigFactoryInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class ConfigPublisso
         * @package Drupal\publisso_gold\Form
         */
        class ConfigPublisso extends FormBase {
        
                /**
                 * @return string
                 */
                public function getFormId () {
                        return 'PublissoConfigForm';
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function buildForm (array $form, FormStateInterface $form_state ) {
                        
                        $config = \Drupal::configFactory()->getEditable('publisso_gold.config');
                        
                        $redirect = $config->get('domain_redirect') ?? [];
                        
                        if(!$form_state->has('cntPresetRedirect')){
                                $form_state->set('cntPresetRedirect', count($redirect));
                        }
                        
                        $form['#tree'] = true;
                        
                        $roles = \Drupal::database()->select('rwPubgoldRoles', 't')->fields('t', ['id', 'name'])->execute()->fetchAllKeyed();
                        $form['config']['admin_role'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('Administrative role'),
                                '#required' => true,
                                '#multiple' => false,
                                '#default_value' => $config->get('admin_role'),
                                '#options' => $roles
                        ];
        
                        
                        $roles = \Drupal::database()->select('rwPubgoldRoles', 't')->fields('t', ['id', 'name'])->execute()->fetchAllKeyed();
                        $form['config']['publisso_role'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('PUBLISSO role'),
                                '#required' => true,
                                '#multiple' => false,
                                '#default_value' => $config->get('publisso_role'),
                                '#options' => $roles
                        ];
                        
                        $form['wrapper-domain_redirect'] = [
                                '#type' => 'details',
                                '#title' => (string)$this->t('Redirect URL\'s to routes'),
                                '#prefix' => '<div id="wrapper-redirect">',
                                '#suffix' => '</div>',
                                'redirect' => [
                                        '#type' => 'table',
                                        '#header' => [
                                                ['data' => [
                                                        '#markup' => (string)$this->t('URL (without http[s]://)')
                                                ]],
                                                ['data' => [
                                                        '#markup' => (string)$this->t('Route')
                                                ]]
                                        ]
                                ],
                                'del' => [],
                                'add' => [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Add redirect'),
                                        '#submit' => ['::addRedirect'],
                                        '#ajax' => [
                                                'callback' => '::addRedirectCallback',
                                                'wrapper' => 'wrapper-redirect',
                                                'progress' => [
                                                        'type' => 'none'
                                                ]
                                        ],
                                        '#limit_validation_errors' => []
                                ]
                        ];
                        
                        reset($redirect);
                        if($form_state->get('cntPresetRedirect')) {
                                
                                for ($i = 0; $i < $form_state->get('cntPresetRedirect'); $i++) {
        
                                        $current = current($redirect);
                                        next($redirect);
                                        
                                        $form['wrapper-domain_redirect']['redirect'][$i] = [
                                                'url'   => [
                                                        '#type'     => 'textfield',
                                                        '#required' => true,
                                                        '#default_value' => str_replace(':', '.', array_flip($redirect)[$current]) ?? null
                                                ],
                                                'route' => [
                                                        '#type'     => 'textfield',
                                                        '#required' => true,
                                                        '#default_value' => $current ?? null
                                                ]
                                        ];
                                }
                                
                                $form['wrapper-domain_redirect']['del'] = [
                                        
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Delete last entry'),
                                        '#submit' => ['::delRedirect'],
                                        '#ajax' => [
                                                'callback' => '::delRedirectCallback',
                                                'wrapper' => 'wrapper-redirect',
                                                'progress' => [
                                                        'type' => 'none'
                                                ]
                                        ],
                                        '#limit_validation_errors' => []
                                ];
                        }
                        
                        $form['submit'] = [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Save configuration')
                        ];
                        
                        $form['cache'] = ['#max-age' => 0];
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function delRedirect(array &$form, FormStateInterface $form_state){
                        $form_state->set('cntPresetRedirect', $form_state->get('cntPresetRedirect') - 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function delRedirectCallback(array &$form, FormStateInterface $form_state){
                
                        $form['wrapper-domain_redirect']['#attributes']['open'] = '';
                        return $form['wrapper-domain_redirect'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addRedirect(array &$form, FormStateInterface $form_state){
                        $form_state->set('cntPresetRedirect', $form_state->get('cntPresetRedirect') + 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addRedirectCallback(array &$form, FormStateInterface $form_state){
                        
                        $form['wrapper-domain_redirect']['#attributes']['open'] = '';
                        return $form['wrapper-domain_redirect'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function submitForm (array &$form, FormStateInterface $form_state ) {
                        
                        $config = \Drupal::configFactory()->getEditable('publisso_gold.config');
                        $config->setData(array_merge($form_state->getValue('config'), []));
                        
                        $redirects = [];
                        foreach($form_state->getValue('wrapper-domain_redirect')['redirect'] as $_){
                                $redirects[str_replace('.', ':', $_['url'])] = $_['route'];
                        }
                        $config->set('domain_redirect', $redirects);
                        
                        $config->save();
                        \Drupal::service('messenger')->addMessage((string)$this->t('Configuration saved!'));
                        return $form;
                }
        }
