<?php
        
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldMailspool.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Database\Connection;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldMailspool extends FormBase {
                private $modname = 'publisso_gold';
                private $database;
                private $modpath;
                private $num_corporations;
                
                public function __construct ( Connection $database ) {
                        $this->database = $database;
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return publisso_goldMailspool|static
                 */
                public static function create (ContainerInterface $container ) {
                        return new static ( $container->get( 'database' ) );
                }
                
                /**
                 *
                 * {@inheritdoc}
                 *
                 */
                public function getFormId () {
                        return 'publisso_goldmailspool';
                }
                
                /**
                 *
                 * {@inheritdoc}
                 *
                 */
                public function buildForm ( array $form, FormStateInterface $form_state ) {
                        $form_state->disableCache();
                        $session = \Drupal::service( 'session' );
                        
                        $this->modpath = drupal_get_path( 'module', $this->modname );
        
                        if ( $this->modpath && !$form_state->get( 'modpath' ) ) {
                                $form_state->set( 'modpath', $this->modpath );
                        }
                        
                        if ( !$this->modpath && $form_state->get( 'modpath' ) ) {
                                $this->modpath = $form_state->get( 'modpath' );
                        }
        
                        $tempstore = \Drupal::service( 'user.private_tempstore' )->get( 'publisso_gold' );
                        
                        require_once( $this->modpath . '/inc/publisso_gold.lib.inc.php' );
                        
                        $result = \Drupal::database()->select( 'rwPubgoldMailout', 't' )->fields( 't', [] )->condition( 'ready_to_send', 0, '=' )->condition( 'created_by_uid', $session->get( 'user' ) [ 'id' ] )->isNull( 'autosend_parent' )->execute()->fetchAssoc();
                        
                        $header = json_decode( $result [ 'header' ], true );
                        
                        $form [ 'title' ] = [
                                '#type'   => 'markup',
                                '#markup' => t( 'You initiated a process which involves an e-mail. Please check and if necessary adapt, e.g. add further information, before sending the mail.' ),
                                '#prefix' => '<h1>',
                                '#suffix' => '</h1>',
                        ];
                        
                        $form [ 'email' ] = [
                                '#tree' => true,
                                'data'  => [
                                        'recipient'  => [
                                                '#title'         => t( 'Recipient' ),
                                                '#type'          => 'textfield',
                                                '#default_value' => $result [ 'recipient' ],
                                                '#required'      => true,
                                        ],
                                        'cc'         => [
                                                '#type'          => 'textfield',
                                                '#title'         => t( 'CC' ),
                                                '#default_value' => implode( '; ', explode( ',', $header [ 'CC' ] ) ),
                                        ],
                                        'bcc'        => [
                                                '#type'       => 'textfield',
                                                '#title'      => t( 'BCC' ),
                                                '#value'      => implode( '; ', explode( ',', $header [ 'BCC' ] ) ),
                                                '#attributes' => [
                                                        'readonly' => 'readonly',
                                                ],
                                                '#maxlength' => 500
                                        ],
                                        'subject'    => [
                                                '#type'          => 'textfield',
                                                '#title'         => t( 'Subject' ),
                                                '#default_value' => $result [ 'subject' ],
                                        ],
                                        'body'       => [
                                                '#type'          => 'textarea',
                                                '#title'         => t( 'Mailtext' ),
                                                '#default_value' => $result [ 'body' ],
                                                '#rows'          => 10,
                                        ],
                                        'id'         => [
                                                '#type'  => 'hidden',
                                                '#value' => $result [ 'id' ],
                                        ],
                                        'attachment' => [
                                                '#type'  => 'hidden',
                                                '#value' => base64_encode( $result [ 'attachment' ] ),
                                        ],
                                ],
                        ];
                        
                        $form [ 'discard' ] = [
                                '#type'                    => 'submit',
                                '#button_type'             => 'primary',
                                '#value'                   => t( 'Discard' ),
                                '#submit'                  => [
                                        '::discard',
                                ],
                                '#prefix'                  => '<br>',
                                '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                                '#limit_validation_errors' => [],
                        ];
                        
                        $form [ 'send' ] = [
                                '#type'        => 'submit',
                                '#button_type' => 'success',
                                '#value'       => t( 'Send' ),
                                '#submit'      => [
                                        '::send',
                                ],
                                '#suffix'      => '<br><br><br>',
                        ];
                        
                        $form [ '#cache' ] [ 'max-age' ] = 0;
                        return $form;
                }
                
                /**
                 *
                 * {@inheritdoc}
                 *
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        return $form;
                }
                
                /**
                 *
                 * {@inheritdoc}
                 *
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function discard (array &$form, FormStateInterface $form_state ) {
                        $email = $form_state->getUserInput() [ 'email' ] [ 'data' ];
                        
                        $qry = \Drupal::database()->delete( 'rwPubgoldMailout' );
                        $cnd = $qry->orConditionGroup()->condition( 'id', $email [ 'id' ], '=' )->condition( 'autosend_parent', $email [ 'id' ], '=' );
                        $qry->condition( $cnd )->execute();
                        
                        $tempstore = \Drupal::service( 'user.private_tempstore' )->get( 'publisso_gold' );
                        
                        if ( null !== ( $back_url = $tempstore->get( 'back_url' ) ) ) {
                                $form_state->setRedirect( $back_url ['url'], $back_url [ 'params' ] );
                        }
                        else {
                                $form_state->setRedirect( 'publisso_gold.mailspool' );
                        }
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function send (array &$form, FormStateInterface $form_state ) {
                        $email = $form_state->getValue( 'email' ) [ 'data' ];
                        
                        $recipients = array_filter( preg_split( '/[;, ]/', $email [ 'recipient' ] ) );
                        
                        foreach ( $recipients as $recipient ) {
                                sendMail( $recipient, $email [ 'subject' ], $email [ 'body' ], [
                                        'CC'  => $email [ 'cc' ],
                                        'BCC' => $email [ 'bcc' ],
                                ], !empty ( $email [ 'attachment' ] ) ? json_decode( base64_decode( $email [ 'attachment' ] ), true ) : false
                                );
                        }
                        
                        \Drupal::database()->delete( 'rwPubgoldMailout' )->condition( 'id', $email [ 'id' ], '=' )->execute();
                        
                        // any mails depending on current mail?
                        $mails = \Drupal::database()->select( 'rwPubgoldMailout', 't' )->fields( 't', [] )->condition( 'autosend_parent', $email [ 'id' ], '=' )->execute()->fetchAll();
                        
                        foreach ( $mails as $mail ) {
                                sendMail( $mail->recipient, $mail->subject, $mail->body, json_decode( $mail->header, true ), !empty ( $mail->attachment ) ? json_decode( $mail->attachment, true ) : false );
                                \Drupal::database()->delete( 'rwPubgoldMailout' )->condition( 'id', $mail->id, '=' )->execute();
                        }
                        
                        $tempstore = \Drupal::service( 'user.private_tempstore' )->get( 'publisso_gold' );
                        
                        if(null !== ($url = $tempstore->get('urlReferer'))){
                                $tempstore->delete('urlReferer');
                                $form_state->setRedirect($url->getRouteName(), $url->getRouteParameters()->all());
                        }
                        
                        
                        // update history-item
                        \Drupal::database()->update( 'rwPubgoldWorkflowMailHistory' )->fields( [
                                                                                                       'mail_sent' => date( 'Y-m-d H:i:s' ),
                                                                                                       'mail_text' => $email [ 'body' ],
                                                                                                       'mail_to'   => $email [ 'recipient' ],
                                                                                                       'mail_cc'   => serialize( $email [ 'cc' ] ),
                                                                                                       'mail_bcc'  => serialize( $email [ 'bcc' ] ),
                                                                                               ]
                        )->condition( 'spooler_id', $email [ 'id' ], '=' )->execute()
                        ;
                        
                        return $form;
                }
        }
