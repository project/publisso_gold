<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldUseridentity.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;

/**
 * Provides a simple example form.
 */
class publisso_goldUseridentity extends FormBase {

	private $current_user = NULL;
	private $modname = 'publisso_gold';
	private $modpath;

	/**
	* {@inheritdoc}
	*/
	public function getFormId() {
		return 'publisso_golduseridentity';
	}

	/**
	* {@inheritdoc}
	*/
	public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
		
		if(!$form_state->has('currentUser'))
			$form_state->set('current_user', new \Drupal\publisso_gold\Controller\User(\Drupal::service('session')->get('user')['id']));
		
		if(!array_key_exists('orig_user', $_SESSION)){
			
			$form['title'] = [
				'#markup' => t('Login as user'),
				'#prefix' => '<h1>',
				'#suffix' => '</h1>'
			];
			
			$form['user'] = [
				'#type' => 'textfield',
				'#autocomplete_route_name' => 'autocomplete.user',
				'#autocomplete_route_parameters' => array(),
				'#required' => true
			];
			
			$form['submit'] = [
				'#type' => 'submit',
				'#value' => t('Login as user'),
				'#button_type' => 'success'
			];
		}
		else{
			
			$form['title'] = [
				'#markup' => t('Restore original user data'),
				'#prefix' => '<h1>',
				'#suffix' => '</h1>'
			];
			
			$form['submit'] = [
				'#type' => 'submit',
				'#value' => t('Restore'),
				'#button_type' => 'success',
				'#limit_error_validation' => []
			];
		}
		
		return $form;
	}

	/**
	* {@inheritdoc}
	*/
	public function validateForm(array &$form, FormStateInterface $form_state) {
		
		if(!$form_state->hasValue('user')){
			return $form;
		}
		
		list($id, $author) = explode('|', $form_state->getValue('user'));
		
		if($id){
			
			$id = base_convert(trim($id), 36, 10);
			$author = trim($author);
			
			$user = new \Drupal\publisso_gold\Controller\User($id);
			
			$username = implode(' ', array_filter([
                                $user->profile->getElement('firstname'),
                                $user->profile->getElement('lastname')
			]));
			
			if(!($username == $author)){
				
				$form_state->setErrorByName('user', (string)t('Please use only suggested values!'));
			}
		}
		else{
			$form_state->setErrorByName('user', (string)t('Please use only suggested values!'));
		}
        
		return $form;
	}
	
	/**
	* {@inheritdoc}
	*/
	public function submitForm(array &$form, FormStateInterface $form_state) {
		
		$session = \Drupal::service('session');
		
		if($form_state->hasValue('user')){
			
			list($id, $author) = explode('|', $form_state->getValue('user'));
			$id = base_convert(trim($id), 36, 10);
			$author = trim($author);
			$user = new \Drupal\publisso_gold\Controller\User($id);
			
			$_SESSION['orig_user'] = $_SESSION['user'];
			$session->set('orig_user', $session->get('user'));
			
			$_SESSION['user'] = array();
			$_SESSION['user']['name'] = $user->getElement('user');
			$_SESSION['user']['role'] = !empty($user->getRoleElement('name')) ? $user->getRoleElement('name') : $_SESSION['setup']->getValue('default.user.role.name');
			$_SESSION['user']['id'] = $user->getElement('id');
			$_SESSION['user']['weight'] = !empty($user->getRoleElement('weight')) ? $user->getRoleElement('weight') : $_SESSION['setup']->getValue('default.user.role.weight');
			$_SESSION['user']['pw_change'] = $user->getElement('pw_change_required');
			$_SESSION['user']['role_id'] = !empty($user->getRoleElement('id')) ? $user->getRoleElement('id') : $_SESSION['setup']->getValue('default.user.role.id');
			
			$_SESSION['user']['isBookEditor']       = \Drupal::service('publisso_gold.tools')->isUserBookEditor($user->getElement('id'));
                        $_SESSION['user']['isBookEiC']          = \Drupal::service('publisso_gold.tools')->isUserBookEiC($user->getElement('id'));
                        $_SESSION['user']['isBookEO']           = \Drupal::service('publisso_gold.tools')->isUserBookEO($user->getElement('id'));
                        
                        $_SESSION['user']['isJournalEditor']    = \Drupal::service('publisso_gold.tools')->isUserJournalEditor($user->getElement('id'));
                        $_SESSION['user']['isJournalEiC']       = \Drupal::service('publisso_gold.tools')->isUserJournalEiC($user->getElement('id'));
                        $_SESSION['user']['isJournalEO']        = \Drupal::service('publisso_gold.tools')->isUserJournalEO($user->getElement('id'));
                        
                        $_SESSION['user']['isConferenceEditor'] = \Drupal::service('publisso_gold.tools')->isUserConferenceEditor($user->getElement('id'));
                        $_SESSION['user']['isConferenceEiC']    = \Drupal::service('publisso_gold.tools')->isUserConferenceEiC($user->getElement('id'));
                        $_SESSION['user']['isConferenceEO']     = \Drupal::service('publisso_gold.tools')->isUserConferenceEO($user->getElement('id'));
                        
                        $_SESSION['user']['isEditor']   = $_SESSION['user']['isBookEditor'] | $_SESSION['user']['isJournalEditor'] | $_SESSION['user']['isConferenceEditor'];
                        $_SESSION['user']['isEiC']      = $_SESSION['user']['isBookEiC'] | $_SESSION['user']['isJournalEiC'] | $_SESSION['user']['isConferenceEiC'];
                        $_SESSION['user']['isEO']       = $_SESSION['user']['isBookEO'] | $_SESSION['user']['isJournalEO'] | $_SESSION['user']['isConferenceEO'];
                        
			
			$_SESSION['logged_in'] = true;
			$session->set('user', $_SESSION['user']);
			\Drupal::service('messenger')->addMessage(t('You are now logged in as: @user', ['@user' => $user->getElement('user')]));
		}
		else{
			$_SESSION['user'] = $_SESSION['orig_user'];
			$session->set('user', $session->get('orig_user'));
			$session->remove('orig_user');
			$session->remove('messages');
			unset($_SESSION['orig_user']);
			unset($_SESSION['messages']);
			\Drupal::service('messenger')->addMessage((string)$this->t('Your userdata has been restored!'));
		}
                \Drupal::cache('menu')->invalidateAll(); // for clearing the menu cache
                \Drupal::service('plugin.manager.menu.link')->rebuild(); // rebuild the menu
		$form_state->setRedirect('publisso_gold.dashboard');
		return $form;
	}
}






