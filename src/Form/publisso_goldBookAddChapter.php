<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldBookAddChapter.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;
use Drupal\Core\Url;
use \Drupal\publisso_gold\Controller\WorkflowInfoMail;

/**
 * Provides chapter create form.
 */
class publisso_goldBookAddChapter extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
	private $modaccessweights = [];
	
	const __ACCESS_DENIED__             = 'You can not access this page. You may need to log in first.';
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldBookAddChapter|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldbookaddchapter';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
            $new_workflow = null;
		$this->getModuleAccessPermissions();
	
		if(!\Drupal::service('publisso_gold.tools')->userHasAccessRight('userprofile'))
                return \Drupal::service('publisso_gold.tools')->accessDenied();
		
		$language = \Drupal::languageManager()->getCurrentLanguage();
        $this->modpath = drupal_get_path('module', $this->modname);
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
		//Link1 (uri): Authors contract
		//Link2 (uri_1): Book policy
		$uri_de    = 'http://www.publisso.de/publishing/books/book-policy/#c5185';
		$url_de 	= Url::fromUri($uri_de);
		$uri_1_de  = 'http://www.publisso.de/publishing/books/book-policy/';
		$url_1_de 	= Url::fromUri($uri_1_de);
		$text_de   = 'German';
	
		$uri_en    = 'http://www.publisso.de/en/publishing/books/book-policy/#c5189';
		$url_en 	= Url::fromUri($uri_en);
		$uri_1_en  = 'http://www.publisso.de/en/publishing/books/book-policy/';
		$url_1_en 	= Url::fromUri($uri_1_en);
		$text_en   = 'English';
		
        //expect build-info-args as array, index 0 is bk_id
        $build_info = $form_state->getBuildInfo();
        $args = $build_info['args'];
        
        if(array_key_exists(0, $args) && array_key_exists('bk_id', $args[0]))
          $form_state->set('bk_id', $args[0]['bk_id']);
        
		if(!$form_state->get('bk_id')){
            drupal_set_message(t('No book given!'), 'error');
            return array();
        }
	
        $link_book = Url::fromRoute('publisso_gold.books_book', ['bk_id' => $form_state->get('bk_id')])->toString();
        	
        if(array_key_exists(0, $args) && array_key_exists('wf_id', $args[0])){
          $form_state->set('wf_id', $args[0]['wf_id']);
          
          $comments = \Drupal::database()->select('rwPubgoldWorkflowCommentsForAuthors', 'cfa')
              ->fields('cfa', [])
              ->condition('cfa_wfid', $form_state->get('wf_id'), '=')
              ->condition('cfa_for_uid', \Drupal::service('session')->get('user')['id'], '=')
              ->execute()
              ->fetchAll();
          
          $renderComments = [];
          $c = 0;
          foreach($comments as $_){
            
            if($c === 0){
              $renderComments = [
                '#type' => 'details',
                '#open' => false,
                '#title' => (string)t('Comment(s) for author'),
                '#description' => (string)t('These comments are only available for you during current workflow. Nobody else can see these comments!'),
                'content' => []
              ];
            }
            
            $_->cfa_comment = base64_decode($_->cfa_comment);
            
            $renderComments['content'][] = [
              '#type' => 'details',
              '#open' => false,
              '#title' => (string)t('Comment').' #' . ($c + 1),
              'content' => [
                  '#type' => 'markup',
                  '#markup' => $_->cfa_comment
              ]
            ];
            
            $c++;
          }
        }
        
//load temporary stored data
		if(array_key_exists(0, $args) && array_key_exists('tmp_id', $args[0]) && !$form_state->get('tmp_id')){
          
			$form_state->set('tmp_id', $args[0]['tmp_id']);
		}
		
		if($form_state->get('tmp_id') && !$form_state->get('form_data')){
            
			$result = \Drupal::database()->select('rwPubgoldBookchapter_temp', 't')
					->fields('t', [])
					->condition('uid', $_SESSION['user']['id'])
					->condition('id', $form_state->get('tmp_id'))
					->countQuery()
					->execute();
			
			if($result > 0){
				$tmp_wf = new \Drupal\publisso_gold\Controller\RunningSubmission($form_state->get('tmp_id'));
				
				$ary = [];
				
				foreach($tmp_wf->getDataKeys() as $key){
					$ary[$key] = $tmp_wf->getDataElement($key);
				}
				
				
				$form_state->set('authors', $ary['authors']);
				
				$cnt_authors = 0;
				
				foreach($ary['authors'] as $_){
					if($_['uid']){
						$cnt_authors++;
					}
				}
				
				$form_state->set('count_authors', $cnt_authors);
				$form_state->set('num_authors', count($ary['authors']) - $cnt_authors);
				$form_state->set('form_data', $ary);
				
				$corporations = [];
				foreach($ary['corporations'] as $k => $v){
					
					if(preg_match('/^\d+$/', $k))
							$corporations[$k] = $v;
				}
				$form_state->set('corporations', $corporations);
				$form_state->set('count_corporations', count($corporations));
				
				//echo '<pre>'.print_r($form_state->get('corporations'), 1).'</pre>'; exit();
			}
		}
//-- load temporary stored data --        

        if(!$form_state->get('step'))
            $form_state->set('step', 1);
        
        $book = $this->database->select('rwPubgoldBooks', 'bk')->fields('bk', [])->condition('bk_id', $form_state->get('bk_id'), '=')->execute()->fetchAssoc();
        
		$objBook = new \Drupal\publisso_gold\Controller\Book($form_state->get('bk_id'));
		
        if($form_state->get('wf_id')){
            
            $workflow = getDashboard(\Drupal::Database(), $form_state->get('wf_id'));
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
			
			$new_workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
            
            $chapter_text = base64_decode($workflow_data->chapter_text);
            $abstract = base64_decode($workflow_data->abstract);
            
            $delta = ['workflow', 'workflow_data'];
            foreach($delta as $_) $form_state->set($_, $$_);
        }
        
        $form['comments_for_author'] = $renderComments;
        
        $form ['site_headline'] = [
            '#type' => 'markup',
            '#markup' => '<h1>'.t(($form_state->get('wf_id') ? 'Edit' : 'Add').' Chapter').'</h1>'
        ];
            
        $form['book_title'] = [
            '#type' => 'markup',
            '#markup' => '<h2>'.$book['bk_title'].'</h2>'
        ];
		
		$form['store_data_control_key'] = [
			'#type' => 'hidden',
			'#value' => 'previewchapter'
		];
		
		$form['initial_info'] = [
			'#type' => 'inline_template',
			'#template' => t('Please read the <a href="'.$link_book.'" target="_blank">manuscript guidelines</a>, the policy '.'(<a href="'.$uri_1_de.'" target="_blank">'.$text_de.'</a>, <a href="'.$uri_1_en.'" target="_blank">'.$text_en.'</a>)'.' and the author\'s contract (<a href="'.$uri_de.'" target="_blank">'.$text_de.'</a>, <a href="'.$uri_en.'" target="_blank">'.$text_en.'</a>). On the second page of the submission form, you will have to accept the author\'s contract (legally binding German version) to conclude the submission.'),
			'#suffix' => '</div><br><br>',
			'#prefix' => '<div class="font-red">',
            '#access' => $form_state->get('step') == 1 ? true : false
		];
        
// title-field      
		
		$form['desc_title'] = [
			'#type' => 'markup',
			'#markup' => t('Please add the title of your chapter here.'),
			'#suffix' => '<br><br>',
            '#access' => $form_state->get('step') == 1 ? true : false
		];
		
        $form['title'] = [
            '#type' => 'textfield',
            '#title' => t('Title'),
            '#required' => true,
            '#access' => $form_state->get('step') == 1 ? true : false
        ];
        
        if(isset($workflow_data->title))
            $form['title']['#default_value'] = $workflow_data->title;
        
        if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            $form['title']['#default_value'] = $fd['title'];
        }
        
// abstract        
		
		$form['desc_abstract'] = [
			'#type' => 'markup',
			'#markup' => t('If an abstract is required, please enter it here.'),
			'#suffix' => '<br><br>',
            '#access' => $form_state->get('step') == 1 ? true : false
		];
		/*
        $form['abstract'] = [
            '#type' => 'text_format',
            '#title' => t('Abstract'),
            '#required' => false,
            '#access' => $form_state->get('step') == 1 ? true : false
        ];
        */
		$form['abstract'] = [
            '#type' => 'textarea',
            '#title' => t('Abstract'),
            '#required' => true,
            '#access' => $form_state->get('step') == 1 ? true : false,
			'#prefix' => '<br><div>'.((string)t('To insert a table into your text, please use the button "insert template". You can edit the table afterwards.')).'</div>',
			'#rows' => 20,
			'#attributes' => [
				'class' => [
					\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
				]
			]
        ];
		
        if(isset($abstract)){
          $form['abstract']['#default_value'] = $abstract;
        }
        
        if($form_state->get('form_data')){
			
            $fd = $form_state->get('form_data');
            $form['abstract']['#default_value'] = $fd['abstract'];
        }
		
		if($objBook->getControlElement('abstract_required') == 0){
			$form['abstract']['#access'] 	= false;
			$form['abstract']['#required'] 	= false;
			
			$form['desc_abstract']['#access'] 	= false;
			$form['desc_abstract']['#required'] = false;
		}
            
// keywords
            
        $form['keywords_description'] = [
            '#type' => 'markup',
            '#markup' => t('Please add the keywords you would like to assign to your chapter in this box, separated with a semicolon.'),
            '#access' => $form_state->get('step') == 1 ? true : false,
            '#prefix' => '<br>'
        ];
        
        $form['keywords'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Keywords'),
            '#default_value' => isset($workflow_data->keywords) ? implode(',',json_decode($workflow_data->keywords)) : '',
            '#access' => $form_state->get('step') == 1 ? true : false,
            '#prefix' => '<br>'
        ];
        
        if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            $form['keywords']['#default_value'] = $fd['keywords'];
        }
        
// chapter text        
        $form['chapter_text'] = [
            '#type' => 'textarea',
            '#title' => t('Chapter Text'),
            '#required' => true,
            '#rows' => 50,
            '#prefix' => '<br><div>'.((string)t('To insert a table into your text, please use the button "insert template". You can edit the table afterwards.')).'</div>',
            '#access' => $form_state->get('step') == 1 ? true : false,
			'#attributes' => [
				'class' => [
					\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
				]
			]
        ];
        
        if(isset($chapter_text)){
          $form['chapter_text']['#default_value'] = $chapter_text;
        }
        
        if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            
            $form['chapter_text']['#default_value'] = $fd['chapter_text'];
        }

//fieldset authors        
        $form['fs_authors'] = [
            '#type' => 'fieldset',
            '#title' => (string)t('Author(s)'),
            '#prefix' => '<br>',
            'content' => [],
            '#access' => $form_state->get('step') == 1 ? true : false
        ];
        
		$form['fs_authors']['description'] = [
			'#type' => 'markup',
			'#markup' => t('Please enter one author per box. Matching entries from the database will be suggested automatically. Authors who are not yet in the database are to be added with name and affiliation as additional author. The Authors’ order can be determined on the next page.'),
			'#weight' => -1,
			'#suffix' => '<br><br>'
		];
		
		$form['fs_authors']['content']['authors'] = [
			'#tree' => true,
			'#prefix' => '<div id="authors_replace">',
			'#suffix' => '</div>',
            '#access' => $form_state->get('step') == 1 ? true : false
		];
		/*
		$form['fs_authors']['content']['description'] = [
			'#type' => 'markup',
			'#markup' => '<p>'.(string)t('Please enter Firstname and Lastname, separated with Whitespace (i.e. John Smith)').'</p>',
            '#access' => $form_state->get('step') == 1 ? true : false
		];
		*/
		if(!$form_state->get('count_authors'))
			$form_state->set('count_authors', 3);
		
		if(isset($new_workflow) && !$form_state->get('authors')){
			
			$cnt_authors = 0;
			
			if(is_array($new_workflow->getDataElement('authors'))) {
                                foreach ( $new_workflow->getDataElement( 'authors' ) as $author ) {
                                        
                                        if ( !empty( $author->uid ) ) $cnt_authors++;
                                }
                        }
			
			$ary = [];
                        if(is_array($new_workflow->getDataElement('authors'))) {
                                foreach ( $new_workflow->getDataElement( 'authors' ) as $author ) {
                                        $ary[] = (array)$author;
                                }
                        }
			
			$form_state->set('authors', $ary);
			$form_state->set('count_authors', $cnt_authors);
			$form_state->set('num_authors', count($new_workflow->getDataElement('authors')) - $cnt_authors);
		}
		
		for($i = 0; $i < $form_state->get('count_authors'); $i++){
			
			$element = [
				'#type' => 'textfield',
				'#autocomplete_route_name' => 'autocomplete.user',
				'#autocomplete_route_parameters' => array(),
                '#access' => $form_state->get('step') == 1 ? true : false
			];
			
			if($form_state->get('authors')){
				
				$fd = $form_state->get('authors');
				
				if($fd[$i]['to_delete'] === true)
					continue;
				
				if(array_key_exists($i, $fd) && array_key_exists('uid', $fd[$i]) && !empty($fd[$i]['uid'])){
					$element['#value'] = strtoupper(base_convert($fd[$i]['uid'], 10, 36)).' | '.$fd[$i]['firstname'].' '.$fd[$i]['lastname'];
					$element['#disabled'] = true;
				}
			}
			
			if($i == 0)
				$element['#title'] = (string)t('Author(s)');
			
			$form['fs_authors']['content']['authors'][$i] = $element;
		}
		
		$form['fs_authors']['content']['add_author'] = [
			'#type' => 'submit',
			'#value' => (string)t('add box'),
			'#submit' => ['::add_author'],
			'#suffix' => '<br><br>',
			'#prefix' => '<br>',
			'#ajax' => array(
				'callback' => '::add_author_callback',
				'wrapper' => 'authors_replace',
				'effect' => 'fade'
			),
            '#access' => $form_state->get('step') == 1 ? true : false
		];
                    
        $form['fs_authors']['content']['more_authors'] = [
            '#tree' => TRUE,
            '#prefix' => '<div id="authors-replace">',
            '#suffix' => '</div>',
            '#access' => $form_state->get('step') == 1 ? true : false
        ];
        
//more authors
        if(!$form_state->get('num_authors'))
           $form_state->set('num_authors', 1);
        /*
        if(isset($workflow_data->more_authors) &&
           count($workflow_data->more_authors)){
            
            $form_state->set('num_authors', count($workflow_data->more_authors));
            $_more_authors = json_decode($workflow_data->more_authors, true);
        }
        */
        $max = $form_state->get('num_authors');
        
        for ($delta = 0; $delta < $max; $delta++) {
            
            if (!isset($form['fs_authors']['content']['more_authors'][$delta])) {
                
                $element = array(
                    '#type' => 'fieldset',
                    '#title' => (string)t('Additional Author ').((string)($delta + 1)),
                    'content' => [
                        'more_author_name' => [
                            '#type' => 'textfield',
                            '#title' => (string)t('Name'),
                            '#default_value' => isset($_more_authors) ? $_more_authors[$delta]['more_author_name'] : '',
                            '#access' => $form_state->get('step') == 1 ? true : false
                        ],
                        
                        'more_author_lastname' => [
                            '#type' => 'textfield',
                            '#title' => (string)t('Lastname'),
                            '#default_value' => isset($_more_authors) ? $_more_authors[$delta]['more_author_lastname'] : '',
                            '#access' => $form_state->get('step') == 1 ? true : false
                        ],
                        
                        'more_author_affiliation' => [
                            '#type' => 'textfield',
                            '#title' => (string)t('Affiliation'),
                            '#default_value' => isset($_more_authors) ? $_more_authors[$delta]['more_author_affiliation'] : '',
                            '#access' => $form_state->get('step') == 1 ? true : false
                        ]
                    ]
                );
                
				if($form_state->has('authors')){
					
					$authors = $form_state->get('authors');
					if(array_key_exists($delta + $form_state->get('count_authors'), $authors)) $element['content']['more_author_name'	  	 ]['#default_value'] = $authors[$delta + $form_state->get('count_authors')]['firstname'	 ];
                                        if(array_key_exists($delta + $form_state->get('count_authors'), $authors)) $element['content']['more_author_lastname'	 ]['#default_value'] = $authors[$delta + $form_state->get('count_authors')]['lastname' 	 ];
                                        if(array_key_exists($delta + $form_state->get('count_authors'), $authors)) $element['content']['more_author_affiliation']['#default_value'] = $authors[$delta + $form_state->get('count_authors')]['affiliation'];
				}
				
                $form['fs_authors']['content']['more_authors'][$delta] = $element;
            }
        }
        
        //if(!$form_state->get('wf_id')){
            $form['fs_authors']['content']['addAuthor'] = [
                '#type' => 'submit',
                '#name' => 'addAuthor',
                '#value' => t('add box'),
                '#submit' => array('::addAuthor'),
                '#suffix' => '<br><br>',
                '#prefix' => '<br><br>',
              
                '#ajax' => array(
                    'callback' => '::addAuthorCallback',
                    'wrapper' => 'authors-replace',
                    'effect' => 'fade'
                ),
                
                '#access' => $form_state->get('step') == 1 ? true : false
              
          ];
        //}
// corporation      

		$form['fs_authors']['content']['corporations_description'] = [
			'#type' => 'markup',
			'#markup' => t('Here you can add corporate authors (working groups etc.). The corporate authors will appear after the personal authors in the order entered.'),
			'#suffix' => '<br><br>',
            '#access' => $form_state->get('step') == 1 ? true : false
		];

		$form['fs_authors']['content']['corporations'] = [
			'#tree' => true,
			'#prefix' => '<div id="corporations_replace">',
			'#suffix' => '</div>',
            '#access' => $form_state->get('step') == 1 ? true : false
		];
		
		if(!$form_state->get('count_corporations'))
			$form_state->set('count_corporations', 1);
		
		if(isset($new_workflow) && !$form_state->get('corporations')){
			
			$form_state->set('corporations', json_decode($new_workflow->getDataElement('corporations'), true));
			$form_state->set('count_corporations', count($form_state->get('corporations')));
		}
		
		for($i = 0; $i < $form_state->get('count_corporations'); $i++){
			
			$element = [
				'#type' => 'textfield',
                '#access' => $form_state->get('step') == 1 ? true : false
			];
			
			if($form_state->get('corporations')){
				
				$fd = $form_state->get('corporations');
				
				if($fd[$i]['to_delete'] === true)
					continue;
				
				$element['#default_value'] = $fd[$i];
			}
			
			if($i == 0)
				$element['#title'] = (string)t('Corporation(s)');
			
			$form['fs_authors']['content']['corporations'][$i] = $element;
		}
		
		$form['fs_authors']['content']['corporations']['content']['add_corporation'] = [
			'#type' => 'submit',
			'#value' => (string)t('Add Corporation'),
			'#submit' => ['::add_corporation'],
			'#suffix' => '<br><br>',
			'#prefix' => '<br>',
			'#ajax' => array(
				'callback' => '::add_corporation_callback',
				'wrapper' => 'corporations_replace',
				'effect' => 'fade'
			),
            '#access' => $form_state->get('step') == 1 ? true : false,
			'#limit_validation_errors' => []
		];
        if($form_state->get('step') == 2){
            
            if(is_array($form_state->get('authors'))){
                
                $form['authors'] = [
                    '#type' => 'table',
                    '#caption' => (string)t('Please mark a corresponding author. You can adjust the authors’ order by changing the position of each author.'),
                    '#header' => [
                        (string)t('Firstname'),
                        (string)t('Lastname'),
                        (string)t('Affiliation'),
                        (string)t('Position'),
                        (string)t('Corresponding author?'),
						(string)t('Delete item'),
                        ''
                    ]
                ];
                
                $i = 0;
                
                foreach($form_state->get('authors') as $_){
                    
                    $form['authors'][$i]['mu_firstname'] = [
                        '#type' => 'markup',
                        '#markup' => $_['firstname']
                    ];
                    
                    $form['authors'][$i]['mu_lastname'] = [
                        '#type' => 'markup',
                        '#markup' => $_['lastname']
                    ];
                    
                    $form['authors'][$i]['mu_affiliation'] = [
                        '#type' => 'markup',
                        '#markup' => $_['affiliation']
                    ];
                    
                    $form['authors'][$i]['weight'] = [
                        '#type' => 'number',
                        '#default_value' => $_['weight'],
                        '#min' => 1,
                        '#max' => 20,
                        '#step' => 1
                    ];
                    
                    $form['authors'][$i]['is_corresponding'] = [
                        '#type' => 'checkbox',
                        '#disabled' => $_['uid'] ? false: true,
                        '#default_value' => $_['is_corresponding']
                    ];
                    
					$form['authors'][$i]['to_delete'] = [
                        '#type' => 'checkbox',
                        '#default_value' => $_['to_delete']
                    ];
                    
                    $form['authors'][$i]['firstname'] = [
                        '#type' => 'hidden',
                        '#value' => $_['firstname']
                    ];
                    
                    $form['authors'][$i]['lastname'] = [
                        '#type' => 'hidden',
                        '#value' => $_['lastname']
                    ];
                    
                    $form['authors'][$i]['affiliation'] = [
                        '#type' => 'hidden',
                        '#value' => $_['affiliation']
                    ];
					
                    $form['authors'][$i]['uid'] = [
                        '#type' => 'hidden',
                        '#value' => $_['uid']
                    ];
                    
                    $i++;
                }
            }
            
// conflict of interest
            
            $form['desc_conflict_of_interest'] = [
                '#type' => 'markup',
                '#markup' => t('Here you may add if there’s any conflict of interest to declare, which might be relevant regarding the development of your chapter. If there’s anything to declare you can add a description in the appearing box.'),
                '#suffix' => '<br>'
            ];
            
            $form['conflict_of_interest'] = [
                '#type' => 'checkbox',
                '#title' => (string)t('Conflict of interest'),
                '#default_value' => isset($workflow_data) ? $workflow_data->conflict_of_interest : ''
            ];
            
			if(isset($new_workflow) && $new_workflow->getDataElement('conflict_of_interest') != 0){
				
				$form['conflict_of_interest']['#checked'] = true;
			}
			
            if($form_state->get('form_data') && array_key_exists('conflict_of_interest', $form_state->get('form_data'))){
                $fd = $form_state->get('form_data');
                $form['conflict_of_interest']['#default_value'] = $fd['conflict_of_interest'];
            }
			
			if(isset($new_workflow) && $new_workflow->getDataElement('state') == 'in revision'){
				$form['conflict_of_interest']['#disabled'] = false;
			}

// conflict of interest - text
            
            $form['conflict_of_interest_text'] = [
                '#type' => 'textfield',
                '#title' => (string)t('Description of conflict of interest'),
				'#maxlength' => 256,
                '#default_value' => isset($workflow_data) ? $workflow_data->conflict_of_interest_text : '',
                '#states' => array(
                    'visible' => array(   // action to take.
                        ':input[name="conflict_of_interest"]' => array('checked' => true),
                    ),
                    'invisible' => array(   // action to take.
                        ':input[name="conflict_of_interest"]' => array('checked' => false),
                    )
                )
            ];
            
			if(isset($new_workflow)){
				
				$form['conflict_of_interest_text']['#default_value'] = $new_workflow->getDataElement('conflict_of_interest_text');
			}
			
            if($form_state->get('form_data') && array_key_exists('conflict_of_interest_text', $form_state->get('form_data'))){
                $fd = $form_state->get('form_data');
                $form['conflict_of_interest_text']['#default_value'] = $fd['conflict_of_interest_text'];
            }
			
			if(isset($new_workflow) && $new_workflow->getDataElement('state') == 'in revision'){
				$form['conflict_of_interest_text']['#disabled'] = false;
			}

// funding
            
            $form['desc_funding'] = [
                '#type' => 'markup',
                '#markup' => t('If you received any funds please add the information here.'),
                '#suffix' => '<br>'
            ];
            
            $form['funding'] = [
                '#type' => 'checkbox',
                '#title' => t('Funds received'),
                '#default_value' => isset($workflow_data) ? $workflow_data->funding : ''
            ];
            
			if(isset($new_workflow) && $new_workflow->getDataElement('funding') != 0){
				
				$form['funding']['#checked'] = true;
			}
			
            if($form_state->get('form_data')){
                $fd = $form_state->get('form_data');
                $form['funding']['#default_value'] = $fd['funding'];
            }
            
// funding name		

            $form['funding_name'] = [
                '#type' => 'textfield',
                '#title' => (string)t('Funding name'),
				'#maxlength' => 256,
                '#default_value' => isset($workflow_data) ? $workflow_data->funding_name : '',
                '#states' => array(
                    'visible' => array(   // action to take.
                        ':input[name="funding"]' => array('checked' => true),
                    ),
                    'invisible' => array(   // action to take.
                        ':input[name="funding"]' => array('checked' => false),
                    )
                ),
            ];
            
			if(isset($new_workflow)){
				
				$form['funding_name']['#default_value'] = $new_workflow->getDataElement('funding_name');
			}
			
            if($form_state->get('form_data')){
                $fd = $form_state->get('form_data');
                $form['funding_name']['#default_value'] = $fd['funding_name'];
            }
            
// funding id
            
            $form['funding_id'] = [
                '#type' => 'textfield',
                '#title' => (string)t('Funding ID'),
                '#default_value' => isset($workflow_data) ? $workflow_data->funding_id : '',
                '#states' => array(
                    'visible' => array(   // action to take.
                        ':input[name="funding"]' => array('checked' => true),
                    ),
                    'invisible' => array(   // action to take.
                        ':input[name="funding"]' => array('checked' => false),
                    )
                ),
            ];
            
			if(isset($new_workflow)){
				
				$form['funding_id']['#default_value'] = $new_workflow->getDataElement('funding_id');
			}
			
            if($form_state->get('form_data')){
                $fd = $form_state->get('form_data');
                $form['funding_id']['#default_value'] = $fd['funding_id'];
            }

// reviewer suggestion
				
			if($objBook->getControlElement('reviewer_suggestion') == 1){
				
				$form['reviewer_suggestion'] = [
					'#type' => 'textfield',
					'#title' => t('Reviewer suggestion')
				];
			}
			
			if(isset($new_workflow)){
				
				$form['reviewer_suggestion']['#default_value'] = $new_workflow->getDataElement('reviewer_suggestion');
			}
			
            if($form_state->get('form_data')){
                $fd = $form_state->get('form_data');
                $form['reviewer_suggestion']['#default_value'] = $fd['reviewer_suggestion'];
            }
//add files to submission

                $form['add_files'] = [
                        '#type' => 'checkbox',
                        '#title' => (string)$this->t('I would like to upload (additional) files'),
                        '#access' => $form_state->get('step') == 2 ? true : false
                ];

// accept policy        
		
			$form['desc_accept_policy'] = [
				'#type' => 'markup',
				'#markup' => '<br>'.t('Please read the policy (<a href="'.$uri_1_de.'" target="_blank">'.$text_de.'</a>, <a href="'.$uri_1_en.'" target="_blank">'.$text_en.'</a>) and accept the author\'s contract (legally binding <a href="'.$uri_de.'" target="_blank">German version</a>, <a href="'.$uri_en.'" target="_blank">translated version</a>) to conclude the submission.'),
				'#access' => $form_state->get('step') == 2 ? true : false
			];
			
			$form['accept_policy'] = [
				'#type' => 'checkbox',
				'#title' => (string)t('I accept the author\'s contract</a>'),
				'#required' => true,
				'#access' => $form_state->get('step') == 2 ? true : false
			];
			
			if(isset($new_workflow) && $new_workflow->getDataElement('accept_policy') != 0){
				$form['accept_policy']['#checked'] = true;
				$form['accept_policy']['#default_value'] = $new_workflow->getDataElement('accept_policy');
			}
			
			if($form_state->get('form_data') && array_key_exists('accept_policy', $form_state->get('form_data'))){
				$fd = $form_state->get('form_data');
				$form['accept_policy']['#default_value'] = $fd['accept_policy'];
			}
        }
        
        if($form_state->get('step') > 1){
            $form['back'] = [
                '#type' => 'submit',
                '#value' => (string)t('Back'),
                '#submit' => ['::back'],
				'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
				'#limit_validation_errors' => []
            ];  
        }

		$form['save'] = [
            '#type' => 'submit',
			'#button_type' => 'primary',
            '#value' => t('Back to book'),
			'#submit' => ['::save'],
			'#access' => $form_state->get('step') == 1 ? true : false,
			'#limit_validation_errors' => []
        ];
        
		$plink = Url::fromRoute('publisso_gold.book.chapter.preview',  ['bk_id' => $form_state->get('bk_id')]);
		$plink = $plink->toString();
		
		$form['preview'] = [
			'#type' => 'button',
			'#value' => t('Preview'),
			'#attributes' => [
				'onclick' => 'return previewBookchapter(\''.$plink.'\', \'publisso-goldbookaddchapter\')'
			],
			'#button_type' => 'primary',
			'#prefix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
			'#access' => $form_state->get('step') == 1 ? true: false
		];
		
		$form['submit'] = [
            '#type' => 'submit',
            '#value' => t($form_state->get('step') == 1 ? (string)t('Continue submission') : ($form_state->get('wf_id') ? 'Finish revision' : 'Submit')),
			'#button_type' => 'success',
			'#prefix' => '&nbsp;&nbsp;&nbsp;'
        ];
		
		if($form_state->get('step') > 1 && !$form_state->get('wf_id')){
			$form['submit']['#attributes'] = 
				array('onclick' => 'if(!confirm("'.t('When you conclude the submission, it will enter the review process. You won’t be able to edit the submission until a possible revision.\n\nOK: Finish submission\nCancel: Continue submission').'")){return false;}');
                        
                        $form['submit']['#states'] = array(
                                'visible' =>    array(   // action to take.
                                                        ':input[name="add_files"]' => array('checked' => false),
                                                ),
                                'invisible' =>  array(   // action to take.
                                                        ':input[name="add_files"]' => array('checked' => true),
                                                )
                        );
                        
                        $form['continue_files'] = [
                                '#type' => 'submit',
                                '#button_type' => 'primary',
                                '#value' => (string)$this->t('Conintue submission'),
                                '#submit' => ['::continueFiles'],
                                '#states' => array(
                                        'visible' =>    array(   // action to take.
                                                                ':input[name="add_files"]' => array('checked' => true),
                                                        ),
                                        'invisible' =>  array(   // action to take.
                                                                ':input[name="add_files"]' => array('checked' => false),
                                                        )
                                )
                        ];
		}
		
		$form['tmp_id'] = [
			'#type' => 'hidden',
			'#default_value' => $form_state->get('tmp_id')
		];
		
		$form['bk_id'] = [
			'#type' => 'hidden',
			'#value' => $form_state->get('bk_id')
		];
		
		$form['ctrl_action'] = [
			'#type' => 'hidden',
			'#value' => 'autosave'
		];
		
		$form['ctrl_type'] = [
			'#type' => 'hidden',
			'#value' => 'bookchapter'
		];
		
		$form['ctrl_step'] = [
			'#type' => 'hidden',
			'#value' => $form_state->get('step')
		];
		
		$form['ctrl_state'] = [
			'#type' => 'hidden',
			'#value' => $new_workflow ? $new_workflow->getDataElement('state') : 'in submission'
		];
		
		$form['#attached'] = [
			'library' => [
				'publisso_gold/default'
			]
		];
		
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        foreach($form_state->getValue('authors') as $_){
            
            list($id, $author) = explode('|', $_);
            
            if($id){
                
                $id = base_convert(trim($id), 36, 10);
                $author = trim($author);
                
                $user = new \Drupal\publisso_gold\Controller\User($id);
                
                if(!($user->profile->getElement('firstname').' '.$user->profile->getElement('lastname') == $author)){
                    
                    $form_state->setErrorByName('authors', (string)t('Please use only suggested values!'));
                }
            }
        }
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function continueFiles(array &$form, FormStateInterface $form_state){
                echo 'Continue with file upload...';
                exit();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function back(array &$form, FormStateInterface $form_state){
        
		$authors = $form_state->get('authors');
		
		$i = 0;
		
		foreach($form_state->getValue('authors') as $k => $v){
			
			$authors[$i]['is_corresponding'] = $v['is_corresponding'] ? true : false;
			$authors[$i]['to_delete'       ] = $v['to_delete'       ] ? true : false;
			$i++;
		}
		
		$new_authors = [];
		$count_authors = $num_authors = 0;
		
		foreach($authors as $_){
			
			if(!$_['to_delete']){
				$new_authors[] = $_;
				!empty($_['uid']) ? $count_authors++ : $num_authors++;
			}
		}
        
		$form_state->set('num_authors', $num_authors);
		$form_state->set('count_authors', $count_authors);
		$form_state->set('authors', $new_authors);
        
        $form_state->set('step', $form_state->get('step') - 1);
        $form_state->setRebuild();
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
		if($form_state->get('step') == 1){
            
            $form_state->set('step', $form_state->get('step') + 1);
			
            $authors = [];
            $weight = 0;
            
            foreach($form_state->getValue('authors') as $_){
                
                list($id, $author) = explode('|', $_);
                $id = base_convert(trim($id), 36, 10);
                
                if($id){
                    
                    $author = new \Drupal\publisso_gold\Controller\User($id);
                    
                    $authors[] = [
                        'firstname'         => $author->profile->getElement('firstname'),
                        'lastname'          => $author->profile->getElement('lastname'),
                        'affiliation'       => $author->profile->getElement('institute'),
                        'uid'               => $author->getElement('id'),
                        'is_corresponding'  => false,
                        'weight'            => ++$weight
                    ];
                }
            }
            
            foreach($form_state->getValue('more_authors') as $_){
                
                if(!(empty($_['content']['more_author_name']) || empty($_['content']['more_author_lastname']) || empty($_['content']['more_author_affiliation']))){
                
                    $authors[] = [
                        'firstname'         => $_['content']['more_author_name'],
                        'lastname'          => $_['content']['more_author_lastname'],
                        'affiliation'       => $_['content']['more_author_affiliation'],
                        'uid'               => null,
                        'is_corresponding'  => false,
                        'weight'            => ++$weight
                    ];
                }
            }
            
			$weight = 0;
			/*
			foreach($form_state->getValue('corporations') as $_){
                
				if(!empty($_)){
					$corporations[] = $_;
				}
            }
			*/
            $form_state->set('form_data', $form_state->getValues());
            $form_state->set('authors', $authors);
			$form_state->set('corporations', $form_state->getValue('corporations'));
			
            $form_state->setRebuild();
            return $form;
        }
        elseif($form_state->get('step') == 2){
            
            //get workflow or create a new one
            
            $new_wf = false;
            
            if(!$form_state->get('wf_id')){
                $workflow = new \Drupal\publisso_gold\Controller\Workflow();
                $workflow->create($_SESSION['user']['id']);
				$workflow->setDataElement('bk_id', $form_state->get('bk_id'));
				$workflow->setDataElement('type', 'bookchapter');
				$workflow->reload();
				
                $new_wf = true;
            }
            else{
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
            }
            
            //get the book
            $book = new \Drupal\publisso_gold\Controller\Book($form_state->get('bk_id'));
            
// -- Store Formdata            
			
            //title
            $workflow->setDataElement('title', $form_state->getValue('title'), false);

            //abstract
            $element                = 'abstract';
            $$element               = $form_state->getValue($element);
            $$element               = normalizeInlineImageLinks($$element);
            $workflow->setDataElement($element, base64_encode($$element), false);
            
            //chapter_text
            $element                = 'chapter_text';
            $$element               = $form_state->getValue($element);
            $$element               = normalizeInlineImageLinks($$element);
            $workflow->setDataElement($element, base64_encode($$element), false);
            
            //authors
            $element                = 'authors';
            $$element               = $form_state->getValue($element);
            $_ = [];
            foreach($$element as $k => $v)
                if($v['to_delete'] == 0) $_[] = $v;
            $workflow->setDataElement($element, $_, false);
            
			//corporations
            $element                = 'corporations';
            $$element               = $form_state->getValue($element);
			unset($corporations['content']);
            $workflow->setDataElement($element, json_encode($$element), false);
			
            //keywords
            $element                = 'keywords';
            $$element               = $form_state->getValue($element);
            $$element               = preg_split('/;/', $$element);
            $$element               = array_map("trim", $$element);
            $workflow->setDataElement($element, json_encode($$element), false);
            
            //all other elements
            $elements = [
                'accept_policy',
                'conflict_of_interest',
                'conflict_of_interest_text',
                'funding_name',
                'funding_id',
                'funding',
                'reviewer_suggestion'
            ];
            
            foreach($elements as $element)
                $workflow->setDataElement($element, $form_state->getValue($element), false);
            
            
            if($new_wf){
                
                //state
                $workflow->setDataElement('state', 'submission finished', false);
                
                //type
                $workflow->setDataElement( 'type', 'bookchapter', false);
                
                //bk_id
                $workflow->setDataElement('bk_id', $book->getElement('id'), false);
            }

// -- Store Form data --            

// -- Assign Submission
            
            //read Editors-in-Chief
            $eic = $book->readEditorsInChief();
            
            //read Editorial Office
            $eo = $book->readEditorialOffice();
            
			$assigned_users = [];
			
            if($new_wf){
				
                if(count($eic) == 1){ //only one Editor-in-Chief, bypass editorial office
                    $submission_assign = 'u:'.$eic[0]->getElement('id');
                    $workflow->setDataElement('state', 'assigned to eic', false);
                    $workflow->setElement('assigned_to_eic', $eic[0]->getElement('id'));
					$assigned_users[] = $eic[0];
                }
                elseif(count($eo) > 0){ //submit to editorial office
                    
                    $submission_assign = '';
                    
                    foreach($eo as $_){
                        $submission_assign .= (!empty($submission_assign) ? ',' : '') . 'u:'.$_->getElement('id');
						
						if(!in_array($_->getElement('id'), $assigned_users))
							$assigned_users[] = $_->getElement('id');
                    }
                }
                else{ //submission to publisso
                    $submission_assign = 'r:2';
                }
                
                $workflow->setElement('assigned_to', $submission_assign);
                
                //save editorial office
                if(count($eo)){
                    $eo_str = '';
                    
                    foreach($eo as $_){
                        $eo_str .= (!empty($eo_str) ? ',' : '') . $_->getElement('id');
                    }
                    
                    $workflow->setElement('assigned_to_eo', $eo_str);
                }
				
				//infomail
        $mail = new WorkflowInfoMail($workflow, 'submission finished');
        if($mail) $mail->send();
				//sendWorkflowInfoMail($workflow, 'submission finished');
            }
            else{
                //Revision finished
                $workflow->setState('revision finished');
                
                //unlock workflow
                $workflow->unlock();
            }
            
			foreach($assigned_users as $_){
				$_ = new \Drupal\publisso_gold\Controller\User($_);
				//send Reg-Mail to user if not yet be done
				if($_->getElement('reg_notification_sent') == 0){
					
					$link = 'http://'.$_SERVER['SERVER_NAME'];
					$password['clear'] = genTempPassword();
					$password['encrypted'] = '*'.strtoupper(sha1(sha1($user['password_clear'], true)));
					
					$_->setElement('password', $password['encrypted']);
					
					foreach(['registration_password', 'registration_info'] as $name){
					
						$vars = [
							'::firstname::' 	=> $_->profile->getElement('firstname'),
							'::lastname::' 		=> $_->profile->getElement('lastname'),
							'::link::' 			=> $link,
							'::email::'			=> $_->profile->getElement('email'),
							'::user::' 			=> $_->getElement('user'),
							'::login_link::' 	=> $link.'/publisso_gold/login',
							'::password::' 		=> $password['clear'],
						];
						
						sendRegistrationInfoMail($name, $vars);
					}
					
					$_->setElement('reg_notification_sent', 1);
				}
				// - Reg-Mail --
			}
			
            //save workflow
            $workflow->setElement('modified', date('Y-m-d H:i:s'));
            $workflow->save();
            
// -- Assign Submission --
            
            //delete temp
            \Drupal::database()->delete('rwPubgoldBookchapter_temp')->condition('id', $form_state->get('form_data')['tmp_id'], '=')->execute();
            
            drupal_set_message(t('Chapter successfully saved!'));
            
            if($form_state->getValue('add_files') == 1){
                
                $workflow->getLock($_SESSION['user']['id']);
                $form_state->setRedirect('publisso_gold.book.workflow.files.add', ['wf_id' => $workflow->getElement('id')]);
            }
            else{
                $form_state->setRedirect('publisso_gold.dashboard');
            }
            
            return $form;
        }
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthor(array &$form, FormStateInterface $form_state) {
        
        $form_state->set('num_authors', $form_state->get('num_authors') + 1);
        
        $form['fs_authors']['content']['more_authors'][] = [
            '#type' => 'fieldset',
            '#title' => (string)t('Author').' #'.$form_state->get('num_authors'),
            'content' => [
                'more_author_name' => [
                    '#type' => 'textfield',
                    '#title' => (string)t('Name')
                ],
                
                'more_author_lastname' => [
                    '#type' => 'textfield',
                    '#title' => (string)t('Lastname')
                ],
                
                'more_author_affiliation' => [
                    '#type' => 'textfield',
                    '#title' => (string)t('Affiliation')
                ]
            ]
        ];
        
        $form_state->setRebuild();
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function add_corporation(array &$form, FormStateInterface $form_state) {
        
        $form_state->set('count_corporations', $form_state->get('count_corporations') + 1);
        
        $element = [
			'#type' => 'textfield'
        ];
        
		$form['fs_authors']['content']['corporations'][$form_state->get('count_corporations')] = $element;
		
        $form_state->setRebuild();
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function add_corporation_callback(array &$form, FormStateInterface $form_state){
		return $form['fs_authors']['content']['corporations'];
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function add_author(array &$form, FormStateInterface $form_state) {
        
        $form_state->set('count_authors', $form_state->get('count_authors') + 1);
        
        $element = [
			'#description' => (string)t('Please enter Firstname and Lastname, separated with Whitespace (i.e. John Smith)'),
			'#type' => 'textfield',
			'#autocomplete_route_name' => 'autocomplete.user',
			'#autocomplete_route_parameters' => array()
        ];
        
		$form['fs_authors']['content']['authors'][$form_state->get('count_authors')] = $element;
		
        $form_state->setRebuild();
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function add_author_callback(array &$form, FormStateInterface $form_state){
		return $form['fs_authors']['content']['authors']; 
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function add_coauthor(array &$form, FormStateInterface $form_state) {
        
        $form_state->set('count_coauthors', $form_state->get('count_coauthors') + 1);
        
        $element = [
			'#description' => (string)t('Please enter Firstname and Lastname, separated with Whitespace (i.e. John Smith)'),
			'#type' => 'textfield',
			'#autocomplete_route_name' => 'autocomplete.user',
			'#autocomplete_route_parameters' => array()
        ];
        
		$form['fs_coauthors']['content']['coauthors'][$form_state->get('count_coauthors')] = $element;
		
        $form_state->setRebuild();
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function add_coauthor_callback(array &$form, FormStateInterface $form_state){
		return $form['fs_coauthors']['content']['coauthors'];
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorCallback(array &$form, FormStateInterface $form_state) {
        return $form['fs_authors']['content']['more_authors'];
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function save(array &$form, FormStateInterface $form_state){
		
		$dbo = \Drupal::database();
        $data = [];
        
        //all other elements
        $elements = [
            'accept_policy',
            'conflict_of_interest',
            'conflict_of_interest_text',
            'funding_name',
            'funding_id',
            'funding',
            'keywords'
        ];
        
        foreach($elements as $element)
            $data = $form_state->getValue($element);
		
		if($form_state->getValue('tmp_id') != "" && $form_state->get('tmp_id') == ""){ //autosave was faster
			$form_state->set('tmp_id', $form_state->getValue('tmp_id'));
		}
		
		if(!$form_state->get('tmp_id')){ //create new dataset
			
			$tmp_wf = new \Drupal\publisso_gold\Controller\RunningSubmission();
			$tmp_wf->create($_SESSION['user']['id']);
			$tmp_wf->setElement('bk_id', $form_state->get('bk_id'));
			$form_state->set('tmp_id', $tmp_wf->getElement('id'));
		}
		else{ //load existing dataset and update/insert data
			$tmp_wf = new \Drupal\publisso_gold\Controller\RunningSubmission($form_state->get('tmp_id'));
		}
		
		$authors = [];
		$weight = 0;
		
		foreach($form_state->getValue('authors') as $_){
			
			list($id, $author) = explode('|', $_);
			$id = base_convert(trim($id), 36, 10);
			
			if($id){
				
				$author = new \Drupal\publisso_gold\Controller\User($id);
				
				$authors[] = [
					'firstname'         => $author->profile->getElement('firstname'),
					'lastname'          => $author->profile->getElement('lastname'),
					'affiliation'       => $author->profile->getElement('institute'),
					'uid'               => $author->getElement('id'),
					'is_corresponding'  => false,
					'weight'            => ++$weight
				];
			}
		}
		
		foreach($form_state->getValue('more_authors') as $_){
			
			if(!(empty($_['content']['more_author_name']) || empty($_['content']['more_author_lastname']) || empty($_['content']['more_author_affiliation']))){
			
				$authors[] = [
					'firstname'         => $_['content']['more_author_name'],
					'lastname'          => $_['content']['more_author_lastname'],
					'affiliation'       => $_['content']['more_author_affiliation'],
					'uid'               => null,
					'is_corresponding'  => false,
					'weight'            => ++$weight
				];
			}
		}
		$data['authors'] = $authors;
		
		$corporations = [];
		
		foreach($form_state->getValue('corporations') as $k => $v){
			if(preg_match('/^\d+$/', $k) && !empty($v)) $corporations[] = $v;
		}
		
		$data['corporations'] = $corporations;
		
		foreach($data as $k => $v){
			$tmp_wf->setDataElement($k, $v, true, true);
        }
        
		$form_state->setRedirect('publisso_gold.books_book', ['bk_id' => $form_state->get('bk_id')]);
		return $form;
	}
	
	private function getModuleAccessPermissions(){
        
        $sql = "SELECT * FROM {rwPubgoldModuleaccess}";
        $result = db_query($sql);
        $result = $result->fetchAll();
        
        foreach($result as $res){
            $this->modaccessweights[$res->mod_name] = $res->mod_minaccessweight;
        }
    }
        
        /**
         * @param $modname
         * @return bool
         */
        private function userHasAccessRight($modname){
        
        if(!array_key_exists($modname, $this->modaccessweights)){
            return false;
        }
        
        return $this->modaccessweights[$modname] <= $_SESSION['user']['weight'];
    }
}
