<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldLogout.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class publisso_goldLogout
 * @package Drupal\publisso_gold\Form
 */
class publisso_goldLogout extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'publisso_goldlogout';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Logout'),
    );
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
          $lp = \Drupal::service('publisso_gold.login_provider');
          $lp->logout();
  }    

}
