<?php
        
        
        namespace Drupal\publisso_gold\Form\User;
        
        
        use Drupal\Core\Ajax\AjaxResponse;
        use Drupal\Core\Ajax\CloseModalDialogCommand;
        use Drupal\Core\Ajax\InvokeCommand;
        use Drupal\Core\Ajax\RedirectCommand;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class AddAffiliations
         * @package Drupal\publisso_gold\Form\User
         */
        class AddAffiliations extends FormBase {
        
                /**
                 * @inheritDoc
                 */
                public function getFormId() {
                        return 'add_affiliations';
                }
        
                /**
                 * @inheritDoc
                 */
                public function buildForm(array $form, FormStateInterface $form_state) {
        
                        $user = Publisso::currentUser();
                        $affiliations = $user->profile()->getAdditionalAffiliations();
                        
                        if(!$form_state->has('cntAffiliations')) {
                                $form_state->set('cntAffiliations', count($affiliations));
                        }
        
                        $form['wrapper-affiliations'] = [
                                '#type' => 'fieldset',
                                '#title' => (string)$this->t('Affiliations'),
                                '#prefix' => '<div id="wrapper-affiliations">',
                                '#suffix' => '</div>',
                                'affiliations' => ['#tree' => true],
                                'actions' => [
                                        '#type' => 'container',
                                        'del' => [],
                                        'add' => [
                                                '#type' => 'submit',
                                                '#value' => (string)$this->t('Add affiliation'),
                                                '#submit' => ['::addAffiliation'],
                                                '#limit_validation_errors' => [],
                                                '#ajax' => [
                                                        'callback' => '::addAffiliationCallback',
                                                        'wrapper' => 'wrapper-affiliations',
                                                        'progress' => [
                                                                'type' => 'none'
                                                        ]
                                                ]
                                        ]
                                ]
                        ];
                        
                        if($form_state->get('cntAffiliations') > 0){
                                
                                $form['wrapper-affiliations']['actions']['del'] = [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Delete last affiliation'),
                                        '#submit' => ['::delAffiliation'],
                                        '#limit_validation_errors' => [],
                                        '#ajax' => [
                                                'callback' => '::delAffiliationCallback',
                                                'wrapper' => 'wrapper-affiliations',
                                                'progress' => [
                                                        'type' => 'none'
                                                ]
                                        ],
                                        '#suffix' => '&nbsp;&nbsp;&nbsp;'
                                ];
                                
                                for($i = 0; $i < $form_state->get('cntAffiliations'); $i++){
                                        
                                        $form['wrapper-affiliations']['affiliations'][$i] = $this->getAffiliationElement($i, $affiliations);
                                }
                        }
                        
                        $form['actions'] = [
                                '#type' => 'actions',
                                'cancel' => [
                                        '#type' => 'button',
                                        '#value' => (string)$this->t('Cancel'),
                                        '#button_type' => 'danger',
                                        '#ajax' => [
                                                'callback' => '::cancel',
                                                'progress' => [
                                                        'type' => 'none'
                                                ]
                                        ]
                                ],
                                'submit' => [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Submit'),
                                        '#ajax' => [
                                                'callback' => '::submitForm',
                                                'progress' => [
                                                        'type' => 'none'
                                                ]
                                        ]
                                ]
                        ];
                        
                        return $form;
                }
        
                /**
                 * @inheritDoc
                 */
                public function submitForm(array &$form, FormStateInterface $form_state) {
                        
                        $user = Publisso::currentUser();
                        $user->profile()->setAdditionalAffiliations($form_state->getValue('affiliations') ?? []);
                        $response = new AjaxResponse();
                        $response->addCommand(new CloseModalDialogCommand());
                        
                        $currentUrl = Url::fromRoute('publisso_gold.userprofile')->toString();
                        $response->addCommand(new RedirectCommand($currentUrl));
                        
                        return $response;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return AjaxResponse
                 */
                public function cancel(array &$form, FormStateInterface $form_state) {
                
                        $response = new AjaxResponse();
                        $response->addCommand(new CloseModalDialogCommand());
                        return $response;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addAffiliation(array &$form, FormStateInterface $form_state){
                        $form_state->set('cntAffiliations', $form_state->get('cntAffiliations') + 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addAffiliationCallback(array &$form, FormStateInterface $form_state){
                        return $form['wrapper-affiliations'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function delAffiliation(array &$form, FormStateInterface $form_state){
                        $form_state->set('cntAffiliations', $form_state->get('cntAffiliations') - 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function delAffiliationCallback(array &$form, FormStateInterface $form_state){
                        return $form['wrapper-affiliations'];
                }
        
                /**
                 * @param int $index
                 * @param array $affiliations
                 * @return array
                 */
                private function getAffiliationElement(int $index, array &$affiliations){
                        
                        $no = $index + 1;
                        
                        return[
                                '#type' => 'fieldset',
                                'no' => [
                                        '#markup' => "#$no",
                                        '#prefix' => '<div class="col-sm-1"><br>',
                                        '#suffix' => '</div>'
                                ],
                                'institute' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Institute'),
                                        '#prefix' => '<div class="col-sm-3">',
                                        '#suffix' => '</div>',
                                        '#required' => true,
                                        '#default_value' => $affiliations[$index]['institute'] ?? null
                                ],
                                'department' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Department'),
                                        '#prefix' => '<div class="col-sm-3">',
                                        '#suffix' => '</div>',
                                        '#required' => true,
                                        '#default_value' => $affiliations[$index]['department'] ?? null
                                ],
                                'city' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('City'),
                                        '#prefix' => '<div class="col-sm-2">',
                                        '#suffix' => '</div>',
                                        '#required' => true,
                                        '#default_value' => $affiliations[$index]['city'] ?? null
                                ],
                                'country' => [
                                        '#type' => 'select',
                                        '#title' => (string)$this->t('Country'),
                                        '#options' => Publisso::tools()->getCountrylist(),
                                        '#prefix' => '<div class="col-sm-3">',
                                        '#suffix' => '</div>',
                                        '#required' => true,
                                        '#default_value' => $affiliations[$index]['country'] ?? null
                                ]
                        ];
                }
        }