<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\editReviewsheet.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Database\Connection;
        use Drupal\file\Element;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Symfony\Component\HttpFoundation\File;
        use Drupal\publisso_gold\Controller\Reviewsheet;
        
        /**
         * Provides a simple example form.
         */
        class editReviewsheet extends FormBase {
                
                private $modname = 'publisso_gold';
                private $database;
                private $modpath;
                private $texts;
                
                public function __construct () {
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'editReviewsheet';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state, $args = [] ) {
                        
                        $session = \Drupal::service( 'session' );
                        $tools = \Drupal::service( 'publisso_gold.tools' );
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        
                        /**
                         * Load the reviewsheet. If no ID delivered or sheet could not be loaded,
                         * throw error-drupal_set_message
                         */
                        if ( empty( $args[ 'id' ] ) && !$form_state->has( 'id' ) ) {
                                return $tools->accessDenied( (string)t( $texts->get( 'reviewsheet.edit.no_id', 'fc' ) ) );
                        }
                        
                        $form_state->set( 'id', $args[ 'id' ] );
                        
                        if ( !$form_state->has( 'copy' ) )
                                $form_state->set( 'copy', array_key_exists( 'copy', $args ) ? !!$args[ 'copy' ] : false );
                        
                        $rp = new Reviewsheet( $form_state->get( 'id' ) );
                        
                        if ( $rp->getElement( 'id' ) === null ) {
                                return $tools->accessDenied( (string)t( $texts->get( 'reviewsheet.edit.not_found', 'fc' ), [ '@nr' => $form_state->get( 'id' ) ] ) );
                        }
                        
                        $sheet = new \DOMDocument();
                        $sheet->loadXML( $rp->getElement( 'sheet' ) );
                        $xpath = new \DOMXpath( $sheet );
                        
                        /**
                         * Begin formular
                         */
                        if ( !$form_state->has( 'items' ) ) $form_state->set( 'items', [] );
                        
                        /**
                         * Load all items from reviewsheet
                         */
                        if ( !$form_state->has( 'sheet_loaded' ) || $form_state->get( 'sheet_loaded' ) === false ) {
                                
                                $items = $form_state->get( 'items' );
                                $countHeadings = $countSubheadings = $countItems = 0;
                                foreach ( $xpath->evaluate( '/reviewsheet//elements/element' ) as $node ) {
                                        
                                        $counter = 'count' . ucfirst( $node->getAttribute( 'type' ) ) . 's';
                                        $$counter++;
                                        $id = $node->getAttribute( 'type' ) . '#' . $$counter;
                                        
                                        switch ( $node->getAttribute( 'type' ) ) {
                                                
                                                case 'heading':
                                                        
                                                        $items[ $id ] = [
                                                                '#type'       => 'container',
                                                                '#tree'       => true,
                                                                '#id'         => "item-wrapper-$id",
                                                                '#attributes' => [
                                                                        'style' => 'padding: 4px; border: 1px solid silver; background-color: lightgray;',
                                                                ],
                                                                'text'        => [
                                                                        '#type'          => 'textfield',
                                                                        '#title'         => (string)t( $texts->get( 'reviewsheet.add.form.heading.title', 'fc' ), [ '@nr' => $countHeadings ] ),
                                                                        '#placeholder'   => (string)t( $texts->get( 'reviewsheet.add.form.heading.placeholder', 'fc' ) ),
                                                                        '#required'      => true,
                                                                        '#default_value' => $xpath->evaluate( 'text', $node )[ 0 ]->nodeValue,
                                                                        '#maxlength'     => 500,
                                                                ],
                                                                'meta'        => [
                                                                        '#type'    => 'container',
                                                                        'position' => [
                                                                                '#title'         => (string)t( $texts->get( 'reviewsheet.add.form.heading.position', 'fc' ) ),
                                                                                '#type'          => 'number',
                                                                                '#min'           => 0,
                                                                                '#size'          => 5,
                                                                                '#prefix'        => '<div class="form-wrapper-left">',
                                                                                '#suffix'        => '</div>',
                                                                                '#default_value' => $node->getAttribute( 'weight' ),
                                                                        ],
                                                                        'delete'   => [
                                                                                '#type'                    => 'submit',
                                                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.heading.btn_delete', 'fc' ), [ '@nr' => $countHeadings ] ),
                                                                                '#prefix'                  => '<div class="form-wrapper-right">',
                                                                                '#suffix'                  => '</div><br clear="all">',
                                                                                '#limit_validation_errors' => [],
                                                                                '#attributes'              => [
                                                                                        'class' => [
                                                                                                'btn-danger',
                                                                                        ],
                                                                                        'item'  => $id,
                                                                                ],
                                                                                '#submit'                  => [
                                                                                        '::deleteHeading',
                                                                                ],
                                                                                '#ajax'                    => [
                                                                                        'wrapper'  => "item-wrapper",
                                                                                        'callback' => '::deleteHeadingCallback',
                                                                                        'effect'   => 'fade',
                                                                                        'progress' => [
                                                                                                'throbber' => 'none',
                                                                                        ],
                                                                                ],
                                                                                'item'                     => $id,
                                                                        ],
                                                                ],
                                                                'type'        => [
                                                                        '#type'  => 'hidden',
                                                                        '#value' => 'heading',
                                                                ],
                                                        ];
                                                        break;
                                                
                                                case 'subheading':
                                                        
                                                        $items[ $id ] = [
                                                                '#type'       => 'container',
                                                                '#tree'       => true,
                                                                '#id'         => "item-wrapper-$id",
                                                                '#attributes' => [
                                                                        'style' => 'padding: 4px; border: 1px solid silver; background-color: lightgray;',
                                                                ],
                                                                'text'        => [
                                                                        '#type'          => 'textfield',
                                                                        '#title'         => (string)t( $texts->get( 'reviewsheet.add.form.subheading.title', 'fc' ), [ '@nr' => $countSubheadings ] ),
                                                                        '#placeholder'   => (string)t( $texts->get( 'reviewsheet.add.form.subheading.placeholder', 'fc' ) ),
                                                                        '#required'      => true,
                                                                        '#default_value' => $xpath->evaluate( 'text', $node )[ 0 ]->nodeValue,
                                                                        '#maxlength'     => 500,
                                                                ],
                                                                'meta'        => [
                                                                        '#type'    => 'container',
                                                                        'position' => [
                                                                                '#title'         => (string)t( $texts->get( 'reviewsheet.add.form.subheading.position', 'fc' ) ),
                                                                                '#type'          => 'number',
                                                                                '#min'           => 0,
                                                                                '#size'          => 5,
                                                                                '#prefix'        => '<div class="form-wrapper-left">',
                                                                                '#suffix'        => '</div>',
                                                                                '#default_value' => $node->getAttribute( 'weight' ),
                                                                        ],
                                                                        'delete'   => [
                                                                                '#type'                    => 'submit',
                                                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.subheading.btn_delete', 'fc' ), [ '@nr' => $countSubheadings ] ),
                                                                                '#prefix'                  => '<div class="form-wrapper-right">',
                                                                                '#suffix'                  => '</div><br clear="all">',
                                                                                '#limit_validation_errors' => [],
                                                                                '#attributes'              => [
                                                                                        'class' => [
                                                                                                'btn-danger',
                                                                                        ],
                                                                                        'item'  => $id,
                                                                                ],
                                                                                '#submit'                  => [
                                                                                        '::deleteSubheading',
                                                                                ],
                                                                                '#ajax'                    => [
                                                                                        'wrapper'  => "item-wrapper",
                                                                                        'callback' => '::deleteSubheadingCallback',
                                                                                        'effect'   => 'fade',
                                                                                        'progress' => [
                                                                                                'throbber' => 'none',
                                                                                        ],
                                                                                ],
                                                                                'item'                     => $id,
                                                                        ],
                                                                ],
                                                                'type'        => [
                                                                        '#type'  => 'hidden',
                                                                        '#value' => 'subheading',
                                                                ],
                                                        ];
                                                        break;
                                                
                                                case 'item':
                                                        $items[ $id ] = [
                                                                '#type'       => 'container',
                                                                '#tree'       => true,
                                                                '#id'         => "item-wrapper-$id",
                                                                '#suffix'     => '<br clear="all">',
                                                                '#attributes' => [
                                                                        'style' => 'padding: 4px; border: 1px solid silver; background-color: lightgray;',
                                                                ],
                                                                'headline'    => [
                                                                        '#markup' => (string)t( $texts->get( 'reviewsheet.add.form.item.headline', 'fc' ), [ '@nr' => $countItems ] ),
                                                                        '#prefix' => '<h3>',
                                                                        '#suffix' => '</h3>',
                                                                ],
                                                                'text'        => [
                                                                        '#type'          => 'textfield',
                                                                        '#title'         => (string)t( $texts->get( 'reviewsheet.add.form.item.title', 'fc' ) ),
                                                                        '#placeholder'   => (string)t( $texts->get( 'reviewsheet.add.form.item.placeholder', 'fc' ) ),
                                                                        '#required'      => true,
                                                                        '#default_value' => $xpath->evaluate( 'text', $node )[ 0 ]->nodeValue,
                                                                        '#maxlength'     => 500,
                                                                ],
                                                                'meta'        => [
                                                                        '#type'    => 'container',
                                                                        'position' => [
                                                                                '#title'         => (string)t( $texts->get( 'reviewsheet.add.form.item.position', 'fc' ) ),
                                                                                '#type'          => 'number',
                                                                                '#min'           => 0,
                                                                                '#size'          => 5,
                                                                                '#prefix'        => '<div class="form-wrapper-left">',
                                                                                '#suffix'        => '</div>',
                                                                                '#default_value' => $node->getAttribute( 'weight' ),
                                                                        ],
                                                                        'delete'   => [
                                                                                '#type'                    => 'submit',
                                                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.item.btn_delete', 'fc' ), [ '@nr' => $countItems ] ),
                                                                                '#prefix'                  => '<div class="form-wrapper-right">',
                                                                                '#suffix'                  => '</div><br clear="all">',
                                                                                '#limit_validation_errors' => [],
                                                                                '#attributes'              => [
                                                                                        'class' => [
                                                                                                'btn-danger',
                                                                                        ],
                                                                                        'item'  => $id,
                                                                                ],
                                                                                '#submit'                  => [
                                                                                        '::deleteItem',
                                                                                ],
                                                                                '#ajax'                    => [
                                                                                        'wrapper'  => "item-wrapper",
                                                                                        'callback' => '::deleteItemCallback',
                                                                                        'effect'   => 'fade',
                                                                                        'progress' => [
                                                                                                'throbber' => 'none',
                                                                                        ],
                                                                                ],
                                                                                'item'                     => $id,
                                                                        ],
                                                                        '#suffix'  => '<hr class="form-add-reviewsheet-item">',
                                                                ],
                                                                'type'        => [
                                                                        '#type'  => 'hidden',
                                                                        '#value' => 'item',
                                                                ],
                                                                'subitems'    => [
                                                                        '#type'   => 'container',
                                                                        'content' => [],
                                                                ],
                                                                'action'      => [
                                                                        'id'                 => [
                                                                                '#type'  => 'hidden',
                                                                                '#value' => $id,
                                                                        ],
                                                                        'addcheckbox' . $id  => [
                                                                                '#type'                    => 'submit',
                                                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.item.btn_addcheckbox', 'fc' ), [ '@nr' => $countItems ] ),
                                                                                '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                                                                                '#limit_validation_errors' => [],
                                                                                '#attributes'              => [
                                                                                        'class' => [
                                                                                                'btn-success',
                                                                                        ],
                                                                                        'item'  => $id,
                                                                                ],
                                                                                '#submit'                  => [
                                                                                        '::addItemCheckbox',
                                                                                ],
                                                                                '#ajax'                    => [
                                                                                        'wrapper'  => "item-wrapper",
                                                                                        'callback' => '::addItemCheckboxCallback',
                                                                                        'effect'   => 'fade',
                                                                                        'progress' => [
                                                                                                'throbber' => 'none',
                                                                                        ],
                                                                                ],
                                                                                'item'                     => $id,
                                                                        ],
                                                                        'addtextfield' . $id => [
                                                                                '#type'                    => 'submit',
                                                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.item.btn_addtextfield', 'fc' ), [ '@nr' => $countItems ] ),
                                                                                '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                                                                                '#limit_validation_errors' => [],
                                                                                '#maxlength'               => 500,
                                                                                '#attributes'              => [
                                                                                        'class' => [
                                                                                                'btn-success',
                                                                                        ],
                                                                                        'item'  => $id,
                                                                                ],
                                                                                '#submit'                  => [
                                                                                        '::addItemTextfield',
                                                                                ],
                                                                                '#ajax'                    => [
                                                                                        'wrapper'  => "item-wrapper",
                                                                                        'callback' => '::addItemTextfieldCallback',
                                                                                        'effect'   => 'fade',
                                                                                        'progress' => [
                                                                                                'throbber' => 'none',
                                                                                        ],
                                                                                ],
                                                                                'item'                     => $id,
                                                                        ],
                                                                ],
                                                        ];
                                                        
                                                        $countCheckboxes = $countTextfields = 0;
                                                        
                                                        foreach ( $xpath->query( 'subelements/subelement', $node ) as $subnode ) {
                                                                
                                                                switch ( $subnode->getAttribute( 'type' ) ) {
                                                                        
                                                                        case 'checkbox':
                                                                                
                                                                                $countCheckboxes++;
                                                                                $subid = "checkbox#$countCheckboxes";
                                                                                
                                                                                $items[ $id ][ 'subitems' ][ 'content' ][ $subid ] = [
                                                                                        '#type'   => 'container',
                                                                                        'content' => [
                                                                                                'text'   => [
                                                                                                        '#type'          => 'textfield',
                                                                                                        '#placeholder'   => (string)t( $texts->get( 'reviewsheet.add.form.item.checkbox.title' ), [ '@nr' => $countCheckboxes ] ),
                                                                                                        '#title_display' => 'inline',
                                                                                                        '#required'      => true,
                                                                                                        'type'           => 'checkbox',
                                                                                                        '#prefix'        => '<div class="form-wrapper-left">',
                                                                                                        '#suffix'        => '</div>',
                                                                                                        '#default_value' => $xpath->evaluate( 'text', $subnode )[ 0 ]->nodeValue,
                                                                                                        '#maxlength'     => 500,
                                                                                                ],
                                                                                                'type'   => [
                                                                                                        '#type'  => 'hidden',
                                                                                                        '#value' => 'checkbox',
                                                                                                ],
                                                                                                'delete' => [
                                                                                                        '#type'                    => 'submit',
                                                                                                        '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.item.checkbox.btn_delete' ), [ '@nr' => $countCheckboxes, '@item' => $id ] ),
                                                                                                        '#prefix'                  => '<div class="form-wrapper-right">',
                                                                                                        '#suffix'                  => '</div>',
                                                                                                        '#limit_validation_errors' => [],
                                                                                                        '#attributes'              => [
                                                                                                                'class' => [
                                                                                                                        'btn-danger',
                                                                                                                ],
                                                                                                                'item'  => $subid,
                                                                                                                'style' => 'width: 300px;',
                                                                                                        ],
                                                                                                        '#submit'                  => [
                                                                                                                '::deleteCheckbox',
                                                                                                        ],
                                                                                                        '#ajax'                    => [
                                                                                                                'wrapper'  => "item-wrapper",
                                                                                                                'callback' => '::deleteCheckboxCallback',
                                                                                                                'effect'   => 'fade',
                                                                                                                'progress' => [
                                                                                                                        'throbber' => 'none',
                                                                                                                ],
                                                                                                        ],
                                                                                                        'item'                     => "$id:$subid",
                                                                                                ],
                                                                                        ],
                                                                                        'type'    => 'checkbox',
                                                                                        '#prefix' => '<div>',
                                                                                        '#suffix' => '</div><br clear="all"><hr class="form-add-reviewsheet-item">',
                                                                                ];
                                                                                break;
                                                                        
                                                                        case 'textfield':
                                                                                
                                                                                $countTextfields++;
                                                                                $subid = "textfield#$countTextfields";
                                                                                
                                                                                $items[ $id ][ 'subitems' ][ 'content' ][ $subid ] = [
                                                                                        '#type'   => 'container',
                                                                                        'content' => [
                                                                                                'text'   => [
                                                                                                        '#type'          => 'textfield',
                                                                                                        '#placeholder'   => (string)t( $texts->get( 'reviewsheet.add.form.item.textfield.title' ), [ '@nr' => $countTextfields ] ),
                                                                                                        '#title_display' => 'inline',
                                                                                                        '#required'      => true,
                                                                                                        'type'           => 'textfield',
                                                                                                        '#prefix'        => '<div class="form-wrapper-left">',
                                                                                                        '#suffix'        => '</div>',
                                                                                                        '#default_value' => $xpath->evaluate( 'text', $subnode )[ 0 ]->nodeValue,
                                                                                                        '#maxlength'     => 500,
                                                                                                ],
                                                                                                'type'   => [
                                                                                                        '#type'  => 'hidden',
                                                                                                        '#value' => 'textfield',
                                                                                                ],
                                                                                                'delete' => [
                                                                                                        '#type'                    => 'submit',
                                                                                                        '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.item.textfield.btn_delete' ), [ '@nr' => $countTextfields, '@item' => $id ] ),
                                                                                                        '#prefix'                  => '<div class="form-wrapper-right">',
                                                                                                        '#suffix'                  => '</div>',
                                                                                                        '#limit_validation_errors' => [],
                                                                                                        '#attributes'              => [
                                                                                                                'class' => [
                                                                                                                        'btn-danger',
                                                                                                                ],
                                                                                                                'item'  => $subid,
                                                                                                                'style' => 'width: 300px;',
                                                                                                        ],
                                                                                                        '#submit'                  => [
                                                                                                                '::deleteTextfield',
                                                                                                        ],
                                                                                                        '#ajax'                    => [
                                                                                                                'wrapper'  => "item-wrapper",
                                                                                                                'callback' => '::deleteTextfieldCallback',
                                                                                                                'effect'   => 'fade',
                                                                                                                'progress' => [
                                                                                                                        'throbber' => 'none',
                                                                                                                ],
                                                                                                        ],
                                                                                                        'item'                     => "$id:$subid",
                                                                                                ],
                                                                                        ],
                                                                                        'type'    => 'textfield',
                                                                                        '#prefix' => '<div>',
                                                                                        '#suffix' => '</div><br clear="all"><hr class="form-add-reviewsheet-item">',
                                                                                ];
                                                                                break;
                                                                }
                                                        }
                                                        break;
                                        }
                                }
                                
                                $form_state->set( 'sheet_loaded', true );
                                $form_state->set( 'items', $items );
                        }
                        
                        $form[ 'name' ] = [
                                '#title'         => (string)t( $texts->get( 'reviewsheet.add.form.sheetname', 'fc' ) ),
                                '#placeholder'   => (string)t( $texts->get( 'reviewsheet.add.form.sheetname.placeholder', 'fc' ) ),
                                '#type'          => 'textfield',
                                '#required'      => true,
                                '#default_value' => $xpath->evaluate( '/reviewsheet/meta/name' )[ 0 ]->nodeValue,
                                '#maxlength'     => 500,
                        ];
                        
                        $form[ 'description' ] = [
                                '#title'         => (string)t( $texts->get( 'reviewsheet.add.form.description', 'fc' ) ),
                                '#placeholder'   => (string)t( $texts->get( 'reviewsheet.add.form.description.placeholder', 'fc' ) ),
                                '#type'          => 'textfield',
                                '#required'      => false,
                                '#maxlength'     => 1000,
                                '#default_value' => $xpath->evaluate( '/reviewsheet/meta/description' )[ 0 ]->nodeValue,
                        ];
                        
                        $form[ 'items' ] = [
                                
                                '#type'   => 'container',
                                '#tree'   => true,
                                '#prefix' => '<div id="item-wrapper">',
                                '#suffix' => '</div>',
                                'content' => [],
                        ];
                        
                        foreach ( $form_state->get( 'items' ) as $item ) {
                                $form[ 'items' ][ 'content' ][] = $item;
                        }
                        
                        $form[ 'actions' ] = [
                                '#type'    => 'container',
                                '#suffix'  => '<hr>',
                                '#prefix'  => '<br>',
                                'elements' => [
                                        'addheading' => [
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.btn_addheading', 'fc' ) ),
                                                '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                                                '#limit_validation_errors' => [],
                                                '#attributes'              => [
                                                        'class' => [
                                                                'btn-success',
                                                        ],
                                                ],
                                                '#submit'                  => [
                                                        '::addHeading',
                                                ],
                                                '#ajax'                    => [
                                                        'wrapper'  => 'item-wrapper',
                                                        'callback' => '::addHeadingCallback',
                                                        'effect'   => 'fade',
                                                        'progress' => [
                                                                'throbber' => 'none',
                                                        ],
                                                ],
                                        ],
                                        
                                        'addsubheading' => [
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.btn_addsubheading', 'fc' ) ),
                                                '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                                                '#limit_validation_errors' => [],
                                                '#attributes'              => [
                                                        'class' => [
                                                                'btn-success',
                                                        ],
                                                ],
                                                '#submit'                  => [
                                                        '::addSubheading',
                                                ],
                                                '#ajax'                    => [
                                                        'wrapper'  => 'item-wrapper',
                                                        'callback' => '::addSubheadingCallback',
                                                        'effect'   => 'fade',
                                                        'progress' => [
                                                                'throbber' => 'none',
                                                        ],
                                                ],
                                        ],
                                        
                                        'additem' => [
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.btn_additem', 'fc' ) ),
                                                '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                                                '#limit_validation_errors' => [],
                                                '#attributes'              => [
                                                        'class' => [
                                                                'btn-success',
                                                        ],
                                                ],
                                                '#submit'                  => [
                                                        '::addItem',
                                                ],
                                                '#ajax'                    => [
                                                        'wrapper'  => 'item-wrapper',
                                                        'callback' => '::addItemCallback',
                                                        'effect'   => 'fade',
                                                        'progress' => [
                                                                'throbber' => 'none',
                                                        ],
                                                ],
                                        ],
                                ],
                        ];
                        
                        $form[ 'back' ] = [
                                '#type'                    => 'submit',
                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.btn_back', 'fc' ) ),
                                '#button_type'             => 'success',
                                '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                                '#attributes'              => [
                                        'class' => [ 'btn-warning' ],
                                ],
                                '#submit'                  => [ '::back' ],
                                '#limit_validation_errors' => [],
                        ];
                        
                        $form[ 'submit' ] = [
                                '#type'        => 'submit',
                                '#value'       => (string)t( $texts->get( 'reviewsheet.add.form.btn_submit', 'fc' ) ),
                                '#button_type' => 'success',
                                '#suffix'      => '&nbsp;&nbsp;&nbsp;',
                                '#attributes'  => [
                                        'class' => [
                                                'btn-success',
                                        ],
                                ],
                        ];
                        
                        $form[ 'delete' ] = [
                                '#type'                    => 'submit',
                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.btn_delete', 'fc' ) ),
                                '#button_type'             => 'success',
                                '#suffix'                  => '<br><br>',
                                '#attributes'              => [
                                        'class' => [
                                                'btn-danger',
                                        ],
                                ],
                                '#submit'                  => [ '::delete' ],
                                '#limit_validation_errors' => [],
                                '#attributes'              => [
                                        'onclick' => 'javascript: return confirm("' . $texts->get( 'reviewsheet.delete.confirm', 'fc' ) . '")',
                                ],
                        ];
                        
                        $form[ '#attributes' ][ 'autocomplete' ] = 'off';
                        $form[ '#cache' ][ 'max-age' ] = 0;
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $tools = \Drupal::service( 'publisso_gold.tools' );
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        $session = \Drupal::service( 'session' );
                        $items = [];
                        
                        $rp = new Reviewsheet( $form_state->get( 'id' ) );
                        $oldSheet = new \DOMDocument();
                        $oldSheet->loadXML( $rp->getElement( 'sheet' ) );
                        $oldXpath = new \DOMXpath( $oldSheet );
                        
                        foreach ( $form_state->getValue( 'items' )[ 'content' ] as $item ) {
                                
                                $ary = [
                                        'type'     => $item[ 'type' ],
                                        'position' => $item[ 'meta' ][ 'position' ],
                                        'text'     => $item[ 'text' ],
                                ];
                                
                                if ( $item[ 'type' ] == 'item' ) {
                                        $ary[ 'subitems' ] = [];
                                }
                                
                                if ( array_key_exists( 'subitems', $item ) ) {
                                        
                                        foreach ( $item[ 'subitems' ][ 'content' ] as $_item ) {
                                                
                                                $_ary = [
                                                        'type' => $_item[ 'content' ][ 'type' ],
                                                        'text' => $_item[ 'content' ][ 'text' ],
                                                ];
                                                
                                                $ary[ 'subitems' ][] = $_ary;
                                        }
                                }
                                
                                $items[] = $ary;
                        }
                        uasort( $items, [ 'self', 'sortItems' ] );
                        
                        $doc = new \DOMDocument( "1.0", "UTF-8" );
                        $xpath = new \DOMXpath( $doc );
                        $doc->formatOutput = false;
                        
                        $name = $form_state->getValue( 'name' );
                        $description = $form_state->getValue( 'description' );
                        $attrCreated = [
                                'datetime' => date( 'Y-m-d H:i:s' ),
                                'uid'      => $session->get( 'user' )[ 'id' ],
                        ];
                        
                        $tools->DOMAppendChild( 'reviewsheet', '', [], $doc, $xpath->evaluate( '/' )[ 0 ] );
                        $tools->DOMAppendChild( 'meta', '', [], $doc, $xpath->evaluate( '/reviewsheet' )[ 0 ] );
                        $tools->DOMAppendChild( 'elements', '', [], $doc, $xpath->evaluate( '/reviewsheet' )[ 0 ] );
                        $tools->DOMAppendChild( 'name', $name, [], $doc, $xpath->evaluate( '/reviewsheet/meta' )[ 0 ] );
                        $tools->DOMAppendChild( 'description', $description, [], $doc, $xpath->evaluate( '/reviewsheet/meta' )[ 0 ] );
                        
                        if ( $form_state->has( 'copy' ) && $form_state->get( 'copy' ) === true ) {
                                //import created and modifications
                                $node = $oldXpath->evaluate( '/reviewsheet/meta/created' )[ 0 ]->cloneNode( true );
                                $xpath->evaluate( '/reviewsheet/meta' )[ 0 ]->appendChild( $doc->importNode( $node ) );
                                
                                $node = $oldXpath->evaluate( '/reviewsheet/meta/modifications' )[ 0 ]->cloneNode( true );
                                $xpath->evaluate( '/reviewsheet/meta' )[ 0 ]->appendChild( $doc->importNode( $node ) );
                                $tools->DOMAppendChild( 'modification', '', [ 'datetime' => date( 'Y-m-d H:i:s' ), 'uid' => $session->get( 'user' )[ 'id' ] ], $doc, $xpath->evaluate( '/reviewsheet/meta/modifications' )[ 0 ] );
                        }
                        else {
                                //set created and modifications like add reviewsheet
                                $tools->DOMAppendChild( 'created', '', $attrCreated, $doc, $xpath->evaluate( '/reviewsheet/meta' )[ 0 ] );
                                $tools->DOMAppendChild( 'modifications', '', [], $doc, $xpath->evaluate( '/reviewsheet/meta' )[ 0 ] );
                        }
                        
                        foreach ( $items as $item ) {
                                
                                $xp = $xpath->evaluate( '/reviewsheet//elements' )[ 0 ];
                                $tools->DOMAppendChild( 'element', '', [ 'type' => $item[ 'type' ], 'weight' => $item[ 'position' ] ], $doc, $xp );
                                
                                $xp = $xpath->evaluate( '/reviewsheet//elements/element[last()]' )[ 0 ];
                                $tools->DOMAppendChild( 'text', $item[ 'text' ], [], $doc, $xp );
                                
                                if ( array_key_exists( 'subitems', $item ) ) {
                                        
                                        $tools->DOMAppendChild( 'subelements', '', [], $doc, $xp );
                                        
                                        if ( count( $item[ 'subitems' ] ) ) {
                                                
                                                $xp = $xpath->evaluate( '/reviewsheet//elements/element[last()]/subelements' )[ 0 ];
                                                
                                                foreach ( $item[ 'subitems' ] as $subitem ) {
                                                        
                                                        $tools->DOMAppendChild( 'subelement', '', [ 'type' => $subitem[ 'type' ] ], $doc, $xp );
                                                        $sub_xp = $xpath->evaluate( '/reviewsheet//elements/element[last()]/subelements/subelement[last()]' )[ 0 ];
                                                        $tools->DOMAppendChild( 'text', $subitem[ 'text' ], [], $doc, $sub_xp );
                                                }
                                        }
                                }
                        }
                        
                        $oldName = $rp->getElement( 'name' );
                        
                        if ( $form_state->has( 'copy' ) && $form_state->get( 'copy' ) === true ) {
                                $rp = new Reviewsheet();
                                $new_id = $rp->createNew( $name, $doc->saveXML() );
                        }
                        else {
                                $rp->setElement( 'sheet', $doc->saveXML() );
                                $id = $rp->getElement( 'id' );
                        }
                        
                        $action = $form_state->has( 'copy' ) && $form_state->get( 'copy' ) === true ? 'copy' : 'edit';
                        
                        drupal_set_message( (string)t( $texts->get( "reviewsheet.$action.success", 'fco' ), [ '@id' => $id, 'new_id' => $new_id, '@name' => $oldName, '@new_name' => $name ] ) );
                        $form_state->setRedirect( 'publisso_gold.reviewsheets' );
                }
        
                /**
                 * @param $a
                 * @param $b
                 * @return int
                 */
                private static function sortItems ($a, $b ) {
                        
                        $aw = $a[ 'position' ];
                        $bw = $b[ 'position' ];
                        
                        if ( $aw == $bw ) return 0;
                        return ( $aw > $bw ) ? +1 : -1;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function addHeading (array &$form, FormStateInterface $form_state ) {
                        
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        $items = $form_state->get( 'items' );
                        
                        $countHeadings = 1;
                        
                        foreach ( $items as $k => $v ) {
                                if ( preg_match( '/^heading#(\d+)/', $k, $matches ) ) {
                                        if ( $matches[ 1 ] + 1 > $countHeadings ) {
                                                $countHeadings = $matches[ 1 ] + 1;
                                        }
                                }
                        }
                        
                        $id = "heading#$countHeadings";
                        
                        $items[ $id ] = [
                                '#type'       => 'container',
                                '#tree'       => true,
                                '#id'         => "item-wrapper-$id",
                                '#attributes' => [
                                        'style' => 'padding: 4px; border: 1px solid silver; background-color: lightgray;',
                                ],
                                'text'        => [
                                        '#type'        => 'textfield',
                                        '#title'       => (string)t( $texts->get( 'reviewsheet.add.form.heading.title', 'fc' ), [ '@nr' => $countHeadings ] ),
                                        '#placeholder' => (string)t( $texts->get( 'reviewsheet.add.form.heading.placeholder', 'fc' ) ),
                                        '#required'    => true,
                                        '#maxlength'   => 500,
                                ],
                                'meta'        => [
                                        '#type'    => 'container',
                                        'position' => [
                                                '#title'  => (string)t( $texts->get( 'reviewsheet.add.form.heading.position', 'fc' ) ),
                                                '#type'   => 'number',
                                                '#min'    => 0,
                                                '#size'   => 5,
                                                '#prefix' => '<div class="form-wrapper-left">',
                                                '#suffix' => '</div>',
                                        ],
                                        'delete'   => [
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.heading.btn_delete', 'fc' ), [ '@nr' => $countHeadings ] ),
                                                '#prefix'                  => '<div class="form-wrapper-right">',
                                                '#suffix'                  => '</div><br clear="all">',
                                                '#limit_validation_errors' => [],
                                                '#attributes'              => [
                                                        'class' => [
                                                                'btn-danger',
                                                        ],
                                                        'item'  => $id,
                                                ],
                                                '#submit'                  => [
                                                        '::deleteHeading',
                                                ],
                                                '#ajax'                    => [
                                                        'wrapper'  => "item-wrapper",
                                                        'callback' => '::deleteHeadingCallback',
                                                        'effect'   => 'fade',
                                                        'progress' => [
                                                                'throbber' => 'none',
                                                        ],
                                                ],
                                                'item'                     => $id,
                                        ],
                                ],
                                'type'        => [
                                        '#type'  => 'hidden',
                                        '#value' => 'heading',
                                ],
                        ];
                        
                        $form_state->set( 'items', $items );
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addHeadingCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'items' ];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function addSubheading (array &$form, FormStateInterface $form_state ) {
                        
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        $items = $form_state->get( 'items' );
                        $countSubheadings = 1;
                        
                        foreach ( $items as $k => $v ) {
                                if ( preg_match( '/^subheading#(\d+)/', $k, $matches ) ) {
                                        if ( $matches[ 1 ] + 1 > $countSubheadings ) {
                                                $countSubheadings = $matches[ 1 ] + 1;
                                        }
                                }
                        }
                        
                        $id = "subheading#$countSubheadings";
                        
                        $items[ $id ] = [
                                '#type'       => 'container',
                                '#tree'       => true,
                                '#id'         => "item-wrapper-$id",
                                '#attributes' => [
                                        'style' => 'padding: 4px; border: 1px solid silver; background-color: lightgray;',
                                ],
                                'text'        => [
                                        '#type'        => 'textfield',
                                        '#title'       => (string)t( $texts->get( 'reviewsheet.add.form.subheading.title', 'fc' ), [ '@nr' => $countSubheadings ] ),
                                        '#placeholder' => (string)t( $texts->get( 'reviewsheet.add.form.subheading.placeholder', 'fc' ) ),
                                        '#required'    => true,
                                        '#maxlength'   => 500,
                                ],
                                'meta'        => [
                                        '#type'    => 'container',
                                        'position' => [
                                                '#title'  => (string)t( $texts->get( 'reviewsheet.add.form.subheading.position', 'fc' ) ),
                                                '#type'   => 'number',
                                                '#min'    => 0,
                                                '#size'   => 5,
                                                '#prefix' => '<div class="form-wrapper-left">',
                                                '#suffix' => '</div>',
                                        ],
                                        'delete'   => [
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.subheading.btn_delete', 'fc' ), [ '@nr' => $countSubheadings ] ),
                                                '#prefix'                  => '<div class="form-wrapper-right">',
                                                '#suffix'                  => '</div><br clear="all">',
                                                '#limit_validation_errors' => [],
                                                '#attributes'              => [
                                                        'class' => [
                                                                'btn-danger',
                                                        ],
                                                        'item'  => $id,
                                                ],
                                                '#submit'                  => [
                                                        '::deleteSubheading',
                                                ],
                                                '#ajax'                    => [
                                                        'wrapper'  => "item-wrapper",
                                                        'callback' => '::deleteSubheadingCallback',
                                                        'effect'   => 'fade',
                                                        'progress' => [
                                                                'throbber' => 'none',
                                                        ],
                                                ],
                                                'item'                     => $id,
                                        ],
                                ],
                                'type'        => [
                                        '#type'  => 'hidden',
                                        '#value' => 'subheading',
                                ],
                        ];
                        
                        $form_state->set( 'items', $items );
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addSubheadingCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'items' ];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function addItem (array &$form, FormStateInterface $form_state ) {
                        
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        $items = $form_state->get( 'items' );
                        
                        $countItems = 1;
                        
                        foreach ( $items as $k => $v ) {
                                if ( preg_match( '/^item#(\d+)/', $k, $matches ) ) {
                                        if ( $matches[ 1 ] + 1 > $countItems ) {
                                                $countItems = $matches[ 1 ] + 1;
                                        }
                                }
                        }
                        
                        $id = "item#$countItems";
                        
                        $items[ $id ] = [
                                '#type'       => 'container',
                                '#tree'       => true,
                                '#id'         => "item-wrapper-$id",
                                '#suffix'     => '<br clear="all">',
                                '#attributes' => [
                                        'style' => 'padding: 4px; border: 1px solid silver; background-color: lightgray;',
                                ],
                                'headline'    => [
                                        '#markup' => (string)t( $texts->get( 'reviewsheet.add.form.item.headline', 'fc' ), [ '@nr' => $countItems ] ),
                                        '#prefix' => '<h3>',
                                        '#suffix' => '</h3>',
                                ],
                                'text'        => [
                                        '#type'        => 'textfield',
                                        '#title'       => (string)t( $texts->get( 'reviewsheet.add.form.item.title', 'fc' ) ),
                                        '#placeholder' => (string)t( $texts->get( 'reviewsheet.add.form.item.placeholder', 'fc' ) ),
                                        '#required'    => true,
                                        '#maxlength'   => 500,
                                ],
                                'meta'        => [
                                        '#type'    => 'container',
                                        'position' => [
                                                '#title'  => (string)t( $texts->get( 'reviewsheet.add.form.item.position', 'fc' ) ),
                                                '#type'   => 'number',
                                                '#min'    => 0,
                                                '#size'   => 5,
                                                '#prefix' => '<div class="form-wrapper-left">',
                                                '#suffix' => '</div>',
                                        ],
                                        'delete'   => [
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.item.btn_delete', 'fc' ), [ '@nr' => $countItems ] ),
                                                '#prefix'                  => '<div class="form-wrapper-right">',
                                                '#suffix'                  => '</div><br clear="all">',
                                                '#limit_validation_errors' => [],
                                                '#attributes'              => [
                                                        'class' => [
                                                                'btn-danger',
                                                        ],
                                                        'item'  => $id,
                                                ],
                                                '#submit'                  => [
                                                        '::deleteItem',
                                                ],
                                                '#ajax'                    => [
                                                        'wrapper'  => "item-wrapper",
                                                        'callback' => '::deleteItemCallback',
                                                        'effect'   => 'fade',
                                                        'progress' => [
                                                                'throbber' => 'none',
                                                        ],
                                                ],
                                                'item'                     => $id,
                                        ],
                                        '#suffix'  => '<hr class="form-add-reviewsheet-item">',
                                ],
                                'type'        => [
                                        '#type'  => 'hidden',
                                        '#value' => 'item',
                                ],
                                'subitems'    => [
                                        '#type'   => 'container',
                                        'content' => [],
                                ],
                                'action'      => [
                                        'id'                 => [
                                                '#type'  => 'hidden',
                                                '#value' => $id,
                                        ],
                                        'addcheckbox' . $id  => [
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.item.btn_addcheckbox', 'fc' ), [ '@nr' => $countItems ] ),
                                                '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                                                '#limit_validation_errors' => [],
                                                '#attributes'              => [
                                                        'class' => [
                                                                'btn-success',
                                                        ],
                                                        'item'  => $id,
                                                ],
                                                '#submit'                  => [
                                                        '::addItemCheckbox',
                                                ],
                                                '#ajax'                    => [
                                                        'wrapper'  => "item-wrapper",
                                                        'callback' => '::addItemCheckboxCallback',
                                                        'effect'   => 'fade',
                                                        'progress' => [
                                                                'throbber' => 'none',
                                                        ],
                                                ],
                                                'item'                     => $id,
                                        ],
                                        'addtextfield' . $id => [
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.item.btn_addtextfield', 'fc' ), [ '@nr' => $countItems ] ),
                                                '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                                                '#limit_validation_errors' => [],
                                                '#attributes'              => [
                                                        'class' => [
                                                                'btn-success',
                                                        ],
                                                        'item'  => $id,
                                                ],
                                                '#submit'                  => [
                                                        '::addItemTextfield',
                                                ],
                                                '#ajax'                    => [
                                                        'wrapper'  => "item-wrapper",
                                                        'callback' => '::addItemTextfieldCallback',
                                                        'effect'   => 'fade',
                                                        'progress' => [
                                                                'throbber' => 'none',
                                                        ],
                                                ],
                                                'item'                     => $id,
                                        ],
                                ],
                        ];
                        
                        $form_state->set( 'items', $items );
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addItemCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'items' ];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function addItemCheckbox (array &$form, FormStateInterface $form_state ) {
                        
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        $te = $form_state->getTriggeringElement()[ 'item' ];
                        $items = $form_state->get( 'items' );
                        $countCheckboxes = 1;
                        
                        foreach ( $items[ $te ][ 'subitems' ][ 'content' ] as $_k => $_v ) {
                                if ( preg_match( "/^checkbox#(\d+)/", $_k, $matches ) ) {
                                        if ( $matches[ 1 ] + 1 > $countCheckboxes ) {
                                                $countCheckboxes = $matches[ 1 ] + 1;
                                        }
                                }
                        }
                        
                        $id = "checkbox#$countCheckboxes";
                        
                        $items[ $te ][ 'subitems' ][ 'content' ][ $id ] = [
                                '#type'   => 'container',
                                'content' => [
                                        'text'   => [
                                                '#type'          => 'textfield',
                                                '#placeholder'   => (string)t( $texts->get( 'reviewsheet.add.form.item.checkbox.title' ), [ '@nr' => $countCheckboxes ] ),
                                                '#title_display' => 'inline',
                                                '#required'      => true,
                                                'type'           => 'checkbox',
                                                '#prefix'        => '<div class="form-wrapper-left">',
                                                '#suffix'        => '</div>',
                                                '#maxlength'     => 500,
                                        ],
                                        'type'   => [
                                                '#type'  => 'hidden',
                                                '#value' => 'checkbox',
                                        ],
                                        'delete' => [
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.item.checkbox.btn_delete' ), [ '@nr' => $countCheckboxes, '@item' => $te ] ),
                                                '#prefix'                  => '<div class="form-wrapper-right">',
                                                '#suffix'                  => '</div>',
                                                '#limit_validation_errors' => [],
                                                '#attributes'              => [
                                                        'class' => [
                                                                'btn-danger',
                                                        ],
                                                        'item'  => $id,
                                                        'style' => 'width: 300px;',
                                                ],
                                                '#submit'                  => [
                                                        '::deleteCheckbox',
                                                ],
                                                '#ajax'                    => [
                                                        'wrapper'  => "item-wrapper",
                                                        'callback' => '::deleteCheckboxCallback',
                                                        'effect'   => 'fade',
                                                        'progress' => [
                                                                'throbber' => 'none',
                                                        ],
                                                ],
                                                'item'                     => "$te:$id",
                                        ],
                                ],
                                'type'    => 'checkbox',
                                '#prefix' => '<div>',
                                '#suffix' => '</div><br clear="all"><hr class="form-add-reviewsheet-item">',
                        ];
                        
                        $form_state->set( 'items', $items );
                        
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addItemCheckboxCallback (array &$form, FormStateInterface $form_state ) {
                        
                        $te = $form_state->getTriggeringElement()[ 'item' ];
                        
                        $items = $form_state->get( 'items' );
                        $parent = $items[ $te ];
                        
                        return $form[ 'items' ];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function addItemTextfield (array &$form, FormStateInterface $form_state ) {
                        
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        $te = $form_state->getTriggeringElement()[ 'item' ];
                        $items = $form_state->get( 'items' );
                        
                        $countTextfields = 1;
                        
                        foreach ( $items[ $te ][ 'subitems' ][ 'content' ] as $_k => $_v ) {
                                if ( preg_match( "/^textfield#(\d+)/", $_k, $matches ) ) {
                                        if ( $matches[ 1 ] + 1 > $countTextfields ) {
                                                $countTextfields = $matches[ 1 ] + 1;
                                        }
                                }
                        }
                        
                        $id = "textfield#$countTextfields";
                        
                        $items[ $te ][ 'subitems' ][ 'content' ][ $id ] = [
                                '#type'      => 'container',
                                'content'    => [
                                        'text'   => [
                                                '#type'          => 'textfield',
                                                '#placeholder'   => (string)t( $texts->get( 'reviewsheet.add.form.item.textfield.title' ), [ '@nr' => $countTextfields ] ),
                                                '#title_display' => 'inline',
                                                '#required'      => true,
                                                'type'           => 'textfield',
                                                '#prefix'        => '<div class="form-wrapper-left">',
                                                '#suffix'        => '</div>',
                                                '#maxlength'     => 500,
                                        ],
                                        'type'   => [
                                                '#type'  => 'hidden',
                                                '#value' => 'textfield',
                                        ],
                                        'delete' => [
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)t( $texts->get( 'reviewsheet.add.form.item.textfield.btn_delete' ), [ '@nr' => $countTextfields, '@item' => $te ] ),
                                                '#prefix'                  => '<div class="form-wrapper-right">',
                                                '#suffix'                  => '</div>',
                                                '#limit_validation_errors' => [],
                                                '#attributes'              => [
                                                        'class' => [
                                                                'btn-danger',
                                                        ],
                                                        'item'  => $id,
                                                        'style' => 'width: 300px;',
                                                ],
                                                '#submit'                  => [
                                                        '::deleteTextfield',
                                                ],
                                                '#ajax'                    => [
                                                        'wrapper'  => "item-wrapper",
                                                        'callback' => '::deleteTextfieldCallback',
                                                        'effect'   => 'fade',
                                                        'progress' => [
                                                                'throbber' => 'none',
                                                        ],
                                                ],
                                                'item'                     => "$te:$id",
                                        ],
                                ],
                                'type'       => 'textfield',
                                '#prefix'    => '<div>',
                                '#suffix'    => '</div><br clear="all"><hr class="form-add-reviewsheet-item">',
                                '#maxlength' => 500,
                        ];
                        
                        $form_state->set( 'items', $items );
                        
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addItemTextfieldCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'items' ];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function deleteHeading (array &$form, FormStateInterface $form_state ) {
                        
                        $te = $form_state->getTriggeringElement()[ 'item' ];
                        
                        $items = $form_state->get( 'items' );
                        unset( $items[ $te ] );
                        $form_state->set( 'items', $items );
                        
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function deleteHeadingCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'items' ];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function deleteSubheading (array &$form, FormStateInterface $form_state ) {
                        
                        $te = $form_state->getTriggeringElement()[ 'item' ];
                        
                        $items = $form_state->get( 'items' );
                        unset( $items[ $te ] );
                        $form_state->set( 'items', $items );
                        
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function deleteSubheadingCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'items' ];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function deleteItem (array &$form, FormStateInterface $form_state ) {
                        
                        $te = $form_state->getTriggeringElement()[ 'item' ];
                        
                        $items = $form_state->get( 'items' );
                        unset( $items[ $te ] );
                        $form_state->set( 'items', $items );
                        
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function deleteItemCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'items' ];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function deleteCheckbox (array &$form, FormStateInterface $form_state ) {
                        
                        $te = array_filter( explode( ':', $form_state->getTriggeringElement()[ 'item' ] ) );
                        $items = $form_state->get( 'items' );
                        unset( $items[ $te[ 0 ] ][ 'subitems' ][ 'content' ][ $te[ 1 ] ] );
                        $form_state->set( 'items', $items );
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function deleteCheckboxCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'items' ];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function deleteTextfield (array &$form, FormStateInterface $form_state ) {
                        
                        $te = array_filter( explode( ':', $form_state->getTriggeringElement()[ 'item' ] ) );
                        $items = $form_state->get( 'items' );
                        unset( $items[ $te[ 0 ] ][ 'subitems' ][ 'content' ][ $te[ 1 ] ] );
                        $form_state->set( 'items', $items );
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function deleteTextfieldCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'items' ];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function back (array &$form, FormStateInterface $form_state ) {
                        $form_state->setRedirect( 'publisso_gold.reviewsheets' );
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function delete (array &$form, FormStateInterface $form_state ) {
                        
                        $rp = new Reviewsheet( $form_state->get( 'id' ) );
                        $rp->delete();
                        $form_state->setRedirect( 'publisso_gold.reviewsheets' );
                }
        }
