<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\BookSearch.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class BookSearch extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return BookSearch|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'BookSearch';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                $texts = \Drupal::service('publisso_gold.texts');
                $form = \Drupal::formBuilder()->getForm('Drupal\search\Form\SearchBlockForm');
                $form['keys']['#description'] = '';
                $form['keys']['#placeholder'] = (string)t($texts->get('book.content.sidebar.booksearch.placeholder', 'fc'));
                
                $form['bk_id'] = [
                        '#type' => 'hidden',
                        '#value' => $args['bk_id']
                ];
                
                $form['actions'] = array('#type' => 'actions');
                $form['actions']['submit'] = array(
                        '#type' => 'submit',
                        '#value' => $this->t('Search'),
                        // Prevent op from showing up in the query string.
                        '#name' => '',
                );
                
                $form['submit'] = [
                        '#type' => 'button',
                        '#value' => (string)$this->t(''),
                        '#attributes' => [
                                'class' => [
                                        'glyphicon glyphicon-search'
                                ],
                                'style' => 'margin-left: -36px;'
                        ],
                        '#submit' => ['::submitForm']
                ];
                
                $form['spacer'] = [
                        '#type' => 'markup',
                        '#markup' => '<br><br>'
                ];
                unset($form['actions']['submit']);
                $form['#action'] = $this->url('publisso_gold.book.search');  
                $form_state->setCached(false);
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        
        return $form;
    }
}
