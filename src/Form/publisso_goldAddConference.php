<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldAddConference.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldAddConference extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $num_corporations;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldAddConference|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldaddconference';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        if($this->modname && !$form_state->get('modname')){
            $form_state->set('modname', $this->modname);
        }
        
        if(!$this->modname && $form_state->get('modname')){
            $this->modname = $form_state->get('modname');
        }
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
        $form['site_headline'] = [
            '#type' => 'markup',
            '#markup' => '<h1>'.t('Add Conference').'</h1>'
        ];
        
        $form['title'] = [
            '#type' => 'textfield',
            '#title' => t('Title'),
            '#required' => true
        ];
        
        $form['title_abbr'] = [
            '#type' => 'textfield',
            '#title' => t('Title abbr.'),
            '#required' => true
        ];
        
        $form['location'] = [
            '#type' => 'textfield',
            '#title' => t('Location'),
            '#required' => true
        ];
        
        $form['date_start'] = [
            '#type' => 'date',
            '#title' => t('Date start'),
            '#required' => true
        ];
        
        $form['description'] = [
            '#type' => 'text_format',
            '#title' => t('Description')
        ];
        
        $form['authors'] = [
            '#type' => 'select',
            '#multiple' => true,
            '#options' => getUsersByRole(\Drupal::database(), [2, 3, 4, 5, 6, 7]),
            '#title' => t('Authors')
        ];
        
        $form['editorial_office'] = [
            '#type' => 'select',
            '#multiple' =>  true,
            '#options' => getUsersByRole(\Drupal::database(), [2, 3, 4, 5, 6, 7]),
            '#title' => t('Editorial Office')
        ];
        
        $form['editors_in_chief'] = [
            '#type' => 'select',
            '#multiple' => true,
            '#options' => getUsersByRole(\Drupal::database(), [2, 3, 4, 5, 6, 7]),
            '#title' => t('Editors-in-Chief')
        ];
        
        $form['editors'] = [
            '#type' => 'select',
            '#multiple' => true,
            '#options' => getUsersByRole(\Drupal::database(), [2, 3, 4, 5, 6, 7]),
            '#title' => t('Editors')
        ];
        
        $form['sustaining_members'] = [
            '#type' => 'textfield',
            '#title' => t('Sustaining members')
        ];
        
        $form['isbn'] = [
            '#type' => 'textfield',
            '#title' => t('ISBN')
        ];
        
        $form['date_end'] = [
            '#type' => 'date',
            '#title' => t('Date end')
        ];
        
        //init counter
        if(!$form_state->get('num_corporation'))
           $form_state->set('num_corporation', 0);
        
        $max = $form_state->get('num_corporation');
        
        $form['corporation'] = array(
          '#tree' => TRUE,
          '#prefix' => '<div id="corporation-replace">',
          '#suffix' => '</div>'
        );
        
        for ($delta = 0; $delta < $max; $delta++) {
            
            if (!isset($form['corporation'][$delta])) {
                
                $element = array(
                    '#type' => 'textfield'
                );
                
                if($delta == 0){
                    $element['#title'] = t('Corporation');
                }
                
                $form['corporation'][$delta] = $element;
            }
        }
        
        $form['addCorporation'] = array(
            '#type' => 'submit',
            '#name' => 'addCorporation',
            '#value' => t('Add Corporation'),
            '#submit' => array('::addMoreSubmit'),
            
            '#ajax' => array(
              'callback' => '::addMoreCallback',
              'wrapper' => 'corporation-replace',
              'effect' => 'fade',
            ),
            
        );
        
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => t('Create Conference')
        ];
        
        $form['#attributes'] = array(
            'enctype' => 'multipart/form-data'
        );
        
        $form['#cache']['max-age'] = 0;
        
        $form_state->setCached(false);
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        //save conferecne in database -> gets last_insert_id
        $corporation = $form_state->getValue('corporation');
        
        $cf_id = \Drupal::database()
            ->insert('rwPubgoldConferences')
            ->fields(
                array(
                    'cf_title' => $form_state->getValue('title'),
                    'cf_location' => $form_state->getValue('location'),
                    'cf_title_abbr' => $form_state->getValue('title_abbr'),
                    'cf_sustaining_members' => $form_state->getValue('sustaining_members'),
                    'cf_description' => base64_encode(json_encode($form_state->getValue('description'))),
                    'cf_isbn' => $form_state->getValue('isbn'),
                    'cf_corporation' => count($corporation) ? json_encode($corporation) : null,
                    'cf_authors' => json_encode($form_state->getValue('authors')),
                    'cf_editors' => json_encode($form_state->getValue('editors')),
                    'cf_date_end' => $form_state->getValue('date_end') ? $form_state->getValue('date_end').' 00:00:00' : null,
                    'cf_date_start' => $form_state->getValue('date_start') ? $form_state->getValue('date_start').' 00:00:00' : null
                )
            )
            ->execute();
        
        //save editors-in-chief
        foreach($form_state->getValue('editors_in_chief') as $_){
            \Drupal::database()
                ->insert('rwPubgoldConferencesEditorsInChief')
                ->fields([
                    'ceic_cfid' => $cf_id,
                    'ceic_uid' => $_
                ])
                ->execute();
        }
        
        //save editorial office
        foreach($form_state->getValue('editorial_office') as $_){
            \Drupal::database()
                ->insert('rwPubgoldConferencesEditorialOffice')
                ->fields([
                    'ceo_cfid' => $cf_id,
                    'ceo_uid' => $_
                ])
                ->execute();
        }
        
        $form_state->cleanValues();
        drupal_set_message(t('Conference successfully created!'), 'status');
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function add_corporation(array &$form, FormStateInterface $form_state){
        
        $form['corporation'][] = [
            '#type' => 'textfield',
            '#title' => t('Corporation')
        ];
        
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addMoreSubmit(array &$form, FormStateInterface $form_state) {
        
        $form_state->set('num_corporation', $form_state->get('num_corporation') + 1);
        $form['corporation'][] = [
            '#type' => 'textfield',
            '#title' => t('Corporation')
        ];
        
        $form_state->setRebuild();
        $form_state->setRebuild();
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addMoreCallback(array &$form, FormStateInterface $form_state) {
        return $form['corporation'];
    }
}
