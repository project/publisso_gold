<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowSetEditors.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldWorkflowSetEditors extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldWorkflowSetEditors|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldworkflowseteditors';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        $args = $form_state->getBuildInfo();
        
        if(count($args))
            $wf_id = $args['args'][0]['wf_id'];
        
        if(!$wf_id)
            $wf_id = $form_state->get('wf_id');
        
        if($wf_id){
            
            $form_state->set('wf_id', $wf_id);
            
            //get the workflow-item
            $workflow = getDashboard(\Drupal::Database(), $wf_id);
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            $new_workflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
			
            //get predefined editors
            $editors = array();
            
            switch($workflow_data->type){
                
                case 'bookchapter':
					$medium = new \Drupal\publisso_gold\Controller\Book($new_workflow->getDataElement('bk_id'));
                    $editors = getBookEditors(\Drupal::database(), $workflow_data->bk_id);
                    break;
                
                case 'journalarticle':
					$medium = new \Drupal\publisso_gold\Controller\Journal($new_workflow->getDataElement('jrn_id'));
                    $editors = getJournalEditors(\Drupal::database(), $workflow_data->jrn_id);
                    break;
                
                case 'congressmeeting':
					$medium = new \Drupal\publisso_gold\Controller\COnference($new_workflow->getDataElement('cf_id'));
                    break;
            }
            
			$editors = $medium->readEditors();
			
			if(!count($editors)){
				
                foreach(getUsersByRole(\Drupal::database(), [2,3,4,5,6,7]) as $id => $name)
					$editors[] = new \Drupal\publisso_gold\Controller\User($id);
			}
			
            foreach($editors as $index => $user){
				
				$editors[$user->getElement('id')] =
					(!empty($user->profile->getElement('graduation')) ? $user->profile->getElement('graduation') . ' ' :'').
					$user->profile->getElement('lastname') . ', ' .
					$user->profile->getElement('firstname') . ' ' .
					(!empty($user->profile->getElement('graduation_suffix')) ? $user->profile->getElement('graduation_suffix') . ' ' :'');
				unset($editors[$index]);
			}
			
            $form = [
                   'action-list' => [
					'#weight' => 100,
					'#type' => 'fieldset',
					'#title' => t('Direct action'),
					'#collapsed' => true,
					'#collapsible' => true,
					'#tree' => false,
					'content' =>[
					
						'comment' => [
							'#type' => 'textarea',
							'#title' => t('Comment'),
							'#suffix' => '<br>'
						],
						
						'comment_for_author' => [
							'#type' => 'textarea',
							'#title' => t('Comment for author'),
							'#suffix' => '<br>'
						],
						
						'action' => [
							'#type' => 'select',
							'#title' => 'Action',
							'#required' => true,
							'#options' => [
								'accept' => t('Accept Submission'),
								'reject' => t('Reject Submission'),
								'revision' => t('To author for revision')
							]
						],
						
						'set_action' => [
							'#type' => 'submit',
							'#value' => t('Set action & close'),
							'#button_type' => 'success',
							'#submit' => [
								'::setAction'
							],
							'#limit_validation_errors' => [[
								'action'
							]]
						]
					]
				],
                
                'editor-list' => [
                    '#type' => 'fieldset',
                    '#title' => t('Set editor(s)'),
                    'content' => [
                        
                        'editors_list' => [
                            '#title' => t('Available users for editor'),
                            '#type' => 'select',
                            '#options' => $editors,
                            '#multiple' => false,
                            '#required' => true
                        ],
                        
						'invite_help' => [
							'#type' => 'markup',
							'#markup' => (string)t('If you would like to assign someone who is not listed, choose "invite user"'),
							'#prefix' => '<div>',
							'#suffix' => '</div>'
						],
						
						'invite' => [
							'#type' => 'link',
							'#title' => t('Invite user'),
							'#url' => Url::fromRoute('publisso_gold.workflow.invite_user', ['wf_id' => $form_state->get('wf_id')]),
							'#suffix' => '<br><br>'
						],
						
                        'submit' => [
                            '#type' => 'submit',
                            '#value' => t('Set editor(s) & close'),
							'#button_type' => 'success',
							'#limit_validation_errors' => [[
								'editors_list'
							]],
                            '#submit' => ['::setEditors']
                        ]
                    ]
                ]
            ];
        }
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function setAction(array &$form, FormStateInterface $form_state) {
		
		$session = \Drupal::service('session');
		$action = $form_state->getValue('action');
		
		$workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
		
		if(!$workflow->getElement('id')){
			drupal_set_message(t('Can\'t assign action to a specific workflow. Please contact technical support team.'), 'error');
			$form_state->setRebuild();
			return $form;
		}
		
		$creator_uid = $workflow->getElement('created_by_uid');
		
		switch($action){
			
			case 'accept':
				$workflow->setState('accepted');
				//$workflow->setElement('assigned_to', 'u:'.implode(',u:', explode(',', $workflow->getElement('assigned_to_eo'))));
				//$workflow->setDataElement('state', 'accepted');
				break;
			
			case 'reject':
				$workflow->setState('rejected');
				//$workflow->setDataElement('state', 'rejected');
				break;
			
			case 'revision':
				$workflow->setState('in revision');
				//$workflow->setElement('assigned_to', 'u:'.$workflow->getElement('created_by_uid'));
				//$workflow->setDataElement('state', 'in revision');
				break;
		}
		
//comments
		$comment = $_REQUEST['comment'];
		$comment_for_author = $_REQUEST['comment_for_author'];
		
		$db_comment = $db_comment_for_author = false;
		
		if(!empty($comment)){
			
			$comment = base64_encode($comment);
			$db_comment = \Drupal::database()->insert('rwPubgoldWorkflowComments')
									 ->fields([
										'wfc_created_by_uid' => $session->get('user')['id'],
										'wfc_comment' => $comment,
										'wfc_wfid' => $workflow->getElement('id')
									 ])
									 ->execute();
		}
		
		if(!empty($comment_for_author)){
			
			$comment_for_author = base64_encode($comment_for_author);
			$db_comment_for_author = \Drupal::database()->insert('rwPubgoldWorkflowCommentsForAuthors')
									 ->fields([
										'cfa_created_by_uid' => $session->get('user')['id'],
										'cfa_comment' => $comment_for_author,
										'cfa_wfid' => $workflow->getElement('id'),
										'cfa_for_uid' => $workflow->getElement('created_by_uid')
									 ])
									 ->execute();
		}
// -- comments --
		
		/******************************************
		 * unlock workflow                        *
		 ******************************************/
		$workflow->setElement('locked', null);
		$workflow->setElement('locked_by_uid', null);
		/******************************************/
		/******************************************/

		$form_state->setRedirect('publisso_gold.dashboard');
		return $form;
	}
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function setEditors(array &$form, FormStateInterface $form_state) {
        
        $editors = $form_state->getValue('editors_list');
        $session = \Drupal::service('session');
        if(!is_array($editors)){
          $editors = str_split($editors, strlen($editors));
        }
        
        $wf_id = $form_state->get('wf_id');
		
		$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
		$objMedium = $objWorkflow->getParentMedium();
        
        $workflow = getDashboard($this->database, $wf_id);
        
//inform users about registration if not done
		$assigned_users = [];
		
		foreach($editors as $uid){
			
			$_user = new \Drupal\publisso_gold\Controller\User($uid);
			
			if($_user->getElement('reg_notification_sent') == 0){
				$assigned_users[] = $_user;
			}
		}
		
		foreach($assigned_users as $_){
			
			//send Reg-Mail to user if not yet be done
			if($_->getElement('reg_notification_sent') == 0){
				
				$port = \Drupal::request()->getPort();
                                $host = \Drupal::request()->getHttpHost();
                                $host = "http".($port == 443 ? 's' : '')."://$host";
				$link = $host;
				$password['clear'] = genTempPassword();
				$password['encrypted'] = '*'.strtoupper(sha1(sha1($user['password_clear'], true)));
				
				$_->setElement('password', $password['encrypted']);
				
				foreach(['registration_password', 'registration_info'] as $name){
				
					$vars = [
						'::firstname::' 	=> $_->profile->getElement('firstname'),
						'::lastname::' 		=> $_->profile->getElement('lastname'),
						'::link::' 			=> $link,
						'::email::'			=> $_->profile->getElement('email'),
						'::user::' 			=> $_->getElement('user'),
						'::login_link::' 	=> $link.'/publisso_gold/login',
						'::password::' 		=> $password['clear'],
					];
					
					sendRegistrationInfoMail($name, $vars);
				}
				
				$_->setElement('reg_notification_sent', 1);
			}
			// - Reg-Mail --
		}
//-- inform users --
		
		$objWorkflow->setState('assigned to editor', $editors);
		$objWorkflow->setElement('eic_last_edited', $session->get('user')['id']);
		$objWorkflow->unlock();
		
        $form_state->setRedirect('publisso_gold.dashboard');
        return $form;
    }
        
        /**
         * @param null $tmpl
         * @param null $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl = NULL, $vars = NULL){
        
        if($tmpl == NULL || $vars == NULL){
            //set Site-Vars
            $this->tmpl = str_replace(array_keys($this->tmpl_vars), array_values($this->tmpl_vars), $this->tmpl);
            
            //remove unused vars
            $this->tmpl = preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl);
        }
        else{
            //set Site-Vars
            $tmpl = str_replace(array_keys($vars), array_values($vars), $tmpl);
            
            //remove unused vars
            return preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $tmpl);
        }
    }
}
