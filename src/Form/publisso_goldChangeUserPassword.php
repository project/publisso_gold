<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldChangeUserPassword.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Ajax\AjaxResponse;
        use Drupal\Core\Ajax\AlertCommand;
        use Drupal\Core\Ajax\CloseModalDialogCommand;
        use Drupal\Core\Form\FormInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Database\Connection;
        use Drupal\Core\Messenger\Messenger;
        use Drupal\file\Element;
        use Drupal\publisso_gold\Controller\Manager\TemplateManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_sso\Classes\SSO;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Symfony\Component\HttpFoundation\File;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldChangeUserPassword extends FormBase {
                
                private $modname = 'publisso_gold';
                private $database;
                private $modpath;
                private $currentUser;
                
                public function __construct () {
                        $this->currentUser = new \Drupal\publisso_gold\Controller\User( $_SESSION[ 'user' ][ 'id' ] );
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'publisso_goldchangeuserpassword';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state ) {
                        
                        if ( !$this->database )
                                $this->database = \Drupal::database();
                        
                        $form[ 'message' ] = [
                                '#markup' => '<div>&nbsp;</div>', '#prefix' => '<div id="wrpMessage">', '#suffix' => '</div>',
                        ];
                        
                        $template = TemplateManager::load('publisso_password_requirements');
                        $template->setVar('prefix', (string)$this->t('The password must meet the following requirements:'));
                        $template->setVar('suffix', (string)$this->t('If the password does not meet the requirements, an error is returned!'));
                        $template->setVar('requirements', '<li>'.(string)$this->t('at least 8 characters').'</li>');
                        $template->appendToVar('requirements', '<li>'.(string)$this->t('letters and numbers').'</li>');
                        $template->appendToVar('requirements', '<li>'.(string)$this->t('punctuation (allowed: ,.;:-_!?&%$=#+*~)').'</li>');
                        $form[ 'info' ] = [
                                '#type'     => 'inline_template',
                                '#template' => $template->parse()
                        ];
                        
                        $form[ 'password_new' ] = [
                                '#type'     => 'password',
                                '#title'    => t( 'Please enter a new password' ) . ':',
                                '#required' => true,
                        ];
                        
                        $form[ 'password_confirm' ] = [
                                '#type'     => 'password',
                                '#title'    => t( 'Please retype your password' ) . ':',
                                '#required' => true,
                        ];
                        
                        $form['cancel'] = [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Cancel'),
                                '#button_type' => 'danger',
                                '#suffix' => '&nbsp;&nbsp;&nbsp;',
                                '#ajax' => [
                                        'callback' => '::cancel',
                                        'throbber' => 'none'
                                ]
                        ];
                        
                        $form[ 'save' ] = [
                                '#type'  => 'submit',
                                '#value' => t( 'Save' ),
                                '#ajax'  => [
                                        'wrapper'  => 'wrpMessage',
                                        'callback' => '::chPassword',
                                ],
                        ];
                        
                        $form[ '#cache' ][ 'max-age' ] = 0;
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        return $form;
                }
                
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return AjaxResponse
                 */
                public function cancel(array &$form, FormStateInterface $form_state){
                        $response = new AjaxResponse();
                        $response->addCommand(new CloseModalDialogCommand());
                        return $response;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return AjaxResponse
                 */
                public function chPassword (array &$form, FormStateInterface $form_state ) {
        
                        $response = new AjaxResponse();
                        $user = Publisso::currentUser();
                        $id = $user->getId();
                        
                        if ( !$id ) {
                                $response->addCommand(new AlertCommand((string)$this->t('ERROR: Userinformation not found!')));
                                return $response;
                        }
                        
                        if ( $form_state->getValue( 'password_new' ) != $form_state->getValue( 'password_confirm' ) ) {
                                $response->addCommand(new AlertCommand((string)$this->t('ERROR: The entered passwords don\'t match!')));
                                return $response;
                        }
                        
                        $password = $form_state->getValue( 'password_new' );
                        
                        if ( strlen( $password ) < 8 ) {
                                $response->addCommand(new AlertCommand((string)$this->t('ERROR: The password must have at least 8 characters!')));
                                return $response;
                        }
                        
                        if ( !preg_match( '/[a-zA-Z]/', $password ) ) {
                                $response->addCommand(new AlertCommand((string)$this->t('ERROR: The entered password must contain letters!')));
                                return $response;
                        }
                        
                        if ( !preg_match( '/[0-9]/', $password ) ) {
                                $response->addCommand(new AlertCommand((string)$this->t('ERROR: The entered password must conain numbers!')));
                                return $response;
                        }
                        
                        if ( !preg_match( '/\W/', $password ) ) {
                                $response->addCommand(new AlertCommand((string)$this->t('ERROR: The entered password must conain not-alphanumeric characters!')));
                                return $response;
                        }
                        
                        $setup = Publisso::setup();
                        $session = \Drupal::service( 'session' );
                        
                        if($setup->getValue('system.sso.active') && SSO::ssoAvailable()){
                                $retSSO = SSO::setPassword($user->getElement('zbmed_kennung'), $session->get('user')['password'], $password);
                                if($retSSO->getLastCode() != 200){
                                        Publisso::tools()->logMsg($retSSO->getLastMessage());
                                        $response->addCommand(new AlertCommand((string)$this->t('ERROR: An Error occured while changing your password! Please contact the system-administrator!')));
                                        return $response;
                                }
                        }
                        
                        $user->setPassword( $password );
                        $user->setElement( 'pw_change_required', 0 );
                        $form_state->cleanValues();
                        $user = \Drupal::service( 'session' )->get( 'user' );
                        $user[ 'password' ] = $password;
                        $user['pw_change'] = 0;
                        \Drupal::service( 'session' )->set( 'user', $user );
                        
                        $response->addCommand(new AlertCommand((string)$this->t('Password successfully changed!')));
                        $response->addCommand(new CloseModalDialogCommand());
                        return $response;
                }
        }
