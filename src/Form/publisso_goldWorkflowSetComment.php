<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowSetComment.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldWorkflowSetComment extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldWorkflowSetComment|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldworkflowsetcomment';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        $args = $form_state->getBuildInfo();
        
        if(count($args))
            $wf_id = $args['args'][0]['wf_id'];
        
        if(!$wf_id)
            $wf_id = $form_state->get('wf_id');
        
        if($wf_id){
            
            $form_state->set('wf_id', $wf_id);
            
            //get the workflow-item
            $workflow = getDashboard(\Drupal::Database(), $wf_id);
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            
            $reviewers = getUsersByRole(\Drupal::database(), [3, 4, 5, 6, 7]);
            
            $form = [
               
                'comment' => [
                    '#type' => 'textarea',
                    '#title' => (string)t('Comment')
                ],
                        
                'submit' => [
                    '#type' => 'submit',
                    '#value' => (string)t('Save comment')
                ]
            ];
        }
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $wf_id = $form_state->get('wf_id');
        $comment = $form_state->getValue('comment');
        
        if($comment != ''){
            
            $comment = base64_encode($comment);
            
            \Drupal::database()
                ->insert('rwPubgoldWorkflowComments')
                ->fields([
                    'wfc_created_by_uid' => \Drupal::service('session')->get('user')['id'],
                    'wfc_comment' => $comment,
                    'wfc_wfid' => $wf_id
                ])
                ->execute();
            
            drupal_set_message((string)t('Comment successfuly saved!'), 'status');
        }
        
        return $form;
    }
        
        /**
         * @param null $tmpl
         * @param null $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl = NULL, $vars = NULL){
        
        if($tmpl == NULL || $vars == NULL){
            //set Site-Vars
            $this->tmpl = str_replace(array_keys($this->tmpl_vars), array_values($this->tmpl_vars), $this->tmpl);
            
            //remove unused vars
            $this->tmpl = preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl);
        }
        else{
            //set Site-Vars
            $tmpl = str_replace(array_keys($vars), array_values($vars), $tmpl);
            
            //remove unused vars
            return preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $tmpl);
        }
    }
}
