<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldTableOfContentItemEdit.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldTableOfContentItemEdit extends FormBase {
    
    private $modname = 'publisso_gold';
    private $modpath;
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldtableofcontentitemedit';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        if($this->modname && !$form_state->get('modname')){
            $form_state->set('modname', $this->modname);
        }
        
        if(!$this->modname && $form_state->get('modname')){
            $this->modname = $form_state->get('modname');
        }
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        if(!$form_state->get('maxSteps')){
                $form_state->set('maxSteps', [
                        'bookchapter' => 2,
                        'journalvolume' => 2,
                        'journalissue' => 3,
                        'journalarticle' => 4
                ]);
        }
        
        if(!$form_state->get('step'))
                $form_state->set('step', 0);
        
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        $args = $form_state->getBuildInfo();
        
        if(!$form_state->get('toc_item'))
                $form_state->set('toc_item', new \Drupal\publisso_gold\Controller\TableOfContentItem($args['args'][0]['toci_id']));
        
        $toc_item = $form_state->get('toc_item');
        
        $form['headline'] = [
            '#type' => 'markup',
            '#markup' => '<h2>'.((string)t('Edit table of content item')).'</h2>'
        ];
        
        if(empty($toc_item->getElement('link')) || substr($toc_item->getElement('link'), 0, 6) == 'intern'){
            
            if(empty($toc_item->getElement('link'))){
                
                $form['assign'] = [
                    '#tree' => true,
                    '#type' => 'fieldset',
                    '#collapsible' => true,
                    '#title' => (string)t('Assign to medium')
                ];
                
                switch($form_state->get('step')){
                    
                    case 0:
                        $form['assign']['medium'] = [
                            '#type' => 'select',
                            '#options' => [
                                'bookchapter' => (string)t('Bookchapter'),
                                'journalarticle' => (string)t('Journalarticle'),
                                'journalvolume' => (string)t('Journalvolume'),
                                'journalissue' => (string)t('Journalissue'),
                                'conferencepaper' => (string)t('Conferencepaper')
                            ]
                        ];
                        break;
                    
                    case 1:
                        
                        switch($form_state->get('medium')){
                            
                            case 'bookchapter':
                                $form['assign']['medium'] = [
                                    '#type' => 'select',
                                    '#options' => $this->getBooks()
                                ];
                                break;
                            
                            case 'journalvolume':
                            case 'journalissue':
                            case 'journalarticle':
                                $journals = [];
                                
                                foreach(\Drupal::service('publisso_gold.tools')->getJournals() as $k => $v){
                                        $journals["j:$k"] = $v;
                                }
                                $form['assign']['medium'] = [
                                        '#type' => 'select',
                                        '#options' => $journals
                                ];
                                break;
                                
                            case 'conferencepaper':
                                break;
                        }
                        
                        break;
                    
                    default:
                        
                                $medium = explode(':', $form_state->get('medium'));
                                
                                switch($medium[0]){
                                        
                                        case 'j':
                                                $volumes = [];
                                                
                                                foreach(\Drupal::service('publisso_gold.tools')->getJournalVolumes($medium[1]) as $k => $v)
                                                        $volumes["v:$k"] = $v;
                                                
                                                $form['assign']['medium'] = [
                                                        '#type' => 'select',
                                                        '#options' => $volumes
                                                ];
                                                break;
                                
                                        case 'b':
                                        
                                                $form['assign']['medium'] = [
                                                '#type' => 'select',
                                                '#options' => $this->getBookchapters($medium[1])
                                                ];
                                                break;
                                        
                                        case 'v':
                                                
                                                $maxSteps = $form_state->get('maxSteps');
                                                $maxSteps['journalarticle'] = 4;
                                                
                                                $issues = [];
                                                foreach(\Drupal::service('publisso_gold.tools')->getVolumeIssues($medium[1]) as $k => $v)
                                                        $issues["i:$k"] = "Issue #$v";
                                        
                                                $form['assign']['medium'] = [
                                                        '#type' => 'select',
                                                        '#options' => $issues
                                                ];
                                                
                                                if(!count($issues)){ //get articles
                                                        
                                                        
                                                        $maxSteps['journalarticle'] = 3;
                                                        $articles = [];
                                                        foreach(\Drupal::service('publisso_gold.tools')->getVolumeArticles($medium[1]) as $k => $v){
                                                                $articles["a:$k"] = $v;
                                                        }
                                                        
                                                        $form['assign']['medium'] = [
                                                                '#type' => 'select',
                                                                '#options' => $articles
                                                        ];
                                                }
                                                
                                                $form_state->set('maxSteps', $maxSteps);
                                                
                                                break;
                                        
                                        case 'i':
                                                        $articles = [];
                                                        foreach(\Drupal::service('publisso_gold.tools')->getIssueArticles($medium[1]) as $k => $v){
                                                                $articles["a:$k"] = $v;
                                                        }
                                                        
                                                        $form['assign']['medium'] = [
                                                                '#type' => 'select',
                                                                '#options' => $articles
                                                        ];
                                                break;
                                        
                                }
                                break;
                }
                
                if($form_state->get('step') > 0){
                    $form['assign']['back'] = [
                        '#type' => 'submit',
                        '#value' => (string)t('Back'),
                        '#submit' => ['::back'],
                        '#suffix' => '&nbsp;&nbsp;&nbsp;'
                    ];
                }
                
                
                if($form_state->get('step') == 0 || $form_state->get('step') < $form_state->get('maxSteps')[$form_state->get('assignType')]){
                    $form['assign']['submit'] = [
                        '#type' => 'submit',
                        '#value' => (string)t('Next'),
                        '#submit' => ['::assign']
                    ];
                }
                else{
                    $form['assign']['submit'] = [
                        '#type' => 'submit',
                        '#value' => (string)t('Finish'),
                        '#submit' => ['::finish_assign']
                    ];
                }
            }
            else{
                $form['unassign'] = [
                    '#type' => 'submit',
                    '#value' => (string)t('Unassign'),
                    '#prefix' => (string)t('This Item is assigned to').': '.$toc_item->getElement('link').'<br>',
                    '#submit' => ['::unassign']
                ];
            }
        }
        
        $form['title'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Title'),
            '#prefix' => '<hr>',
            '#default_value' => $toc_item->getElement('title'),
            '#maxlength' => 1000
        ];
        
        $form['description'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Description'),
            '#default_value' => $toc_item->getElement('description')
        ];
        
        $form['authors'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Authors'),
            '#default_value' => $toc_item->getElement('authors'),
            '#maxlength' => 512
        ];
        
        if(empty($toc_item->getElement('link')) || substr($toc_item->getElement('link'), 0, 6) != 'intern'){
            
            $form['link'] = [
                '#type' => 'textfield',
                '#title' => (string)t('external Link'),
                '#description' => (string)t('You can either set an external link or assign this item intern in the section above.'),
                '#default_value' => $toc_item->getElement('link')
            ];
        }
        
        $form['weight'] = [
            '#type' => 'number',
            '#title' => 'weight',
            '#default_value' => $toc_item->getElement('weight'),
            '#size' => 3,
            '#maxlength' => 3
        ];
        
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => (string)t('Save')
        ];
        
        $form['cancel'] = [
            '#type' => 'submit',
            '#value' => (string)t('Back to table of content'),
            '#submit' => ['::cancel'],
            '#prefix' => '<hr>'
        ];
        
        $form['#cache']['max-age'] = 0;
        $form_state->setCached(false);
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function cancel(array &$form, FormStateInterface $form_state){
        
        $form_state->setRedirect('publisso_gold.table_of_content.edit', ['toc_id' => $form_state->get('toc_item')->getElement('tocid')]);
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function assign(array &$form, FormStateInterface $form_state){
        
        $form_state->set('former_medium', $form_state->get('medium'));
        
        if($form_state->get('step') == 0){
                $form_state->set('assignType', $form_state->getValue('assign')['medium']);
        }
        
        /*
        switch($form_state->get('step')){
            
            case 0:
                $assign = $form_state->getValue('assign');
                $form_state->set('medium', $assign['medium']);
                break;
            
            case 1:
                $assign = $form_state->getValue('assign');
                $form_state->set('medium', $assign['medium']);
                break;
        }
        */
        $assign = $form_state->getValue('assign');
        $form_state->set('medium', $assign['medium']);
        
        $form_state->set('step', $form_state->get('step') + 1);
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function finish_assign(array &$form, FormStateInterface $form_state){
        
        $assign = $form_state->getValue('assign');
        $assign = explode(':', $assign['medium']);
        
        switch($assign[0]){
            
                case 'bc':
                        $form_state->get('toc_item')->setElement('link', 'intern://chapter/'.$assign[1]);
                        break;
                
                case 'v':
                        $form_state->get('toc_item')->setElement('link', 'intern://volume/'.$assign[1]);
                        break;
                
                case 'i':
                        $form_state->get('toc_item')->setElement('link', 'intern://issue/'.$assign[1]);
                        break;
                
                case 'a':
                        $form_state->get('toc_item')->setElement('link', 'intern://article/'.$assign[1]);
                        break;
        }
        
        $form_state->set('step', 0);
        
        $toc = new \Drupal\publisso_gold\Controller\TableOfContent($form_state->get('toc_item')->getElement('tocid'));
        $toc->recreateStructure();
        
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function unassign(array &$form, FormStateInterface $form_state){
        $form_state->get('toc_item')->setElement('link', '');
        $form_state->set('step', 0);
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function back(array &$form, FormStateInterface $form_state){
        
        $form_state->set('medium', $form_state->get('former_medium'));
        
        $form_state->set('step', $form_state->get('step') - 1);
        $form_state->setRebuild();
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $form_state->get('step', 0);
        $t = \Drupal::service('publisso_gold.texts');
        
        if($form_state->hasValue('link'))
            $form_state->get('toc_item')->setElement('link',        $form_state->getValue('link'),          true   );
        $form_state->get('toc_item')->setElement('title',       $form_state->getValue('title'),         true   );
        $form_state->get('toc_item')->setElement('description', $form_state->getValue('description'),   true   );
        $form_state->get('toc_item')->setElement('weight',      $form_state->getValue('weight'),        true   );
        $form_state->get('toc_item')->setElement('authors',     $form_state->getValue('authors'),       true   );
        
        $toc = new \Drupal\publisso_gold\Controller\TableOfContent($form_state->get('toc_item')->getElement('tocid'));
        $toc->recreateStructure();
        
        drupal_set_message((string)t($t->get('global.item_saved', 'fc')));
        $form_state->setRedirect('publisso_gold.table_of_content.edit', ['toc_id' => $toc->getElement('id')]);
        
        return $form;
    }
        
        /**
         * @return array
         */
        private function getAvailableMedia(){
        
        $available_media = [];
            
        //getting books
        $assigned_media = [];
        
        $result = \Drupal::database()
            ->select('rwPubgoldTableOfContent', 't')
            ->fields('t', ['toc_medium_id'])
            ->condition('toc_medium', 'book', '=')
            ->condition('toc_medium_id', '0', '>')
            ->execute()
            ->fetchAll();
        
        foreach($result as $_){
            $assigned_media[] = $_->toc_medium_id;
        }
            
        $result = \Drupal::database()->select('rwPubgoldBooks', 'b')->fields('b', []);
        
        if(count($assigned_media))
            $result->condition('bk_id', $assigned_media, 'NOT IN');
        
        $result = $result->execute()->fetchAll();
        
        foreach($result as $_){
            $available_media['books']['b:'.$_->bk_id] = $_->bk_title;
        }
        
        //getting journals
        $assigned_media = [];
        
        $result = \Drupal::database()
            ->select('rwPubgoldTableOfContent', 't')
            ->fields('t', ['toc_medium_id'])
            ->condition('toc_medium', 'journal', '=')
            ->condition('toc_medium_id', '0', '>')
            ->execute()
            ->fetchAll();
        
        foreach($result as $_){
            $assigned_media[] = $_->toc_medium_id;
        }
            
        $result = \Drupal::database()->select('rwPubgoldJournals', 'b')->fields('b', []);
        
        if(count($assigned_media))
            $result->condition('jrn_id', $assigned_media, 'NOT IN');
        
        $result = $result->execute()->fetchAll();
        
        foreach($result as $_){
            $available_media['journal']['j:'.$_->jrn_id] = $_->jrn_title;
        }
        
        //getting conferences
        $assigned_media = [];
        
        $result = \Drupal::database()
            ->select('rwPubgoldTableOfContent', 't')
            ->fields('t', ['toc_medium_id'])
            ->condition('toc_medium', 'conference', '=')
            ->condition('toc_medium_id', '0', '>')
            ->execute()
            ->fetchAll();
        
        foreach($result as $_){
            $assigned_media[] = $_->toc_medium_id;
        }
            
        $result = \Drupal::database()->select('rwPubgoldConferences', 'b')->fields('b', []);
        
        if(count($assigned_media))
            $result->condition('cf_id', $assigned_media, 'NOT IN');
        
        $result = $result->execute()->fetchAll();
        
        foreach($result as $_){
            $available_media['conference']['c:'.$_->cf_id] = $_->cf_title;
        }
        
        return $available_media;
    }
        
        /**
         * @return array
         */
        private function getBooks(){
        
        $ary = [];
        
        foreach(\Drupal::database()
            ->select('rwPubgoldBooks', 't')
            ->fields('t', ['bk_id', 'bk_title'])
            ->execute()
            ->fetchAll() as $_){
                
                $ary['b:'.$_->bk_id] = $_->bk_title;
        }
        
        return $ary;
    }
        
        /**
         * @param $bk_id
         * @return array
         */
        private function getBookchapters($bk_id){
        
        $ary = [];
        
        foreach(\Drupal::database()
            ->select('rwPubgoldBookChapters', 't')
            ->fields('t', ['cp_id', 'cp_title'])
            ->condition('cp_bkid', $bk_id, '=')
            ->isNull('cp_next_version')
            ->execute()
            ->fetchAll() as $_){
                
                $ary['bc:'.$_->cp_id] = $_->cp_title;
        }
        
        return $ary;
    }
}
