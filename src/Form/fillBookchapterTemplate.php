<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\fillBookchapterTemplate.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Database\Connection;
        use Drupal\Core\Url;
        use Drupal\file\Element;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\Workflow\ChapterWorkflow;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Symfony\Component\HttpFoundation\File;
        use Drupal\publisso_gold\Controller\BookchapterTemplate;
        use Symfony\Component\HttpFoundation\Response;

        /**
         * Provides a simple example form.
         */
        class fillBookchapterTemplate extends FormBase {
                
                private $modname = 'publisso_gold';
                private $database;
                private $modpath;
                
                public function __construct () {
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'fillBookchapterTemplate';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state, $args = [] ) {
                        
                        $texts = \Drupal::service('publisso_gold.texts');
                        $form_state->setCached(false);
                        
                        if(!$form_state->has('args')) $form_state->set('args', $args);
                        if ( !$form_state->get( 'templateID' ) ) $form_state->set( 'templateID', $args[ 'templateID' ] );
                        
                        $session = \Drupal::service( 'session' );
                        
                        //load template
                        $template = new BookchapterTemplate( null, $form_state->get( 'templateID' ) );
        
                        $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold.'.$this->getFormId().'#'.$form_state->get( 'templateID' ));
                        $formdata = $tempstore->get('formdata');
                        
                        
                        if ( !$template->getTemplateArticle() ) {
                                
                                if ( !$template->createTemplateArticle() ) {
                                        return $form;
                                }
                        }
                        
                        $form['#tree'] = false;
                        define('APACHE_MIME_TYPES_URL','http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types');
                        
                        $form['hint'] = [
                                '#markup' => (string)$this->t($texts->get('form.bookchaptertemplate.hint')),
                                '#suffix' => '<hr>'
                        ];
                        
                        $form['reflink'] = [
                                '#type' => 'link',
                                '#title' => (string)$this->t('Add references'),
                                '#url' => \Drupal\Core\Url::fromRoute('publisso_gold.book.template', ['id' =>
                                                                                                              $template->ID(), 'action' => 'references']),
                                '#suffix' => '<br><br>',
                                '#attributes' => [
                                        'class' => [
                                                'btn',
                                                'btn-warning'
                                        ]
                                ]
                        ];
                        
                        $filter = $template->getStoredDataFilter($template->getTemplateArticle());
                        if(count($filter)){
                                
                                $form['filter'] = [
                                        '#type' => 'details',
                                        '#title' => (string)$this->t('Load former data')
                                ];
                                
                                $rName = \Drupal::routeMatch()->getRouteName();
                                $param = \Drupal::routeMatch()->getParameters()->all();
                                
                                $i = 0;
                                foreach($filter as $name => $_){
                                        
                                        $url = new Url($rName, array_merge($param, $_));
                                        $form['filter'][$i++] = [
                                                '#type'         => 'link',
                                                '#url'          => $url,
                                                '#title'        => $name,
                                                '#suffix'       => '<br>'
                                        ];
                                        
                                }
                        }
                        
                        $cachedForm = $template->getCachedForm();
                        
                        $form[ 'template' ] = !$cachedForm || $cachedForm ? $template->getFormArticle( $template->getTemplateArticle(), $args, $formdata ?? null) : $cachedForm;
                        if(!$cachedForm) $template->setCachedForm($form['template']);
                        
                        #print_r($form['template']);
                        if($form['template']['ctrl#has_workflow']['#value'] == 1){
                                
                                #$workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($form['template']['ctrl#wf_id']['#value']);
                                $workflow = WorkflowManager::getItem($form['template']['ctrl#wf_id']['#value']);
                                if(!($workflow->getElement('state') != 'published' && $workflow->userIsAssigned())) {
                                        
                                        $wfKeys = [];
        
                                        foreach ( $workflow->getKeys() as $key ) {
                                                $wfKeys[ 'workflow' ][ 'wf_' . $key ] = $workflow->getElement( $key );
                                        }
        
                                        $wfKeys[ 'workflow' ][ 'wf_id' ] = $workflow->getElement( 'id' );
                                        $form_state->set( 'args', array_merge( $form_state->get( 'args' ) ?? [], $wfKeys ) );
        
                                        $form[ 'hint_wf_exists' ] = [
                                                '#markup' => (string)$this->t( 'There is already a submission for this version of the data collection. This must first be completed before data can be entered again. Changes to the data are not saved.' ),
                                                '#prefix' => '<div class="alert alert-danger">',
                                                '#suffix' => '</div>'
                                        ];
                                }
                        }
        
                        $form[ 'submit' ] = [
                                '#type'   => 'submit',
                                '#value'  => (string)t( 'Save' ),
                                '#prefix' => '<br>',
                                '#suffix' => '&nbsp;&nbsp;&nbsp;'
                        ];
                        
                        if(!$form_state->has('wf_id')) { //temporary deactivated, Todo: activate button
        
                                $form[ 'create_workflow' ] = [
                                        '#type'   => 'submit',
                                        '#value'  => (string)t( 'Continue' ),
                                        '#suffix' => '<br><br><br>',
                                        '#attributes' => [
                                                'class' => [
                                                        'btn-primary'
                                                ]
                                        ],
                                        '#submit' => ['::createWorkflow']
                                ];
                        }

                        $form[ '#cache' ][ 'max-age' ] = 0;
                        //verhindert, dass der Browser "mitdenkt" und die Felder nach Gutdünken füllt - anderenfalls funktionieren z.B. vorab ausgewählte Elemente im Multiple-Select (z.B. im FF) nicht
                        $form['#attributes']['autocomplete'] = 'off';

                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array|BookchapterTemplate
                 */
                private function saveForm(array &$form, FormStateInterface $form_state){
        
                        $tools = \Drupal::service('publisso_gold.tools');
                        $template = new BookchapterTemplate( null, $form_state->get( 'templateID' ) );
                        $xml = new \DOMDocument();
                        $xml->loadXML($template->getTemplateArticle());
                        $xpath = new \DOMXPath($xml);
        
                        $versioningFields = [];
                        foreach($xpath->evaluate('/root/metas/meta[@id="versioning-fields"]/fields/field') as $_){
                                $versioningFields[] = $_->getAttribute('field-id');
                        }
        
                        foreach ( $form_state->getValues() as $k => $v ) {
                                $values[$k] = $v;
                        }
        
                        foreach($versioningFields as $_){
                                $attr[$_] = $values[$_];
                        }
        
                        $version = $form_state->getValue('ctrl#version');
                        $createVersion = $form_state->getValue('ctrl#create_version');
                        $versioningFields = $form_state->getValue('ctrl#versioning-fields');
        
                        $filter = [];
                        foreach($versioningFields as $field){
                                $filter[$field] = $form_state->getValue($field);
                        }
                        
                        $references = [];
                        
                        foreach($form_state->getValues() as $k => $v){
                
                                $valueNode = $template->getDataValueNode($xml, $k, $filter, $template->getNextDataVersion($xml->saveXML(), $filter));
                
                                if($valueNode) {
                                        
                                        if($valueNode->hasAttribute('wf_id')){
                                                
                                                $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($valueNode->getAttribute('wf_id'));
                                                
                                                if(!($workflow->getElement('state') != 'published' && $workflow->userIsAssigned())) {
                                                        
                                                        \Drupal::service( 'messenger' )->addError( (string)$this->t('There is already a submission for this version of the data collection. This must first be completed before data can be entered again. Changes to the data are not saved.') );
                                                        return $form;
                                                }
                                        }
                                        
                                        $nl = [];
                                        foreach($valueNode->childNodes as $_) $nl[] = $_;
                                        foreach($nl as $_) $_->parentNode->removeChild($_);

                                        $varNode = $valueNode->parentNode->parentNode->parentNode;
                                        
                                        if($varNode->getAttribute('id') == 'country'){
                                                
                                                foreach(\Drupal::service('country_manager')->getList() as $_k => $_v)
                                                        $countries[(string)$_v] = $_k;
                                                
                                                $imgPath = '/'.\Drupal::service('module_handler')->getModule('publisso_gold')->getPath().'/images/cflags/';
                                                $file = strtolower($countries[$v]).'.png';
                                                $imgPath .= is_file(DRUPAL_ROOT.$imgPath.$file) ? $file : 'dummy.png';
                                                unset($file);
                                        }
                                        
                                        if($varNode->hasAttribute('valueseparator')){

                                                $valueseparator = $varNode->getAttribute('valueseparator');
                                                if($valueseparator == "EOL") $valueseparator = PHP_EOL;

                                                if(!empty($valueseparator)) {

                                                        $v = explode($valueseparator, $v);
                                                }
                                        }
                                        
                                        if(is_string($v)) $v = [$v];
                                        $v = array_filter(array_map("trim", $v));
                                        
                                        foreach($v as $_){
                                                
                                                $entryAttribs = [];
                                                
                                                if(isset($imgPath) && $imgPath){
                                                        $entryAttribs['iconpath'] = $imgPath;
                                                        unset($imgPath);
                                                }
                                                
                                                $entryNode = \Drupal::service('publisso_gold.tools')->DOMAppendChild('entry', '', $entryAttribs, $xml, $valueNode);
                                                \Drupal::service('publisso_gold.tools')->appendCdata($xml, $entryNode, $_);
                                        }

                                        $fieldID = $valueNode->parentNode->parentNode->parentNode->getAttribute('id');
                        
                                        if($form_state->hasValue($fieldID."_references")){
                                
                                                $refNode = \Drupal::service('publisso_gold.tools')->DOMAppendChild('references', '', [], $xml, $valueNode);
                                
                                                foreach($form_state->getValue($fieldID."_references") as $_){
                                                        
                                                        if(!in_array($_, $references)){
                                                                $references[] = $_;
                                                        }
                                                        
                                                        //suche die reference-Node
                                                        $_refNodeNL = $xpath->evaluate('/root/references/reference[@md5="'.$_.'"]');
                                                        if($_refNodeNL->length) $_refNode = $_refNodeNL->item(0);
                                                        
                                                        $_refNo = array_search($_, $references) + 1;
                                                        $_refNode->setAttribute('no', $_refNo);
                                                        
                                                        \Drupal::service('publisso_gold.tools')->DOMAppendChild('reference', $_, ['no' => $_refNo], $xml, $refNode);
                                                        
                                                        unset($_refNodeNL);
                                                        unset($_refNode);
                                                        unset($_refNo);
                                                }
                                        }
                                }
                        }
        
                        //suche Max-Referenz-Nummer
                        $max = 0;
        
                        if($node = $xpath->evaluate('/root/references/reference/@no[not(. < ../../reference/@no)]')){ //$node = DOMAttributeList
                                $max = $node->item(0)->nodeValue;
                        }
                        
                        foreach($xpath->evaluate('/root/references/reference') as $node){
                                
                                if(!in_array($node->getAttribute('md5'), $references)){
                                        $node->setAttribute('no', ++$max);
                                }
                        }
                        
                        //suche alle Reference-Nodes ohne Nummer
                        /*
                        $nl = $xpath->evaluate('/root/references/reference[not(@no)]');
                        if($nl->length){
                                //suche Max-Referenz-Nummer
                                $max = 0;
                                
                                if($node = $xpath->evaluate('/root/references/reference/@no[not(. < ../../reference/@no)]')){ //$node = DOMAttributeList
                                        $max = $node->item(0)->nodeValue;
                                }
                                
                                foreach($nl as $node){
                                        $node->setAttribute('no', ++$max);
                                }
                        }
                        */
                        $template->saveTemplateArticle($xml->saveXML());
                        $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold.'.$this->getFormId().'#'.$form_state->get( 'templateID' ));
                        $tempstore->delete('formdata');
                        return $template;
                }
        
                /**
                 * {@inheritdoc}
                 */
                public function createWorkflow ( array &$form, FormStateInterface $form_state) {
                        
                        #print_r(array_keys($form_state->getValues())); exit;
                        
                        $template = $this->saveForm($form, $form_state);
                        
                        if(is_array($template)){ return $form; } //if $form comes back, an error occured
                        
                        $xml = $template->getTemplateArticle();
                        
                        $doc = new \DOMDocument();
                        $doc->loadXML($xml);
                        
                        $versioningFields = $form_state->getValue('ctrl#versioning-fields');
        
                        $filter = [];
                        foreach($versioningFields as $field){
                                $filter[$field] = $form_state->getValue($field);
                        }
                        
                        $version = $template->getMaxDataVersion($xml, $filter);
                        $workflow = null;
                        
                        foreach($form_state->getValues() as $k => $v) {
                                
                                $valueNode = $template->getDataValueNode( $doc, $k, $filter, $template->getNextDataVersion( $xml, $filter ) );
                                
                                if($valueNode){
                                        
                                        if($valueNode->hasAttribute('wf_id')){
                                                $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($valueNode->getAttribute('wf_id'));
                                                break;
                                        }
                                        else{
                                                if($workflow === null) {
                                                        $workflow = new ChapterWorkflow();
                                                        $workflow->init( $template->getElement( 'bk_id' ), 'bcs', 1 );
                                                        $workflow->setDataElement( 'predefined_maintext', true );
                                                        $workflow->setDataElement('bookchapter_template', $template->getElement('id'));
                                                }
                                                
                                                $valueNode->setAttribute('wf_id', $workflow->getElement('id'));
                                                
                                        }
                                }
                        }
        
                        $workflow->setDataElement('predefined_title', true);
                        $workflow->setDataElement('title', implode(' ', $filter));
                        $workflow->setDataElement('title_short', implode(' ', $filter));
                        
                        $template->saveTemplateArticle($doc->saveXML());
                        
                        $xsl = new \DOMDocument();
                        $xsl->loadXML($template->getElement('xsl'));
                        $xpath = new \DOMXPath($doc);
                        $proc = new \XSLTProcessor();
                        $proc->importStylesheet($xsl);
                        
                        $proc->setParameter('','version', $version);
                        
                        foreach($filter as $k => $v){
                                $proc->setParameter('', $k, $v);
                        }
                        
                        $doc = $proc->transformToDoc($doc);
                        $references = [];
                        
                        foreach($xpath->evaluate('/root/references/reference') as $node){
                                $references[$node->getAttribute('no')] = $node->nodeValue;
                        }
                        ksort($references);
                        
                        $refString = '';
                        foreach($references as $k => $v){
                                $refString .= (!empty($refString) ? "\n" : '')."[$k] $v";
                        }
                        
                        if(!empty($refString)) {
                                $workflow->setDataElement( 'references', $refString );
                                $workflow->setDataElement('predefined_references', true);
                        }
                        $workflow->setDataElement('chapter_text', (preg_replace('/\n/m', '', $doc->saveHTML($doc->documentElement))));
                        
                        if($workflow->getElement('state') != 'running submission'){
                                $workflow->setDataElement('chapter_text', base64_encode($workflow->getDataElement('chapter_text')));
                        }
                        
                        $form_state->setRedirect( 'publisso_gold.workflow.item', ['wf_id' => $workflow->getElement('id')]);
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state) {
                        
                        $this->saveForm($form, $form_state);
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function autosave(array &$form, FormStateInterface $form_state){
                        
                        $versioningFields = $form_state->getValue('ctrl#versioning-fields');
                        
                        $te = $form_state->getTriggeringElement();

                        $attributes = $te['#attributes'];
                        $field = $te['#parents'][0];
                        $value = $te['#value'];
                        
                        if(in_array($field, $versioningFields)){
                                $form_state->set('versioningFieldsModified', true);
                        }
                        
                        $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold.'.$this->getFormId().'#'.$form_state->get( 'templateID' ));
                        
                        if(null === ($formdata = $tempstore->get('formdata'))) {
                                $tempstore->set('formdata', []);
                        }
        
                        $formdata = $tempstore->get('formdata');
                        $formdata[$field] = $value;
                        $tempstore->set('formdata', $formdata);
                        
                        $form_state->setRebuild();
                        return $form;
                }
        }
