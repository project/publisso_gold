<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldReviewPage.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use \Drupal\publisso_gold\Controller\WorkflowInfoMail;

/**
 * Provides a simple example form.
 */
class publisso_goldReviewPage extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $num_corporations;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldReviewPage|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldreviewpage';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $session = \Drupal::service('session');
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        $args = $form_state->getBuildInfo();
        
        if(!count($args)){
            
            return [
                '#cache' => [
                    'max-age' => 0
                ],
                
                'error' => [
                    '#type' => 'markup',
                    '#markup' => '<span class="rwAlert">'.t('No review-object given!').'</span>'
                ]
            ];
        }
        
        //prepare data
        foreach($args['args'][0] as $k => $v)
            $$k = $v;
        
        $form_state->set('wf_id', $workflow->wf_id);
        $workflow_data = json_decode(base64_decode($workflow->wf_data));
        $objWorkflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
		
        if(isset($book)){
            $rp_id = $book['bk_rpid'];
            $orig_text = base64_decode($workflow_data->chapter_text);
			$objMedium = new \Drupal\publisso_gold\Controller\Book($objWorkflow->getDataElement('bk_id'));
        }
        
        if(isset($journal)){
            $rp_id = $journal['jrn_rpid'];
            $orig_text = base64_decode($workflow_data->article_text);
			$objMedium = new \Drupal\publisso_gold\Controller\Journal($objWorkflow->getDataElement('jrn_id'));
        }
        
        if(isset($conference)){
            $rp_id = $conference['cf_rpid'];
            $orig_text = base64_decode($workflow_data->paper_text);
			$objMedium = new \Drupal\publisso_gold\Controller\Conference($objWorkflow->getDataElement('cf_id'));
        }
        
        $review_page = getReviewPage($rp_id);
		
        $form_state->set('review_page', $review_page);
        
        //get workflow cache
        $is_wfcache = false;
        $wf_cache = \Drupal::database()
            ->select('rwPubgoldWorkflowCache', 'wc')
            ->fields('wc', [])
            ->condition('wc_uid', $session->get('user')['id'])
            ->condition('wc_wfid', $workflow->wf_id)
            ->execute()
            ->fetchAssoc();
        
        if($wf_cache !== false){
          $is_wfcache = true;
            $wf_cache['wc_data'] = json_decode(base64_decode($wf_cache['wc_data']));
        }
        
        //exists values for review-page?
        /*
        if(isset($workflow_data->review_pages)){
            
            $review_pages = json_decode(base64_decode($workflow_data->review_pages), true);
            
            if(array_key_exists($_SESSION['user']['id'], $review_pages)){
                
                $rp = json_decode(base64_decode($review_pages[$_SESSION['user']['id']]), true);
            }
        }
        */
        $form = [];
        
		$form['title'] = [
			'#type' => 'markup',
			'#markup' => '<h1>'.$workflow_data->title.'</h1>'
		];
		
        if($objWorkflow->getDataElement('abstract')){
        
                $form['abstract'] = [
                '#type' => 'details',
                '#open' => false,
                'content' => [
                    '#markup' => base64_decode($objWorkflow->getDataElement('abstract'))
                ],
                '#title' => (string)t('Abstract')
            ];
        }
		
        if($objMedium->getControlElement('review_change_origtext') == 1){
            
            $form['orig_text'] = [
                '#type' => 'textarea',
                '#default_value' => isset($wf_cache['wc_data']->orig_text) ? $wf_cache['wc_data']->orig_text : $orig_text,
                '#title' => t('original text from author'),
				'#attributes' => [
					'class' => [
						\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
					]
				]
            ];
        }
        else{
            
            $form['orig_text'] = [
                '#type' => 'details',
                '#open' => false,
                'content' => [
                    '#type' => 'inline_template',
                    '#template' => $orig_text
                ],
                '#title' => t('Original text from author'),
                '#description' => t('
                    This is the original Version of the text. Direct editing is not possible.
                '),
            ];
        }
        
        $form['references'] = [
                '#type'  => 'details',
                '#title' => (string)t('References'),
                'content'   => [
                        '#markup' => nl2br($objWorkflow->getDataElement('references'))
                ],
                '#prefix'=> '<br>'
        ];
        
        $form['files'] = $this->FormElement_WorfklowFiles($objWorkflow);
        
        if(isset($workflow_data->review_pages)){
          $form['review_history'] = renderReviewPageForm($workflow_data->review_pages, $session->get('user')['id']);
          $form['review_history']['#prefix'] = '<br>';
        }
        
        $form['review'] = [
            '#type' => 'details',
            '#title' => (string)t('Reviewpage'),
            '#open' => true,
            'content' => [],
            '#prefix' => '<br>'
        ];
        
        $c = 0;
        
        foreach($review_page as $text => $type){
            
            if($is_wfcache === true){
                
                if($type == 'textarea'){
                    $value = $wf_cache['wc_data']->$c;
                }
                elseif($type == 'checkbox'){
                    $value = $wf_cache['wc_data']->$c;
                }
            }
            
            $element['rp_'.$c][$c] = [
                '#type' => $type,
                '#title' => $text
            ];
            
            
            
            if(isset($value))
                $element['rp_'.$c][$c]['#default_value'] = $value;
            
            $form['review']['content'][] = $element['rp_'.$c];
            
            $c++;
        }
        
        $form['review']['content']['comment_for_author'] = [
            '#type' => 'textarea',
            '#title' => (string)t('Comment for author')
        ];
        
        if($is_wfcache){
          $form['review']['content']['comment_for_author']['#default_value'] = $wf_cache['wc_data']->comment_for_author;
        }
        
        $form['review']['content']['recommendation'] = [
            '#type' => 'select',
            '#title' => (string)t('Recommendation'),
            '#required' => true,
            '#prefix' => '<br>',
            '#options' => [
                'reject' => (string)t('Rejected in the present form'),
                'accept' => (string)t('Accepted in the present form'),
                'revision' => (string)('Revision neccessary')
            ]
        ];
        
        if($is_wfcache){
          $form['review']['content']['recommendation']['#default_value'] = $wf_cache['wc_data']->recommendation;
        }
        
        $form['review']['content']['reject_review'] = [
            '#type' => 'submit',
            '#value' => (string)t('Reject review'),
            '#prefix' => '<br><div style="float: left;">',
            '#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
            '#submit' => ['::rejectReview'],
            '#attributes' => array('onclick' => 'if(!confirm("'.t('This rejects the review-request.\nEditors will be informed!\nThis action can not be undone!\n\nOK: Reject review\nCancel: Continue review').'")){return false;}'),
            '#validate' => ['::validateReject'],
            '#limit_validation_errors' => []
        ];
        
        $form['review']['content']['back'] = [
            '#type' => 'submit',
            '#value' => (string)t('Back & close item'),
            '#submit' => ['::back'],
            '#limit_validation_errors' => []
        ];
        
        $form['review']['content']['save'] = [
            '#type' => 'submit',
            '#value' => (string)t('Save'),
            '#prefix' => '&nbsp;&nbsp;'
        ];
        
        $form['review']['content']['finish'] = [
            '#type' => 'submit',
            '#value' => t('Finish review'),
            '#submit' => ['::finishReview'],
            '#prefix' => '&nbsp;&nbsp;',
            '#validate' => ['::validateFinish'],
            '#attributes' => array('onclick' => 'if(!confirm("'.t('This complete your review. This action can not be undone!\n\nOK: Finish review\nCancel: Continue review').'")){return false;}')
        ];
	
        $form['#attached'] = [
			'library' => [
				'publisso_gold/default'
			]
		];
		
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function validateReject(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function back(array &$form, FormStateInterface $form_state) {
        
        $session = \Drupal::service('session');
		$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
		
        //save orig-text 
        if($form_state->hasValue('orig_text')){
            
            //get workflow for history
            $workflow = getDashboard(\Drupal::database(), $form_state->get('wf_id'));
            $workflow = $workflow[0];
            
            //prepare workflow for history
            $workflow_history = array();
            $workflow_history['wfh_created_by_uid'] = $session->get('user')['id'];
            
            foreach($workflow as $k => $v){
                
                if($k == 'wf_id'){
                    $key = 'wfh_wfid';
                }
                else{
                    
                    $key = explode("_", $k, 2);
                    $key = $key[0].'h_'.$key[1];
                    
                }
                
                $workflow_history[$key] = $v;
            }
            
            \Drupal::database()->insert('rwPubgoldWorkflowHistory')->fields($workflow_history)->execute();
            
            //update orig-text
            switch($objWorkflow->getDataElement('type')){
				case 'bookchapter':
					$elem_name = 'chapter_text';
					break;
				
				case 'journalarticle':
					$elem_name = 'article_text';
					break;
				
				case 'conferencepaper':
					$elem_name = 'paper_text';
					break;
			}
			
			$objWorkflow->setDataElement($elem_name, base64_encode($form_state->getValue('orig_text')));
        }
        
        $objWorkflow->setElement('locked', null);
		$objWorkflow->setElement('locked_by_uid', null);
        
        $form_state->setRedirect('publisso_gold.dashboard');
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function validateFinish(array &$form, FormStateInterface $form_state) {
        
        if($form_state->getValue('recommendation') == '')
            $form_state->setErrorByName('recommendation', 'Please select an option!');
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $session = \Drupal::service('session');
        $data = [
            ':wc_data' => base64_encode(json_encode($form_state->getValues())),
            ':wc_uid' => $session->get('user')['id'],
            ':wc_wfid' => $form_state->get('wf_id')
        ];
        
        if($form_state->hasValue('orig_text')){
            
            $workflow = getDashboard(\Drupal::database(), $form_state->get('wf_id'));
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            $chapter_text = json_decode(base64_decode($workflow_data->chapter_text));
        }
        
        $sql = "REPLACE INTO {rwPubgoldWorkflowCache}
            (`wc_data`, `wc_uid`, `wc_wfid`)
            VALUES (:wc_data, :wc_uid, :wc_wfid)";
        
        \Drupal::database()
            ->query($sql, $data);
        
        drupal_set_message((string)t('Data successfully saved!'));
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function finishReview(array $form, FormStateInterface $form_state){
        
        $session = \Drupal::service('session');
        $workflow = getDashboard(\Drupal::database(), $form_state->get('wf_id'));
        $workflow = $workflow[0];
        $workflow_data = json_decode(base64_decode($workflow->wf_data));
		$new_workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
        
        //save comment for author
        $comments = $form_state->getValue('comment_for_author'); 
		
        if(!empty($comments)){
          
          $comment = array(
              'cfa_created_by_uid' => $session->get('user')['id'],
              'cfa_for_uid' => $workflow_data->author_uid ? $workflow_data->author_uid : $new_workflow->getElement('created_by_uid'),
              'cfa_comment' => base64_encode($comments),
              'cfa_wfid' => $form_state->get('wf_id')
          );
          
          \Drupal::database()
              ->insert('rwPubgoldWorkflowCommentsForAuthors')
              ->fields($comment)
              ->execute();
        }
		
        //save current workflow in history
		$new_workflow->historize();
        
        //if changes in orig text
        if($form_state->hasValue('orig_text')){
          
            $orig_text = $form_state->getValue('orig_text');
            
            //change orig text
            
            if($workflow_data->type == 'bookchapter')
              $workflow_data->chapter_text = base64_encode($orig_text);
            
            if($workflow_data->type == 'journalarticle')
              $workflow_data->article_text = base64_encode($orig_text);
            
            if($workflow_data->type == 'conferencepaper')
              $workflow_data->paper_text = base64_encode($orig_text);
        }
        
        //set recommendation
        
        $recommendation = $form_state->getValue('recommendation');

        \Drupal::database()->insert('rwPubgoldWorkflowRecommendations')
                           ->fields([
                                'wf_id' => $form_state->get('wf_id'),
                                'created_by_uid' => $session->get('user')['id'],
                                'recommendation' => $recommendation
                           ])
                           ->execute();
        
        //process review page
        $rp = $form_state->get('review_page');
        $c = 0;
        $new_rp = [];
        
        foreach($rp as $text => $type){
          
          $var = $form_state->getValue($c);
          
          if($type == 'textarea'){
            
            $new_rp[$c]['value'] = $var;
          }
          
          if($type == 'checkbox'){
            $new_rp[$c]['value'] = $var;
          }
          
          $new_rp[$c]['text'] = $text;
          $new_rp[$c]['type'] = $type;
          $c++;
        }
        
        if(isset($workflow_data->review_pages)){
          $old_rp = json_decode(base64_decode($workflow_data->review_pages), true);
          $old_rp[$session->get('user')['id']][] = base64_encode(json_encode($new_rp));
          $workflow_data->review_pages = base64_encode(json_encode($old_rp));
        }
        else{
          $old_rp = [];
          $old_rp[$session->get('user')['id']][] = base64_encode(json_encode($new_rp));
          $workflow_data->review_pages = base64_encode(json_encode($old_rp));
        }
        //set reviewer edited (append to list)
        $reviewers = [$session->get('user')['id']];
        
        if(empty($workflow->wf_reviewer_edited))
          $reviewers_old = [];
        else
          $reviewers_old = explode(',', $workflow->wf_reviewer_edited);
        
        if(!in_array($session->get('user')['id'], $reviewers_old))
          $reviewers = array_merge($reviewers, $reviewers_old);
        
        $workflow->wf_reviewer_edited = implode(',', $reviewers);
        $workflow->wf_data = base64_encode(json_encode($workflow_data));
        
        //delete my workflow-cache
        \Drupal::database()->delete('rwPubgoldWorkflowCache')
                           ->condition('wc_uid', $session->get('user')['id'], '=')
                           ->condition('wc_wfid', $form_state->get('wf_id'), '=')
                           ->execute();
        //save workflow
        unset($workflow->wf_id);
        \Drupal::database()->update('rwPubgoldWorkflow')->fields((array)$workflow)->condition('wf_id', $form_state->get('wf_id'), '=')->execute();
        
		//reload workflow because changes are written
		$new_workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
		
        //remove reviewer from assignment        
		$new_workflow->removeReviewer($session->get('user')['id']);
		
		$vars = [];
		
		foreach($new_workflow->getKeys() as $key){
			$vars["::workflow.$key::"] = $new_workflow->getElement($key);
		}
		
		foreach($new_workflow->getDataKeys() as $key){
			$vars["::workflow.data.$key::"] = $new_workflow->getDataElement($key);
		}
		
    $mail = new WorkflowInfoMail($new_workflow, 'reviewer finished');
    if($mail) {
      $mail->addVars($vars);
      $mail->send();
    }
		//sendWorkflowInfoMail($new_workflow, 'reviewer finished', $vars);
		/*
        $res = \Drupal::database()->query("
          UPDATE {rwPubgoldWorkflow}
            SET
              `wf_assigned_to_reviewer` =
                TRIM(BOTH ',' FROM
                  REPLACE(
                    REPLACE(CONCAT(',',REPLACE(`wf_assigned_to_reviewer`, ',', ',,'), ','),',".$_SESSION['user']['id'].",', ''), ',,', ',')
                )
            WHERE
              FIND_IN_SET('".$_SESSION['user']['id']."', `wf_assigned_to_reviewer`)
        ");
        
        $res = \Drupal::database()->query("
          UPDATE {rwPubgoldWorkflow}
            SET
              `wf_assigned_to` =
                TRIM(BOTH ',' FROM
                  REPLACE(
                    REPLACE(CONCAT(',',REPLACE(`wf_assigned_to`, ',', ',,'), ','),',u:".$_SESSION['user']['id'].",', ''), ',,', ',')
                )
            WHERE
              FIND_IN_SET('u:".$_SESSION['user']['id']."', `wf_assigned_to`)
        ");
        */
        //get remaining reviewers
		/*
        $result = \Drupal::database()
          ->select('rwPubgoldWorkflow', 'wf')
          ->fields('wf', [])
          ->condition('wf_id', $form_state->get('wf_id'), '=')
          ->execute()
          ->fetchAssoc();
        
        if($result['wf_assigned_to'] == ''){ //all reviewers finished -> set new status and give back to editor
         */
		//echo '<pre>'.print_r($new_workflow, 1).'</pre>'; exit();
		if(!count($new_workflow->readReviewers())){
			
			$new_workflow->setDataElement('state', 'review finished');
			$new_workflow->setElement('state', 'review finished');
			$new_workflow->setElement('assigned_to', 'u:'.implode(',u:', explode(',', $new_workflow->getElement('assigned_to_editor'))));
      
      $mail = new WorkflowInfoMail($new_workflow, 'review finished');
      if($mail) $mail->send();
			//sendWorkflowInfoMail($new_workflow, 'review finished');
			/*
            //get workflow 
            $workflow = \Drupal::database()->select('rwPubgoldWorkflow', 'wf')
              ->fields('wf', [])->condition('wf_id', $form_state->get('wf_id'), '=')
              ->execute()->fetchAll();
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            
            //update workflow data
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            $workflow_data->state = 'review finished';
            $workflow->wf_data = base64_encode(json_encode($workflow_data));
            $workflow->wf_assigned_to = 'u:'.implode(',u:', explode(',', $workflow->wf_assigned_to_editor));
            
            //save workflow
            unset($workflow->wf_id);
            \Drupal::database()->update('rwPubgoldWorkflow')->fields((array)$workflow)->condition('wf_id', $form_state->get('wf_id'), '=')->execute();
			*/
        }
        /*
        //unlock workflow
        $sql = "
            update 
                rwpg_rwPubgoldWorkflow 
            set
                wf_locked = 0,
                wf_locked_by_uid = NULL
            where 
                wf_id = :wf_id
        ";
        
        $data = [
            ':wf_id' => $form_state->get('wf_id')
        ];
        
        \Drupal::database()->query($sql, $data);
        */
		$new_workflow->unlock();
        $form_state->setRedirect('publisso_gold.dashboard');
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function finish(array &$form, FormStateInterface $form_state) {
        
        $session = \Drupal::service('session');
        $schema = $form_state->get('review_page');
        $review_page = [];
        
        $c = 0;
        
        foreach($schema as $text => $type){
            
            $review_page[$c]['text' ] = $text;
            $review_page[$c]['type' ] = $type;
            $review_page[$c]['value'] = $form_state->getValue($c);
            $c++;
        }
        
        $workflow = getDashboard(\Drupal::database(), $form_state->get('wf_id'));
        $workflow = $workflow[0];
        $workflow_data = json_decode(base64_decode($workflow->wf_data));
        
        if(!isset($workflow_data->review_pages))
            $workflow_data->review_pages = [];
        else
            $review_pages = json_decode(base64_decode($workflow_data->review_pages), true);
        
        $review_pages[$session->get('user')['id']][] = base64_encode(json_encode($review_page));
        $workflow_data->review_pages = base64_encode(json_encode($review_pages));
        
        \Drupal::Database()
            ->update('rwPubgoldWorkflow')
            ->fields([
                'wf_data' => base64_encode(json_encode($workflow_data))
            ])
            ->condition('wf_id', $form_state->get('wf_id'), '=')
            ->execute();
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function rejectReview(array &$form, FormStateInterface $form_state){
        
        $session = \Drupal::service('session');
        $wf_id = $form_state->get('wf_id');
        $workflow = getDashboard(\Drupal::database(), $wf_id);
        $workflow = $workflow[0];
        $workflow_data = json_decode(base64_decode($workflow->wf_data));
        $objWorkflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
		
		
		//remove reviewer
		$objWorkflow->removeReviewer($session->get('user')['id']);
		
		//info mail
		$vars = [];
		$sender = new \Drupal\publisso_gold\Controller\User($session->get('user')['id']);
		
		foreach($sender->getElementKeys() as $key){
			$vars["::initiator.$key::"] = $sender->getElement($key);
		}
		
		foreach($sender->profile->getElementKeys() as $key){
			$vars["::initiator.profile.$key::"] = $sender->profile->getElement($key);
		}
		
    $mail = new WorkflowInfoMail($objWorkflow, 'reviewer rejected');
    if($mail) {
      
      $mail->addVars($vars);
      $mail->send();
    }
		//sendWorkflowInfoMail($objWorkflow, 'reviewer rejected', $vars);
		
		if(!count($objWorkflow->readReviewers())){
			$objWorkflow->setElement('assigned_to', 'u:'.implode(',u:', explode(',', $objWorkflow->getElement('assigned_to_editor'))));
			$objWorkflow->setDataElement('state', 'review finished');
			$objWorkflow->setElement('state', 'review finished');
      
      $mail = new WorkflowInfoMail($objWorkflow, 'review finished');
      if($mail) $mail->send();
			//sendWorkflowInfoMail($objWorkflow, 'review finished');
		}
		
		$objWorkflow->unlock();
		
        $form_state->setRedirect('publisso_gold.dashboard');
        
        return $form;
    }
        
        /**
         * @param $workflow
         * @return array
         */
        private function FormElement_WorfklowFiles(&$workflow){
        
                $files_tmpl = [];
                $fids = $workflow->getDataElement('files');
                if($fids) $fids = unserialize($fids);
                
                if(is_array($fids)){
                        
                        foreach($fids as $fid){
                                
                                $blob = new \Drupal\publisso_gold\Controller\Blob($fid);
                                
                                $files_tmpl[] = [
                                        '#type' => 'fieldset',
                                        'content' => [
                                                '#type' => 'link',
                                                '#title' => $blob->meta['name'],
                                                '#url' => Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()]),
                                                '#suffix' => '<br><i>'.$blob->meta['description'].'</i>'
                                        ],
                                        'preview' => preg_match('/^(image|video|audio)\//', $blob->type, $matches) ? [
                                                '#type' => 'inline_template',
                                                '#template' => (
                                                        $matches[1] == 'image' ? '<br><img height="100" src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'">' : (
                                                                $matches[1] == 'video' ? '<br><video height="100" controls><source src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'" type="'.$blob->type.'"></video>' : (
                                                                        $matches[1] == 'audio' ? '<br><audio controls><source src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'" type="'.$blob->type.'"></audio>' : (
                                                                                ''
                                                                        )
                                                                )
                                                        )
                                                )
                                        ] : ''
                                ];
                        }
                }
                
                if(count($files_tmpl)){
                        return [
                            '#type' => 'details',
                            '#title' => t('Assigned files'),
                            '#open' => false,
                            'content' => $files_tmpl,
                            '#prefix' => '<br>'
                        ];
                }
                else{
                        return [];
                }
        }
}
