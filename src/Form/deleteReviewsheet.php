<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\deleteReviewsheet.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;
use Drupal\publisso_gold\Controller\Reviewsheet;

/**
 * Provides a simple example form.
 */
class deleteReviewsheet extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $texts;
    
    public function __construct(){
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'deleteReviewsheet';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
        
        $session        = \Drupal::service('session');
        $tools          = \Drupal::service('publisso_gold.tools');
        $this->texts    = \Drupal::service('publisso_gold.texts');
        
        /**
         * Load the reviewsheet. If no ID delivered or sheet could not be loaded,
         * throw error-drupal_set_message
         */
        if(empty($args['id']) && !$form_state->has('id')){
                return $tools->accessDenied((string)t($texts->get('reviewsheet.edit.no_id', 'fc')));
        }
        
        $form_state->set('id', $args['id']);
        
        $rp = new Reviewsheet($form_state->get('id'));
        
        if($rp->getElement('id') === null){
                return $tools->accessDenied((string)t($texts->get('reviewsheet.edit.not_found', 'fc'), ['@nr' => $form_state->get('id')]));
        }
        //$bootstrap = new \Bootstrap();
        
        $form['actions'] = [
                'back' => [
                        '#type' => 'submit',
                        '#value' => (string)t($this->texts->get('reviewsheet.delete.cancel', 'fco')),
                        '#submit' => ['::back'],
                        '#suffix' => '&nbsp;&nbsp;&nbsp;',/*
                        "#icon" => \Bootstrap::glyphicon('chevron-left'),*/
                        '#button_type' => 'warning'
                ],
                'submit' => [
                        '#type' => 'submit',
                        '#value' => (string)t($this->texts->get('reviewsheet.delete.do', 'fco')),
                        '#button_type' => 'danger',
                        '#suffix' => '<br><br>'
                ]
        ];
        
        $form['#cache']['max-age'] = 0;
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $texts = \Drupal::service('publisso_gold.texts');
        $rp = new Reviewsheet($form_state->get('id'));
        $rp->delete();
        drupal_set_message((string)t($texts->get('reviewsheet.delete.success', 'fc'), ['@id' => $rp->getElement('id'), '@name' => $rp->getElement('name')]));
        $form_state->setRedirect('publisso_gold.reviewsheets');
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function back(array &$form, FormStateInterface $form_state){
        $form_state->setRedirect('publisso_gold.reviewsheets');
    }
}
