<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldConferenceAddPaper.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldConferenceAddPaper extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldConferenceAddPaper|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldconferenceaddpaper';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $this->modpath = drupal_get_path('module', $this->modname);
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
        //expect build-info-args as array, index 0 is bk_id
        $build_info = $form_state->getBuildInfo();
        $args = $build_info['args'];
        
        $form_state->set('cf_id', $args[0]['cf_id']);
        $form_state->set('wf_id', $args[0]['wf_id']);
        
        if(!$form_state->get('cf_id')){
            drupal_set_message(t('No conference given!'), 'error');
            return array();
        }
        
        $conference = getConference(\Drupal::database(), $form_state->get('cf_id'));
        
        if($form_state->get('wf_id')){
            $workflow = getDashboard(\Drupal::Database(), $form_state->get('wf_id'));
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            $paper_text = json_decode(base64_decode($workflow_data->paper_text));
            $abstract = json_decode(base64_decode($workflow_data->abstract));
            
            $delta = ['workflow', 'workflow_data'];
            foreach($delta as $_) $form_state->set($_, $$_);
        }
        
        $form = [
            
            'site_headline' => [
            '#type' => 'markup',
            '#markup' => '<h1>'.t('Add Paper').'</h1>'
            ],
            
            'conference_title' => [
                '#type' => 'markup',
                '#markup' => '<h2>'.$conference['cf_title'].'</h2>'
            ],
            
            'title' => [
                '#type' => 'textfield',
                '#title' => t('Title'),
                '#required' => true,
                '#default_value' => isset($workflow_data->title) ? $workflow_data->title : ''
            ],
            
            'paper_text' => [
                '#type' => 'text_format',
                '#title' => t('Paper Text'),
                '#required' => true,
                '#rows' => 50,
                '#format' => isset($paper_text->format) ? $paper_text->format : 'basic_html',
                '#default_value' => isset($paper_text->value) ? $paper_text->value : ''
            ],
            
            'article_type' => [
                '#type' => 'textfield',
                '#title' => t('Article-type'),
                '#required' => false,
                '#default_value' => isset($workflow_data->article_type) ? $workflow_data->article_type : ''
            ],
            
            'doc_number' => [
                '#type' => 'textfield',
                '#title' => t('docNumber'),
                '#required' => true,
                '#default_value' => isset($workflow_data->doc_number) ? $workflow_data->doc_number : ''
            ],
            
            'license' => [
                '#type' => 'textfield',
                '#title' => t('License'),
                '#required' => true,
                '#default_value' => isset($workflow_data->license) ? $workflow_data->license : ''
            ],
            
            'abstract' => [
                '#type' => 'text_format',
                '#title' => t('Abstract'),
                '#required' => false,
                '#format' => isset($abstract->format) ? $abstract->format : 'basic_html',
                '#default_value' => isset($abstract->value) ? $abstract->value : ''
            ],
            
            'publication_year' => [
                '#type' => 'textfield',
                '#title' => t('Publication Year'),
                '#required' => true,
                '#default_value' => isset($workflow_data->publication_year) ? $workflow_data->publication_year : '' 
            ],
            
            'publication_place' => [
                '#type' => 'textfield',
                '#title' => t('Publication Place'),
                '#required' => true,
                '#default_value' => isset($workflow_data->publication_place) ? $workflow_data->publication_place : '' 
            ],
            
            'publisher' => [
                '#type' => 'textfield',
                '#title' => t('Publisher'),
                '#required' => true,
                '#default_value' => isset($workflow_data->publisher) ? $workflow_data->publisher : '' 
            ],
            
            'doi' => [
                '#type' => 'textfield',
                '#title' => t('DOI'),
                '#required' => true,
                '#default_value' => isset($workflow_data->doi) ? $workflow_data->doi : '' 
            ],
            
            'ddc' => [
                '#type' => 'textfield',
                '#title' => t('DDC'),
                '#required' => true,
                '#default_value' => isset($workflow_data->ddc) ? $workflow_data->ddc : '' 
            ],
            
            'corresponding_author' => [
                '#type' => 'textfield',
                '#title' => t('Corresponding Author'),
                '#required' => false,
                '#default_value' => isset($workflow_data->corresponding_author) ? $workflow_data->corresponding_author : '' 
            ],
            
            'urn' => [
                '#type' => 'textfield',
                '#title' => t('URN'),
                '#default_value' => isset($workflow_data->urn) ? $workflow_data->urn : ''
            ],
            
            'keywords' => [
                '#type' => 'textfield',
                '#title' => t('Keywords'),
                '#default_value' => isset($workflow_data->corresponding_author) ? implode(', ', json_decode($workflow_data->keywords)) : ''
            ],
            
            'funding_name' => [
                '#type' => 'textfield',
                '#title' => t('Funding Name'),
                '#default_value' => isset($workflow_data->funding_name) ? $workflow_data->funding_name : ''
            ],
            
            'funding_id' => [
                '#type' => 'textfield',
                '#title' => t('Funding ID'),
                '#default_value' => isset($workflow_data->funding_id) ? $workflow_data->funding_id : ''
            ],
            
            'presenting_author' => [
                '#type' => 'textfield',
                '#title' => t('Presenting author'),
                '#required' => false,
                '#default_value' => isset($workflow_data->presenting_author) ? $workflow_data->presenting_author : ''
            ],
            
            'session' => [
                '#type' => 'textfield',
                '#title' => t('Session'),
                '#required' => false,
                '#default_value' => isset($workflow_data->session) ? $workflow_data->session : ''
            ],
            
            'version' => [
                '#type' => 'textfield',
                '#title' => t('Version'),
                '#required' => false,
                '#default_value' => isset($workflow_data->version) ? $workflow_data->version : ''
            ],
            
            'submit' => [
                '#type' => 'submit',
                '#value' => t(($form_state->get('wf_id') ? 'Finish revision' : 'Save paper'))
            ]
            
        ];
        
        $form['#cache']['max-age'] = 0;
      
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        //get conference eo
        $eo = getConferenceEditorialOffice(\Drupal::database(), $form_state->get('cf_id'));
        $submission_assign = $eo_str = '';
        
        foreach($eo as $_){
            $submission_assign = (!empty($submission_assign) ? ',' : '').'u:'.$_['up_uid'];
            $eo_str = (!empty($eo_str) ? ',' : '').$_['up_uid'];
        }
        
        if(empty($submission_assign)) //if no eo assigned -> publisso
            $submission_assign = 'r:2';
        
        $abstract               = $form_state->getValue('abstract');
        $abstract['value']      = normalizeInlineImageLinks($abstract['value']);
        
        $paper_text           = $form_state->getValue('paper_text');
        $paper_text['value']  = normalizeInlineImageLinks($paper_text['value']);
        
        foreach($form_state->getValues() as $k => $v)
          $data[$k] = $v;
        
        $data = [
            
            'abstract'               => base64_encode(json_encode($abstract)),
            'paper_text'             => base64_encode(json_encode($paper_text)),
            'author_uid'             => $_SESSION['user']['id'],
            'license'                => $form_state->getValue('license'),
            'article_type'           => $form_state->getValue('article_type'),
            'publication_year'       => $form_state->getValue('publication_year'),
            'publication_place'      => $form_state->getValue('publication_place'),
            'publisher'              => $form_state->getValue('publisher'),
            'doi'                    => $form_state->getValue('doi'),
            'ddc'                    => $form_state->getValue('ddc'),
            'corresponding_author'   => $form_state->getValue('corresponding_author'),
            'urn'                    => $form_state->getValue('urn'),
            'keywords'               => json_encode(preg_split('/[^a-zA-Z0-9 ]+/', $form_state->getValue('keywords'))),
            'funding_name'           => $form_state->getValue('funding_name'),
            'funding_id'             => $form_state->getValue('funding_id'),
            'version'                => $form_state->getValue('version'),
            'cf_id'                  => $form_state->get('cf_id'),
            'title'                  => $form_state->getValue('title'),
            'title_translated'       => $form_state->getValue('title_translated'),
            'volume'                 => $form_state->getValue('volume'),
            'version'                => $form_state->getValue('version'),
            'doc_number'             => $form_state->getValue('doc_number'),
            'presenting_author'      => $form_state->getValue('presenting_author'),
            'session'                => $form_state->getValue('session'),
            'volume_title'           => $form_state->getValue('volume_title'),
            'state'                  => 'submission finished',
            'type'                   => 'conferencepaper'
        ];
        
        if(!$form_state->get('wf_id')){
            
            $fields = [
                    'wf_assigned_to' => $submission_assign,
                    'wf_data' => base64_encode(json_encode($data)),
                    'wf_assigned_to_eo' => $eo_str,
                    'wf_created_by_uid' => $_SESSION['user']['id']
            ];
            
            $wf_id = \Drupal::database()
                ->insert('rwPubgoldWorkflow')
                ->fields($fields)
                ->execute();
        }
        else{
            
            $delta = ['workflow', 'workflow_data'];
            foreach($delta as $_) $$_ = $form_state->get($_);
            
            $data['state'] = 'revision finished';
            $submission_assign = 'u:'.$workflow->wf_editor_last_edited;
            
            $fields = [
                    'wf_assigned_to' => $submission_assign,
                    'wf_data' => base64_encode(json_encode($data)),
                    'wf_locked' => 0,
                    'wf_locked_by_uid' => null
            ];
            
            \Drupal::database()
                ->update('rwPubgoldWorkflow')
                ->fields($fields)
                ->execute();
        }
        
        drupal_set_message(t('Paper successfully saved!'));
        $form_state->setRedirect('publisso_gold.conferences_conference', ['cf_id' => $form_state->get('cf_id')]);
        
        return $form;
    }    
}
