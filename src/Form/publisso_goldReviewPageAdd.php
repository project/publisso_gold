<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldReviewPageAdd.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldReviewPageAdd extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $num_corporations;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldReviewPageAdd|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldreviewpageadd';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        $form['name'] = [
          '#type' => 'textfield',
          '#title' => (string)t('Reviewpage - Name'),
          '#required' => true
        ];
        
        //init counter
        if(!$form_state->get('num_items'))
           $form_state->set('num_items', 0);
        
        $max = $form_state->get('num_items');
        
        $form['items'] = array(
          '#tree' => TRUE,
          '#prefix' => '<div id="items-replace">',
          '#suffix' => '</div>'
        );
        
        for ($delta = 0; $delta < $max; $delta++) {
            
            if (!isset($form['items'][$delta])) {
                
                $element = array(
                    '#type' => 'select',
                    '#title' => t('Item').' #'.$delta,
                    '#options' => [
                        'jn' => (string)t('Yes/No Field'),
                        'text' => (string)t('Textfield')
                    ],
                    '#required' => true
                );
                
                $form['items'][$delta]['type'][] = $element;
                
                $element = array(
                    '#type' => 'textfield',
                    '#title' => t('Text').' #'.$delta,
                    '#suffix' => '<hr>',
                    '#required' => false
                );
                
                $form['items'][$delta]['text'][] = $element;
            }
        }
        
        $form['addItem'] = array(
            '#type' => 'submit',
            '#name' => 'addItem',
            '#value' => t('Add item'),
            '#submit' => array('::addMoreSubmit'),
            '#suffix' => '<br><br>',
            '#prefix' => '<br><br>',
            
            '#ajax' => array(
              'callback' => '::addMoreCallback',
              'wrapper' => 'items-replace',
              'effect' => 'fade',
            ),
            
        );
        
        $form['review']['content'][] = [
            '#type' => 'submit',
            '#value' => (string)t('Save'),
            '#prefix' => '<br>'
        ];
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        $name = $form_state->getValue('name');
        
        $sql = "SELECT COUNT(*) AS `name_exists` FROM {rwPubgoldReviewPage} WHERE `rp_name` = '$name'";
        $result = \Drupal::database()->query($sql)->fetchAssoc();
        
        if($result['name_exists']){
            $form_state->setErrorByName('name', (string)t('Name already exists!'));
        }
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function validateFinish(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $items = $form_state->getValue('items');
        $schema = [];
        
        foreach($items as $item){
            
            $type = $item['type'][0];
            $text = $item['text'][0];
            
            $schema[$text] = $type == 'jn' ? 'checkbox' : 'textarea';
        }
        
        \Drupal::database()
          ->insert('rwPubgoldReviewPage')
          ->fields([
            'rp_name' => $form_state->getValue('name'),
            'rp_schema' => base64_encode(json_encode($schema))
          ])
          ->execute();
        
        drupal_set_message('Review-Page successfully created', 'status');
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function add_item(array &$form, FormStateInterface $form_state){
        
        $form['items'][] = [
            '#type' => 'select',
            '#title' => t('Type'),
            '#options' => [
                'jn' => (string)t('Yes/No Field'),
                'text' => (string)t('Textfield')
            ],
            '#required' => true
        ];
        
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addMoreSubmit(array &$form, FormStateInterface $form_state) {
        
        $form_state->set('num_items', $form_state->get('num_items') + 1);
        $form['items'][] = [
            '#type' => 'select',
            '#title' => t('Type'),
            '#options' => [
                'jn' => (string)t('Yes/No Field'),
                'text' => (string)t('Textfield')
            ],
            '#required' => true
        ];
        
        $form_state->setRebuild();
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addMoreCallback(array &$form, FormStateInterface $form_state) {
        return $form['items'];
    }
}
