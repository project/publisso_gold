<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldLogout.
 */
namespace Drupal\publisso_gold\Form;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Connection;
use \Drupal\file\Entity\File;
use Drupal\publisso_gold\Controller\Publisso;
use Drupal\publisso_sso\Classes\SSO;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Drupal\publisso_gold\Controller\User;

/**
 * Provides a simple example form.
 */
class publisso_goldUserprofile extends FormBase {
	private $modname = 'publisso_gold';
	private $database;
	private $modpath;
	private $user = NULL;
	private $current_user = NULL;
	public function __construct(Connection $database) {
		$this->database = $database;
		$this->current_user = new \Drupal\publisso_gold\Controller\User ( $_SESSION ['user'] ['id'] );
	}
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldUserprofile|static
         */
        public static function create(ContainerInterface $container) {
		return new static ( $container->get ( 'database' ) );
	}
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function getFormId() {
		return 'publisso_golduserprofile';
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
		$t = \Drupal::service ( 'publisso_gold.texts' );
		
		$user = Publisso::currentUser();
		
		if (! $form_state->has ( 'user' ))
			$form_state->set ( 'user', $args ['user'] );
		
		$form ['form'] = [
			'#type' => 'container',
			'#tree' => true
		];
		
		$form ['form'] ['salutation'] = array (
			'#type' => 'select',
			'#title' => t ( 'Salutation' ),
			'#options' => [
				0 => t ( 'Mr' ),
				1 => t ( 'Ms' )
			],
			'#default_value' => $user->profile()->getElement('salutation'),
			'#required' => true
		);
		
		$form ['form'] ['graduation'] = array (
			'#type' => 'textfield',
			'#title' => t ( 'Title prefix' ),
			'#default_value' => $user->profile()->getElement('graduation'),
		);
		
		$form ['form'] ['lastname'] = array (
			'#type' => 'textfield',
			'#title' => t ( 'Last Name' ),
			'#required' => true,
			'#default_value' => $user->profile()->getElement('lastname'),
		);
		
		$form ['form'] ['firstname'] = array (
			'#type' => 'textfield',
			'#title' => t ( 'First Name' ),
			'#required' => true,
			'#default_value' => $user->profile()->getElement('firstname'),
		);
		
		$form ['form'] ['graduation_suffix'] = array (
			'#type' => 'textfield',
			'#title' => t ( 'Title suffix' ),
			'#default_value' => $user->profile()->getElement('graduation_suffix'),
		);
		
		$form ['form'] ['email'] = array (
			'#type' => 'email',
			'#title' => t ( 'Emailaddress' ),
			'#required' => true,
			'#default_value' => $user->profile()->getElement('email'),
		);
		
		$form ['form'] ['orcid'] = array (
			'#type' => 'textfield',
			'#title' => t ( 'ORCID' ),
			'#default_value' => $user->profile()->getElement('orcid'),
		);
		
		$form ['form'] ['picture'] = array (
			'#type' => 'managed_file',
			'#title' => t ( 'Profile picture' ),
			'#upload_validators' => [
				'file_validate_extensions' => \Drupal::service ( 'publisso_gold.setup' )->getValue ( 'image.upload.valid_extensions' ) ?? ['gif png jpg jpeg'],
				'file_validate_size' => [30 * 1024 * 1024]
			],
                        '#progress_indicator' => 'bar',
                        '#progress_message' => (string)$this->t('Uploading...')
		);
		
		$form ['form'] ['show_picture'] = array (
			'#type' => 'markup',
			'#markup' => '<img src="' . Url::fromRoute ( 'publisso_gold.getPicture', [
				'type' => 'up',
				'id' => $user->getId()
			] )->toString () . '">'
		);
		
		$form ['form'] ['delete_picture'] = array (
			'#type' => 'checkbox',
			'#title' => t ( 'delete picture' )
		);
		
		$form ['form'] ['institute'] = array (
			'#type' => 'textfield',
			'#title' => t ( 'Institution' ),
			'#required' => true,
			'#default_value' => $user->profile()->getElement('institute')
		);
		
		$form ['form'] ['department'] = array (
			'#type' => 'textfield',
			'#title' => t ( 'Department' ),
			'#required' => false,
			'#default_value' => $user->profile()->getElement('department')
		);
		
		$form ['form'] ['fs_area_of_expertise'] = [
			'#type' => 'fieldset',
			'#title' => t ( 'Area of Expertise' ),
			
			'area_of_expertise' => [
				'#type' => 'select',
				'#title' => t ( 'Area of Expertise' ),
				'#options' => \Drupal::service ( 'publisso_gold.tools' )->getAreasOfExpertise ()
			],
			
			'up_area_of_expertise' => [
				'#type' => 'textfield',
				'#title' => t ( 'Area of Expertise Freetext' ),
				'#default_value' => $user->profile()->getElement('area_of_expertise')
			]
		];
		
		$form ['form'] ['correspondence_language'] = [
			'#type' => 'select',
			'#title' => t ( 'Correspondence Language' ),
			'#options' => \Drupal::service ( 'publisso_gold.tools' )->getCorrespondenceLanguages (),
			'#default_value' => $user->profile()->getElement('correspondence_language')
		];
		
		$form ['form'] ['country'] = array (
			'#type' => 'select',
			'#title' => t ( 'Country' ),
			'#options' => \Drupal::service ( 'publisso_gold.tools' )->getCountrylist (),
			'#required' => true,
			'#default_value' => $user->profile()->getElement('country'),
			'#attributes' => [
				'class' => [
					'country-select'
				]
			]
		);
		
		$form ['form'] ['postal_code'] = array (
			'#type' => 'textfield',
			'#title' => t ( 'Postal code' ),
			'#required' => false,
			'#default_value' => $user->profile()->getElement('postal_code')
		);
		
		$form ['form'] ['city'] = array (
			'#type' => 'textfield',
			'#title' => t ( 'City' ),
			'#required' => true,
			'#default_value' => $user->profile()->getElement('city')
		);
		
		$form ['form'] ['street'] = array (
			'#type' => 'textfield',
			'#title' => t ( 'Street & Number' ),
			'#required' => false,
			'#default_value' => $user->profile()->getElement('street')
		);
		
		$form ['form'] ['telephone'] = [
			'#type' => 'textfield',
			'#title' => ( string ) t ( 'Telephone' ),
			'#default_value' => $user->profile()->getElement('telephone')
		];
		
		$form ['form'] ['fs_pp'] = [
			'#type' => 'fieldset',
			'#title' => ( string ) $this->t ( $t->get ( 'userprofile.edit.pp.headline', 'fc' ) ),
			'content' => [
				'pp' => [
					'desc' => [
						'#markup' => nl2br ( ( string ) $this->t ( $t->get ( 'userprofile.edit.pp.description' ), [
							'@link_pp' => Link::fromTextAndUrl ( ( string ) $this->t ( $t->get ( 'userprofile.edit.pp.description.link' ) ), Url::fromRoute ( 'publisso_gold.privacy_protection', [
								'action' => 'view'
							], [
								'attributes' => [
									'target' => '_blank'
								]
							] ) )->toString ()
						] ) )
					],
					'field' => [
						'#type' => 'checkbox',
						'#title' => ( string ) $this->t ( $t->get ( 'userprofile.edit.pp.agree.label', 'fc' ) ),
						'#required' => true,
						'#default_value' => ! ! $user->profile()->agreedLatestPrivacyProtection()
					]
				],
				'boards' => [
					'desc' => [
						'#markup' => nl2br ( ( string ) $this->t ( $t->get ( 'userprofile.edit.boards.description' ) ) )
					],
					'field' => [
						'#type' => 'checkbox',
						'#title' => ( string ) $this->t ( $t->get ( 'userprofile.edit.pp.agree.label', 'fc' ) ),
						'#default_value' => ! ! $user->profile()->getElement('show_data_in_boards')
					]
				]
			]
		];
		
		$form ['submit'] = array (
			'#type' => 'submit',
			'#value' => ( string ) t ( $t->get ( 'global.save', 'fc' ) )
		);
		
		$form ['#attributes'] = array (
			'enctype' => 'multipart/form-data'
		);
		
		$form ['#cache'] ['#max-age'] = 0;
		$form_state->setCached ( false );
		return $form;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {
		$email = $form_state->getValue ( 'form' ) ['email'];
		$user = $form_state->get ( 'user' );
		$texts = \Drupal::service ( 'publisso_gold.texts' );
		
		if (! \Drupal::service ( 'email.validator' )->isValid ( $email )) {
			$form_state->setErrorByName ( 'email', t ( 'Please enter a valid email-address!' ) );
		}
		
		$reason = null;
		
		// Email in user profiles?
		if (! ! \Drupal::database ()->select ( 'rwPubgoldUsers', 't' )->fields ( 't', [ ] )->condition ( 'initial_email', $email, '=' )->condition ( 'id', $user->getElement ( 'id' ), '!=' )->countQuery ()->execute ()->fetchField ()) {
			$form_state->setErrorByName ( 'form][email', t ( 'This email-address already exists!' ) );
			$reason = 'user';
		}
		
		if (! ! \Drupal::database ()->select ( 'rwPubgoldUserProfiles', 't' )->fields ( 't', [ ] )->condition ( 'up_email', $email, '=' )->condition ( 'up_uid', $user->getElement ( 'id' ), '!=' )->countQuery ()->execute ()->fetchField ()) {
			$form_state->setErrorByName ( 'form][email', t ( 'This email-address already exists!' ) );
			$reason = 'profile';
		}
		
		if (! ! \Drupal::database ()->select ( 'rwPubgoldRegisterPending', 't' )->fields ( 't', [ ] )->condition ( 'rp_email', $email, '=' )->countQuery ()->execute ()->fetchField ()) {
			$form_state->setErrorByName ( 'form][email', t ( 'This email-address already exists! (RP)' ) );
			$reason = 'register';
		}
		
		if (! ! \Drupal::database ()->select ( 'rwPubgoldUserprofilePreview', 't' )->fields ( 't', [ ] )->condition ( 'pv_email', $email, '=' )->countQuery ()->execute ()->fetchField ()) {
			$form_state->setErrorByName ( 'form][email', t ( 'This email-address already exists! (RP)' ) );
			$reason = 'admin';
		}
		
		if ($reason)
			error_log ( 'Can\'t update userprofile - reason: ' . $reason . ' -- Look at ' . __METHOD__ . ' in ' . __FILE__ );
		$te = $form_state->getTriggeringElement ();
		
		if (count ( $form_state->getValue ( 'form' ) ['picture'] ) && $te ['#name'] != 'form_picture_remove_button') {
			
			$file = File::load ( $form_state->getValue ( 'form' ) ['picture'] [0] );
			$type = $file->getMimeType ();
			$uri = preg_replace ( '/^temporary:\//', '/tmp', $file->getFileUri () );
			$finfo = finfo_open ();
			$finfo = explode ( ' ', finfo_file ( $finfo, $uri ), 3 );
			$validTypes = [
				'jpe',
				'jpeg',
				'jpg',
				'png',
				'gif'
			];
			
			if (! ($finfo [1] == 'image' && in_array ( strtolower ( $finfo [0] ), $validTypes ))) {
				$form_state->setErrorByName ( 'form][picture', $this->t ( $texts->get ( 'userprofile.edit.error.wrong_filetype', 'fc' ) ) );
				error_log ( 'Determined wrong file for avatar for user "' . $user->profile->getReadableName () . ' (ID: ' . $user->getElement ( 'id' ) . ')": ' . implode ( ' ', $finfo ) );
			}
		}
		
		return $form;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {
		$session = \Drupal::service ( 'session' );
		$user = $form_state->get ( 'user' );
		
		$noAutoSave = [
			'picture',
			'delete_picture',
			'fs_area_of_expertise',
			'fs_pp'
		];
		foreach ( $form_state->getValue ( 'form' ) as $k => $v ) {
			if (! in_array ( $k, $noAutoSave ))
				$user->profile->setElement ( $k, $v, false );
		}
		
		$user->profile->setElement ( 'area_of_expertise', $form_state->getValue ( 'form' ) ['fs_area_of_expertise'] ['up_area_of_expertise'] );
		
		\Drupal::database ()->delete ( 'rwPubgoldUserAreasOfExpertise' )->condition ( 'uaoe_uid', $user->getElement ( 'id' ), '=' )->execute ();
		if ($form_state->getValue ( 'form' ) ['fs_area_of_expertise'] ['area_of_expertise'])
			\Drupal::database ()->insert ( 'rwPubgoldUserAreasOfExpertise' )->fields ( [
				'uaoe_id' => $form_state->getValue ( 'form' ) ['fs_area_of_expertise'] ['area_of_expertise'],
				'uaoe_uid' => $user->getElement ( 'id' )
			] )->execute ();
		
		if ($form_state->getValue ( 'form' ) ['delete_picture'] == 1) {
			$user->profile->setElement ( 'picture', null, false );
			$user->profile->setElement ( 'picture_type', null, false );
		}
		
		// if uploaded file, process data (determine image and get image-data)
		if (count ( $form_state->getValue ( 'form' ) ['picture'] )) {
			
			if (false !== ($ret = \Drupal::service ( 'publisso_gold.tools' )->processUploadedUserAvatar ( $form_state->getValue ( 'form' ) ['picture'] [0] ))) {
				$user->profile->setElement ( 'picture_type', $ret [0], false );
				$user->profile->setElement ( 'picture', $ret [1], false );
			}
		}
		
		$user->profile->setElement ( 'show_data_in_boards', $form_state->getValue ( 'form' ) ['fs_pp'] ['content'] ['boards'] ['field'], false );
		
		$user->profile->setElement ( 'edited', 0, false );
		
		$user->profile->save ();
		
		if(Publisso::setup()->getValue('system.sso.active') && SSO::ssoAvailable()){
		        $retSSO = SSO::updateUser($user->getElement('zbmed_kennung'), \Drupal::service('session')->get('user')['password'], [
                                'institut' => $user->profile->getElement('institute'),
                                'LKURZ' => preg_match('/^\d+$/', $user->profile->getElement('country')) ? array_search(\Drupal::service('publisso_gold.tools')->getCountry($user->profile->getElement('country')), \Drupal::service('publisso_gold.tools')->getCountrylist()) : $user->profile->getElement('country'),
                                'LORT' => $user->profile->getElement('city'),
                                'name' => $user->profile->getReadableName(),
                                'LSTRAS' => $user->profile->getElement('street'),
                                'LPZAHL' => $user->profile->getElement('postal_code'),
                                'LTELNR' => $user->profile->getElement('telephone'),
                                'LSPRACHE' => $user->profile->getElement('correspondence_language'),
                                'LEMAILADR' => $user->profile->getElement('email')
                        ]);
		        
		        if($retSSO->getLastCode() != 200){
		                Publisso::tools()->logMsg('ERROR in '.__FILE__.' line '.__LINE__.': '.$retSSO->getLastMessage());
                        }
                }
		/*
		$sso = \Drupal::service ( 'publisso_gold.sso_provider' );
		if ($sso->SSOActive () === true) {
			if ($sso->updateUser ( $user ) === false) {
				\Drupal::service ( 'publisso_gold.tools' )->logMsg ( $sso->getLastResponseMessage () );
			}
		}
		*/
		\Drupal::service ( 'messenger' )->addMessage ( 'Your changes have been successfully stored' );
		$form_state->setRedirect ( 'publisso_gold.userprofile' );
	}
        
        /**
         * @param $user
         * @param $password
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        private function updateZBMED(&$user, $password) {
		$user_local = $user;
		
		if (! $user_local->getElement ( 'initial_email' ))
			$user_local->setElement ( 'initial_email', $user_local->profile->email );
		
		$data = [
			'method' => 'change_user_data',
			'parameters' => [
				'remote_id' => $user_local->getElement ( 'zbmed_kennung' ),
				'pw' => $_SESSION ['user'] ['password'],
				'address' => [
					'institut' => $user_local->profile->getElement ( 'institute' ),
					'LKURZ' => preg_match ( '/^\d+$/', $user_local->profile->getElement ( 'country' ) ) ? array_search ( getCountry ( $user_local->profile->getElement ( 'country' ) ), getCountrylist () ) : $user_local->profile->getElement ( 'country' ),
					'LORT' => $user_local->profile->getElement ( 'city' ),
					'name' => implode ( ', ', [
						$user_local->profile->getElement ( 'lastname' ),
						$user_local->profile->getElement ( 'firstname' )
					] ),
					'LSTRAS' => $user_local->profile->getElement ( 'street' ),
					'LPZAHL' => $user_local->profile->getElement ( 'postal_code' ),
					'LTELNR' => $user_local->profile->getElement ( 'telephone' ),
					'LSPRACHE' => $user_local->profile->getElement ( 'correspondence_language' ),
					'LLAND' => getCountry ( $user_local->profile->getElement ( 'country' ) ),
					'PRGRP' => 'z2',
					'LEMAILADR' => $user_local->profile->getElement ( 'email' )
				]
			],
			'values' => [ ]
		];
		
		$response = \Drupal::httpClient ()->request ( 'POST', 'https://lvps46-163-114-198.dedicated.hosteurope.de/zbmedauth', [
			'headers' => [
				'Content-Type' => 'application/json'
			],
			'http_errors' => false,
			'verify' => false,
			'query' => [
				'data' => base64_encode ( json_encode ( $data ) )
			]
		] );
		
		if ($response->getStatusCode () == 200) {
			
			$ret = json_decode ( $response->getBody ()->getContents () );
			error_log ( 'updateZBMED: ' . $ret->msg );
			if ($ret->code == 200) {
			} else {
				error_log ( 'updateZBMED: ' . $ret->code . ': ' . $ret->msg );
			}
		}
		error_log ( 'updateZBMED: ' . $response->getStatusCode () . ': ' . $response->getBody ()->getContents () );
	}
        
        /**
         * @param $password
         * @return bool
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        private function updateZBMEDPassword($password) {
		$user_local = new \Drupal\publisso_gold\Controller\User ( $this->current_user->getElement ( 'id' ) );
		
		$data = [
			'method' => 'change_password',
			'parameters' => [
				'remote_id' => $user_local->getElement ( 'zbmed_kennung' ),
				'pw' => $_SESSION ['user'] ['password'],
				'new_pw' => $password
			],
			'values' => [ ]
		];
		error_log ( "Update password with " . json_encode ( $data ) );
		$response = \Drupal::httpClient ()->request ( 'POST', 'https://lvps46-163-114-198.dedicated.hosteurope.de/zbmedauth', [
			'headers' => [
				'Content-Type' => 'application/json'
			],
			'http_errors' => false,
			'verify' => false,
			'query' => [
				'data' => base64_encode ( json_encode ( $data ) )
			]
		] );
		
		if ($response->getStatusCode () == 200) {
			
			$ret = json_decode ( $response->getBody ()->getContents () );
			
			error_log ( "updateZBMEDPassword: " . $response->getBody ()->getContents () );
			return true;
		}
		
		return false;
	}
}

