<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldInviteUser_Book46.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Controller\ControllerBase;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Drupal\Core\Database\Connection;
        use Drupal\Core\Url;
        use \Drupal\publisso_gold\Controller\WorkflowInfoMail;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldInviteUser_Book46 extends FormBase {
                
                private $current_user = null;
                private $modname      = 'publisso_gold';
                private $modpath;
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'publisso_goldinviteuserbook46';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state, $args = [] ) {
                        //echo '<pre>Hallo</pre>'; exit();
                        $this->modpath = drupal_get_path( 'module', $this->modname );
                        require_once( $this->modpath . '/inc/publisso_gold.lib.inc.php' );
                        
                        $this->current_user = new \Drupal\publisso_gold\Controller\User( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] );
                        
                        $form_state->setCached( false ); //don't cache this form
                        
                        if ( !$form_state->get( 'wf_id' ) && array_key_exists( 'wf_id', $args ) )
                                $form_state->set( 'wf_id', $args[ 'wf_id' ] );
                        
                        if ( !$form_state->get( 'medium' ) && array_key_exists( 'medium', $args ) )
                                $form_state->set( 'medium', $args[ 'medium' ] );
                        
                        switch ( $form_state->get( 'medium' ) ) {
                                case 'book':
                                        $form_state->set( 'medium_id', $args[ 'bk_id' ] );
                                        break;
                                
                                case 'journal':
                                        $form_state->set( 'medium_id', $args[ 'jrn_id' ] );
                                        break;
                                
                                case 'conference':
                                        $form_state->set( 'medium_id', $args[ 'cf_id' ] );
                                        break;
                        }
                        
                        $roles = [
                                'edoff'    => (string)t( 'Editorial Office' ),
                                'eic'      => (string)t( 'Editor in Chief' ),
                                'editor'   => (string)t( 'Editor' ),
                                'reviewer' => (string)t( 'Reviewer' ),
                                'author'   => (string)t( 'Author' ),
                        ];
                        
                        $weights = [
                                'edoff'    => 5,
                                'eic'      => 4,
                                'editor'   => 3,
                                'reviewer' => 2,
                                'author'   => 1,
                        ];
                        
                        if ( $form_state->has( 'wf_id' ) || $form_state->has( 'medium' ) ) {
                                
                                $workflow = new \Drupal\publisso_gold\Controller\Workflow( $form_state->get( 'wf_id' ) );
                                $book = new \Drupal\publisso_gold\Controller\Book( $form_state->get( 'medium_id' ) );
                                
                                $myWeight = $this->getRoleWeight( $book, $workflow );
                                
                                foreach ( $roles as $_role => $_form ) {
                                        
                                        if ( $myWeight < $weights[ $_role ] )
                                                unset( $roles[ $_role ] );
                                }
                                
                                $form[ 'userdata' ] = [
                                        '#tree'   => true,
                                        '#title'  => t( 'Create user-account' ),
                                        '#type'   => 'fieldset',
                                        'account' => [
                                                'firstname' => [
                                                        '#type'          => 'textfield',
                                                        '#title'         => t( 'Firstname' ),
                                                        '#required'      => true,
                                                        '#default_value' => $form_state->get( 'form_data' ) ? $form_state->get( 'form_data' )[ 'account' ][ 'firstname' ] : '',
                                                ],
                                                
                                                'lastname' => [
                                                        '#type'          => 'textfield',
                                                        '#title'         => t( 'Lastname' ),
                                                        '#required'      => true,
                                                        '#default_value' => $form_state->get( 'form_data' ) ? $form_state->get( 'form_data' )[ 'account' ][ 'lastname' ] : '',
                                                ],
                                                
                                                'email' => [
                                                        '#type'          => 'email',
                                                        '#title'         => t( 'Email-Address' ),
                                                        '#required'      => true,
                                                        '#default_value' => $form_state->get( 'form_data' ) ? $form_state->get( 'form_data' )[ 'account' ][ 'email' ] : '',
                                                ],
                                        ],
                                        '#access' => $form_state->get( 'foundUsers' ) || $form_state->get( 'foundUsersWithEmail' ) ? false : true,
                                ];
                                
                                $form[ 'userdata' ][ 'role' ] = [
                                        '#type'          => 'select',
                                        '#title'         => t( 'Role' ),
                                        '#size'          => count( $roles ) + 1,
                                        '#options'       => $roles,
                                        '#multiple'      => false,
                                        '#required'      => true,
                                        '#default_value' => $form_state->get( 'form_data' ) ? $form_state->get( 'form_data' )[ 'role' ] : 'edoff',
                                        '#access'        => $form_state->get( 'foundUsers' ) || $form_state->get( 'foundUsersWithEmail' ) ? false : true,
                                ];
                                
                                $form[ 'mailvars_author' ] = [
                                        '#tree'   => true,
                                        '#title'  => t( 'Mailinhalte' ),
                                        '#type'   => 'fieldset',
                                        '#states' => [
                                                'visible'   => [   // action to take.
                                                                   ':input[name="userdata[role]"]' => [ 'value' => 'author' ],
                                                ],
                                                'invisible' => [   // action to take.
                                                                   ':input[name="userdata[role]"]' => [ '!value' => 'author' ],
                                                ],
                                        ],
                                        'help'    => [
                                                '#type'   => 'markup',
                                                '#markup' => (string)t( 'Bitte ergänzen Sie die notwendigen Informationen. Diese werden dann automatisch in die vorformulierte Einladung übernommen und per E-Mail an den ausgewählten Empfänger versendet.' ),
                                                '#prefix' => '<div>',
                                                '#suffix' => '</div>',
                                        ],
                                        'vars'    => [
                                                
                                                'chapter-title' => [
                                                        '#type'          => 'textfield',
                                                        '#title'         => t( 'Titel des Kapitels' ),
                                                        '#required'      => false,
                                                        '#default_value' => $form_state->get( 'form_data' ) ? $form_state->get( 'form_data' )[ 'mailvars_author' ][ 'chapter-title' ] : '',
                                                ],
                                                
                                                'char-count' => [
                                                        '#type'          => 'textfield',
                                                        '#title'         => t( 'Maximale Zeichenzahl (inkl. Leerzeichen)' ),
                                                        '#required'      => false,
                                                        '#default_value' => $form_state->get( 'form_data' ) ? $form_state->get( 'form_data' )[ 'mailvars_author' ][ 'char-count' ] : '',
                                                ],
                                                
                                                'deadline' => [
                                                        '#type'          => 'textfield',
                                                        '#title'         => t( 'Abgabefrist' ),
                                                        '#required'      => false,
                                                        '#default_value' => $form_state->get( 'form_data' ) ? $form_state->get( 'form_data' )[ 'mailvars_author' ][ 'deadline' ] : '',
                                                ],
                                                
                                                'notes' => [
                                                        '#type'          => 'textarea',
                                                        '#title'         => t( 'Zusätzliche Informationen' ),
                                                        '#required'      => false,
                                                        '#default_value' => $form_state->get( 'form_data' ) ? $form_state->get( 'form_data' )[ 'mailvars_author' ][ 'notes' ] : '',
                                                ],
                                        ],
                                        '#access' => $form_state->get( 'foundUsers' ) || $form_state->get( 'foundUsersWithEmail' ) ? false : true,
                                ];
                                
                                $form[ 'mailvars_editor' ] = [
                                        '#tree'   => true,
                                        '#title'  => t( 'Mailinhalte' ),
                                        '#type'   => 'fieldset',
                                        '#states' => [
                                                'visible'   => [   // action to take.
                                                                   ':input[name="userdata[role]"]' => [ 'value' => 'editor' ],
                                                ],
                                                'invisible' => [   // action to take.
                                                                   ':input[name="userdata[role]"]' => [ '!value' => 'editor' ],
                                                ],
                                        ],
                                        'vars'    => [
                                                
                                                'section' => [
                                                        '#type'          => 'textfield',
                                                        '#title'         => t( 'Abschnitt' ),
                                                        '#required'      => false,
                                                        '#default_value' => $form_state->get( 'form_data' ) ? $form_state->get( 'form_data' )[ 'mailvars_editor' ][ 'section' ] : '',
                                                ],
                                                
                                                'chapterlist' => [
                                                        '#type'          => 'textarea',
                                                        '#title'         => t( 'Liste der Kapitel (evtl. mit Autorenvorschlägen)' ),
                                                        '#required'      => false,
                                                        '#default_value' => $form_state->get( 'form_data' ) ? $form_state->get( 'form_data' )[ 'mailvars_editor' ][ 'chapterlist' ] : '',
                                                ],
                                                
                                                'char-count' => [
                                                        '#type'          => 'textfield',
                                                        '#title'         => t( 'Maximale Zeichenzahl für den gesamten Abschnitt (inkl. Leerzeichen)' ),
                                                        '#required'      => false,
                                                        '#default_value' => $form_state->get( 'form_data' ) ? $form_state->get( 'form_data' )[ 'mailvars_editor' ][ 'char-count' ] : '',
                                                ],
                                                /*
                                                'suggestion' => [
                                                        '#type' => 'textfield',
                                                        '#title' => t('Suggestion'),
                                                        '#required' => false,
                                                        '#default_value' => $form_state->get('form_data') ? $form_state->get('form_data')['mailvars_editor']['suggestion'] : ''
                                                ],
                                                */
                                                'deadline'   => [
                                                        '#type'          => 'textfield',
                                                        '#title'         => t( 'Abgabefrist für den gesamten Abschnitt' ),
                                                        '#required'      => false,
                                                        '#default_value' => $form_state->get( 'form_data' ) ? $form_state->get( 'form_data' )[ 'mailvars_editor' ][ 'deadline' ] : '',
                                                ],
                                                
                                                'notes' => [
                                                        '#type'          => 'textarea',
                                                        '#title'         => t( 'Zusätzliche Informationen' ),
                                                        '#required'      => false,
                                                        '#default_value' => $form_state->get( 'form_data' ) ? $form_state->get( 'form_data' )[ 'mailvars_editor' ][ 'notes' ] : '',
                                                ],
                                        ],
                                        '#access' => $form_state->get( 'foundUsers' ) || $form_state->get( 'foundUsersWithEmail' ) ? false : true,
                                ];
                        }
                        else {
                                // multistep-form
                        }
                        if ( $form_state->get( 'foundUsers' ) ) {
                                
                                $foundUsers = [];
                                
                                foreach ( \Drupal::database()->select( 'rwPubgoldUserProfiles', 't' )->fields( 't', [ 'up_uid', 'up_graduation', 'up_firstname', 'up_lastname', 'up_graduation_suffix', 'up_institute', 'up_country' ] )->condition( 'up_id', $form_state->get( 'foundUsers' ), 'IN' )->execute()->fetchAll() as $result ) {
                                        $foundUsers[ $result->up_uid ] =
                                                implode( ', ', array_filter( [
                                                                                     implode( ' ', array_filter( [
                                                                                                                         !empty( $result->up_graduation ) ? $result->up_graduation : null,
                                                                                                                         $result->up_firstname,
                                                                                                                         $result->up_lastname,
                                                                                                                         !empty( $result->up_graduation_suffix ) ? $result->up_graduation_suffix : null,
                                                                                                                 ]
                                                                                     )
                                                                                     ),
                                                                                     !empty( $result->up_institute ) ? $result->up_institute : null,
                                                                                     !empty( $result->up_country ) ? getCountry( $result->up_country ) : null,
                                                                             ]
                                                )
                                                );
                                }
                                
                                if ( $form_state->has( 'usersAtZBMED' ) ) {
                                        
                                        foreach ( $form_state->get( 'usersAtZBMED' ) as $k => $user ) {
                                                
                                                $name = explode( ',', $user->name );
                                                array_walk( $name, "trim" );
                                                
                                                $foundUsers[ '::extern_' . $k ] =
                                                        implode( ', ', array_filter( [
                                                                                             implode( ' ', array_reverse( $name ) ),
                                                                                             !empty( $user->institut ) ? $user->institut : null,
                                                                                             !empty( $user->LKURZ ) ? getCountry( $user->LKURZ ) : null,
                                                                                     ]
                                                        )
                                                        );
                                        }
                                }
                                
                                $foundUsers[ '_default_' ] = '<span class="rwError">' . t( 'The user I would like to invite is not yet registered in the database. Continue the invitation with the data previously entered.' ) . '</span>';
                                
                                $form[ 'users' ] = [
                                        '#type'        => 'radios',
                                        '#title'       => t( 'Select user from database' ),
                                        '#description' => t( 'Match your request records from the database. If one applies it, so select it, otherwise, check the box "continue with previously set values".' ),
                                        '#options'     => $foundUsers,
                                        '#required'    => true,
                                ];
                                
                                $form[ 'back' ] = [
                                        '#type'                    => 'submit',
                                        '#button_type'             => 'primary',
                                        '#submit'                  => [ '::back' ],
                                        '#value'                   => t( 'Back' ),
                                        '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                                        '#limit_validation_errors' => [],
                                ];
                        }
                        
                        if ( $form_state->get( 'foundUsersWithEmail' ) ) {
                                
                                $foundUsers = [];
                                
                                foreach ( \Drupal::database()->select( 'rwPubgoldUserProfiles', 't' )->fields( 't', [ 'up_uid', 'up_graduation', 'up_firstname', 'up_lastname', 'up_graduation_suffix', 'up_institute', 'up_country' ] )->condition( 'up_id', $form_state->get( 'foundUsersWithEmail' ), 'IN' )->execute()->fetchAll() as $result ) {
                                        $foundUsers[ $result->up_uid ] =
                                                implode( ', ', array_filter( [
                                                                                     implode( ' ', array_filter( [
                                                                                                                         !empty( $result->up_graduation ) ? $result->up_graduation : null,
                                                                                                                         $result->up_firstname,
                                                                                                                         $result->up_lastname,
                                                                                                                         !empty( $result->up_graduation_suffix ) ? $result->up_graduation_suffix : null,
                                                                                                                 ]
                                                                                     )
                                                                                     ),
                                                                                     !empty( $result->up_institute ) ? $result->up_institute : null,
                                                                                     !empty( $result->up_country ) ? getCountry( $result->up_country ) : null,
                                                                             ]
                                                )
                                                );
                                }
                                
                                if ( $form_state->has( 'usersAtZBMED' ) ) {
                                        
                                        foreach ( $form_state->get( 'usersAtZBMED' ) as $k => $user ) {
                                                
                                                $name = explode( ',', $user->name );
                                                array_walk( $name, "trim" );
                                                
                                                $foundUsers[ '::extern_' . $k ] =
                                                        implode( ', ', array_filter( [
                                                                                             implode( ' ', array_reverse( $name ) ),
                                                                                             !empty( $user->institut ) ? $user->institut : null,
                                                                                             !empty( $user->LKURZ ) ? getCountry( $user->LKURZ ) : null,
                                                                                     ]
                                                        )
                                                        );
                                        }
                                }
                                /* muss raus */
                                $foundUsers[ '_default_' ] = '<span class="rwError">' . t( 'The user I would like to invite is not yet registered in the database. Continue the invitation with the data previously entered.' ) . '</span>';
                                /* --------- */
                                $form[ 'users' ] = [
                                        '#type'        => 'radios',
                                        '#description' => t( 'The entered e-mail address exists in the database. If you think that user, then select it and go for, otherwise, please go back and change the data.' ),
                                        '#title'       => t( 'Select user from database' ),
                                        '#options'     => $foundUsers,
                                        '#required'    => true,
                                ];
                                
                                $form[ 'back' ] = [
                                        '#type'                    => 'submit',
                                        '#button_type'             => 'primary',
                                        '#submit'                  => [ '::back' ],
                                        '#value'                   => t( 'Back' ),
                                        '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                                        '#limit_validation_errors' => [],
                                ];
                        }
                        
                        $form[ 'submit' ] = [
                                '#type'        => 'submit',
                                '#value'       => t( 'Submit' ),
                                '#button_type' => 'success',
                        ];
                        
                        $form[ '#attached' ] = [
                                'library' => [
                                        'publisso_gold/default',
                                ],
                        ];
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        if ( $form_state->hasValue( 'userdata' ) ) {
                                
                                $var = 'usersAtZBMED';
                                if ( strtolower( \Drupal::service( 'publisso_gold.setup' )->getValue( 'system.sso.active' ) ) == 'yes' )
                                        $$var = nameSearchAtZBMED( $form_state->getValue( 'userdata' )[ 'account' ][ 'firstname' ], $form_state->getValue( 'userdata' )[ 'account' ][ 'lastname' ] );
                                
                                $form_state->set( $var, $$var );
                                
                                $emailsAtZBMED = [];
                                
                                foreach ( $usersAtZBMED as $user ) {
                                        $emailsAtZBMED[] = \Drupal::service( 'email.validator' )->isValid( $user->remote_id ) ? $user->remote_id : $user->LEMAILADR;
                                }
                                
                                $usedAtZBMED = usedAtZBMED( $form_state->getValue( 'userdata' )[ 'account' ][ 'email' ] );
                                
                                if ( $usedAtZBMED == -1 ) {
                                        $form_state->setErrorByName( 'userdata["account"]["email"]', $this->t( 'Can\'t verify emailaddress!' ) );
                                }
                                elseif ( !in_array( $form_state->getValue( 'userdata' )[ 'account' ][ 'email' ], $emailsAtZBMED ) && $usedAtZBMED == 1 && !usedLocal( $form_state->getValue( 'userdata' )[ 'account' ][ 'email' ] ) ) {
                                        $form_state->setErrorByName( 'userdata["account"]["email"]', $this->t( 'This email address is central used but not local verifiable!' ) );
                                }
                        }
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function back ( array &$form, FormStateInterface $form_state ) {
                        
                        $this->current_user = new \Drupal\publisso_gold\Controller\User( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] );
                        $form_state->set( 'foundUsers', false );
                        $form_state->set( 'foundUsersWithEmail', false );
                        $form_state->setRebuild();
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $this->current_user = new \Drupal\publisso_gold\Controller\User( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] );
                        
                        $maildata = [];
                        
                        $roles = [
                                'edoff'    => (string)t( 'Editorial Office' ),
                                'eic'      => (string)t( 'Editor in Chief' ),
                                'editor'   => (string)t( 'Editor' ),
                                'reviewer' => (string)t( 'Reviewer' ),
                                'author'   => (string)t( 'Author' ),
                        ];
                        
                        if ( !$form_state->hasValue( 'users' ) ) {
                                
                                $account = $form_state->getValue( 'userdata' )[ 'account' ];
                                $role = $form_state->getValue( 'userdata' )[ 'role' ];
                                
                                $mailvars_author = $form_state->getValue( 'mailvars_author' )[ 'vars' ];
                                $mailvars_editor = $form_state->getValue( 'mailvars_editor' )[ 'vars' ];
                                
                                //email-address in database?
                                
                                $sql = "
				select 
					`up_id` 
				from 
					`rwpg_rwPubgoldUserProfiles` 
				where 
					`up_email` = :email";
                                
                                $result = \Drupal::database()->query( $sql, [
                                        ':email' => $account[ 'email' ],
                                ]
                                )->fetchAllKeyed()
                                ;
                                
                                if ( count( $result ) ) {
                                        $form_state->set( 'foundUsersWithEmail', array_keys( $result ) );
                                        $form_state->set( 'form_data', [ 'account' => $account, 'role' => $role, 'mailvars_author' => $mailvars_author, 'mailvars_editor' => $mailvars_editor ] );
                                        $form_state->setRebuild();
                                        return $form;
                                }
                                
                                //is a similar user in database?
                                
                                $sql = "
				select 
					`up_id` 
				from 
					`rwpg_rwPubgoldUserProfiles` 
				where 
					(soundex(`up_firstname`) = soundex(:firstname) and soundex(`up_lastname`) = soundex(:lastname))";
                                
                                $result = \Drupal::database()->query( $sql, [
                                        ':firstname' => $account[ 'firstname' ],
                                        ':lastname'  => $account[ 'lastname' ],
                                ]
                                )->fetchAllKeyed()
                                ;
                                
                                
                                if ( count( $result ) ) {
                                        $form_state->set( 'foundUsers', array_keys( $result ) );
                                        $form_state->set( 'form_data', [ 'account' => $account, 'role' => $role, 'mailvars_author' => $mailvars_author, 'mailvars_editor' => $mailvars_editor ] );
                                        $form_state->setRebuild();
                                        return $form;
                                }
                        }
                        else {
                                
                                $account = $form_state->get( 'form_data' )[ 'account' ];
                                $role = $form_state->get( 'form_data' )[ 'role' ];
                                $mailvars_author = $form_state->get( 'form_data' )[ 'mailvars_author' ];
                                $mailvars_editor = $form_state->get( 'form_data' )[ 'mailvars_editor' ];
                        }
                        
                        if ( $form_state->hasValue( 'users' ) && $form_state->getValue( 'users' ) != '_default_' ) {
                                
                                if ( $form_state->getValue( 'users' ) != '_default_' ) {
                                        
                                        $uid = $form_state->getValue( 'users' );
                                }
                        }
                        else {
                                $this->determineUsername( $account );
                                $this->genPassword( $account );
                        }
                        
                        $token = md5( (int)uniqid() * time() );
                        
                        $url_accept = new Url( 'publisso_gold.invitation', [ 'token' => $token, 'action' => 'accept' ] );
                        $url_accept->setAbsolute( true );
                        $url_accept = $url_accept->toString();
                        
                        $url_reject = new Url( 'publisso_gold.invitation', [ 'token' => $token, 'action' => 'reject' ] );
                        $url_reject->setAbsolute( true );
                        $url_reject = $url_reject->toString();
                        
                        switch ( $role ) {
                                
                                case 'author':
                                        foreach ( $mailvars_author as $k => $v ) $maildata[ $k ] = $v;
                                        $mailtmpl = getMailtemplate( 'invitation_authors_book_medpsych' );
                                        $mailtmpl_reg = getMailtemplate( 'invitation_authors_book_medpsych_reg' );
                                        $attachments = [ '/modules/custom/publisso_gold/var/mail/attachments/Hinweise_für_AutorInnen.pdf' ];
                                        break;
                                
                                case 'editor':
                                        foreach ( $mailvars_editor as $k => $v ) $maildata[ $k ] = $v;
                                        $mailtmpl = getMailtemplate( 'invitation_editors_book_medpsych' );
                                        $mailtmpl_reg = getMailtemplate( 'invitation_editors_book_medpsych_reg' );
                                        $attachments = [ '/modules/custom/publisso_gold/var/mail/attachments/Hinweise_für_EditorInnen.pdf' ];
                                        break;
                                
                                default:
                                        $mailtmpl = getMailtemplate( 'invitation' );
                                        $attachments = [];
                                        break;
                        }
                        
                        $medium_type = $medium = null;
                        
                        if ( $form_state->get( 'wf_id' ) ) {
                                
                                $workflow = new \Drupal\publisso_gold\Controller\Workflow( $form_state->get( 'wf_id' ) );
                                
                                switch ( $workflow->getDataElement( 'type' ) ) {
                                        
                                        case 'bookchapter':
                                                $medium_type = 'book';
                                                $medium = new \Drupal\publisso_gold\Controller\Book( $workflow->getDataElement( 'bk_id' ) );
                                                break;
                                        
                                        case 'journalarticle':
                                                $medium_type = 'journal';
                                                $medium = new \Drupal\publisso_gold\Controller\Journal( $workflow->getDataElement( 'jrn_id' ) );
                                                break;
                                        
                                        case 'conferencepaper':
                                                $medium_type = 'conference';
                                                $medium = new \Drupal\publisso_gold\Controller\Conference( $workflow->getDataElement( 'cf_id' ) );
                                                break;
                                }
                        }
                        
                        if ( $form_state->get( 'medium' ) && $form_state->get( 'medium_id' ) ) {
                                
                                switch ( $form_state->get( 'medium' ) ) {
                                        
                                        case 'book':
                                                $medium_type = 'book';
                                                $medium = new \Drupal\publisso_gold\Controller\Book( $form_state->get( 'medium_id' ) );
                                                break;
                                        
                                        case 'journalarticle':
                                                $medium_type = 'journal';
                                                $medium = new \Drupal\publisso_gold\Controller\Journal( $form_state->get( 'medium_id' ) );
                                                break;
                                        
                                        case 'conferencepaper':
                                                $medium_type = 'conference';
                                                $medium = new \Drupal\publisso_gold\Controller\Conference( $form_state->get( 'medium_id' ) );
                                                break;
                                }
                        }
                        
                        //prepare Mail-templates
                        $url_login = new Url( 'publisso_gold.invitation', [ 'token' => $token, 'action' => 'accept' ] );
                        $url_medium = new Url( 'publisso_gold.books_book', [ 'bk_id' => $medium->getElement( 'id' ) ] );
                        
                        $vars = [
                                '::role::'                       => $roles[ $role ],
                                '::link_accept::'                => $url_accept,
                                '::link_reject::'                => $url_reject,
                                '::email::'                      => $account[ 'email' ],
                                '::link.manuscript_guidelines::' => 'http://www.publisso.de/open-access-publizieren/buecher/',
                                '::link.login::'                 => $url_login->setAbsolute()->toString(),
                                '::link.book::'                  => $url_medium->setAbsolute()->toString(),
                        ];
                        
                        foreach ( $account as $k => $v ) {
                                $vars[ "::$k::" ] = $v;
                        }
                        
                        foreach ( $medium->getDataKeys() as $key ) {
                                $mailtmpl[ 'template' ] = str_replace( "::medium.$key::", ( isB64( $medium->getElement( $key ) ) ? base64_decode( $medium->getElement( $key ) ) : $medium->getElement( $key ) ), $mailtmpl[ 'template' ] );
                                $mailtmpl_reg[ 'template' ] = str_replace( "::medium.$key::", ( isB64( $medium->getElement( $key ) ) ? base64_decode( $medium->getElement( $key ) ) : $medium->getElement( $key ) ), $mailtmpl_reg[ 'template' ] );
                        }
                        
                        foreach ( \Drupal::service( 'publisso_gold.setup' )->getKeys() as $key ) {
                                $mailtmpl[ 'template' ] = str_replace( "::setup.$key::", ( isB64( \Drupal::service( 'publisso_gold.setup' )->getValue( $key ) ) ? base64_decode( \Drupal::service( 'publisso_gold.setup' )->getValue( $key ) ) : \Drupal::service( 'publisso_gold.setup' )->getValue( $key ) ), $mailtmpl[ 'template' ] );
                                $mailtmpl_reg[ 'template' ] = str_replace( "::setup.$key::", ( isB64( \Drupal::service( 'publisso_gold.setup' )->getValue( $key ) ) ? base64_decode( \Drupal::service( 'publisso_gold.setup' )->getValue( $key ) ) : \Drupal::service( 'publisso_gold.setup' )->getValue( $key ) ), $mailtmpl_reg[ 'template' ] );
                        }
                        
                        if ( $role == 'author' ) {
                                foreach ( $mailvars_author as $k => $v ) {
                                        $vars[ "::$k::" ] = $v;
                                }
                        }
                        
                        if ( $role == 'editor' ) {
                                foreach ( $mailvars_editor as $k => $v ) {
                                        $vars[ "::$k::" ] = $v;
                                }
                        }
                        
                        foreach ( $this->current_user->profile->getElementKeys() as $key ) {
                                $vars[ "::sender.profile.$key::" ] = $this->current_user->profile->getElement( $key );
                        }
                        
                        $mailtmpl[ 'template' ] = replaceVars( $mailtmpl[ 'template' ], $vars );
                        $mailtmpl[ 'recipient' ] = replaceVars( $mailtmpl[ 'recipient' ], $vars );
                        
                        $mailtmpl_reg[ 'template' ] = replaceVars( $mailtmpl_reg[ 'template' ], $vars );
                        $mailtmpl_reg[ 'recipient' ] = replaceVars( $mailtmpl_reg[ 'recipient' ], $vars );
                        
                        if ( !isset( $uid ) || empty( $uid ) ) {
                                
                                /******************************************************************************
                                 *Save profile to database                                                    *
                                 ******************************************************************************/
                                
                                $fields = [
                                        'user'           => $account[ 'username' ],
                                        'password'       => $account[ 'password_encrypted' ],
                                        'firstname'      => $account[ 'firstname' ],
                                        'lastname'       => $account[ 'lastname' ],
                                        'email'          => $account[ 'email' ],
                                        'role'           => $role,
                                        'wf_id'          => $form_state->get( 'wf_id' ),
                                        'medium_type'    => $medium_type,
                                        'medium_id'      => $medium != null ? $medium->getElement( 'id' ) : $medium,
                                        'created_by_uid' => $this->current_user->getElement( 'id' ),
                                        'token'          => $token,
                                ];
                                
                                \Drupal::database()->insert( 'rwPubgoldUserInvitations' )->fields( $fields )->execute();
                                
                                /******************************************************************************
                                 *send email                                                                  *
                                 ******************************************************************************/
                                $autosend_parent = null;
                                
                                if ( in_array( $role, explode( ',', \Drupal::service( 'publisso_gold.setup' )->getValue( 'user_invite_edit_mail_for_role' ) ) ) )
                                        $autosend_parent = saveMail2Spooler( $mailtmpl[ 'recipient' ], $mailtmpl, [ 'CC' => [ $this->current_user->profile->getElement( 'email' ) ], 'BCC' => [ 'livingbooks@zbmed.de' ] ], json_encode( $attachments ) );
                                else
                                        sendMail( $mailtmpl[ 'recipient' ], $mailtmpl[ 'subject' ], $mailtmpl[ 'template' ] );
                                
                                if ( $role == 'author' || $role == 'editor' ) {
                                        if ( $autosend_parent )
                                                saveMail2Spooler( $mailtmpl_reg[ 'recipient' ], $mailtmpl_reg, [], false, $autosend_parent );
                                        else
                                                sendMail( $mailtmpl_reg[ 'recipient' ], $mailtmpl_reg[ 'subject' ], $mailtmpl_reg[ 'template' ] );
                                }
                                
                        }
                        else {
                                //direct assign
                                
                                //if extern user, create and set uid
                                if ( preg_match( '/::extern_(\d+)/', $uid, $matches ) ) {
                                        
                                        $user_zbmed = $form_state->get( 'usersAtZBMED' )[ $matches[ 1 ] ];
                                        $user_local = new \Drupal\publisso_gold\Controller\User();
                                        
                                        if ( strpos( $user_zbmed, ',' ) === false ) {
                                                $name = explode( ' ', $user_zbmed->name );
                                                $lastname = array_pop( $name );
                                                $firstname = implode( ' ', $name );
                                        }
                                        else {
                                                $name = preg_split( '/\s*,\s*/', $user_zbmed->name );
                                                $lastname = array_shift( $name );
                                                $firstname = implode( ' ', $name );
                                        }
                                        
                                        $email = \Drupal::service( 'email.validator' )->isValid( $user_zbmed->remote_id ) ? $user_zbmed->remote_id : $user_zbmed->LEMAILADR;
                                        $user_local->create( $email, $string = openssl_random_pseudo_bytes( mt_rand( 8, 12 ) ), $firstname, $lastname, $email );
                                        
                                        $user_local->profile->setElement( 'institute', $user_zbmed->institut );
                                        $user_local->profile->setElement( 'city', $user_zbmed->LORT );
                                        $user_local->profile->setElement( 'telephone', $user_zbmed->LTELNR );
                                        $user_local->profile->setElement( 'postal_code', $user_zbmed->LPZAHL );
                                        $user_local->profile->setElement( 'street', $user_zbmed->LSTRAS );
                                        $user_local->profile->setElement( 'country', $user_zbmed->LKURZ );
                                        
                                        $user_local->setElement( 'initial_email', $user_zbmed->remote_id );
                                        $user_local->setElement( 'zbmed_kennung', $user_zbmed->KENNUNG );
                                        $uid = $user_local->getElement( 'id' );
                                }
                                
                                switch ( $role ) {
                                        
                                        case 'reviewer':
                                                
                                                $medium->addReviewer( $uid );
                                                
                                                if ( isset( $workflow ) ) {
                                                        
                                                        $workflow->setElement( 'assigned_to_reviewer', $workflow->getElement( 'assigned_to_reviewer' ) . ( !empty( $workflow->getElement( 'assigned_to_reviewer' ) ) ? ',' : '' ) . $uid );
                                                        
                                                        if ( $workflow->getDataElement( 'state' ) == 'in review' ) {
                                                                $workflow->setElement( 'assigned_to', $workflow->getElement( 'assigned_to' ) . ( !empty( $workflow->getElement( 'assigned_to' ) ) ? ',' : '' ) . 'u:' . $uid );
                                                                
                                                                $mail = new WorkflowInfoMail( $workflow, 'in review' );
                                                                if ( $mail ) $mail->send();
                                                                //sendWorkflowInfoMail($workflow, 'in review');
                                                        }
                                                        elseif ( $workflow->getDataElement( 'state' ) == 'assigned to editor' ) {
                                                                $workflow->setState( 'in review', [ $uid ] );
                                                        }
                                                }
                                                
                                                break;
                                        
                                        case 'eic':
                                                $medium->addEditorInChief( $uid );
                                                break;
                                        
                                        case 'edoff':
                                                $medium->addEitorialOffice( $uid );
                                                break;
                                        
                                        case 'editor':
                                                $medium->addEditor( $uid );
                                                break;
                                        
                                        case 'author':
                                                $medium->addAuthor( $uid );
                                                break;
                                }
                                
                                if ( $role == 'author' || $role == 'editor' ) {
                                        if ( in_array( $role, explode( ',', \Drupal::service( 'publisso_gold.setup' )->getValue( 'user_invite_edit_mail_for_role' ) ) ) )
                                                saveMail2Spooler( $mailtmpl[ 'recipient' ], $mailtmpl, [ 'CC' => [ $this->current_user->profile->getElement( 'email' ) ], 'BCC' => [ 'livingbooks@zbmed.de' ] ], json_encode( $attachments ) );
                                        else
                                                sendMail( $mailtmpl[ 'recipient' ], $mailtmpl[ 'subject' ], $mailtmpl[ 'template' ] );
                                }
                        }
                        
                        //save maildata
                        if ( $role == 'author' || $role == 'editor' ) {
                                $maildata[ 'role' ] = $role;
                                foreach ( $account as $k => $v ) $maildata[ $k ] = $v;
                                $maildata = base64_encode( json_encode( $maildata ) );
                                \Drupal::database()->insert( 'rwPubgoldSentMails' )->fields( [
                                                                                                     'creator_uid' => \Drupal::service( 'session' )->get( 'user' )[ 'id' ],
                                                                                                     'data'        => $maildata,
                                                                                                     'medium_type' => $medium_type,
                                                                                                     'medium_id'   => $medium->getElement( 'id' ),
                                                                                             ]
                                )->execute()
                                ;
                        }
                        
                        drupal_set_message( t( 'The user was successfully invited/assigned' ) );
                        $form_state->setRedirect( 'publisso_gold.dashboard' );
                        
                        return $form;
                }
        
                /**
                 * @param array $account
                 */
                private function determineUsername (array &$account ) {
                        
                        $num = 0;
                        $found = false;
                        
                        while ( !$found ) {
                                
                                //suggest username
                                if ( $num < strlen( $account[ 'firstname' ] ) )
                                        $username = strtolower( substr( $account[ 'firstname' ], 0, 1 + $num ) ) . strtolower( $account[ 'lastname' ] );
                                else
                                        $username = strtolower( substr( $account[ 'firstname' ], 0, 1 + $num ) ) . strtolower( $account[ 'lastname' ] ) . ( $num - strlen( $account[ 'firstname' ] ) + 1 );
                                
                                $count = 0;
                                
                                $count += \Drupal::database()->select( 'rwPubgoldUsers', 't' )->fields( 't', [] )->condition( 'user', $username, '=' )->countQuery()->execute()->fetchField();
                                $count += \Drupal::database()->select( 'rwPubgoldRegisterPending', 't' )->fields( 't', [] )->condition( 'rp_username', $username, '=' )->countQuery()->execute()->fetchField();
                                $count += \Drupal::database()->select( 'rwPubgoldUserInvitations', 't' )->fields( 't', [] )->condition( 'user', $username, '=' )->countQuery()->execute()->fetchField();
                                
                                if ( !$count ) {
                                        $account[ 'username' ] = $username;
                                        $found = true;
                                }
                                
                                $num++;
                        }
                }
        
                /**
                 * @param $email
                 * @return int
                 */
                private function emailExists ($email ) {
                        
                        $count = 0;
                        
                        $count += \Drupal::database()->select( 'rwPubgoldUserProfiles', 't' )->fields( 't', [] )->condition( 'up_email', $email, '=' )->countQuery()->execute()->fetchField();
                        $count += \Drupal::database()->select( 'rwPubgoldRegisterPending', 't' )->fields( 't', [] )->condition( 'rp_email', $email, '=' )->countQuery()->execute()->fetchField();
                        $count += \Drupal::database()->select( 'rwPubgoldUserInvitations', 't' )->fields( 't', [] )->condition( 'email', $email, '=' )->countQuery()->execute()->fetchField();
                        
                        return $count;
                }
        
                /**
                 * @param array $account
                 */
                private function genPassword (array &$account ) {
                        
                        $account[ 'password' ] = genTempPassword();
                        $account[ 'password_encrypted' ] = '*' . strtoupper( sha1( sha1( $account[ 'password' ], true ) ) );
                }
        
                /**
                 * @param $book
                 * @param null $workflow
                 * @return int
                 */
                private function getRoleWeight (&$book, &$workflow = null ) {
                        
                        $weight = 1; //author is default
                        
                        if ( $book->isUserReviewer( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) ) $weight = 2;
                        if ( $book->isUserEditor( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) ) $weight = 3;
                        if ( $book->isUserEditorInChief( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) ) $weight = 4;
                        if ( $book->isUserEditorialOffice( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) ) $weight = 5;
                        
                        if ( $workflow ) {
                                
                                if ( $workflow->isReviewer( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) && $weight < 2 ) $weight = 2;
                                if ( $workflow->isEditor( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) && $weight < 3 ) $weight = 3;
                                if ( $workflow->isEditorInChief( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) && $weight < 4 ) $weight = 4;
                                if ( $workflow->isEditorialOffice( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) && $weight < 5 ) $weight = 5;
                        }
                        
                        if ( \Drupal::service( 'session' )->get( 'user' )[ 'role_id' ] == 2 ) $weight = 6; //PUBLISSO
                        if ( \Drupal::service( 'session' )->get( 'user' )[ 'role_id' ] == 1 ) $weight = 7; //Administrator
                        
                        return $weight;
                }
        }






