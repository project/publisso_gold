<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\PubmedSearch.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class PubmedSearch extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return PubmedSearch|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'PubmedSearch';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                $form = \Drupal::formBuilder()->getForm('Drupal\search\Form\SearchBlockForm');
                $form['keys']['#description'] = '';
                $form['keys']['#name'] = 'term';
                
                if(isset($args['term'])){
                        //echo '<pre>'.print_r($args, 1).'</pre>'; exit();
                }
                
                $form['bk_id'] = [
                        '#type' => 'hidden',
                        '#value' => $args['bk_id']
                ];
                
                if(array_key_exists('term', $args)){
                        
                        $form['keys'] = [
                                '#type' => 'search',
                                '#title' => 'Search',
                                '#title_display' => 'invisible',
                                '#size' => 15,
                                '#default_value' => $args['term'],
                                '#input_group_button' => 1,
                                '#input' => 1,
                                '#maxlength' => 128,
                                '#theme' => 'input__search',
                                '#icon_position' => 'before',
                                '#weight' => 0,
                                '#description_display' => 'after',
                                '#name' => 'term',
                        ];
                }

                
                $form['actions'] = array('#type' => 'actions');
                $form['actions']['submit'] = array(
                        '#type' => 'submit',
                        '#value' => $this->t('Search'),
                        // Prevent op from showing up in the query string.
                        '#name' => '',
                );
                
                $form['submit'] = [
                        '#type' => 'button',
                        '#value' => (string)$this->t(''),
                        '#attributes' => [
                                'class' => [
                                        'glyphicon glyphicon-search'
                                ],
                                'style' => 'margin-left: -36px;'
                        ],
                        '#submit' => ['::submitForm']
                ];
                
                $form['spacer'] = [
                        '#type' => 'markup',
                        '#markup' => '<br><br>'
                ];
        
                $form['#attributes'] = array_merge($form['#attributes'], ['target' => '_blank']);
                
                unset($form['actions']['submit']);
                $form['#action'] = 'https://www.ncbi.nlm.nih.gov/m/pubmed/';  
                $form_state->setCached(false);
                
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        
        return $form;
    }
}
