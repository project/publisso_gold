<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldUsermanagementFilter.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormState;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldUsermanagementFilter extends FormBase {
    
        private $modname = 'publisso_gold';
        private $database;
        private $modpath;
        private $user = NULL;
        private $selected_roleid = '';
        private $selected_startletter = '';
        private $modaccessweights;
        
        /**
        * {@inheritdoc}
        */
        public function getFormId() {
                return 'publisso_goldusermanagementfilter';
        }

        /**
        * {@inheritdoc}
        */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                if(!$form_state->has('page') && array_key_exists('page', $args)) $form_state->set('page', $args['page']);
                if(!$form_state->has('sort') && array_key_exists('sort', $args)) $form_state->set('sort', $args['sort']);
                
                $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
                if($tempstore->get('usermanagement.filter.roleid')) $form_state->set('roleid', $tempstore->get('usermanagement.filter.roleid'));
                if($tempstore->get('usermanagement.filter.letter')) $form_state->set('letter', $tempstore->get('usermanagement.filter.letter'));
                
                $qryStartletter = \Drupal::database()->select('rwPubgoldUserProfiles', 't');
                $qryStartletter->addExpression('SUBSTRING(up_lastname, 1, 1)', 'letter');
                $qryStartletter->distinct();
                $qryStartletter->orderBy('up_lastname', 'ASC');
                $Startletters = $qryStartletter->execute()->fetchCol();
                
                $services = ['session', 'publisso_gold.tools', 'publisso_gold.setup', 'publisso_gold.texts'];
                foreach($services as $service){
                        if(preg_match('/((.+)\.){0,1}(.+)/', $service, $matches)){
                                $_ = $matches[3];
                                if(!$form_state->has($_)) $form_state->set($_, \Drupal::service($service));
                        }
                }

                //if user set as editor, eic or eo
                $user_has_rights = $form_state->get('session')->get('user')['isEditor'] | $form_state->get('session')->get('user')['isEiC'] | $form_state->get('session')->get('user')['isEO'];
                
        
                //get userroles, except guest
                $result = \Drupal::database()->select('rwPubgoldRoles', 'ur')->fields('ur', [])->condition('weight', 10, '>')->execute()->fetchAll();
                
                $userroles = array();
                
                if(is_array($result)){
                        foreach($result as $res){
                                $userroles[$res->id] = $res->name;
                        }
                }
        
                $form['roleid'] = array(
                        '#type' => 'select',
                        '#title' => (string)$this->t('Userrole'),
                        '#options' => $userroles,
                        '#required' => true
                );
                
                if($form_state->has('roleid')){
                        
//                         $form['lastname_startletter'] = array(
//                                 '#type' => 'select',
//                                 '#title' => (string)$this->t('Lastname starts with'),
//                                 '#options' => array_merge(
//                                         [' ' => (string)$this->t('All')], 
//                                         array_combine($Startletters, $Startletters)
//                                 )
//                         );
                        
//                         $form['lastname_startletter']['#options'] = array_merge([' ' => (string)$this->t('All')], array_combine($Startletters, $Startletters));
                        
                        $form['roleid']['#default_value'] = $form_state->get('roleid');
//                         $form['lastname_startletter']['#default_value'] = $form_state->get('letter');
                }
        
                $form['submit'] = array(
                        '#type' => 'submit',
                        '#value' => (string)$this->t('Apply filter')
                );
        
                if($form_state->has('roleid') || $form_state->has('page')){
                
                        $qry = \Drupal::database()->select('rwPubgoldUserProfiles', 'p');
                        
                        $qry->leftJoin('rwPubgoldUserroles', 'ur', 'p.up_uid = ur.user_id');
                        $qry->leftJoin('rwPubgoldUsers', 'u', 'u.id = p.up_uid');
                        $qry->leftJoin('rwPubgoldRoles', 'r', 'r.id = ur.role_id');
                        
                        $qry->condition('u.user', 'deleted:%', 'NOT LIKE');
                        $qry->condition('u.deleted', 0, '=');
                        
                        $pager = clone $qry;
                        
                        $qry->fields('p', [
                                'up_graduation',
                                'up_lastname',
                                'up_firstname',
                                'up_institute',
                                'up_country',
                                'up_city',
                                'up_street'
                        ])->fields('u', [
                                'user',
                                'id'
                        ])->fields('r', [
                                'name'
                        ]);
                        
                        
                        
                        
                        if($form_state->has('page')){
                                $qry->condition('up_lastname', $form_state->get('page').'%', 'LIKE');
                        }
                        
                        if($form_state->hasValue('roleid')){
                                $qry->condition('ur.role_id', $form_state->get('roleid'), '=');
                        }
                        
                        $pager->addExpression('substring(up_lastname, 1, 1)', 'letter');
                        $pager->groupBy('letter', 'ASC');
                        $pager->orderBy('letter');
                                                
                        $pages = $pager->execute()->fetchCol();
                        
                        $currentUrl = \Drupal::routeMatch()->getRouteName();
                        $currentParams = \Drupal::routeMatch()->getRawParameters()->all();
                        
                        
                        for($i = 0; $i < count($pages); $i++){
                        	
                        	if($form_state->has('page') && $form_state->get('page') == $pages[$i]){
                        		$pages[$i] = '<li class="active"><span>'.$pages[$i].'</span></li>';
                        	}
                        	else{
	                        	$url = Url::fromRoute($currentUrl, array_merge($currentParams, ['page' => $pages[$i]]));
	                        	$lnk = Link::fromTextAndUrl($pages[$i], $url);
	                        	$pages[$i] = '<li>'.$lnk->toString().'</li>';
                        	}
                        }
                        
                        
                        $form['pager'] = [
                        	'#markup' => implode($pages),
                        	'#prefix' => '<ul class="pager">',
                        	'#suffix' => '</ul>'
                        ];
                        
                        $usersort = false;
                        if($form_state->has('sort') && array_key_exists('field', $form_state->get('sort'))){
                                
                                $map = [
                                        'id'            => 'u.id',
                                        'graduation'    => 'p.up_graduation',
                                        'lastname'      => 'p.up_lastname',
                                        'firstname'     => 'p.up_firstname',
                                        'username'      => 'u.user',
                                        'institute'     => 'p.up_institute',
                                        'country'       => 'p.up_country',
                                        'city'          => 'p.up_city',
                                        'street'        => 'p.up_street'
                                ];
                                
                                if(array_key_exists($form_state->get('sort')['field'], $map)){
                                        
                                        $usersort = true;
                                        $direction = 'ASC';
                                        
                                        if(preg_match('/^(ASC|DESC)$/', $form_state->get('sort')['order'], $matches)){
                                                $direction = $matches[0];
                                        }
                                        
                                        $qry->orderBy($map[$form_state->get('sort')['field']], $direction);
                                }
                        }
                        
                        if(!$usersort){
                                $qry->orderBy('p.up_lastname', 'ASC')->orderBy('p.up_firstname', 'ASC');
                        }
                        
                        $result = $qry->execute()->fetchAll();
                        
                        if(is_array($result)){
                                
                                foreach($result as $_){
                                        
                                        $urlEdit   = Url::fromRoute('publisso_gold.usermanagement', ['uid' => $_->id]);
                                        $urlDelete = Url::fromRoute('publisso_gold.usermanagement.user.delete', ['uid' => $_->id]);
                                        
                                        $rows[] = [
                                        	[
                                        		'data' => $_->id,
                                        		'field' => 'id'
                                        	],
                                                [
                                                        'data' => $_->up_graduation,
                                                        'field' => 'graduation'
                                                ],
                                                [
                                                        'data' => $_->up_lastname,
                                                        'field' => 'lastname'
                                                ],
                                                [
                                                        'data' => $_->up_firstname,
                                                        'field' => 'firstname'
                                                ],
                                                [
                                                        'data' => $_->user,
                                                        'field' => 'user'
                                                ],
                                                [
                                                        'data' => $_->up_institute,
                                                        'field' => 'institute'
                                                ],
                                                [
                                                        'data' => $form_state->get('tools')->getCountry($_->up_country),
                                                        'field' => 'country'
                                                ],
                                                [
                                                        'data' => $_->up_city,
                                                        'field' => 'city'
                                                ],
                                                [
                                                        'data' => $_->up_street,
                                                        'field' => 'street'
                                                ],
                                                [
                                                        'data' => [
                                                                '#type' => 'dropbutton',
                                                                '#links' => [
                                                                        'edit' => [
                                                                                '#type' => 'link',
                                                                                '#url' => $urlEdit,
                                                                                '#title' => 'Edit'
                                                                        ],
                                                                        'delete' => [
                                                                                '#type' => 'link',
                                                                                '#url' => $urlDelete,
                                                                                '#title' => 'Delete'
                                                                        ],
                                                                ]
                                                        ],
                                                        'field' => 'actions',
                                                        'width' => "10%"
                                                ]
                                        ];
                                }
                        }
                        
                        $form['result'] = array(
                        '#type' => 'table',
                        '#prefix' => '<hr>',
                        '#rows' => $rows,
                        '#header' => [
	                        	[
	                        		'data' => (string)$this->t($form_state->get('texts')->get('usermanagement.list.col.id', 'fco')),
	                        		'field' => 'id'
	                        	],
                                        [
                                                'data' => (string)$this->t($form_state->get('texts')->get('usermanagement.list.col.graduation', 'fc')),
                                                'field' => 'graduation'
                                        ],
                                        [
                                                'data' => (string)$this->t($form_state->get('texts')->get('usermanagement.list.col.lastname', 'fc')),
                                                'field' => 'lastname'
                                        ],
                                        [
                                                'data' => (string)$this->t($form_state->get('texts')->get('usermanagement.list.col.firstname', 'fc')),
                                                'field' => 'firstname'
                                        ],
                                        [
                                                'data' => (string)$this->t($form_state->get('texts')->get('usermanagement.list.col.username', 'fc')),
                                                'field' => 'user'
                                        ],
                                        [
                                                'data' => (string)$this->t($form_state->get('texts')->get('usermanagement.list.col.institute', 'fc')),
                                                'field' => 'institute'
                                        ],
                                        [
                                                'data' => (string)$this->t($form_state->get('texts')->get('usermanagement.list.col.country', 'fc')),
                                                'field' => 'country'
                                        ],
                                        [
                                                'data' => (string)$this->t($form_state->get('texts')->get('usermanagement.list.col.city', 'fc')),
                                                'field' => 'city'
                                        ],
                                        [
                                                'data' => (string)$this->t($form_state->get('texts')->get('usermanagement.list.col.street', 'fc')),
                                                'field' => 'street'
                                        ],
                                        [
                                                'data' => '',
                                                'field' => 'actions'
                                        ]
                                ]
                        );
                }
                
                return $form;
        }
  
   
        /**
        * {@inheritdoc}
        */
        public function validateForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                
                $form_state->set('roleid', $form_state->getValue('roleid'));
                $form_state->set('letter', $form_state->get('lastname_startletter'));
                $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
                $tempstore->set('usermanagement.filter.roleid', $form_state->getValue('roleid'));
                $tempstore->set('usermanagement.filter.letter', $form_state->getValue('lastname_startletter'));
                $form_state->setRebuild();
                return $form;
        }    


}

