<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldSetPassword.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;
use Drupal\Core\Url;

/**
 * Provides a simple example form.
 */
class publisso_goldSetPassword extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'publisso_goldsetpassword';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $this->modpath = drupal_get_path('module', $this->modname);
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
        $args = $form_state->getBuildInfo();
        
        if(!$form_state->get('login_id'))
                $form_state->set('login_id', $args['args'][0]['login_id']);
        
        $form['info'] = [
            '#type' => 'markup',
            '#markup' => (string)t('
                Now you can set your new password.
            ')
        ];
        
        $form['password'] = [
            '#type' => 'password_confirm'
        ];
        
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => (string)t('Send')
        ];
        
        $form['#cache']['max-age'] = 0;
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        //getting submitted fields
        foreach($form_state->getValues() as $k => $v) $$k = $v;
        
        if($form_state->get('login_id')){
            
            $dbo = \Drupal::database();
            
            $query = $dbo->update('rwPubgoldUsers');
            $query = $query->condition('id', $form_state->get('login_id'), '=');
            $query = $query->expression('password', 'PASSWORD(:password)', [':password' => $password]);
            $query->execute();
            
            $query = $dbo->delete('rwPubgoldPWReset');
            $query = $query->condition('pwr_loginid', $form_state->get('login_id'));
            $query->execute();
            
            $form_state->setRedirect('publisso_gold.login');
            drupal_set_message((string)t('Your password has been updated. Now you can login with your new password.'));
        }

        return $form;
    }
}