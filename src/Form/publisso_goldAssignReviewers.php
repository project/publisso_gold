<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldAssignReviewers.
 */
namespace Drupal\publisso_gold\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldAssignReviewers extends FormBase {
        
        private $modname = 'publisso_gold';
        private $database;
        private $modpath;
        
        public function __construct(Connection $database) {
                
                $this->database = $database;
        }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldAssignReviewers|static
         */
        public static function create(ContainerInterface $container) {
                
                return new static ( $container->get ( 'database' ) );
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function getFormId() {
                
                return 'publisso_goldassignreviewers';
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function buildForm(array $form, FormStateInterface $form_state) {
                
                $this->modpath = drupal_get_path ( 'module', $this->modname );
                
                if ($this->modpath && ! $form_state->get ( 'modpath' )) {
                        $form_state->set ( 'modpath', $this->modpath );
                }
                
                if (! $this->modpath && $form_state->get ( 'modpath' )) {
                        $this->modpath = $form_state->get ( 'modpath' );
                }
                
                $args = $form_state->getBuildInfo ();
                
                if (count ( $args ))
                        $wf_id = $args ['args'] [0] ['wf_id'];
                
                if (! $form_state->has ( 'wf_id' ))
                        $form_state->set ( 'wf_id', $wf_id );
                
                if ($form_state->has ( 'wf_id' )) {
                        
                        // get the workflow-item
                        $workflow = getDashboard ( \Drupal::Database (), $wf_id );
                        $workflow = $workflow [0];
                        $workflow_data = json_decode ( base64_decode ( $workflow->wf_data ) );
                        $objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
                        
                        // get predefined editors
                        $editors = array ();
                        
                        switch ($objWorkflow->getDataElement ( 'type' )) {
                                
                                case 'bookchapter' :
                                        $medium = new \Drupal\publisso_gold\Controller\Book ( $objWorkflow->getDataElement ( 'bk_id' ) );
                                        break;
                                
                                case 'journalarticle' :
                                        $medium = new \Drupal\publisso_gold\Controller\Journal ( $objWorkflow->getDataElement ( 'jrn_id' ) );
                                        break;
                                
                                case 'congressmeeting' :
                                        $medium = new \Drupal\publisso_gold\Controller\Conference ( $objWorkflow->getDataElement ( 'cf_id' ) );
                                        break;
                        }
                        
                        foreach ( $medium->readReviewers () as $_user ) {
                                $availReviewers [$_user->getElement ( 'id' )] = implode ( ' ', [ 
                                        $_user->profile->getElement ( 'graduation' ),
                                        implode ( ', ', [ 
                                                $_user->profile->getElement ( 'lastname' ),
                                                $_user->profile->getElement ( 'firstname' ) 
                                        ] ),
                                        $_user->profile->getElement ( 'graduation_suffix' ) 
                                ] );
                        }
                        
                        if (! count ( $availReviewers )) {
                                $availReviewers = getUsersByRole ( \Drupal::database (), [ 
                                        2,
                                        3,
                                        4,
                                        5,
                                        6,
                                        7 
                                ] );
                        }
                        
                        $reviewers = [ ];
                        
                        foreach ( $availReviewers as $id => $user ) {
                                if (! $objWorkflow->isReviewer ( $id )) {
                                        $reviewers [$id] = $user;
                                }
                        }
                        
                        $form = [ 
                                
                                'reviewer-list' => [ 
                                        '#type' => 'fieldset',
                                        '#title' => t ( 'Set reviewers(s)' ),
                                        '#prefix' => '<br>',
                                        'content' => [ 
                                                'description' => [ 
                                                        '#type' => 'markup',
                                                        '#markup' => t ( 'Please check the person(s) which should be added to the list of reviewers.' ),
                                                        '#suffix' => '<br><br>' 
                                                ],
                                                'reviewers_list' => [ 
                                                        '#title' => t ( 'available Users for Reviewer' ),
                                                        '#type' => 'select',
                                                        '#multiple' => true,
                                                        '#options' => $reviewers,
                                                        '#required' => true,
                                                        '#default_value' => explode ( ',', $workflow->wf_assigned_to_reviewer ),
                                                        '#size' => 20 
                                                ],
                                                
                                                'invite_help' => [ 
                                                        '#type' => 'markup',
                                                        '#markup' => ( string ) t ( 'If you would like to assign someone who is not listet, choose "invite user"' ),
                                                        '#prefix' => '<div>',
                                                        '#suffix' => '</div>' 
                                                ],
                                                
                                                'invite' => [ 
                                                        '#type' => 'link',
                                                        '#title' => t ( 'Invite user' ),
                                                        '#url' => Url::fromRoute ( 'publisso_gold.workflow.invite_user', [ 
                                                                'wf_id' => $form_state->get ( 'wf_id' ) 
                                                        ] ),
                                                        '#suffix' => '<br>' 
                                                ],
                                                
                                                'submit' => [ 
                                                        '#type' => 'submit',
                                                        '#value' => t ( 'Set Reviewers(s) & close' ),
                                                        '#submit' => [ 
                                                                '::setReviewers' 
                                                        ],
                                                        '#limit_validation_errors' => [ 
                                                                [ 
                                                                        'reviewers_list' 
                                                                ] 
                                                        ] 
                                                ] 
                                        ] 
                                ] 
                        ];
                        
                        if ($medium->getControlElement ( 'allow_invite_users' ) == 0)
                                unset ( $form ['invite'] );
                }
                
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function validateForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function setReviewers(array &$form, FormStateInterface $form_state) {
                
                $reviewers = $form_state->getValue ( 'reviewers_list' );
                $objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
                
                // inform users about registration if not done
                
                foreach ( $reviewers as $uid ) {
                        
                        $_user = new \Drupal\publisso_gold\Controller\User ( $uid );
                        
                        if ($_user->getElement ( 'reg_notification_sent' ) == 0) {
                                
                                $port = \Drupal::request ()->getPort ();
                                $host = \Drupal::request ()->getHttpHost ();
                                $host = "http" . ($port == 443 ? 's' : '') . "://$host";
                                $link = $host;
                                $password ['clear'] = genTempPassword ();
                                $password ['encrypted'] = '*' . strtoupper ( sha1 ( sha1 ( $user ['password_clear'], true ) ) );
                                
                                $_user->setElement ( 'password', $password ['encrypted'] );
                                
                                foreach ( [ 
                                        'registration_password',
                                        'registration_info' 
                                ] as $name ) {
                                        
                                        $vars = [ 
                                                '::firstname::' => $_user->profile->getElement ( 'firstname' ),
                                                '::lastname::' => $_user->profile->getElement ( 'lastname' ),
                                                '::link::' => $link,
                                                '::email::' => $_user->profile->getElement ( 'email' ),
                                                '::user::' => $_user->getElement ( 'user' ),
                                                '::login_link::' => $link . '/publisso_gold/login',
                                                '::password::' => $password ['clear'] 
                                        ];
                                        
                                        sendRegistrationInfoMail ( $name, $vars );
                                }
                                
                                $_user->setElement ( 'reg_notification_sent', 1 );
                        }
                        
                        $objWorkflow->newUserReview ( $uid );
                }
                
                $objWorkflow->addReviewer ( array_keys ( $reviewers ) );
                $form_state->setRedirect ( 'publisso_gold.dashboard' );
                return $form;
        }
        
        /**
         * @param null $tmpl
         * @param null $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl = NULL, $vars = NULL) {
                if ($tmpl == NULL || $vars == NULL) {
                        // set Site-Vars
                        $this->tmpl = str_replace ( array_keys ( $this->tmpl_vars ), array_values ( $this->tmpl_vars ), $this->tmpl );
                        
                        // remove unused vars
                        $this->tmpl = preg_replace ( '(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl );
                } else {
                        // set Site-Vars
                        $tmpl = str_replace ( array_keys ( $vars ), array_values ( $vars ), $tmpl );
                        
                        // remove unused vars
                        return preg_replace ( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
                }
        }
}
