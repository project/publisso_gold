<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldInvitationActionReject.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;

/**
 * Provides a simple example form.
 */
class publisso_goldInvitationActionreject extends FormBase {

	private $current_user = NULL;
	private $modname = 'publisso_gold';
	private $modpath;

	/**
	* {@inheritdoc}
	*/
	public function getFormId() {
		return 'publisso_goldinvitationactionreject';
	}

	/**
	* {@inheritdoc}
	*/
	public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
		
		$this->modpath = drupal_get_path('module', $this->modname);
		require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
		
		$this->current_user = new \Drupal\publisso_gold\Controller\User($_SESSION['user']['id']);
		
		$form_state->setCached(false); //don't cache this form
		$form = [];
		
		$form['headline'] = [
			'#type' => 'markup',
			'#markup' => '<h2>'.t('Reject invitation').'</h2>'
		];
		
		$form['description'] = [
			'#type' => 'markup',
			'#markup' => '<pre>'.t('You have decided not to accept the invitation. Click the following button to delete the invitation from our system.').'</pre>'
		];
		
		$form['reject'] = [
			'#type' => 'submit',
			'#value' => t('Reject Invitation'),
			'#button_type' => 'success'
		];
		
		if(!$form_state->get('token') && array_key_exists('token', $args))
			$form_state->set('token', $args['token']);
		
		
		
		return $form;
	}

	/**
	* {@inheritdoc}
	*/
	public function validateForm(array &$form, FormStateInterface $form_state) {
		
		return $form;
	}

	/**
	* {@inheritdoc}
	*/
	public function submitForm(array &$form, FormStateInterface $form_state) {
		
		$roles = [
			'edoff' => 'editorial office',
			'eic' => 'editor-in-chief',
			'editor' => 'editor',
			'reviewer' => 'reviewer',
			'author' => 'author'
		];
		
		$result = \Drupal::database()->select('rwPubgoldUserInvitations', 't')->fields('t', [])->condition('token', $form_state->get('token'), '=')->execute()->fetchAssoc();
		$mailvars = [];
		
		$result['role'] = $roles[$result['role']];
		
		foreach($result as $key => $val){
			$mailvars["::$key::"] = $val;
		}
		
		$recipient = new \Drupal\publisso_gold\Controller\User($result['created_by_uid']);
		
		foreach($recipient->getKeys() as $key){
			$mailvars["::recipient.$key::"] = $recipient->getElement($key);
		}
		
		foreach($recipient->profile->getKeys() as $key){
			$mailvars["::recipient.profile.$key::"] = $recipient->profile->getElement($key);
		}
		
		if($result['wf_id']){
			
			$workflow = new \Drupal\publisso_gold\Controller\Workflow($result['wf_id']);
			
			switch($workflow->getDataElement('type')){
				
				case 'bookchapter':
					$medium = new \Drupal\publisso_gold\Controller\Book($workflow->getDataElement('bk_id'));
					break;
				
				case 'journalarticle':
					$medium = new \Drupal\publisso_gold\Controller\Journal($workflow->getDataElement('jrn_id'));
					break;
				
				case 'conferencepaper':
					$medium = new \Drupal\publisso_gold\Controller\Conference($workflow->getDataElement('cf_id'));
					break;
			}
			
			foreach($workflow->getKeys() as $key){
				$mailvars["::workflow.$key::"] = $workflow->getElement($key);
			}
			
			foreach($workflow->getDataKeys() as $key){
				$mailvars["::workflow.data.$key::"] = $workflow->getElement($key);
			}
		}
		
		if(!$medium && $result['medium_type'] && $result['medium_id']){
			
			switch($result['medium_type']){
				
				case 'book':
					$medium = new \Drupal\publisso_gold\Controller\Book($result['medium_id']);
					break;
				
				case 'journal':
					$medium = new \Drupal\publisso_gold\Controller\Journal($result['medium_id']);
					break;
				
				case 'conference':
					$medium = new \Drupal\publisso_gold\Controller\Conference();
					break;
			}
		}
		
		if($medium){
			
			foreach($medium->getControlKeys() as $key){
				$mailvars["::medium.control.$key::"] = $medium->getControlElement($key);
			}
			
			foreach($medium->getKeys() as $key){
				$mailvars["::medium.$key::"] = $medium->getElement($key);
			}
		}
		
		\Drupal::database()->update('rwPubgoldUserInvitations')->fields(['token' => md5(time()), 'status' => 'rejected'])->condition('token', $form_state->get('token'), '=')->execute();
		
		$mailout = getMailtemplate('info invitation rejected');
		$mailout['recipient'] = str_replace(array_keys($mailvars), array_values($mailvars), $mailout['recipient']);
		sendInfoMail($mailout, $mailvars);
		
		drupal_set_message(t('Your invitation has been deleted!'));
		$form_state->setRedirect('publisso_gold.content');
		return $form;
	}
}






