<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldConferencemanagementFormalAdaptConferencePaper.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldConferencemanagementFormalAdaptConferencePaper extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldConferencemanagementFormalAdaptConferencePaper|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldconferencemanagementformaladaptconferencepaper';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $this->modpath = drupal_get_path('module', $this->modname);
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
        //expect build-info-args as array
        $build_info = $form_state->getBuildInfo();
        $args = $build_info['args'];
        
        $form_state->set('cf_id', $args[0]['cf_id']);
        $form_state->set('wf_id', $args[0]['wf_id']);
        
        if(!$form_state->get('cf_id')){
            drupal_set_message(t('No conference given!'), 'error');
            return array();
        }
        
        $conference = getConference(\Drupal::database(), $form_state->get('cf_id'));
        
        if($form_state->get('wf_id')){
            $workflow = getDashboard(\Drupal::Database(), $form_state->get('wf_id'));
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            $paper_text = json_decode(base64_decode($workflow_data->paper_text));
            $abstract = json_decode(base64_decode($workflow_data->abstract));
            
            $delta = ['workflow', 'workflow_data'];
            foreach($delta as $_) $form_state->set($_, $$_);
        }
        
        $author = getAllUserData(\Drupal::database(), $workflow_data->author_uid);
        
        $form = [
            
            'site_headline' => [
            '#type' => 'markup',
            '#markup' => '<h1>'.t('Add Chapter').'</h1>'
            ],
            
            'conference_title' => [
                '#type' => 'markup',
                '#markup' => '<h2>'.$conference['cf_title'].'</h2>'
            ],
            
            'author' => [
                '#type' => 'markup',
                '#markup' => $author['profile_graduation'].' '.$author['profile_lastname'].', '.$author['profile_firstname'].' '.$author['profile_graduation_suffix'],
                '#prefix' => '<h4>'.(string)t('Author').': ',
                '#suffix' => '</h4>'
            ],
            
            'title' => [
                '#type' => 'textfield',
                '#title' => t('Title'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->title : ''
            ],
            
            'license' => [
                '#type' => 'textfield',
                '#title' => t('License'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->license : ''
            ],
            
            'abstract' => [
                '#type' => 'text_format',
                '#title' => t('Abstract'),
                '#required' => true,
                '#format' => isset($abstract) ? $abstract->format : 'basic_html',
                '#default_value' => isset($abstract) ? $abstract->value : ''
            ],
            
            'publication_year' => [
                '#type' => 'textfield',
                '#title' => t('Publication Year'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->publication_year : ''
            ],
            
            'publication_place' => [
                '#type' => 'textfield',
                '#title' => t('Publication Place'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->publication_place : ''
            ],
            
            'publisher' => [
                '#type' => 'textfield',
                '#title' => t('Publisher'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->publisher : ''
            ],
            
            'doi' => [
                '#type' => 'textfield',
                '#title' => t('DOI'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->doi : ''
            ],
            
            'ddc' => [
                '#type' => 'textfield',
                '#title' => t('DDC'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->ddc : ''
            ],
            
            'corresponding_author' => [
                '#type' => 'textfield',
                '#title' => t('Corresponding Author'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->corresponding_author : ''
            ],
            
            'paper_text' => [
                '#type' => 'text_format',
                '#title' => t('Paper Text'),
                '#required' => true,
                '#rows' => 50,
                '#format' => isset($paper_text) ? $paper_text->format : 'full_html',
                '#default_value' => isset($paper_text) ? $paper_text->value : ''
            ],
            
            'urn' => [
                '#type' => 'textfield',
                '#title' => t('URN'),
                '#default_value' => isset($workflow_data) ? $workflow_data->urn : ''
            ],
            
            'keywords' => [
                '#type' => 'textfield',
                '#title' => t('Keywords'),
                '#default_value' => isset($workflow_data) ? implode(', ', json_decode($workflow_data->keywords)) : ''
            ],
            
            'funding_name' => [
                '#type' => 'textfield',
                '#title' => t('Funding Name'),
                '#default_value' => isset($workflow_data) ? $workflow_data->funding_name : ''
            ],
            
            'funding_id' => [
                '#type' => 'textfield',
                '#title' => t('Funding ID'),
                '#default_value' => isset($workflow_data) ? $workflow_data->funding_id : ''
            ],
            
            'article_type' => [
                '#type' => 'textfield',
                '#title' => t('Article-type'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->article_type : ''
            ],
            
            'doc_number' => [
                '#type' => 'textfield',
                '#title' => t('docNumber'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->doc_number : ''
            ],
            
            'session' => [
                '#type' => 'textfield',
                '#title' => t('Session'),
                '#default_value' => isset($workflow_data) ? $workflow_data->session : ''
            ],
            
            'presenting_author' => [
                '#type' => 'textfield',
                '#title' => t('Presenting author'),
                '#default_value' => isset($workflow_data) ? $workflow_data->presenting_author : ''
            ],
            
            'description_errata' => [
                '#type' => 'markup',
                '#markup' => '<span class="rwAlert">'.t('You can set Version OR Errata OR Correction!').'</span>'
            ],
            
            'version' => [
                '#type' => 'textfield',
                '#title' => t('Version'),
                '#default_value' => isset($workflow_data) ? $workflow_data->version : ''
            ],
            
            'errata' => [
                '#type' => 'text_format',
                '#title' => t('Errata'),
                '#format' => 'full_html'
            ],
            
            'correction' => [
                '#type' => 'text_format',
                '#title' => t('Correction'),
                '#format' => 'full_html'
            ],
            
            'comment' => [
                '#type' => 'text_format',
                '#format' => 'basic_html',
                '#title' => (string)t('Comment')
            ],
            
            'submit' => [
                '#type' => 'submit',
                '#value' => (string)t('Request clearing from author')
            ]
            
        ];
        
        $form['#cache']['max-age'] = 0;
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $delta = ['workflow', 'workflow_data'];
        foreach($delta as $_) $$_ = $form_state->get($_);
        
        //save comment if esists
        if($form_state->getValue('comment')){
            
            $comment = $form_state->getValue('comment');
            
            if($comment['value'] != ''){
                
                $comment = base64_encode(json_encode($comment));
                
                \Drupal::database()
                    ->insert('rwPubgoldWorkflowComments')
                    ->fields([
                        'wfc_created_by_uid' => $_SESSION['user']['id'],
                        'wfc_comment' => $comment,
                        'wfc_wfid' => $workflow->wf_id
                    ])
                    ->execute();
            }
        }
        
        //prepare workflow for history
        $workflow_history = array();
        $workflow_history['wfh_created_by_uid'] = $_SESSION['user']['id'];
        
        foreach($workflow as $k => $v){
            
            if($k == 'wf_id'){
                $key = 'wfh_wfid';
            }
            else{
                
                $key = explode("_", $k, 2);
                $key = $key[0].'h_'.$key[1];
                
            }
            
            $workflow_history[$key] = $v;
        }
        
        \Drupal::database()->insert('rwPubgoldWorkflowHistory')->fields($workflow_history)->execute();

        $workflow_data->abstract               = base64_encode(json_encode($form_state->getValue('abstract')));
        $workflow_data->paper_text             = base64_encode(json_encode($form_state->getValue('paper_text')));
        $workflow_data->erratum                = base64_encode(json_encode($form_state->getValue('errata')));
        $workflow_data->correction             = base64_encode(json_encode($form_state->getValue('correction')));
        $workflow_data->license                = $form_state->getValue('license');
        $workflow_data->publication_year       = $form_state->getValue('publication_year');
        $workflow_data->publication_place      = $form_state->getValue('publication_place');
        $workflow_data->publisher              = $form_state->getValue('publisher');
        $workflow_data->doi                    = $form_state->getValue('doi');
        $workflow_data->ddc                    = $form_state->getValue('ddc');
        $workflow_data->corresponding_author   = $form_state->getValue('corresponding_author');
        $workflow_data->urn                    = $form_state->getValue('urn');
        $workflow_data->keywords               = json_encode(preg_split('/[^a-zA-Z0-9]+/', $form_state->getValue('keywords')));
        $workflow_data->funding_name           = $form_state->getValue('funding_name');
        $workflow_data->funding_id             = $form_state->getValue('funding_id');
        $workflow_data->version                = $form_state->getValue('version');
        $workflow_data->cf_id                  = $form_state->get('cf_id');
        $workflow_data->title                  = $form_state->getValue('title');
        $workflow_data->presenting_author      = $form_state->getValue('presenting_author');
        $workflow_data->article_type           = $form_state->getValue('article_type');
        $workflow_data->doc_number             = $form_state->getValue('doc_number');
        $workflow_data->issue                  = $form_state->getValue('issue');
        $workflow_data->issue_title            = $form_state->getValue('issue_title');
        $workflow_data->state                  = 'author clearing request';

        $workflow->wf_data = base64_encode(json_encode($workflow_data));
        
        \Drupal::database()
            ->update('rwPubgoldWorkflow')
            ->fields([
                'wf_data' => $workflow->wf_data,
                'wf_assigned_to' => 'u:'.$workflow_data->author_uid,
                'wf_locked' => 0,
                'wf_locked_by_uid' => null
            ])
            ->condition('wf_id', $form_state->get('wf_id'), '=')
            ->execute();
        
        return $form;
    }
}
