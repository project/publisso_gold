<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldSearch.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Url;
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Controller\ControllerBase;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Drupal\Core\Database\Connection;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldSearch extends FormBase {
                
                private $user = null;
                private $database;
                
                public function __construct ( Connection $database ) {
                        $this->database = $database;
                        
                        if ( isset( $_SESSION[ 'user' ] ) )
                                $this->user = $this->database->select( 'rwpgvwUserProfiles', 'u' )->fields( 'u', [] )->condition( 'id', $_SESSION[ 'user' ][ 'id' ], '=' )->execute()->fetchAssoc();
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return publisso_goldSearch|static
                 */
                public static function create (ContainerInterface $container ) {
                        return new static( $container->get( 'database' ) );
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'publisso_goldsearch';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state ) {
                        
                        $form = \Drupal::formBuilder()->getForm( 'Drupal\search\Form\SearchBlockForm' );
                        
                        $request = \Drupal::requestStack()->getCurrentRequest();
                        
                        $form[ 'keys' ][ '#description' ] = '';
                        
                        if ( $request->query->has( 'keys' ) ) $form[ 'keys' ][ '#value' ] = $request->query->get( 'keys' );
                        
                        $form[ 'actions' ] = [ '#type' => 'actions' ];
                        $form[ 'actions' ][ 'submit' ] = [
                                '#type'  => 'submit',
                                '#value' => $this->t( 'Search' ),
                                // Prevent op from showing up in the query string.
                                '#name'  => '',
                        ];
                        
                        $form[ 'submit' ] = [
                                '#type'       => 'button',
                                '#value'      => (string)$this->t( '' ),
                                '#attributes' => [
                                        'class' => [
                                                'glyphicon glyphicon-search',
                                        ],
                                        'style' => 'margin-left: -36px;',
                                ],
                                '#submit'     => [ '::submitForm' ],
                        ];
                        unset( $form[ 'actions' ][ 'submit' ] );
                        $form[ '#action' ] = $this->url( 'publisso_gold.search' );
                        $form_state->setCached( false );
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function formSubmit (array &$form, FormStateInterface $form_state ) {
                        
                        //reach($form_state->getValues() as $k => $v){
                        //      echo "$k<br>";
                        //
                        
                        //it();
                        //$_SESSION['page']['search'] = $_REQUEST['keys'];
                        //$form_state->setRedirect('publisso_gold.search', ['term' => $_REQUEST['keys']]);
                }
                
                
        }






