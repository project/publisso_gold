<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldBookchapterDeletePDF.
 */
namespace Drupal\publisso_gold\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\File;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Url;

/**
 * Provides a simple example form.
 */
class publisso_goldBookchapterDeletePDF extends FormBase {
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function getFormId() {
                return 'publisso_gold-bookchapter-delete-pdf';
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                if(\Drupal::service('session')->get('user')['weight'] < 70){
                        $form['msg'] = ['#markup' => (string)$this->t('You are not authorized to do this!'), '#prefix' => '<div class="rwError">', '#suffix' => '</div>'];
                        return $form;
                }
                
                $form_state->set('cp_id', $args['cp_id']);
                $form_state->set('route', $args['route']);
                $form_state->set('referrer', $args['referrer']);
                
                $chapter = new \Drupal\publisso_gold\Controller\Bookchapter($args['cp_id']);
                
                $form['headline'] = [
                        '#markup' => (string)$this->t('Do you really want to delete the stored PDF for chapter<br> "<i>@title</i>"?', ['@title' => $chapter->getElement('title')])
                ];
                
                $form['cancel'] = [
                        '#type' => 'button',
                        '#value' => (string)$this->t('No'),
                        '#button_type' => 'danger',
                        '#ajax' => [
                                'callback' => '::cancel',
                                'progress' => [
                                        'throbber' => 'none'
                                ]
                        ],
                        '#prefix' => '<hr>',
                        '#suffix' => '&nbsp;&nbsp;&nbsp;'
                ];
                
                $form['cancel'] = [
                        '#type' => 'button',
                        '#value' => (string)$this->t('No'),
                        '#button_type' => 'danger',
                        '#ajax' => [
                                'callback' => '::cancel',
                                'progress' => [
                                        'throbber' => 'none'
                                ]
                        ],
                        '#prefix' => '<hr>',
                        '#suffix' => '&nbsp;&nbsp;&nbsp;'
                ];
                
                $form['delete'] = [
                        '#type' => 'button',
                        '#value' => (string)$this->t('Yes'),
                        '#button_type' => 'success',
                        '#ajax' => [
                                'callback' => '::delete',
                                'progress' => [
                                        'throbber' => 'none'
                                ]
                        ],
                        '#suffix' => '&nbsp;&nbsp;&nbsp;'
                ];
                
                $form['#cache'] = [
                        'max-age' => 0
                ];
                
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function validateForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function delete(array &$form, FormStateInterface $form_state) {
                
                $chapter = new \Drupal\publisso_gold\Controller\Bookchapter($form_state->get('cp_id'));
                
                if($chapter->getElement('pdf_blob_id')){
                        
                        $blob = new \Drupal\publisso_gold\Controller\Blob($chapter->getElement('pdf_blob_id'));
                        $blob->delete();
                        $chapter->setElement('pdf_blob_id', null);
                }
                
                $command = new CloseDialogCommand();
                $response = new AjaxResponse();
                $response->addCommand($command);
                $response->addCommand(new RemoveCommand('#lnkChapterPDFDelete'));
                $response->addCommand(new RemoveCommand('#lnkChapterPDFDownload'));
                return $response;
                
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function cancel(array &$form, FormStateInterface $form_state) {
                
                $rUrl = Url::fromRoute($form_state->get('route')['name'], $form_state->get('route')['param']);
                
                $command = new CloseDialogCommand();
                $response = new AjaxResponse();
                $response->addCommand($command);
                return $response;
        }
}
