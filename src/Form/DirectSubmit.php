<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\DirectSubmit.
 */
namespace Drupal\publisso_gold\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Url;
use Drupal\publisso_gold\Controller\Manager\BookManager;
use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
use Drupal\publisso_gold\Controller\Manager\JournalManager;
use Drupal\publisso_gold\Controller\Manager\WorkflowSchemaManager;
use Drupal\publisso_gold\Controller\Medium\Book;
use Drupal\publisso_gold\Controller\Publisso;
use \Drupal\publisso_gold\Controller\User;
use Drupal\publisso_gold\Controller\Workflow\ArticleWorkflow;
use Drupal\publisso_gold\Controller\Workflow\ChapterWorkflow;

/**
 * Provides a simple example form.
 */
class DirectSubmit extends FormBase {
	
	private $user;
	private $texts;
	private $tools;
	private $session;
	private $setup;
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function getFormId() {
		return 'directsubmit';
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
         
                $requestParams = \Drupal::requestStack()->getCurrentRequest()->query->all();
                
                if(!$form_state->has('initialLoad')) {
                        foreach ( [ 'book', 'journal', 'conference' ] as $type ) {
                                if ( array_key_exists( $type, $requestParams ) ) {
                                        $form_state->setValue( 'type', $type );
                                        $form_state->setValue( $type, $requestParams[ $type ] );
                                }
                        }
                        
                        $form_state->set('initialLoad', true);
                }
	        
		$form['type'] = [
		        '#type' => 'select',
                        '#title' 	=> (string)$this->t('Type'),
                        '#options' => [
                                'book'		=> (string)$this->t('Book'),
                                'journal' 	=> (string)$this->t('Journal'),
                                'conference'	=> (string)$this->t('Conference')
                        ],
                        '#required' => true,
                        '#ajax' => [
                                'progress' => [
                                        'type' => 'none'
                                ],
                                'callback' => '::checkChapterTemplate',
                                'wrapper' => 'wrapper-template',
                                'method' => 'html',
                                'prevent' => 'blur'
                        ],
                        '#default_value' => $form_state->getValue('type') ?? null,
                        '#limit_validation_errors' => []
                ];
		
                $form['book'] = [
                        '#type' => 'select',
                        '#title' => (string)$this->t('Book'),
                        '#options' => $this->getBooklist(),
                        '#states' => [
                                'visible' => [
                                        ':input[name="type"]' => ['value' => 'book']
                                ],
                                'required' => [
                                        ':input[name="type"]' => ['value' => 'book']
                                ]
                        ],
                        '#ajax' => [
                                'progress' => [
                                        'type' => 'none'
                                ],
                                'callback' => '::checkChapterTemplate',
                                'wrapper' => 'wrapper-template',
                                'method' => 'html',
                                'prevent' => 'blur'
                        ],
                        '#limit_validation_errors' => []
                ];
                
                if($form_state->hasValue('book')) $form['book']['#default_value'] = $form_state->getValue('book');
                
                $form['wrapper-template'] = [
                        '#type' => 'container',
                        '#prefix' => '<div id="wrapper-template">',
                        '#suffix' => '</div>',
                        'template' => []
                ];
                
                $type = $form_state->getValue('type') ?? $form_state->getUserInput()['type'] ?? null;
                $medium = $form_state->getValue('book') ?? $form_state->getUserInput()['book'] ?? null;
                
                if($type == 'book' && $medium) {
                        
                        $book = BookManager::getBook($medium, false);
                        $templates = $book->getTemplates();
                        Publisso::log(print_r($templates, 1));
                        if(count($templates)) {
                                
                                $options = ['' => (string)$this->t('Standard chapter')];
                                $options[(string)$this->t('From template')] = [];
                                foreach ( $templates as $id => $item ) {
                                        $options[(string)$this->t('From template')][ $id ] = $item->title;
                                }
                                
                                $form[ 'wrapper-template' ][ 'template' ] = [
                                        '#type'     => 'select',
                                        '#title'    => (string)$this->t( 'Template' ),
                                        '#options'  => $options,
                                        '#attributes' => [
                                                'name' => 'template'
                                        ]
                                ];
                                
                        }
                }
                
                $form['journal'] = [
                        '#type' => 'select',
                        '#title' => (string)$this->t('Journal'),
                        '#options' => $this->getJournallist(),
                        '#states' => [
                                'visible' => [
                                        ':input[name="type"]' => ['value' => 'journal']
                                ],
                                'required' => [
                                        ':input[name="type"]' => ['value' => 'journal']
                                ]
                        ],
                        '#limit_validation_errors' => []
                ];
                
                if($form_state->hasValue('journal')) $form['journal']['#default_value'] = $form_state->getValue('journal');
                
                $form['conference'] = [
                        '#type' => 'select',
                        '#title' => (string)$this->t('Conference'),
                        '#options' => $this->getConferencelist(),
                        '#states' => [
                                'visible' => [
                                        ':input[name="type"]' => ['value' => 'conference']
                                ],
                                'required' => [
                                        ':input[name="type"]' => ['value' => 'conference']
                                ]
                        ],
                        '#limit_validation_errors' => []
                ];
                
                if($form_state->hasValue('conference')) $form['conference']['#default_value'] = $form_state->getValue('conference');
                
                $form['editorial'] = [
                        '#type'	 => 'checkbox',
                        '#title' => Publisso::text()->get('form.directsubmit.editorial.title', 'fc'),
                ];
                
                if(Publisso::currentUser()->getWeight() < 70){
        
                        $eoStates = [];
                        foreach(BookManager::getUserEOBooks() as $bk_id){
                
                                if(count($eoStates)) $eoStates[] = 'or';
                
                                $eoStates[] = [
                                        ':input[name="type"]' => ['value' => 'book'],
                                        ':input[name="book"]' => ['value' => $bk_id]
                                ];
                        }
        
                        foreach(JournalManager::getUserEOJournals() as $jrn_id){
                
                                if(count($eoStates)) $eoStates[] = 'or';
                
                                $eoStates[] = [
                                        ':input[name="type"]' => ['value' => 'journal'],
                                        ':input[name="journal"]' => ['value' => $jrn_id]
                                ];
                        }
                        
                        $form['editorial']['#states'] = ['visible' => $eoStates];
                }
                
                $form['submit'] = [
                        '#type'  	=> 'submit',
                        '#value' 	=> $this->t('Submit'),
                        '#button_type'	=> 'success'
                ];
                
		return $form;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {
		return $form;
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function checkChapterTemplate(array &$form, FormStateInterface $form_state){
	        return $form['wrapper-template'];
        }
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {
		
	        switch($form_state->getValue('type')){
	                
                        case 'book':
                                $bk_id = $form_state->getValue('book');
                                $book = BookManager::getBook($bk_id, false);
                                
                                if(!($book->isUserEditorialOffice() || Publisso::currentUser()->getWeight() >= 70)) $form_state->setValue('editorial', 0);
                                
                                if(!empty($form_state->getValue('template'))){
                                        $template = $form_state->getValue('template');
                                        $form_state->setRedirect('publisso_gold.book.template', ['id' => $template, 'action' => 'fill']);
                                }
                                else {
        
                                        $param = [];
                                        if ( $form_state->getValue( 'editorial' ) ) $param[ 'editorial' ] = $form_state->getValue( 'editorial' );
        
                                        if ( !$book->getElement( 'wfschema' ) ) {
                                                $form_state->setRedirect( 'publisso_gold.book.addchapter', array_merge( [ 'bk_id' => $bk_id ], $param ) );
                                        }
                                        else {
                                                $schema = WorkflowSchemaManager::getSchema($book->getElement('wfschema'));
                                                $workflow = new ChapterWorkflow();
                                                $workflow->init( $bk_id, 'bcs', $schema->getEntryPointID() );
                                                if ( $form_state->getValue( 'editorial' ) ) $workflow->setDataElement('editorial', 1);
                                                $form_state->setRedirect( 'publisso_gold.workflow.item', [ 'wf_id' => $workflow->getElement( 'id' ) ] );
                                        }
                                }
                                break;
                                
                        case 'journal':
                                $jrn_id = $form_state->getValue('journal');
                                $journal = JournalManager::getJournal($jrn_id);
                                if(!($journal->isUserEditorialOffice() || Publisso::currentUser()->getWeight() >= 70)) $form_state->setValue('editorial', 0);
        
                                $param = [];
                                if($form_state->getValue('editorial')) $param['editorial'] = $form_state->getValue('editorial');
        
                                if ( !$journal->getElement( 'wfschema' ) ) {
                                        $form_state->setRedirect('publisso_gold.journal_addarticle', array_merge(['jrn_id' => $jrn_id], $param));
                                }
                                else {
                                        $schema = WorkflowSchemaManager::getSchema($journal->getElement('wfschema'));
                                        $workflow = new ArticleWorkflow();
                                        $workflow->init( $jrn_id, 'jas', $schema->getEntryPointID() );
                                        if ( $form_state->getValue( 'editorial' ) ) $workflow->setDataElement('editorial', 1);
                                        $form_state->setRedirect( 'publisso_gold.workflow.item', [ 'wf_id' => $workflow->getElement( 'id' ) ] );
                                }
                                
                                break;
                                
                        case 'conference':
                                
                                $form_state->setRedirect( 'publisso_gold.conference.submit', [ 'param' => $form_state->getValue( 'conference' ) ] );
                                break;
                }
	        
		return $form;
	}
        
        /**
         * @return array
         */
        private function getBooklist(){
		
	        $retList = [];
	        $list = BookManager::getBooksList(['bk_title'], [['bk_title', 'ASC']]);
	        
	        foreach($list as $item){
	                
	                $url = Url::fromRoute('publisso_gold.book.addchapter', ['bk_id' => $item->bk_id]);
	                if($url->access()){
	                      $retList[$item->bk_id] = $item->bk_title;
                        }
                }
	        
	        return $retList;
	}
        
        /**
         * @return array
         */
        private function getJournallist(){
		
		$list = [];
		
		foreach(\Drupal::database()->select('rwPubgoldJournals', 't')
                                           ->fields('t', ['jrn_id', 'jrn_title'])
                                           ->condition('jrn_id', JournalManager::getJournalIDsWithVolumes(), 'IN')
                                           ->orderBy('jrn_title', 'ASC')
                                           ->execute()->fetchAll() as $_){
			$list[$_->jrn_id] = $_->jrn_title;
		}
		
		return $list;
	}
        
        /**
         * @return array
         */
        private function getConferencelist(){
		
	        $list = [];
	        
	        foreach(ConferenceManager::getConferences(['status' => 'submission_possible']) as $id){
	                
	                $conference = ConferenceManager::getConference($id);
	                $list[$id] = $conference->getElement('title');
                }
		
		return $list;
	}
}
