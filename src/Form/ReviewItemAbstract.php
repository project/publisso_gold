<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\ReviewItem.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Ajax\AjaxResponse;
        use Drupal\Core\Ajax\CloseModalDialogCommand;
        use Drupal\Core\Ajax\CssCommand;
        use Drupal\Core\Ajax\HtmlCommand;
        use Drupal\Core\Ajax\RedirectCommand;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Database\Connection;
        use Drupal\publisso_gold\Classes\Ajax\TimeoutHtmlCommand;
        use http\Url;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use \Drupal\publisso_gold\Controller\WorkflowInfoMail;
        use \Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use \Drupal\publisso_gold\Controller\Publisso;
        
        /**
         * Provides a simple example form.
         */
        class ReviewItemAbstract extends FormBase {
                
                private $modname = 'publisso_gold';
                private $database;
                
                public function __construct ( Connection $database ) {
                        $this->database = $database;
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return ReviewItemAbstract|static
                 */
                public static function create (ContainerInterface $container ) {
                        return new static( $container->get( 'database' ) );
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'reviewitemabstract';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state, $wf_id = null ) {

                        $form_state->setCached( false );

                        $tools = \Drupal::service( 'publisso_gold.tools' );
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        $session = \Drupal::service( 'session' );
                        
                        if ( !$form_state->has( 'wf_id' ) ) $form_state->set( 'wf_id', $wf_id );
                        $workflow = WorkflowManager::getItem ( $form_state->get( 'wf_id' ) );
                        $medium = $workflow->getMedium();
                        
                        if ( !$form_state->has( 'rs_id' ) ) $form_state->set( 'rs_id', $workflow->getReviewsheet( $session->get( 'user' )[ 'id' ], 'id' ) );
                        
                        $doc = new \DOMDocument();
                        $doc->loadXML( $workflow->getReviewsheetByID( $form_state->get( 'rs_id' ), 'reviewsheet' ) );
                        $xpath = new \DOMXpath( $doc );
                        
                        if ( !$form_state->has( 'doc' ) ) $form_state->set( 'doc', $doc );
                        
                        /**
                         * First, display little prosa for this workflow-item
                         * i.e. metadata, files, comments and so on...
                         **/
                        
                        $meta = [];
                        
                        $form[ 'info' ] = [];
                        foreach ( $meta as $_ => $__ ) {
                                
                                if ( is_array( $__ ) )
                                        $form[ 'info' ][ $_ ] = $workflow->getRenderableTemplate( $_, $__ );
                                else
                                        $form[ 'info' ][ $_ ] = $workflow->getRenderableTemplate( $__ );
                        }
                        
                        $form['info']['reviewsheets'] = $workflow->getReviewsheetForm([ 'uid' => $session->get( 'user' )[ 'id' ], 'state' => 'finished' ]);
                        
                        /**
                         * Display the original text from author. If changing of this text is allowed,
                         * create an editable text, else display it like the abstract
                         **/
                        #$form[ 'orig_text' ] = $this->getWorkflowOrigtext( $workflow, $medium );
                        
                        if ( $xpath->evaluate( '/reviewsheet/workflowdata/submission_text' )->length && $form[ 'orig_text' ][ '#type' ] == 'textarea' ) {
                                $form[ 'orig_text' ][ '#default_value' ] = base64_decode(
                                        $xpath->evaluate( '/reviewsheet/workflowdata/submission_text' )[ 0 ]->nodeValue
                                );
                        }

                        /**
                         * Display the review-sheet
                         **/
                        $form[ 'sheet' ] = [
                                '#tree'   => true,
                                'content' => $this->getForm( $form_state ),
                                '#prefix' => '<div id="rwFakePage">',
                                '#suffix' => '</div>',
                        ];
                        
                        $form[ 'sep' ] = [
                                '#markup' => '<hr>',
                        ];
                        
                        /**
                         * Build the section for comment for author ans recommendation
                         **/
                        $form[ 'comments' ] = [
                                '#type'          => 'container',
                                '#tree'          => true,
                                'for_author'     => [
                                        '#type'  => 'textarea',
                                        '#title' => (string)t( $texts->get( 'workflow.item.review.field.comment_for_author.title', 'fc' ) ),
                                ],
                                'for_editor'     => [
                                        '#type'  => 'textarea',
                                        '#title' => (string)t( $texts->get( 'workflow.item.review.field.comment_for_editor.title', 'fc' ) ),
                                ],
                                'recommendation' => [
                                        '#type'     => 'select',
                                        '#title'    => (string)t( $texts->get( 'workflow.item.review.field.recommendation.title', 'fc' ) ),
                                        '#options'  => [
                                                ''         => (string)t( $texts->get( 'workflow.item.review.field.recommendation.option.empty' ) ),
                                                'reject'   => (string)t( $texts->get( 'workflow.item.review.field.recommendation.option.reject', 'fc' ) ),
                                                'accept'   => (string)t( $texts->get( 'workflow.item.review.field.recommendation.option.accept', 'fc' ) ),
                                                'revision' => (string)t( $texts->get( 'workflow.item.review.field.recommendation.option.revision', 'fc' ) ),
                                        ],
                                        '#required' => false,
                                ],
                        ];
                        
                        if ( $xpath->evaluate( '/reviewsheet/comments/comment_for_author' )->length ) {
                                $form[ 'comments' ][ 'for_author' ][ '#default_value' ] = $xpath->evaluate( '/reviewsheet/comments/comment_for_author' )[ 0 ]->nodeValue;
                        }
                        
                        if ( $xpath->evaluate( '/reviewsheet/comments/comment_for_editor' )->length ) {
                                $form[ 'comments' ][ 'for_editor' ][ '#default_value' ] = $xpath->evaluate( '/reviewsheet/comments/comment_for_editor' )[ 0 ]->nodeValue;
                        }
                        
                        if ( $xpath->evaluate( '/reviewsheet/comments/recommendation' )->length ) {
                                $form[ 'comments' ][ 'recommendation' ][ '#default_value' ] = $xpath->evaluate( '/reviewsheet/comments/recommendation' )[ 0 ]->nodeValue;
                        }
                        
                        /**
                         * Build the rest of the form (seperators, buttons, ...)
                         **/
                        $form[ 'sep' ] = [
                                '#markup' => '<hr>',
                        ];
        
                        $form['messages'] = [
                                '#type' => '#markup',
                                '#markup' => '&nbsp;',
                                '#prefix' => '<div id="messages">',
                                '#suffix' => '</div><br>'
                        ];
                        
                        $form[ 'buttons' ] = $this->getActions();
                        
                        $form[ 'sep' ] = [
                                '#markup' => '<br><br>',
                        ];
                        
                        $form[ '#cache' ][ 'max-age' ] = 0;
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $button = $form_state->getTriggeringElement();
                        $action = $button['#attributes'][ 'action' ];
                        
                        $data = [];
                        
                        if ( $button[ '#limit_validation_errors' ] === false ) {
                                $data = $form_state->getValues();
                        }
                        else {
                                $data = $form_state->getUserInput();
                        }
                        //$this->$action($form, $form_state, $data);
                        
                        try {
                                return $this->$action( $form, $form_state, $data );
                        }
                        catch ( \Error $e ) {
                                
                                Publisso::log( "Error in file >>" . $e->getFile() . "<< Line " . $e->getLine() . ": " . $e->getMessage() );
                                \Drupal::service( 'messenger' )->addMessage( (string)t( \Drupal::service( 'publisso_gold.texts' )->get( 'global.message.system.function_not_implemented', 'fc' ) ), 'error' );
                        }
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @param array $form_data
                 * @return AjaxResponse
                 */
                private function back (array &$form, FormStateInterface $form_state, $form_data = [] ) {
                        
                        $form_data = $form_state->getUserInput();
                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $form_state->get( 'wf_id' ) );
                        $workflow->setReviewState( 'progress' );
                        $this->save( $form, $form_state, $form_data, false );
                        if ( $workflow->locked() ) $workflow->unlock();
                        $form_state->setRedirect( 'publisso_gold.dashboard' );
                        
                        $response = new AjaxResponse();
                        $response->addCommand(new CloseModalDialogCommand());
                        return $response;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @param array $form_data
                 * @return array
                 * @throws \Exception
                 */
                private function finish (array &$form, FormStateInterface $form_state, $form_data = [] ) {
                        
                        $session = \Drupal::service( 'session' );
                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $form_state->get( 'wf_id' ) );
                        
                        $this->save( $form, $form_state, $form_data, false );
                        
                        //load finally reviewsheet with all saved things
                        $doc = new \DOMDocument();
                        $doc->loadXML( $workflow->getReviewsheet() );
                        $xpath = new \DOMXpath( $doc );
                        
                        //save comment for the author
                        if ( $xpath->evaluate( '/reviewsheet/comments/comment_for_author' )->length ) {
                                
                                $comment = $xpath->evaluate( '/reviewsheet/comments/comment_for_author' )[ 0 ]->nodeValue;
                                
                                if ( !empty( $comment ) ) {
                                        
                                        $comment = [
                                                'cfa_created_by_uid' => $session->get( 'user' )[ 'id' ],
                                                'cfa_for_uid'        => $workflow->getElement( 'created_by_uid' ),
                                                'cfa_comment'        => base64_encode( $comment ),
                                                'cfa_wfid'           => $form_state->get( 'wf_id' ),
                                        ];
                                        
                                        \Drupal::database()
                                               ->insert( 'rwPubgoldWorkflowCommentsForAuthors' )
                                               ->fields( $comment )
                                               ->execute()
                                        ;
                                }
                        }
                        
                        //save comment for the editor
                        if ( $xpath->evaluate( '/reviewsheet/comments/comment_for_editor' )->length ) {
                                
                                $comment = $xpath->evaluate( '/reviewsheet/comments/comment_for_editor' )[ 0 ]->nodeValue;
                                
                                if ( !empty( $comment ) ) {
                                        
                                        $comment = [
                                                'wfc_created_by_uid' => $session->get( 'user' )[ 'id' ],
                                                'wfc_comment'        => base64_encode( $comment ),
                                                'wfc_wfid'           => $form_state->get( 'wf_id' ),
                                        ];
                                        
                                        \Drupal::database()
                                               ->insert( 'rwPubgoldWorkflowComments' )
                                               ->fields( $comment )
                                               ->execute()
                                        ;
                                }
                        }
                        
                        //save recommendation
                        if ( $xpath->evaluate( '/reviewsheet/comments/recommendation' )->length ) {
                                
                                $comment = $xpath->evaluate( '/reviewsheet/comments/recommendation' )[ 0 ]->nodeValue;
                                
                                if ( !empty( $comment ) ) {
                                        
                                        \Drupal::database()->insert( 'rwPubgoldWorkflowRecommendations' )
                                               ->fields( [
                                                                 'wf_id'          => $form_state->get( 'wf_id' ),
                                                                 'created_by_uid' => $session->get( 'user' )[ 'id' ],
                                                                 'recommendation' => $comment,
                                                         ]
                                               )
                                               ->execute()
                                        ;
                                }
                        }
                        
                        $workflow->finishReview();
                        if ( $workflow->locked() ) $workflow->unlock();
                        $form_state->setRedirect( 'publisso_gold.dashboard' );
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @param array $form_data
                 * @param bool $direct
                 * @return array|AjaxResponse
                 */
                private function save (array &$form, FormStateInterface $form_state, $form_data = [], $direct = true ) {
                        
                        $sheet = $form_data[ 'sheet' ][ 'content' ];
                        $comments = $form_data[ 'comments' ];
                        $doc = $form_state->get( 'doc' );
                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $form_state->get( 'wf_id' ) );
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        
                        $xpath = new \DOMXpath( $doc );
                        $tools = \Drupal::service( 'publisso_gold.tools' );
                        
                        foreach ( $xpath->evaluate( '/reviewsheet/elements/element' ) as $id => $element ) {
                                
                                if(!array_key_exists($id + 1, $sheet)) continue;
                                $data = $sheet[ $id + 1 ]; //hier plus eins, da ein Metafeld (Beschreibung) als erstes Form-Element hinzukommt und die Elemente selbst einen Zähler nach hinten rutschen.
                                
                                if ( is_array( $data[0] ) ) { //if data is a structure (i.e. if it has sub-elements)
                                        
                                        if ( array_key_exists( 'subelements', $data[0] ) ) { //sub-elements have to exist
                                                
                                                foreach ( $xpath->evaluate( 'subelements/subelement', $element ) as $sub_id => $sub_element ) { //walk the subelements (id = position)
                                                        
                                                        if(!array_key_exists($sub_id, $data[0]['subelements'])) continue;
                                                        $sub_data = $data[0][ 'subelements' ][ $sub_id ][0]; //get matching form-data-element (id = position)
                                                        
                                                        //get list of value-nodes (should be none ore one)
                                                        $value_nodes = $xpath->evaluate( 'value', $sub_element );
                                                        
                                                        if ( $value_nodes->length > 0 ) //if value-node already exists, update
                                                                $value_nodes[ 0 ]->nodeValue = $sub_data;
                                                        else //append value-node
                                                                $tools->DOMAppendChild( 'value', $sub_data, [], $doc, $sub_element );
                                                }
                                        }
                                }
                                else { //data sould be a value - no subelements given
                                        
                                        //get list of value-nodes (should be none ore one)
                                        $value_nodes = $xpath->evaluate( 'value', $element );
                                        
                                        if ( $value_nodes->length > 0 ) //if value-node already exists, update
                                                $value_nodes[ 0 ]->nodeValue = $data[0];
                                        else //append value-node
                                                $tools->DOMAppendChild( 'value', $data[0], [], $doc, $element );
                                }
                        }
                        
                        /**
                         * Save the comment(s) and recommendation
                         **/
                        if ( !$xpath->evaluate( '/reviewsheet/comments' )->length ) {
                                
                                //if no comments added to the review, create root-element "comments"
                                $tools->DOMAppendChild( 'comments', '', [], $doc, $xpath->evaluate( '/reviewsheet' )[ 0 ] );
                                //append the comments for the author
                                $tools->DOMAppendChild( 'comment_for_author', $comments[ 'for_author' ], [], $doc, $xpath->evaluate( '/reviewsheet/comments' )[ 0 ] );
                                //append the comments for the editor
                                $tools->DOMAppendChild( 'comment_for_editor', $comments[ 'for_editor' ], [], $doc, $xpath->evaluate( '/reviewsheet/comments' )[ 0 ] );
                                //append the recommendation
                                $tools->DOMAppendChild( 'recommendation', $comments[ 'recommendation' ], [], $doc, $xpath->evaluate( '/reviewsheet/comments' )[ 0 ] );
                        }
                        else {
                                //if comments are already saved - update
                                //check, if comment for the author already exists (normally impossible, but wei'll be sure),
                                if ( !( $xpath->evaluate( '/reviewsheet/comments/comment_for_author' )->length ) ) {
                                        //append node for comment for author
                                        $tools->DOMAppendChild( 'comment_for_author', $comments[ 'for_author' ], [], $doc, $xpath->evaluate( '/reviewsheet/comments' )[ 0 ] );
                                }
                                else {
                                        //update node comment for author
                                        $xpath->evaluate( '/reviewsheet/comments/comment_for_author' )[ 0 ]->nodeValue = $comments[ 'for_author' ];
                                }
                                
                                //if comments are already saved - update
                                //check, if comment for the editor already exists (normally impossible, but wei'll be sure),
                                if ( !( $xpath->evaluate( '/reviewsheet/comments/comment_for_editor' )->length ) ) {
                                        //append node for comment for editor
                                        $tools->DOMAppendChild( 'comment_for_editor', $comments[ 'for_editor' ], [], $doc, $xpath->evaluate( '/reviewsheet/comments' )[ 0 ] );
                                }
                                else {
                                        //update node comment for author
                                        $xpath->evaluate( '/reviewsheet/comments/comment_for_editor' )[ 0 ]->nodeValue = $comments[ 'for_editor' ];
                                }
                                
                                //check, if recommendation already exists (normally impossible, but wei'll be sure),
                                if ( !( $xpath->evaluate( '/reviewsheet/comments/recommendation' )->length ) ) {
                                        //append node recommendation
                                        $tools->DOMAppendChild( 'recommendation', $comments[ 'recommendation' ], [], $doc, $xpath->evaluate( '/reviewsheet/comments' )[ 0 ] );
                                }
                                else {
                                        //update node recommendation
                                        $xpath->evaluate( '/reviewsheet/comments/recommendation' )[ 0 ]->nodeValue = $comments[ 'recommendation' ];
                                }
                        }
                        
                        /**
                         * If changes on original text allowed, save it too
                         **/
                        if ( array_key_exists( 'orig_text', $form_data ) ) {
                                
                                //if the structure has no element to store the workflow-data
                                if ( !( $xpath->evaluate( '/reviewsheet/workflowdata/' )->length ) ) {
                                        $tools->DOMAppendChild( 'workflowdata', '', [], $doc, $xpath->evaluate( '/reviewsheet' )[ 0 ] );
                                }
                                
                                if ( !( $xpath->evaluate( '/reviewsheet/workflowdata/submission_text' )->length ) ) {
                                        //append node submission_text
                                        $tools->DOMAppendChild( 'submission_text', base64_encode( $form_data[ 'orig_text' ] ), [], $doc, $xpath->evaluate( '/reviewsheet/workflowdata' )[ 0 ] );
                                }
                                else {
                                        //update node recommendation
                                        $xpath->evaluate( '/reviewsheet/workflowdata/submission_text' )[ 0 ]->nodeValue = base64_encode( $form_data[ 'orig_text' ] );
                                }
                        }
                        
                        /**
                         * Save the current review
                         **/
                        $workflow->updateReview( $doc->saveXML() );
                        
                        if ( $direct === false ) { //if this function is called implicit
                                
                                /**
                                 * Inform the user of state
                                 **/
                                \Drupal::service( 'messenger' )->addMessage( $texts->get( 'workflow.item.review.message.saved', 'fc' ) );
                                return $form;
                        }
                        
                        $workflow->setReviewState( 'progress' );
        
                        $response = new AjaxResponse();
                        $response->addCommand(new CssCommand('#messages', ['color' => 'darkgreen']));
                        $response->addCommand(new HtmlCommand('#messages', 'Successfully saved!'));
                        $response->addCommand(new TimeoutHtmlCommand('#messages', 2000, '&nbsp;'));
                        
                        return $response;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return AjaxResponse
                 */
                private function reject (array &$form, FormStateInterface $form_state ) {
                        Publisso::log('Reject review');
                        $currentUser = Publisso::currentUser();
                        $objWorkflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        
                        $objWorkflow->finishReview($currentUser->getId(), 'rejected');
                        $objWorkflow->unlock();
                        $form_state->setRedirect( 'publisso_gold.dashboard' );
                        
                        $response = new AjaxResponse();
                        
                        $url = \Drupal\Core\Url::fromRoute('publisso_gold.dashboard')->toString();
                        $response->addCommand(new RedirectCommand($url));
                        return $response;
                }
        
                /**
                 * @return array
                 */
                private function getActions () {
                        
                        $ary = [];
                        
                        $ary[ 'reject_review' ] = [
                                '#type'                    => 'button',
                                '#value'                   => (string)t( \Drupal::service( 'publisso_gold.texts' )->get( 'workflow.item.review.button_reject.title', 'fc' ) ),
                                '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                                '#attributes'              => [
                                        'onclick' => 'if(!confirm("' . ( (string)t( \Drupal::service( 'publisso_gold.texts' )->get( 'workflow.item.review.button_reject.confirm', 'fc' ) ) ) . '")){return false;}',
                                ],
                                '#limit_validation_errors' => [],
                                '#attributes' => [
                                        'action'                   => 'reject',
                                ],
                                '#button_type'             => 'danger',
                                '#ajax' => [
                                        'callback' => '::submitForm',
                                        'progress' => [
                                                'type' => 'none'
                                        ]
                                ]
                        ];
                        
                        $ary[ 'back' ] = [
                                '#type'                    => 'button',
                                '#value'                   => (string)t( \Drupal::service( 'publisso_gold.texts' )->get( 'workflow.item.review.button_back.title', 'fc' ) ),
                                '#limit_validation_errors' => [],
                                '#attributes' => [
                                        'action'                   => 'back',
                                ],
                                '#button_type'             => 'warning',
                                '#ajax' => [
                                        'callback' => '::submitForm',
                                        'progress' => [
                                                'type' => 'none'
                                        ]
                                ]
                        ];
                        
                        $ary[ 'save' ] = [
                                '#type'                    => 'button',
                                '#value'                   => (string)t( \Drupal::service( 'publisso_gold.texts' )->get( 'workflow.item.review.button_save.title', 'fc' ) ),
                                '#prefix'                  => '&nbsp;&nbsp;&nbsp;',
                                '#attributes' => [
                                        'action'                   => 'save',
                                ],
                                '#limit_validation_errors' => [],
                                '#button_type'             => 'primary',
                                '#ajax' => [
                                        'callback' => '::submitForm',
                                        'progress' => [
                                                'type' => 'none'
                                        ]
                                ]
                        ];
                        
                        return $ary;
                }
        
                /**
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                private function getForm (FormStateInterface $form_state ) {
                        
                        $xpath = new \DOMXpath( $form_state->get( 'doc' ) );
                        $ary = [];
                        
                        $desc = $xpath->evaluate( '/reviewsheet/meta/description' )->item( 0 );
                        if ( $desc ) $ary[] = [
                                '#markup' => $desc->nodeValue,
                                '#suffix' => '</div>',
                                '#prefix' => '<div class="alert alert-info">',
                        ];
                        
                        foreach ( $xpath->evaluate( '/reviewsheet/elements/element' ) as $element ) {
                                $ary[] = $this->parseElement( $element, $xpath );
                        }
                        
                        return $ary;
                }
        
                /**
                 * @param \DOMElement $element
                 * @param \DOMXpath $xpath
                 * @param string[] $divider
                 * @return array
                 */
                private function parseElement (\DOMElement &$element, \DOMXpath &$xpath, $divider = ['#markup' => '<hr>' ] ) {
                        
                        $ary = [];
                        
                        switch ( $element->getAttribute( 'type' ) ) {
                                
                                case 'item':
                                        $ary = [
                                                '#type'   => 'markup',
                                                '#prefix' => '<p>',
                                                '#suffix' => '</p>',
                                        ];
                                        break;
                                
                                case 'heading':
                                        $ary = [
                                                '#type'   => 'markup',
                                                '#prefix' => '<h2>',
                                                '#suffix' => '</h2>',
                                        ];
                                        break;
                                
                                case 'subheading':
                                        $ary = [
                                                '#type'   => 'markup',
                                                '#prefix' => '<h3>',
                                                '#suffix' => '</h3>',
                                        ];
                                        break;
                                
                                case 'checkbox':
                                        $ary = [
                                                '#type' => 'checkbox',
                                        ];
                                        break;
                                
                                case 'textfield':
                                        $ary = [
                                                '#type' => 'textfield',
                                                '#maxlength' => 2000
                                        ];
                                        break;
                        }
                        
                        $text = $xpath->evaluate( 'text', $element )[ 0 ];
                        
                        if ( $xpath->evaluate( 'value', $element )->length )
                                $ary[ '#default_value' ] = $xpath->evaluate( 'value', $element )[ 0 ]->nodeValue;
                        
                        if ( $ary[ '#type' ] == 'markup' ) {
                                $ary[ '#markup' ] = $text->nodeValue;
                                $ary[ '#prefix' ] = '<div class="rwTextBold">';
                                $ary[ '#suffix' ] = '</div>';
                        }
                        else {
                                $ary[ '#title' ] = $text->nodeValue;
                                
                                $ary[ '#label_attributes' ] = [
                                        'class' => [
                                                'rwTextNormal',
                                        ],
                                ];
                        }
                        
                        if ( $xpath->evaluate( 'subelements/subelement', $element )->length ) {
                                
                                $ary[ 'subelements' ] = [];
                                
                                foreach ( $xpath->evaluate( 'subelements/subelement', $element ) as $subnode ) {
                                        $ary[ 'subelements' ][] = $this->parseElement( $subnode, $xpath, null );
                                }
                        }
                        
                        return [ $ary, $divider ];
                }
        
                /**
                 * @param $workflow
                 * @param $medium
                 * @return array
                 */
                private function getWorkflowOrigtext (&$workflow, &$medium ) {
                        
                        if ( $medium->getControlElement( 'review_change_origtext' ) == 1 ) {
                                
                                return [
                                        '#type'          => 'textarea',
                                        '#default_value' => $workflow->getDataElement( ':orig_text' ),
                                        '#title'         => (string)t( \Drupal::service( 'publisso_gold.texts' )->get( 'workflow.item.infobox.headline.orig_text', 'fc' ) ),
                                        '#attributes'    => [
                                                'class' => [
                                                        \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' ),
                                                ],
                                        ],
                                ];
                        }
                        else {
                                return [
                                        '#type'        => 'details',
                                        '#open'        => false,
                                        'content'      => [
                                                '#type'     => 'inline_template',
                                                '#template' => $workflow->getDataElement( ':orig_text' ),
                                        ],
                                        '#title'       => (string)t( \Drupal::service( 'publisso_gold.texts' )->get( 'workflow.item.infobox.headline.orig_text', 'fc' ) ),
                                        '#description' => (string)t( \Drupal::service( 'publisso_gold.texts' )->get( 'workflow.item.infobox.description.orig_text', 'fco' ) ),
                                ];
                        }
                }
        }
