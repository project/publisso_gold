<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldJournalmanagementPublishJournalArticle.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldJournalmanagementPublishJournalArticle extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $num_corporations;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldJournalmanagementPublishJournalArticle|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldjournalmanagementpublishjournalarticle';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        $args = $form_state->getBuildInfo();
        
        if(count($args)){
            $wf_id  = $args['args'][0]['wf_id' ];
            $jrn_id = $args['args'][0]['jrn_id'];
        }
        
        if(!$wf_id)
            $wf_id = $form_state->get('wf_id');
        
        if(!$jrn_id)
            $jrn_id = $form_state->get('jrn_id');
        
        if($wf_id && $jrn_id){
            
            $form_state->set('wf_id' , $wf_id );
            $form_state->set('jrn_id', $jrn_id);
            
            $workflow = getDashboard(\Drupal::database(), $wf_id);
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            
            $delta = ['workflow', 'workflow_data'];
            foreach($delta as $_) $form_state->set($_, $$_);
            
            $form['submit'] = [
                    '#type' => 'submit',
                    '#value' => (string)t('Publish Article'),
					'#button_type' => 'success',
					'#suffix' => '<br><br>'
            ];
        }
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $delta = ['workflow', 'workflow_data', 'wf_id', 'bk_id'];
        foreach($delta as $_) $$_ = $form_state->get($_);
        
		$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
		$objWorkflow->historize();
		$objWorkflow->getElement('type') == 'jas' ? $objWorkflow->publish() : $objWorkflow->publishErratum();
		$objWorkflow->unlock();
        $form_state->setRedirect('publisso_gold.dashboard');
        
        return $form;
    }
        
        /**
         * @param null $tmpl
         * @param null $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl = NULL, $vars = NULL){
        
        if($tmpl == NULL || $vars == NULL){
            //set Site-Vars
            $this->tmpl = str_replace(array_keys($this->tmpl_vars), array_values($this->tmpl_vars), $this->tmpl);
            
            //remove unused vars
            $this->tmpl = preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl);
        }
        else{
            //set Site-Vars
            $tmpl = str_replace(array_keys($vars), array_values($vars), $tmpl);
            
            //remove unused vars
            return preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $tmpl);
        }
    }
}
