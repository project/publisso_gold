<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldBookmanagementFormalAdaptBookChapter.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldBookmanagementFormalAdaptBookChapter extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldBookmanagementFormalAdaptBookChapter|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldbookmanagementformaladaptbookchapter';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $this->modpath = drupal_get_path('module', $this->modname);
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
        //expect build-info-args as array
        $build_info = $form_state->getBuildInfo();
        $args = $build_info['args'];
        
        $form_state->set('bk_id', $args[0]['bk_id']);
        $form_state->set('wf_id', $args[0]['wf_id']);
        
        if(!$form_state->get('bk_id')){
            drupal_set_message(t('No book given!'), 'error');
            return array();
        }
        
        $book = $this->database->select('rwPubgoldBooks', 'bk')->fields('bk', [])->condition('bk_id', $form_state->get('bk_id'), '=')->execute()->fetchAssoc();
        
        if($form_state->get('wf_id')){
            $workflow = getDashboard(\Drupal::Database(), $form_state->get('wf_id'));
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            $chapter_text = json_decode(base64_decode($workflow_data->chapter_text));
            $abstract = json_decode(base64_decode($workflow_data->abstract));
            
            $delta = ['workflow', 'workflow_data'];
            foreach($delta as $_) $form_state->set($_, $$_);
			
			$new_wf = \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
        }
        /*
        $form = \Drupal::formBuilder()
            ->getForm(
                "Drupal\\".$this->modname."\Form\publisso_goldBookmanagementAddChapter",
                [
                    'bk_id' => $form_state->get('bk_id'),
                    'wf_id' => $form_state->get('wf_id')
                ]
            );
        */
        $author = getAllUserData(\Drupal::database(), $workflow_data->author_uid);
        
        $more_authors = [];
        
        if(isset($workflow_data->more_authors)){
          
          $_ = json_decode($workflow_data->more_authors);
          $delta = 1;
          foreach($_ as $_author){
            
            $more_authors['content'][] = array(
                '#type' => 'fieldset',
                '#title' => (string)t('Author').' #'.(string)($delta),
                'content' => [
                    'more_author_name~'.$delta => [
                        '#type' => 'textfield',
                        '#title' => (string)t('Name'),
                        '#default_value' => $_author->more_author_name
                    ],
                    
                    'more_author_lastname~'.$delta => [
                        '#type' => 'textfield',
                        '#title' => (string)t('Lastname'),
                        '#default_value' => $_author->more_author_lastname
                    ],
                    
                    'more_author_affiliation~'.$delta => [
                        '#type' => 'textfield',
                        '#title' => (string)t('Affiliation'),
                        '#default_value' => $_author->more_author_affiliation
                    ]
                ]
            );
            
            $delta++;
          }
        }
        
        $form = [
            
            'site_headline' => [
            '#type' => 'markup',
            '#markup' => '<h1>'.t('Formal Adapt Chapter').'</h1>'
            ],
            
            'book_title' => [
                '#type' => 'markup',
                '#markup' => '<h2>'.$book['bk_title'].'</h2>'
            ],
            
            'author' => [
                '#type' => 'markup',
                '#markup' => $author['profile_graduation'].' '.$author['profile_lastname'].', '.$author['profile_firstname'].' '.$author['profile_graduation_suffix'],
                '#prefix' => '<h4>'.(string)t('Author').': ',
                '#suffix' => '</h4>'
            ],
            
            'title' => [
                '#type' => 'textfield',
                '#title' => t('Title'),
                '#required' => true,
                '#default_value' => $new_wf->getDataElement('title')
            ],
            
            'abstract' => [
                '#type' => 'text_format',
                '#title' => t('Abstract'),
                '#required' => true,
                '#format' => isset($abstract) ? $abstract->format : 'basic_html',
                '#default_value' => isset($abstract) ? $abstract->value : ''
            ],
            
			'keywords' => [
				'#type' => 'textfield',
				'#title' => (string)t('Keywords'),
				'#default_value' => isset($workflow_data->keywords) ? implode(', ', json_decode($workflow_data->keywords, true)) : '',
				'#prefix' => '<br>'
			],
			
            'chapter_text' => [
                '#type' => 'text_format',
                '#title' => t('Article Text'),
                '#required' => true,
                '#rows' => 50,
                '#format' => isset($chapter_text) ? $chapter_text->format : 'full_html',
                '#default_value' => isset($chapter_text) ? $chapter_text->value : ''
            ],
            
            'authors' => [
                '#type' => 'select',
                '#multiple' => true,
                '#options' => getUsersByRole(\Drupal::database(), [2,3,4,5,6,7]),
                '#title'=> (string)t('Authors'),
                '#default_value' => isset($workflow_data->authors) ? explode(',', $workflow_data->authors) : []
            ],
            
            'more_authors' => $more_authors,
            
            'corresponding_authors' => [
                '#type' => 'select',
                '#multiple' => true,
                '#options' => getUsersByRole(\Drupal::database(), [2,3,4,5,6,7]),
                '#title' => t('Corresponding Author(s)'),
                '#required' => true,
                '#default_value' => isset($workflow_data->corresponding_authors) ? explode(',', $workflow_data->corresponding_authors) : ''
            ],
            
            'corporation' => [
                '#type' => 'textfield',
                '#title' => t('Corporation'),
                '#default_value' => isset($workflow_data->corporation) ? $workflow_data->corporation : ''
            ],
            
            'doi' => [
                '#type' => 'textfield',
                '#title' => (string)t('DOI'),
                '#default_value' => isset($workflow_data->doi) ? $workflow_data->doi : ''
            ],
            
            'version' => [
                '#type' => 'textfield',
                '#title' => (string)t('Version'),
                '#default_value' => isset($workflow_data->version) ? $workflow_data->version : ''
            ],
            
            'conflict_of_interest' => [
                '#type' => 'checkbox',
                '#title' => (string)t('Conflict of interest'),
                '#default_value' => isset($workflow_data->conflict_of_interest) ? $workflow_data->conflict_of_interest : 0,
                '#required' => false
            ],
            
            'conflict_of_interest_text' => [
                '#type' => 'textfield',
                '#title' => t('Description conflict of interest'),
                '#default_value' => isset($workflow_data->conflict_of_interest_text) ? $workflow_data->conflict_of_interest_text : ''
            ],
            
            'accept_policy' => [
                '#type' => 'markup',
                '#markup' => (string)t('I accept the policy and the author\'s contract').': '. ($workflow_data->accept_policy ? (string)t('Yes') : (string)t('No')).'<br><br>'
            ],
            
            'funding_name' => [
                '#type' => 'textfield',
                '#title' => t('Funing Name'),
                '#default_value' => isset($workflow_data->funding_name) ? $workflow_data->funding_name : ''
            ],
            
            'funding_id' => [
                '#type' => 'textfield',
                '#title' => t('Funing ID'),
                '#default_value' => isset($workflow_data->funding_id) ? $workflow_data->funding_id : ''
            ],
            
            'keywords' => [
                '#type' => 'textfield',
                '#title' => t('Keywords'),
                '#default_value' => isset($workflow_data->keywords) ? implode(', ', json_decode($workflow_data->keywords)) : ''
            ],
            
            'license' => [
                '#type' => 'textfield',
                '#title' => t('License'),
                '#required' => true,
                '#default_value' => isset($workflow_data->license) ? $workflow_data->license : ''
            ],
            
            'publication_place' => [
                '#type' => 'textfield',
                '#title' => t('Publication place'),
                '#required' => true,
                '#default_value' => $book['bk_publication_place'] /*isset($workflow_data->publication_year) ? $workflow_data->publication_year : ''*/
            ],
            
            'publication_year' => [
                '#type' => 'textfield',
                '#title' => t('Publication year'),
                '#required' => true,
                '#default_value' => $book['bk_begin_publication'] /*isset($workflow_data->publication_year) ? $workflow_data->publication_year : ''*/
            ],
            
            'sub_category' => [
                '#type' => 'textfield',
                '#title' => t('Subcategory'),
                '#required' => true,
                '#default_value' => $book['bk_sub_category'] /*isset($workflow_data->sub_category) ? $workflow_data->sub_category : ''*/
            ],
            
            'comment' => [
                '#type' => 'text_format',
                '#format' => 'basic_html',
                '#title' => (string)t('Comment')
            ],
            
            'submit' => [
                '#type' => 'submit',
                '#value' => (string)t('Request clearing from author')
            ]
            
        ];
        
        $form_state->disableCache();
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $more_authors = [];
        //find more-authors
        foreach($form_state->getValues() as $k => $v){
          
          if(stristr($k, 'more_author_')){
            $k = explode('~', $k);
            $more_authors[$k[1] - 1][$k[0]] = $v;
          }
        }
        
        $delta = ['workflow', 'workflow_data'];
        foreach($delta as $_) $$_ = $form_state->get($_);
        
        //save comment if esists

            
        $comment = $form_state->getValue('comment');
        
        if($comment['value'] != ''){
            
            $comment = base64_encode(json_encode($comment));
            
            \Drupal::database()
                ->insert('rwPubgoldWorkflowComments')
                ->fields([
                    'wfc_created_by_uid' => $_SESSION['user']['id'],
                    'wfc_comment' => $comment,
                    'wfc_wfid' => $workflow->wf_id
                ])
                ->execute();
        }

        
        //prepare workflow for history
        $workflow_history = array();
        $workflow_history['wfh_created_by_uid'] = $_SESSION['user']['id'];
        
        foreach($workflow as $k => $v){
            
            if($k == 'wf_id'){
                $key = 'wfh_wfid';
            }
            else{
                
                $key = explode("_", $k, 2);
                $key = $key[0].'h_'.$key[1];
                
            }
            
            $workflow_history[$key] = $v;
        }
        
        \Drupal::database()->insert('rwPubgoldWorkflowHistory')->fields($workflow_history)->execute();
        
        foreach($form_state->getValues() as $k => $v){
          $workflow_data->$k = $v;
        }
        
        $workflow_data->abstract               = base64_encode(json_encode($form_state->getValue('abstract')));
        $workflow_data->chapter_text           = base64_encode(json_encode($form_state->getValue('chapter_text')));
        $workflow_data->errata                 = base64_encode(json_encode($form_state->getValue('errata')));
        $workflow_data->correction             = base64_encode(json_encode($form_state->getValue('correction')));
        $workflow_data->license                = $form_state->getValue('license');
        $workflow_data->publication_place      = $form_state->getValue('publication_place');
        $workflow_data->publisher              = $form_state->getValue('publisher');
        $workflow_data->doi                    = $form_state->getValue('doi');
        $workflow_data->ddc                    = $form_state->getValue('ddc');
        $workflow_data->corresponding_authors  = implode(',', $form_state->getValue('corresponding_authors'));
        $workflow_data->urn                    = $form_state->getValue('urn');
        $workflow_data->keywords               = json_encode(preg_split('/[^a-zA-Z0-9äöüÄÖÜß]+/', $form_state->getValue('keywords')));
        $workflow_data->funding_name           = $form_state->getValue('funding_name');
        $workflow_data->funding_id             = $form_state->getValue('funding_id');
        $workflow_data->version                = $form_state->getValue('version');
        $workflow_data->title                  = $form_state->getValue('title');
        $workflow_data->doc_number             = $form_state->getValue('doc_number');
        $workflow_data->state                  = 'author clearing request';
        $workflow_data->authors                = implode(',', $form_state->getValue('authors'));
        $workflow_data->corporation            = $form_state->getValue('corporation');
        $workflow_data->conflict_of_interest   = $form_state->getValue('conflict_of_interest');
        $workflow_data->conflict_of_interest_text = $form_state->getValue('conflict_of_interest_text');
        $workflow_data->author_contract        = $form_state->getValue('author_contract');
        $workflow_data->sub_category           = $form_state->getValue('sub_category');
        $workflow_dta->more_authors            = json_encode($more_authors);

        $workflow->wf_data = base64_encode(json_encode($workflow_data));
        
        \Drupal::database()
            ->update('rwPubgoldWorkflow')
            ->fields([
                'wf_data' => $workflow->wf_data,
                'wf_assigned_to' => 'u:'.$workflow_data->author_uid,
                'wf_locked' => 0,
                'wf_locked_by_uid' => null
            ])
            ->condition('wf_id', $form_state->get('wf_id'), '=')
            ->execute();
        
        return $form;
    }
}
