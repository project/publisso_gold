<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowEditorActionAfterRevision.
 */
namespace Drupal\publisso_gold\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Drupal\publisso_gold\Controller\WorkflowInfoMail;

/**
 * Provides a simple example form.
 */
class publisso_goldWorkflowEditorActionAfterRevision extends FormBase {
	private $modname = 'publisso_gold';
	private $database;
	private $modpath;
	private $num_corporations;
	public function __construct(Connection $database) {
		$this->database = $database;
	}
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldWorkflowEditorActionAfterRevision|static
         */
        public static function create(ContainerInterface $container) {
		return new static ( $container->get ( 'database' ) );
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function getFormId() {
		return 'publisso_goldworkfloweditoractionafterrevision';
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {
		$this->modpath = drupal_get_path ( 'module', $this->modname );
		
		if ($this->modpath && ! $form_state->get ( 'modpath' )) {
			$form_state->set ( 'modpath', $this->modpath );
		}
		
		if (! $this->modpath && $form_state->get ( 'modpath' )) {
			$this->modpath = $form_state->get ( 'modpath' );
		}
		
		$args = $form_state->getBuildInfo ();
		$wf_id = $args ['args'] [0] ['wf_id'];
		
		if (! $form_state->get ( 'wf_id' ))
			$form_state->set ( 'wf_id', $wf_id );
		
		if (! $form_state->get ( 'step' ))
			$form_state->set ( 'step', 0 );
		
		if ($wf_id) {
			
			$workflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
			
			switch($workflow->getElement('revision_ordered_by')){
				
				case 'eo':
					$actions = [
						'revision' 	=> ( string ) $this->t ( 'Back to author for revision' ),
						'assign_eic'	=> ( string ) $this->t ( 'Assign EiC' ),
						'reject' 	=> ( string ) $this->t ( 'Reject submission' )
					];
					break;
				
				case 'eic':
					
					$actions = [
						'revision' 	=> ( string ) $this->t ( 'Back to author for revision' ),
						'accept'	=> ( string ) $this->t ( 'Accept submission' ),
						'reject' => ( string ) $this->t ( 'Reject submission' ),
						'assign_editor' => ( string ) $this->t ( 'Assign editor' )
					];
					
					if(count($workflow->getUserReviewsheets(['nogroup' == true, 'state' => 'all']))){
						$actions = [
							'revision' 	=> ( string ) $this->t ( 'Back to author for revision' ),
							'accept'	=> ( string ) $this->t ( 'Accept submission' ),
							'reject' => ( string ) $this->t ( 'Reject submission' ),
							'to_editor'	=> ( string ) $this->t ( 'Back to editor' )
						];
					}
					
					break;
				
				case 'editor':
					$actions = [
						'review' => ( string ) t ( 'New review' ),
						'revision' => ( string ) t ( 'Back to author for revision' ),
						'decision_eic' => ( string ) t ( 'Decision EiC' )
					];
					break;
				
				default:
					$actions = [
						'review' => ( string ) t ( 'New review' ),
						'revision' => ( string ) t ( 'Back to author for revision' ),
						'decision_eic' => ( string ) t ( 'Decision EiC' )
					];
			}
   
			if(array_key_exists('to_editor', $actions) && !$workflow->hasEditors()){
			        unset($actions['to_editor']);
                        }
			
			$form ['editor_action'] = [ 
				'#title' => ( string ) t ( 'Select action' ),
				'#type' => 'select',
				'#options' => $actions,
				'#required' => true 
			];
			
			if ($form_state->get ( 'action' )) {
				
				$action = $form_state->get ( 'action' );
				$need_submit = true;
				
				$form ['editor_action'] = [ 
					'#type' => 'markup',
					'#markup' => ( string ) t ( 'Choosed action: ' ) . $actions [$action],
					'#prefix' => '<h3>',
					'#suffix' => '</h3>' 
				];
				
				if ($action == 'revision') {
					
					$form ['comment'] = [ 
						'#title' => ( string ) t ( 'Comment for author' ),
						'#type' => 'textarea',
						'#required' => true 
					];
				} elseif ($action == 'review') {
					
					$form = \Drupal::formBuilder ()->getForm ( "Drupal\\" . $this->modname . "\Form\publisso_goldWorkflowSetReviewers", [ 
						'wf_id' => $form_state->get ( 'wf_id' ) 
					] );
					$need_submit = false;
				} else {
					
					$form ['comment'] = [ 
						'#title' => ( string ) t ( 'Comment' ),
						'#type' => 'textarea',
						'#required' => true 
					];
				}
				
				$form ['back'] = [ 
					'#type' => 'submit',
					'#value' => ( string ) t ( 'Back' ),
					'#submit' => [ 
						'::back' 
					],
					'#prefix' => '',
					'#suffix' => '<br><br>',
					'#limit_validation_errors' => [ ] 
				];
				
				if ($need_submit === true) {
					
					$form ['submit'] = [ 
						'#type' => 'submit',
						'#value' => ( string ) t ( 'Submit' ),
						'#suffix' => '<br><br><br>',
						'#limit_validation_errors' => [ 
							[ 
								'comment' 
							] 
						] 
					];
				}
			}
			
			if ($form_state->get ( 'step' ) == 0) {
				
				$form ['submit'] = [ 
					'#type' => 'submit',
					'#value' => ( string ) t ( 'Submit' ),
					'#suffix' => '<br><br>' 
				];
			}
		}
		
		$form ['#cache'] = [ 
			'max-age' => 0 
		];
		
		return $form;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {
		return $form;
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function back(array $form, FormStateInterface $form_state) {
		$step = $form_state->get ( 'step' );
		
		$form_state->set ( 'action', null );
		$form_state->set ( 'step', -- $step );
		$form_state->setRebuild ();
		
		return $form;
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function setReviewers(array &$form, FormStateInterface $form_state) {
		$session = \Drupal::service ( 'session' );
		$reviewers = $_REQUEST ["reviewers_list"];
		
		$wf_id = $form_state->get ( 'wf_id' );
		
		$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $wf_id );
		$workflow = getDashboard ( \Drupal::database (), $wf_id );
		
		if (count ( $workflow )) {
			
			// inform users about registration if not done
			$assigned_users = [ ];
			
			foreach ( $reviewers as $uid ) {
				
				$_user = new \Drupal\publisso_gold\Controller\User ( $uid );
				
				if ($_user->getElement ( 'reg_notification_sent' ) == 0) {
					$assigned_users [] = $_user;
				}
			}
			
			foreach ( $assigned_users as $_ ) {
				
				// send Reg-Mail to user if not yet be done
				if ($_->getElement ( 'reg_notification_sent' ) == 0) {
					
					$port = \Drupal::request ()->getPort ();
					$host = \Drupal::request ()->getHttpHost ();
					$host = "http" . ($port == 443 ? 's' : '') . "://$host";
					$link = $host;
					$password ['clear'] = genTempPassword ();
					$password ['encrypted'] = '*' . strtoupper ( sha1 ( sha1 ( $user ['password_clear'], true ) ) );
					
					$_->setElement ( 'password', $password ['encrypted'] );
					
					foreach ( [ 
						'registration_password',
						'registration_info' 
					] as $name ) {
						
						$vars = [ 
							'::firstname::' => $_->profile->getElement ( 'firstname' ),
							'::lastname::' => $_->profile->getElement ( 'lastname' ),
							'::link::' => $link,
							'::email::' => $_->profile->getElement ( 'email' ),
							'::user::' => $_->getElement ( 'user' ),
							'::login_link::' => $link . '/publisso_gold/login',
							'::password::' => $password ['clear'] 
						];
						
						sendRegistrationInfoMail ( $name, $vars );
					}
					
					$_->setElement ( 'reg_notification_sent', 1 );
				}
				// - Reg-Mail --
				
				$objWorkflow->newUserReview ( $_ );
			}
			// -- inform users --
			
			$workflow = $workflow [0];
			$workflow_data = json_decode ( base64_decode ( $workflow->wf_data ) );
			$workflow_data->state = 'in review';
			$workflow->wf_data = base64_encode ( json_encode ( $workflow_data ) );
			
			$data = [ 
				'wf_assigned_to_reviewer' => implode ( ',', $reviewers ),
				'wf_assigned_to' => 'u:' . implode ( ',u:', $reviewers ),
				'wf_locked' => 0,
				'wf_locked_by_uid' => NULL,
				'wf_data' => $workflow->wf_data,
				'wf_editor_last_edited' => $session->get ( 'user' ) ['id'],
				'wf_reviewer_edited' => '' 
			];
			
			\Drupal::database ()->update ( 'rwPubgoldWorkflow' )->fields ( $data )->condition ( 'wf_id', $wf_id, '=' )->execute ();
		}
		
		$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
		/*
		$mail = new WorkflowInfoMail ( $objWorkflow, 'assigned to reviewer' );
		if ($mail)
			$mail->send ();
		*/
		// sendWorkflowInfoMail($objWorkflow, 'assigned to reviewer');
		$form_state->setRedirect ( 'publisso_gold.dashboard' );
		return $form;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {
		$session = \Drupal::service ( 'session' );
		$step = $form_state->get ( 'step' );
		
		if ($step == 0) {
			
			$form_state->set ( 'action', $form_state->getValue ( 'editor_action' ) );
			$form_state->set ( 'step', ++ $step );
			$form_state->setRebuild ();
		} elseif ($step == 1 && $form_state->get ( 'action' ) == 'review') {
			
			$workflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
			$workflow->setState ( 'in review', $_REQUEST ['reviewers_list'] );
			$workflow->setElement ( 'editor_last_edited', $session->get ( 'user' ) ['id'] );
			$workflow->unlock ();
			
			$form_state->setRedirect ( 'publisso_gold.dashboard' );
		} elseif ($step == 1 && $form_state->get ( 'action' ) == 'decision_eic') {
			
			$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
			$objWorkflow->setState ( 'decision eic' );
			$objWorkflow->setElement ( 'editor_last_edited', $session->get ( 'user' ) ['id'] );
			$objWorkflow->unlock ();
			
			// save comment if esists
			if ($form_state->getValue ( 'comment' )) {
				
				$comment = $form_state->getValue ( 'comment' );
				
				if ($comment != '') {
					
					$comment = base64_encode ( $comment );
					
					\Drupal::database ()->insert ( 'rwPubgoldWorkflowComments' )->fields ( [ 
						'wfc_created_by_uid' => $session->get ( 'user' ) ['id'],
						'wfc_comment' => $comment,
						'wfc_wfid' => $objWorkflow->getElement ( 'id' ) 
					] )->execute ();
				}
			}
			
			$form_state->setRedirect ( 'publisso_gold.dashboard' );
		}
		elseif ($step == 1 && $form_state->get ( 'action' ) == 'accept') {
			
			$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
			$objWorkflow->setState ( 'accepted' );
			$objWorkflow->setElement ( 'editor_last_edited', $session->get ( 'user' ) ['id'] );
			$objWorkflow->unlock ();
			
			// save comment if esists
			if ($form_state->getValue ( 'comment' )) {
				
				$comment = $form_state->getValue ( 'comment' );
				
				if ($comment != '') {
					
					$comment = base64_encode ( $comment );
					
					\Drupal::database ()->insert ( 'rwPubgoldWorkflowComments' )->fields ( [
						'wfc_created_by_uid' => $session->get ( 'user' ) ['id'],
						'wfc_comment' => $comment,
						'wfc_wfid' => $objWorkflow->getElement ( 'id' )
					] )->execute ();
				}
			}
			
			$form_state->setRedirect ( 'publisso_gold.dashboard' );
		}
		elseif ($step == 1 && $form_state->get ( 'action' ) == 'assign_editor') {
			
			$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
			$objWorkflow->setState ( 'assigned to eic', [$session->get ( 'user' ) ['id']] );
			$objWorkflow->setElement ( 'editor_last_edited', $session->get ( 'user' ) ['id'] );
			//$objWorkflow->unlock ();
			
			// save comment if esists
			if ($form_state->getValue ( 'comment' )) {
				
				$comment = $form_state->getValue ( 'comment' );
				
				if ($comment != '') {
					
					$comment = base64_encode ( $comment );
					
					\Drupal::database ()->insert ( 'rwPubgoldWorkflowComments' )->fields ( [
						'wfc_created_by_uid' => $session->get ( 'user' ) ['id'],
						'wfc_comment' => $comment,
						'wfc_wfid' => $objWorkflow->getElement ( 'id' )
					] )->execute ();
				}
			}
			
			//$form_state->setRedirect ( 'publisso_gold.dashboard' );
		}
		elseif ($step == 1 && $form_state->get ( 'action' ) == 'assign_eic') {
			
			$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
			$objWorkflow->setElement ('state',  'submission finished' );
			$objWorkflow->setDataElement ('state',  'submission finished' );
			$objWorkflow->setElement ( 'editor_last_edited', $session->get ( 'user' ) ['id'] );
			//$objWorkflow->unlock ();
			
			// save comment if esists
			if ($form_state->getValue ( 'comment' )) {
				
				$comment = $form_state->getValue ( 'comment' );
				
				if ($comment != '') {
					
					$comment = base64_encode ( $comment );
					
					\Drupal::database ()->insert ( 'rwPubgoldWorkflowComments' )->fields ( [
						'wfc_created_by_uid' => $session->get ( 'user' ) ['id'],
						'wfc_comment' => $comment,
						'wfc_wfid' => $objWorkflow->getElement ( 'id' )
					] )->execute ();
				}
			}
			
			//$form_state->setRedirect ( 'publisso_gold.dashboard' );
		}
		elseif ($step == 1 && $form_state->get ( 'action' ) == 'reject') {
			$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
			$objWorkflow->setState ( 'rejected' );
			$objWorkflow->unlock();
			$form_state->setRedirect ( 'publisso_gold.dashboard' );
		}
		elseif($step == 1 && $form_state->get ( 'action' ) == 'to_editor'){
			$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
			$objWorkflow->setElement('state', 'assigned to editor');
			$objWorkflow->setElement('assigned_to', 'u:'.implode(',u:', array_filter(explode(',', $objWorkflow->getElement('assigned_to_editor')))));
			$objWorkflow->unlock();
			$form_state->setRedirect ( 'publisso_gold.dashboard' );
		}
		elseif ($step == 1 && $form_state->get ( 'action' ) == 'revision') {
			
			$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
			$objWorkflow->setState ( 'in revision' );
			$objWorkflow->unlock ();
			
			$wf_id = $form_state->get ( 'wf_id' );
			$workflow = getDashboard ( \Drupal::database (), $wf_id );
			
			$comment = $form_state->getValue ( 'comment' );
			
			if (! empty ( $comment )) {
				
				$comment = base64_encode ( $comment );
				
				\Drupal::database ()->insert ( 'rwPubgoldWorkflowCommentsForAuthors' )->fields ( [ 
					'cfa_for_uid' => $objWorkflow->getElement ( 'created_by_uid' ),
					'cfa_comment' => $comment,
					'cfa_wfid' => $objWorkflow->getElement ( 'id' ),
					'cfa_created_by_uid' => $session->get ( 'user' ) ['id'] 
				] )->execute ();
			}
			
			$form_state->setRedirect ( 'publisso_gold.dashboard' );
		}
		return $form;
	}
        
        /**
         * @param $tmpl
         * @param $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl, $vars) {
		$tmpl = ( string ) $tmpl;
		
		$keys = array_keys ( $vars );
		$vars = array_values ( $vars );
		
		// set Site-Vars
		$tmpl = str_replace ( $keys, $vars, $tmpl );
		
		// remove unused vars
		return preg_replace ( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
	}
}
