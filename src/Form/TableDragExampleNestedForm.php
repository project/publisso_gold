<?php
        
        
        namespace Drupal\publisso_gold\Form;


        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Controller\Publisso;
        use Symfony\Component\DependencyInjection\ContainerInterface;

        /**
         * Class TableDragExampleNestedForm
         * @package Drupal\publisso_gold\Form
         */
        class TableDragExampleNestedForm extends FormBase {
                
                protected $render;
                
                public function _construct(){
                        $this->render = \Drupal::service('renderer');
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId() {
                        return 'tabledrag_example_parent_form';
                }
        
                /**
                 * Build the parent-child example form.
                 *
                 * Tabledrag will take care of updating the parent_id relationship on each
                 * row of our table when we drag items around, but we need to build out the
                 * initial tree structure ourselves. This means ordering our items such
                 * that children items come directly after their parent items, and all items
                 * are sorted by weight relative to their siblings.
                 *
                 * @param array $form
                 *   Render array representing from.
                 * @param \Drupal\Core\Form\FormStateInterface $form_state
                 *   Current form state.
                 *
                 * @return array
                 *   The render array defining the elements of the form.
                 */
                public function buildForm(array $form, FormStateInterface $form_state) {
                        
                        $render = \Drupal::service('renderer');
                        
                        $form['table-row'] = [
                                '#type' => 'table',
                                '#header' => [
                                        $this
                                                ->t('Type'),
                                        $this
                                                ->t('Description'),
                                        $this
                                                ->t('Weight'),
                                        $this
                                                ->t('Parent'),
                                        ''
                                ],
                                '#empty' => $this
                                        ->t('Sorry, There are no items!'),
                                // TableDrag: Each array value is a list of callback arguments for
                                // drupal_add_tabledrag(). The #id of the table is automatically
                                // prepended; if there is none, an HTML ID is auto-generated.
                                '#tabledrag' => [
                                        [
                                                'action' => 'match',
                                                'relationship' => 'parent',
                                                'group' => 'row-pid',
                                                'source' => 'row-id',
                                                'hidden' => TRUE,
                                                /* hides the WEIGHT & PARENT tree columns below */
                                                'limit' => FALSE,
                                        ],
                                        [
                                                'action' => 'order',
                                                'relationship' => 'sibling',
                                                'group' => 'row-weight',
                                        ],
                                ],
                                '#prefix' => '<div id="div-table">',
                                '#suffix' => '</div>'
                        ];
                
                        // Build the table rows and columns.
                        //
                        // The first nested level in the render array forms the table row, on which
                        // you likely want to set #attributes and #weight.
                        // Each child element on the second level represents a table column cell in
                        // the respective table row, which are render elements on their own. For
                        // single output elements, use the table cell itself for the render element.
                        // If a cell should contain multiple elements, simply use nested sub-keys to
                        // build the render element structure for the renderer service as you would
                        // everywhere else.
                        
                        if(!$form_state->has('results')) {
        
                                $results = [];
                                
                                $form_state->set('lastID', 0);
                                $form_state->set('results', $results);
                        }
                        #Publisso::log(print_r($form_state->get('results'), 1));
                        
                        //calculate depth
                        $results = $form_state->get('results');
                        foreach ($results as $id => $row) {
                                $results[$id]->depth = $this->getItemDepth($row, 0, $form_state->get('results'));
                        }
                        $form_state->set('results', $results);
                        
                       
                        foreach ($form_state->get('results') as $row) {
                        
                                // TableDrag: Mark the table row as draggable.
                                $form['table-row'][$row->id]['#attributes']['class'][] = 'draggable';
                        
                                // Indent item on load.
                                if (isset($row->depth) && $row->depth > 0) {
                                        $indentation = [
                                                '#theme' => 'indentation',
                                                '#size' => $row->depth,
                                        ];
                                }
                        
                                // Some table columns containing raw markup.
                                
                                $form['table-row'][$row->id]['name'] = [
                                        '#markup' => 'Item #'.$row->id.' ('.ucfirst($row->type).')',
                                        '#prefix' => !empty($indentation) ? $render
                                                ->render($indentation) : '',
                                ];
                                
                                if($row->type == 'day'){
                                        $form['table-row'][$row->id]['object'] = [
                                                '#type' => 'date',
                                                '#required' => true,
                                                '#title' => 'Day:',
                                        ];
                                }
                                elseif($row->type == 'session'){
                                        $form['table-row'][$row->id]['object'] = [
                                                '#type' => 'container',
                                                'content' => [
                                                        'title' => [
                                                                '#type' => 'textfield',
                                                                '#required' => true,
                                                                '#title' => 'Title:'
                                                        ],
                                                        'time_start' => [
                                                                '#type' => 'textfield',
                                                                '#title' => 'From:',
                                                                '#placeholder' => 'HH:MM',
                                                                '#size' => 6
                                                        ],
                                                        'time_end' => [
                                                                '#type' => 'textfield',
                                                                '#title' => 'To:',
                                                                '#placeholder' => 'HH:MM',
                                                                '#size' => 6
                                                        ]
                                                ],
                                                '#prefix' => '<div class="form-wrapper-childs-left">',
                                                '#suffix' => '</div>'
                                        ];
                                }
                                /*
                                $form['table-row'][$row->id]['description'] = [
                                        '#type' => 'textfield',
                                        '#required' => TRUE,
                                        '#default_value' => $row->description,
                                ];
                                */
                                // This is hidden from #tabledrag array (above).
                                // TableDrag: Weight column element.
                                $form['table-row'][$row->id]['weight'] = [
                                        '#type' => 'weight',
                                        '#title' => $this
                                                ->t('Weight for ID @id', [
                                                        '@id' => $row->id,
                                                ]),
                                        '#title_display' => 'invisible',
                                        '#default_value' => $row->weight,
                                        // Classify the weight element for #tabledrag.
                                        '#attributes' => [
                                                'class' => [
                                                        'row-weight',
                                                ],
                                        ],
                                ];
                                $form['table-row'][$row->id]['parent']['id'] = [
                                        '#parents' => [
                                                'table-row',
                                                $row->id,
                                                'id',
                                        ],
                                        '#type' => 'hidden',
                                        '#value' => $row->id,
                                        '#attributes' => [
                                                'class' => [
                                                        'row-id',
                                                ],
                                        ],
                                ];
        
                                $form['table-row'][$row->id]['parent']['type'] = [
                                        '#parents' => [
                                                'table-row',
                                                $row->id,
                                                'type',
                                        ],
                                        '#type' => 'hidden',
                                        '#value' => $row->type,
                                        '#attributes' => [
                                                'class' => [
                                                        'row-id',
                                                ],
                                        ],
                                ];
                                
                                $form['table-row'][$row->id]['parent']['pid'] = [
                                        '#parents' => [
                                                'table-row',
                                                $row->id,
                                                'pid',
                                        ],
                                        '#type' => 'number',
                                        '#size' => 3,
                                        '#min' => 0,
                                        '#title' => $this
                                                ->t('Parent ID'),
                                        '#title_display' => 'invisible',
                                        '#default_value' => $row->pid,
                                        '#attributes' => [
                                                'class' => [
                                                        'row-pid',
                                                ],
                                        ],
                                ];
        
                                $form['table-row'][$row->id]['delete'.$row->id] = [
                                        '#type' => 'submit',
                                        '#value' => 'Delete Item #'.$row->id,
                                        '#button_type' => 'danger',
                                        '#attributes' => [
                                        ],
                                        '#limit_validation_errors' => [],
                                        '#submit' => ['::removeItem'],
                                        '#ajax' => [
                                                'wrapper' => 'div-table',
                                                'callback' => '::removeItemCallback'
                                        ],
                                        '#id' => 'delete|'.$row->id
                                ];
                        }
                        $form['actions'] = [
                                '#type' => 'actions',
                        ];
                        $form['actions']['submit'] = [
                                '#type' => 'submit',
                                '#value' => $this
                                        ->t('Save All Changes'),
                        ];
                        
                        $form['actions']['addDay'] = [
                                '#type' => 'submit',
                                '#value' => 'Add day',
                                '#submit' => ['::addDay'],
                                '#limit_validation_errors' => [],
                                '#ajax' => [
                                        'wrapper' => 'div-table',
                                        'callback' => '::addDayCallback'
                                ]
                        ];
        
                        $form['actions']['addSession'] = [
                                '#type' => 'submit',
                                '#value' => 'Add session',
                                '#submit' => ['::addSession'],
                                '#limit_validation_errors' => [],
                                '#ajax' => [
                                        'wrapper' => 'div-table',
                                        'callback' => '::addSessionCallback'
                                ]
                        ];
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function removeItem(array &$form, FormStateInterface $form_state){
                        $id = explode('|', $form_state->getTriggeringElement()['#id'])[1];
                        
                        $results = $form_state->get('results');
                        Publisso::log(print_r($results, 1));
                        Publisso::log('Remove '.$id);
                        foreach($results as $_id => $_row){
                                if($_row->pid == $id) $results[$_id]->pid = null;
                        }
                        
                        unset($results[$id]);
                        Publisso::log(print_r($results, 1));
                        $form_state->set('results', $results);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function removeItemCallback(array &$form, FormStateInterface $form_state){
                        return $form['table-row'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addSession(array &$form, FormStateInterface $form_state){
                        
                        $results = $this->setData($form_state);;
                        $id = $form_state->get('lastID') + 1;
                        $results[$id] = (object)[
                                'id'         => $id,
                                'weight'     => 1,
                                'type'       => 'session',
                                'pid'        => null
                        ];
                
                        $form_state->set('results', $results);
                        $form_state->set('lastID', $id);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                private function setData(FormStateInterface $form_state){

                        $results = [];
                        foreach ($form_state->getUserInput()['table-row'] as $id => $row){
                                $results[$row['id']] = (object)[
                                        'id' => $row['id'],
                                        'weight' => $row['weight'],
                                        'type' => $row['type'],
                                        'pid' => $row['pid']
                                ];
                        }
                        return $results;
                }
        
                /**
                 * @param $row
                 * @param $depth
                 * @param $results
                 * @return mixed
                 */
                private function getItemDepth($row, $depth, $results){
                        if(!$row->pid) return $depth;
                        else return $this->getItemDepth($results[$row->pid], $depth + 1, $results);
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addSessionCallback(array &$form, FormStateInterface $form_state){
                        return $form['table-row'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addDay(array &$form, FormStateInterface $form_state){
                        
                        $results = $this->setData($form_state);;
                        $id = $form_state->get('lastID') + 1;
                        $results[$id] = (object)[
                                'id'         => $id,
                                'weight'     => 1,
                                'type'       => 'day',
                                'pid'        => null
                        ];
                        
                        $form_state->set('results', $results);
                        $form_state->set('lastID', $id);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addDayCallback(array &$form, FormStateInterface $form_state){
                        return $form['table-row'];
                }
                
                /**
                 * Form submission handler for the 'Return to' action.
                 *
                 * @param array $form
                 *   An associative array containing the structure of the form.
                 * @param \Drupal\Core\Form\FormStateInterface $form_state
                 *   The current state of the form.
                 */
                public function cancel(array &$form, FormStateInterface $form_state) {
                        $form_state
                                ->setRedirect('tabledrag_example.description');
                }
        
                /**
                 * Submit handler for the form.
                 *
                 * Updates the 'weight' column for each element in our table, taking into
                 * account that item's new order after the drag and drop actions have been
                 * performed.
                 *
                 * @param array $form
                 *   An associative array containing the structure of the form.
                 * @param \Drupal\Core\Form\FormStateInterface $form_state
                 *   The current state of the form.
                 */
                public function submitForm(array &$form, FormStateInterface $form_state) {
                
                        echo '<pre>';
                        print_r($form_state->getValue('table-row'));
                        echo '</pre>';
                        exit;
                }
        
                /**
                 * Retrieves the tree structure from db and sorts by parent/child/weight.
                 *
                 * The sorting should result in children items immediately following their
                 * parent items, with items at the same level of the hierarchy sorted by
                 * weight.
                 *
                 * The approach used here may be considered too database-intensive.
                 * Optimization of the approach is left as an exercise for the reader. :)
                 *
                 * @return array
                 *   An associative array storing our ordered tree structure.
                 */
                public function getData() {
                
                        // Get all 'root node' items (items with no parents), sorted by weight.
                        $root_items = $this->database
                                ->select('tabledrag_example', 't')
                                ->fields('t')
                                ->condition('pid', '0', '=')
                                ->condition('id', 11, '<')
                                ->orderBy('weight')
                                ->execute()
                                ->fetchAll();
                
                        // Initialize a variable to store our ordered tree structure.
                        $tree = [];
                
                        // Depth will be incremented in our getTree()
                        // function for the first parent item, so we start it at -1.
                        $depth = -1;
                
                        // Loop through the root item, and add their trees to the array.
                        foreach ($root_items as $root_item) {
                                $this
                                        ->getTree($root_item, $tree, $depth);
                        }
                        return $tree;
                }
        
                /**
                 * Recursively adds $item to $item_tree, ordered by parent/child/weight.
                 *
                 * @param mixed $item
                 *   The item.
                 * @param array $tree
                 *   The item tree.
                 * @param int $depth
                 *   The depth of the item.
                 */
                public function getTree($item, array &$tree = [], &$depth = 0) {
                
                        // Increase our $depth value by one.
                        $depth++;
                
                        // Set the current tree 'depth' for this item, used to calculate
                        // indentation.
                        $item->depth = $depth;
                
                        // Add the item to the tree.
                        $tree[$item->id] = $item;
                
                        // Retrieve each of the children belonging to this nested demo.
                        $children = $this->database
                                ->select('tabledrag_example', 't')
                                ->fields('t')
                                ->condition('pid', $item->id, '=')
                                ->condition('id', 11, '<')
                                ->orderBy('weight')
                                ->execute()
                                ->fetchAll();
                        foreach ($children as $child) {
                        
                                // Make sure this child does not already exist in the tree, to
                                // avoid loops.
                                if (!in_array($child->id, array_keys($tree))) {
                                
                                        // Add this child's tree to the $itemtree array.
                                        $this
                                                ->getTree($child, $tree, $depth);
                                }
                        }
                
                        // Finished processing this tree branch.  Decrease our $depth value by one
                        // to represent moving to the next branch.
                        $depth--;
                }
        
        }
