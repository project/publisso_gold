<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldBookSubmissionAddFiles.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldBookSubmissionAddFiles extends FormBase {
    
        private $modname = 'publisso_gold';
        private $modpath;
    
        /**
        * {@inheritdoc}
        */
        public function getFormId() {
        return 'publisso_goldBookSubmissionAddFiles';
        }
        
        /**
         * {@inheritdoc}
         */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                if(!$form_state->has('wf_id'))
                        $form_state->set('wf_id', $args['wf_id']);
                
                if(!$form_state->get('wf_id')){
                        
                        return[
                                '#markup' => (string)$this->t('The corresponding workflow was not found')
                        ];
                }
                
                $form['description'] = array(
                        '#markup' => (string)$this->t('Here you can upload additional Files to your submission'),
                        '#prefix' => '<h3>',
                        '#suffix' => '</h3>'
                );
    
                $form['#tree'] = true;
                
                $form['files'] = $this->FormElement_WorfklowFiles(new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id')));
                
                $form['files_fieldset'] = [
                        '#type' => 'fieldset',
                        '#title' => (string)$this->t('Upload additional Files'),
                        '#prefix' => '<div id="files-fieldset-wrapper">',
                        '#suffix' => '</div>',
                ];
                
                $numFiles = $form_state->get('numFiles');
                
                if (empty($numFiles)) {
                        $numFiles = $form_state->set('numFiles', 1);
                }
                
                for ($i = 0; $i < $numFiles; $i++) {
                        
                        $form['files_fieldset']['file'][$i] = [
                                
                                '#type' => 'fieldset',
                                'file' => [
                                        '#type' => 'managed_file',
                                        '#title' => (string)$this->t('File').' #'.$i,
                                        '#required' => true,
                                        '#upload_validators' => [
                                                'file_validate_extensions' => array('gif png jpg jpeg doc docx xls xlsx midi mp4 mp3 avi mpg mpeg wav mov mid odt ods odp pdf'),
                                                // Pass the maximum file size in bytes
                                                'file_validate_size' => array(30 * 1024 * 1024),
                                        ]
                                ],
                                'meta' => [
                                        'name' => [
                                                '#type' => 'textfield',
                                                '#title' => (string)$this->t('Filename'),
                                                '#required' => true
                                        ],
                                        'description' => [
                                                '#type' => 'textfield',
                                                '#title' => (string)$this->t('Description')
                                        ]
                                ]
                        ];
                }
                
                $form['files_fieldset']['actions']['add_name'] = [
                        '#type' => 'submit',
                        '#value' => t('Add file'),
                        '#submit' => array('::addOne'),
                        '#prefix' => '<br>',
                        '#ajax' => [
                                'callback' => '::addmoreCallback',
                                'wrapper' => 'files-fieldset-wrapper',
                                'effect' => 'fade',
                                'progress' => [
                                        'type' => 'none'
                                ]
                        ],
                ];
                
                if($numFiles > 1){
                        
                        $form['files_fieldset']['actions']['remove_file'] = [
                                '#type' => 'submit',
                                '#value' => t('Remove last'),
                                '#submit' => array('::removeCallback'),
                                '#prefix' => '&nbsp;&nbsp;&nbsp;',
                                '#ajax' => [
                                        'callback' => '::addmoreCallback',
                                        'wrapper' => 'files-fieldset-wrapper',
                                        'effect' => 'fade',
                                        'progress' => [
                                                'type' => 'none'
                                        ]
                                ],
                                '#limit_validation_errors' => []
                        ];
                }
                
                $form_state->setCached(false);
                
                $form['actions']['submit'] = [
                        '#type' => 'submit',
                        '#value' => $this->t('Submit'),
                        '#prefix' => '<br>'
                ];
            
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addOne(array &$form, FormStateInterface $form_state) {
                
                $numFiles = $form_state->get('numFiles');
                $form_state->set('numFiles', ++$numFiles);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addmoreCallback(array &$form, FormStateInterface $form_state) {
                return $form['files_fieldset'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function removeCallback(array &$form, FormStateInterface $form_state) {
                
                $numFiles = $form_state->get('numFiles');
                if($numFiles > 1) $form_state->set('numFiles', --$numFiles);
                $form_state->setRebuild();
        }
        
        /**
         * {@inheritdoc}
         */
        public function validateForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
    
        /**
         * {@inheritdoc}
         */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                
                $blobIds = [];
                
                foreach($form_state->getValue('files_fieldset')['file'] as $file){
                       
                        if(count($file['file'])){
                                
                                $file_id = $file['file'][0];
                                $file_meta = serialize($file['meta']);
                                $file_db = \Drupal::database()->select('file_managed', 't')->fields('t', [])->condition('fid', $file_id, '=')->execute()->fetchAssoc();
                                $file_type = $file_db['filemime'];
                                $file_path = $file_db['uri'];
                                
                                if(preg_match('/^temporary:\/\/(.+)/', $file_path, $matches)){
                                        $file_content = file_get_contents('/tmp/'.$matches[1]);
                                }
                                
                                if($file_type && $file_content){
                                        
                                        $blob = new \Drupal\publisso_gold\Controller\Blob();
                                        $blob->create($file_content, $file_type, $file_meta);
                                        $blobIds[] = $blob->getId();
                                }
                        }
                }
                
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $wfBlobs = unserialize($workflow->getDataElement('files'));
                if(!is_array($wfBlobs)) $wfBlobs = [];
                $workflow->setDataElement('files', serialize(array_unique(array_merge($wfBlobs, $blobIds))));
                drupal_set_message((string)$this->t('Successfully saved'));
                $workflow->unlock();
                $form_state->setRedirect('publisso_gold.dashboard');
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @return AjaxResponse
         */
        public function delFile(array &$form, FormStateInterface $form_state, $args = []) {
                
                $fid = $form_state->getTriggeringElement()['fid'];
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $response = new AjaxResponse();
                
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                $fids = array_diff($fids, [$fid]);
                $workflow->setDataElement('files', serialize($fids));
                
                //check if file are deleted
                //reload Workflow
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                
                if(array_search($fid, $fids)){
                        
                        $file = new \Drupal\publisso_gold\Controller\Blob($fid);
                        $response->addCommand(new AppendCommand('div#rwBlob-'.$fid, '<div class="rwError">'.((string)$this->t('Error deleting file "'.$file->meta['name'].'"')).'</div>'));
                }
                else{
                        $response->addCommand(new RemoveCommand('div#rwBlob-'.$fid));
                }
                return $response;
        }
        
        /**
         * @param $workflow
         * @return array
         */
        private function FormElement_WorfklowFiles(&$workflow){
        
                $files_tmpl = [];
                $fids = $workflow->getDataElement('files');
                if($fids) $fids = unserialize($fids);
                
                if(is_array($fids)){
                        
                        foreach($fids as $fid){
                                
                                $blob = new \Drupal\publisso_gold\Controller\Blob($fid);
                                
                                $files_tmpl[] = [
                                        '#type' => 'fieldset',
                                        '#id' => 'rwBlob-'.$fid,
                                        'content' => [
                                                '#type' => 'link',
                                                '#title' => $blob->meta['name'],
                                                '#url' => Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()]),
                                                '#suffix' => '<br><i>'.$blob->meta['description'].'</i>'
                                        ],
                                        'preview' => preg_match('/^(image|video|audio)\//', $blob->type, $matches) ? [
                                                '#type' => 'inline_template',
                                                '#template' => (
                                                        $matches[1] == 'image' ? '<br><img height="100" src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'">' : (
                                                                $matches[1] == 'video' ? '<br><video height="100" controls><source src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'" type="'.$blob->type.'"></video>' : (
                                                                        $matches[1] == 'audio' ? '<br><audio controls><source src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'" type="'.$blob->type.'"></audio>' : (
                                                                                ''
                                                                        )
                                                                )
                                                        )
                                                )
                                        ] : '',
                                        'action#'.$fid => [
                                                '#type' => 'button',
                                                '#value' => (string)$this->t('Delete File \''.$blob->meta['name'].'\''),
                                                '#prefix' => '<br><br>',
                                                'fid' => $fid,
                                                '#ajax' => [
                                                        'callback' => '::delFile',
                                                        'wrapper' => 'rwBlob-'.$fid,
                                                        'effect' => 'slide'
                                                ],
                                                'limit_validation_errors' => []
                                        ]
                                ];
                        }
                }
                
                if(count($files_tmpl)){
                        return [
                            '#type' => 'details',
                            '#title' => t('Assigned files'),
                            '#open' => false,
                            'content' => $files_tmpl,
                            '#prefix' => '<br>'
                        ];
                }
                else{
                        return [];
                }
        }
}
