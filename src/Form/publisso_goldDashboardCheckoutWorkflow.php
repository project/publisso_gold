<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldDashboardCheckoutWorkflow.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Mail;

/**
 * Provides a simple example form.
 */
class publisso_goldDashboardCheckoutWorkflow extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $num_corporations;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldDashboardCheckoutWorkflow|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_golddashboardcheckoutworkflow';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $form_state->disableCache();
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        $args = $form_state->getBuildInfo();
      
        $dashboard = $args['args'][0]['data'];
        $medium = $args['args'][0]['medium'];
        
        $suffix = $args['args'][0]['medium'];
        
        if($dashboard && $medium){
            
            $form_state->set('wf_id', $dashboard->wf_id);
            
            $workflow = new \Drupal\publisso_gold\Controller\Workflow($dashboard->wf_id);
            
            if($dashboard->author_uid){ // old way
                $author = new \Drupal\publisso_gold\Controller\User($dashboard->author_uid);
            }
            else{ //new way
                $author = new \Drupal\publisso_gold\Controller\User($workflow->getElement('created_by_uid'));
            }
            
            $author->profile->setElement('salutation', getSalutation($author->profile->getElement('salutation')), false);
            
            $tmpl_vars = array();
            $tmpl = file_get_contents($this->modpath.'/inc/publisso_gold_dashboard_'.$dashboard->type.'.tmpl.inc.php');
            
            
            $tmpl_vars['::profile_salutation::'         ] = $author->profile->getElement('salutation'       );
            $tmpl_vars['::profile_graduation::'         ] = $author->profile->getElement('graduation'       );
            $tmpl_vars['::profile_firstname::'          ] = $author->profile->getElement('firstname'        );
            $tmpl_vars['::profile_lastname::'           ] = $author->profile->getElement('lastname'         );
            $tmpl_vars['::profile_graduation_suffix::'  ] = $author->profile->getElement('graduation_suffix');
            
            foreach($dashboard as $k => $v){
              
              if(!is_array($v) && !is_object($v))
                $tmpl_vars["::wf_$k::"] = $v;
            }
 
            $tmpl_vars['::txt_author::'] = (string)t('Submitted by');
            $tmpl_vars['::txt_status::'] = (string)t('Status');
            $tmpl_vars['::status::'] = (string)$workflow->getDataElement('state');
            
            $form[$workflow->getElement('id')] = array();
            
            $_type = $_subtype = '';
            
            switch($workflow->getDataElement('type')){
                
                case 'bookchapter':
                    $_type = 'book';
                    $_subtype = 'chapter';
					$objMedium = new \Drupal\publisso_gold\Controller\Book($workflow->getDataElement('bk_id'));
                    break;
                
                case 'journalarticle':
                    $_type = 'journal';
                    $_subtype = 'article';
					$objMedium = new \Drupal\publisso_gold\Controller\Journal($workflow->getDataElement('jrn_id'));
                    break;
                
                case 'conferencepaper':
                    $_type = 'conference';
                    $_subtype = 'paper';
					$objMedium = new \Drupal\publisso_gold\Controller\Conference($workflow->getDataElement('cf_id'));
                    break;
            }
            
            $form[$workflow->getElement('id')]['content'] = [
                '#type' => 'details',
                '#title' => (string)t('@ID: @ITEM_TITLE in @MEDIUM_TITLE', [
                                        '@ID'           => $workflow->getElement('id'), 
                                        '@ITEM_TITLE'   => $workflow->getDataElement('title'), 
                                        '@MEDIUM_TITLE' => $medium['title']
                                        ]),
                '#open' => false,
                '#description' => $this->renderVars($tmpl, $tmpl_vars),
				'content' => [
				]
            ];
            
			if(($objMedium->isUserEditor($_SESSION['user']['id']) || 
			    $objMedium->isUserEditorialOffice($_SESSION['user']['id']) || 
				$objMedium->isUserEditorInChief($_SESSION['user']['id']) || 
				preg_match('/^1|2$/', $_SESSION['user']['role_id']))){
				/*
				if($objMedium->getControlElement('allow_invite_users') != 0){
					$form[$workflow->getElement('id')]['content']['content']['lnk_invite_users'] = [
						'#type' => 'markup',
						'#markup' => '<a href="'.(Url::fromRoute('publisso_gold.workflow.invite_user', ['wf_id' => $workflow->getElement('id')])->toString()).'">'.t('Invite Users').'</a>'
					];
				}
				*/
				/*
				if(count($workflow->readReviewers())){
					
					$form[$workflow->getElement('id')]['content']['content']['lnk_unassign_reviewers'] = [
						'#type' => 'markup',
						'#markup' => '<a href="'.(Url::fromRoute('publisso_gold.workflow.unassign_reviewers', ['wf_id' => $workflow->getElement('id')])->toString()).'">'.t('Unassign Reviewers').'</a>',
						'#prefix' => '&nbsp;&nbsp;&nbsp;'
					];
				}
				*/
			}
			
            $form[$workflow->getElement('id')]['content']['checkout'] = [
                '#type' => 'submit',
                '#value' => t('Open'),
                '#prefix' => '<br>',
				'#button_type' => 'success'
            ];
            
            $form[$workflow->getElement('id')]['content']['wf_id'] = [
                '#type' => 'hidden',
                '#value' => $workflow->getElement('id')
            ];
            
            $form[$workflow->getElement('id')]['content']['state'] = [
                '#type' => 'hidden',
                '#value' => $workflow->getDataElement('state')
            ];
            
            
            /*
            //no need to checkout if changes allowed in orig-text and state is "in revision"
            if(array_key_exists('allow_review_orig_change', $medium) && ($medium['allow_review_orig_change'] == 0 && $dashboard->state == 'in revision')){
                
                $form[$dashboard->wf_id]['content'] = [
                    '#type' => 'submit',
                    '#value' => t('review'),
                    '#submit' => ['::beginnReview']
                ];
            }
            */
        }
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function beginnReview(array &$form, FormStateInterface $form_state){
        
        $form_state->setRedirect('publisso_gold.review_item', ['wf_id' => $form_state->getValue('wf_id')]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $wf_id = $_REQUEST['wf_id'];
        $texts = \Drupal::service('publisso_gold.texts');
        
        $workflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
        
        if($workflow->getLock($_SESSION['user']['id'])){
            
            //if in revision, return to dashboard
                if($workflow->getDataElement('state') == 'in review'){
                        
                        //if new Reviewsheet -> call separate Controller
                        switch(substr($workflow->getElement('type'), 0, 1)){

                                case 'b': //for books
                                        $medium = new \Drupal\publisso_gold\Controller\Book($workflow->getDataElement('bk_id'), true);
                                        break;

                                case 'j': //for journals
                                        $medium = new \Drupal\publisso_gold\Controller\Journal($workflow->getDataElement('jrn_id'), true);
                                        break;

                                case 'c': //for conferences
                                        $medium = new \Drupal\publisso_gold\Controller\Conference($workflow->getDataElement('cf_id'), true);
                                        break;
                        }
                        
                        $rp = new \Drupal\publisso_gold\Controller\Reviewsheet($medium->getElement('rpid'));
                        
                        if(!empty($rp->getElement('schema'))){
                                $form_state->setRedirect('publisso_gold.review_item', ['wf_id' => $wf_id]);
                        }
                        elseif(!empty($rp->getElement('sheet'))){
                                $form_state->setRedirect('publisso_gold.review_item', ['wf_id' => $wf_id]);
                        }
                        else{
                               drupal_set_message((string)t($texts->get('workflow.item.review.no_method'))); 
                        }
                }
                else{
                        $form_state->setRedirect('publisso_gold.workflow_item', ['wf_id' => $wf_id]);
                }
        }
        else{
            drupal_set_message(t('This item is currently locked! ('.$workflow->getElement('id').')'), 'error');
        }
        
        return $form;
    }
        
        /**
         * @param $tmpl
         * @param $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl, $vars){
        
        $tmpl = (string)$tmpl;
        
        $keys = array_keys($vars);
        $vars = array_values($vars);
        
        //echo '<pre>'.print_r($vars, 1).'</pre>'; phpinfo(); exit();
        
        //set Site-Vars
        $tmpl = str_replace($keys, $vars, $tmpl);
        
        //remove unused vars
        return preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $tmpl);
     }
}
