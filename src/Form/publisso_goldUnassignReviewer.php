<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldUnassignReviewer.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;

/**
 * Provides a simple example form.
 */
class publisso_goldUnassignReviewer extends FormBase {

	private $current_user = NULL;
	private $modname = 'publisso_gold';
	private $modpath;

	/**
	* {@inheritdoc}
	*/
	public function getFormId() {
		return 'publisso_goldunassignreviewer';
	}

	/**
	* {@inheritdoc}
	*/
	public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                $session = \Drupal::service('session');
		//echo '<pre>Hallo</pre>'; exit();
		$this->modpath = drupal_get_path('module', $this->modname);
		require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
		
		$this->current_user = new \Drupal\publisso_gold\Controller\User($session->get('user')['id']);
		$form_state->setCached(false); //don't cache this form
		
		if(!$form_state->get('wf_id') && array_key_exists('wf_id', $args))
			$form_state->set('wf_id', $args['wf_id']);
		
		if($form_state->has('wf_id'))
			$workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
		
		$form['title'] = [
			'#type' => 'markup',
			'#markup' => t('Unassign Reviewer(s)'),
			'#prefix' => '<h1>',
			'#suffix' => '</h1>'
		];
		
		$reviewers = [];
		
		foreach($workflow->readReviewers() as $user){
			$reviewers[$user->getElement('id')] = [
				'graduation' => $user->profile->getElement('graduation') ? $user->profile->getElement('graduation') : '',
				'firstname' => $user->profile->getElement('firstname'),
				'lastname' => $user->profile->getElement('lastname'),
				'graduation_suffix' => $user->profile->getElement('graduation_suffix') ? $user->profile->getElement('graduation_suffix') : '',
			];
		}
		
		$form['reviewers'] = [
			'#type' => 'tableselect',
			'#multiple' => true,
			'#header' => [
				'graduation' => t('Graduaion'),
				'firstname' => t('Firstname'),
				'lastname' => t('Lastname'),
				'graduation_suffix' => t('Graduation-suffix')
			],
			'#options' => $reviewers
		];
		
		$form['submit'] = [
			'#type' => 'submit',
			'#value' => t('Submit'),
			'#button_type' => 'success'
		];
		
		$form['#attached'] = [
			'library' => [
				'publisso_gold/default'
			]
		];
		
		return $form;
	}

	/**
	* {@inheritdoc}
	*/
	public function validateForm(array &$form, FormStateInterface $form_state) {
		
		return $form;
	}

	/**
	* {@inheritdoc}
	*/
	public function submitForm(array &$form, FormStateInterface $form_state) {
		
		$this->current_user = new \Drupal\publisso_gold\Controller\User($_SESSION['user']['id']);
		$workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
		$reviewers = [];
		
		foreach($form_state->getValue('reviewers') as $k => $v){
			if($v) $reviewers[] = $v;
		}
		
		if(count($reviewers))
			$workflow->removeReviewer($reviewers);
		
		//echo '<pre>';
		//print_r($reviewers);
		//print_r($workflow->readReviewers());
		if(!$workflow->getElement('assigned_to_reviewer')){
			//echo '';
			//print_r(array_filter(explode(',', $workflow->getElement('assigned_to_editor'))), 1);
			$workflow->setState('assigned to editor', array_filter(explode(',', $workflow->getElement('assigned_to_editor'))));
		}
		//echo '</pre>'; exit();
		$form_state->setRedirect('publisso_gold.dashboard');
		
		return $form;
	}
}






