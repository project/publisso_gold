<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowSetEic.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Database\Connection;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldWorkflowSetEic extends FormBase {
                
                private $modname = 'publisso_gold';
                private $database;
                private $modpath;
                
                public function __construct ( Connection $database ) {
                        $this->database = $database;
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return publisso_goldWorkflowSetEic|static
                 */
                public static function create (ContainerInterface $container ) {
                        return new static( $container->get( 'database' ) );
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'publisso_goldworkflowseteic';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state ) {
                        
                        $this->modpath = drupal_get_path( 'module', $this->modname );
                        
                        if ( $this->modpath && !$form_state->get( 'modpath' ) ) {
                                $form_state->set( 'modpath', $this->modpath );
                        }
                        
                        if ( !$this->modpath && $form_state->get( 'modpath' ) ) {
                                $this->modpath = $form_state->get( 'modpath' );
                        }
                        
                        $args = $form_state->getBuildInfo();
                        
                        if ( count( $args ) )
                                $wf_id = $args[ 'args' ][ 0 ][ 'wf_id' ];
                        
                        if ( !$wf_id )
                                $wf_id = $form_state->get( 'wf_id' );
                        
                        if ( $wf_id ) {
                                
                                $form_state->set( 'wf_id', $wf_id );
                                
                                $objWorkflow = new \Drupal\publisso_gold\Controller\Workflow( $form_state->get( 'wf_id' ) );
                                
                                switch ( $objWorkflow->getDataElement( 'type' ) ) {
                                        
                                        case 'bookchapter':
                                                $medium = new \Drupal\publisso_gold\Controller\Book( $objWorkflow->getDataElement( 'bk_id' ) );
                                                break;
                                        
                                        case 'journalarticle':
                                                $medium = new \Drupal\publisso_gold\Controller\Book( $objWorkflow->getDataElement( 'jrn_id' ) );
                                                break;
                                        
                                        case 'conferencepaper':
                                                $medium = new \Drupal\publisso_gold\Controller\Book( $objWorkflow->getDataElement( 'cf_id' ) );
                                                break;
                                }
                                
                                $eicList = [];
                                foreach ( $medium->readEditorsInChief() as $eic ) {
                                        if ( !in_array( $eic->getElement( 'id' ), $eicList ) ) {
                                                $eicList[ $eic->getElement( 'id' ) ] = implode( ' ', array_filter( [ $eic->profile->getElement( 'graduation' ), implode( ' ', [ $eic->profile->getElement( 'firstname' ), $eic->profile->getElement( 'lastname' ) ] ), $eic->profile->getElement( 'graduation_suffix' ) ] ) );
                                        }
                                }
                                
                                if ( !count( $eicList ) )
                                        $eicList = getUsersByRole( \Drupal::database(), [ 2, 3, 4, 5, 6, 7 ] );
                                
                                $form = [
                                        'action-list' => [
                                                '#weight'      => 100,
                                                '#type'        => 'fieldset',
                                                '#title'       => t( 'Direct action' ),
                                                '#collapsed'   => true,
                                                '#collapsible' => true,
                                                '#tree'        => false,
                                                'content'      => [
                                                        
                                                        'comment' => [
                                                                '#type'   => 'textarea',
                                                                '#title'  => t( 'Comment' ),
                                                                '#suffix' => '<br>',
                                                        ],
                                                        
                                                        'comment_for_author' => [
                                                                '#type'   => 'textarea',
                                                                '#title'  => t( 'Comment for author' ),
                                                                '#suffix' => '<br>',
                                                        ],
                                                        
                                                        'action' => [
                                                                '#type'     => 'select',
                                                                '#title'    => 'Action',
                                                                '#required' => true,
                                                                '#options'  => [
                                                                        'accept'   => t( 'Accept Submission' ),
                                                                        'reject'   => t( 'Reject Submission' ),
                                                                        'revision' => t( 'To author for revision' ),
                                                                ],
                                                        ],
                                                        
                                                        'set_action' => [
                                                                '#type'                    => 'submit',
                                                                '#value'                   => t( 'Set action & close' ),
                                                                '#button_type'             => 'success',
                                                                '#submit'                  => [
                                                                        '::setAction',
                                                                ],
                                                                '#limit_validation_errors' => [
                                                                        [
                                                                                'action',
                                                                        ],
                                                                ],
                                                        ],
                                                ],
                                        ],
                                        'eic-list'    => [
                                                '#type'   => 'fieldset',
                                                '#title'  => t( 'set Editor-in-Chief' ),
                                                'content' => [
                                                        
                                                        'eic' => [
                                                                '#title'    => t( 'available Users for Editor-in-Chief' ),
                                                                '#type'     => 'select',
                                                                '#options'  => $eicList,
                                                                '#multiple' => true,
                                                                '#required' => true,
                                                        ],
                                                        
                                                        'set_eic' => [
                                                                '#type'                    => 'submit',
                                                                '#value'                   => t( 'Set editor-in-chief & close' ),
                                                                '#button_type'             => 'success',
                                                                '#submit'                  => [
                                                                        '::submitForm',
                                                                ],
                                                                '#limit_validation_errors' => [
                                                                        [
                                                                                'eic',
                                                                        ],
                                                                ],
                                                        ],
                                                ],
                                        ],
                                ];
                        }
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function setAction ( array &$form, FormStateInterface $form_state ) {
                        
                        $session = \Drupal::service( 'session' );
                        $action = $form_state->getValue( 'action' );
                        
                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $form_state->get( 'wf_id' ) );
        
                        $comment_for_author = $form_state->getUserInput()['comment_for_author'];
                        $comment = $form_state->getUserInput()['comment'];
                        
                        if ( !$workflow->getElement( 'id' ) ) {
                                \Drupal::service('messenger')->addError( t( 'Can\'t assign action to a specific workflow. Please contact technical support team.' ));
                                $form_state->setRebuild();
                                return $form;
                        }
                        
                        $creator_uid = $workflow->getElement( 'created_by_uid' );
                        
                        switch ( $action ) {
                                
                                case 'accept':
                                        //$workflow->setElement('assigned_to', 'u:'.implode(',u:', explode(',', $workflow->getElement('assigned_to_eo'))));
                                        //$workflow->setDataElement('state', 'accepted');
                                        $workflow->setState( 'accepted' );
                                        break;
                                
                                case 'reject':
                                        //$workflow->setDataElement('state', 'rejected');
                                        $workflow->setState( 'rejected' );
                                        break;
                                
                                case 'revision':
                                        //$workflow->setElement('assigned_to', 'u:'.$workflow->getElement('created_by_uid'));
                                        $workflow->setElement( 'revision_ordered_by', 'eo' );
                                        //$workflow->setDataElement('state', 'in revision');
                                        $workflow->setState( 'in revision' );
                                        break;
                        }
                        
                        //assign all available EiC
                        
                        switch ( substr( $workflow->getElement( 'type' ), 0, 1 ) ) {
                                
                                case 'b':
                                        $medium = new \Drupal\publisso_gold\Controller\Book( $workflow->getDataElement( 'bk_id' ) );
                                        break;
                                
                                case 'j':
                                        $medium = new \Drupal\publisso_gold\Controller\Journal( $workflow->getDataElement( 'jrn_id' ) );
                                        break;
                                
                                case 'c':
                                        $medium = new \Drupal\publisso_gold\Controller\Conference( $workflow->getDataElement( 'cf_id' ) );
                                        break;
                        }
                        
                        if ( is_object( $medium ) && method_exists( $medium, 'readEditorsInChief' ) ) {
                                
                                $eicList = [];
                                
                                foreach ( $medium->readEditorsInChief() as $user ) {
                                        
                                        array_push( $eicList, $user->getElement( 'id' ) );
                                }
                                
                                $workflow->setElement( 'assigned_to_eic', implode( ',', array_filter( $eicList ) ) );
                        }
                        else {
                                error_log( 'Can\'t assign eic for medium in workflow-item ' . $workflow->getElement( 'id' ) );
                        }
//comments
                        $db_comment = $db_comment_for_author = false;
                        
                        if ( !empty( $comment ) ) {
                                
                                $comment = base64_encode( $comment );
                                $db_comment = \Drupal::database()->insert( 'rwPubgoldWorkflowComments' )
                                                     ->fields( [
                                                                       'wfc_created_by_uid' => $session->get( 'user' )[ 'id' ],
                                                                       'wfc_comment'        => $comment,
                                                                       'wfc_wfid'           => $workflow->getElement( 'id' ),
                                                               ]
                                                     )
                                                     ->execute()
                                ;
                        }
                        
                        if ( !empty( $comment_for_author ) ) {
                                
                                $comment_for_author = base64_encode( $comment_for_author );
                                $db_comment_for_author = \Drupal::database()->insert( 'rwPubgoldWorkflowCommentsForAuthors' )
                                                                ->fields( [
                                                                                  'cfa_created_by_uid' => $session->get( 'user' )[ 'id' ],
                                                                                  'cfa_comment'        => $comment_for_author,
                                                                                  'cfa_wfid'           => $workflow->getElement( 'id' ),
                                                                                  'cfa_for_uid'        => $workflow->getElement( 'created_by_uid' ),
                                                                          ]
                                                                )
                                                                ->execute()
                                ;
                        }
// -- comments --
                        
                        /******************************************
                         * unlock workflow                        *
                         ******************************************/
                        $workflow->setElement( 'locked', null );
                        $workflow->setElement( 'locked_by_uid', null );
                        /******************************************/
                        /******************************************/
                        
                        $form_state->setRedirect( 'publisso_gold.dashboard' );
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $session = \Drupal::service( 'session' );
                        $eic = $form_state->getValue( 'eic' );

//inform users about registration if not done
                        $assigned_users = [];
                        
                        foreach ( $eic as $uid ) {
                                
                                $_user = new \Drupal\publisso_gold\Controller\User( $uid );
                                
                                if ( $_user->getElement( 'reg_notification_sent' ) == 0 ) {
                                        $assigned_users[] = $_user;
                                }
                        }
                        
                        foreach ( $assigned_users as $_ ) {
                                
                                //send Reg-Mail to user if not yet be done
                                if ( $_->getElement( 'reg_notification_sent' ) == 0 ) {
                                        
                                        $port = \Drupal::request()->getPort();
                                        $host = \Drupal::request()->getHttpHost();
                                        $host = "http" . ( $port == 443 ? 's' : '' ) . "://$host";
                                        $link = $host;
                                        $password[ 'clear' ] = genTempPassword();
                                        $password[ 'encrypted' ] = '*' . strtoupper( sha1( sha1( $user[ 'password_clear' ], true ) ) );
                                        
                                        $_->setElement( 'password', $password[ 'encrypted' ] );
                                        
                                        foreach ( [ 'registration_password', 'registration_info' ] as $name ) {
                                                
                                                $vars = [
                                                        '::firstname::'  => $_->profile->getElement( 'firstname' ),
                                                        '::lastname::'   => $_->profile->getElement( 'lastname' ),
                                                        '::link::'       => $link,
                                                        '::email::'      => $_->profile->getElement( 'email' ),
                                                        '::user::'       => $_->getElement( 'user' ),
                                                        '::login_link::' => $link . '/publisso_gold/login',
                                                        '::password::'   => $password[ 'clear' ],
                                                ];
                                                
                                                sendRegistrationInfoMail( $name, $vars );
                                        }
                                        
                                        $_->setElement( 'reg_notification_sent', 1 );
                                }
                                // - Reg-Mail --
                        }
//-- inform users --
                        
                        $workflow = new \Drupal\publisso_gold\Controller\Workflow( $form_state->get( 'wf_id' ) );
                        $workflow->setState( 'assigned to eic', $eic );
                        $workflow->unlock();
                        
                        $form_state->setRedirect( 'publisso_gold.dashboard' );
                        return $form;
                }
        
                /**
                 * @param null $tmpl
                 * @param null $vars
                 * @return string|string[]|null
                 */
                private function renderVars ($tmpl = null, $vars = null ) {
                        
                        if ( $tmpl == null || $vars == null ) {
                                //set Site-Vars
                                $this->tmpl = str_replace( array_keys( $this->tmpl_vars ), array_values( $this->tmpl_vars ), $this->tmpl );
                                
                                //remove unused vars
                                $this->tmpl = preg_replace( '(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl );
                        }
                        else {
                                //set Site-Vars
                                $tmpl = str_replace( array_keys( $vars ), array_values( $vars ), $tmpl );
                                
                                //remove unused vars
                                return preg_replace( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
                        }
                }
        }
