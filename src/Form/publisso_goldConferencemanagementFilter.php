<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldConferencemanagementFilter.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormState;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldConferencemanagementFilter extends FormBase {
    
    const __ACCESS_DENIED__             = 'You can not access this page. You may need to log in first.';
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $user = NULL;
    private $selected_bookid = '';
    private $selected_author = '';
    private $selected_editor = '';
    private $modaccessweights;
    
    public function __construct(Connection $database){
        $this->database = $database;
        $this->user = $this->database->select('rwpgvwUserProfiles', 'u')->fields('u', [])->condition('id', $_SESSION['user']['id'], '=')->execute()->fetchAssoc();
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldConferencemanagementFilter|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'publisso_goldconferencemanagementfilter';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    if(!$this->database)
        $this->database = \Drupal::database();
    
    if(!$this->user)
        $this->user = $this->database->select('rwpgvwUserProfiles', 'u')->fields('u', [])->condition('id', $_SESSION['user']['id'], '=')->execute()->fetchAssoc();
    
    $this->getModuleAccessPermissions();
    
    if(!\Drupal::service('publisso_gold.tools')->userHasAccessRight('conferencemanagement'))
                return \Drupal::service('publisso_gold.tools')->accessDenied();
    
    $this->modpath = drupal_get_path('module', $this->modname);
    require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
    
    $conferences = array();
    
    foreach(getConferences(\Drupal::database()) as $conference){
        
        $conferences[$conference->cf_id] = [
            'cf_title' => $conference->cf_title,
            'cf_location' => $conference->cf_location
        ];
    }
    
    //get template(s)
    $tmpl_head = file_get_contents($this->modpath.'/inc/publisso_gold_conferencemanagement_conferencelist_head.tmpl.inc.php');
    $tmpl_data = file_get_contents($this->modpath.'/inc/publisso_gold_conferencemanagement_conferencelist_data.tmpl.inc.php');
    $tmpl_foot = file_get_contents($this->modpath.'/inc/publisso_gold_conferencemanagement_conferencelist_foot.tmpl.inc.php');
    
    $markup = '';
    $tmpl_vars = array();
    $markup .= $tmpl_head;
    
    foreach($conferences as $cf_id => $_){
        
        $tmpl = $tmpl_data;
        $vars = array();
        
        $vars['::title::'] = $_['cf_title'];
        $vars['::location::'] = $_['cf_location'];
        $vars['::lnk_conference::'] = (string)t('View Conference');
        $vars['::cf_id::'] = $cf_id;
        
        $tmpl = $this->renderVars($tmpl, $vars);
        $markup .= $tmpl;
    }
    
    $markup .= $tmpl_foot;
    
    $tmpl_vars['::txt_hl_title::'] = (string)t('Title');
    $tmpl_vars['::txt_hl_location::'] = (string)t('Location');
    
    $markup = $this->renderVars($markup, $tmpl_vars);

    $form['conferences'] = [
        '#type' => 'markup',
        '#markup' => $markup
    ];
    
    $form['#cache'] = [
        'max-age' => 0
    ];
    
    return $form;
  }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function apply_filter(array &$form, FormStateInterface $form_state){
        
        $form_state->setRebuild();
    }
        
        /**
         * @param $tmpl
         * @param array $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl, $vars = array()){
        
        //set Site-Vars
        $tmpl = str_replace(array_keys($vars), array_values($vars), $tmpl);
        
        //remove unused vars
        $tmpl = preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $tmpl);
        
        return $tmpl;
    }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
        
  }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

    }    
    
    private function getModuleAccessPermissions(){
        
        $result = $this->database->select('rwPubgoldModuleaccess', 'ma')->fields('ma', [])->execute()->fetchAll();
        
        foreach($result as $res){
            $this->modaccessweights[$res->mod_name] = $res->mod_minaccessweight;
        }
    }
        
        /**
         * @param $modname
         * @return bool
         */
        private function userHasAccessRight($modname){
        
        if(!array_key_exists($modname, $this->modaccessweights)){
            return false;
        }
        return $this->modaccessweights[$modname] <= $_SESSION['user']['weight'];
    }
}
