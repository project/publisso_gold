<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\Workflow\WorkflowState_Alternative.
         */
        namespace Drupal\publisso_gold\Form\Workflow;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        
        /**
         * Provides a simple example form.
         */
        class WorkflowState_Alternative extends FormBase {
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId() {
                        return 'workflow.WorkflowState_Alternative';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                        
                        if(array_key_exists('wf_id', $args)){
                                
                                $form_state->set('wf_id', $args['wf_id']);
                                
                                $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($args['wf_id']);
                                $medium = $workflow->getMedium();
                                
                                $actions = [];
                                
                                
                                foreach($workflow->getSchemaItemActions() as $_){
                                        
                                        if(!empty($_['schema_select_action_required_field'])){
                                                
                                                $fields = array_filter(array_map('trim', explode(',', $_['schema_select_action_required_field'])) ?? []);
                                                
                                                foreach($fields as $field){
                                                        
                                                        if(in_array($field, $workflow->getKeys())){
                                                                if(empty($workflow->getElement($field))) continue 2;
                                                        }
                                                }
                                        }
                                        
                                        if(!empty($_['schema_select_action']))
                                                $actions[$_['schema_follower']] = (string)$this->t($_['schema_select_action']);
                                }
                                
                                $form = [
                                        'action-list' => [
                                                '#type' => 'fieldset',
                                                '#title' => t('Direct action'),
                                                '#collapsed' => true,
                                                '#collapsible' => true,
                                                '#suffix' => '<br>',
                                                '#tree' => false,
                                                'content' =>[
                                                        
                                                        'action' => [
                                                                '#type' => 'select',
                                                                '#title' => 'Action',
                                                                '#required' => true,
                                                                '#options' => $actions
                                                        ],
                                                        
                                                        'set_action' => [
                                                                '#type' => 'submit',
                                                                '#value' => t('Set action & close'),
                                                                '#button_type' => 'success'
                                                        ]
                                                ]
                                        ]
                                ];
                        }
                        
                        return $form;
                }
                
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm(array &$form, FormStateInterface $form_state) {
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm(array &$form, FormStateInterface $form_state) {
                        
                        if(null !== ($workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($form_state->get('wf_id')))) {
                                
                                $medium = $workflow->getMedium();
                                
                                //setzen der verfügbaren EiC falls nicht bereits geschehen, da sie für verschiedene Stati gebraucht werden
                                if(!$workflow->getElement('assigned_to_eic')) {
                                        $eic = [];
                                        foreach ( $medium->readEditorsInChief() as $user ) {
                                                $eic[] = $user->getElement( 'id' );
                                        }
        
                                        $workflow->setElement( 'assigned_to_eic', implode( ',', $eic ) );
                                }
                                
                                $newSchemaID = $workflow->getSchemaItem($form_state->getValue('action'))['schema_id'];
                                
                                $workflow->newSchemaState($newSchemaID);
                                
                                if($workflow->getLock())
                                        $workflow->unlock();
                        }
        
                        $addParams = [];
                        if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                                $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                                \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                        }
        
                        $form_state->setRedirect('publisso_gold.dashboard', $addParams, ['fragment' => 'rwDashboardItem-'.$form_state->get('wf_id')]);
                        return $form;
                }
        }
