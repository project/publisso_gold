<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\Workflow\WorkflowState_0006.
         */
        namespace Drupal\publisso_gold\Form\Workflow;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\Workflow\Comment;

        /**
         * Provides a simple example form.
         */
        class WorkflowState_0006 extends FormBase {
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId() {
                        return 'workflow.WorkflowState_0006';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                        
                        if(array_key_exists('wf_id', $args)){

                                $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($args['wf_id']);

                                $form_state->set('wf_id', $args['wf_id']);
                                $form_state->set('new_identifier', $workflow->getSchemaDefaultFollower()['schema_follower']);

                                $medium = $workflow->getMedium();
                                
                                $editorList= [];
                                foreach($medium->readEditors() as $editor){
                                        if(!in_array($editor->getElement('id'), $editorList)){
                                                $editorList[$editor->getElement('id')] = implode(' ', array_filter([$editor->profile->getElement('graduation'), implode(' ', [$editor->profile->getElement('firstname'), $editor->profile->getElement('lastname')]), $editor->profile->getElement('graduation_suffix')]));
                                        }
                                }
        
                                $actionItems = $workflow->getSchemaItemActions(null, true);
                                $options = [];
        
                                $comment_options = [];
        
                                foreach($actionItems as $item){
                                        $options[$item['schema_follower']] = (string)$this->t($item['schema_select_action']);
                                        if($item['schema_comment_required'] == 1) $comment_options[] = $item['schema_follower'];
                                }
        
                                $comment_required = [];
                                $not_comment_required = [];
                                foreach($comment_options as $_){
                
                                        if(count($comment_required)) $comment_required[] = 'or';
                                        $comment_required[] = [':input[name="follower"]' => array('value' => $_)];
                                        $not_comment_required[] = [':input[name="follower"]' => array('!value' => $_)];
                                }
        
                                $form['follower'] = [
                                        '#type' => 'select',
                                        '#title' => (string)$this->t('Select action'),
                                        '#required' => true,
                                        '#options' => $options
                                ];
                                
                                $form['editor-list'] = [
                                        '#type' => 'fieldset',
                                        '#title' => t('set Editor'),
                                        'content' => [
                        
                                                'editor' => [
                                                        '#title' => t('available Users for Editor'),
                                                        '#type' => 'select',
                                                        '#options' => $editorList,
                                                        '#multiple' => false,
                                                        '#states' => [
                                                                'required' => [':input[name="follower"]' => ['value' => 11]]
                                                        ]
                                                ]
                                        ],
                                        '#states' => [
                                                'visible' => [':input[name="follower"]' => ['value' => 11]]
                                        ]
                                ];
                                
                                $form['comment'] = [
                                        '#type' => 'textarea',
                                        '#title' => (string)$this->t('Comment for editorial office'),
                                        '#states' => [
                                                'visible' => [
                                                        [':input[name="follower"]' => ['value' => 21]],
                                                ]
                                        ],
                                        '#placeholder' => (string)$this->t('Please note: your comment may be also visible to others involved in the book publication.')
                                ];
        
                                $form['comment_for_author'] = [
                                        '#type' => 'textarea',
                                        '#title' => (string)$this->t('Comment for author'),
                                        '#states' => [
                                                'visible' => [
                                                        [':input[name="follower"]' => ['value' => 8]],
                                                ],
                                                'required' => [
                                                        ':input[name="follower"]' => ['value' => 8]
                                                ]
                                        ],
                                        '#placeholder' => (string)$this->t('Please note: your comment may be also visible to others involved in the book publication.')
                                ];
                        }
                        
                        $form['actions'] = ['#type' => 'actions', '#suffix' => '<br>'];
                        
                        $form['actions']['submit'] = [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Submit'),
                                '#button_type' => 'success',
                                '#attributes' => [
                                        'class' => ['btn-success']
                                ]
                        ];
                        
                        return $form;
                }
                
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm(array &$form, FormStateInterface $form_state) {
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm(array &$form, FormStateInterface $form_state) {
                        
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        
                        if($workflow->getLock()){
                                
                                $comment = null;
                                $comment_for_author = null;
                                
                                switch($form_state->getValue('follower')){
                                        
                                        case 8: //revision
                                                $comment = $form_state->getValue('comment_for_author');
                                                break;
                                                
                                        case 11: //assign editor
                                                $editor = $form_state->getValue('editor');
                                                $workflow->setElement('assigned_to_editor', $editor);
                                                $workflow->setElement('assigned_to', "u:$editor");
                                                break;
                                                
                                        case 21: //accept
                                                $comment = $form_state->getValue('comment');
                                                break;
                                }
        
                                if ( !empty( $comment ) ) {
                                        Comment::new( $form_state->get( 'wf_id' ), $workflow->getElement( 'schema_identifier' ), $comment, null, null, $workflow->getSchemaCurrentRole() );
                                }
        
                                if ( !empty( $comment_for_author ) ) {
                                        Comment::new( $form_state->get( 'wf_id' ), $workflow->getElement( 'schema_identifier' ), $comment, null, null, $workflow->getSchemaCurrentRole(), 'submitting_author' );
                                }
                                
                                $workflow->setElement( 'editor_last_edited', Publisso::currentUser()->getId() );
                                $workflow->newSchemaState( $form_state->getValue( 'follower' ) );
                                $workflow->unlock();
                                $addParams = [];
                                if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                                        $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                                        \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                                }
        
                                $form_state->setRedirect('publisso_gold.dashboard', $addParams, ['fragment' => 'rwDashboardItem-'.$form_state->get('wf_id')]);
                                return $form;
                        }
                        else{
                                \Drupal::messenger()->addError((string)$this->t('Cant get write-lock for this item. Please contact support!'));
                                return $form;
                        }
                        /*
                        if(null !== ($workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($form_state->get('wf_id')))) {
                                
                                if ( $workflow->getLock() ) {
                                        
                                        $workflow->setElement('assigned_to_editor', $form_state->getValue('editor'));
                                        $workflow->setElement('assigned_to', 'u:'.$form_state->getValue('editor'));
                                        $workflow->newSchemaState($form_state->get('new_identifier'));
                                        
                                        $workflow->unlock();
                                        $form_state->setRedirect('publisso_gold.dashboard');
                                }
                        }
        
                        $addParams = [];
                        if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                                $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                                \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                        }
        
                        $form_state->setRedirect('publisso_gold.dashboard', $addParams, ['fragment' => 'rwDashboardItem-'.$form_state->get('wf_id')]);
                        return $form;
                        */
                }
        }
