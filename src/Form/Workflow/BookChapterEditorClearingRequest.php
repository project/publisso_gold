<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowAuthorActionAfterAuthorClearingRequest.
         */
        
        namespace Drupal\publisso_gold\Form\Workflow;
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Database\Connection;
        use Drupal\publisso_gold\Controller\Workflow\Comment;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Url;
        
        /**
         * Provides a simple example form.
         */
        class BookChapterEditorClearingRequest extends FormBase {
                
                private $modname = 'publisso_gold';
                private $database;
                private $modpath;
                private $num_corporations;
                
                public function __construct ( Connection $database ) {
                        $this->database = $database;
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return BookChapterEditorClearingRequest|static
                 */
                public static function create (ContainerInterface $container ) {
                        return new static( $container->get( 'database' ) );
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'BookChapterEditorClearingRequest';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state, $args = [] ) {
                        
                        if ( !$form_state->get( 'wf_id' ) )
                                $form_state->set( 'wf_id', $args['wf_id'] );
                        
                        $wf_id = $form_state->get('wf_id');
                        
                        if ( $wf_id ) {
                                
                                $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($wf_id);
                                
                                if ( $workflow->getDataElement( 'cp_id' ) ) {
                                        
                                        $url = Url::fromRoute( 'publisso_gold.export.chapter', [ 'cp_id' => $workflow->getDataElement( 'cp_id' ) ] );
                                        
                                        $form[ 'pdf' ] = [
                                                '#type'       => 'link',
                                                '#title'      => (string)$this->t( 'Download as PDF' ),
                                                '#url'        => $url,
                                                '#prefix'     => '<span class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;',
                                                '#suffix'     => '<br><br>',
                                                '#attributes' => [
                                                        'class' => [
                                                                'download-pdf',
                                                        ],
                                                ],
                                        ];
                                }
                                
                                $actions = $workflow->getSchemaItemActions(null, true);
                                $options = [];
                                $comment_options = [];
                                $schema_default = null;
                                
                                foreach($actions as $item){
                                        $options[$item['schema_follower']] = (string)$this->t($item['schema_select_action']);
                                        if($item['schema_comment_required'] == 1) $comment_options[] = $item['schema_follower'];
                                        if($item['schema_default_action']) $schema_default = $item['schema_follower'];
                                }
                                
                                $form[ 'author_action' ] = [
                                        '#title'    => (string)t( 'select action' ),
                                        '#type'     => 'select',
                                        '#options'  => $options,
                                        '#required' => true,
                                ];
                                
                                $form[ 'comment' ] = [
                                        '#title'    => (string)t( 'Comment for editorial office' ),
                                        '#type'     => 'textarea',
                                        '#states'   => [
                                                'required' => [':input[name="author_action"]' => ['value' => 25]]
                                        ]
                                ];
                                
                                $previewUrl = Url::fromRoute( 'publisso_gold.workflow.preview', [ 'wf_id' => $form_state->get( 'wf_id' ) ] );
        
                                if($workflow->getDataElement('cp_id')) $previewUrl = Url::fromRoute( 'publisso_gold.book.chapter', [ 'bk_id' => $workflow->getDataElement('bk_id'), 'cp_id' => $workflow->getDataElement('cp_id') ] );
        
                                $form[ 'preview' ] = [
                                        '#type'       => 'link',
                                        '#url'        => $previewUrl,
                                        '#title'      => (string)t( 'Preview' ),
                                        '#attributes' => [
                                                'class'  => [
                                                        'btn', 'btn-warning',
                                                ],
                                                'target' => [
                                                        '_blank',
                                                ],
                                        ],
                                        '#suffix'     => '&nbsp;&nbsp;&nbsp;',
                                ];
        
                                $form[ 'submit' ] = [
                                        '#type'  => 'submit',
                                        '#value' => (string)t( 'Submit' ),
                                        '#suffix' => '<br><br>'
                                ];
                        }
                        
                        $form[ '#cache' ] = [
                                'max-age' => 0,
                        ];
                        
                        return $form;
                }
                
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function back (array $form, FormStateInterface $form_state ) {
                        
                        $step = $form_state->get( 'step' );
                        
                        $form_state->set( 'action', null );
                        $form_state->set( 'step', --$step );
                        $form_state->setRebuild();
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $session = \Drupal::service( 'session' );
                        $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($form_state->get('wf_id'));
                        
                        //save comment
                        $comment = $form_state->getValue( 'comment' );
                        
                        if($comment)
                                $comment = Comment::new($form_state->get('wf_id'), $workflow->getElement('schema_identifier'), $comment, \Drupal::service('session')->get('user')['id'], null, $workflow->getSchemaCurrentRole(), 'editorial_office');
                        
                        if($form_state->hasValue('accept_policy')){
                                $workflow->setDataElement('accept_policy', 1);
                        }
                        
                        $workflow->newSchemaState($form_state->getValue('author_action'));
                        $workflow->unlock();
        
                        $addParams = [];
                        if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                                $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                                \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                        }
        
                        $form_state->setRedirect('publisso_gold.dashboard', $addParams, ['fragment' => 'rwDashboardItem-'.$form_state->get('wf_id')]);
                        
                        return $form;
                }
        
                /**
                 * @param $tmpl
                 * @param $vars
                 * @return string|string[]|null
                 */
                private function renderVars ($tmpl, $vars ) {
                        
                        $tmpl = (string)$tmpl;
                        
                        $keys = array_keys( $vars );
                        $vars = array_values( $vars );
                        
                        //set Site-Vars
                        $tmpl = str_replace( $keys, $vars, $tmpl );
                        
                        //remove unused vars
                        return preg_replace( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
                }
        }
