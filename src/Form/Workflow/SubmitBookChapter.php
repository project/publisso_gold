<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\Workflow\SubmitBookChapter.
 */
namespace Drupal\publisso_gold\Form\Workflow;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\BaseCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\publisso_gold\Classes\Ajax\OpenUrlCommand;
use Drupal\publisso_gold\Controller\BookchapterTemplate;
use Drupal\publisso_gold\Controller\Manager\BookManager;
use Drupal\publisso_gold\Controller\Manager\ChapterManager;
use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
use Drupal\publisso_gold\Controller\Publisso;
use Drupal\publisso_gold\Controller\User;
use \Drupal\publisso_gold\Controller\Workflow\WorkflowInfoMail;
use \Drupal\publisso_gold\Controller\Workflow\ChapterWorkflow;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail;
use Drupal\Core\Url;
use Drupal\file\Element;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides chapter create form.
 */
class SubmitBookChapter extends FormBase {
        
        private $database;
	private $formFields;
    
        /**
         * {@inheritdoc}
         */
        public function getFormId() {
                return 'wf_submitbookchapter';
        }
  
        /**
         * {@inheritdoc}
         */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                /**
                 * link some libraries
                 */
                $this->formFields = \Drupal::service('publisso_gold.form.fields');
        
                /**
                 * Der initialisierte Workflow sollte in den Args übergeben sein - wenn nicht, dann wird eine Fehlermeldung ausgegeben.
                 * Das Workflow-Item wird im $form_state gespeichert. Wenn es dort gespeichert ist, dann werden die Args nicht mehr geprüft.
                 * In diesem Zuge wird auch das Elternmedium (Buch) gesetzt.
                 */
                
                if(array_key_exists('wf_id', $args) && !$form_state->has('wf_id')){
                        $form_state->set('wf_id', $args['wf_id']);
                }
                
                if(!$form_state->has('workflow')){
                        if(array_key_exists('wf_id', $args)){
                                $form_state->set('workflow', new ChapterWorkflow($args['wf_id']));
                                $form_state->set('book', $form_state->get('workflow')->getMedium());
                                $form_state->set('editorial', $form_state->get('workflow')->getDataElement('editorial'));
                        }
                }
        
                if(!$form_state->has('workflow')){
                        \Drupal::service('messenger')->addError((string)$this->t('Assigned workflow-item not given!'));
                        return $form;
                }
                
                $book = new \Drupal\publisso_gold\Controller\Medium\Book($form_state->get('workflow')->getDataElement('bk_id'));
                $workflow = $form_state->get('workflow');

                /**
                 * is this workflow editorial, then remove checkbox "authors contract"
                 * and make sure, a corresponding author is set. this author will be
                 * set as creator of this workflow
                 **/
                if(!$form_state->has('editorial')){
                        
                        if(($form_state->get('book')->isUserEditorialOffice(\Drupal::service('session')->get('user')['id'])) || \Drupal::service('session')->get('user')['weight'] >= 70)
                                $form_state->set('editorial', !!\Drupal::request()->get('editorial'));
                        else
                                $form_state->set('editorial', false);
        
                        $workflow->setDataElement('editorial', $form_state->get('editorial'));
                }
                else{
                        /**
                         * wenn ein redaktioneller Workflow weitergeführt wird, dann nochmals prüfen, ob der aktuelle Benutzer die Berechtigung dazu hat
                         */
                        if($workflow->getDataElement('editorial')) {
                                if ( !( $form_state->get( 'book' )->isUserEditorialOffice( \Drupal::service( 'session' )->get( 'user' )[ 'id' ] ) ) || \Drupal::service( 'session' )->get( 'user' )[ 'weight' ] >= 70 ) {
                                        \Drupal::service( 'messenger' )->addError( 'You dont have permissions to continue this workflow!' );
                                        return $form;
                                }
                        }
                }
                
                /**
                 * set some global parameters
                 */
        
                $link_book = Url::fromRoute('publisso_gold.books_book.manuscript_guidelines', ['bk_id' => $book->getElement('id')])->toString();
                $language = \Drupal::languageManager()->getCurrentLanguage();
        
		//Link1 (uri): Authors contract
		//Link2 (uri_1): Book policy
		$uri_de         = 'https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/#c5185';
		$url_de         = Url::fromUri($uri_de);
		$uri_1_de       = 'https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/';
		$url_1_de 	= Url::fromUri($uri_1_de);
		$text_de        = 'German';
	
		$uri_en         = 'https://www.publisso.de/en/publishing/books/books-policy/#c5189';
		$url_en 	= Url::fromUri($uri_en);
		$uri_1_en       = 'https://www.publisso.de/en/publishing/books/books-policy/';
		$url_1_en 	= Url::fromUri($uri_1_en);
		$text_en        = 'English';
		
                //initiate steps if not done
                if(!$form_state->get('step')) $form_state->set('step', 1);
                
                /*#############################################################################################*/
                
                /**
                 * show the form.
                 * at this point should all parameters set
                 * and only form-depending things processed
                 */
                $form['#tree'] = true;
                
                $form ['mu_site_headline'] = [
                        '#markup' => (string)$this->t(($workflow->getElement('state') == 'running submission' ? 'Add' : 'Edit').' Chapter'),
                        '#prefix' => '<h1>',
                        '#suffix' => '</h1>'
                ];
                
                $form['mu_book_title'] = [
                        '#markup' => $book->getElement('title'),
                        '#prefix' => '<h2>',
                        '#suffix' => '</h2>'
                ];
                
                /**
                 * form step 1
                 */
/* Step 1 */    if($form_state->get('step') == 1):
                        
                        //disclaimer
                        $form['mu_initial_info'] = [
                                '#type' => 'inline_template',
                                '#template' => (string)$this->t(
                                        'Please read the <a href="@link_book" target="_blank">manuscript guidelines</a>, the policy
                                        (<a href="@uri_1_de" target="_blank">@text_de</a>, <a href="@uri_1_en" target="_blank">@text_en</a>)
                                        and the author\'s contract (<a href="@uri_de" target="_blank">@text_de</a>, <a href="@uri_en" target="_blank">@text_en</a>).
                                        On the second page of the submission form, you will have to accept the author\'s contract (legally binding German version) to conclude the submission.',
                                        [
                                                '@link_book'    => $link_book,
                                                '@uri_1_de'     => $uri_1_de,
                                                '@text_de'      => $text_de,
                                                '@uri_1_en'     => $uri_1_en,
                                                '@text_en'      => $text_en,
                                                '@uri_de'       => $uri_de,
                                                '@uri_en'       => $uri_en
                                        ]
                                ),
                                '#suffix' => '</div><br><br>',
                                '#prefix' => '<div class="font-red">'
                        ];
                        
                        if(!$workflow->getDataElement('predefined_title')) {
                                //chapter-title
                                $form[ 'title' ] = [
                                        '#type'          => 'textfield',
                                        '#title'         => t( 'Title' ),
                                        '#required'      => true,
                                        '#placeholder'   => (string)$this->t( 'Enter the title of your chapter here' ),
                                        '#maxlength'     => 255,
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $workflow->getDataElement( 'title' )
                                ];
        
                                $form[ 'title_short' ] = [
                                        '#type'          => 'textfield',
                                        '#title'         => t( 'Short-title (for PDF-Output as "Running title")' ),
                                        '#required'      => true,
                                        '#placeholder'   => (string)$this->t( 'Enter the short-title of your chapter here' ),
                                        '#maxlength'     => 255,
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $workflow->getDataElement( 'title_short' )
                                ];
                        }
                        else{
                                $form['title'] = [
                                        '#markup' => $workflow->getDataElement('title'),
                                        '#prefix' => '<h2>',
                                        '#suffix' => '</h2>'
                                ];
                        }
                        
                        if($form_state->get('form_data')['title_short']) $form['title_short']['#default_value'] = $form_state->get('form_data')['title_short'];
                
                        //abstract (if required)
                        if($book->getControlElement('abstract_required') == 1 || $book->getControlElement('abstract_required') == 2):
                                
                                $form['abstract'] = [
                                        '#type'         => 'textarea',
                                        '#title'        => (string)$this->t('Abstract'),
                                        '#prefix'       => (string)$this->t('If an abstract is required or optional, please enter it here.'),
                                        '#rows'         => 20,
                                        '#attributes'   => [
                                                'class'         => [
                                                        \Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
                                                ],
                                                'readonly' => 'readonly'
                                        ],
                                        '#ajax'         => [
                                                'event'         => 'change',
                                                'prevent'       => 'blur',
                                                'progress'      => [
                                                        'type'          => 'none'
                                                ],
                                                'callback'      => '::autosave',
                                                'wrapper'       => 'autosave_msg'
                                        ],
                                        '#required'             => $book->getControlElement('abstract_required') == 1 ? true : false,
                                        '#default_value' => $workflow->getDataElement('abstract')
                                ];
                                
                        endif;
                        
                        //keywords
                        $form['keywords'] = [
                                '#type'                 => 'textfield',
                                '#title'                => (string)t('Keywords'),
                                '#placeholder'          => (string)$this->t('Enter the keywords separated with a semicolon here'),
                                '#maxlength'            => 400,
                                '#ajax'         => [
                                        'event'         => 'change',
                                        'prevent'       => 'blur',
                                        'progress'      => [
                                                'type'          => 'none'
                                        ],
                                        'callback'      => '::autosave',
                                        'disable-refocus'       => true
                                ],
                                '#default_value' => $workflow->getDataElement('keywords')
                        ];
                        
                        if($form_state->get('form_data')['keywords']) $form['keywords']['#default_value'] = $form_state->get('form_data')['keywords'];
                        
                        //chapter text
                        if(!$workflow->getDataElement('predefined_maintext')) {
                                $form[ 'chapter_text' ] = [
                                        '#type'          => 'textarea',
                                        '#title'         => t( 'Main Text' ),
                                        '#required'      => false,
                                        '#rows'          => 50,
                                        '#required'      => true,
                                        '#attributes'    => [
                                                'class'    => [
                                                        \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' )
                                                ],
                                                'readonly' => 'readonly'
                                        ],
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $workflow->getDataElement( 'chapter_text' )
                                ];
                        }
                        else{
                                $form['chapter_text'] = [
                                        '#type' => 'fieldset',
                                        '#title' => (string)$this->t('Main text'),
                                        'main_text' => [
                                                '#type' => 'inline_template',
                                                '#template' => $workflow->getDataElement('chapter_text')
                                        ]
                                ];
                        }
                        
                        if($form_state->get('form_data')['chapter_text']) $form['chapter_text']['#default_value'] = $form_state->get('form_data')['chapter_text'];
                
                        if(!$workflow->getDataElement('predefined_maintext')) {
                                $form[ 'references' ] = [
                                        '#type'          => 'textarea',
                                        '#title'         => (string)t( 'References' ),
                                        '#rows'          => 10,
                                        '#cols'          => 60,
                                        '#prefix'        => '<br>',
                                        '#id'            => 'ta-rwReferences',
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                //                                         'prevent'       => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $workflow->getDataElement( 'references' )
                                ];
                        }
                        else{
                                $form['references'] = [
                                        '#type' => 'fieldset',
                                        '#title' => (string)$this->t('References'),
                                        'main_text' => [
                                                '#type' => 'inline_template',
                                                '#template' => nl2br($workflow->getDataElement('references'))
                                        ]
                                ];
                        }
                        
                        if($form_state->get('form_data')['references']) $form['references']['#default_value'] = $form_state->get('form_data')['references'];
                        
                        if($workflow->hasDataElement('bookchapter_template')){
                                
                                $template = new BookchapterTemplate(null, $workflow->getDataElement('bookchapter_template'));
                                
                                $url = $template->getUrlFromWorkflow($workflow->getElement('id'));
                                $url->setOptions(['attributes' => ['class' => ['btn', 'btn-info']]]);
                                
                                if($url->access()){
                                        $form['edit_template'] = [
                                                '#type' => 'link',
                                                '#title' => (string)$this->t('Edit text/references'),
                                                '#url' => $url,
                                                '#suffix' => '<br>'
                                        ];
                                }
                        }
                        
                        //authors
                        $author_db = [];
                        if($workflow->getDataElement('author_db')){
                                $author_db = json_decode($workflow->getDataElement('author_db'), 1);
                        }
                        
                        $author_add = [];
                        if($workflow->getDataElement('author_add')){
                                $author_add = json_decode($workflow->getDataElement('author_add'), 1);
                        }
                        
                        if(count($author_db) && $form_state->getRebuildInfo('step1') != true) $form_state->set('cntAuthorsDB' , count($author_db));
                        if(count($author_add) && $form_state->getRebuildInfo('step1') != true) $form_state->set('cntAuthorsAdd', count($author_add));
                        if(!$form_state->has('cntAuthorsDB' ) || $form_state->get('cntAuthorsDB') < 3){ $form_state->set('cntAuthorsDB' , 3); }
                        if(!$form_state->has('cntAuthorsAdd')){ $form_state->set('cntAuthorsAdd', 1); }
                        
                        $form['fs_authors'] = [
                                '#type' => 'fieldset',
                                '#title' => (string)$this->t('Author(s)'),
                                '#prefix' => '<br>',
                                'content' => [
                                        'description' => [
                                                '#markup' => (string)$this->t('Enter one author per box. Authors who are not yet in the database can be added as additional authors.'),
                                                '#suffix' => '<br><br>'
                                        ],
                                        'author-db' => [
                                                '#type'   => 'container',
                                                '#prefix' => '<div id="author-db-wrapper">',
                                                '#suffix' => '</div>',
                                                '#tree'   => true,
                                                'content' => [
                                                        '#type' => 'table',
                                                        '#header' => [
                                                                (string)$this->t('Author(s)'),
                                                                (string)$this->t('Corresponding'),
                                                                (string)$this->t('Position')
                                                        ]
                                                ]
                                        ],
                                        'author-db-add' => [
                                                '#type'                    => 'submit',
                                                '#button_type'             => 'warning',
                                                '#value'                   => (string)$this->t('add box'),
                                                '#limit_validation_errors' => [],
                                                '#submit'                  => ['::addAuthorDB'],
                                                '#prefix'                  => '',
                                                '#suffix'                  => '<br><br>',
                                                '#description'             => (string)$this->t('Add further authors'),
                                                '#ajax'                    => [
                                                        'wrapper'       => 'author-db-wrapper',
                                                        'effect'        => 'fade',
                                                        'callback'      => '::addAuthorDBCallback'
                                                ]
                                        ],
                                        'author-add' => [
                                                '#type'    => 'container',
                                                '#prefix'  => '<div id="author-add-wrapper">',
                                                '#suffix'  => '</div>',
                                                '#tree'    => true,
                                                'content' => []
                                        ],
                                        'author-add-add' => [
                                                '#type'                    => 'submit',
                                                '#button_type'             => 'warning',
                                                '#value'                   => (string)$this->t('add box '), //Space is important!
                                                '#limit_validation_errors' => [],
                                                '#submit'                  => ['::addAuthorAdd'],
                                                '#prefix'                  => '<br>',
                                                '#description'             => (string)$this->t('For more additional authors, add more boxes'),
                                                '#ajax'                    => [
                                                        'wrapper'       => 'author-add-wrapper',
                                                        'effect'        => 'fade',
                                                        'callback'      => '::addAuthorAddCallback'
                                                ]
                                        ]
                                ]
                        ];
                
                        for($i = 0; $i < $form_state->get('cntAuthorsDB'); $i++){
                                
                                $form['fs_authors']['content']['author-db']['content'][$i] = [
                                        'author'           => [
                                                '#type'                          => 'textfield',
                                                '#autocomplete_route_name'       => 'autocomplete.user.public',
                                                '#autocomplete_route_parameters' => array(),
                                                '#title'                         => (string)$this->t('Author(s)'),
                                                '#title_display'                 => 'invisible',
                                                '#placeholder'                   => (string)$this->t('Enter at least 3 characters of the name and choose from the database'),
                                                '#description'                   => (string)$this->t('Matching entries from the database will be suggested automatically'),
                                                '#maxlength'                     => 256,
                                                '#id' => 'author_db_'.$i,
                                                '#ajax'         => [
                                                        'event'         => 'autocompleteclose change',
                                                        'prevent'       => 'blur autocompleteclose change',
                                                        'progress'      => [
                                                                'type'          => 'none'
                                                        ],
                                                        'callback'      => '::autosave',
                                                        'disable-refocus'       => true,
                                                        'wrapper' => 'wrapper-add-affiliations-'.$i,
                                                        'method' => 'html'
                                                ]
                                        ],
                                        'is-corresponding' => [
                                                '#type' => 'checkbox',
                                                '#ajax'         => [
                                                        'event'         => 'change',
                                                        'prevent'       => 'blur',
                                                        'progress'      => [
                                                                'type'          => 'none'
                                                        ],
                                                        'callback'      => '::autosave',
                                                        'disable-refocus'       => true
                                                ]
                                        ],
                                        'weight'           => [
                                                '#type'          => 'number',
                                                '#size'          => 3,
                                                '#min'           => 1,
                                                '#max'           => 100,
                                                '#description'   => (string)$this->t('The authors’ order can be determined here'),
                                                '#ajax'         => [
                                                        'event'         => 'change',
                                                        'prevent'       => 'blur',
                                                        'progress'      => [
                                                                'type'          => 'none'
                                                        ],
                                                        'callback'      => '::autosave',
                                                        'disable-refocus'       => true
                                                ]
                                        ],
                                        'wrapper-add_affiliation' => [
                                                '#type' => 'details',
                                                '#title' => (string)$this->t('Additional affiliations'),
                                                'add_affiliations' => [
                                                        '#type' => 'table',
                                                        '#header' => [
                                                                (string)$this->t('Affiliation'),
                                                                (string)$this->t('Show'),
                                                                (string)$this->t('Weight')
                                                        ]
                                                ],
                                                '#prefix' => '<div id="wrapper-add-affiliations-'.$i.'">',
                                                '#suffix' => '</div>'
                                        ]
                                ];
                                
                                if(isset($author_db[$i]['author'])){
                                        
                                        $form['fs_authors']['content']['author-db']['content'][$i]['author'          ]['#default_value'] = $author_db[$i]['author'          ];
                                        $form['fs_authors']['content']['author-db']['content'][$i]['is-corresponding']['#default_value'] = $author_db[$i]['is-corresponding'] ?? 0;
                                        $form['fs_authors']['content']['author-db']['content'][$i]['weight'          ]['#default_value'] = $author_db[$i]['weight'          ] ?? '';
                                        
                                        if(null !== ($user = $this->getAuthorFromString($form_state->getUserInput()['fs_authors']['content']['author-db']['content'][$i]['author'] ?? $author_db[$i]['author']))){
                                                
                                                $addAffiliations = $user->profile->getAdditionalAffiliations();
                                                
                                                if(!count($addAffiliations)){
                                                        $form['fs_authors']['content']['author-db']['content'][$i]['wrapper-add_affiliation']['add_affiliations'] = null;
                                                }
                                                else {
                                                        foreach ($addAffiliations as $_affiliation) {
                
                                                                $affiliation = implode('; ', array_filter([
                                                                        $_affiliation['department'],
                                                                        $_affiliation['institute'],
                                                                        $_affiliation['city'],
                                                                        Publisso::tools()->getCountry($_affiliation['country'])
                                                                ]));
                
                                                                $form['fs_authors']['content']['author-db']['content'][$i]['wrapper-add_affiliation']['add_affiliations'][] = [
                                                                        'affiliation' => [
                                                                                '#type'          => 'textfield',
                                                                                '#default_value' => $affiliation,
                                                                                '#maxlength' => 256
                                                                        ],
                                                                        'hidden'      => [
                                                                                '#type' => 'checkbox'
                                                                        ],
                                                                        'weight'      => [
                                                                                '#type'       => 'number',
                                                                                '#max-length' => 3,
                                                                                '#attributes' => [
                                                                                        'style' => 'width: 5em;'
                                                                                ]
                                                                        ]
                                                                ];
                                                        }
                                                }
                                        }
                                }
                        }
                        //\Drupal::service('publisso_gold.tools')->logMsg(print_r($form_state->get('cntAuthorsAdd'), 1));
                        
                        for($i = 0; $i < $form_state->get('cntAuthorsAdd'); $i++){
        
                                if(!$form_state->has('cntAddAuthorAffiliaitions-'.$i)){
                                        $form_state->set('cntAddAuthorAffiliaitions-'.$i, 0);
                                }
                                
                                $_args = ['strings' => ['@nr' => ($i + 1)]];
                                
                                $form['fs_authors']['content']['author-add']['content']['author-add-'.$i] =
                                        $this->formFields->getField('submitchapter.author_add', $_args);
                                $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content'] = [
                                        'firstname'     => $this->formFields->getField('submitchapter.author_add_firstname'     , $_args),
                                        'lastname'      => $this->formFields->getField('submitchapter.author_add_lastname'      , $_args),
                                        'affiliation'   => $this->formFields->getField('submitchapter.author_add_affiliation'   , $_args),
                                        'wrapper_add_affiliations' => [
                                                '#type' => 'container',
                                                '#prefix' => '<div id="wrapper-author-add-add_affiliations">',
                                                '#suffix' => '</div><br>',
                                                'add_affiliations' => [],
                                                'add' => [
                                                        '#type' => 'submit',
                                                        '#value' => (string)$this->t('Add affiliation'),
                                                        '#submit' => ['::addAuthorAddAffiliation'],
                                                        '#name' => 'add-author-add-affiliation-'.$i,
                                                        '#limit_validation_errors' => [],
                                                        '#attributes' => [
                                                                'index' => $i
                                                        ],
                                                        '#ajax' => [
                                                                'callback' => '::addAuthorAddAffiliationCallback',
                                                                'method' => 'html',
                                                                'wrapper' => 'wrapper-author-add-add_affiliations'
                                                        ]
                                                ]
                                        ],
                                        'weight'        => $this->formFields->getField('submitchapter.author_add_weight'        , $_args)
                                ];
                                
                                for($ii = 0; $ii < $form_state->get('cntAddAuthorAffiliaitions-'.$i); $ii++){
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['wrapper_add_affiliations']['add_affiliations'][$ii] = [
                                                '#type' => 'textfield'
                                        ];
                                }
                                
                                if(isset($author_add[$i])){
                                        
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['firstname'  ]['#default_value'] = $author_add[$i]['firstname'  ];
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['lastname'   ]['#default_value'] = $author_add[$i]['lastname'   ];
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['affiliation']['#default_value'] = $author_add[$i]['affiliation'];
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['weight'     ]['#default_value'] = $author_add[$i]['weight'     ];
                                }
                        }
                        
                        //corporations
                        $corporations = [];
                        if($workflow->getDataElement('corporation')){
                                $corporations = json_decode($workflow->getDataElement('corporation'), 1);
                        }
                        if(count($corporations) && $form_state->getRebuildInfo('step1') != true) $form_state->set('cntCorporations' , count($corporations));
                        if(!$form_state->has('cntCorporations')){ $form_state->set('cntCorporations', 1); }
                        
                        $form['corporations'] = [
                                '#type'   => 'container',
                                '#prefix' => '<div id="corporations-wrapper">',
                                '#suffix' => '</div>',
                                '#tree'   => true,
                                'content' => []
                        ];
                        
                        $form['corporations-add'] = [
                                '#type'                    => 'submit',
                                '#button_type'             => 'warning',
                                '#value'                   => (string)$this->t('add corporation'), //Space is important!
                                '#limit_validation_errors' => [],
                                '#submit'                  => ['::addCorporation'],
                                '#prefix'                  => '<br>',
                                '#description'             => (string)$this->t('Add more corporations'),
                                '#ajax'                    => [
                                        'wrapper'       => 'corporations-wrapper',
                                        'effect'        => 'fade',
                                        'callback'      => '::addCorporationCallback'
                                ]
                        ];
                        
                        for($i = 0; $i < $form_state->get('cntCorporations'); $i++){
                                
                                $form['corporations']['content'][$i] = [
                                        '#type'          => 'textfield',
                                        '#title'         => (string)$this->t('Corporation(s)'),
                                        '#title_display' => $i ? 'invisible' : 'before',
                                        '#placeholder'   => (string)$this->t('Enter corporations that should appear as authors here'),
                                        '#description'   => (string)$this->t('One corporation per box'),
                                        '#ajax'         => [
                                                'event'         => 'change',
                                                'prevent'       => 'blur',
                                                'progress'      => [
                                                        'type'          => 'none'
                                                ],
                                                'callback'      => '::autosave',
                                                'disable-refocus'       => true
                                        ]
                                ];
                                
                                if(isset($corporations[$i])) $form['corporations']['content'][$i]['#default_value'] = $corporations[$i];
                        }
                        
/* Step 2 */    elseif($form_state->get('step') == 2):
                        
                        //conflict of interest
                        $form['conflict_of_interest'] = [
                                '#type'         => 'checkbox',
                                '#title'        => (string)t('Conflict of interest'),
                                '#prefix'       => (string)$this->t('Here you may add if there’s any conflict of interest to declare, which might be relevant regarding the development of your chapter. If there’s anything to declare you can add a description in the appearing box.'),
                                '#ajax'         => [
                                        'event'         => 'change',
                                        'prevent'       => 'blur',
                                        'progress'      => [
                                                'type'          => 'none'
                                        ],
                                        'callback'      => '::autosave',
                                        'disable-refocus'       => true,
                                        'suppress_required_fields_validation' => true
                                ],
                                '#default_value' => $workflow->getDataElement('conflict_of_interest')
                        ];
                        
                        $form['conflict_of_interest_text'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t('Description of conflict of interest'),
                                '#maxlength'     => 256,
                                '#default_value' => isset($workflow_data) ? $workflow_data->conflict_of_interest_text : '',
                                '#states'        => array(
                                        'visible'       => array(   // action to take.
                                                ':input[name="conflict_of_interest"]' => array('checked' => true),
                                        ),
                                        'invisible'     => array(   // action to take.
                                                ':input[name="conflict_of_interest"]' => array('checked' => false),
                                        )
                                ),
                                '#ajax'         => [
                                        'event'         => 'change',
                                        'prevent'       => 'blur',
                                        'progress'      => [
                                                'type'          => 'none'
                                        ],
                                        'callback'      => '::autosave',
                                        'disable-refocus'       => true
                                ],
                                '#default_value' => $workflow->getDataElement('conflict_of_interest_text')
                        ];
                        
                        //funding
                        $form['funding'] = [
                                '#type'         => 'checkbox',
                                '#title'        => t('Funds received'),
                                '#prefix'       => (string)$this->t('If you received any funds please add the information here.'),
                                '#ajax'         => [
                                        'event'         => 'change',
                                        'prevent'       => 'blur',
                                        'progress'      => [
                                                'type'          => 'none'
                                        ],
                                        'callback'      => '::autosave',
                                        'disable-refocus'       => true
                                ],
                                '#default_value' => $workflow->getDataElement('funding')
                        ];
                        
                        $form['funding_name'] = [
                                '#type'      => 'textfield',
                                '#title'     => (string)$this->t('Funding name'),
                                '#maxlength' => 256,
                                '#states'    => array(
                                        'visible'   => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => true),
                                        ),
                                        'invisible' => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => false),
                                        )
                                ),
                                '#ajax'         => [
                                        'event'         => 'change',
                                        'prevent'       => 'blur',
                                        'progress'      => [
                                                'type'          => 'none'
                                        ],
                                        'callback'      => '::autosave',
                                        'disable-refocus'       => true
                                ],
                                '#default_value' => $workflow->getDataElement('funding_name')
                        ];
                        
                        $form['funding_id'] = [
                                '#type'   => 'textfield',
                                '#title'  => (string)t('Funding ID'),
                                '#states' => array(
                                        'visible'   => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => true),
                                        ),
                                        'invisible' => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => false),
                                        )
                                ),
                                '#ajax'         => [
                                        'event'         => 'change',
                                        'prevent'       => 'blur',
                                        'progress'      => [
                                                'type'          => 'none'
                                        ],
                                        'callback'      => '::autosave',
                                        'disable-refocus'       => true
                                ],
                                '#default_value' => $workflow->getDataElement('funding_id')
                        ];
                        
                        //reviewer suggestion
                        if($book->getControlElement('reviewer_suggestion') == 1):
				
				$form['reviewer_suggestion'] = [
					'#type'  => 'textfield',
					'#title' => t('Reviewer suggestion'),
                                        '#placeholder' => (string)$this->t('Enter the names of potential reviewers of your chapter here'),
                                        '#ajax'         => [
                                                'event'         => 'change',
                                                'prevent'       => 'blur',
                                                'progress'      => [
                                                        'type'          => 'none'
                                                ],
                                                'callback'      => '::autosave',
                                                'disable-refocus'       => true
                                        ],
                                        '#default_value' => $workflow->getDataElement('reviewer_suggestion')
				];
    
			endif;
                        
                        //additional files
                        $form['add_files'] = [
                                '#type'  => 'checkbox',
                                '#title' => (string)$this->t('I would like to upload (additional) files'),
                                '#ajax'         => [
                                        'event'         => 'change',
                                        'prevent'       => 'blur',
                                        'progress'      => [
                                                'type'          => 'none'
                                        ],
                                        'callback'      => '::autosave',
                                        'disable-refocus'       => true
                                ],
                                '#default_value' => $workflow->getDataElement('add_files')
                        ];
                        
                        //accept policy
			$form['accept_policy'] = [
				'#type'     => 'checkbox',
                                '#prefix'   => (string)$this->t('Please read the policy (<a href="'.$uri_1_de.'" target="_blank">'.$text_de.'</a>, <a href="'.$uri_1_en.'" target="_blank">'.$text_en.'</a>) and accept the author\'s contract (legally binding <a href="'.$uri_de.'" target="_blank">German version</a>, <a href="'.$uri_en.'" target="_blank">translated version</a>) to conclude the submission.'),
				'#title'    => (string)t('I accept the author\'s contract</a>'),
				'#required' => true,
				'#access' => !$form_state->get('editorial'),
                                '#ajax'         => [
                                        'event'         => 'change',
                                        'prevent'       => 'blur',
                                        'progress'      => [
                                                'type'          => 'none'
                                        ],
                                        'callback'      => '::autosave',
                                        'disable-refocus'       => true
                                ],
                                '#default_value' => $workflow->getDataElement('accept_policy')
			];
                        
                        if(isset($form_state->get('form_data')['accept_policy'])) $form['accept_policy']['#default_value'] = $form_state->get('form_data')['accept_policy'];
                        true;
                        
/* Step 3 */    elseif($form_state->get('step') == 3):
                
                        $form['description'] = array(
                                '#markup' => (string)$this->t('Here you can upload additional Files to your submission'),
                                '#prefix' => '<h3>',
                                '#suffix' => '</h3>'
                        );
                        
                        $form['files_fieldset'] = [
                                '#type' => 'fieldset',
                                '#title' => (string)$this->t('Upload additional Files'),
                                '#prefix' => '<div id="files-fieldset-wrapper">',
                                '#suffix' => '</div>',
                        ];
                        
                        if(!$form_state->has('numFiles')) $form_state->set('numFiles', 1);
                        $numFiles = $form_state->get('numFiles');
                        
                        for ($i = 0; $i < $numFiles; $i++) {

                                $form['files_fieldset']['file'][$i] = [

                                        '#type' => 'fieldset',
                                        'file'  => [
                                                '#type'                 => 'managed_file',
                                                '#title'                => (string)$this->t('File').' #'.$i,
                                                '#required'             => true,
                                                '#upload_validators'    => [
                                                        'file_validate_extensions' => \Drupal::service('publisso_gold.setup')->getValue('file.upload.valid_extensions') ? [\Drupal::service('publisso_gold.setup')->getValue('file.upload.valid_extensions')] : array('gif png jpg jpeg doc docx xls xlsx midi mp4 mp3 avi mpg mpeg wav mov mid odt ods odp pdf'),
                                                        // Pass the maximum file size in bytes
                                                        'file_validate_size' => array(30 * 1024 * 1024),
                                                ],
                                                '#progress_indicator'   => 'bar',
                                                '#progress_message'     => (string)$this->t('Uploading file...')
                                        ],
                                        'meta'  => [
                                                'name' => [
                                                        '#type'         => 'textfield',
                                                        '#title'        => (string)$this->t('Filename'),
                                                        '#required'     => true
                                                ],
                                                'description'   => [
                                                        '#type'         => 'textfield',
                                                        '#title'        => (string)$this->t('Description')
                                                ]
                                        ]
                                ];
                        }
                        
                        $form['files_fieldset']['actions']['add_name'] = [
                                '#type' => 'submit',
                                '#value' => t('Add file'),
                                '#submit' => array('::addOne'),
                                '#prefix' => '<br>',
                                '#ajax' => [
                                        'callback' => '::addmoreCallback',
                                        'wrapper' => 'files-fieldset-wrapper',
                                        'effect' => 'fade',
                                        'progress' => [
                                                'type' => 'none'
                                        ]
                                ],
                        ];
                        
                        if($numFiles > 1){

                                $form['files_fieldset']['actions']['remove_file'] = [
                                        '#type' => 'submit',
                                        '#value' => t('Remove last'),
                                        '#submit' => array('::removeCallback'),
                                        '#prefix' => '&nbsp;&nbsp;&nbsp;',
                                        '#ajax' => [
                                                'callback' => '::addmoreCallback',
                                                'wrapper' => 'files-fieldset-wrapper',
                                                'effect' => 'fade',
                                                'progress' => [
                                                        'type' => 'none'
                                                ]
                                        ],
                                        '#limit_validation_errors' => []
                                ];
                        }
                endif;
                
                /*#############################################################################################*/
                
                $form['spacer'] = [
                        '#markup' => '<hr>'
                ];
                
                $form['close'] = [
                        '#type'                         => 'button',
                        '#value'                        => (string)$this->t('Back to book'),
                        '#button_type'                  => 'warning',
                        '#suffix'                       => '&nbsp;&nbsp;&nbsp;',
                        '#limit_validation_errors'      => [],
                        '#ajax'                         => [
                                'callback' => '::backToBook',
                                'progress' => [
                                        '#type' => 'none'
                                ]
                        ]
                ];
                
                $form['close1'] = [
                        '#type'                         => 'button',
                        '#value'                        => (string)$this->t('Back to dashboard'),
                        '#button_type'                  => 'warning',
                        '#suffix'                       => '&nbsp;&nbsp;&nbsp;',
                        '#limit_validation_errors'      => [],
                        '#ajax'                         => [
                                'callback' => '::backToDashboard',
                                'progress' => [
                                        '#type' => 'none'
                                ]
                        ]
                ];
                
                $form['preview'] = [
			'#type'                         => 'button',
			'#value'                        => (string)$this->t('Preview'),
			'#button_type'                  => 'primary',
			'#suffix'                       => '&nbsp;&nbsp;&nbsp;',
                        '#limit_validation_errors'      => [],
                        '#description'                  => (string)$this->t('See how your chapter looks like'),
			'#ajax'                         => [
                                'callback' => '::previewBookchapter',
                                'progress' => [
                                        'type' => 'none'
                                ]
                        ]
		];
                
                $form['back'] = [
                        '#type'                    => 'submit',
                        '#value'                   => (string)$this->t('Back'),
                        '#button_type'             => 'danger',
                        '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                        '#submit'                  => ['::stepBack'],
                        '#access'                  => $form_state->get('step') != 1,
                        '#limit_validation_errors' => []
                ];
                
                $form['submit_1'] = [
                        '#type'         => 'submit',
                        '#value'        => (string)$this->t('Continue submission'),
                        '#button_type'  => 'success',
                        '#access'       => $form_state->get('step') == 1,
                        '#submit'       => ['::processForm']
                ];
                
                $form['submit_2'] = [
                        '#type'         => 'submit',
                        '#value'        => (string)$this->t('Submit'),
                        '#button_type'  => 'success',
                        '#access'       => $form_state->get('step') == 2,
                        '#submit'       => ['::processForm'],
                        '#attributes'   => [
                                'onclick' => 'if(!confirm("'.t('When you conclude the submission, it will enter the review process. You won’t be able to edit the submission until a possible revision.\n\nOK: Finish submission\nCancel: Continue submission').'")){return false;}'
                        ],
                        '#states'       => [
                                'visible'   => [   // action to take.
                                        ':input[name="add_files"]' => array('checked' => false),
                                ],
                                'invisible' => [   // action to take.
                                        ':input[name="add_files"]' => array('checked' => true),
                                ]
                        ]
                ];
                
                $form['submit_3'] = [
                        '#type'         => 'submit',
                        '#button_type'  => 'success',
                        '#value'        => (string)$this->t('Continue submission'),
                        '#submit'       => ['::processForm'],
                        '#access'       => $form_state->get('step') == 2,
                        '#states'       => array(
                                'visible'   => [// action to take.
                                                        ':input[name="add_files"]' => array('checked' => true),
                                ],
                                'invisible' => [// action to take.
                                                        ':input[name="add_files"]' => array('checked' => false),
                                ]
                        )
                ];
                
                $form['submit_4'] = [
                        '#type'         => 'submit',
                        '#value'        => (string)$this->t('Submit'),
                        '#button_type'  => 'success',
                        '#access'       => $form_state->get('step') == 3,
                        '#submit'       => ['::processForm'],
                        '#attributes'   => [
                                'onclick' => 'if(!confirm("'.t('When you conclude the submission, it will enter the review process. You won’t be able to edit the submission until a possible revision.\n\nOK: Finish submission\nCancel: Continue submission').'")){return false;}'
                        ]
                ];
                
                $form['store_data_control_key'] = [
			'#type'         => 'hidden',
			'#value'        => 'previewchapter'
		];
                
                $form['ctrl_action'] = [
                        '#type' => 'hidden',
                        '#value' => 'autosave'
                ];
                
                $form['ctrl_type'] = [
                        '#type' => 'hidden',
                        '#value' => 'bookchapter'
                ];
                
                $form['tmp_id'] = [
                        '#type' => 'hidden',
                        '#id' => 'tmp_id'
                ];
                
                if(!empty($form_state->get('form_data')['tmp_id'])){
                        $form['tmp_id']['#value'] = $form_state->get('form_data')['tmp_id'];
                }
                
                $form['bk_id'] = [
                        '#type' => 'hidden',
                        '#value' => $book->getElement('id')
                ];
                /*
                $form['debug'] = [
                        '#markup' => print_r($form_state->get('form_data'), 1),
                        '#prefix' => '<pre> Arguments:',
                        '#suffix' => '</pre>'
                ];
                */
                
                $form['append-js'] = [
                        '#type'         => 'inline_template',
                        '#template'     => '<script type="text/javascript">var int = window.setInterval("autosaveBookchapter(\''.$this->getFormId().'\', \''.(base64_encode(serialize($form_state->get('form_data')))).'\')", 30000);</script>'
                ];
                
                $form['#cache'] = [
                        'max-age' => 0
                ];
                
                $form_state->setCached(false);
                
                return $form;
        }
        
        /**
         * {@inheritdoc}
         */
        public function validateForm(array &$form, FormStateInterface $form_state){
                
                switch($form_state->get('step')){
                        case 1:
                                $this->validateStep1($form, $form_state);
                                break;
                        case 2:
                                break;
                        case 3:
                                break;
                }
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        protected function validateStep1(array &$form, FormStateInterface $form_state){
                
                //\Drupal::service('publisso_gold.tools')->logMsg($form_state->getValue('abstract'));
                
                $i = 0;
                $corresponding = false;
                foreach($form_state->getValue('fs_authors')['content']['author-db']['content'] as $_){
                        
                        if(!$corresponding && $_['is-corresponding']) $corresponding = true;
                        
                        [$author, $data] = preg_split('/;\s{1,}/', $_['author'], 2);
                        
                        if(!empty($author)){
                                
                                $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                $found = 0;
                                $last_id = null;
                                $string = '';

                                foreach($res as $v){
                                        $string .= ' || '.implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)])).' | '.implode( '; ', [$author, $data]);
                                        if(
                                                implode( '; ', [$author, $data])
                                                                ==
                                                implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)]))
                                        ){
                                                $found++;
                                                $last_id = $v->up_uid;
                                        }
                                }

                                if($found != 1){
                                        $form_state->setError($form, (string)$this->t('Please use only suggested values in author-field #@i!', ['@i' => $i + 1]));
                                }
                        }
                        $i++;
                }
                
                if($form_state->get('editorial') && !$corresponding){
                        $form_state->setErrorByName('author-db', 'You have to set at least one corresponding author!');
                }
                
                return $form;
        }
        
        /**
         * {@inheritdoc}
         */
        public function submitForm(array &$form, FormStateInterface $form_state){
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function processForm(array &$form, FormStateInterface $form_state){
                
                switch($form_state->get('step')){
                        case 1:
                                $this->processStep1($form, $form_state);
                                
                                //proceed to step 2
                                $form_state->set('step', 2);
                                $form_state->setRebuild();
                                break;
                        case 2:
                                $this->processStep2($form, $form_state);
                                
                                if($form_state->getValue('add_files') == 1){
                                        $form_state->set('step', 3);
                                        $form_state->setRebuild();
                                }
                                else{
                                        $this->finishSubmission($form, $form_state);
                                }
                                break;
                        case 3:
                                $this->processStep3($form, $form_state);
                                $this->finishSubmission($form, $form_state);
                                break;
                }
                return $form;
        }
        
        /**
         * @param $form
         * @param FormStateInterface $form_state
         */
        protected function processStep1(&$form, FormStateInterface $form_state){
                
                /**
                 * store formdata in a temporary storage
                 */
                $workflow = $form_state->get('workflow');
                $simpleValues = [
                        'title',
                        'title_short',
                        'keywords',
                        'abstract',
                        'chapter_text',
                        'references',
                        'conflict_of_interest',
                        'conflict_of_interest_text',
                        'funding',
                        'funding_id',
                        'funding_name',
                        'accept_policy',
                        'reviewer_suggestion',
                        'add_files'
                ];
        
                foreach($simpleValues as $_){
                        if($form_state->hasValue($_)){
                                $workflow->setDataElement($_, $form_state->getValue($_));
                        }
                }
        
                $workflow->setDataElement('author_db', json_encode($form_state->getValue('fs_authors')['content']['author-db']['content']));
                
                $authors = [];
                foreach($form_state->getValue('fs_authors')['content']['author-add']['content'] as $_){
                        array_push($authors, $_['content']);
                }
                $workflow->setDataElement('author_add', json_encode($authors));
                $workflow->setDataElement('corporations', json_encode($form_state->getValue('corporations')['content'] ?? []));
                
                $workflow->reload();
                $form_state->set('workflow', $workflow);
        }
        
        /**
         * @param $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        protected function processStep2(&$form, FormStateInterface $form_state){
                
                $workflow = $form_state->get('workflow');
                
                $keys = [
                        'conflict_of_interest'     ,
                        'conflict_of_interest_text',
                        'funding'                  ,
                        'funding_name'             ,
                        'funding_id'               ,
                        'reviewer_suggestion'      ,
                        'accept_policy'            ,
                        'add_files'
                ];
        
                foreach($keys as $_){
                        if($form_state->hasValue($_)){
                                $workflow->setDataElement($_, $form_state->getValue($_));
                        }
                }
                return $form;
        }
        
        /**
         * @param $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        protected function processStep3(&$form, FormStateInterface $form_state){
                
                //Step 3 is actually the last - will be processed in "::finishSubmission"
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function stepBack(array &$form, FormStateInterface $form_state){
                
                switch($form_state->get('step')){
                        
                        case 2:
                                $this->processStep2($form, $form_state);
                                break;
                        case 3:
                                $this->processStep3($form, $form_state);
                                break;
                }
                
                $form_state->set('step', ((int)$form_state->get('step')) - 1);
                $form_state->setRebuildInfo([]);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function storeFormData2Workflow(array &$form, FormStateInterface $form_state){
                #publisso::log(print_r($form_state->getValues(), 1));
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        protected function finishSubmission(array &$form, FormStateInterface $form_state){
                
                $workflow = $form_state->get('workflow');
                $book = $workflow->getParentMedium();
                
                
                //abstract
                $workflow->setDataElement('abstract', base64_encode($workflow->getDataElement('abstract')), false);

                //chapter_text
                $workflow->setDataElement('chapter_text', base64_encode($workflow->getDataElement('chapter_text')), false);
                
                //keywords
                $keywords = array_filter(array_map('trim', preg_split('/[,;]/', $workflow->getDataElement('keywords'))));
                $workflow->setDataElement('keywords', json_encode($keywords), false);
                
                //authors
                $authors = [];
                
                foreach(json_decode($workflow->getDataElement('author_db'), true) as $item){
                        
                        $author           = $item['author'];
                        $is_corresponding = $item['is-corresponding'];
                        $weight           = $item['weight'];
                        
                        [$author, $data] = preg_split('/;\s{1,}/', $author, 2);
                        if(!empty($author)){
                                $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                $found = 0;
                                $last_id = null;
                                $string = '';

                                foreach($res as $v){
                                        $string .= ' || '.implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)])).' | '.implode( '; ', [$author, $data]);
                                        if(
                                                implode( '; ', [$author, $data])
                                                                ==
                                                implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)]))
                                        ){
                                                $found++;
                                                $last_id = $v->up_uid;
                                        }
                                }

                                if($found == 1){
                                        
                                        $user = new \Drupal\publisso_gold\Controller\User($last_id);
                                        
                                        $authors[] = [
                                                'weight'             => $weight,
                                                'is_corresponding'   => $is_corresponding,
                                                'firstname'          => $user->profile->getElement('firstname'),
                                                'lastname'           => $user->profile->getElement('lastname'),
                                                'affiliation'        => $user->profile->getElement('institute'),
                                                'uid'                => $user->getElement('id'),
                                                'profile_archive_id' => $user->profile->makeSnapshot(\Drupal::service('session')->get('user')['id'])
                                        ];
                                }
                        }
                }
                
                /**
                 * if editorial workflow, the first author "is_sorresponding" will
                 * be set as creator
                 **/
                if($form_state->get('editorial')){
                        
                        foreach($authors as $_){
                                if($_['is_corresponding'] && $_['uid']){
                                        $workflow->setElement('created_by_uid', $_['uid']);
                                        break;
                                }
                        }
                }
                
                /**
                 * add the additional authors to workflow
                 **/
                foreach(json_decode($workflow->getDataElement('author_add'), true) as $item){
                        
                        if(!(empty($item['firstname']) || empty($item['lastname']) || empty($item['affiliation'])))
                                $authors[] = array_map('trim', $item);
                }
                
                $workflow->setDataElement('authors', json_encode($authors), false);
                
                //files
                $blobIds = [];
                $wfBlobs = unserialize($workflow->getDataElement('files'));
                if(!is_array($wfBlobs)) $wfBlobs = [];
                
                if($form_state->hasValue('files_fieldset')){
                        
                        foreach($form_state->getValue('files_fieldset')['file'] as $file){

                                if(count($file['file'])){

                                        $file_id = $file['file'][0];
                                        $file_meta = serialize($file['meta']);
                                        $file_db = \Drupal::database()->select('file_managed', 't')->fields('t', [])->condition('fid', $file_id, '=')->execute()->fetchAssoc();
                                        $file_type = $file_db['filemime'];
                                        $file_path = $file_db['uri'];

                                        if(preg_match('/^temporary:\/\/(.+)/', $file_path, $matches)){
                                                $file_content = file_get_contents('/tmp/'.$matches[1]);
                                        }

                                        if($file_type && $file_content){

                                                $blob = new \Drupal\publisso_gold\Controller\Blob();
                                                $blob->create($file_content, $file_type, $file_meta);
                                                $blobIds[] = $blob->getId();
                                        }
                                }
                        }
                }
                
                $workflow->setDataElement('files', serialize(array_unique(array_merge($wfBlobs, $blobIds))));

                $follower_state = $workflow->getSchemaDefaultFollower('schema_follower') ?? null;
                $new_status = $workflow->getSchemaFollowers($follower_state)[0]['schema_status'];
                $mailtemplate = $workflow->getSchemaItemByIdentifierAndFollower($workflow->getElement('schema_identifier'), $follower_state)['schema_mailtemplate_leave'];
                
                if(!$follower_state){
                        \Drupal::service('messenger')->addError((string)$this->t('Cant proceed workflow (no follower determined)!'));
                        return $form;
                }
        
                //read Editors-in-Chief
                $eic = $book->readEditorsInChief();

                //read Editorial Office
                $eo = $book->readEditorialOffice();

                $assigned_users = [];
                $directState = null;
                
                /**
                 * hier absichtlich auf -1 gesetzt, da es laut neuester
                 * Anforderung immer zuerst ans EO gehen soll
                 * siehe Ticket #136
                 */
                if(count($eic) == -1){ //only one Editor-in-Chief, bypass editorial office
                        $submission_assign = 'u:'.$eic[0]->getElement('id');
                        $workflow->setDataElement('state', 'assigned to eic', false);
                        $workflow->setElement('assigned_to_eic', $eic[0]->getElement('id'));
                        $assigned_users[] = $eic[0];
                        $directState = 'assignedtoeic';
                }
                elseif(count($eo) > 0){ //submit to editorial office

                        $submission_assign = '';

                        foreach($eo as $_){
                                
                                $submission_assign .= (!empty($submission_assign) ? ',' : '') . 'u:'.$_->getElement('id');

                                if(!in_array($_->getElement('id'), $assigned_users))
                                        $assigned_users[] = $_->getElement('id');
                        }
                }
                else{ //submission to publisso
                        $role_id = \Drupal::service('publisso_gold.setup')->getValue('default.admin.role.id');
                        $submission_assign = "r:$role_id";
                        
                        //set these users as eo
                        $assigned_users = \Drupal::service('publisso_gold.tools')->getUserIDsByRole($role_id);
                        
                        foreach($assigned_users as $_)
                                $eo[] = new \Drupal\publisso_gold\Controller\User($_);
                }
                
                $workflow->setElement('assigned_to', $submission_assign);
                
                //save editorial office
                if(count($eo)){
                        $eo_str = '';

                        foreach($eo as $_){
                                $eo_str .= (!empty($eo_str) ? ',' : '') . $_->getElement('id');
                        }

                        $workflow->setElement('assigned_to_eo', $eo_str, false);
                }
                
                
                
                $workflow->setElement('state', $new_status);
                $workflow->setElement('schema_identifier', $follower_state);
                
                if($directState) $workflow->setState($directState);
                
                $workflow->setElement('modified', date('Y-m-d H:i:s'), false);
                $workflow->save();
                $workflow->unlock();

                if($mailtemplate) {
                        $mail = new WorkflowInfoMail($workflow, $mailtemplate);
                        if ($mail) $mail->send();
                }
                
                $form_state->setRedirect('publisso_gold.dashboard');
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthorDB(array &$form, FormStateInterface $form_state){
                $cntAuthorsDB = $form_state->get('cntAuthorsDB');
                $cntAuthorsDB++;
                $form_state->set('cntAuthorsDB', $cntAuthorsDB);
                $form_state->addRebuildInfo('step1', true);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorDBCallback(array &$form, FormStateInterface $form_state){
                return $form['fs_authors']['content']['author-db'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthorAdd(array &$form, FormStateInterface $form_state){
                $cntAuthorsAdd = $form_state->get('cntAuthorsAdd');
                $cntAuthorsAdd++;
                $form_state->set('cntAuthorsAdd', $cntAuthorsAdd);
                $form_state->addRebuildInfo('step1', true);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorAddCallback(array &$form, FormStateInterface $form_state){
                return $form['fs_authors']['content']['author-add'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addCorporation(array &$form, FormStateInterface $form_state){
                $cntCorporations = $form_state->get('cntCorporations');
                $cntCorporations++;
                $form_state->set('cntCorporations', $cntCorporations);
                $form_state->addRebuildInfo('step1', true);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addCorporationCallback(array &$form, FormStateInterface $form_state){
                //error_log('Calling "addCorporationCallback"...');
                return $form['corporations'];
        }
        
        /**
         * @param $bk_id
         * @return array
         */
        protected function getBook($bk_id){
                
                if(empty($bk_id)){
                        return [
                                'msg' => (string)$this->t('Initiate submission without book! Abort!'),
                                'book' => false
                        ];
                }
                
                //try to load book. if fails, throw an error!
                #$book = new \Drupal\publisso_gold\Controller\Book($bk_id);
                $book = BookManager::getBook($bk_id);
                
                if(!count($book->getKeys())){
                        return [
                                'msg' => (string)$this->t('Book (@bk_id) not found! Abort!', ['@bk_id' => $bk_id]),
                                'book' => false
                        ];
                }
                
                return [
                        'msg' => 'OK',
                        'book' => $book
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addOne(array &$form, FormStateInterface $form_state) {
                
                $numFiles = $form_state->get('numFiles');
                $form_state->set('numFiles', ++$numFiles);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addmoreCallback(array &$form, FormStateInterface $form_state) {
                return $form['files_fieldset'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function removeCallback(array &$form, FormStateInterface $form_state) {
                
                $numFiles = $form_state->get('numFiles');
                if($numFiles > 1) $form_state->set('numFiles', --$numFiles);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @return AjaxResponse
         */
        public function delFile(array &$form, FormStateInterface $form_state, $args = []) {
                
                $fid = $form_state->getTriggeringElement()['fid'];
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $response = new AjaxResponse();
                
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                $fids = array_diff($fids, [$fid]);
                $workflow->setDataElement('files', serialize($fids));
                
                //check if file are deleted
                //reload Workflow
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                
                if(array_search($fid, $fids)){
                        
                        $file = new \Drupal\publisso_gold\Controller\Blob($fid);
                        $response->addCommand(new AppendCommand('div#rwBlob-'.$fid, '<div class="rwError">'.((string)$this->t('Error deleting file "'.$file->meta['name'].'"')).'</div>'));
                }
                else{
                        $response->addCommand(new RemoveCommand('div#rwBlob-'.$fid));
                }
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return AjaxResponse
         * @throws \Exception
         */
        public function previewBookchapter(array &$form, FormStateInterface $form_state){
                
                $this->processForm($form, $form_state);
                $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                $this->storeFormData2Workflow($form, $form_state);
                $workflow->publish(false, true);
                $workflow->reload();
                $form_state->set('workflow', $workflow);
                
                $chapter = ChapterManager::getChapter($workflow->getDataElement('cp_id'));
                $chapter->setElement('citation_note', null);
        
                //abstract
                $chapter->setElement('abstract', base64_encode($chapter->getElement('abstract')), false);
        
                //chapter_text
                $chapter_text = $chapter->getElement('chapter_text');
                if(!Publisso::tools()->isBase64($chapter_text)){
                        $chapter_text = base64_encode($chapter_text);
                }
                $chapter->setElement('chapter_text', $chapter_text, false);
        
                //keywords
                $keywords = array_filter(array_map('trim', preg_split('/[,;]/', $form_state->getValue('keywords'))));
                $chapter->setElement('keywords', json_encode($keywords), false);
        
                //authors
                $authors = [];
        
                foreach(json_decode($workflow->getDataElement('author_db'), true) as $item){
                
                        $author           = $item['author'];
                        $is_corresponding = $item['is-corresponding'];
                        $weight           = $item['weight'];
                
                        [$author, $data] = preg_split('/;\s{1,}/', $author, 2);
                        if(!empty($author)){
                                $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                $found = 0;
                                $last_id = null;
                                $string = '';
                        
                                foreach($res as $v){
                                        $string .= ' || '.implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)])).' | '.implode( '; ', [$author, $data]);
                                        if(
                                                implode( '; ', [$author, $data])
                                                ==
                                                implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)]))
                                        ){
                                                $found++;
                                                $last_id = $v->up_uid;
                                        }
                                }
                        
                                if($found == 1){
                                
                                        $user = new \Drupal\publisso_gold\Controller\User($last_id);
                                
                                        $authors[] = [
                                                'weight'             => $weight,
                                                'is_corresponding'   => $is_corresponding,
                                                'firstname'          => $user->profile->getElement('firstname'),
                                                'lastname'           => $user->profile->getElement('lastname'),
                                                'affiliation'        => $user->profile->getElement('institute'),
                                                'uid'                => $user->getElement('id'),
                                                'profile_archive_id' => $user->profile->makeSnapshot(\Drupal::service('session')->get('user')['id'])
                                        ];
                                }
                        }
                }
        
                /**
                 * if editorial workflow, the first author "is_sorresponding" will
                 * be set as creator
                 **/
                if($form_state->get('editorial')){
                
                        foreach($authors as $_){
                                if($_['is_corresponding'] && $_['uid']){
                                        $chapter->setElement('created_by_uid', $_['uid']);
                                        break;
                                }
                        }
                }
        
                /**
                 * add the additional authors to workflow
                 **/
                foreach(json_decode($workflow->getDataElement('author_add'), true) as $item){
                
                        if(!(empty($item['firstname']) || empty($item['lastname']) || empty($item['affiliation'])))
                                $authors[] = array_map('trim', $item);
                }
        
                $chapter->setElement('authors', json_encode($authors), false);
                $chapter->save();
                
                $response = new AjaxResponse();
                $response->addCommand(new OpenUrlCommand(null, $chapter->getLink(), '_blank'));
                
                
                
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return AjaxResponse
         */
        public function backToBook(array &$form, FormStateInterface $form_state){
                
                $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
                $tempstore->delete('bookchapter_tmpid');
                
                $url = Url::fromRoute('publisso_gold.books_book', ['bk_id' => $form_state->get('book')->getElement('id')]);
                $url->setAbsolute();
                
                $response = new AjaxResponse();
                $command  = new RedirectCommand($url->toString());
                $response->addCommand($command);
                return $response;
                
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return AjaxResponse
         */
        public function backToDashboard(array &$form, FormStateInterface $form_state){
                
                $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
                $tempstore->delete('bookchapter_tmpid');
                
                $url = Url::fromRoute('publisso_gold.dashboard', ['action' => 'todo']);
                $url->setAbsolute();
                
                $response = new AjaxResponse();
                $command  = new RedirectCommand($url->toString());
                $response->addCommand($command);
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed|string[]
         */
        public function autosave(array &$form, FormStateInterface $form_state){
                
                $workflow = $form_state->get('workflow');
                $te = $form_state->getTriggeringElement();
                $ui = $form_state->getUserInput();
                
                $simpleValues = [
                        'title',
                        'title_short',
                        'keywords',
                        'abstract',
                        'chapter_text',
                        'references',
                        'conflict_of_interest',
                        'conflict_of_interest_text',
                        'funding',
                        'funding_id',
                        'funding_name',
                        'accept_policy',
                        'reviewer_suggestion',
                        'add_files'
                ];
                
                if(in_array($te['#name'], $simpleValues)){
                        
                        $value = $ui[$te['#name']];
                        $workflow->setDataElement($te['#name'], $value);
                }
                else{
                        
                        if($te['#array_parents'][0] == 'fs_authors'){
                                
                                if($te['#array_parents'][2] == 'author-db'){
                                        $workflow->setDataElement('author_db', json_encode(array_filter($ui['fs_authors']['content']['author-db']['content'], ['self', 'filterAuthorsDB'])));
                                }
                                
                                if($te['#array_parents'][2] == 'author-add'){
                                        $authors = [];
                                        foreach($ui['fs_authors']['content']['author-add']['content'] as $_){
                                                array_push($authors, $_['content']);
                                        }
                                        $workflow->setDataElement('author_add', json_encode($authors));
                                }
                        }
                        
                        if($te['#array_parents'][0] == 'corporations'){
                                
                                $corporations = $workflow->getDataElement('corporation');
                                
                                if(!\Drupal::service('publisso_gold.tools')->isJSON($corporations)) $corporations = [];
                                else $corporations = json_decode($corporations, true);

                                $corporations[$te['#array_parents'][2]] = $te['#value'];
                                $workflow->setDataElement('corporation', json_encode($corporations));
                        }
                }
                $workflow->reload();
                $form_state->set('workflow', $workflow);
                $form_state->setRebuild();
                
                if($te['#array_parents'][0] == 'fs_authors') {
                        error_log("Return addAffil");
                        if ($te['#array_parents'][2] == 'author-db') {
                                $i = $te['#array_parents'][4];
                                return $form['fs_authors']['content']['author-db']['content'][$i]['wrapper-add_affiliation'];
                        }
                }
                
                return ['#markup' => ''];
        }
        
        /**
         * @param $var
         * @return bool
         */
        private function filterAuthorsDB($var){
                return !empty($var['author']);
        }
        
        private function getAuthorFromString(string $authorString) :? User {
                
                $author           = $authorString;
                [$author, $data] = preg_split('/;\s{1,}/', $author, 2);
                
                if(!empty($author)){
                        $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                        $found = 0;
                        $last_id = null;
                        $string = '';
                
                        foreach($res as $v){
                                $string .= ' || '.implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)])).' | '.implode( '; ', [$author, $data]);
                                if(
                                        implode( '; ', [$author, $data])
                                        ==
                                        implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)]))
                                ){
                                        $found++;
                                        $last_id = $v->up_uid;
                                }
                        }
                
                        if($found === 1){
                        
                                return new \Drupal\publisso_gold\Controller\User($last_id);
                        }
                }
                
                return null;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthorAddAffiliation(array &$form, FormStateInterface $form_state){
                $index = $form_state->getTriggeringElement()['#attributes']['index'];
                $counter = "cntAddAuthorAffiliaitions-$index";
                $form_state->set($counter, $form_state->get($counter) + 1);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorAddAffiliationCallback(array &$form, FormStateInterface $form_state){
                $index = $form_state->getTriggeringElement()['#attributes']['index'];
                return $form['fs_authors']['content']['author-add']['content']['author-add-'.$index]['content']['wrapper_add_affiliations'];
        }
}
