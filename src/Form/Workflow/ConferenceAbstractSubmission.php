<?php
        
        
        namespace Drupal\publisso_gold\Form\Workflow;
        
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Language\LanguageManager;
        use Drupal\Core\Link;
        use Drupal\Core\Url;
        use Drupal\file\Entity\File;
        use Drupal\publisso_gold\Classes\AuthorsContract;
        use Drupal\publisso_gold\Controller\Manager\AbstractManager;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\FunderManager;
        use Drupal\publisso_gold\Controller\Manager\SubmissionFundingManager;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Controller\Publisso as P;

        /**
         * Class ConferenceAbstractSubmission
         * @package Drupal\publisso_gold\Form\Workflow
         */
        class ConferenceAbstractSubmission extends FormBase {
        
                /**
                 * Returns a unique string identifying the form.
                 *
                 * The returned ID should be a unique string that can be a valid PHP function
                 * name, since it's used in hook implementation names such as
                 * hook_form_FORM_ID_alter().
                 *
                 * @return string
                 *   The unique string identifying the form.
                 */
                public function getFormId () {
                        return 'ConferenceAbstractSubmission';
                }
                
                /**
                 * Form constructor.
                 *
                 * @param array                                $form
                 *   An associative array containing the structure of the form.
                 * @param \Drupal\Core\Form\FormStateInterface $form_state
                 *   The current state of the form.
                 *
                 * @return array
                 *   The form structure.
                 */
                public function buildForm ( array $form, FormStateInterface $form_state, $args = [] ) {
                        
                        /**
                         * Loading the required parts for this item
                         */
                        if(!$form_state->has('wf_id')) $form_state->set('wf_id', $args['wf_id']);
                        
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $abstract = AbstractManager::getAbstract( $workflow->getDataElement( 'ca_id' ));
                        
                        if(!$abstract->exists()) return $form;
        
                        $conference = $abstract->getParentMedium();
                        $languages = [];
                        $allowedLanguages = $conference->getElement('allowed_submission_languages') ?? [$conference->getElement('language') => \Drupal::languageManager()->getLanguage($conference->getElement('language'))->getName()];

                        foreach(LanguageManager::getStandardLanguageList() as $langCode => $language){
                                if(in_array($langCode, $allowedLanguages))
                                        $languages[$langCode] = $language[0].' ('.$language[1].')';
                        }
                        
                        if(!count($languages)) $languages = [$conference->getElement('language') => (string)$this->t(\Drupal::languageManager()->getLanguage($conference->getElement('language'))->getName())];
                        
                        $fieldTranslations = $conference->getElement('field_translations') ?? [];
                        $titleTranslations = @unserialize( $workflow->getDataElement( 'title_translations' ) );
                        if(!$titleTranslations) $titleTranslations = [];
                        
                        if(array_key_exists('title', $fieldTranslations) && $fieldTranslations['title'] == 'mandatory' && !count($titleTranslations)){
                                $titleTranslations[] = [];
                                $workflow->setDataElement('title_translations', serialize($titleTranslations));
                                $workflow->reload();
                        }
        
                        $authorsContract = AuthorsContract::load($conference->getElement('authors_contract'));
                        $urlAuthorsContract = $authorsContract->getUrl(['attributes' => ['target' => '_blank']]);
                        $lnkAuthorsContract = Link::fromTextAndUrl((string) $this->t('author\'s contract'), $urlAuthorsContract)->toString();
                        
                        /**
                         * build the form
                         */
                        
                        $form['preamble'] = [
                                '#markup' => (string)$this->t('Please read the @lnkAuthorsContract. To complete the submission, you must agree to the author\'s contract.', ['@lnkAuthorsContract' => $lnkAuthorsContract]),
                                '#suffix' => '<br><br>'
                        ];
                        
                        $form['title'] = [
                                '#type'                 => 'textfield',
                                '#title'                => (string)$this->t('Title'),
                                '#required'             => true,
                                '#default_value'        => $workflow->getDataElement('title'),
                                '#ajax'                 => [
                                        'callback'              => '::autosave',
                                        'progress'              => [
                                                'type'                  => 'none'
                                        ],
                                        'prevent'               => 'blur',
                                        'disable-refocus'       => true
                                ]
                        ];
                        
                        if($conference->getElement('allow_multilingual_submissions') && $fieldTranslations['title'] != 'disabled') {
        
                                $form[ 'title-translation-container' ] = [
                                        '#type'        => 'fieldset',
                                        '#title'       => (string)$this->t('Title translations'),
                                        '#collapsible' => true,
                                        '#collapsed'   => true,
                                        '#tree'        => true,
                                        'translations' => [],
                                        '#prefix'      => '<div id="title-translation-wrapper">',
                                        '#suffix'      => '</div>',
                                        'actions'      => [
                                                '#type'   => 'actions',
                                                'add'     => [
                                                        '#weight'                  => 1,
                                                        '#type'                    => 'submit',
                                                        '#value'                   => (string)$this->t( 'Add title-translation' ),
                                                        '#submit'                  => [ '::addTitleTranslation' ],
                                                        '#ajax'                    => [
                                                                'wrapper'  => 'title-translation-wrapper',
                                                                'callback' => '::addTitleTranslationCallback',
                                                                'method'   => 'html'
                                                        ],
                                                        '#limit_validation_errors' => []
                                                ],
                                                '#suffix' => ''
                                        ],
                                        '#attributes' => [
                                                'style' => ['margin-bottom: 10px;']
                                        ]
                                ];
        
                                $titleTranslations = unserialize( $workflow->getDataElement( 'title_translations' ) );
                                
                                if ( !$titleTranslations ) $titleTranslations = [];
        
                                if ( $titleTranslations && count( $titleTranslations ) ) {
                                        
                                        if(($fieldTranslations['title'] == 'mandatory' && count($titleTranslations) > 1) || $fieldTranslations['title'] != 'mandatory')
                                        $form[ 'title-translation-container' ][ 'actions' ][ 'del' ] = [
                                                '#weight'                  => 0,
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)$this->t( 'Remove last title-translation' ),
                                                '#submit'                  => [ '::removeTitleTranslation' ],
                                                '#ajax'                    => [
                                                        'wrapper'  => 'title-translation-wrapper',
                                                        'callback' => '::removeTitleTranslationCallback',
                                                        'method'   => 'html'
                                                ],
                                                '#limit_validation_errors' => []
                                        ];
                
                                        foreach ( $titleTranslations as $i => $translation ) {
                                                $form[ 'title-translation-container' ][ 'translations' ][ $i ] = [
                                                        '#type'    => 'fieldset',
                                                        'language' => [
                                                                '#type'     => 'select',
                                                                '#title'    => (string)$this->t( 'Language' ),
                                                                '#options'  => $languages,
                                                                '#required' => true,
                                                                '#default_value' => $translation['language'] ?? null,
                                                                '#ajax'          => [
                                                                        'event'           => 'change',
                                                                        'progress'        => [
                                                                                'type' => 'none'
                                                                        ],
                                                                        'callback'        => '::autosave',
                                                                        'disable-refocus' => true
                                                                ],
                                                        ],
                                                        'title'    => [
                                                                '#type'     => 'textfield',
                                                                '#title'    => (string)$this->t( 'Title' ),
                                                                '#required' => true,
                                                                '#default_value' => $translation['title'] ?? null,
                                                                '#ajax'          => [
                                                                        'event'           => 'change',
                                                                        'progress'        => [
                                                                                'type' => 'none'
                                                                        ],
                                                                        'callback'        => '::autosave',
                                                                        'disable-refocus' => true
                                                                ],
                                                        ]
                                                ];
                                        }
                                }
                        }
                        
                        $submissionTypesAllowed = $conference->getElement('submission_types_allowed') ?? [];
                        $form['submission_type'] = [
                                '#type'                 => 'select',
                                '#title'                => (string)$this->t('Submission type'),
                                '#required'             => true,
                                '#options'              => array_combine($submissionTypesAllowed, $submissionTypesAllowed),
                                '#default_value'        => $workflow->getDataElement('submission_type'),
                                '#ajax'                 => [
                                        'callback'              => '::autosave',
                                        'progress'              => [
                                                'type'                  => 'none'
                                        ],
                                        'prevent'               => 'blur',
                                        'disable-refocus'       => true
                                ]
                        ];
                        
                        $categoriesAllowed = $conference->getElement('categories_allowed') ?? [];
                        $form['category'] = [
                                '#type'                 => 'select',
                                '#title'                => (string)$this->t('Category'),
                                '#required'             => true,
                                '#options'              => array_combine($categoriesAllowed, $categoriesAllowed),
                                '#multiple'             => true,
                                '#default_value'        => @unserialize($workflow->getDataElement('category')) ?? [],
                                '#ajax'                 => [
                                        'callback'              => '::autosave',
                                        'progress'              => [
                                                'type'                  => 'none'
                                        ],
                                        'prevent'               => 'blur',
                                        'disable-refocus'       => true
                                ]
                        ];
                        
                        $form['language'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('Language'),
                                '#options' => $languages,
                                '#default_value' => $workflow->getDataElement('language'),
                                '#required' => true,
                                '#ajax'                 => [
                                        'callback'              => '::autosave',
                                        'progress'              => [
                                                'type'                  => 'none'
                                        ],
                                        'prevent'               => 'blur',
                                        'disable-refocus'       => true
                                ]
                        ];
                        
                        $form['container-keywords'] = [
                                '#type'         => 'fieldset',
                                '#collapsible'  => true,
                                '#collapsed'    => true,
                                '#title'        => (string)$this->t('Keywords'),
                                '#prefix'       => '<div id="wrapper-keywords">',
                                '#suffix'       => '</div><br>',
                                'keywords'      => [],
                                'actions'       => [
                                        '#type'         => 'actions',
                                        '#weight'       => 80,
                                        'add'           => [
                                                '#weight'       => 1,
                                                '#type'         => 'submit',
                                                '#submit'       => ['::addKeyword'],
                                                '#value'        => (string)$this->t('Add keyword'),
                                                '#ajax'         => [
                                                        'callback'      => '::addKeywordCallback',
                                                        'progress'      => [
                                                                'type'          => 'none'
                                                        ],
                                                        'wrapper'       => 'wrapper-keywords',
                                                        'method'        => 'html'
                                                ],
                                                '#limit_validation_errors' => []
                                        ]
                                ]
                        ];
                        
                        if($workflow->hasDataElement('keywords')) {
                                foreach ( unserialize( $workflow->getDataElement( 'keywords' ) ) as $kwNr => $keyword ) {
                                        $form[ 'container-keywords' ][ 'keywords' ]["keyword#$kwNr"] = [
                                                '#type'          => 'textfield',
                                                '#default_value' => $keyword,
                                                '#ajax'                  => [
                                                        'callback'              => '::autosave',
                                                        'progress'              => [
                                                                'type'                  => 'none'
                                                        ],
                                                        'prevent'        => 'blur',
                                                        'disable-refocus' => true
                                                ]
                                        ];
                                }
                                
                                if(count($form[ 'container-keywords' ][ 'keywords' ])){
                                        $form['container-keywords']['actions']['delete'] = [
                                                '#weight'       => 0,
                                                '#type'         => 'submit',
                                                '#submit'       => ['::deleteKeyword'],
                                                '#value'        => (string)$this->t('Delete last keyword'),
                                                '#ajax'         => [
                                                        'callback'      => '::deleteKeywordCallback',
                                                        'progress'      => [
                                                                'type'          => 'none'
                                                        ],
                                                        'wrapper'       => 'wrapper-keywords',
                                                        'method'        => 'html'
                                                ],
                                                '#limit_validation_errors' => []
                                        ];
                                }
                        }
        
                        $keywordsTranslations = unserialize( $workflow->getDataElement( 'keywords_translations' ) );
                        if(!is_array($keywordsTranslations)) $keywordsTranslations = [];
                        if(array_key_exists('keywords', $fieldTranslations) && $fieldTranslations['keywords'] == 'mandatory' && !count($keywordsTranslations)){
                                $keywordsTranslations[] = [];
                                $workflow->setDataElement('keywords_translations', serialize($keywordsTranslations));
                                $workflow->reload();
                        }
        
                        if($conference->getElement('allow_multilingual_submissions') && $fieldTranslations['keywords'] != 'disabled') {
                
                                $form['container-keywords'][ 'keywords-translation-container' ] = [
                                        '#weight'      => 90,
                                        '#type'        => 'fieldset',
                                        '#title'       => (string)$this->t('Keywords translations'),
                                        '#collapsible' => true,
                                        '#collapsed'   => true,
                                        '#tree'        => true,
                                        'translations' => [],
                                        '#prefix'      => '<div id="keywords-translation-wrapper">',
                                        '#suffix'      => '</div>',
                                        'actions'      => [
                                                '#type'   => 'actions',
                                                'add'     => [
                                                        '#weight'                  => 1,
                                                        '#type'                    => 'submit',
                                                        '#value'                   => (string)$this->t( 'Add keywords-translation' ),
                                                        '#submit'                  => [ '::addKeywordsTranslation' ],
                                                        '#ajax'                    => [
                                                                'wrapper'  => 'keywords-translation-wrapper',
                                                                'callback' => '::addKeywordsTranslationCallback',
                                                                'method'   => 'html'
                                                        ],
                                                        '#limit_validation_errors' => []
                                                ],
                                                '#suffix' => ''
                                        ],
                                        '#attributes' => [
                                                'style' => ['margin-bottom: 10px;']
                                        ]
                                ];
                                
                                if ( $keywordsTranslations && count( $keywordsTranslations ) ) {
        
                                        if(($fieldTranslations['keywords'] == 'mandatory' && count($keywordsTranslations) > 1 ) || ($fieldTranslations['keywords'] != 'mandatory')) {
                                                $form[ 'container-keywords' ][ 'keywords-translation-container' ][ 'actions' ][ 'del' ] = [
                                                        '#weight'                  => 0,
                                                        '#type'                    => 'submit',
                                                        '#value'                   => (string)$this->t( 'Remove last keywords-translation' ),
                                                        '#submit'                  => [ '::removeKeywordsTranslation' ],
                                                        '#ajax'                    => [
                                                                'wrapper'  => 'keywords-translation-wrapper',
                                                                'callback' => '::removeKeywordsTranslationCallback',
                                                                'method'   => 'html'
                                                        ],
                                                        '#limit_validation_errors' => []
                                                ];
                                        }
        
                                        foreach ( $keywordsTranslations as $i => $translation ) {
                                                $form[ 'container-keywords' ][ 'keywords-translation-container' ][ 'translations' ][ $i ] = [
                                                        '#type'    => 'fieldset',
                                                        'language' => [
                                                                '#type'          => 'select',
                                                                '#title'         => (string)$this->t( 'Language' ),
                                                                '#options'       => $languages,
                                                                '#required'      => true,
                                                                '#default_value' => $translation[ 'language' ] ?? null,
                                                                '#ajax'          => [
                                                                        'event'           => 'change',
                                                                        'progress'        => [
                                                                                'type' => 'none'
                                                                        ],
                                                                        'callback'        => '::autosave',
                                                                        'disable-refocus' => true
                                                                ],
                                                                '#suffix'        => '<hr>'
                                                        ],
                                                        'keywords' => [],
                                                ];
        
                                                foreach ( unserialize( $workflow->getDataElement( 'keywords' ) ?? 'a:0:{}' ) as $kwNr => $keyword ) {
                                                        $form[ 'container-keywords' ][ 'keywords-translation-container' ][ 'translations' ][ $i ][ 'keywords' ][ "keyword#$kwNr" ] = [
                                                                '#type'          => 'textfield',
                                                                '#default_value' => $translation[ 'keywords' ][ $kwNr ] ?? null,
                                                                '#ajax'          => [
                                                                        'callback'        => '::autosave',
                                                                        'progress'        => [
                                                                                'type' => 'none'
                                                                        ],
                                                                        'prevent'         => 'blur',
                                                                        'disable-refocus' => true
                                                                ]
                                                        ];
                                                }
                                        }
                                        
                                }
                        }
                        
                        $form[ 'main_text' ] = [
                                '#type'          => 'textarea',
                                '#title'         => t( 'Text' ),
                                '#rows'          => 30,
                                '#attributes'    => [
                                        'class'    => [
                                                P::setup()->getValue( 'submission.text_editor' )
                                        ],
                                        'readonly' => 'readonly'
                                ],
                                '#ajax'          => [
                                        'event'           => 'change',
                                        'prevent'         => 'blur',
                                        'progress'        => [
                                                'type' => 'none'
                                        ],
                                        'callback'        => '::autosave',
                                        'disable-refocus' => true
                                ],
                                '#default_value' => $workflow->getDataElement( 'main_text' ),
                                '#states' => [
                                        'required' => [':input[name="submission_type"]' => ['!value' => 'Poster']]
                                ]
                        ];
        
                        if($conference->getElement('allow_multilingual_submissions') && $fieldTranslations['text'] != 'disabled') {
                
                                $form[ 'main_text-translation-container' ] = [
                                        '#type'        => 'fieldset',
                                        '#title'       => (string)$this->t('Text translations'),
                                        '#collapsible' => true,
                                        '#collapsed'   => true,
                                        '#tree'        => true,
                                        'translations' => [],
                                        '#prefix'      => '<div id="main_text-translation-wrapper">',
                                        '#suffix'      => '</div>',
                                        'actions'      => [
                                                '#type'   => 'actions',
                                                'add'     => [
                                                        '#weight'                  => 1,
                                                        '#type'                    => 'submit',
                                                        '#value'                   => (string)$this->t( 'Add text-translation' ),
                                                        '#submit'                  => [ '::addTextTranslation' ],
                                                        /*'#ajax'                    => [
                                                                'wrapper'  => 'main_text-translation-wrapper',
                                                                'callback' => '::addTextTranslationCallback',
                                                                'method'   => 'html'
                                                        ],*/
                                                        '#limit_validation_errors' => []
                                                ],
                                                '#suffix' => ''
                                        ],
                                        '#attributes' => [
                                                'style' => ['margin-bottom: 10px;']
                                        ]
                                ];
                
                                $textTranslations = unserialize( $workflow->getDataElement( 'text_translations' ) );
                
                                if ( !$textTranslations ) $textTranslations = [];
                
                                if ( $textTranslations && count( $textTranslations ) ) {
                        
                                        if(($fieldTranslations['text'] == 'mandatory' && count($textTranslations) > 1) || $fieldTranslations['text'] != 'mandatory')
                                                $form[ 'main_text-translation-container' ][ 'actions' ][ 'del' ] = [
                                                        '#weight'                  => 0,
                                                        '#type'                    => 'submit',
                                                        '#value'                   => (string)$this->t( 'Remove last text-translation' ),
                                                        '#submit'                  => [ '::removeTextTranslation' ],
                                                        /*'#ajax'                    => [
                                                                'wrapper'  => 'main_text-translation-wrapper',
                                                                'callback' => '::removeTextTranslationCallback',
                                                                'method'   => 'html'
                                                        ],*/
                                                        '#limit_validation_errors' => []
                                                ];
                        
                                        foreach ( $textTranslations as $i => $translation ) {
                                                $form[ 'main_text-translation-container' ][ 'translations' ][ $i ] = [
                                                        '#type'    => 'fieldset',
                                                        'language' => [
                                                                '#type'     => 'select',
                                                                '#title'    => (string)$this->t( 'Language' ),
                                                                '#options'  => $languages,
                                                                '#required' => true,
                                                                '#default_value' => $translation['language'] ?? null,
                                                                '#ajax'          => [
                                                                        'event'           => 'change',
                                                                        'progress'        => [
                                                                                'type' => 'none'
                                                                        ],
                                                                        'callback'        => '::autosave',
                                                                        'disable-refocus' => true
                                                                ],
                                                        ],
                                                        'main_text'    => [
                                                                '#type'          => 'textarea',
                                                                '#title'         => t( 'Text' ),
                                                                '#rows'          => 30,
                                                                '#attributes'    => [
                                                                        'class'    => [
                                                                                P::setup()->getValue( 'submission.text_editor' )
                                                                        ],
                                                                        'readonly' => 'readonly'
                                                                ],
                                                                '#ajax'          => [
                                                                        'event'           => 'change',
                                                                        'prevent'         => 'blur',
                                                                        'progress'        => [
                                                                                'type' => 'none'
                                                                        ],
                                                                        'callback'        => '::autosave',
                                                                        'disable-refocus' => true
                                                                ],
                                                                '#default_value' => $translation['main_text'] ?? null,
                                                                '#states' => [
                                                                        'required' => [':input[name="submission_type"]' => ['!value' => 'Poster']]
                                                                ]
                                                        ]
                                                ];
                                        }
                                }
                        }
                        
                        if($conference->getElement('references_allowed') && $conference->getElement('max_references') != 0) {
        
                                $form[ 'references' ] = [
                                        '#type'          => 'textarea',
                                        '#title'         => (string)t( 'References' ),
                                        '#rows'          => 10,
                                        '#cols'          => 60,
                                        '#prefix'        => '<br>',
                                        '#id'            => 'ta-rwReferences',
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $workflow->getDataElement( 'references' )
                                ];
                                
                                if($conference->getElement('max_references') > 0){
                                        $form['references']['#title'] .= " (max: ".$conference->getElement('max_references').")";
                                }
                        }
                        
                        $form['wrapper_authors'] = [
                                '#type'         => 'fieldset',
                                '#collapsible'  => true,
                                '#collapsed'    => true,
                                '#title'        => (string)$this->t('Authors & corporations'),
                                'content'       => [
                                        'db'            => [
                                                '#type'         => 'fieldset',
                                                '#collapsible'  => true,
                                                '#collapsed'    => true,
                                                '#title'        => (string)$this->t('Authors'),
                                                '#prefix'       => '<div id="wrapper-authorsDB">',
                                                '#suffix'       => '</div><br>',
                                                'authors'       => [
                                                        '#type'         => 'table',
                                                        '#empty'        => (string)$this->t('No authors have been specified yet...'),
                                                        '#header'       => [
                                                                ['data' => (string)$this->t('Author')],
                                                                ['data' => (string)$this->t('Weight/Pos.')],
                                                                ['data' => (string)$this->t('presenting')],
                                                                ['data' => (string)$this->t('corresponding')],
                                                        ],
                                                ],
                                                'actions' => [
                                                        '#type'         => 'actions',
                                                        'remove'        => [],
                                                        'add'           => [
                                                                '#type'         => 'submit',
                                                                '#value'        => (string)$this->t('Add author'),
                                                                '#submit'       => ['::addAuthorDB'],
                                                                '#ajax'         => [
                                                                        'callback'      => '::addAuthorDBCallback',
                                                                        'wrapper'       => 'wrapper-authorsDB',
                                                                        'method'        => 'html'
                                                                ],
                                                                '#limit_validation_errors' => []
                                                        ]
                                                ]
                                        ],
                                        'add'            => [
                                                '#type'         => 'fieldset',
                                                '#collapsible'  => true,
                                                '#collapsed'    => true,
                                                '#title'        => (string)$this->t('Additional authors'),
                                                '#prefix'       => '<div id="wrapper-authorsAdd">',
                                                '#suffix'       => '</div><br>',
                                                'authors'       => [
                                                        '#type'         => 'table',
                                                        '#empty'        => (string)$this->t('No additional authors have been specified yet...'),
                                                        '#header'       => [
                                                                ['data' => (string)$this->t('Firstname')],
                                                                ['data' => (string)$this->t('Lastname')],
                                                                ['data' => (string)$this->t('Affiliation')],
                                                                ['data' => (string)$this->t('Weight/Pos.')],
                                                                ['data' => (string)$this->t('presenting')],
                                                                ['data' => (string)$this->t('corresponding')],
                                                        ],
                                                ],
                                                'actions' => [
                                                        '#type'         => 'actions',
                                                        'remove'        => [],
                                                        'add'           => [
                                                                '#type'         => 'submit',
                                                                '#value'        => (string)$this->t('Add additional author'),
                                                                '#submit'       => ['::addAuthorAdd'],
                                                                '#ajax'         => [
                                                                        'callback'      => '::addAuthorAddCallback',
                                                                        'wrapper'       => 'wrapper-authorsAdd',
                                                                        'method'        => 'html'
                                                                ],
                                                                '#limit_validation_errors' => []
                                                        ]
                                                ]
                                        ],
                                        'corporations'     => [
                                                '#type'         => 'fieldset',
                                                '#collapsible'  => true,
                                                '#collapsed'    => true,
                                                '#title'        => (string)$this->t('Corporations'),
                                                '#prefix'       => '<div id="wrapper-corporations">',
                                                '#suffix'       => '</div><br>',
                                                'authors'       => [
                                                        '#type'         => 'table',
                                                        '#empty'        => (string)$this->t('No corporations have been specified yet...'),
                                                ],
                                                'actions' => [
                                                        '#type'         => 'actions',
                                                        'remove'        => [],
                                                        'add'           => [
                                                                '#type'         => 'submit',
                                                                '#value'        => (string)$this->t('Add corporation'),
                                                                '#submit'       => ['::addCorporation'],
                                                                '#ajax'         => [
                                                                        'callback'      => '::addCorporationCallback',
                                                                        'wrapper'       => 'wrapper-corporations',
                                                                        'method'        => 'html'
                                                                ],
                                                                '#limit_validation_errors' => []
                                                        ]
                                                ]
                                        ]
                                ]
                        ];
                        
                        $authorsDB = unserialize($workflow->getDataElement('authors_db')) ?? [];

                        if(is_array($authorsDB)) {
                                
                                $i = 0;
                                foreach ( $authorsDB as $author ) {
                
                                        $form[ 'wrapper_authors' ][ 'content' ][ 'db' ][ 'authors' ][ 'authorDB#' . $i ] = [
                                                'author'        => [
                                                        '#type'                          => 'textfield',
                                                        '#autocomplete_route_name'       => 'autocomplete.user.public.new',
                                                        '#autocomplete_route_parameters' => [],
                                                        '#required'                      => true,
                                                        '#ajax'                          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#default_value'                 => $author[ 'author' ] ?? ''
                                                ],
                                                'weight'        => [
                                                        '#type'          => 'number',
                                                        '#min'           => 0,
                                                        '#size'          => 3,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#default_value' => $author[ 'weight' ] ?? 0
                                                ],
                                                'presenting'    => [
                                                        '#type'          => 'checkbox',
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#default_value' => $author[ 'presenting' ] ?? 0
                                                ],
                                                'corresponding' => [
                                                        '#type'          => 'checkbox',
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#default_value' => $author[ 'corresponding' ] ?? 0
                                                ]
                                        ];
                
                                        $i++;
                                }
        
                                if ( count( $authorsDB ) ) {
                                        $form[ 'wrapper_authors' ][ 'content' ][ 'db' ][ 'actions' ][ 'remove' ] = [
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)$this->t( 'Delete last author' ),
                                                '#submit'                  => [ '::deleteAuthorDB' ],
                                                '#ajax'                    => [
                                                        'callback' => '::deleteAuthorDBCallback',
                                                        'wrapper'  => 'wrapper-authorsDB',
                                                        'method'   => 'html'
                                                ],
                                                '#limit_validation_errors' => []
                                        ];
                                }
                        }
        
                        $authorsAdd = unserialize($workflow->getDataElement('authors_add')) ?? [];
                        
                        if(is_array($authorsAdd)) {
                                
                                $i = 0;
                                foreach ( $authorsAdd as $author ) {
                
                                        $form[ 'wrapper_authors' ][ 'content' ][ 'add' ][ 'authors' ][ 'authorAdd#' . $i ] = [
                                                'firstname'        => [
                                                        '#type'          => 'textfield',
                                                        '#required'      => true,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#default_value' => $author[ 'firstname' ] ?? ''
                                                ],
                                                'lastname'        => [
                                                        '#type'          => 'textfield',
                                                        '#required'      => true,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#default_value' => $author[ 'lastname' ] ?? ''
                                                ],
                                                'affiliation'   => [
                                                        '#type'          => 'textfield',
                                                        '#required'      => true,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#default_value' => $author[ 'affiliation' ] ?? ''
                                                ],
                                                'weight'        => [
                                                        '#type'          => 'number',
                                                        '#min'           => 0,
                                                        '#size'          => 3,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#default_value' => $author[ 'weight' ] ?? 0
                                                ],
                                                'presenting'    => [
                                                        '#type'          => 'checkbox',
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#default_value' => $author[ 'presenting' ] ?? 0
                                                ],
                                                'corresponding' => [
                                                        '#type'          => 'checkbox',
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#default_value' => $author[ 'corresponding' ] ?? 0
                                                ]
                                        ];
                
                                        $i++;
                                }
        
        
                                if ( count( $authorsAdd ) ) {
                                        $form[ 'wrapper_authors' ][ 'content' ][ 'add' ][ 'actions' ][ 'remove' ] = [
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)$this->t( 'Delete last additional author' ),
                                                '#submit'                  => [ '::deleteAuthorAdd' ],
                                                '#ajax'                    => [
                                                        'callback' => '::deleteAuthorAddCallback',
                                                        'wrapper'  => 'wrapper-authorsAdd',
                                                        'method'   => 'html'
                                                ],
                                                '#limit_validation_errors' => []
                                        ];
                                }
                        }
        
                        $corporations = unserialize($workflow->getDataElement('corporations')) ?? [];
                        
                        if(is_array($corporations)) {
                                
                                $i = 0;
                                foreach ( $corporations as $corporation ) {
                
                                        $form[ 'wrapper_authors' ][ 'content' ][ 'corporations' ][ 'authors' ][ 'corporation#' . $i ] = [
                                                [
                                                        '#type'          => 'textfield',
                                                        '#required'      => true,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#default_value' => $corporation ?? ''
                                                ]
                                        ];
                
                                        $i++;
                                }
        
                                if ( count( $corporations ) ) {
                                        $form[ 'wrapper_authors' ][ 'content' ][ 'corporations' ][ 'actions' ][ 'remove' ] = [
                                                '#type'                    => 'submit',
                                                '#value'                   => (string)$this->t( 'Delete last corporation' ),
                                                '#submit'                  => [ '::deleteCorporation' ],
                                                '#ajax'                    => [
                                                        'callback' => '::deleteCorporationCallback',
                                                        'wrapper'  => 'wrapper-corporations',
                                                        'method'   => 'html'
                                                ],
                                                '#limit_validation_errors' => []
                                        ];
                                }
                        }
                        
                        $form['ctrlCoi'] = [
                                '#type'                 => 'checkbox',
                                '#title'                => (string)t('Conflict of interest'),
                                '#default_value'        => $workflow->getDataElement('ctrlCoi'),
                                '#ajax'          => [
                                        'event'           => 'change',
                                        'progress'        => [
                                                'type' => 'none'
                                        ],
                                        'callback'        => '::autosave',
                                        'disable-refocus' => true
                                ],
                        ];
                        
                        $form['conflict_of_interest'] = [
                                '#type'                 => 'textfield',
                                '#title'                => (string)t('Description of conflict of interest'),
                                '#default_value'        => $workflow->getDataElement('conflict_of_interest'),
                                '#states'               => [
                                        'visible'                => [':input[name="ctrlCoi"]' => ['checked' => true]],
                                        'required'               => [':input[name="ctrlCoi"]' => ['checked' => true]]
                                ],
                                '#ajax'          => [
                                        'event'           => 'change',
                                        'progress'        => [
                                                'type' => 'none'
                                        ],
                                        'callback'        => '::autosave',
                                        'disable-refocus' => true
                                ],
                        ];
        
                        $coiTranslations = @unserialize( $workflow->getDataElement( 'coi_translations' ) );
                        if(!$coiTranslations) $coiTranslations = [];
                        if(array_key_exists('coi', $fieldTranslations) && $fieldTranslations['coi'] == 'mandatory' && !count($coiTranslations)){
                                $coiTranslations[] = [];
                                $workflow->setDataElement('coi_translations', serialize($coiTranslations));
                                $workflow->reload();
                        }
        
                        if($conference->getElement('allow_multilingual_submissions') && $fieldTranslations['coi'] != 'disabled') {
                
                                $form[ 'coi-translation-container' ] = [
                                        '#type'        => 'fieldset',
                                        '#title'       => (string)$this->t('Conflict of interest translations'),
                                        '#collapsible' => true,
                                        '#collapsed'   => true,
                                        '#tree'        => true,
                                        'translations' => [],
                                        '#prefix'      => '<div id="coi-translation-wrapper">',
                                        '#suffix'      => '</div>',
                                        'actions'      => [
                                                '#type'   => 'actions',
                                                'add'     => [
                                                        '#weight'                  => 1,
                                                        '#type'                    => 'submit',
                                                        '#value'                   => (string)$this->t( 'Add conflict-of-interest-translation' ),
                                                        '#submit'                  => [ '::addCoiTranslation' ],
                                                        '#ajax'                    => [
                                                                'wrapper'  => 'coi-translation-wrapper',
                                                                'callback' => '::addCoiTranslationCallback',
                                                                'method'   => 'html'
                                                        ],
                                                        '#limit_validation_errors' => []
                                                ],
                                                '#suffix' => ''
                                        ],
                                        '#attributes' => [
                                                'style' => ['margin-bottom: 10px;']
                                        ],
                                        '#states' => [
                                                'visible' => [
                                                        ':input[name="ctrlCoi"]' => ['checked' => true]
                                                ]
                                        ]
                                ];
                
                                $coiTranslations = unserialize( $workflow->getDataElement( 'coi_translations' ) );
                
                                if ( !$coiTranslations ) $coiTranslations = [];
                
                                if ( $coiTranslations && count( $coiTranslations ) ) {
                        
                                        if(($fieldTranslations['coi'] == 'mandatory' && count($coiTranslations) > 1) || $fieldTranslations['coi'] != 'mandatory')
                                                $form[ 'coi-translation-container' ][ 'actions' ][ 'del' ] = [
                                                        '#weight'                  => 0,
                                                        '#type'                    => 'submit',
                                                        '#value'                   => (string)$this->t( 'Remove last conflict-of-interest-translation' ),
                                                        '#submit'                  => [ '::removeCoiTranslation' ],
                                                        '#ajax'                    => [
                                                                'wrapper'  => 'coi-translation-wrapper',
                                                                'callback' => '::removeCoiTranslationCallback',
                                                                'method'   => 'html'
                                                        ],
                                                        '#limit_validation_errors' => []
                                                ];
                        
                                        foreach ( $coiTranslations as $i => $translation ) {
                                                $form[ 'coi-translation-container' ][ 'translations' ][ $i ] = [
                                                        '#type'    => 'fieldset',
                                                        'language' => [
                                                                '#type'     => 'select',
                                                                '#title'    => (string)$this->t( 'Language' ),
                                                                '#options'  => $languages,
                                                                '#required' => true,
                                                                '#default_value' => $translation['language'] ?? null,
                                                                '#ajax'          => [
                                                                        'event'           => 'change',
                                                                        'progress'        => [
                                                                                'type' => 'none'
                                                                        ],
                                                                        'callback'        => '::autosave',
                                                                        'disable-refocus' => true
                                                                ],
                                                        ],
                                                        'conflict_of_interest'    => [
                                                                '#type'     => 'textfield',
                                                                '#title'    => (string)$this->t( 'Conflict of interest' ),
                                                                '#required' => true,
                                                                '#default_value' => $translation['conflict_of_interest'] ?? null,
                                                                '#ajax'          => [
                                                                        'event'           => 'change',
                                                                        'progress'        => [
                                                                                'type' => 'none'
                                                                        ],
                                                                        'callback'        => '::autosave',
                                                                        'disable-refocus' => true
                                                                ],
                                                        ]
                                                ];
                                        }
                                }
                        }
                        
                        $form['ctrlFunding'] = [
                                '#type'                 => 'checkbox',
                                '#title'                => (string)$this->t('Funds received'),
                                '#default_value'        => $workflow->getDataElement('ctrlFunding'),
                                '#ajax'          => [
                                        'event'           => 'change',
                                        'progress'        => [
                                                'type' => 'none'
                                        ],
                                        'callback'        => '::autosave',
                                        'disable-refocus' => true
                                ],
                        ];
                        
                        $form['wrapper-funding'] = [
                                '#type' => 'fieldset',
                                '#collapsible' => true,
                                '#collapsed' => true,
                                '#title' => (string)$this->t('Funding information'),
                                '#prefix' => '<div id="wrapper-funding">',
                                '#suffix' => '</div>',
                                'actions' => [
                                        '#type' => 'actions',
                                        'del' => [],
                                        'add' => [
                                                '#type' => 'submit',
                                                '#value' => (string)$this->t('Add funding'),
                                                '#button_type' => 'success',
                                                '#limit_validation_errors' => [],
                                                '#attributes' => [
                                                        'class' => [
                                                                'btn',
                                                                'btn-success'
                                                        ]
                                                ],
                                                '#submit' => ['::addFunding'],
                                                '#ajax' => [
                                                        'callback' => '::addFundingCallback',
                                                        'wrapper' => 'wrapper-funding',
                                                        'method' => 'html'
                                                ]
                                        ]
                                ],
                                'fundings' => [],
                                '#states' => [
                                        'visible' => [
                                                ':input[name="ctrlFunding"]' => ['checked' => true]
                                        ]
                                ]
                        ];
                        
                        $fundings = @unserialize($workflow->getDataElement('funding')) ?? [];
                        
                        if(is_array($fundings) && count($fundings)){
                                
                                foreach($fundings as $i => $funding) {
                                        
                                        $form[ 'wrapper-funding' ]['fundings']['funding#'.$i] = [
                                                '#type' => 'fieldset',
                                                '#tree' => true,
                                                'funding_type' => [
                                                        '#type' => 'select',
                                                        '#title' => (string)$this->t('Funding type'),
                                                        '#options' => [
                                                                'manual' => (string)$this->t('Manual entry'),
                                                                'stream' => (string)$this->t('By funding-stream')
                                                        ],
                                                        '#default_value' => $funding['funding_type'] ?? null,
                                                        '#required' => true,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                ],
                                                'funder_name' => [
                                                        '#type'          => 'textfield',
                                                        '#title'         => (string)$this->t('Funding name'),
                                                        '#default_value' => $funding['funder_name'] ?? null,
                                                        '#states' => [
                                                                'visible'  => [':input[name="funding#'.$i.'[funding_type]"]' => ['value' => 'manual']]
                                                        ],
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                ],
        
                                                'funder_id' => [
                                                        '#type'          => 'textfield',
                                                        '#title'         => (string)$this->t('Funding ID'),
                                                        '#default_value' => $funding['funder_id'] ?? null,
                                                        '#states' => [
                                                                'visible'  => [':input[name="funding#'.$i.'[funding_type]"]' => ['value' => 'manual']]
                                                        ],
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                ],
        
                                                'funder' => [
                                                        '#type' => 'select',
                                                        '#title' => (string)$this->t('Funds received from one of the following funders'),
                                                        '#required' => false,
                                                        '#options' => FunderManager::getNormalizedAndIndexedList(),
                                                        '#default_value' => $funding['funder'] ?? null,
                                                        '#states' => [
                                                                'visible'  => [':input[name="funding#'.$i.'[funding_type]"]' => ['value' => 'stream']]
                                                        ],
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                ],
        
                                                'funder_award_number' => [
                                                        '#type' => 'textfield',
                                                        '#title' => (string)$this->t('ProjectID or Award number'),
                                                        '#default_value' => $funding['funder_award_number'] ?? null,
                                                        '#states' => [
                                                                'visible'  => [':input[name="funding#'.$i.'[funding_type]"]' => ['value' => 'stream']]
                                                        ],
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                ],
                                                'funder_award_uri' => [
                                                        '#type' => 'url',
                                                        '#title' => (string)$this->t('Award URI'),
                                                        '#default_value' => $funding['funder_award_uri'] ?? null,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#states' => [
                                                                'visible'  => [':input[name="funding#'.$i.'[funding_type]"]' => ['value' => 'stream']]
                                                        ],
                                                ],
                                                'funder_identifier' => [
                                                        '#type' => 'url',
                                                        '#title' => (string)$this->t('Funder identifier'),
                                                        '#default_value' => $funding['funder_identifier'] ?? null,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#states' => [
                                                                'visible'  => [':input[name="funding#'.$i.'[funding_type]"]' => ['value' => 'stream']]
                                                        ],
                                                ],
                                                'funder_funding_stream' => [
                                                        '#type' => 'textfield',
                                                        '#title' => (string)$this->t('Funding program'),
                                                        '#default_value' => $funding['funder_funding_stream'] ?? null,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#states' => [
                                                                'visible'  => [':input[name="funding#'.$i.'[funding_type]"]' => ['value' => 'stream']]
                                                        ],
                                                ],
                                                'funder_award_title' => [
                                                        '#type' => 'textfield',
                                                        '#title' => (string)$this->t('Project name'),
                                                        '#default_value' => $funding['funder_award_title'] ?? null,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                        '#states' => [
                                                                'visible'  => [':input[name="funding#'.$i.'[funding_type]"]' => ['value' => 'stream']]
                                                        ],
                                                ],
                                        ];
                                }
                                
                                $form['wrapper-funding']['actions']['del'] = [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Delete last funding'),
                                        '#button_type' => 'danger',
                                        '#limit_validation_errors' => [],
                                        '#attributes' => [
                                                'class' => [
                                                        'btn',
                                                        'btn-danger'
                                                ]
                                        ],
                                        '#submit' => ['::delFunding'],
                                        '#ajax' => [
                                                'callback' => '::delFundingCallback',
                                                'wrapper' => 'wrapper-funding',
                                                'method' => 'html'
                                        ]
                                ];
                        }
                        
                        if($conference->getElement('award')){
                                
                                $form['award'] = [
                                        '#type' => 'radios',
                                        '#title' => (string)$this->t('I would like my submission to be considered for the award issued by the host of the conference: <br>@title', ['@title' => $conference->getElement('award_title')]),
                                        '#options' => [
                                                '0' => (string)$this->t('No'),
                                                '1' => (string)$this->t('Yes')
                                        ],
                                        '#prefix' => '<br>',
                                        '#default_value' => $workflow->getDataElement('award') ?? 0,
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ]
                                ];
                                
                                if($conference->getElement('award_info')){
                                        
                                        $form['award_info'] = [
                                                '#type' => 'textfield',
                                                '#title' => (string)$this->t($conference->getElement('award_info')),
                                                '#states' => [
                                                        'visible' => [
                                                                ':input[name="award"]' => ['value' => 1]
                                                        ]
                                                ],
                                                '#default_value' => $workflow->getDataElement('award_info'),
                                                '#ajax'          => [
                                                        'event'           => 'change',
                                                        'progress'        => [
                                                                'type' => 'none'
                                                        ],
                                                        'callback'        => '::autosave',
                                                        'disable-refocus' => true
                                                ]
                                        ];
                                }
                        }
                        
                        $form['accept_authors_contract'] = [
                                '#type' => 'checkbox',
                                '#title' => (string)t('I accept the @link</a>', ['@link' => $lnkAuthorsContract]),
                                '#required' => true
                        ];
                        
                        $form['wrapper-attachments'] = [
                                '#title' => (string)$this->t('Attachments'),
                                '#type' => 'fieldset',
                                '#collapsible' => true,
                                '#collapsed' => false,
                                'attachments' => [
                                        '#type' => 'managed_file',
                                        '#title' => (string)$this->t('Attachments'),
                                        '#title_display' => 'invisible',
                                        '#multiple' => true,
                                        '#default_value' => unserialize($workflow->getDataElement('attachments')) ?? [],
                                        '#upload_location' => 'public://publisso_gold/conferences/abstracts/attachments/'
                                ],
                                '#default_value' => unserialize($workflow->getDataElement('attachments')) ?? []
                        ];
                        
                        $form['actions'] = ['#type' => 'actions', '#suffix' => '<br>'];
                        
                        $form['actions']['preview'] = [
                                '#type' => 'link',
                                '#title' => (string)$this->t('Preview'),
                                '#url' => Url::fromRoute('publisso_gold.conferences_conference.abstract', ['cf_id' => $conference->getId(), 'ca_id' => $abstract->getId()], ['attributes' => ['target' => '_blank']]),
                                '#attributes' => [
                                        'class' => [
                                                'btn',
                                                'btn-primary'
                                        ]
                                ]
                        ];
                        
                        $form['actions']['submit'] = [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Submit'),
                                '#submit' => ['::submitForm'],
                                '#button_type' => 'success',
                                '#attributes' => [
                                        'class' => [
                                                'btn',
                                                'btn-success'
                                        ]
                                ]
                        ];
                        
                        $form['cache'] = [
                                '#max-age' => 0
                        ];
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array|void
                 */
                public function validateForm (array &$form, FormStateInterface $form_state ) {
                        
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $conference = ConferenceManager::getConference($workflow->getDataElement('cf_id'));
                        
                        //check the References
                        if($conference->getElement('max_references') > -1){
                                $numReferences = count(explode("\n", $form_state->getValue('references')));
                                if($numReferences > $conference->getElement('max_references')){
                                        $form_state->setErrorByName('references', (string)$this->t('The number of references exceeds the permitted amount!'));
                                }
                        }
                        
                        return $form;
                }
        
                /**
                 * Form submission handler.
                 *
                 * @param array                                $form
                 *   An associative array containing the structure of the form.
                 * @param \Drupal\Core\Form\FormStateInterface $form_state
                 *   The current state of the form.
                 *
                 * @return array
                 * @throws \Drupal\Core\Entity\EntityStorageException
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        $this->storeData2Item($form, $form_state);
                        if($this->storeData2Abstract($form_state)) {
                                \Drupal::messenger()->addMessage( (string)$this->t( 'Abstract successfully saved!' ) );
                                $workflow = WorkflowManager::getItem( $form_state->get( 'wf_id' ) );
                                $conference = ConferenceManager::getConference($workflow->getDataElement('cf_id'));
                                $follower = $workflow->getSchemaDefaultFollower( 'schema_follower' );
                                if(($workflow->getSchemaEntryPointIdentifier() == $workflow->getSchemaCurrentIdentifier() || !$conference->submissionPossible())) $workflow->newSchemaState( $follower );
                                $form_state->setRedirect( 'publisso_gold.dashboard' );
                        }
                        return $form;
                }
        
                /**
                 * @param FormStateInterface $form_state
                 *
                 * @return bool
                 */
                public function storeData2Abstract ( FormStateInterface $form_state ) :bool{
                        
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $workflow->setDataElement('submitted', date("Y-m-d H:i:s"));
                        $abstract = AbstractManager::getAbstract($workflow->getDataElement('ca_id'));
                        $conference = ConferenceManager::getConference($workflow->getDataElement('cf_id'));
                        
                        if($abstract){
                                
                                foreach($workflow->getDataKeys() as $key){
                                        if($abstract->hasElement($key)){
                                                $abstract->setElement($key, $workflow->getDataElement($key));
                                        }
                                }
                                
                                //authors_db - split name
                                if(false === ($authors = @unserialize($workflow->getDataElement('authors_db')))) $authors = [];
                                
                                foreach($authors as $i => $author){
                                        $name = explode(';', $author['author'], 2);
                                        [$lastname, $firstname] = explode(',', $name[0]);
                                        $affiliation = $name[1] ?? '';
                                        $authors[$i] = array_merge($author, ['firstname' => trim($firstname), 'lastname' => trim($lastname), 'affiliation' => trim($affiliation)]);
                                }
                                $abstract->setElement('authors_db', serialize($authors));
                                
                                //authors contract
                                $abstract->setElement('authors_contract', $conference->getElement('authors_contract'));
                                
                                //translations
                                $translationMapping = [
                                        'title' => 'title',
                                        'keywords' => 'keywords',
                                        'text' => 'main_text',
                                        'coi' => 'conflict_of_interest'
                                ];
                                
                                foreach($translationMapping as $identifierPrefix => $field){
                                        
                                        if($workflow->hasDataElement($identifierPrefix.'_translations')){
                
                                                $translations = @unserialize($workflow->getDataElement($identifierPrefix.'_translations'));
                                                if(!$translations) $translations = [];
                                                
                                                if(!count($translations)){
                                                        $abstract->deleteElementTranslations($field);
                                                }
                                                else {
                                                        foreach ( $translations as $translation ) {
                                                                $abstract->setElementTranslation( $field, is_scalar( $translation[ $field ] ) ? $translation[ $field ] : serialize( $translation[ $field ] ), $translation[ 'language' ] );
                                                        }
                                                }
                                        }
                                        else{
                                                $abstract->deleteElementTranslations($field);
                                        }
                                }
                                
                                //fundings
                                if($workflow->hasDataElement('ctrlFunding') && !!$workflow->getDataElement('ctrlFunding') === true){
                                        SubmissionFundingManager::delete($abstract->getId(), 'abstract');
                                        $fundings = @unserialize($workflow->getDataElement('funding'));
                                        
                                        if(!$fundings) $fundings = [];
                                        $number = 1;
                                        foreach($fundings as $f){
                                                
                                                $funding = SubmissionFundingManager::create($abstract->getId(), 'abstract', $number++);
                                                
                                                if($funding){
                                                        
                                                        switch($f['funding_type']){
                                                                case 'manual':
                                                                        $funding->setElement('funder_name', $f['funder_name'] ?? null);
                                                                        $funding->setElement('funder_id', $f['funder_id'] ?? null);
                                                                        break;
                                                                        
                                                                case 'stream':
                                                                        $funding->setElement('funder'                   , $f['funder']                  ?? null);
                                                                        $funding->setElement('funder_award_number'      , $f['funder_award_number']     ?? null);
                                                                        $funding->setElement('funder_award_uri'         , $f['funder_award_uri']        ?? null);
                                                                        $funding->setElement('funder_award_identifier'  , $f['funder_identifier']       ?? null);
                                                                        $funding->setElement('funder_funding_stream'    , $f['funder_funding_stream']   ?? null);
                                                                        $funding->setElement('funder_award_title'       , $f['funder_award_title']      ?? null);
                                                                        break;
                                                        }
                                                        
                                                        $funding->setElement('type', $f['funding_type']);
                                                        $funding->save();
                                                }
                                        }
                                }
                                return $abstract->save();
                        }
                        
                        return false;
                }
                
                /**
                 * @param array              $form
                 *   An associative array containing the structure of the form.
                 * @param FormStateInterface $form_state
                 *   The current state of the form
                 *
                 * @return array
                 * @throws \Drupal\Core\Entity\EntityStorageException
                 */
                public function storeData2Item(array &$form, FormStateInterface $form_state){
                        
                        $workflow               = WorkflowManager::getItem($form_state->get('wf_id'));
                        $conference             = ConferenceManager::getConference($workflow->getDataElement('cf_id'));
                        $values                 = $form_state->getValues();
                        $fieldTranslations      = $conference->getElement('field_translations') ?? [];
                        
                        //title & translations
                        $workflow->setDataElement('title', $values['title'], false);
                        if($conference->getElement('allow_multilingual_submissions') && $fieldTranslations['title'] != 'disabled') {
                                $titleTranslations = [];
                                foreach($values['title-translation-container']['translations'] ?? [] as $titleTranslation){
                                        $titleTranslations[] = $titleTranslation;
                                }
                                $workflow->setDataElement('title_translations', serialize($titleTranslations), false);
                        }
                        else{
                                $workflow->setDataElement('title_translations', null, false);
                        }
                        
                        //submission_type
                        $key = 'submission_type';
                        if(array_key_exists($key, $values)) $workflow->setDataElement($key, $values[$key], false); else $workflow->setDataElement($key, null);
                        
                        //category
                        $key = 'category';
                        if(array_key_exists($key, $values)) $workflow->setDataElement($key, serialize(array_keys($values[$key])), false); else $workflow->setDataElement($key, null);
                        
                        //language
                        $key = 'language';
                        if(array_key_exists($key, $values)) $workflow->setDataElement($key, $values[$key], false); else $workflow->setDataElement($key, $conference->getElement($key), false);
                        
                        //keywords
                        $keywordsKeys = array_reduce(array_keys($values), function($match, $string){
                                if(preg_match('/^keyword#(\d+)/', $string, $matches)){
                                        $match[$matches[1]] = $matches[0];
                                }
                                return $match;
                        }, []);
                        
                        $keywords = [];
                        foreach($keywordsKeys as $k => $v){
                                if(!empty($values[$v])) $keywords[$k] = $values[$v];
                        }
                        $workflow->setDataElement('keywords', serialize($keywords), false);
        
                        if($conference->getElement('allow_multilingual_submissions') && $fieldTranslations['keywords'] != 'disabled') {
                                $keywordsTranslations = [];
                                foreach($values['keywords-translation-container']['translations'] ?? [] as $keywordsTranslation){
                                        $keywordsTranslations[] = $keywordsTranslation;
                                }
                                $workflow->setDataElement('keywords_translations', serialize($keywordsTranslations), false);
                        }
                        else{
                                $workflow->setDataElement('keywords_translations', null, false);
                        }
        
                        //main_text
                        $key = 'main_text';
                        if(array_key_exists($key, $values)) $workflow->setDataElement($key, $values[$key], false); else $workflow->setDataElement($key, $conference->getElement($key), false);
        
                        if($conference->getElement('allow_multilingual_submissions') && $fieldTranslations['text'] != 'disabled') {
                                $textTranslations = [];
                                foreach($values['main_text-translation-container']['translations'] ?? [] as $textTranslation){
                                        $textTranslations[] = $textTranslation;
                                }
                                $workflow->setDataElement('text_translations', serialize($textTranslations), false);
                        }
                        else{
                                $workflow->setDataElement('text_translations', null, false);
                        }
        
                        //references
                        $key = 'references';
                        if(array_key_exists($key, $values)) $workflow->setDataElement($key, $values[$key], false); else $workflow->setDataElement($key, $conference->getElement($key), false);
        
                        //authors
                        $authorDB = $authorAdd = $corporation = [];
                        foreach($values['authors'] ?? [] as $k => $v){
                                
                                if(preg_match('/^(authorDB|authorAdd|corporation)#(\d+?)$/', $k, $matches)){
                                        ${$matches[1]}[$matches[2]] = count($v) == 1 ? $v[0] : $v;
                                }
                        }
                        $workflow->setDataElement('authors_db', serialize($authorDB), false);
                        $workflow->setDataElement('authors_add', serialize($authorAdd), false);
                        $workflow->setDataElement('corporations', serialize($corporation), false);
                        
                        //conflict of interest
                        if(!$values['ctrlCoi']){
                                $workflow->setDataElement('ctrlCoi'             , 0, false);
                                $workflow->setDataElement('conflict_of_interest', null, false);
                                $workflow->setDataElement('coi_translations'    , null, false);
                        }
                        else{
                                $workflow->setDataElement('ctrlCoi'             , 1, false);
                                $workflow->setDataElement('conflict_of_interest', $values['conflict_of_interest'], false);
        
                                if($conference->getElement('allow_multilingual_submissions') && $fieldTranslations['coi'] != 'disabled') {
                                        $coiTranslations = [];
                                        foreach($values['coi-translation-container']['translations'] ?? [] as $coiTranslation){
                                                $coiTranslations[] = $coiTranslation;
                                        }
                                        $workflow->setDataElement('coi_translations', serialize($coiTranslations), false);
                                }
                                else{
                                        $workflow->setDataElement('coi_translations', null, false);
                                }
                        }
                        
                        //funding
                        if(!$values['ctrlFunding']){
                                $workflow->setDataElement('funding', null, false);
                        }
                        else{
                                $fundingKeys = array_reduce(array_keys($values), function($match, $string){
                                        if(preg_match('/^funding#(\d+)/', $string, $matches)){
                                                $match[$matches[1]] = $matches[0];
                                        }
                                        return $match;
                                }, []);
        
                                $fundings = [];
                                foreach($fundingKeys as $k => $v){
                                        if(!empty($values[$v])) $fundings[$k] = $values[$v];
                                }
                                
                                $workflow->setDataElement('funding', serialize($fundings), false);
                        }
                        
                        //award
                        if(!$values['award'] ?? false){
                                $workflow->setDataElement('award', 0, false);
                        }
                        else{
                                $workflow->setDataElement('award', 1, false);
                                $workflow->setDataElement('award_info', $values['award_info'] ?? null, false);
                        }
                        
                        //authors contract
                        $key = 'accept_authors_contract';
                        if(array_key_exists($key, $values))$workflow->setDataElement($key, $values[$key], false); else $workflow->setDataElement($key, $conference->getElement($key), false);
                        
                        //attachments
                        $key = 'attachments';
                        $attachmentsExists = @unserialize($workflow->getDataElement('attachments'));
                        if(!$attachmentsExists) $attachmentsExists = [];
                        
                        if(array_key_exists($key, $values)){
                                
                                $attachments = [];
                                $attachmentsNew = array_diff($values['attachments'], $attachmentsExists);
                                
                                foreach($attachmentsNew as $fid){
                                        $file = File::load($fid);
                                        $file->setPermanent();
                                        $file->save();
                                        $attachments[] = $fid;
                                }
                                
                                $attachmentsOld = array_diff($attachmentsExists, $values['attachments']);
                                $deleted = [];
                                foreach($attachmentsOld ?? [] as $fid){
                                        if(null !== ($file = File::load($fid))) {
                                                $file->delete();
                                                $deleted[] = $fid;
                                        }
                                }
                                
                                $workflow->setDataElement('attachments', serialize($values['attachments']), false);
                        }
                        
                        $workflow->save();
                        return $form;
                }
                
                /**
                 * Form autosave handler.
                 *
                 * @param array                                $form
                 *   An associative array containing the structure of the form.
                 * @param \Drupal\Core\Form\FormStateInterface $form_state
                 *   The current state of the form.
                 *
                 * @return array
                 */
                public function autosave ( array &$form, FormStateInterface $form_state ) {
                        $te = $form_state->getTriggeringElement();
                        $key = $te['#name'];

                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        P::log('Autosave for key: '.$key);
                        P::log(print_r($form_state->getUserInput(), 1));
                        if(preg_match('/^keyword#(\d+)/', $key, $matches)){
                                $keywords = unserialize($workflow->getDataElement('keywords')) ?? [];
                                $keywords[$matches[1]] = $form_state->getUserInput()[ $key ];
                                $workflow->setDataElement('keywords', serialize($keywords));
                        }
                        elseif(preg_match('/^authors\[authorDB#(\d+?)\]\[(.+?)\]$/', $key, $matches)){
                                $id     = $matches[1];
                                $field  = $matches[2];
                                $authors = unserialize($workflow->getDataElement('authors_db')) ?? [];
                                $authors[$id][$field] = $form_state->getUserInput()['authors']['authorDB#'.$id][$field];
                                $workflow->setDataElement('authors_db', serialize($authors));
                        }
                        elseif(preg_match('/^authors\[authorAdd#(\d+?)\]\[(.+?)\]$/', $key, $matches)){
                                $id     = $matches[1];
                                $field  = $matches[2];
                                $authors = unserialize($workflow->getDataElement('authors_add')) ?? [];
                                $authors[$id][$field] = $form_state->getUserInput()['authors']['authorAdd#'.$id][$field];
                                $workflow->setDataElement('authors_add', serialize($authors));
                        }
                        elseif(preg_match('/^authors\[corporation#(\d+?)\]\[0\]$/', $key, $matches)){
                                $id     = $matches[1];
                                $authors = unserialize($workflow->getDataElement('corporations')) ?? [];
                                $authors[$id] = $form_state->getUserInput()['authors']['corporation#'.$id][0];
                                $workflow->setDataElement('corporations', serialize($authors));
                        }
                        elseif(preg_match('/^title-translation-container\[translations\]\[(\d+?)\]\[(.+?)\]$/', $key, $matches)){
                                $id     = $matches[1];
                                $field  = $matches[2];
                                $translations = unserialize($workflow->getDataElement('title_translations')) ?? [];
                                $translations[$id][$field] = $form_state->getUserInput()['title-translation-container']['translations'][$id][$field];
                                $workflow->setDataElement('title_translations', serialize($translations));
                        }
                        elseif(preg_match('/^coi-translation-container\[translations\]\[(\d+?)\]\[(.+?)\]$/', $key, $matches)){
                                $id     = $matches[1];
                                $field  = $matches[2];
                                $translations = unserialize($workflow->getDataElement('coi_translations')) ?? [];
                                $translations[$id][$field] = $form_state->getUserInput()['coi-translation-container']['translations'][$id][$field];
                                $workflow->setDataElement('coi_translations', serialize($translations));
                        }
                        elseif(preg_match('/^main_text-translation-container\[translations\]\[(\d+?)\]\[(.+?)\]$/', $key, $matches)){
                                $id     = $matches[1];
                                $field  = $matches[2];
                                $translations = unserialize($workflow->getDataElement('text_translations')) ?? [];
                                $translations[$id][$field] = $form_state->getUserInput()['main_text-translation-container']['translations'][$id][$field];
                                $workflow->setDataElement('text_translations', serialize($translations));
                        }
                        elseif(preg_match('/^keywords-translation-container\[translations\]\[(\d+?)\]\[(.+?)\](\[keyword#(\d+?)\]){0,1}$/', $key, $matches)){
                                $id     = $matches[1];
                                $field  = $matches[2];
                                $translations = unserialize($workflow->getDataElement('keywords_translations')) ?? [];
                                
                                if($field == 'language')
                                        $translations[$id][$field] = $form_state->getUserInput()['keywords-translation-container']['translations'][$id][$field];
                                else{
                                        $kwNr = $matches[4];
                                        $translations[$id][$field][$kwNr] = $form_state->getUserInput()['keywords-translation-container']['translations'][$id]['keywords']["keyword#$kwNr"];
                                }
                                P::log(print_r($translations, 1));
                                $workflow->setDataElement('keywords_translations', serialize($translations));
                        }
                        elseif(preg_match('/^funding#(\d+?)\[(.+?)\]$/', $key, $matches)){
                                $id     = $matches[1];
                                $field  = $matches[2];
                                $fundings = unserialize($workflow->getDataElement('funding')) ?? [];
                                $fundings[$id][$field] = $form_state->getUserInput()['funding#'.$id][$field];
                                $workflow->setDataElement('funding', serialize($fundings));
                        }
                        else {
                                $workflow->setDataElement( $key, is_scalar($form_state->getUserInput()[ $key ]) ? $form_state->getUserInput()[ $key ] : serialize($form_state->getUserInput()[ $key ]) );
                        }
                        $form_state->setRebuild();
                        return ['#markup' => ''];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function delFunding(array &$form, FormStateInterface $form_state){
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $fundings = unserialize($workflow->getDataElement('funding')) ?? [];
                        array_pop($fundings);
                        $workflow->setDataElement('funding', serialize($fundings));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function delFundingCallback(array &$form, FormStateInterface $form_state){
                        $form['wrapper-funding']['#collapsed'] = false;
                        return $form['wrapper-funding'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addFunding(array &$form, FormStateInterface $form_state){
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $fundings = unserialize($workflow->getDataElement('funding')) ?? [];
                        $fundings[] = [];
                        $workflow->setDataElement('funding', serialize($fundings));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addFundingCallback(array &$form, FormStateInterface $form_state){
                        $form['wrapper-funding']['#collapsed'] = false;
                        return $form['wrapper-funding'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addKeywordsTranslation(array &$form, FormStateInterface $form_state){
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $translations = unserialize($workflow->getDataElement('keywords_translations')) ?? [];
                        $translations[] = [];
                        $workflow->setDataElement('keywords_translations', serialize($translations));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addKeywordsTranslationCallback(array &$form, FormStateInterface $form_state){
                        $form['container-keywords']['keywords-translation-container']['#collapsed'] = false;
                        return $form['container-keywords']['keywords-translation-container'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function removeKeywordsTranslation(array &$form, FormStateInterface $form_state){
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $translations = unserialize($workflow->getDataElement('keywords_translations')) ?? [];
                        array_pop($translations);
                        $workflow->setDataElement('keywords_translations', serialize($translations));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function removeKeywordsTranslationCallback(array &$form, FormStateInterface $form_state){
                        $form['container-keywords']['keywords-translation-container']['#collapsed'] = false;
                        return $form['container-keywords']['keywords-translation-container'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addTextTranslation(array &$form, FormStateInterface $form_state){
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $translations = unserialize($workflow->getDataElement('text_translations')) ?? [];
                        $translations[] = [];
                        $workflow->setDataElement('text_translations', serialize($translations));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addTextTranslationCallback(array &$form, FormStateInterface $form_state){
                        $form['main_text-translation-container']['#collapsed'] = false;
                        return $form['main_text-translation-container'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function removeTextTranslation(array &$form, FormStateInterface $form_state){
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $translations = unserialize($workflow->getDataElement('text_translations')) ?? [];
                        array_pop($translations);
                        $workflow->setDataElement('text_translations', serialize($translations));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function removeTextTranslationCallback(array &$form, FormStateInterface $form_state){
                        $form['main_text-translation-container']['#collapsed'] = false;
                        return $form['main_text-translation-container'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addTitleTranslation(array &$form, FormStateInterface $form_state){
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $translations = unserialize($workflow->getDataElement('title_translations')) ?? [];
                        $translations[] = [];
                        $workflow->setDataElement('title_translations', serialize($translations));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addTitleTranslationCallback(array &$form, FormStateInterface $form_state){
                        $form['title-translation-container']['#collapsed'] = false;
                        return $form['title-translation-container'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function removeTitleTranslation(array &$form, FormStateInterface $form_state){
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $translations = unserialize($workflow->getDataElement('title_translations')) ?? [];
                        array_pop($translations);
                        $workflow->setDataElement('title_translations', serialize($translations));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function removeTitleTranslationCallback(array &$form, FormStateInterface $form_state){
                        $form['title-translation-container']['#collapsed'] = false;
                        return $form['title-translation-container'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addCoiTranslation(array &$form, FormStateInterface $form_state){
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $translations = unserialize($workflow->getDataElement('coi_translations')) ?? [];
                        $translations[] = [];
                        $workflow->setDataElement('coi_translations', serialize($translations));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addCoiTranslationCallback(array &$form, FormStateInterface $form_state){
                        $form['coi-translation-container']['#collapsed'] = false;
                        return $form['coi-translation-container'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function removeCoiTranslation(array &$form, FormStateInterface $form_state){
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $translations = unserialize($workflow->getDataElement('coi_translations')) ?? [];
                        array_pop($translations);
                        $workflow->setDataElement('coi_translations', serialize($translations));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function removeCoiTranslationCallback(array &$form, FormStateInterface $form_state){
                        $form['coi-translation-container']['#collapsed'] = false;
                        return $form['coi-translation-container'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function deleteKeyword(array &$form, FormStateInterface $form_state){
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $keywords = unserialize($workflow->getDataElement('keywords'));
        
                        //speichere vorhandene Keywords, da beim Ändern der Keywörter und direktem Klick auf "Add keyword" kein AjaxRequest zum automatischen Speichern erfolgt
                        foreach($form_state->getUserInput() as $k => $v){
                
                                if(preg_match('/^keyword#(\d+)/', $k, $matches)) {
                                        $keywords[$matches[1]] = $v;
                                }
                        }
                        
                        array_pop($keywords);
                        $workflow->setDataElement('keywords', serialize($keywords));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function deleteKeywordCallback(array &$form, FormStateInterface $form_state){
                        $form['container-keywords']['#collapsed'] = false;
                        return $form['container-keywords'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addKeyword(array &$form, FormStateInterface $form_state){
                        
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        
                        if($workflow->hasDataElement('keywords'))
                                $keywords = unserialize($workflow->getDataElement('keywords')) ?? [];
                        else
                                $keywords = [];
                        
                        //speichere vorhandene Keywords, da beim Ändern der Keywörter und direktem Klick auf "Add keyword" kein AjaxRequest zum automatischen Speichern erfolgt
                        foreach($form_state->getUserInput() as $k => $v){
        
                                if(preg_match('/^keyword#(\d+)/', $k, $matches)) {
                                        $keywords[$matches[1]] = $v;
                                }
                        }
                        
                        $keywords[count($keywords)] = '';
                        $workflow->setDataElement('keywords', serialize($keywords));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addKeywordCallback(array &$form, FormStateInterface $form_state){
                        $form['container-keywords']['#collapsed'] = false;
                        return $form['container-keywords'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addAuthorDB(array &$form, FormStateInterface $form_state){
                        
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $authors = unserialize($workflow->getDataElement('authors_db')) ?? [];
                        $authors[] = [];
                        $workflow->setDataElement('authors_db', serialize($authors));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addAuthorDBCallback(array &$form, FormStateInterface $form_state){
                        $form['wrapper_authors']['content']['db']['#collapsed'] = false;
                        return $form['wrapper_authors']['content']['db'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function deleteAuthorDB(array &$form, FormStateInterface $form_state){
                
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $authors = unserialize($workflow->getDataElement('authors_db')) ?? [];
                        array_pop($authors);
                        $workflow->setDataElement('authors_db', serialize($authors));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function deleteAuthorDBCallback(array &$form, FormStateInterface $form_state){
                        $form['wrapper_authors']['content']['db']['#collapsed'] = false;
                        return $form['wrapper_authors']['content']['db'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addAuthorAdd(array &$form, FormStateInterface $form_state){
                
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $authors = unserialize($workflow->getDataElement('authors_add')) ?? [];
                        $authors[] = [];
                        $workflow->setDataElement('authors_add', serialize($authors));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addAuthorAddCallback(array &$form, FormStateInterface $form_state){
                        $form['wrapper_authors']['content']['add']['#collapsed'] = false;
                        return $form['wrapper_authors']['content']['add'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function deleteAuthorAdd(array &$form, FormStateInterface $form_state){
                
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $authors = unserialize($workflow->getDataElement('authors_add')) ?? [];
                        array_pop($authors);
                        $workflow->setDataElement('authors_add', serialize($authors));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function deleteAuthorAddCallback(array &$form, FormStateInterface $form_state){
                        $form['wrapper_authors']['content']['add']['#collapsed'] = false;
                        return $form['wrapper_authors']['content']['add'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addCorporation(array &$form, FormStateInterface $form_state){
                
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $authors = unserialize($workflow->getDataElement('corporations')) ?? [];
                        $authors[] = [];
                        $workflow->setDataElement('corporations', serialize($authors));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addCorporationCallback(array &$form, FormStateInterface $form_state){
                        $form['wrapper_authors']['content']['corporations']['#collapsed'] = false;
                        return $form['wrapper_authors']['content']['corporations'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function deleteCorporation(array &$form, FormStateInterface $form_state){
                
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        $authors = unserialize($workflow->getDataElement('corporations')) ?? [];
                        array_pop($authors);
                        $workflow->setDataElement('corporations', serialize($authors));
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function deleteCorporationCallback(array &$form, FormStateInterface $form_state){
                        $form['wrapper_authors']['content']['corporations']['#collapsed'] = false;
                        return $form['wrapper_authors']['content']['corporations'];
                }
        }