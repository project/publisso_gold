<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowEditorActionAfterRevision.
 */
namespace Drupal\publisso_gold\Form\Workflow;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Drupal\publisso_gold\Controller\Workflow\Comment;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Drupal\publisso_gold\Controller\WorkflowInfoMail;

/**
 * Provides a simple example form.
 */
class WorkflowState_0019 extends FormBase {
	private $modname = 'publisso_gold';
	private $database;
	private $modpath;
	private $num_corporations;
	public function __construct(Connection $database) {
		$this->database = $database;
	}
        
        /**
         * @param ContainerInterface $container
         * @return WorkflowState_0019|static
         */
        public static function create(ContainerInterface $container) {
		return new static ( $container->get ( 'database' ) );
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function getFormId() {
		return 'WorkflowState_0019';
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
		
	        if(!$form_state->has('wf_id')) $form_state->set('wf_id', $args['wf_id']);
	        if(!$form_state->get('wf_id')) return $form;
	        
	        $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($form_state->get('wf_id'));
	        
	        $actionItems = $workflow->getSchemaItemActions(null, true);
	        $options = [];
                
                $comment_options = [];
                
	        foreach($actionItems as $item){
	                $options[$item['schema_follower']] = (string)$this->t($item['schema_select_action']);
                        if($item['schema_comment_required'] == 1) $comment_options[] = $item['schema_follower'];
                }

                $comment_required = [];
                $not_comment_required = [];
                foreach($comment_options as $_){
                        
                        if(count($comment_required)) $comment_required[] = 'or';
                        $comment_required[] = [':input[name="follower"]' => array('value' => $_)];
                        $not_comment_required[] = [':input[name="follower"]' => array('!value' => $_)];
                }
	        
	        $form['follower'] = [
	                '#type' => 'select',
                        '#title' => (string)$this->t('Select action'),
                        '#required' => true,
                        '#options' => $options
                ];
                
                $form['comment_for_author'] = [
                        '#type' => 'textarea',
                        '#title' => (string)$this->t('Comment for author'),
                        '#states' => [
                                'visible'     => [':input[name="follower"]' => array('value' => 17)],
                                'required'     => [':input[name="follower"]' => array('value' => 17)],
                                'invisible'   => [':input[name="follower"]' => array('!value' => 17)],
                                'optional'   => [':input[name="follower"]' => array('!value' => 17)],
                        ]
                ];
                
                $form['comment_for_eo'] = [
                        '#type' => 'textarea',
                        '#title' => (string)$this->t('Comment for editorial office'),
                        '#states' => [
                                'visible'     => [':input[name="follower"]' => array('value' => 21)],
                                'invisible'   => [':input[name="follower"]' => array('!value' => 21)],
                        ],
                ];
                
                $form['comment_for_editor'] = [
                        '#type' => 'textarea',
                        '#title' => (string)$this->t('Comment for editor'),
                        '#states' => [
                                'visible'     => [':input[name="follower"]' => array('value' => 11)],
                                'required'   => [':input[name="follower"]' => array('value' => 11)],
                        ],
                ];
                
	        $form['submit'] = [
	                '#type' => 'submit',
                        '#value' => (string)$this->t('Submit'),
                        '#suffix' => '<br><br>'
                ];
                
		$form ['#cache'] = [
			'max-age' => 0
		];
		
		return $form;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {
	        return $form;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {
	        
	        $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($form_state->get('wf_id'));
	        $session = \Drupal::service('session');
	        
	        if($workflow->getLock()) {
                
                        switch ( $form_state->getValue( 'follower' ) ) {
                                
                                case 11: //to editor
                                        $comment = $form_state->getValue( 'comment_for_editor' );
                                        if ( !empty( $comment ) ) {
                                                Comment::new( $form_state->get( 'wf_id' ), $workflow->getElement( 'schema_identifier' ), $comment, null, null, $workflow->getSchemaCurrentRole() );
                                        }
                                        break;
                                
                                case 17: //revision
                                
                                        $comment = $form_state->getValue( 'comment_for_author' );
                                
                                        if ( !empty( $comment ) ) {
                                                Comment::new( $form_state->get( 'wf_id' ), $workflow->getElement( 'schema_identifier' ), $comment, null, null, $workflow->getSchemaCurrentRole(), 'submitting_author' );
                                        }
                                
                                        break;
                        
                                case 21: //accept
        
                                        $comment = $form_state->getValue( 'comment_for_eo' );
                                        if ( !empty( $comment ) ) {
                                                Comment::new( $form_state->get( 'wf_id' ), $workflow->getElement( 'schema_identifier' ), $comment, null, null, $workflow->getSchemaCurrentRole() );
                                        }
                                
                                        break;
                        }
                
                        $workflow->setElement( 'editor_last_edited', $session->get( 'user' ) [ 'id' ] );
                        $workflow->newSchemaState( $form_state->getValue( 'follower' ) );
                        $workflow->unlock();
                }
                
                $addParams = [];
                if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                        $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                        \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                }
                
                $form_state->setRedirect('publisso_gold.dashboard', $addParams, ['fragment' => 'rwDashboardItem-'.$form_state->get('wf_id')]);
		return $form;
	}
}
