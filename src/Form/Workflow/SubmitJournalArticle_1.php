<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\Workflow\SubmitJournalChapter_1.
 */
namespace Drupal\publisso_gold\Form\Workflow;

use Drupal\Core\Form\FormState;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\publisso_gold\Controller\Manager\JournalManager;
use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
use Drupal\publisso_gold\Controller\Medium\Journal;
use Drupal\publisso_gold\Controller\Publisso;
use Drupal\publisso_gold\Controller\Workflow\ArticleWorkflow;
use \Drupal\publisso_gold\Controller\Workflow\ChapterWorkflow;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides chapter create form.
 */
class SubmitJournalArticle_1 extends FormBase {
        
        protected $workflow    = null;
        protected $journal     = null;
        
        /**
         * @return string
         */
        public function getFormId() {
                return 'SubmitJournalArticle_1';
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @return array
         * @throws \Exception
         */
        public function buildForm(array $form, FormStateInterface $form_state, array $args = []) {
                
                $this->preBuild($form, $form_state, $args);
                if(!$this->workflow()){ $this->messenger()->addError('Coulnd load item!'); return $form; }
                $this->getForm($form, $form_state);
                $this->getActions($form);
                $form['#cache'] = ['max-age' => 0];
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function proceedStep2(array &$form, FormStateInterface $form_state){
                
                $this->saveForm($form, $form_state);
                $ts = \Drupal::service('tempstore.private')->get('publisso_gold');
                $ts->set('overwrite-SubmitJournalArticle_1', 'Form\Workflow\SubmitJournalArticle_2');
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @throws \Exception
         */
        protected function preBuild(array &$form, FormStateInterface $form_state, array $args = []){
                if(!$form_state->get('wf_id')) $form_state->set('wf_id', $args['wf_id']);
                SubmitJournalArticleHelper::saveUserInput($form_state->getUserInput(), $form_state->get('wf_id'));
                $this->setWorkflow($form_state->get('wf_id'));
                $this->setJournal();
                if($this->workflow()) $this->workflow()->publish(false, true);
        }
        
        /**
         * @param int $wf_id
         */
        protected function setWorkflow(int $wf_id){
                if(!$this->workflow) $this->workflow = WorkflowManager::getItem($wf_id);
        }
        
        protected function setJournal(){
                $this->journal = JournalManager::getJournal($this->workflow()->getDataElement('jrn_id'));
        }
        
        protected function workflow() :? ArticleWorkflow {
                return $this->workflow;
        }
        
        protected function journal() : Journal {
                return $this->journal;
        }
        
        /**
         * @param array $form
         */
        protected function getActions(array &$form){
                $form['actions'] = [
                        '#type' => 'actions',
                        'back_to_journal' => [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Back to journal'),
                                '#submit' => ['::saveForm', '::backToJournal'],
                                '#button_type' => 'warning'
                        ],
                        'back_to_dashboard' => [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Back to dashboard'),
                                '#submit' => ['::saveForm', '::backToDashboard'],
                                '#button_type' => 'warning'
                        ],
                        'preview' => [
                                '#type' => 'button',
                                '#value' => (string)$this->t('Preview'),
                                '#button_type' => 'primary',
                                '#ajax' => [
                                        'callback' => '::preview'
                                ]
                        ],
                        'continue' => [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Continue submission'),
                                '#button_type' => 'success',
                                '#submit' => ['::proceedStep2']
                        ]
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getForm(array &$form, FormStateInterface $form_state){
        
                $this->getFormHeader            ($form, $form_state);
                $this->getFormTitle             ($form, $form_state);
                $this->getFormAbstract          ($form, $form_state);
                $this->getFormKeywords          ($form, $form_state);
                $this->getFormTextReferences    ($form, $form_state);
                $this->getFormAuthorsDB         ($form, $form_state);
                $this->getFormAuthorsAdd        ($form, $form_state);
                $this->getFormCorporations      ($form, $form_state);
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormHeader(array &$form, FormStateInterface $form_state){
                
                $form ['mu_site_headline'] = [
                        '#markup' => (string)$this->t(($this->workflow()->getElement('state') == 'running submission' ? 'Add' : 'Edit').' Article'),
                        '#prefix' => '<h1>',
                        '#suffix' => '</h1>'
                ];
        
                $form['mu_journal_title'] = [
                        '#markup' => $this->journal()->getElement('title'),
                        '#prefix' => '<h2>',
                        '#suffix' => '</h2>'
                ];
        
                $url = Url::fromRoute('publisso_gold.journals_journal.manuscript_guidelines', ['jrn_id' => $this->journal()->getElement('id')], ['attributes' => ['target' => '_blank']]);
        
                $form['mu_initial_info'] = [
                        '#type' => 'inline_template',
                        '#template' => $this->t(
                                'Please read the @manuscript_guidelines, the policy (@policy_de, @policy_en) and the author\'s contract (@contract_de, @contract_en). On the second page of the submission form, you will have to accept the author\'s contract (legally binding German version) to conclude the submission.',
                                [
                                        '@manuscript_guidelines'    => Link::fromTextAndUrl($this->t('manuscript guidelines'), $url)->toString(),
                                        '@policy_en' => Link::fromTextAndUrl($this->t('English'), Url::fromUri('https://www.publisso.de/en/publishing/books/books-policy/', ['attributes' => ['target' => '_blank']]))->toString(),
                                        '@policy_de' => Link::fromTextAndUrl($this->t('German'), Url::fromUri('https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/' ,['attributes' => ['target' => '_blank']]))->toString(),
                                        '@contract_en' => Link::fromTextAndUrl($this->t('English'), Url::fromUri('https://www.publisso.de/en/publishing/books/books-policy/#c5189', ['attributes' => ['target' => '_blank']]))->toString(),
                                        '@contract_de' => Link::fromTextAndUrl($this->t('German'), Url::fromUri('https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/#c5185', ['attributes' => ['target' => '_blank']]))->toString(),
                                ]
                        ),
                        '#suffix' => '</div><br><br>',
                        '#prefix' => '<div class="font-red">'
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormTitle(array &$form, FormStateInterface $form_state){
                
                if(!$this->workflow()->getDataElement('predefined_title')) {
                        //article-title
                        $form[ 'title' ] = [
                                '#type'          => 'textfield',
                                '#title'         => t( 'Title' ),
                                '#required'      => true,
                                '#placeholder'   => (string)$this->t( 'Enter the title of your article here' ),
                                '#maxlength'     => 255,
                                '#ajax'          => [
                                        'event'           => 'change',
                                        'prevent'         => 'blur',
                                        'progress'        => [
                                                'type' => 'none'
                                        ],
                                        'callback'        => '::autosave',
                                        'disable-refocus' => true
                                ],
                                '#default_value' => $this->workflow()->getDataElement( 'title' )
                        ];
                        /*
                        $form[ 'title_short' ] = [
                                '#type'          => 'textfield',
                                '#title'         => t( 'Short-title (for PDF-Output as "Running title")' ),
                                '#required'      => true,
                                '#placeholder'   => (string)$this->t( 'Enter the short-title of your article here' ),
                                '#maxlength'     => 255,
                                '#ajax'          => [
                                        'event'           => 'change',
                                        'prevent'         => 'blur',
                                        'progress'        => [
                                                'type' => 'none'
                                        ],
                                        'callback'        => '::autosave',
                                        'disable-refocus' => true
                                ],
                                '#default_value' => $this->workflow()->getDataElement( 'title_short' )
                        ];
                        */
                }
                else{
                        $form['title'] = [
                                '#markup' => $this->workflow()->getDataElement('title'),
                                '#prefix' => '<h2>',
                                '#suffix' => '</h2>'
                        ];
                }
        
                $form['title_short']['#default_value'] = $this->workflow()->getDataElement('title_short');
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormAbstract(array &$form, FormStateInterface $form_state){
                
                //abstract (if required)
                if($this->journal()->getControlElement('abstract_required') == 1 || $this->journal()->getControlElement('abstract_required') == 2):
                
                        $form['abstract'] = [
                                '#type'         => 'textarea',
                                '#title'        => (string)$this->t('Abstract'),
                                '#prefix'       => (string)$this->t('If an abstract is required or optional, please enter it here.'),
                                '#rows'         => 20,
                                '#attributes'   => [
                                        'class'         => [
                                                \Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
                                        ],
                                        'readonly' => 'readonly'
                                ],
                                '#ajax'         => [
                                        'event'         => 'change',
                                        'prevent'       => 'blur',
                                        'progress'      => [
                                                'type'          => 'none'
                                        ],
                                        'callback'      => '::autosave',
                                        'wrapper'       => 'autosave_msg'
                                ],
                                '#required'             => $this->journal()->getControlElement('abstract_required') == 1 ? true : false,
                                '#default_value' => $this->workflow()->getDataElement('abstract')
                        ];
        
                endif;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormKeywords(array &$form, FormStateInterface $form_state){
                
                $form['keywords'] = [
                        '#type'                 => 'textfield',
                        '#title'                => (string)t('Keywords'),
                        '#placeholder'          => (string)$this->t('Enter the keywords separated with a semicolon here'),
                        '#maxlength'            => 400,
                        '#ajax'         => [
                                'event'         => 'change',
                                'prevent'       => 'blur',
                                'progress'      => [
                                        'type'          => 'none'
                                ],
                                'callback'      => '::autosave',
                                'disable-refocus'       => true
                        ],
                        '#default_value' => $this->workflow()->getDataElement('keywords')
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormTextReferences(array &$form, FormStateInterface $form_state){
                
                if(!$this->workflow()->getDataElement('predefined_maintext')) {
                        $form[ 'article_text' ] = [
                                '#type'          => 'textarea',
                                '#title'         => t( 'Main Text' ),
                                '#rows'          => 50,
                                '#required'      => true,
                                '#attributes'    => [
                                        'class'    => [
                                                \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' )
                                        ],
                                        'readonly' => 'readonly'
                                ],
                                '#ajax'          => [
                                        'event'           => 'change',
                                        'prevent'         => 'blur',
                                        'progress'        => [
                                                'type' => 'none'
                                        ],
                                        'callback'        => '::autosave',
                                        'disable-refocus' => true
                                ],
                                '#default_value' => $this->workflow()->getDataElement( 'article_text' )
                        ];
                }
                else{
                        $form['article_text'] = [
                                '#type' => 'fieldset',
                                '#title' => (string)$this->t('Main text'),
                                'main_text' => [
                                        '#type' => 'inline_template',
                                        '#template' => $this->workflow()->getDataElement('article_text')
                                ]
                        ];
                }
        
                if(!$this->workflow()->getDataElement('predefined_maintext')) {
                        $form[ 'references' ] = [
                                '#type'          => 'textarea',
                                '#title'         => (string)t( 'References' ),
                                '#rows'          => 10,
                                '#cols'          => 60,
                                '#prefix'        => '<br>',
                                '#id'            => 'ta-rwReferences',
                                '#ajax'          => [
                                        'event'           => 'change',
                                        #'prevent'       => 'blur',
                                        'progress'        => [
                                                'type' => 'none'
                                        ],
                                        'callback'        => '::autosave',
                                        'disable-refocus' => true
                                ],
                                '#default_value' => $this->workflow()->getDataElement( 'references' )
                        ];
                }
                else{
                        $form['references'] = [
                                '#type' => 'fieldset',
                                '#title' => (string)$this->t('References'),
                                'main_text' => [
                                        '#type' => 'inline_template',
                                        '#template' => nl2br($this->workflow()->getDataElement('references'))
                                ]
                        ];
                }
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormAuthorsDB(array &$form, FormStateInterface $form_state){
                
                $authorsDB = json_decode($this->workflow()->getDataElement('author_db'), 1) ?? [];
                
                if($form_state->hasValue('wrapper_authors_db')) $authorsDB = $form_state->getValue('wrapper_authors_db')['authors'];
                
                $form_state->set('cntAuthorsDB', count($authorsDB));
                
                if($form_state->get('cntAuthorsDB') < 3) $form_state->set('cntAuthorsDB', 3);
                
                $form['wrapper_authors_db'] = [
                        '#tree' => true,
                        '#type' => 'fieldset',
                        '#collapsible' => true,
                        '#title' => (string)$this->t('Author(s)'),
                        '#description' => (string)$this->t('Enter one author per box. Authors who are not yet in the database can be added as additional authors.'),
                        '#description_attributes' => [
                                'style' => 'font-size: 16px;'
                        ],
                        'authors' => [
                                '#type' => 'table',
                                '#header' => [
                                        ['data' => (string)$this->t('Author(s)'), 'width' => '45%'],
                                        ['data' => (string)$this->t('Corr.'), 'width' => '5%', 'title' => (string)$this->t('Corresponding author')],
                                        ['data' => (string)$this->t('Pos.'), 'width' => '5%', 'title' => (string)$this->t('Position')],
                                        ['data' => (string)$this->t('Additional affiliations'), 'width' => '45%']
                                ],
                                '#prefix' => '<div id="wrapper-authors-db">',
                                '#suffix' => '</div>'
                        ],
                        'actions' => [
                                '#type' => 'actions',
                                'add' => [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Add row'),
                                        '#submit' => ['::addAuthorDB'],
                                        '#ajax' => [
                                                'callback' => '::addAuthorDBCallback',
                                                'wrapper' => 'wrapper-authors-db'
                                        ],
                                        '#limit_validation_errors' => []
                                ]
                        ]
                ];
                
                for($i = 0; $i < $form_state->get('cntAuthorsDB'); $i++){
                        
                        $author = $user = $affiliations = null;
                        
                        if(array_key_exists($i, $authorsDB)){
                                $author = $authorsDB[$i];
                                $user = SubmitJournalArticleHelper::getAuthorFromString($author['author']);
                                if($user) $affiliations = $user->profile->getAdditionalAffiliations();
                        }
                        
                        $form['wrapper_authors_db']['authors'][$i] = [
                                'author' => [
                                        '#type' => 'textfield',
                                        '#autocomplete_route_name' => 'autocomplete.user.public',
                                        '#autocomplete_route_parameters' => [],
                                        '#ajax'         => [
                                                'event'         => 'autocompleteclose change',
                                                'prevent'       => 'blur',
                                                'progress'      => [
                                                        'type'          => 'none'
                                                ],
                                                'callback'      => '::autosave',
                                                'disable-refocus'       => true,
                                                'wrapper' => "add_affiliations_".$i,
                                                
                                        ],
                                        '#default_value' => $author['author']
                                ],
                                'is_corresponding' => [
                                        '#type' => 'checkbox',
                                        '#default_value' => $author['is_corresponding'],
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ]
                                ],
                                'weight' => [
                                        '#type' => 'number',
                                        '#default_value' => $author['weight'],
                                        '#attributes' => [
                                                'style' => 'width: 5em;'
                                        ],
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ]
                                ],
                                'wrapper-add_affiliation' => [
                                        '#type' => 'fieldset',
                                        '#collapsible' => true,
                                        '#collapsed' => true,
                                        '#title' => (string)$this->t('Additional affiliations'),
                                        'add_affiliations' => [
                                                '#type' => 'table',
                                                '#header' => [
                                                        ['data' => (string)$this->t('Affiliation'), 'width' => '90%'],
                                                        ['data' => (string)$this->t('Show'), 'width' => '5%'],
                                                        ['data' => (string)$this->t('Weight'), 'width' => '5%']
                                                ]
                                        ],
                                        '#prefix' => '<div id="add_affiliations_'.$i.'">',
                                        '#suffix' => '</div>'
                                ]
                        ];
                        
                        $affIndex = 0;
                        if(!$affiliations) $affiliations = [];
                        
                        foreach ($affiliations ?? [] as $affiliation) {
        
                                $affString = implode('; ', [
                                        $affiliation['department'],
                                        $affiliation['institute'],
                                        $affiliation['city'],
                                        Publisso::tools()->getCountry($affiliation['country'])
                                ]);
        
                                $form['wrapper_authors_db']['authors'][$i]['wrapper-add_affiliation']['add_affiliations'][$affIndex] = [
                                        [
                                                'affiliation' => [
                                                        '#type'          => 'textfield',
                                                        '#default_value' => $affString,
                                                        '#attributes'    => [
                                                                'title' => $affString,
                                                                'readonly' => 'readonly'
                                                        ]
                                                ]
                                        ],
                                        [
                                                'show' => [
                                                        '#type'          => 'checkbox',
                                                        '#default_value' => $author['add_affiliations'][$affIndex]['show'] ?? 0,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'prevent'         => 'blur',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                ]
                                        ],
                                        [
                                                'weight' => [
                                                        '#type'          => 'number',
                                                        '#default_value' => $author['add_affiliations'][$affIndex]['weight'] ?? null,
                                                        '#attributes'    => [
                                                                'style' => 'width: 5em;'
                                                        ],
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'prevent'         => 'blur',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                ]
                                        ]
                                ];
                                $affIndex++;
                        }
                }
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormAuthorsAdd(array &$form, FormStateInterface $form_state) {
                
                $authors = json_decode($this->workflow()->getDataElement('author_add'), 1) ?? [];
                
                if(!$form_state->has('cntAuthorsAdd')) $form_state->set('cntAuthorsAdd', count($authors));
                if($form_state->get('cntAuthorsAdd') < 1) $form_state->set('cntAuthorsAdd', 1);
        
                $form['wrapper_authors_add'] = [
                        '#type' => 'fieldset',
                        '#collapsible' => true,
                        '#title' => (string) $this->t('Additional author(s)'),
                        '#tree' => true,
                        'authors' => [
                                '#type' => 'container',
                                '#prefix' => '<div id="wrapper_authors_add">',
                                '#suffix' => '</div>'
                        ]
                ];
        
                $form['wrapper_authors_add']['btn_add_author'] = [
                        '#type' => 'submit',
                        '#value' => (string) $this->t('Add box'),
                        '#prefix' => '<br>',
                        '#limit_validation_errors' => [],
                        '#submit' => ['::addAuthorAdd'],
                        '#ajax' => [
                                'wrapper' => 'wrapper_authors_add',
                                'callback' => '::addAuthorAddCallback'
                        ]
                ];
                
                for($i = 0; $i < $form_state->get('cntAuthorsAdd'); $i++){
                        
                        $addAffiliations = $authors[$i]['add_affiliations'] ?? [];
                        
                        if(!$form_state->has('cntAuthorsAdd'.$i.'Affiliations')) $form_state->set('cntAuthorsAdd'.$i.'Affiliations', count($addAffiliations));
                        
                        $form['wrapper_authors_add']['authors'][$i] = [
                                '#type' => 'fieldset',
                                '#collapsible' => true,
                                '#title' => (string)$this->t('Additional author #@nr', ['@nr' => $i + 1]),
                                'firstname' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('First name'),
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $authors[$i]['firstname'] ?? null
                                ],
                                'lastname' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Last name'),
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $authors[$i]['lastname'] ?? null
                                ],
                                'affiliation' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Affiliation'),
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $authors[$i]['affiliation'] ?? null
                                ],
                                'add_affiliations' => [
                                        '#type' => 'container',
                                        '#prefix' => '<div id="wrapper_author_add_'.$i.'_affiliations">',
                                        '#suffix' => '<br></div>',
                                        'affiliations' => []
                                ],
                                'btn_add_affiliation' => [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Add affiliation'),
                                        '#suffix' => '<br><br>',
                                        '#name' => 'author_add_'.$i.'_affiliation_add',
                                        '#submit' => ['::AuthorAddAffiliationAdd'],
                                        '#attributes' => [
                                                'number' => $i
                                        ],
                                        '#limit_validation_errors' => [],
                                        '#ajax' => [
                                                'callback' => '::AuthorAddAffiliationAddCallback',
                                                'wrapper' => 'wrapper_author_add_'.$i.'_affiliations'
                                        ]
                                ],
                                'weight' => [
                                        '#type' => 'number',
                                        '#title' => (string)$this->t('Position'),
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $authors[$i]['weight'] ?? null
                                ]
                        ];
                        
                        for($ii = 0; $ii < $form_state->get('cntAuthorsAdd'.$i.'Affiliations'); $ii++){
        
                                $form['wrapper_authors_add']['authors'][$i]['add_affiliations'][$ii] = [
                                        '#type' => 'textfield',
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $addAffiliations[$ii] ?? null
                                ];
                        }
                }
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormCorporations(array &$form, FormStateInterface $form_state){
                $corporation = json_decode($this->workflow()->getDataElement('corporations'), 1) ?? [];
                if(!$form_state->has('cntCorporation')) $form_state->set('cntCorporation', count($corporation));
                if($form_state->get('cntCorporation') < 1) $form_state->set('cntCorporation', 1);
                
                $form['wrapper_corporations'] = [
                        '#type' => 'fieldset',
                        '#tree' => true,
                        '#collapsible' => true,
                        '#title' => (string)$this->t('Corporations'),
                        'corporation' => [
                                '#type' => 'container',
                                '#prefix' => '<div id="wrapper_corporations">',
                                '#suffix' => '<br></div>'
                        ],
                        'btn_add_corporation' => [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Add corporation'),
                                '#submit' => ['::addCorporation'],
                                '#limit_validation_errors' => [],
                                '#ajax' => [
                                        'wrapper' => 'wrapper_corporations',
                                        'callback' => '::addCorporationCallback'
                                ]
                        ]
                ];
                
                for($i = 0; $i < $form_state->get('cntCorporation'); $i++){
                        
                        $form['wrapper_corporations']['corporation'][$i] = [
                                '#type' => 'textfield',
                                '#default_value' => $corporation[$i] ?? null,
                                '#ajax'          => [
                                        'event'           => 'change',
                                        'prevent'         => 'blur',
                                        'progress'        => [
                                                'type' => 'none'
                                        ],
                                        'callback'        => '::autosave',
                                        'disable-refocus' => true
                                ]
                        ];
                }
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed|string[]
         * @throws \Exception
         */
        public function autosave(array &$form, FormStateInterface $form_state){
                return SubmitJournalArticleHelper::autosave($form, $form_state, $this->workflow);
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return \Drupal\Core\Ajax\AjaxResponse
         * @throws \Exception
         */
        public function preview(array &$form, FormStateInterface $form_state){
                return SubmitJournalArticleHelper::preview($form_state);
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthorDB(array &$form, FormStateInterface $form_state){
                $form_state->set('cntAuthorsDB', $form_state->get('cntAuthorsDB') + 1);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorDBCallback(array &$form, FormStateInterface $form_state){
                return $form['wrapper_authors_db']['authors'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthorAdd(array &$form, FormStateInterface $form_state){
                $form_state->set('cntAuthorsAdd', $form_state->get('cntAuthorsAdd') + 1);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorAddCallback(array &$form, FormStateInterface $form_state){
                return $form['wrapper_authors_add']['authors'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function AuthorAddAffiliationAdd(array &$form, FormStateInterface $form_state){
                
                $number = $form_state->getTriggeringElement()['#attributes']['number'];
                $form_state->set('cntAuthorsAdd'.$number.'Affiliations', $form_state->get('cntAuthorsAdd'.$number.'Affiliations') + 1);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function AuthorAddAffiliationAddCallback(array &$form, FormStateInterface $form_state){
                $number = $form_state->getTriggeringElement()['#attributes']['number'];
                return $form['wrapper_authors_add']['authors'][$number]['add_affiliations'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addCorporation(array &$form, FormStateInterface $form_state){
                $form_state->set('cntCorporation', $form_state->get('cntCorporation') + 1);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addCorporationCallback(array &$form, FormStateInterface $form_state){
                return $form['wrapper_corporations']['corporation'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function saveForm(array &$form, FormStateInterface $form_state){
                
                foreach(SubmitJournalArticleHelper::getSimpleValues() as $field){
                        if($form_state->hasValue($field)) $this->workflow()->setDataElement($field, $form_state->getValue($field));
                }
                
                if($form_state->hasValue('wrapper_authors_db')){
                        $authorsDB = SubmitJournalArticleHelper::getAuthorsDBFromForm($form_state->getValue('wrapper_authors_db'));
                        $this->workflow()->setDataElement('author_db', json_encode($authorsDB));
                }
        
                if($form_state->hasValue('wrapper_authors_add')){
                        $this->workflow()->setDataElement('author_add', json_encode($form_state->getValue('wrapper_authors_add')['authors']));
                }
                
                if($form_state->hasValue('wrapper_corporations')){
                        $this->workflow()->setDataElement('corporations', json_encode($form_state->getValue('wrapper_corporations')['corporation']));
                }
                
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function backToJournal(array &$form, FormStateInterface $form_state){
                $form_state->setRedirectUrl($this->journal()->getUrl());
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function backToDashboard(array &$form, FormStateInterface $form_state){
                $form_state->setRedirect('publisso_gold.dashboard');
        }
}
