<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\Workflow\WorkflowState_0011.
         */
        namespace Drupal\publisso_gold\Form\Workflow;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\Workflow\Comment;

        /**
         * Provides a simple example form.
         */
        class WorkflowState_0011 extends FormBase {
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId() {
                        return 'workflow.WorkflowState_0011';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                        
                        if(array_key_exists('wf_id', $args)){

                                $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($args['wf_id']);
                                $form_state->set('wf_id', $args['wf_id']);
                                $form_state->set('new_identifier', $args['follower_overwrite'] ?? $workflow->getSchemaDefaultFollower()['schema_follower']);

                                $medium = $workflow->getMedium();
        
                                $actionItems = $workflow->getSchemaItemActions(null, true);
                                $options = [];
        
                                $comment_options = [];
        
                                foreach($actionItems as $item){
                                        $options[$item['schema_follower']] = (string)$this->t($item['schema_select_action'] ?? '');
                                        if($item['schema_comment_required'] == 1) $comment_options[] = $item['schema_follower'];
                                }
        
                                $comment_required = [];
                                $not_comment_required = [];
                                foreach($comment_options as $_){
                
                                        if(count($comment_required)) $comment_required[] = 'or';
                                        $comment_required['visible'][] = [':input[name="follower"]' => array('value' => $_)];
                                        $comment_required['required'][] = [':input[name="follower"]' => array('value' => $_)];
                                        $not_comment_required[] = [':input[name="follower"]' => array('!value' => $_)];
                                }
                                
                                $reviewerList= [];
                                foreach($medium->readReviewers() as $reviewer){
                                        if(!in_array($reviewer->getElement('id'), $reviewerList)){
                                                $reviewerList[$reviewer->getElement('id')] = $reviewer->profile->getReadableFullName();
                                        }
                                }
        
                                $form['follower'] = [
                                        '#type' => 'select',
                                        '#title' => (string)$this->t('Select action'),
                                        '#required' => true,
                                        '#options' => $options
                                ];
                                
                                $form ['reviewer-list'] = [
                                        '#type' => 'fieldset',
                                        '#title' => t ( 'Set reviewer(s)' ),
                                        '#prefix' => '<br>',
                                        'content' => [
                        
                                                'reviewers_list' => [
                                                        '#title' => t ( 'Available users for reviewer' ),
                                                        '#type' => 'select',
                                                        '#multiple' => true,
                                                        '#options' => $reviewerList,
                                                        '#default_value' => explode ( ',', $workflow->getElement('assigned_to_reviewer') ),
                                                        '#size' => 20,
                                                        '#states'    => array(
                                                                'required'   => [':input[name="follower"]' => array('value' => 12)],
                                                        )
                                                ],
                        
                                                'invite_help' => [
                                                        '#type' => 'markup',
                                                        '#markup' => ( string ) t ( 'If you would like to assign someone not listed, choose “invite”. Please note, that you have to assign the reviewer after the invitation has been accepted. When the chapter is in review you can assign, invite or unassign reviewers via "Submissions where I am editor" in the dashboard.' ),
                                                        '#prefix' => '<div>',
                                                        '#suffix' => '</div>'
                                                ],
                        
                                                'invite' => [
                                                        '#type' => 'link',
                                                        '#title' => t ( 'Invite user' ),
                                                        '#url' => Url::fromRoute ( 'publisso_gold.workflow.invite_user', [
                                                                'wf_id' => $form_state->get ( 'wf_id' )
                                                        ] ),
                                                        '#suffix' => '<br><br>'
                                                ]
                                        ],
                                        '#states'    => array(
                                                'visible'     => [':input[name="follower"]' => array('value' => 12)],
                                        )
                                ];
        
                                $form['recommendation'] = [
                                        '#type' => 'select',
                                        '#title' => (string)$this->t('Recommendation'),
                                        '#states' => [
                                                'visible'     => [':input[name="follower"]' => array('value' => 16)],
                                                'required'     => [':input[name="follower"]' => array('value' => 16)],
                                                'invisible'   => [':input[name="follower"]' => array('!value' => 16)],
                                                'optional'   => [':input[name="follower"]' => array('!value' => 16)],
                                        ],
                                        '#options' => [
                                                'accept' => (string)$this->t('Accept submission'),
                                                'reject' => (string)$this->t('Reject submission'),
                                                'revision' => (string)$this->t('To author for revision')
                                        ]
                                ];
                                
                                $form['comment'] = [
                                        '#type' => 'textarea',
                                        '#title' => (string)$this->t('Comment for EiC'),
                                        '#states' => [
                                                'visible' => [
                                                        ':input[name="follower"]' => ['value' => 16]
                                                ]
                                        ],
                                        '#placeholder' => (string)$this->t('Please note: your comment may be also visible to others involved in the book publication.')
                                ];
                                
                                $form['submit'] = [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Submit'),
                                        '#suffix' => '<hr>'
                                ];
                        }
        
                        return $form;
                }
                
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm(array &$form, FormStateInterface $form_state) {
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm(array &$form, FormStateInterface $form_state) {
                        
                        if(null === ($workflow = WorkflowManager::getItem($form_state->get('wf_id')))){
                                \Drupal::service('messenger')->addError((string)$this->t('Failed to load workflow-item #@id', ['@id' => $form_state->get('wf_id')]));
                                $form_state->setRebuild();
                                return $form;
                        }
                        
                        if(!$workflow->getLock()){
                                \Drupal::service('messenger')->addError((string)$this->t('Cant get write-lock for workflow-item #@id', ['@id' => $form_state->get('wf_id')]));
                                $form_state->setRebuild();
                                return $form;
                        }
                        
                        switch($form_state->getValue('follower')){
                             
                                case 12: //assign reviewer
                                        
                                        $reviewers = $form_state->getValue ( 'reviewers_list' );
        
                                        // inform users about registration if not done
                                        $assigned_users = [ ];
        
                                        foreach ( $reviewers as $uid ) {
                
                                                $_user = new \Drupal\publisso_gold\Controller\User ( $uid );
                
                                                if ($_user->getElement ( 'reg_notification_sent' ) == 0) {
                                                        $assigned_users [] = $_user;
                                                }
                                        }
        
                                        foreach ( $assigned_users as $_ ) {
                
                                                // send Reg-Mail to user if not yet be done
                                                if ($_->getElement ( 'reg_notification_sent' ) == 0) {
                        
                                                        $port = \Drupal::request ()->getPort ();
                                                        $host = \Drupal::request ()->getHttpHost ();
                                                        $host = "http" . ($port == 443 ? 's' : '') . "://$host";
                                                        $link = $host;
                                                        $password ['clear'] = Publisso::tools()->genTempPassword ();
                                                        $password['encrypted'] = Publisso::tools()->getDBEncPassword($password['clear']);
                        
                                                        $_->setElement ( 'password', $password ['encrypted'] );
                        
                                                        foreach ( ['registration_password', 'registration_info'] as $name ) {
                                
                                                                $vars = [
                                                                        '::firstname::' => $_->profile->getElement ( 'firstname' ),
                                                                        '::lastname::' => $_->profile->getElement ( 'lastname' ),
                                                                        '::link::' => $link,
                                                                        '::email::' => $_->profile->getElement ( 'email' ),
                                                                        '::user::' => $_->getElement ( 'user' ),
                                                                        '::login_link::' => $link . '/publisso_gold/login',
                                                                        '::password::' => $password ['clear']
                                                                ];
                                
                                                                Publisso::tools()->sendRegistrationInfoMail ( $name, $vars );
                                                        }
                        
                                                        $_->setElement ( 'reg_notification_sent', 1 );
                                                }
                                                // - Reg-Mail --
                
                                                $workflow->newUserReview($_);
                                        }
                                        // -- inform users --
        
        
                                        $workflow->setElement('assigned_to_reviewer', implode(',', array_unique($reviewers)), false);
                                        $workflow->setElement('reviewer_edited', null, false);
                                        $workflow->save();
                                        foreach($reviewers as $_){
                                                $workflow->newUserReview($_);
                                        }
                                        break;
                                        
                                case 16: //recommendation EiC
                                        $comment = $form_state->getValue( 'comment' );
        
                                        if ( !empty( $comment ) ) {
                                                Comment::new( $form_state->get( 'wf_id' ), $workflow->getElement( 'schema_identifier' ), $comment, null, null, $workflow->getSchemaCurrentRole(), 'submitting_author' );
                                        }
                                        
                                        if($form_state->hasValue('recommendation')){
                                                
                                                if($recommendation = $form_state->getValue('recommendation')){
                                                        $workflow->setRecommendation( $recommendation );
                                                }
                                        }
                                        break;
                        }
                        
                        $workflow->newSchemaState($form_state->getValue('follower'));
                        $workflow->setElement ( 'editor_last_edited', Publisso::currentUser()->getId() );
                        $workflow->unlock ();
                        
                        $addParams = [];
                        $fragment = [];
                        if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                                $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                                $fragment = ['fragment' => 'rwDashboardItem-'.$form_state->get('wf_id')];
                                \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                        }
                        if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardItem')) {
                                $addParams[ 'item' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardItem' );
                                \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardItem');
                        }
        
                        $form_state->setRedirect('publisso_gold.dashboard', $addParams, $fragment);
                        return $form;
                }
        }
