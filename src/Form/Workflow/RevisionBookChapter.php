<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\RevisionBookChapter.
 */
namespace Drupal\publisso_gold\Form\Workflow;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Drupal\publisso_gold\Controller\BookchapterTemplate;
use Drupal\publisso_gold\Controller\Manager\ChapterManager;
use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
use Drupal\publisso_gold\Controller\Publisso;
use Drupal\publisso_gold\Controller\Workflow\Comment;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;
use Drupal\Core\Url;

/**
 * Provides chapter create form.
 */
class RevisionBookChapter extends FormBase {
        private $modname = 'publisso_gold';
        private $modpath;
        private $modaccessweights = [];
        
        const __ACCESS_DENIED__             = 'You can not access this page. You may need to log in first.';
    
        /**
         * {@inheritdoc}
         */
        public function getFormId() {
                return 'revisionbookchapter';
        }
        
        /**
         * {@inheritdoc}
         */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                $setup = \Drupal::service('publisso_gold.setup');
                $tools = \Drupal::service('publisso_gold.tools');

                $wf_id = $args['wf_id'];

                $workflow = $tools->getWorkflowItem($wf_id);
                $book = $workflow->getMedium();

                $form_state->set('current_schema_identifier', $workflow->getSchemaCurrentIdentifier());
                
                $form_state->set('new_schema_identifier', $workflow->getSchemaDefaultFollower('schema_follower'));
                $form_state->set('new_schema_role', $workflow->getSchemaDefaultFollower('schema_followerrole'));
                
                if(!$form_state->get('new_schema_identifier')){

                        \Drupal::service('messenger')->addError($this->t('ERROR: Can\'t forward workflow-item. Missing new identifier!'));
                        return  $form;
                }

                /**
                 * link some libraries //deprecated
                 */
                $this->modpath = drupal_get_path('module', $this->modname);
                require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
                
                if(!$form_state->has('book')) $form_state->set('book', $book);
                if(!$form_state->has('workflow')) $form_state->set('workflow', $workflow);
                
                /**
                 * workflow has to be set in revision-state. if not, abort.
                 */
                if($form_state->get('workflow')->getElement('state') != 'in revision'){
                        return [
                                '#markup' => (string)$this->t('ERROR: Workflow has a wrong state (@state)!', ['@state' => $form_state->get('workflow')->getDataElement('state')])
                        ];
                }
                
                /**
                 * set some global parameters
                 */
                
                $link_book = Url::fromRoute('publisso_gold.books_book.manuscript_guidelines', ['bk_id' => $book->getElement('id')])->toString();
                $language = \Drupal::languageManager()->getCurrentLanguage();
        
                //Link1 (uri): Authors contract
                //Link2 (uri_1): Book policy
                $uri_de         = 'https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/#c5185';
		$url_de         = Url::fromUri($uri_de);
		$uri_1_de       = 'https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/';
		$url_1_de 	= Url::fromUri($uri_1_de);
		$text_de        = 'German';
	
		$uri_en         = 'https://www.publisso.de/en/publishing/books/books-policy/#c5189';
		$url_en 	= Url::fromUri($uri_en);
		$uri_1_en       = 'https://www.publisso.de/en/publishing/books/books-policy/';
		$url_1_en 	= Url::fromUri($uri_1_en);
		$text_en        = 'English';
                
                //initiate steps if not done
                if(!$form_state->get('step')) $form_state->set('step', 1);
                
//load workflow-data unless already done

                if(!$form_state->get('wf_data_loaded') === true){
                        
                        $form_data = [
                        ];
                        
                        $key = 'abstract';
                        $form_data[$key] = base64_decode($form_state->get('workflow')->getDataElement($key));
                        
                        $key = 'chapter_text';
                        $form_data[$key] = base64_decode($form_state->get('workflow')->getDataElement($key));
                        
                        $key = 'keywords';
                        $form_data[$key] = implode('; ', json_decode($form_state->get('workflow')->getDataElement($key), true));
                        
                        $form_data['author_db' ] = [];
                        $form_data['author_add'] = [];
                        
                        foreach(json_decode($form_state->get('workflow')->getDataElement('authors'), true) as $author){
                                
                                if(array_key_exists('uid', $author)){
                                        
                                        $user = new \Drupal\publisso_gold\Controller\User($author['uid']);
                                        
                                        if($user && $user->getElement('id')){
                                                
                                                $ary = [];
                                                
                                                $ary['author'] = implode(
                                                        '; ',
                                                        array_filter([
                                                                implode(
                                                                        ' ',
                                                                        array_filter([
                                                                                $user->profile->getElement('firstname'),
                                                                                $user->profile->getElement('lastname' )
                                                                        ])
                                                                ),
                                                                $user->profile->getElement('department'),
                                                                $user->profile->getElement('institute' ),
                                                                getCountry($user->profile->getElement('country'))
                                                        ])
                                                );
                                                
                                                $ary['is-corresponding' ] = $author['is_corresponding'  ];
                                                $ary['weight'           ] = $author['weight'            ];
                                                
                                                $form_data['author_db'][] = $ary;
                                        }
                                }
                                else{
                                        $form_data['author_add'][] = $author;
                                }
                        }
                        
                        $form_data['corporation'] = json_decode($form_state->get('workflow')->getDataElement('corporations'), true);
                        
                        //load simple data
                        $keys = [
                                'title', 'title_short', 'conflict_of_interest', 'conflict_of_interest_text', 'funding', 'funding_name', 'funding_id', 'reviewer_suggestion', 'accept_policy', 'references'
                        ];
                        
                        foreach($keys as $key)
                                $form_data[$key] = $form_state->get('workflow')->getDataElement($key);
                        
                        $form_state->set('form_data', $form_data);
                        $form_state->set('wf_data_loaded', true);
                }
                
                /*#############################################################################################*/
                
                /**
                 * show the form.
                 * at this point should all parameters set
                 * and only form-depending things processed
                 */
                $form['#tree'] = true;
                
                #$form['mu_comments'] = $this->getCommentsForAuthor($form_state->get('workflow')->getElement('id'));
                #$form['mu_comments'] = $form_state->get('workflow')->getRenderableTemplate('commentsforauthor_anon');
                #$form['mu_files'   ] = $this->getAssignedFiles($form_state->get('workflow')->getElement('id'));
                
                $form ['mu_site_headline'] = [
                        '#markup' => (string)$this->t('Revise Chapter'),
                        '#prefix' => '<h1>',
                        '#suffix' => '</h1>'
                ];
                
                $form['mu_book_title'] = [
                        '#markup' => $book->getElement('title'),
                        '#prefix' => '<h2>',
                        '#suffix' => '</h2>'
                ];
                
                /**
                 * form step 1
                 */
/* Step 1 */    if($form_state->get('step') == 1) {
                
                        //disclaimer
                        $form[ 'mu_initial_info' ] = [
                                '#type'     => 'inline_template',
                                '#template' => (string)$this->t(
                                        'Please read the <a href="@link_book" target="_blank">manuscript guidelines</a>, the policy
                                        (<a href="@uri_1_de" target="_blank">@text_de</a>, <a href="@uri_1_en" target="_blank">@text_en</a>)
                                        and the author\'s contract (<a href="@uri_de" target="_blank">@text_de</a>, <a href="@uri_en" target="_blank">@text_en</a>).
                                        On the second page of the submission form, you will have to accept the author\'s contract (legally binding German version) to conclude the submission.',
                                        [
                                                '@link_book' => $link_book,
                                                '@uri_1_de'  => $uri_1_de,
                                                '@text_de'   => $text_de,
                                                '@uri_1_en'  => $uri_1_en,
                                                '@text_en'   => $text_en,
                                                '@uri_de'    => $uri_de,
                                                '@uri_en'    => $uri_en
                                        ]
                                ),
                                '#suffix'   => '</div><br><br>',
                                '#prefix'   => '<div class="font-red">'
                        ];
                
                        if(!$workflow->getDataElement('predefined_title')) {
                                //chapter-title
                                $form['title'] = [
                                        '#type'        => 'textfield',
                                        '#title'       => t('Title'),
                                        '#required'    => true,
                                        '#placeholder' => (string)$this->t('Enter the title of your chapter here'),
                                        '#maxlength'   => 255
                                ];
        
                                if ($form_state->get('form_data')['title']) $form['title']['#default_value'] = $form_state->get('form_data')['title'];
        
                                $form['title_short'] = [
                                        '#type'        => 'textfield',
                                        '#title'       => t('Short-title (for PDF-Output as "Running title")'),
                                        '#required'    => true,
                                        '#placeholder' => (string)$this->t('Enter the short-title of your chapter here'),
                                        '#maxlength'   => 255,
                                        '#ajax'        => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                ];
        
                                if ($form_state->get('form_data')['title_short']) $form['title_short']['#default_value'] = $form_state->get('form_data')['title_short'];
                        }
                        else{
                                $form['title'] = [
                                        '#markup' => $workflow->getDataElement('title'),
                                        '#prefix' => '<h2>',
                                        '#suffix' => '</h2>'
                                ];
                        }
                        
                        //abstract (if required)
                        if ( $book->getControlElement( 'abstract_required' ) == 1 || $book->getControlElement( 'abstract_required' ) == 2 ):
                        
                                $form[ 'abstract' ] = [
                                        '#type'       => 'textarea',
                                        '#title'      => (string)$this->t( 'Abstract' ),
                                        '#required'   => $book->getControlElement( 'abstract_required' ) == 1 ? true : false,
                                        '#prefix'     => (string)$this->t( 'If an abstract is required or optional, please enter it here.' ),
                                        '#rows'       => 20,
                                        '#attributes' => [
                                                'class' => [
                                                        \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' )
                                                ]
                                        ]
                                ];
                        
                                if ( $form_state->get( 'form_data' )[ 'abstract' ] ) $form[ 'abstract' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'abstract' ];
                
                        endif;
                
                        //keywords
                
                        $form[ 'keywords' ] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t( 'Keywords' ),
                                '#default_value' => $form_data[ 'keywords' ] ?? '',
                                '#placeholder'   => (string)$this->t( 'Enter the keywords separated with a semicolon here' ),
                                '#maxlength'     => 400
                        ];
                
                        if ( $form_state->get( 'form_data' )[ 'keywords' ] ) $form[ 'keywords' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'keywords' ];
                
                        //chapter text
                        if(!$workflow->getDataElement('predefined_maintext')) {
                                
                                $form['chapter_text'] = [
                                        '#type'       => 'textarea',
                                        '#title'      => t('Main Text'),
                                        '#required'   => true,
                                        '#rows'       => 50,
                                        '#attributes' => [
                                                'class' => [
                                                        \Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
                                                ]
                                        ]
                                ];
        
                                if ($form_state->get('form_data')['chapter_text']) $form['chapter_text']['#default_value'] = $form_state->get('form_data')['chapter_text'];
                        }
                        else{
                                $form['chapter_text'] = [
                                        '#type' => 'fieldset',
                                        '#title' => (string)$this->t('Main text'),
                                        'main_text' => [
                                                '#type' => 'inline_template',
                                                '#template' => base64_decode($workflow->getDataElement('chapter_text'))
                                        ]
                                ];
                        }
                
                        if(!$workflow->getDataElement('predefined_maintext')) {
                                
                                $form['references'] = [
                                        '#type'   => 'textarea',
                                        '#title'  => (string)t('References'),
                                        '#rows'   => 10,
                                        '#cols'   => 60,
                                        '#prefix' => '<br>',
                                        '#id'     => 'ta-rwReferences'
                                ];
                        }
                        else{
                                $form['references'] = [
                                        '#type' => 'fieldset',
                                        '#title' => (string)$this->t('References'),
                                        'main_text' => [
                                                '#type' => 'inline_template',
                                                '#template' => nl2br($workflow->getDataElement('references'))
                                        ]
                                ];
                        }
                
                        if ( $form_state->get( 'form_data' )[ 'references' ] ) $form[ 'references' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'references' ];
                
                        if($workflow->hasDataElement('bookchapter_template')){
                        
                                $template = new BookchapterTemplate(null, $workflow->getDataElement('bookchapter_template'));
                        
                                $url = $template->getUrlFromWorkflow($workflow->getElement('id'));
                                $url->setOptions(['attributes' => ['class' => ['btn', 'btn-info']]]);
                        
                                if($url->access()){
                                        $form['edit_template'] = [
                                                '#type' => 'link',
                                                '#title' => (string)$this->t('Edit text/references'),
                                                '#url' => $url,
                                                '#suffix' => '<br>'
                                        ];
                                }
                        }
                        
                        //authors
                        if ( count( $form_state->get( 'form_data' )[ 'author_db' ] ) && $form_state->getRebuildInfo( 'step1' ) != true ) $form_state->set( 'cntAuthorsDB', count( $form_state->get( 'form_data' )[ 'author_db' ] ) );
                        if ( count( $form_state->get( 'form_data' )[ 'author_add' ] ) && $form_state->getRebuildInfo( 'step1' ) != true ) $form_state->set( 'cntAuthorsAdd', count( $form_state->get( 'form_data' )[ 'author_add' ] ) );
                        if ( !$form_state->has( 'cntAuthorsDB' ) || $form_state->get( 'cntAuthorsDB' ) < 3 ) {
                                $form_state->set( 'cntAuthorsDB', 3 );
                        }
                        if ( !$form_state->has( 'cntAuthorsAdd' ) ) {
                                $form_state->set( 'cntAuthorsAdd', 1 );
                        }
                
                        $form[ 'fs_authors' ] = [
                                '#type'   => 'fieldset',
                                '#title'  => (string)$this->t( 'Author(s)' ),
                                '#prefix' => '<br>',
                                'content' => [
                                        'description'    => [
                                                '#markup' => (string)$this->t( 'Enter one author per box. Authors who are not yet in the database can be added as additional authors.' ),
                                                '#suffix' => '<br><br>'
                                        ],
                                        'author-db'      => [
                                                '#type'   => 'container',
                                                '#prefix' => '<div id="author-db-wrapper">',
                                                '#suffix' => '</div>',
                                                '#tree'   => true,
                                                'content' => [
                                                        '#type'   => 'table',
                                                        '#header' => [
                                                                (string)$this->t( 'Author(s)' ),
                                                                (string)$this->t( 'Corresponding' ),
                                                                (string)$this->t( 'Position' )
                                                        ]
                                                ]
                                        ],
                                        'author-db-add'  => [
                                                '#type'                    => 'submit',
                                                '#button_type'             => 'warning',
                                                '#value'                   => (string)$this->t( 'add box' ),
                                                '#limit_validation_errors' => [],
                                                '#submit'                  => [ '::addAuthorDB' ],
                                                '#prefix'                  => '',
                                                '#suffix'                  => '<br><br>',
                                                '#description'             => (string)$this->t( 'Add further authors' ),
                                                '#ajax'                    => [
                                                        'wrapper'  => 'author-db-wrapper',
                                                        'effect'   => 'fade',
                                                        'callback' => '::addAuthorDBCallback'
                                                ]
                                        ],
                                        'author-add'     => [
                                                '#type'   => 'container',
                                                '#prefix' => '<div id="author-add-wrapper">',
                                                '#suffix' => '</div>',
                                                '#tree'   => true,
                                                'content' => []
                                        ],
                                        'author-add-add' => [
                                                '#type'                    => 'submit',
                                                '#button_type'             => 'warning',
                                                '#value'                   => (string)$this->t( 'add box ' ), //Space is important!
                                                '#limit_validation_errors' => [],
                                                '#submit'                  => [ '::addAuthorAdd' ],
                                                '#prefix'                  => '<br>',
                                                '#description'             => (string)$this->t( 'For more additional authors, add more boxes' ),
                                                '#ajax'                    => [
                                                        'wrapper'  => 'author-add-wrapper',
                                                        'effect'   => 'fade',
                                                        'callback' => '::addAuthorAddCallback'
                                                ]
                                        ]
                                ]
                        ];
                
                        for ( $i = 0; $i < $form_state->get( 'cntAuthorsDB' ); $i++ ) {
                        
                                $form[ 'fs_authors' ][ 'content' ][ 'author-db' ][ 'content' ][ $i ] = [
                                        'author'           => [
                                                '#type'                          => 'textfield',
                                                '#autocomplete_route_name'       => 'autocomplete.user.public',
                                                '#autocomplete_route_parameters' => [],
                                                '#title'                         => (string)$this->t( 'Author(s)' ),
                                                '#title_display'                 => 'invisible',
                                                '#placeholder'                   => (string)$this->t( 'Enter at least 3 characters of the name and choose from the database' ),
                                                '#description'                   => (string)$this->t( 'Matching entries from the database will be suggested automatically' ),
                                                '#id'                            => 'author_db_' . $i
                                        ],
                                        'is-corresponding' => [
                                                '#type' => 'checkbox'
                                        ],
                                        'weight'           => [
                                                '#type'        => 'number',
                                                '#size'        => 3,
                                                '#min'         => 1,
                                                '#max'         => 100,
                                                '#description' => (string)$this->t( 'The authors’ order can be determined here' )
                                        ]
                                ];
                        
                                if ( isset( $form_state->get( 'form_data' )[ 'author_db' ][ $i ][ 'author' ] ) ) {
                                        $form[ 'fs_authors' ][ 'content' ][ 'author-db' ][ 'content' ][ $i ][ 'author' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'author_db' ][ $i ][ 'author' ];
                                        $form[ 'fs_authors' ][ 'content' ][ 'author-db' ][ 'content' ][ $i ][ 'is-corresponding' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'author_db' ][ $i ][ 'is-corresponding' ];
                                        $form[ 'fs_authors' ][ 'content' ][ 'author-db' ][ 'content' ][ $i ][ 'weight' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'author_db' ][ $i ][ 'weight' ];
                                }
                        }
                
                        for ( $i = 0; $i < $form_state->get( 'cntAuthorsAdd' ); $i++ ) {
                        
                                $form[ 'fs_authors' ][ 'content' ][ 'author-add' ][ 'content' ][ 'author-add-' . $i ] = [
                                        '#type'   => 'fieldset',
                                        '#tree'   => true,
                                        '#title'  => (string)$this->t( 'Additional author @nr', [ '@nr' => ( $i + 1 ) ] ),
                                        'content' => [
                                                'firstname'   => [
                                                        '#type'        => 'textfield',
                                                        '#title'       => (string)$this->t( 'Firstname' ),
                                                        '#placeholder' => (string)$this->t( 'Enter the firstname of additional author #@nr', [ '@nr' => ( $i + 1 ) ] )
                                                ],
                                                'lastname'    => [
                                                        '#type'        => 'textfield',
                                                        '#title'       => (string)$this->t( 'Lastname' ),
                                                        '#placeholder' => (string)$this->t( 'Enter the lastname of additional author #@nr', [ '@nr' => ( $i + 1 ) ] )
                                                ],
                                                'affiliation' => [
                                                        '#type'        => 'textfield',
                                                        '#title'       => (string)$this->t( 'Affiliation' ),
                                                        '#placeholder' => (string)$this->t( 'Enter the affiliation of additional author #@nr', [ '@nr' => ( $i + 1 ) ] )
                                                ],
                                                'weight'      => [
                                                        '#type'  => 'number',
                                                        '#size'  => 3,
                                                        '#title' => (string)$this->t( 'Position' )
                                                ]
                                        ]
                                ];
                        
                                if ( isset( $form_state->get( 'form_data' )[ 'author_add' ][ $i ] ) ) {
                                        $form[ 'fs_authors' ][ 'content' ][ 'author-add' ][ 'content' ][ 'author-add-' . $i ][ 'content' ][ 'firstname' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'author_add' ][ $i ][ 'firstname' ];
                                        $form[ 'fs_authors' ][ 'content' ][ 'author-add' ][ 'content' ][ 'author-add-' . $i ][ 'content' ][ 'lastname' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'author_add' ][ $i ][ 'lastname' ];
                                        $form[ 'fs_authors' ][ 'content' ][ 'author-add' ][ 'content' ][ 'author-add-' . $i ][ 'content' ][ 'affiliation' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'author_add' ][ $i ][ 'affiliation' ];
                                        $form[ 'fs_authors' ][ 'content' ][ 'author-add' ][ 'content' ][ 'author-add-' . $i ][ 'content' ][ 'weight' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'author_add' ][ $i ][ 'weight' ];
                                }
                        }
                
                        //corporations
                        if ( count( $form_state->get( 'form_data' )[ 'corporation' ] ) && $form_state->getRebuildInfo( 'step1' ) != true ) $form_state->set( 'cntCorporations', count( $form_state->get( 'form_data' )[ 'corporation' ] ) );
                        if ( !$form_state->has( 'cntCorporations' ) ) {
                                $form_state->set( 'cntCorporations', 1 );
                        }
                
                        $form[ 'corporations' ] = [
                                '#type'   => 'container',
                                '#prefix' => '<div id="corporations-wrapper">',
                                '#suffix' => '</div>',
                                '#tree'   => true,
                                'content' => []
                        ];
                
                        $form[ 'corporations-add' ] = [
                                '#type'                    => 'submit',
                                '#button_type'             => 'warning',
                                '#value'                   => (string)$this->t( 'add corporation' ), //Space is important!
                                '#limit_validation_errors' => [],
                                '#submit'                  => [ '::addCorporation' ],
                                '#prefix'                  => '<br>',
                                '#description'             => (string)$this->t( 'Add more corporations' ),
                                '#ajax'                    => [
                                        'wrapper'  => 'corporations-wrapper',
                                        'effect'   => 'fade',
                                        'callback' => '::addCorporationCallback'
                                ]
                        ];
                
                        for ( $i = 0; $i < $form_state->get( 'cntCorporations' ); $i++ ) {
                        
                                $form[ 'corporations' ][ 'content' ][ $i ] = [
                                        '#type'          => 'textfield',
                                        '#title'         => (string)$this->t( 'Corporation(s)' ),
                                        '#title_display' => $i ? 'invisible' : 'before',
                                        '#placeholder'   => (string)$this->t( 'Enter corporations that should appear as authors here' ),
                                        '#description'   => (string)$this->t( 'One corporation per box' )
                                ];
                        
                                if ( isset( $form_state->get( 'form_data' )[ 'corporation' ][ $i ] ) ) $form[ 'corporations' ][ 'content' ][ $i ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'corporation' ][ $i ];
                        }
                
                        $form[ 'comment' ] = [
                                '#type'         => 'textarea',
                                '#title'        => (string)$this->t( 'Comment for @role', [ '@role' => Publisso::text()->get( 'global.role.' . explode( ':', $form_state->get( 'new_schema_role' ) )[ 1 ] ) ] ),
                                '#prefix'       => '<br><br>',
                                '#required'     => true
                        ];
                
                        if ( $form_state->get( 'form_data' )[ 'comment' ] ?? false ) $form[ 'comment' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'comment' ];
                }
                /* Step 2 */    elseif($form_state->get('step') == 2) {
                
                        //conflict of interest
                        $form[ 'conflict_of_interest' ] = [
                                '#type'   => 'checkbox',
                                '#title'  => (string)t( 'Conflict of interest' ),
                                '#prefix' => (string)$this->t( 'Here you may add if there’s any conflict of interest to declare, which might be relevant regarding the development of your chapter. If there’s anything to declare you can add a description in the appearing box.' )
                        ];
                
                        if ( isset( $form_state->get( 'form_data' )[ 'conflict_of_interest' ] ) ) $form[ 'conflict_of_interest' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'conflict_of_interest' ];
                
                        $form[ 'conflict_of_interest_text' ] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t( 'Description of conflict of interest' ),
                                '#maxlength'     => 256,
                                '#default_value' => isset( $workflow_data ) ? $workflow_data->conflict_of_interest_text : '',
                                '#states'        => [
                                        'visible'   => [   // action to take.
                                                           ':input[name="conflict_of_interest"]' => [ 'checked' => true ],
                                        ],
                                        'invisible' => [   // action to take.
                                                           ':input[name="conflict_of_interest"]' => [ 'checked' => false ],
                                        ]
                                ]
                        ];
                
                        if ( isset( $form_state->get( 'form_data' )[ 'conflict_of_interest_text' ] ) ) $form[ 'conflict_of_interest_text' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'conflict_of_interest_text' ];
                
                        //funding
                        $form[ 'funding' ] = [
                                '#type'   => 'checkbox',
                                '#title'  => t( 'Funds received' ),
                                '#prefix' => (string)$this->t( 'If you received any funds please add the information here.' )
                        ];
                
                        if ( isset( $form_state->get( 'form_data' )[ 'funding' ] ) ) $form[ 'funding' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'funding' ];
                
                        $form[ 'funding_name' ] = [
                                '#type'      => 'textfield',
                                '#title'     => (string)$this->t( 'Funding name' ),
                                '#maxlength' => 256,
                                '#states'    => [
                                        'visible'   => [   // action to take.
                                                           ':input[name="funding"]' => [ 'checked' => true ],
                                        ],
                                        'invisible' => [   // action to take.
                                                           ':input[name="funding"]' => [ 'checked' => false ],
                                        ]
                                ],
                        ];
                
                        if ( isset( $form_state->get( 'form_data' )[ 'funding_name' ] ) ) $form[ 'funding_name' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'funding_name' ];
                
                        $form[ 'funding_id' ] = [
                                '#type'   => 'textfield',
                                '#title'  => (string)t( 'Funding ID' ),
                                '#states' => [
                                        'visible'   => [   // action to take.
                                                           ':input[name="funding"]' => [ 'checked' => true ],
                                        ],
                                        'invisible' => [   // action to take.
                                                           ':input[name="funding"]' => [ 'checked' => false ],
                                        ]
                                ],
                        ];
                
                        if ( isset( $form_state->get( 'form_data' )[ 'funding_id' ] ) ) $form[ 'funding_id' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'funding_id' ];
                
                        //reviewer suggestion
                        if ( $book->getControlElement( 'reviewer_suggestion' ) == 1 ):
                        
                                $form[ 'reviewer_suggestion' ] = [
                                        '#type'        => 'textfield',
                                        '#title'       => t( 'Reviewer suggestion' ),
                                        '#placeholder' => (string)$this->t( 'Enter the names of potential reviewers of your chapter here' )
                                ];
                        
                                if ( isset( $form_state->get( 'form_data' )[ 'reviewer_suggestion' ] ) ) $form[ 'reviewer_suggestion' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'reviewer_suggestion' ];
                        endif;
                
                        //additional files
                        $form[ 'add_files' ] = [
                                '#type'  => 'checkbox',
                                '#title' => (string)$this->t( 'I would like to upload (additional) files' )
                        ];
                
                        if ( isset( $form_state->get( 'form_data' )[ 'add_files' ] ) ) $form[ 'add_files' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'add_files' ];
                
                        //accept policy
                        $form[ 'accept_policy' ] = [
                                '#type'     => 'checkbox',
                                '#prefix'   => (string)$this->t( 'Please read the policy (<a href="' . $uri_1_de . '" target="_blank">' . $text_de . '</a>, <a href="' . $uri_1_en . '" target="_blank">' . $text_en . '</a>) and accept the author\'s contract (legally binding <a href="' . $uri_de . '" target="_blank">German version</a>, <a href="' . $uri_en . '" target="_blank">translated version</a>) to conclude the submission.' ),
                                '#title'    => (string)t( 'I accept the author\'s contract</a>' ),
                                '#required' => true,
                                '#disabled' => true
                        ];
                
                        if ( isset( $form_state->get( 'form_data' )[ 'accept_policy' ] ) ) $form[ 'accept_policy' ][ '#default_value' ] = $form_state->get( 'form_data' )[ 'accept_policy' ];
                        true;
                }
                /* Step 3 */
                elseif ( $form_state->get( 'step' ) == 3 ) {
        
                        $form[ 'description' ] = [
                                '#markup' => (string)$this->t( 'Here you can upload additional Files to your submission' ),
                                '#prefix' => '<h3>',
                                '#suffix' => '</h3>'
                        ];
        
                        $form[ 'files_fieldset' ] = [
                                '#type'   => 'fieldset',
                                '#title'  => (string)$this->t( 'Upload additional Files' ),
                                '#prefix' => '<div id="files-fieldset-wrapper">',
                                '#suffix' => '</div>',
                        ];
        
                        if ( !$form_state->has( 'numFiles' ) ) $form_state->set( 'numFiles', 1 );
                        $numFiles = $form_state->get( 'numFiles' );
        
                        for ( $i = 0; $i < $numFiles; $i++ ) {
                
                                $form[ 'files_fieldset' ][ 'file' ][ $i ] = [
                        
                                        '#type' => 'fieldset',
                                        'file'  => [
                                                '#type'               => 'managed_file',
                                                '#title'              => (string)$this->t( 'File' ) . ' #' . $i,
                                                '#required'           => true,
                                                '#upload_validators'  => [
                                                        'file_validate_extensions' => $setup->getValue( 'file.upload.valid_extensions' ) ? [ $setup->getValue( 'file.upload.valid_extensions' ) ] : [ 'gif png jpg jpeg doc docx xls xlsx midi mp4 mp3 avi mpg mpeg wav mov mid odt ods odp pdf' ],
                                                        // Pass the maximum file size in bytes
                                                        'file_validate_size'       => [ 30 * 1024 * 1024 ],
                                                ],
                                                '#progress_indicator' => 'bar',
                                                '#progress_message'   => (string)$this->t( 'Uploading file...' )
                                        ],
                                        'meta'  => [
                                                'name'        => [
                                                        '#type'     => 'textfield',
                                                        '#title'    => (string)$this->t( 'Filename' ),
                                                        '#required' => true
                                                ],
                                                'description' => [
                                                        '#type'  => 'textfield',
                                                        '#title' => (string)$this->t( 'Description' )
                                                ]
                                        ]
                                ];
                        }
        
                        $form[ 'files_fieldset' ][ 'actions' ][ 'add_name' ] = [
                                '#type'   => 'submit',
                                '#value'  => t( 'Add file' ),
                                '#submit' => [ '::addOne' ],
                                '#prefix' => '<br>',
                                '#ajax'   => [
                                        'callback' => '::addmoreCallback',
                                        'wrapper'  => 'files-fieldset-wrapper',
                                        'effect'   => 'fade',
                                        'progress' => [
                                                'type' => 'none'
                                        ]
                                ],
                        ];
        
                        if ( $numFiles > 1 ) {
                
                                $form[ 'files_fieldset' ][ 'actions' ][ 'remove_file' ] = [
                                        '#type'                    => 'submit',
                                        '#value'                   => t( 'Remove last' ),
                                        '#submit'                  => [ '::removeCallback' ],
                                        '#prefix'                  => '&nbsp;&nbsp;&nbsp;',
                                        '#ajax'                    => [
                                                'callback' => '::addmoreCallback',
                                                'wrapper'  => 'files-fieldset-wrapper',
                                                'effect'   => 'fade',
                                                'progress' => [
                                                        'type' => 'none'
                                                ]
                                        ],
                                        '#limit_validation_errors' => []
                                ];
                        }
                }
        
                /*#############################################################################################*/
                
                $form['spacer'] = [
                        '#markup' => '<hr>'
                ];
                
                $form['close'] = [
                        '#type'                         => 'submit',
                        '#default_value'                => (string)$this->t('Back to dashboard'),
                        '#button_type'                  => 'warning',
                        '#suffix'                       => '&nbsp;&nbsp;&nbsp;',
                        '#limit_validation_errors'      => [],
                        '#submit'                       => ['::backToBook']
                ];
                
                $form['preview'] = [
                        '#type'                         => 'button',
                        '#value'                        => (string)$this->t('Preview'),
                        '#button_type'                  => 'primary',
                        '#suffix'                       => '&nbsp;&nbsp;&nbsp;',
                        '#limit_validation_errors'      => [],
                        '#description'                  => (string)$this->t('See how your chapter looks like'),
                        '#ajax'                         => [
                                'callback' => '::previewBookchapter',
                                'progress' => [
                                        'type' => 'none'
                                ]
                        ]
                ];
                
                $form['back'] = [
                        '#type'                    => 'submit',
                        '#value'                   => (string)$this->t('Back'),
                        '#button_type'             => 'danger',
                        '#suffix'                  => '&nbsp;&nbsp;&nbsp;',
                        '#submit'                  => ['::stepBack'],
                        '#access'                  => $form_state->get('step') != 1,
                        '#limit_validation_errors' => []
                ];
                
                $form['submit_1'] = [
                        '#type'         => 'submit',
                        '#value'        => (string)$this->t('Continue revision'),
                        '#button_type'  => 'success',
                        '#access'       => $form_state->get('step') == 1,
                        '#submit'       => ['::processForm']
                ];
                
                $form['submit_2'] = [
                        '#type'         => 'submit',
                        '#value'        => (string)$this->t('Finish revision'),
                        '#button_type'  => 'success',
                        '#access'       => $form_state->get('step') == 2,
                        '#submit'       => ['::processForm'],
                        '#attributes'   => [
                                'onclick' => 'if(!confirm("'.t('When you conclude the submission, it will enter the review process. You won’t be able to edit the submission until a possible revision.\n\nOK: Finish submission\nCancel: Continue submission').'")){return false;}'
                        ],
                        '#states'       => [
                                'visible'   => [   // action to take.
                                        ':input[name="add_files"]' => array('checked' => false),
                                ],
                                'invisible' => [   // action to take.
                                        ':input[name="add_files"]' => array('checked' => true),
                                ]
                        ]
                ];
                
                $form['submit_3'] = [
                        '#type'         => 'submit',
                        '#button_type'  => 'success',
                        '#value'        => (string)$this->t('Continue revision'),
                        '#submit'       => ['::processForm'],
                        '#access'       => $form_state->get('step') == 2,
                        '#states'       => array(
                                'visible'   => [// action to take.
                                                        ':input[name="add_files"]' => array('checked' => true),
                                ],
                                'invisible' => [// action to take.
                                                        ':input[name="add_files"]' => array('checked' => false),
                                ]
                        )
                ];
                
                $form['submit_4'] = [
                        '#type'         => 'submit',
                        '#value'        => (string)$this->t('Finish revision'),
                        '#button_type'  => 'success',
                        '#access'       => $form_state->get('step') == 3,
                        '#submit'       => ['::processForm'],
                        '#attributes'   => [
                                'onclick' => 'if(!confirm("'.t('When you conclude the submission, it will enter the review process. You won’t be able to edit the submission until a possible revision.\n\nOK: Finish submission\nCancel: Continue submission').'")){return false;}'
                        ]
                ];
                
                $form['store_data_control_key'] = [
                        '#type'         => 'hidden',
                        '#value'        => 'previewchapter'
                ];
                
                $form['ctrl_action'] = [
                        '#type' => 'hidden',
                        '#value' => 'autosave'
                ];
                
                $form['ctrl_type'] = [
                        '#type' => 'hidden',
                        '#value' => 'bookchapter'
                ];
                
                $form['tmp_id'] = [
                        '#type' => 'hidden',
                        '#id' => 'tmp_id'
                ];
                
                if(!empty($form_state->get('form_data')['tmp_id'])){
                        $form['tmp_id']['#value'] = $form_state->get('form_data')['tmp_id'];
                }
                
                $form['bk_id'] = [
                        '#type' => 'hidden',
                        '#value' => $form_state->get('book')->getElement('id')
                ];
                
                $form['#cache'] = [
                        'max-age' => 0
                ];
                
                $form_state->setCached(false);
                
                return $form;
        }
        
        /**
         * {@inheritdoc}
         */
        public function validateForm(array &$form, FormStateInterface $form_state){
                
                switch($form_state->get('step')){
                        case 1:
                                $this->validateStep1($form, $form_state);
                                break;
                        case 2:
                                break;
                        case 3:
                                break;
                }
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        protected function validateStep1(array &$form, FormStateInterface $form_state){
                
                $i = 0;
                foreach($form_state->getValue('fs_authors')['content']['author-db']['content'] as $_){

                        [$author, $data] = preg_split('/;\s{1,}/', $_['author'], 2);
                        
                        if(!empty($author)){
                                $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                $found = 0;
                                $last_id = null;
                                $string = '';

                                foreach($res as $v){
                                        $string .= ' || '.implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, Publisso::tools()->getCountry($v->up_country)])).' | '.implode( '; ', [$author, $data]);
                                        if(
                                                implode( '; ', [$author, $data])
                                                                ==
                                                implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, Publisso::tools()->getCountry($v->up_country)]))
                                        ){
                                                $found++;
                                                $last_id = $v->up_uid;
                                        }
                                }

                                if($found != 1){
                                        $form_state->setError($form, (string)$this->t('Please use only suggested values in author-field #@i!', ['@i' => $i + 1]));
                                }
                        }
                        $i++;
                }
                return $form;
        }
        
        /**
         * {@inheritdoc}
         */
        public function submitForm(array &$form, FormStateInterface $form_state){
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function processForm(array &$form, FormStateInterface $form_state){
                
                if(empty($form_state->get('form_data')['tmp_id']) && !empty($form_state->getValue('tmp_id'))){
                        $form_data = $form_state->get('form_data');
                        $form_data['tmp_id'] = $form_state->getValue('tmp_id');
                        $form_state->set('form_data', $form_data);
                }
                
                switch($form_state->get('step')){
                        case 1:
                                $this->processStep1($form, $form_state);

                                //proceed to step 2
                                $form_state->set('step', 2);
                                $form_state->setRebuild();
                                break;
                        case 2:
                                $this->processStep2($form, $form_state);
                                
                                if($form_state->getValue('add_files') == 1){
                                        $form_state->set('step', 3);
                                        $form_state->setRebuild();
                                }
                                else{
                                        $this->finishRevision($form, $form_state);
                                }
                                break;
                        case 3:
                                $this->processStep3($form, $form_state);
                                $this->finishRevision($form, $form_state);
                                break;
                }
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function processStep1(array &$form, FormStateInterface $form_state){
                
                //init temp sorage if not done
                if(!$form_state->has('form_data')) $form_state->set('form_data', []);
                
                /**
                 * store formdata in a temporary storage
                 */
                $form_data = $form_state->get('form_data');
        
                if($form_state->hasValue('title'        )) $form_data['title'           ] = $form_state->getValue('title'       );
                if($form_state->hasValue('title_short'  )) $form_data['title_short'     ] = $form_state->getValue('title_short' );
                if($form_state->hasValue('keywords'     )) $form_data['keywords'        ] = $form_state->getValue('keywords'    );
                if($form_state->hasValue('chapter_text' )) $form_data['chapter_text'    ] = $form_state->getValue('chapter_text');
                if($form_state->hasValue('references'   )) $form_data['references'      ] = $form_state->getValue('references'  );
                $form_data['comment'] = $form_state->getValue('comment');
                
                if($form_state->hasValue('abstract'))
                        $form_data['abstract'] = $form_state->getValue('abstract');
                
                //authors in db
                $form_data['author_db'] = [];
                
                foreach($form_state->getValue('fs_authors')['content']['author-db']['content'] as $authorDB){
                        if(!empty($authorDB['author'])) $form_data['author_db'][] = $authorDB;
                }
                
                //additional authors
                $form_data['author_add'] = [];
                foreach($form_state->getValue('fs_authors')['content']['author-add']['content'] as $authorAdd){
                        if(!empty($authorAdd['content']['firstname'  ]) &&
                           !empty($authorAdd['content']['lastname'   ]) &&
                           !empty($authorAdd['content']['affiliation'])){
                                $form_data['author_add'][] = $authorAdd['content'];
                        }
                }
                
                //corporations
                $form_data['corporation'] = [];
                foreach($form_state->getValue('corporations')['content'] as $corporation){
                        if(!empty($corporation)) $form_data['corporation'][] = $corporation;
                }
                Publisso::log(print_r($form_state->getValues(), 1));
                
                //store $form_data
                $form_state->set('form_data', $form_data);
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function processUserInputStep1(array &$form, FormStateInterface $form_state){
                
                //init temp sorage if not done
                if(!$form_state->has('form_data')) $form_state->set('form_data', []);
                
                /**
                 * store formdata in a temporary storage
                 */
                $form_data = $form_state->get('form_data');
                $form_raw  = $form_state->getUserInput();
                
                $form_data['title'] = $form_raw['title'];
                $form_data['keywords'] = $form_raw['keywords'];
                $form_data['chapter_text'] = $form_raw['chapter_text'];
                $form_data['references'] = $form_raw['references'];
                
                if(array_key_exists('abstract', $form_raw))
                        $form_data['abstract'] = $form_raw['abstract'];
                
                //authors in db
                $form_data['author_db'] = [];
                
                foreach($form_raw['fs_authors']['content']['author-db']['content'] as $authorDB){
                        if(!empty($authorDB['author'])) $form_data['author_db'][] = $authorDB;
                }
                
                //additional authors
                $form_data['author_add'] = [];
                foreach($form_raw['fs_authors']['content']['author-add']['content'] as $authorAdd){
                        if(!empty($authorAdd['content']['firstname'  ]) &&
                           !empty($authorAdd['content']['lastname'   ]) &&
                           !empty($authorAdd['content']['affiliation'])){
                                $form_data['author_add'][] = $authorAdd['content'];
                        }
                }
                
                //corporations
                $form_data['corporation'] = [];
                foreach($form_raw['corporations']['content'] as $corporation){
                        if(!empty($corporation)) $form_data['corporation'][] = $corporation;
                }
                
                //store $form_data
                $form_state->set('form_data', $form_data);
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        protected function processStep2(array &$form, FormStateInterface $form_state){
                
                $form_data = $form_state->get('form_data');
                
                $keys = [
                        'conflict_of_interest'     ,
                        'conflict_of_interest_text',
                        'funding'                  ,
                        'funding_name'             ,
                        'funding_id'               ,
                        'reviewer_suggestion'      ,
                        'accept_policy'            ,
                        'add_files'
                ];
                
                foreach($keys as $key){
                        if($form_state->hasValue($key)) $form_data[$key] = $form_state->getValue($key);
                }
                
                $form_state->set('form_data', $form_data);
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        protected function processUserInputStep2(array &$form, FormStateInterface $form_state){
                
                $form_data = $form_state->get('form_data');
                $form_raw  = $form_state->getUserInput();
                
                $keys = [
                        'conflict_of_interest'     ,
                        'conflict_of_interest_text',
                        'funding'                  ,
                        'funding_name'             ,
                        'funding_id'               ,
                        'reviewer_suggestion'      ,
                        'accept_policy'            ,
                        'add_files'
                ];
                
                foreach($keys as $key){
                        if(array_key_exists($key, $form_raw)) $form_data[$key] = $form_raw[$key];
                }
                
                $form_state->set('form_data', $form_data);
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        protected function processStep3(array &$form, FormStateInterface $form_state){
                
                //Step 3 is actually the last - will be processed in "::finishSubmission"
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function stepBack(array &$form, FormStateInterface $form_state){
                /*
                switch($form_state->get('step')){
                        
                        case 2:
                                $this->processStep2($form, $form_state);
                                break;
                        case 3:
                                $this->processStep3($form, $form_state);
                                break;
                }
                */
                $form_state->set('step', ((int)$form_state->get('step')) - 1);
                $form_state->setRebuildInfo([]);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        protected function finishRevision(array &$form, FormStateInterface $form_state){
                
                //get the book
                $book = $form_state->get('book');
                
                //get stored form-data
                $form_data = $form_state->get('form_data');
                
                //get the workflow
                $workflow = $form_state->get('workflow');
                $workflow->reload();
        
                if(!!$form_data['comment']){
                        Comment::new( $workflow->getElement('id'), $workflow->getElement( 'schema_identifier' ), $form_data['comment'], null, null, $workflow->getSchemaCurrentRole(), 'submitting_author' );
                }
                
                //set the data
                $this->storeFormData2Workflow($workflow, $form_state);
                $workflow->setDataElement('revised', date('Y-m-d H:i:s'));
                
                if(!$workflow->newSchemaState($form_state->get('new_schema_identifier'))){
                        return $form;
                }
                $workflow->save();
                $workflow->unlock();
        
                $addParams = [];
                if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                        $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                        \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                }
        
                $form_state->setRedirect('publisso_gold.dashboard', $addParams, ['fragment' => 'rwDashboardItem-'.$workflow->getElement('id')]);
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthorDB(array &$form, FormStateInterface $form_state){
                $cntAuthorsDB = $form_state->get('cntAuthorsDB');
                $cntAuthorsDB++;
                $form_state->set('cntAuthorsDB', $cntAuthorsDB);
                $form_state->addRebuildInfo('step1', true);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorDBCallback(array &$form, FormStateInterface $form_state){
                return $form['fs_authors']['content']['author-db'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthorAdd(array &$form, FormStateInterface $form_state){
                $cntAuthorsAdd = $form_state->get('cntAuthorsAdd');
                $cntAuthorsAdd++;
                $form_state->set('cntAuthorsAdd', $cntAuthorsAdd);
                $form_state->addRebuildInfo('step1', true);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorAddCallback(array &$form, FormStateInterface $form_state){
                return $form['fs_authors']['content']['author-add'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addCorporation(array &$form, FormStateInterface $form_state){
                $cntCorporations = $form_state->get('cntCorporations');
                $cntCorporations++;
                $form_state->set('cntCorporations', $cntCorporations);
                $form_state->addRebuildInfo('step1', true);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addCorporationCallback(array &$form, FormStateInterface $form_state){
                error_log('Calling "addCorporationCallback"...');
                return $form['corporations'];
        }
        
        /**
         * @param $bk_id
         * @return array
         */
        protected function getBook($bk_id){
                
                if(empty($bk_id)){
                        return [
                                'msg' => (string)$this->t('Initiate submission without book! Abort!'),
                                'book' => false
                        ];
                }
                
                //try to load book. if fails, throw an error!
                $book = new \Drupal\publisso_gold\Controller\Book($bk_id);
                
                if(!count($book->getKeys())){
                        return [
                                'msg' => (string)$this->t('Book (@bk_id) not found! Abort!', ['@bk_id' => $bk_id]),
                                'book' => false
                        ];
                }
                
                return [
                        'msg' => 'OK',
                        'book' => $book
                ];
        }
        
        /**
         * @param $wf_id
         * @return array
         */
        protected function getWorkflow($wf_id){
                
                if(empty($wf_id)){
                        return [
                                'msg' => (string)$this->t('Initiate revision without workflow! Abort!'),
                                'workflow' => false
                        ];
                }
                
                //try to load workflow. if fails, throw an error!
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
                
                if(!count($workflow->getKeys())){
                        return [
                                'msg' => (string)$this->t('Workflow (@wf_id) not found! Abort!', ['@wf_id' => $wf_id]),
                                'workflow' => false
                        ];
                }
                
                return [
                        'msg' => 'OK',
                        'workflow' => $workflow
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addOne(array &$form, FormStateInterface $form_state) {
                
                $numFiles = $form_state->get('numFiles');
                $form_state->set('numFiles', ++$numFiles);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addmoreCallback(array &$form, FormStateInterface $form_state) {
                return $form['files_fieldset'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function removeCallback(array &$form, FormStateInterface $form_state) {
                
                $numFiles = $form_state->get('numFiles');
                if($numFiles > 1) $form_state->set('numFiles', --$numFiles);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @return AjaxResponse
         */
        public function delFile(array &$form, FormStateInterface $form_state, $args = []) {
                
                $fid = $form_state->getTriggeringElement()['fid'];
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $response = new AjaxResponse();
                
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                $fids = array_diff($fids, [$fid]);
                $workflow->setDataElement('files', serialize($fids));
                
                //check if file are deleted
                //reload Workflow
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                
                if(array_search($fid, $fids)){
                        
                        $file = new \Drupal\publisso_gold\Controller\Blob($fid);
                        $response->addCommand(new AppendCommand('div#rwBlob-'.$fid, '<div class="rwError">'.((string)$this->t('Error deleting file "'.$file->meta['name'].'"')).'</div>'));
                }
                else{
                        $response->addCommand(new RemoveCommand('div#rwBlob-'.$fid));
                }
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return AjaxResponse
         * @throws \Exception
         */
        public function previewBookchapter(array &$form, FormStateInterface $form_state){
                
                switch($form_state->get('step')){
                        case 1:
                                $this->processStep1($form, $form_state);
                                break;
                        case 2:
                                $this->processStep2($form, $form_state);
                                break;
                        case 3:
                                $this->processStep3($form, $form_state);
                                break;
                }
        
                $workflow = $form_state->get('workflow');
                $this->storeFormData2Workflow($workflow, $form_state);
                
                $workflow->publish(false, true);
                $form_state->set('workflow', $workflow);
                $chapter = ChapterManager::getChapter($workflow->getDataElement('cp_id'));
                $chapter->setElement('citation_note', null);
                
                
                //abstract
                $chapter->setElement('abstract', base64_encode($chapter->getElement('abstract')));
                
                //chapter_text
                #$chapter->setElement('chapter_text', base64_encode($chapter->getElement('chapter_text')), false);
                
                //keywords
                $keywords = array_filter(array_map('trim', preg_split('/[,;]/', $workflow->getDataElement('keywords'))));
                $chapter->setElement('keywords', json_encode($keywords), false);
                
                //authors
                $authors = [];
                #Publisso::log(print_r($chapter->getElement('authors'), 1));
                
                $chapter->save();
                
                $response = new AjaxResponse();
                
                $command = new InvokeCommand(
                        'html',
                        'trigger',
                        [
                                'wopen',
                                [
                                        'url' => $chapter->getLink()
                                ]
                        ]
                );
                
                $response->addCommand($command);
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @throws \Exception
         */
        public function backToBook(array &$form, FormStateInterface $form_state){
                
                $workflow = $form_state->get('workflow');
                $workflow->reload();
                
                $call = "processUserInputStep".$form_state->get('step');
                if(method_exists($this, $call)) $this->$call($form, $form_state);
                
                $this->storeFormData2Workflow($workflow, $form_state);
                
                $workflow->unlock();
                $form_state->setRedirect('publisso_gold.dashboard');
        }
        
        /**
         * @param $workflow
         * @param FormStateInterface $form_state
         * @throws \Exception
         */
        protected function storeFormData2Workflow(&$workflow, FormStateInterface $form_state){
                
                $session = \Drupal::service('session');
                $form_data = $form_state->get('form_data');
                
                //title
                $workflow->setDataElement('title', $form_data['title'], false);
                
                //abstract
                $element                = 'abstract';
                $$element               = $form_data[$element];
                $$element               = \Drupal::service('publisso_gold.tools')->normalizeInlineImageLinks($$element);
                $workflow->setDataElement($element, base64_encode($$element), false);

                //chapter_text
                $element                = 'chapter_text';
                $$element               = $form_data[$element];
                $$element               = \Drupal::service('publisso_gold.tools')->normalizeInlineImageLinks($$element);
                $workflow->setDataElement($element, base64_encode($$element), false);
                
                //keywords
                $keywords = array_filter(array_map('trim', explode(';', $form_data['keywords'])));
                $workflow->setDataElement('keywords', json_encode($keywords), false);
                
                //corporations
                $workflow->setDataElement('corporations', json_encode(array_filter(array_map('trim', $form_data['corporation']))), false);
                
                //conflict of interest
                $workflow->setDataElement('conflict_of_interest'     , $form_data['conflict_of_interest'     ], false);
                $workflow->setDataElement('conflict_of_interest_text', $form_data['conflict_of_interest_text'], false);
                
                //funding
                $workflow->setDataElement('funding'     , $form_data['funding'     ], false);
                $workflow->setDataElement('funding_name', $form_data['funding_name'], false);
                $workflow->setDataElement('funding_id'  , $form_data['funding_id'  ], false);
                
                //suggested reviewers
                $workflow->setDataElement('reviewer_suggestion', $form_data['reviewer_suggestion'], false);
                
                //accepted policy
                $workflow->setDataElement('accept_policy', $form_data['accept_policy'], false);
                
                //references
                $workflow->setDataElement('references', $form_data['references'], false);
                
                //authors
                $authors = [];
                
                foreach($form_data['author_db'] as $item){
                        
                        $author           = $item['author'];
                        $is_corresponding = $item['is-corresponding'];
                        $weight           = $item['weight'];
                        
                        [$author, $data] = preg_split('/;\s{1,}/', $author, 2);
                        if(!empty($author)){
                                $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                $found = 0;
                                $last_id = null;
                                $string = '';

                                foreach($res as $v){
                                        $string .= ' || '.implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, \Drupal::service('publisso_gold.tools')->getCountry($v->up_country)])).' | '.implode( '; ', [$author, $data]);
                                        if(
                                                implode( '; ', [$author, $data])
                                                                ==
                                                implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, \Drupal::service('publisso_gold.tools')->getCountry($v->up_country)]))
                                        ){
                                                $found++;
                                                $last_id = $v->up_uid;
                                        }
                                }

                                if($found == 1){
                                        
                                        $user = new \Drupal\publisso_gold\Controller\User($last_id);
                                        
                                        $authors[] = [
                                                'weight'             => $weight,
                                                'is_corresponding'   => $is_corresponding,
                                                'firstname'          => $user->profile->getElement('firstname'),
                                                'lastname'           => $user->profile->getElement('lastname'),
                                                'affiliation'        => $user->profile->getElement('institute'),
                                                'uid'                => $user->getElement('id'),
                                                'profile_archive_id' => $user->profile->makeSnapshot($session->get('user')['id'])
                                        ];
                                }
                        }
                }
                
                foreach($form_data['author_add'] as $item){
                        $authors[] = array_map('trim', $item);
                }
                
                $workflow->setDataElement('authors', json_encode($authors), false);
                
                //files
                $blobIds = [];
                $wfBlobs = unserialize($workflow->getDataElement('files'));
                
                if(!is_array($wfBlobs)) $wfBlobs = [];
                
                if($form_state->hasValue('files_fieldset')){
                        
                        foreach($form_state->getValue('files_fieldset')['file'] as $file){

                                if(count($file['file'])){

                                        $file_id = $file['file'][0];
                                        $file_meta = serialize($file['meta']);
                                        $file_db = \Drupal::database()->select('file_managed', 't')->fields('t', [])->condition('fid', $file_id, '=')->execute()->fetchAssoc();
                                        $file_type = $file_db['filemime'];
                                        $file_path = $file_db['uri'];

                                        if(preg_match('/^temporary:\/\/(.+)/', $file_path, $matches)){
                                                $file_content = file_get_contents('/tmp/'.$matches[1]);
                                        }

                                        if($file_type && $file_content){

                                                $blob = new \Drupal\publisso_gold\Controller\Blob();
                                                $blob->create($file_content, $file_type, $file_meta);
                                                $blobIds[] = $blob->getId();
                                        }
                                }
                        }
                }
                
                $workflow->setDataElement('files', serialize(array_unique(array_merge($wfBlobs, $blobIds))), false);
                $workflow->setElement('modified', date('Y-m-d H:i:s'), false);
                $workflow->save();
        }
        
        /**
         * @param $wf_id
         * @return array
         */
        protected function getCommentsForAuthor($wf_id){
                
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
                $renderComments = [];
                $c = 0;
                
                foreach($workflow->getCommentsForAuthor() as $_){

                        if($c === 0){
                                $renderComments = [
                                        '#type' => 'details',
                                        '#open' => false,
                                        '#title' => (string)t('Comment(s) for author'),
                                        'content' => []
                                ];
                        }

                        $_->cfa_comment = base64_decode($_->cfa_comment);

                        $renderComments['content'][] = [
                                '#type' => 'details',
                                '#open' => false,
                                '#title' => (string)t('Comment').' #' . ($c + 1),
                                'content' => [
                                        '#type' => 'markup',
                                        '#markup' => $_->cfa_comment
                                ]
                        ];

                        $c++;
                }
                
                return $renderComments;
        }
        
        /**
         * @param $wf_id
         * @return array
         */
        protected function getAssignedFiles($wf_id){
                
                $files_tmpl = [];
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
                $fids = $workflow->getDataElement('files');
                if($fids) $fids = unserialize($fids);
                
                if(is_array($fids)){
                        
                        foreach($fids as $fid){
                                
                                $blob = new \Drupal\publisso_gold\Controller\Blob($fid);
                                
                                $files_tmpl[] = [
                                        '#type' => 'fieldset',
                                        '#id' => 'rwBlob-'.$fid,
                                        'content' => [
                                                '#type' => 'link',
                                                '#title' => $blob->meta['name'],
                                                '#url' => Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()]),
                                                '#suffix' => '<br><i>'.$blob->meta['description'].'</i>'
                                        ],
                                        'preview' => preg_match('/^(image|video|audio)\//', $blob->type, $matches) ? [
                                                '#type' => 'inline_template',
                                                '#template' => (
                                                        $matches[1] == 'image' ? '<br><img height="100" src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'">' : (
                                                                $matches[1] == 'video' ? '<br><video height="100" controls><source src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'" type="'.$blob->type.'"></video>' : (
                                                                        $matches[1] == 'audio' ? '<br><audio controls><source src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'" type="'.$blob->type.'"></audio>' : (
                                                                                ''
                                                                        )
                                                                )
                                                        )
                                                )
                                        ] : '',
                                        'action#'.$fid => [
                                                '#type' => 'button',
                                                '#value' => (string)$this->t('Delete File \''.$blob->meta['name'].'\''),
                                                '#prefix' => '<br><br>',
                                                'fid' => $fid,
                                                '#ajax' => [
                                                        'callback' => '::delWorkflowFile',
                                                        'wrapper' => 'rwBlob-'.$fid,
                                                        'effect' => 'slide'
                                                ],
                                                'limit_validation_errors' => []
                                        ]
                                ];
                        }
                }
                
                if(count($files_tmpl)){
                        return [
                            '#type' => 'details',
                            '#title' => t('Assigned files'),
                            '#open' => false,
                            'content' => $files_tmpl,
                            '#prefix' => '<br>'
                        ];
                }
                else{
                        return [];
                }
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @return AjaxResponse
         */
        public function delWorkflowFile(array &$form, FormStateInterface $form_state, $args = []) {
                
                $fid = $form_state->getTriggeringElement()['fid'];
                $workflow = $form_state->get('workflow');
                $response = new AjaxResponse();
                
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                $fids = array_diff($fids, [$fid]);
                $workflow->setDataElement('files', serialize($fids));
                
                //check if file are deleted
                //reload Workflow
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                
                if(array_search($fid, $fids)){
                        
                        $file = new \Drupal\publisso_gold\Controller\Blob($fid);
                        $response->addCommand(new AppendCommand('div#rwBlob-'.$fid, '<div class="rwError">'.((string)$this->t('Error deleting file "'.$file->meta['name'].'"')).'</div>'));
                }
                else{
                        $response->addCommand(new RemoveCommand('div#rwBlob-'.$fid));
                }
                
                $form_state->setRebuild();
                return $response;
        }
}
