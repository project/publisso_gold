<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\Workflow\Checkin.
         */
        namespace Drupal\publisso_gold\Form\Workflow;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Provides a simple example form.
         */
        class Checkin extends FormBase {
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId() {
                        return 'workflow.checkin';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                        
                        if(array_key_exists('wf_id', $args)){
                                
                                $form_state->set('wf_id', $args['wf_id']);
                                
                                $form['submit'] = [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Checkin Item'),
                                        '#suffix' => '<br><br>',
                                        '#button_type' => 'primary',
                                        '#description' => (string)$this->t('Removes the exclusiv-write-permission for this item and returns to dashboard. Doesnt save any entered data.')
                                ];
                        }
                        
                        return $form;
                }
                
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm(array &$form, FormStateInterface $form_state) {
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm(array &$form, FormStateInterface $form_state) {
        
                        if(null !== ($workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($form_state->get('wf_id')))) {
                                
                                if($workflow->getLock())
                                        $workflow->unlock();
                        }
        
                        $addParams = [];
                        $fragment = [];
                        if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                                $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                                $fragment = ['fragment' => 'rwDashboardItem-'.$form_state->get('wf_id')];
                                \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                        }
        
                        if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardItem')) {
                                $addParams[ 'item' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardItem' );
                                \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardItem');
                        }
                        
                        $form_state->setRedirect('publisso_gold.dashboard', $addParams, $fragment);
                        
                        return $form;
                }
        }
