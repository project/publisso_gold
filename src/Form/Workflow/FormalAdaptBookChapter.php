<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\FormalAdaptBookChapter.
 */
namespace Drupal\publisso_gold\Form\Workflow;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\publisso_gold\Classes\Ajax\OpenUrlCommand;
use Drupal\publisso_gold\Controller\Manager\ChapterManager;
use Drupal\publisso_gold\Controller\Publisso;
use Drupal\publisso_gold\Controller\Workflow\ChapterWorkflow;
use Drupal\publisso_gold\Controller\Workflow\Comment;
use Drupal\Core\Url;

/**
 * Provides chapter create form.
 */
class FormalAdaptBookChapter extends FormBase {

        private $modname = 'publisso_gold';
        private $database;
        private $modpath;
        private $modaccessweights = [];

        protected $workflow;
        
        const __ACCESS_DENIED__             = 'You can not access this page. You may need to log in first.';
        
        /**
         * {@inheritdoc}
         */
        public function __construct(){
        }
        
        /**
         * {@inheritdoc}
         */
        public function getFormId() {
                return 'formaladaptbookchapter';
        }
  
        /**
         * {@inheritdoc}
         */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                if(!$form_state->has('simple_values')){
                        $form_state->set('simpleValues', [
                                'title'                    ,
                                'conflict_of_interest'     ,
                                'conflict_of_interest_text',
                                'funding'                  ,
                                'funding_name'             ,
                                'funding_id'               ,
                                'reviewer_suggestion'      ,
                                'accept_policy'            ,
                                'add_files'                ,
                                'doi'                      ,
                                'version'                  ,
                                'license'                  ,
                                'publication_place'        ,
                                'publication_year'         ,
                                'sub_category'             ,
                                'comment'                  ,
                                'references'               ,
                                'title_short'              ,
                                'received'                 ,
                                'revised'                  ,
                                'accepted'                 ,
                                'published'
                        ]);
                }
                $setup = \Drupal::service('publisso_gold.setup');
                $tools = \Drupal::service('publisso_gold.tools');
                //$form_state->set('form_data', unserialize(base64_decode('YTo2OntzOjU6InRpdGxlIjtzOjE4OiJUaXRsZSBvZiBDaGFwdGVyIDEiO3M6ODoia2V5d29yZHMiO3M6MzE6IktleXdvcmQgMTsgS2V5d29yZCAyOyBLZXl3b3JkIDMiO3M6MTI6ImNoYXB0ZXJfdGV4dCI7czo1MzM6IjxwPiZiZHF1bztEbyB5b3Ugc2VlIGFueSA8c3BhbiBzdHlsZT0iY29sb3I6ICNmZjAwMDA7Ij48c3Ryb25nPlRlbGV0dWJiaWVzPC9zdHJvbmc+PC9zcGFuPiBpbiBoZXJlPyBEbyB5b3Ugc2VlIGEgc2xlbmRlciBwbGFzdGljIHRhZyBjbGlwcGVkIHRvIG15IHNoaXJ0IHdpdGggbXkgbmFtZSBwcmludGVkIG9uIGl0PyBEbyB5b3Ugc2VlIGEgbGl0dGxlIEFzaWFuIGNoaWxkIHdpdGggYSBibGFuayBleHByZXNzaW9uIG9uIGhpcyBmYWNlIHNpdHRpbmcgb3V0c2lkZSBvbiBhIG1lY2hhbmljYWwgaGVsaWNvcHRlciB0aGF0IHNoYWtlcyB3aGVuIHlvdSBwdXQgcXVhcnRlcnMgaW4gaXQ/IDxzcGFuIHN0eWxlPSJiYWNrZ3JvdW5kLWNvbG9yOiAjZmYwMDAwOyI+Tm88L3NwYW4+PyBXZWxsLCB0aGF0J3Mgd2hhdCB5b3Ugc2VlIGF0IGEgdG95IHN0b3JlLiBBbmQgeW91IG11c3QgdGhpbmsgeW91J3JlIGluIGEgdG95IHN0b3JlLCBiZWNhdXNlIHlvdSdyZSBoZXJlIHNob3BwaW5nIGZvciBhbiBpbmZhbnQgbmFtZWQgSmViLiZsZHF1bzs8L3A+IjtzOjk6ImF1dGhvcl9kYiI7YTo1OntpOjA7czoxODoiMSB8IFN0ZWZhbiBTZWxpZ2VyIjtpOjE7czoxMzoiUSB8IEMuIFNjaHVseiI7aToyO3M6MTc6IlAgfCBVcnN1bGEgQXJuaW5nIjtpOjM7czoxNzoiTyB8IEthdGphIFBsZXRzY2giO2k6NDtzOjIzOiIxVCB8IENvbnN0YW56ZSBCZXJpbmdlciI7fXM6MTA6ImF1dGhvcl9hZGQiO2E6Mjp7aTowO2E6Mzp7czo5OiJmaXJzdG5hbWUiO3M6NjoiS3V0dGVsIjtzOjg6Imxhc3RuYW1lIjtzOjg6IkRhZGRlbGR1IjtzOjExOiJhZmZpbGlhdGlvbiI7czo4OiJGaXNodG93biI7fWk6MTthOjM6e3M6OToiZmlyc3RuYW1lIjtzOjQ6IkthcmwiO3M6ODoibGFzdG5hbWUiO3M6NToiS25hY2siO3M6MTE6ImFmZmlsaWF0aW9uIjtzOjExOiJFbnRlbmhhdXNlbiI7fX1zOjExOiJjb3Jwb3JhdGlvbiI7YToyOntpOjA7czoxMzoiQ29ycG9yYXRpb24gSSI7aToxO3M6MTQ6IkNvcnBvcmF0aW9uIElJIjt9fQ==')));
                
                /**
                 * link some libraries
                 */
                $this->modpath = drupal_get_path('module', $this->modname);
                require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');

                if(!$form_state->has('wf_id')){
                        $form_state->set('wf_id', $args['wf_id']);
                }

                $workflow = $tools->getWorkflowItem($form_state->get('wf_id'));
                $this->workflow = $workflow;
                $book = $workflow->getMedium();
                
                
                if(!$form_state->has('book')) $form_state->set('book', $book);
                if(!$form_state->has('workflow')) $form_state->set('workflow', $workflow);
                
                /**
                 * workflow has to be set in revision-state. if not, abort.
                 */
                $allowedStates = [
                        'accepted',
                        'author change request',
                        'editor change request'
                ];
                if(!in_array($form_state->get('workflow')->getDataElement('state'), $allowedStates)){
                        return [
                                '#markup' => (string)$this->t('ERROR: Workflow has a wrong state (@state)!', ['@state' => $form_state->get('workflow')->getDataElement('state')])
                        ];
                }
                
                /**
                 * set some global parameters
                 */
                
                $link_book = Url::fromRoute('publisso_gold.books_book', ['bk_id' => $book->getElement('id')])->toString();
                $language = \Drupal::languageManager()->getCurrentLanguage();
                $this->database = \Drupal::database();
        
                //Link1 (uri): Authors contract
                //Link2 (uri_1): Book policy
                $uri_de         = 'https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/#c5185';
		$url_de         = Url::fromUri($uri_de);
		$uri_1_de       = 'https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/';
		$url_1_de 	= Url::fromUri($uri_1_de);
		$text_de        = 'German';
	
		$uri_en         = 'https://www.publisso.de/en/publishing/books/books-policy/#c5189';
		$url_en 	= Url::fromUri($uri_en);
		$uri_1_en       = 'https://www.publisso.de/en/publishing/books/books-policy/';
		$url_1_en 	= Url::fromUri($uri_1_en);
		$text_en        = 'English';
                
                //initiate steps if not done
                if(!$form_state->get('step')) $form_state->set('step', 1);
                
//load workflow-data unless already done

                if(!$form_state->get('wf_data_loaded') === true){
                        
                        $form_data = [
                        ];
                        
                        $key = 'abstract';
                        $form_data[$key] = Publisso::tools()->isBase64($form_state->get('workflow')->getDataElement($key)) ? base64_decode($form_state->get('workflow')->getDataElement($key)) : $form_state->get('workflow')->getDataElement($key);
                        
                        $key = 'chapter_text';
                        $form_data[$key] = Publisso::tools()->isBase64($form_state->get('workflow')->getDataElement($key)) ? base64_decode($form_state->get('workflow')->getDataElement($key)) : $form_state->get('workflow')->getDataElement($key);
                        
                        $key = 'keywords';
                        $form_data[$key] = implode('; ', json_decode($form_state->get('workflow')->getDataElement($key), true));
                        
                        $form_data['author_db' ] = [];
                        $form_data['author_add'] = [];
                        
                        foreach(json_decode($form_state->get('workflow')->getDataElement('authors'), true) as $author){
                                
                                if(array_key_exists('uid', $author)){
                                        
                                        $user = new \Drupal\publisso_gold\Controller\User($author['uid']);
                                        
                                        if($user && $user->getElement('id')){
                                                
                                                $ary = [];
                                                
                                                $ary['author'] = implode(
                                                        '; ',
                                                        array_filter([
                                                                implode(
                                                                        ' ',
                                                                        array_filter([
                                                                                $user->profile->getElement('firstname'),
                                                                                $user->profile->getElement('lastname' )
                                                                        ])
                                                                ),
                                                                $user->profile->getElement('department'),
                                                                $user->profile->getElement('institute' ),
                                                                Publisso::tools()->getCountry($user->profile->getElement('country'))
                                                        ])
                                                );
                                                
                                                $ary['is-corresponding' ] = $author['is_corresponding'  ];
                                                $ary['weight'           ] = $author['weight'            ];
                                                
                                                $form_data['author_db'][] = $ary;
                                        }
                                }
                                else{
                                        $form_data['author_add'][] = $author;
                                }
                        }
                        
                        $form_data['corporation'] = json_decode($form_state->get('workflow')->getDataElement('corporations'), true);
                        
                        //load simple data
                        
                        foreach($form_state->get('simpleValues') as $key)
                                $form_data[$key] = $form_state->get('workflow')->getDataElement($key);
                        

                        $form_state->set('form_data', $form_data);
                        $form_state->set('wf_data_loaded', true);
                }
                
                /*#############################################################################################*/
                
                /**
                 * show the form.
                 * at this point should all parameters set
                 * and only form-depending things processed
                 */
                $form['#tree'] = true;
                
                #$form['mu_comments'] = $this->getCommentsForAuthor($form_state->get('workflow')->getElement('id'));
                #$form['mu_files'   ] = $this->getAssignedFiles($form_state->get('workflow')->getElement('id'));
                
                $form ['mu_site_headline'] = [
                        '#markup' => (string)$this->t('Formal adapt chapter'),
                        '#prefix' => '<h1>',
                        '#suffix' => '</h1>'
                ];
                
                $form['mu_book_title'] = [
                        '#markup' => $book->getElement('title'),
                        '#prefix' => '<h2>',
                        '#suffix' => '</h2>'
                ];
                
                /**
                 * form step 1
                 */
/* Step 1 */    //if($form_state->get('step') == 1):
                        
                        //chapter-title
                        $form['title'] = [
                                '#type'         => 'textfield',
                                '#title'        => t('Title'),
                                '#required'     => true,
                                '#placeholder'  => (string)$this->t('Enter the title of your chapter here'),
                                '#maxlength'    => 255
                        ];
                        
                        $form['title_short'] = [
                                '#type'         => 'textfield',
                                '#title'        => t('Short-title (for PDF-Output as "Running title")'),
                                '#required'     => false,
                                '#placeholder'  => (string)$this->t('Enter the short-title of your chapter here'),
                                '#maxlength'    => 255
                        ];
                        
                        if($form_state->get('form_data')['title']) $form['title']['#default_value'] = $form_state->get('form_data')['title'];
                        if($form_state->get('form_data')['title_short']) $form['title_short']['#default_value'] = $form_state->get('form_data')['title_short'];
                
                        //abstract (if required)
                        if($book->getControlElement('abstract_required') == 1 || $book->getControlElement('abstract_required') == 2):
                                
                                $form['abstract'] = [
                                        '#type'         => 'textarea',
                                        '#title'        => (string)$this->t('Abstract'),
                                	'#required'     => $book->getControlElement('abstract_required') == 1 ? true : false,
                                        '#prefix'       => (string)$this->t('If an abstract is required or optional, please enter it here.'),
                                        '#rows'         => 20,
                                        '#attributes'   => [
                                                'class'         => [
                                                	\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
                                                ]
                                        ]
                                ];
                                
                                if($form_state->get('form_data')['abstract']) $form['abstract']['#default_value'] = $form_state->get('form_data')['abstract'];

                        endif;
                        
                        //keywords
                        $form['keywords'] = [
                                '#type'                 => 'textfield',
                                '#title'                => (string)t('Keywords'),
                                '#default_value'        => strlen($form_state->get('form_data')['keywords']) > 0 ?? implode(';', json_decode($form_state->get('form_data')['keywords'], true)),
                                '#placeholder'          => (string)$this->t('Enter the keywords separated with a semicolon here'),
                                '#maxlength'            => 400
                        ];
                        
                        if($form_state->get('form_data')['keywords']) $form['keywords']['#default_value'] = $form_state->get('form_data')['keywords'];
                        
                        //chapter text
                        if(!$workflow->getDataElement('predefined_maintext')) {
                                $form[ 'chapter_text' ] = [
                                        '#type'       => 'textarea',
                                        '#title'      => t( 'Main Text' ),
                                        '#required'   => true,
                                        '#rows'       => 50,
                                        '#attributes' => [
                                                'class' => [
                                                        \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' )
                                                ]
                                        ]
                                ];
        
                                if($form_state->get('form_data')['chapter_text']) $form['chapter_text']['#default_value'] = $form_state->get('form_data')['chapter_text'];
                        }
                        else{
                                $form['chapter_text'] = [
                                        '#type' => 'fieldset',
                                        '#title' => (string)$this->t('Main Text'),
                                        'main_text' => [
                                                '#type' => 'inline_template',
                                                '#template' => base64_decode($workflow->getDataElement('chapter_text'))
                                        ]
                                ];
                        }
                        
                        if(!$workflow->getDataElement('predefined_references')) {
                                $form[ 'references' ] = [
                                        '#type'   => 'textarea',
                                        '#title'  => (string)t( 'References' ),
                                        '#rows'   => 10,
                                        '#cols'   => 60,
                                        '#prefix' => '<br>',
                                        '#id'     => 'ta-rwReferences'
                                ];
                        }
                        else{
                                $form['references'] = [
                                        '#type' => 'fieldset',
                                        '#title' => (string)$this->t('References'),
                                        'main_text' => [
                                                '#type' => 'inline_template',
                                                '#template' => nl2br($workflow->getDataElement('references'))
                                        ]
                                ];
                        }
                        
                        if($form_state->get('form_data')['references']) $form['references']['#default_value'] = $form_state->get('form_data')['references'];
                        
                        //authors
                $this->getFormAuthorsDB($form, $form_state);
                $this->getFormAuthorsAdd($form, $form_state);

                        /*if(count($form_state->get('form_data')['author_db' ]) && $form_state->getRebuildInfo('step1') != true) $form_state->set('cntAuthorsDB' , count($form_state->get('form_data')['author_db' ]));
                        if(count($form_state->get('form_data')['author_add']) && $form_state->getRebuildInfo('step1') != true) $form_state->set('cntAuthorsAdd', count($form_state->get('form_data')['author_add']));
                        if(!$form_state->has('cntAuthorsDB' ) || $form_state->get('cntAuthorsDB') < 3){ $form_state->set('cntAuthorsDB' , 3); }
                        if(!$form_state->has('cntAuthorsAdd')){ $form_state->set('cntAuthorsAdd', 1); }
                        
                        $form['fs_authors'] = [
                                '#type' => 'fieldset',
                                '#title' => (string)$this->t('Author(s)'),
                                '#prefix' => '<br>',
                                'content' => [
                                        'description' => [
                                                '#markup' => (string)$this->t('Enter one author per box. Authors who are not yet in the database can be added as additional authors'),
                                                '#suffix' => '<br><br>'
                                        ],
                                        'author-db' => [
                                                '#type'   => 'container',
                                                '#prefix' => '<div id="author-db-wrapper">',
                                                '#suffix' => '</div>',
                                                '#tree'   => true,
                                                'content' => [
                                                        '#type' => 'table',
                                                        '#header' => [
                                                                (string)$this->t('Author(s)'),
                                                                (string)$this->t('Corresponding'),
                                                                (string)$this->t('Position')
                                                        ]
                                                ]
                                        ],
                                        'author-db-add' => [
                                                '#type'                    => 'submit',
                                                '#button_type'             => 'warning',
                                                '#value'                   => (string)$this->t('add box'),
                                                '#limit_validation_errors' => [],
                                                '#submit'                  => ['::addAuthorDB'],
                                                '#prefix'                  => '',
                                                '#suffix'                  => '<br><br>',
                                                '#description'             => (string)$this->t('Add further authors'),
                                                '#ajax'                    => [
                                                        'wrapper'       => 'author-db-wrapper',
                                                        'effect'        => 'fade',
                                                        'callback'      => '::addAuthorDBCallback'
                                                ]
                                        ],
                                        'author-add' => [
                                                '#type'    => 'container',
                                                '#prefix'  => '<div id="author-add-wrapper">',
                                                '#suffix'  => '</div>',
                                                '#tree'    => true,
                                                'content' => []
                                        ],
                                        'author-add-add' => [
                                                '#type'                    => 'submit',
                                                '#button_type'             => 'warning',
                                                '#value'                   => (string)$this->t('add box '), //Space is important!
                                                '#limit_validation_errors' => [],
                                                '#submit'                  => ['::addAuthorAdd'],
                                                '#prefix'                  => '<br>',
                                                '#description'             => (string)$this->t('For more additional authors, add more boxes'),
                                                '#ajax'                    => [
                                                        'wrapper'       => 'author-add-wrapper',
                                                        'effect'        => 'fade',
                                                        'callback'      => '::addAuthorAddCallback'
                                                ]
                                        ]
                                ]
                        ];
                        
                        for($i = 0; $i < $form_state->get('cntAuthorsDB'); $i++){
                                
                                $form['fs_authors']['content']['author-db']['content'][$i] = [
                                        'author'           => [
                                                '#type'                          => 'textfield',
                                                '#autocomplete_route_name'       => 'autocomplete.user.public',
                                                '#autocomplete_route_parameters' => array(),
                                                '#title'                         => (string)$this->t('Author(s)'),
                                                '#title_display'                 => 'invisible',
                                                '#placeholder'                   => (string)$this->t('Enter at least 3 characters of the name and choose from the database'),
                                                '#description'                   => (string)$this->t('Matching entries from the database will be suggested automatically'),
                                                '#maxlength'                     => 256,
                                                '#id' => 'author_db_'.$i
                                        ],
                                        'is-corresponding' => [
                                                '#type' => 'checkbox'
                                        ],
                                        'weight'           => [
                                                '#type'          => 'number',
                                                '#size'          => 3,
                                                '#min'           => 1,
                                                '#max'           => 100,
                                                '#description'   => (string)$this->t('The authors’ order can be determined here')
                                        ]
                                ];
                                
                                if(isset($form_state->get('form_data')['author_db'][$i]['author'])){
                                        $form['fs_authors']['content']['author-db']['content'][$i]['author'          ]['#default_value'] = $form_state->get('form_data')['author_db'][$i]['author'          ];
                                        $form['fs_authors']['content']['author-db']['content'][$i]['is-corresponding']['#default_value'] = $form_state->get('form_data')['author_db'][$i]['is-corresponding'];
                                        $form['fs_authors']['content']['author-db']['content'][$i]['weight'          ]['#default_value'] = $form_state->get('form_data')['author_db'][$i]['weight'          ];
                                }
                        }
                        
                        for($i = 0; $i < $form_state->get('cntAuthorsAdd'); $i++){
                                
                                $form['fs_authors']['content']['author-add']['content']['author-add-'.$i] = [
                                        '#type'                          => 'fieldset',
                                        '#tree'                          => true,
                                        '#title'                         => (string)$this->t('Additional author @nr', ['@nr' => ($i + 1)]),
                                        'content' => [
                                                'firstname'   => [
                                                        '#type' => 'textfield',
                                                        '#title' => (string)$this->t('Firstname'),
                                                        '#placeholder' => (string)$this->t('Enter the firstname of additional author #@nr', ['@nr' => ($i + 1)])
                                                ],
                                                'lastname'    => [
                                                        '#type' => 'textfield',
                                                        '#title' => (string)$this->t('Lastname'),
                                                        '#placeholder' => (string)$this->t('Enter the lastname of additional author #@nr', ['@nr' => ($i + 1)])
                                                ],
                                                'affiliation' => [
                                                        '#type' => 'textfield',
                                                        '#title' => (string)$this->t('Affiliation'),
                                                        '#placeholder' => (string)$this->t('Enter the affiliation of additional author #@nr', ['@nr' => ($i + 1)]),
                                                        '#maxlength' => 300,
                                                ],
                                                'weight'      => [
                                                        '#type'          => 'number',
                                                        '#size'          => 3,
                                                        '#title'         => (string)$this->t('Position')
                                                ]
                                        ]
                                ];
                                
                                if(isset($form_state->get('form_data')['author_add'][$i])){
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['firstname'  ]['#default_value'] = $form_state->get('form_data')['author_add'][$i]['firstname'  ];
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['lastname'   ]['#default_value'] = $form_state->get('form_data')['author_add'][$i]['lastname'   ];
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['affiliation']['#default_value'] = $form_state->get('form_data')['author_add'][$i]['affiliation'];
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['weight'     ]['#default_value'] = $form_state->get('form_data')['author_add'][$i]['weight'     ];
                                }
                        }
                        */
                        //corporations
                        if(count($form_state->get('form_data')['corporation']) && $form_state->getRebuildInfo('step1') != true) $form_state->set('cntCorporations' , count($form_state->get('form_data')['corporation']));
                        if(!$form_state->has('cntCorporations')){ $form_state->set('cntCorporations', 1); }
                        
                        $form['corporations'] = [
                                '#type'   => 'container',
                                '#prefix' => '<div id="corporations-wrapper">',
                                '#suffix' => '</div>',
                                '#tree'   => true,
                                'content' => []
                        ];
                        
                        $form['corporations-add'] = [
                                '#type'                    => 'submit',
                                '#button_type'             => 'warning',
                                '#value'                   => (string)$this->t('add corporation'), //Space is important!
                                '#limit_validation_errors' => [],
                                '#submit'                  => ['::addCorporation'],
                                '#prefix'                  => '<br>',
                                '#description'             => (string)$this->t('Add more corporations'),
                                '#suffix'                  => '<br><br>',
                                '#ajax'                    => [
                                        'wrapper'       => 'corporations-wrapper',
                                        'effect'        => 'fade',
                                        'callback'      => '::addCorporationCallback'
                                ]
                        ];
                        
                        for($i = 0; $i < $form_state->get('cntCorporations'); $i++){
                                
                                $form['corporations']['content'][$i] = [
                                        '#type'          => 'textfield',
                                        '#title'         => (string)$this->t('Corporation(s)'),
                                        '#title_display' => $i ? 'invisible' : 'before',
                                        '#placeholder'   => (string)$this->t('Enter corporations that should appear as authors here'),
                                        '#description'   => (string)$this->t('One corporation per box')
                                ];
                                
                                if(isset($form_state->get('form_data')['corporation'][$i])) $form['corporations']['content'][$i]['#default_value'] = $form_state->get('form_data')['corporation'][$i];
                        }
                        
                        $form['doi'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t('DOI'),
                                '#default_value' => $form_state->get('form_data')['doi']
                        ];
                        
                        $form['version'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t('Version'),
                                '#default_value' => $form_state->get('form_data')['version']
                        ];
                        
/* Step 2 */    //elseif($form_state->get('step') == 2):
                        
                        //conflict of interest
                        $form['conflict_of_interest'] = [
                                '#type'         => 'checkbox',
                                '#title'        => (string)t('Conflict of interest'),
                                '#prefix'       => (string)$this->t('Here you may add if there’s any conflict of interest to declare, which might be relevant regarding the development of your chapter. If there’s anything to declare you can add a description in the appearing box.')
                        ];
                        
                        if(isset($form_state->get('form_data')['conflict_of_interest'])) $form['conflict_of_interest']['#default_value'] = $form_state->get('form_data')['conflict_of_interest'];
                        
                        $form['conflict_of_interest_text'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t('Description of conflict of interest'),
                                '#maxlength'     => 256,
                                '#default_value' => isset($workflow_data) ? $workflow_data->conflict_of_interest_text : '',
                                '#states'        => array(
                                        'visible'       => array(   // action to take.
                                                ':input[name="conflict_of_interest"]' => array('checked' => true),
                                        ),
                                        'invisible'     => array(   // action to take.
                                                ':input[name="conflict_of_interest"]' => array('checked' => false),
                                        )
                                )
                        ];
                        
                        if(isset($form_state->get('form_data')['conflict_of_interest_text'])) $form['conflict_of_interest_text']['#default_value'] = $form_state->get('form_data')['conflict_of_interest_text'];
                        
                        //funding
                        $form['funding'] = [
                                '#type'         => 'checkbox',
                                '#title'        => t('Funds received'),
                                '#prefix'       => (string)$this->t('If you received any funds please add the information here.')
                        ];
                        
                        if(isset($form_state->get('form_data')['funding'])) $form['funding']['#default_value'] = $form_state->get('form_data')['funding'];
                        
                        $form['funding_name'] = [
                                '#type'      => 'textfield',
                                '#title'     => (string)$this->t('Funding name'),
                                '#maxlength' => 256,
                                '#states'    => array(
                                        'visible'   => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => true),
                                        ),
                                        'invisible' => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => false),
                                        )
                                ),
                        ];
                        
                        if(isset($form_state->get('form_data')['funding_name'])) $form['funding_name']['#default_value'] = $form_state->get('form_data')['funding_name'];
                        
                        $form['funding_id'] = [
                                '#type'   => 'textfield',
                                '#title'  => (string)t('Funding ID'),
                                '#states' => array(
                                        'visible'   => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => true),
                                        ),
                                        'invisible' => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => false),
                                        )
                                ),
                        ];
                        
                        if(isset($form_state->get('form_data')['funding_id'])) $form['funding_id']['#default_value'] = $form_state->get('form_data')['funding_id'];
                        
                        $form['license'] = [
                                '#type'          => 'select',
                                '#title'         => t('License'),
                                '#required'      => true,
                                '#options'       => \Drupal::service('publisso_gold.tools')->getLicenses(),
                                '#default_value' => $form_state->get('form_data')['license']
                        ];
                        
                        $form['publication_place'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t('Publication place'),
                                '#required'      => true,
                                '#default_value' => isset($form_state->get('form_data')['publication_place']) ? $form_state->get('form_data')['publication_place'] : $book->getElement('publication_place')
                        ];
                        
                        $form['publication_year'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t('Publication year'),
                                '#required'      => true,
                                '#default_value' => isset($form_state->get('form_data')['publication_year']) ? $form_state->get('form_data')['publication_year'] : $book->getElement('publication_year')
                        ];
                        
                        $form['sub_category'] = [
                                '#type'          => 'textfield',
                                '#title'         => t('Subcategory'),
                                '#required'      => true,
                                '#default_value' => isset($form_state->get('form_data')['sub_category']) ? $form_state->get('form_data')['sub_category'] : $book->getElement('sub_category')
                        ];
                        
                        $form['reviewer_suggestion'] = [
                                '#type'  => 'textfield',
                                '#title' => t('Reviewer suggestion'),
                                '#placeholder' => (string)$this->t('Enter the names of potential reviewers of your chapter here')
                        ];
                        
                        if(isset($form_state->get('form_data')['reviewer_suggestion'])) $form['reviewer_suggestion']['#default_value'] = $form_state->get('form_data')['reviewer_suggestion'];

                        //accept policy
                        $form['accept_policy'] = [
                                '#type'     => 'checkbox',
                                '#title'    => (string)t('I accept the author\'s contract</a>'),
                                '#required' => true,
                                '#disabled' => true
                        ];
                        
                        if(isset($form_state->get('form_data')['accept_policy'])) $form['accept_policy']['#default_value'] = $form_state->get('form_data')['accept_policy'];
                        true;
                        
/* Step 3 */    //elseif($form_state->get('step') == 3):
                
                        $form['description'] = array(
                                '#markup' => (string)$this->t('Here you can upload additional Files to your submission'),
                                '#prefix' => '<h3>',
                                '#suffix' => '</h3>'
                        );
                        
                        $form['files_fieldset'] = [
                                '#type' => 'fieldset',
                                '#title' => (string)$this->t('Upload additional Files'),
                                '#prefix' => '<div id="files-fieldset-wrapper">',
                                '#suffix' => '</div>',
                        ];
                        
                        if(!$form_state->has('numFiles')) $form_state->set('numFiles', 0);
                        $numFiles = $form_state->get('numFiles');
                        
                        for ($i = 0; $i < $numFiles; $i++) {

                                $form['files_fieldset']['file'][$i] = [

                                        '#type' => 'fieldset',
                                        'file'  => [
                                                '#type'                 => 'managed_file',
                                                '#title'                => (string)$this->t('File').' #'.($i + 1),
                                                '#required'             => true,
                                                '#upload_validators'    => [
                                                        'file_validate_extensions' => $setup->getValue('file.upload.valid_extensions') ? [$setup->getValue('file.upload.valid_extensions')] : array('gif png jpg jpeg doc docx xls xlsx midi mp4 mp3 avi mpg mpeg wav mov mid odt ods odp pdf'),
                                                        // Pass the maximum file size in bytes
                                                        'file_validate_size' => array(30 * 1024 * 1024),
                                                ],
                                                '#progress_indicator'   => 'bar',
                                                '#progress_message'     => (string)$this->t('Uploading file...')
                                        ],
                                        'meta'  => [
                                                'name' => [
                                                        '#type'         => 'textfield',
                                                        '#title'        => (string)$this->t('Filename'),
                                                        '#required'     => true
                                                ],
                                                'description'   => [
                                                        '#type'         => 'textfield',
                                                        '#title'        => (string)$this->t('Description')
                                                ]
                                        ]
                                ];
                        }
                        
                        $form['files_fieldset']['actions']['add_name'] = [
                                '#type' => 'submit',
                                '#value' => t('Add file'),
                                '#submit' => array('::addOne'),
                                '#prefix' => '<br>',
                                '#limit_validation_errors' => [],
                                '#ajax' => [
                                        'callback' => '::addmoreCallback',
                                        'wrapper' => 'files-fieldset-wrapper',
                                        'effect' => 'fade',
                                        'progress' => [
                                                'type' => 'none'
                                        ]
                                ],
                        ];
                        
                        if($numFiles > 0){

                                $form['files_fieldset']['actions']['remove_file'] = [
                                        '#type' => 'submit',
                                        '#value' => t('Remove last'),
                                        '#submit' => array('::removeCallback'),
                                        '#prefix' => '&nbsp;&nbsp;&nbsp;',
                                        '#ajax' => [
                                                'callback' => '::addmoreCallback',
                                                'wrapper' => 'files-fieldset-wrapper',
                                                'effect' => 'fade',
                                                'progress' => [
                                                        'type' => 'none'
                                                ]
                                        ],
                                        '#limit_validation_errors' => []
                                ];
                        }
                //endif;
                
                /*#############################################################################################*/
                
                $form['override_dates'] = [
                        '#type' => 'details',
                        '#title' => (string)$this->t('Override dates'),
                        '#tree' => false,
                        'dates' => [
                                'received' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Received'),
                                        '#default_value' => $form_state->get('form_data')['received'] ? date("Y-m-d", strtotime($form_state->get('form_data')['received'])) : ($form_state->get('workflow')->getStateChainElement('submission finished') ? date("Y-m-d",strtotime($form_state->get('workflow')->getStateChainElement('submission finished'))) :false) ?? ""
                                ],
                                'revised' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Revised'),
                                        '#default_value' => $form_state->get('form_data')['revised'] ? date("Y-m-d", strtotime($form_state->get('form_data')['revised'])) : ($form_state->get('workflow')->getStateChainElement('revision finished') ? date("Y-m-d",strtotime($form_state->get('workflow')->getStateChainElement('revision finished'))) :false) ?? ""
                                ],
                                'accepted' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Accepted'),
                                        '#default_value' => $form_state->get('form_data')['accepted'] ? date("Y-m-d", strtotime($form_state->get('form_data')['accepted'])) : ($form_state->get('workflow')->getStateChainElement('accepted') ? date("Y-m-d",strtotime($form_state->get('workflow')->getStateChainElement('accepted'))) :false) ?? ""
                                ],
                                'published' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Published'),
                                        '#default_value' => $form_state->get('form_data')['published'] ? date("Y-m-d", strtotime($form_state->get('form_data')['published'])) : ($form_state->get('workflow')->getStateChainElement('published') ? date("Y-m-d",strtotime($form_state->get('workflow')->getStateChainElement('published'))) :false) ?? ""
                                ]
                        ]
                ];
        
                $actionItems = $workflow->getSchemaItemActions(null, true);
                $options = [];
        
                $comment_options = [];
        
                foreach($actionItems as $item){
                        $options[$item['schema_follower']] = (string)$this->t($item['schema_select_action'] ?? 'n.a.');
                        if($item['schema_comment_required'] == 1) $comment_options[] = $item['schema_follower'];
                }
        
                $comment_required = [];
                $not_comment_required = [];
                foreach($comment_options as $_){
                
                        if(count($comment_required)) $comment_required[] = 'or';
                        $comment_required[] = [':input[name="follower"]' => array('value' => $_)];
                        $not_comment_required[] = [':input[name="follower"]' => array('!value' => $_)];
                }
        
                $form['follower'] = [
                        '#type' => 'select',
                        '#title' => (string)$this->t('Select action'),
                        '#required' => true,
                        '#options' => $options
                ];
        
                $form['comment_for_author'] = [
                        '#type' => 'textarea',
                        '#title' => (string)$this->t('Comment for author'),
                        '#states' => [
                                'visible'     => [':input[name="follower"]' => array('value' => 22)]
                        ]
                ];
                
                $form['comment_for_eic'] = [
                        '#type' => 'textarea',
                        '#title' => (string)$this->t('Comment for eic'),
                        '#states' => [
                                'visible'     => [':input[name="follower"]' => array('value' => 23)]
                        ]
                ];
                
                $form['comment_for_publisso'] = [
                        '#type' => 'textarea',
                        '#title' => (string)$this->t('Comment for PUBLISSO'),
                        '#states' => [
                                'visible'     => [':input[name="follower"]' => array('value' => 28)]
                        ],
                ];
                
                $form['close'] = [
                        '#type'                         => 'submit',
                        '#default_value'                => (string)$this->t('Back to dashboard'),
                        '#button_type'                  => 'warning',
                        '#suffix'                       => '&nbsp;&nbsp;&nbsp;',
                        '#limit_validation_errors'      => [],
                        '#submit'                       => ['::backToDashboard']
                ];
                
                $form['preview'] = [
                        '#type'                         => 'button',
                        '#value'                        => (string)$this->t('Preview'),
                        '#button_type'                  => 'primary',
                        '#suffix'                       => '&nbsp;&nbsp;&nbsp;',
                        '#limit_validation_errors'      => [],
                        '#description'                  => (string)$this->t('See how your chapter looks like'),
                        '#ajax'                         => [
                                'callback' => '::previewBookchapter',
                                'progress' => [
                                        'type' => 'none'
                                ]
                        ]
                ];
                
                
                $form['genpdf'] = [
                        '#type'                         => 'button',
                        '#value'                        => (string)$this->t('Create PDF'),
                        '#button_type'                  => 'primary',
                        '#suffix'                       => '&nbsp;&nbsp;&nbsp;',
                        '#limit_validation_errors'      => [],
                        '#description'                  => (string)$this->t('Create PDF'),
                        '#ajax'                         => [
                                'callback' => '::genPDF',
                                'progress' => [
                                        'type' => 'none'
                                ]
                        ]
                ];
                
                $form['finish'] = [
                        '#type'         => 'submit',
                        '#value'        => (string)$this->t('Finish'),
                        '#button_type'  => 'success',
                        '#submit'       => ['::processForm']
                ];
                
                $form['store_data_control_key'] = [
                        '#type'         => 'hidden',
                        '#value'        => 'previewchapter'
                ];
                
                $form['ctrl_action'] = [
                        '#type' => 'hidden',
                        '#value' => 'autosave'
                ];
                
                $form['ctrl_type'] = [
                        '#type' => 'hidden',
                        '#value' => 'bookchapter'
                ];
                
                $form['tmp_id'] = [
                        '#type' => 'hidden',
                        '#id' => 'tmp_id'
                ];
                
                if(!empty($form_state->get('form_data')['tmp_id'])){
                        $form['tmp_id']['#value'] = $form_state->get('form_data')['tmp_id'];
                }
                
                $form['bk_id'] = [
                        '#type' => 'hidden',
                        '#value' => $form_state->get('book')->getElement('id')
                ];
                
                $form['#cache'] = [
                        'max-age' => 0
                ];
                
                $form['spacer'] = [
                        '#markup' => '<hr>'
                ];
                
                $form_state->setCached(false);
                
                return $form;
        }
        
        /**
         * {@inheritdoc}
         */
        public function validateForm(array &$form, FormStateInterface $form_state){
                $i = 0;
                foreach($form_state->getValue('fs_authors')['content']['author-db']['content'] as $_){

                        [$author, $data] = preg_split('/;\s{1,}/', $_['author'], 2);
                        
                        if(!empty($author)){
                                $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                $found = 0;
                                $last_id = null;
                                $string = '';

                                foreach($res as $v){
                                        $string .= ' || '.implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, Publisso::tools()->getCountry($v->up_country)])).' | '.implode( '; ', [$author, $data]);
                                        if(
                                                implode( '; ', [$author, $data])
                                                                ==
                                                implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, Publisso::tools()->getCountry($v->up_country)]))
                                        ){
                                                $found++;
                                                $last_id = $v->up_uid;
                                        }
                                }

                                if($found != 1){
                                        $form_state->setError($form, (string)$this->t('Please use only suggested values in author-field #@i!', ['@i' => $i + 1]));
                                }
                        }
                        $i++;
                }
                
                return $form;
        }
        
        /**
         * {@inheritdoc}
         */
        public function submitForm(array &$form, FormStateInterface $form_state){
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return AjaxResponse
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        public function genPDF(array &$form, FormStateInterface $form_state){
        
                $workflow = $form_state->get('workflow');
                $this->processForm($form, $form_state, false);
                $this->storeFormData2Workflow($workflow, $form_state);
                $workflow->publish(false, true);
                $workflow->reload();
                $form_state->set('workflow', $workflow);
                
                $url = Url::fromRoute('publisso_gold.export.chapter.volatile', ['cp_id' => $workflow->getDataElement('cp_id')]);
                $url->setAbsolute();
        
                $chapter = ChapterManager::getChapter( $workflow->getDataElement( 'cp_id'));

                $blobID = $chapter->getElement('pdf_blob_id');
                
                if($blobID){
                        $blob = new \Drupal\publisso_gold\Controller\Blob($blobID);
                        $blob->delete($blobID);
                        $chapter->setElement('pdf_blob_id', null);
                }
                Publisso::log('Open PDF...');
                $response = new AjaxResponse();
                $response->addCommand(new OpenUrlCommand(null, $url->toString(), '_blank'));
                
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param bool $finish
         * @return array
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        public function processForm(array &$form, FormStateInterface $form_state, $finish = true){
                
                //init temp sorage if not done
                if(!$form_state->has('form_data')) $form_state->set('form_data', []);
                
                /**
                 * store formdata in a temporary storage
                 */
                $form_data = $form_state->get('form_data');
                
                if($form_state->hasValue('title'        )) $form_data['title'           ] = $form_state->getValue('title'       );
                if($form_state->hasValue('title_short'  )) $form_data['title_short'     ] = $form_state->getValue('title_short' );
                if($form_state->hasValue('keywords'     )) $form_data['keywords'        ] = $form_state->getValue('keywords'    );
                if($form_state->hasValue('chapter_text' )) $form_data['chapter_text'    ] = $form_state->getValue('chapter_text');
                if($form_state->hasValue('references'   )) $form_data['references'      ] = $form_state->getValue('references'  );
                
                if($form_state->hasValue('abstract'))
                        $form_data['abstract'] = $form_state->getValue('abstract');
                
                //authors in db
                $form_data['author_db'] = [];
                
                foreach($form_state->getValue('fs_authors')['content']['author-db']['content'] as $authorDB){
                        if(!empty($authorDB['author'])) $form_data['author_db'][] = $authorDB;
                }
                
                //additional authors
                $form_data['author_add'] = [];
                foreach($form_state->getValue('fs_authors')['content']['author-add']['content'] as $authorAdd){
                        if(!empty($authorAdd['content']['firstname'  ]) &&
                           !empty($authorAdd['content']['lastname'   ]) &&
                           !empty($authorAdd['content']['affiliation'])){
                                $form_data['author_add'][] = $authorAdd['content'];
                        }
                }
                
                //corporations
                $form_data['corporation'] = [];
                foreach($form_state->getValue('corporations')['content'] as $corporation){
                        if(!empty($corporation)) $form_data['corporation'][] = $corporation;
                }
                
                foreach($form_state->get('simpleValues') as $key){
                        $form_data[$key] = $form_state->getValue($key);
                }
                
                //store $form_data
                $form_state->set('form_data', $form_data);
                
                if($finish) $this->finishAdaption($form, $form_state);
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function processUserInput(array &$form, FormStateInterface $form_state){
                
                //init temp sorage if not done
                if(!$form_state->has('form_data')) $form_state->set('form_data', []);
                
                /**
                 * store formdata in a temporary storage
                 */
                $form_data = $form_state->get('form_data');
                $form_raw  = $form_state->getUserInput();
                
                if(array_key_exists('title'       , $form_raw)) $form_data['title'       ] = $form_raw['title'       ];
                if(array_key_exists('title_short' , $form_raw)) $form_data['title_short' ] = $form_raw['title_short' ];
                if(array_key_exists('keywords'    , $form_raw)) $form_data['keywords'    ] = $form_raw['keywords'    ];
                if(array_key_exists('chapter_text', $form_raw)) $form_data['chapter_text'] = $form_raw['chapter_text'];
                if(array_key_exists('references'  , $form_raw)) $form_data['references'  ] = $form_raw['references'  ];
                
                if(array_key_exists('abstract', $form_raw))
                        $form_data['abstract'] = $form_raw['abstract'];
                
                //authors in db
                $form_data['author_db'] = [];
                
                foreach($form_raw['fs_authors']['content']['author-db']['content'] as $authorDB){
                        if(!empty($authorDB['author'])) $form_data['author_db'][] = $authorDB;
                }
                
                //additional authors
                $form_data['author_add'] = [];
                foreach($form_raw['fs_authors']['content']['author-add']['content'] as $authorAdd){
                        if(!empty($authorAdd['content']['firstname'  ]) &&
                           !empty($authorAdd['content']['lastname'   ]) &&
                           !empty($authorAdd['content']['affiliation'])){
                                $form_data['author_add'][] = $authorAdd['content'];
                        }
                }
                
                //corporations
                $form_data['corporation'] = [];
                foreach($form_raw['corporations']['content'] as $corporation){
                        if(!empty($corporation)) $form_data['corporation'][] = $corporation;
                }
                
                foreach($form_state->get('simpleValues') as $key){
                        if(array_key_exists($key, $form_raw)) $form_data[$key] = $form_raw[$key];
                }
                
                //store $form_data
                $form_state->set('form_data', $form_data);
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        protected function finishAdaption(array &$form, FormStateInterface $form_state){
                
                //get the book
                $book = $form_state->get('book');
                
                //get stored form-data
                $form_data = $form_state->get('form_data');
                
                //get the workflow
                $workflow = $form_state->get('workflow');
                $workflow->reload();
                $workflow->historize();
                
                //set the data
                $this->storeFormData2Workflow($workflow, $form_state, true);
        
                
                
                //create the PDF
                $cp_id = $workflow->publish(false, true);
                $url = \Drupal\Core\Url::fromRoute('publisso_gold.export.chapter', ['cp_id' => $cp_id, 'type' => 'pdf']);
                $url->setAbsolute();
                
                try {
                        \Drupal::httpClient()->request('GET', $url->toString(), ['headers' => ['Content-Type' => 'application/text'], 'http_errors' => false, 'verify' => false]);
                }
                catch(\Exception $e){
                        Publisso::log($e->getMessage());
                }
                
                
                switch($form_state->getValue('follower')){
                        case 22: //author clearing request
                                $comment = $form_state->getValue('comment_for_author');
                                if ( !empty( $comment ) ) {
                                        Comment::new( $workflow->getElement('id'), $workflow->getElement( 'schema_identifier' ), $comment, null, null, $workflow->getSchemaCurrentRole(), 'submitting_author' );
                                }
                                break;
                        case 23:
                                $comment = $form_state->getValue('comment_for_eic');
                                if ( !empty( $comment ) ) {
                                        Comment::new( $workflow->getElement('id'), $workflow->getElement( 'schema_identifier' ), $comment, null, null, $workflow->getSchemaCurrentRole());
                                }
                                break;
                        case 28: //set ready for publication
                                $comment = $form_state->getValue('comment_for_publisso');
                                if ( !empty( $comment ) ) {
                                        Comment::new( $workflow->getElement('id'), $workflow->getElement( 'schema_identifier' ), $comment, null, null, $workflow->getSchemaCurrentRole());
                                }
                                break;
                }
                
                $workflow->newSchemaState($form_state->getValue('follower'));
                $workflow->save();

                $workflow->unlock();
        
                $addParams = [];
                if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                        $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                        \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                }
        
                $form_state->setRedirect('publisso_gold.dashboard', $addParams, ['fragment' => 'rwDashboardItem-'.$form_state->get('wf_id')]);
                
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
//        public function addAuthorDB(array &$form, FormStateInterface $form_state){
//                $cntAuthorsDB = $form_state->get('cntAuthorsDB');
//                $cntAuthorsDB++;
//                $form_state->set('cntAuthorsDB', $cntAuthorsDB);
//                $form_state->addRebuildInfo('step1', true);
//                $form_state->setRebuild();
//        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
//        public function addAuthorDBCallback(array &$form, FormStateInterface $form_state){
//                return $form['fs_authors']['content']['author-db'];
//        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
//        public function addAuthorAdd(array &$form, FormStateInterface $form_state){
//                $cntAuthorsAdd = $form_state->get('cntAuthorsAdd');
//                $cntAuthorsAdd++;
//                $form_state->set('cntAuthorsAdd', $cntAuthorsAdd);
//                $form_state->addRebuildInfo('step1', true);
//                $form_state->setRebuild();
//        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
//        public function addAuthorAddCallback(array &$form, FormStateInterface $form_state){
//                return $form['fs_authors']['content']['author-add'];
//        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addCorporation(array &$form, FormStateInterface $form_state){
                $cntCorporations = $form_state->get('cntCorporations');
                $cntCorporations++;
                $form_state->set('cntCorporations', $cntCorporations);
                $form_state->addRebuildInfo('step1', true);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addCorporationCallback(array &$form, FormStateInterface $form_state){
                return $form['corporations'];
        }
        
        /**
         * @param $bk_id
         * @return array
         */
        protected function getBook($bk_id){
                
                if(empty($bk_id)){
                        return [
                                'msg' => (string)$this->t('Initiate submission without book! Abort!'),
                                'book' => false
                        ];
                }
                
                //try to load book. if fails, throw an error!
                $book = new \Drupal\publisso_gold\Controller\Book($bk_id);
                
                if(!count($book->getKeys())){
                        return [
                                'msg' => (string)$this->t('Book (@bk_id) not found! Abort!', ['@bk_id' => $bk_id]),
                                'book' => false
                        ];
                }
                
                return [
                        'msg' => 'OK',
                        'book' => $book
                ];
        }
        
        /**
         * @param $wf_id
         * @return array
         */
        protected function getWorkflow($wf_id){
                
                if(empty($wf_id)){
                        return [
                                'msg' => (string)$this->t('Initiate revision without workflow! Abort!'),
                                'workflow' => false
                        ];
                }
                
                //try to load workflow. if fails, throw an error!
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
                
                if(!count($workflow->getKeys())){
                        return [
                                'msg' => (string)$this->t('Workflow (@wf_id) not found! Abort!', ['@wf_id' => $wf_id]),
                                'workflow' => false
                        ];
                }
                
                return [
                        'msg' => 'OK',
                        'workflow' => $workflow
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addOne(array &$form, FormStateInterface $form_state) {
                
                $numFiles = $form_state->get('numFiles');
                $form_state->set('numFiles', ++$numFiles);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addmoreCallback(array &$form, FormStateInterface $form_state) {
                return $form['files_fieldset'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function removeCallback(array &$form, FormStateInterface $form_state) {
                
                $numFiles = $form_state->get('numFiles');
                if($numFiles > 0) $form_state->set('numFiles', --$numFiles);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @return AjaxResponse
         */
        public function delFile(array &$form, FormStateInterface $form_state, $args = []) {
                
                $fid = $form_state->getTriggeringElement()['fid'];
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $response = new AjaxResponse();
                
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                $fids = array_diff($fids, [$fid]);
                $workflow->setDataElement('files', serialize($fids));
                
                //check if file are deleted
                //reload Workflow
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                
                if(array_search($fid, $fids)){
                        
                        $file = new \Drupal\publisso_gold\Controller\Blob($fid);
                        $response->addCommand(new AppendCommand('div#rwBlob-'.$fid, '<div class="rwError">'.((string)$this->t('Error deleting file "'.$file->meta['name'].'"')).'</div>'));
                }
                else{
                        $response->addCommand(new RemoveCommand('div#rwBlob-'.$fid));
                }
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return AjaxResponse
         * @throws \Exception
         */
        public function previewBookchapter(array &$form, FormStateInterface $form_state){
                
                $workflow = $form_state->get('workflow');
                $this->processUserInput($form, $form_state);
                $this->storeFormData2Workflow($workflow, $form_state);
                $workflow->publish(false, true);
                $workflow->reload();
                $form_state->set('workflow', $workflow);
                $chapter = ChapterManager::getChapter($workflow->getDataElement('cp_id'));
                $chapter->setElement('citation_note', null);
                
                $response = new AjaxResponse();
                $response->addCommand(new OpenUrlCommand(null, $chapter->getLink(), '_blank'));
                
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @throws \Exception
         */
        public function backToDashboard(array &$form, FormStateInterface $form_state){
                
                $workflow = $form_state->get('workflow');
                $workflow->reload();
                $workflow->historize();
                
                $call = "processUserInput";
                if(method_exists($this, $call)) $this->$call($form, $form_state);
                
                $this->storeFormData2Workflow($workflow, $form_state);
                
                $workflow->unlock();
        
                $addParams = [];
                if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                        $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                        \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                }
        
                $form_state->setRedirect('publisso_gold.dashboard', $addParams, ['fragment' => 'rwDashboardItem-'.$workflow->getElement('id')]);
        }
        
        /**
         * @param $workflow
         * @param FormStateInterface $form_state
         * @param false $store_files
         * @throws \Exception
         */
        protected function storeFormData2Workflow(&$workflow, FormStateInterface $form_state, $store_files = false){
                
                $session = \Drupal::service('session');
                $form_data = $form_state->get('form_data');
                
                //title
                $workflow->setDataElement('title', $form_data['title'], false);
                
                //abstract
                $element                = 'abstract';
                $$element               = $form_data[$element];
                $$element               = Publisso::tools()->normalizeInlineImageLinks($$element);
                $workflow->setDataElement($element, base64_encode($$element), false);

                //chapter_text
                $element                = 'chapter_text';
                if(array_key_exists($element, $form_data)) {
                        $$element = $form_data[ $element ];
                        $$element = Publisso::tools()->normalizeInlineImageLinks( $$element );
                        $workflow->setDataElement( $element, base64_encode( $$element ), false );
                }
                //keywords
                $keywords = array_filter(array_map('trim', explode(';', $form_data['keywords'])));
                $workflow->setDataElement('keywords', json_encode($keywords), false);
                
                //corporations
                $workflow->setDataElement('corporations', json_encode(array_filter(array_map('trim', $form_data['corporation']))), false);
                
                foreach($form_state->get('simpleValues') as $key){
                        if(array_key_exists($key, $form_data)) $workflow->setDataElement($key, $form_data[$key], false);
                }
                
                //authors
                $authors = [];
                
                foreach($form_data['author_db'] as $item){
                        
                        $author           = $item['author'];
                        $is_corresponding = $item['is-corresponding'];
                        $weight           = $item['weight'];
                        
                        [$author, $data] = preg_split('/;\s{1,}/', $author, 2);
                        if(!empty($author)){
                                $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                $found = 0;
                                $last_id = null;
                                $string = '';

                                foreach($res as $v){
                                        $string .= ' || '.implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, Publisso::tools()->getCountry($v->up_country)])).' | '.implode( '; ', [$author, $data]);
                                        if(
                                                implode( '; ', [$author, $data])
                                                                ==
                                                implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, Publisso::tools()->getCountry($v->up_country)]))
                                        ){
                                                $found++;
                                                $last_id = $v->up_uid;
                                        }
                                }

                                if($found == 1){
                                        
                                        $user = new \Drupal\publisso_gold\Controller\User($last_id);
                                        
                                        $authors[] = [
                                                'weight'             => $weight,
                                                'is_corresponding'   => $is_corresponding,
                                                'firstname'          => $user->profile->getElement('firstname'),
                                                'lastname'           => $user->profile->getElement('lastname'),
                                                'affiliation'        => $user->profile->getElement('institute'),
                                                'uid'                => $user->getElement('id'),
                                                'profile_archive_id' => $user->profile->makeSnapshot($session->get('user')['id'])
                                        ];
                                }
                        }
                }
                
                foreach($form_data['author_add'] as $item){
                        $authors[] = array_map('trim', $item);
                }
                
                $workflow->setDataElement('authors', json_encode($authors), false);
                
                if($store_files === true){
                        //files
                        $blobIds = [];
                        $wfBlobs = unserialize($workflow->getDataElement('files'));
                        
                        if(!is_array($wfBlobs)) $wfBlobs = [];
                        
                        if($form_state->hasValue('files_fieldset')){
                                
                                foreach($form_state->getValue('files_fieldset')['file'] as $file){

                                        if(count($file['file'])){

                                                $file_id = $file['file'][0];
                                                $file_meta = serialize($file['meta']);
                                                $file_db = \Drupal::database()->select('file_managed', 't')->fields('t', [])->condition('fid', $file_id, '=')->execute()->fetchAssoc();
                                                $file_type = $file_db['filemime'];
                                                $file_path = $file_db['uri'];

                                                if(preg_match('/^temporary:\/\/(.+)/', $file_path, $matches)){
                                                        $file_content = file_get_contents('/tmp/'.$matches[1]);
                                                }

                                                if($file_type && $file_content){

                                                        $blob = new \Drupal\publisso_gold\Controller\Blob();
                                                        $blob->create($file_content, $file_type, $file_meta);
                                                        $blobIds[] = $blob->getId();
                                                }
                                        }
                                }
                        }
                        
                        $workflow->setDataElement('files', serialize(array_unique(array_merge($wfBlobs, $blobIds))), false);
                }
                $workflow->setElement('modified', date('Y-m-d H:i:s'), false);
                
                $workflow->save();
        }
        
        /**
         * @param $wf_id
         * @return array
         */
        protected function getCommentsForAuthor($wf_id){
                
                $session = \Drupal::service('session');
                $comments = \Drupal::database()->select('rwPubgoldWorkflowCommentsForAuthors', 'cfa')
                        ->fields('cfa', [])
                        ->condition('cfa_wfid', $wf_id, '=')
                        ->condition('cfa_for_uid', $session->get('user')['id'], '=')
                        ->execute()
                        ->fetchAll();

                $renderComments = [];
                $c = 0;
                
                foreach($comments as $_){

                        if($c === 0){
                                $renderComments = [
                                        '#type' => 'details',
                                        '#open' => false,
                                        '#title' => (string)t('Comment(s) for author'),
                                        '#description' => (string)t('These comments are only available for you during current workflow. Nobody else can see these comments!'),
                                        'content' => []
                                ];
                        }

                        $_->cfa_comment = base64_decode($_->cfa_comment);

                        $renderComments['content'][] = [
                                '#type' => 'details',
                                '#open' => false,
                                '#title' => (string)t('Comment').' #' . ($c + 1),
                                'content' => [
                                        '#type' => 'markup',
                                        '#markup' => $_->cfa_comment
                                ]
                        ];

                        $c++;
                }
                
                return $renderComments;
        }
        
        /**
         * @param $wf_id
         * @return array
         */
        protected function getAssignedFiles($wf_id){
                
                $files_tmpl = [];
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
                $fids = $workflow->getDataElement('files');
                if($fids) $fids = unserialize($fids);
                
                if(is_array($fids)){
                        
                        foreach($fids as $fid){
                                
                                $blob = new \Drupal\publisso_gold\Controller\Blob($fid);
                                
                                $files_tmpl[] = [
                                        '#type' => 'fieldset',
                                        '#id' => 'rwBlob-'.$fid,
                                        'content' => [
                                                '#type' => 'link',
                                                '#title' => $blob->meta['name'],
                                                '#url' => Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()]),
                                                '#suffix' => '<br><i>'.$blob->meta['description'].'</i>'
                                        ],
                                        'preview' => preg_match('/^(image|video|audio)\//', $blob->type, $matches) ? [
                                                '#type' => 'inline_template',
                                                '#template' => (
                                                        $matches[1] == 'image' ? '<br><img height="100" src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'">' : (
                                                                $matches[1] == 'video' ? '<br><video height="100" controls><source src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'" type="'.$blob->type.'"></video>' : (
                                                                        $matches[1] == 'audio' ? '<br><audio controls><source src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'" type="'.$blob->type.'"></audio>' : (
                                                                                ''
                                                                        )
                                                                )
                                                        )
                                                )
                                        ] : '',
                                        'action#'.$fid => [
                                                '#type' => 'button',
                                                '#value' => (string)$this->t('Delete File \''.$blob->meta['name'].'\''),
                                                '#prefix' => '<br><br>',
                                                'fid' => $fid,
                                                '#ajax' => [
                                                        'callback' => '::delWorkflowFile',
                                                        'wrapper' => 'rwBlob-'.$fid,
                                                        'effect' => 'slide'
                                                ],
                                                'limit_validation_errors' => []
                                        ]
                                ];
                        }
                }
                
                if(count($files_tmpl)){
                        return [
                            '#type' => 'details',
                            '#title' => t('Assigned files'),
                            '#open' => false,
                            'content' => $files_tmpl,
                            '#prefix' => '<br>'
                        ];
                }
                else{
                        return [];
                }
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @return AjaxResponse
         */
        public function delWorkflowFile(array &$form, FormStateInterface $form_state, $args = []) {
                
                $fid = $form_state->getTriggeringElement()['fid'];
                $workflow = $form_state->get('workflow');
                $response = new AjaxResponse();
                
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                $fids = array_diff($fids, [$fid]);
                $workflow->setDataElement('files', serialize($fids));
                
                //check if file are deleted
                //reload Workflow
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                
                if(array_search($fid, $fids)){
                        
                        $file = new \Drupal\publisso_gold\Controller\Blob($fid);
                        $response->addCommand(new AppendCommand('div#rwBlob-'.$fid, '<div class="rwError">'.((string)$this->t('Error deleting file "'.$file->meta['name'].'"')).'</div>'));
                }
                else{
                        $response->addCommand(new RemoveCommand('div#rwBlob-'.$fid));
                }
                
                $form_state->setRebuild();
                return $response;
        }

        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormAuthorsDB(array &$form, FormStateInterface $form_state){

                $authorsDB = json_decode($this->workflow()->getDataElement('author_db'), 1) ?? [];
                Publisso::log(print_r($form_state->get('cntAuthorsDB'), 1));
                #if($form_state->hasValue('wrapper_authors_db')) $authorsDB = $form_state->getValue('wrapper_authors_db')['authors'];

                if($form_state->get('cntAuthorsDB') < count($authorsDB)) $form_state->set('cntAuthorsDB', count($authorsDB));

                if($form_state->get('cntAuthorsDB') < 3) $form_state->set('cntAuthorsDB', 3);

                $form['wrapper_authors_db'] = [
                        '#tree' => true,
                        '#type' => 'fieldset',
                        '#collapsible' => true,
                        '#title' => (string)$this->t('Author(s)'),
                        '#description' => (string)$this->t('Enter one author per box. Authors who are not yet in the database can be added as additional authors.'),
                        '#description_attributes' => [
                                'style' => 'font-size: 16px;'
                        ],
                        'authors' => [
                                '#type' => 'table',
                                '#header' => [
                                        ['data' => (string)$this->t('Author(s)'), 'width' => '45%'],
                                        ['data' => (string)$this->t('Corr.'), 'width' => '5%', 'title' => (string)$this->t('Corresponding author')],
                                        ['data' => (string)$this->t('Pos.'), 'width' => '5%', 'title' => (string)$this->t('Position')],
                                        ['data' => (string)$this->t('Additional affiliations'), 'width' => '45%']
                                ],
                                '#prefix' => '<div id="wrapper-authors-db">',
                                '#suffix' => '</div>'
                        ],
                        'actions' => [
                                '#type' => 'actions',
                                'add' => [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Add row'),
                                        '#submit' => ['::addAuthorDB'],
                                        '#ajax' => [
                                                'callback' => '::addAuthorDBCallback',
                                                'wrapper' => 'wrapper-authors-db'
                                        ],
                                        '#limit_validation_errors' => []
                                ]
                        ]
                ];

                for($i = 0; $i < $form_state->get('cntAuthorsDB'); $i++){

                        $author = $user = $affiliations = null;

                        if(array_key_exists($i, $authorsDB)){
                                $author = $authorsDB[$i];
                                $user = SubmitBookChapterHelper::getAuthorFromString($author['author']);
                                if($user) $affiliations = $user->profile->getAdditionalAffiliations();
                        }

                        $form['wrapper_authors_db']['authors'][$i] = [
                                'author' => [
                                        '#type' => 'textfield',
                                        '#autocomplete_route_name' => 'autocomplete.user.public',
                                        '#autocomplete_route_parameters' => [],
                                        '#ajax'         => [
                                                'event'         => 'autocompleteclose change',
                                                'prevent'       => 'blur',
                                                'progress'      => [
                                                        'type'          => 'none'
                                                ],
                                                'callback'      => '::autosave',
                                                'disable-refocus'       => true,
                                                'wrapper' => "add_affiliations_".$i,

                                        ],
                                        '#default_value' => $author['author']
                                ],
                                'is_corresponding' => [
                                        '#type' => 'checkbox',
                                        '#default_value' => $author['is_corresponding'],
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ]
                                ],
                                'weight' => [
                                        '#type' => 'number',
                                        '#default_value' => $author['weight'],
                                        '#attributes' => [
                                                'style' => 'width: 5em;'
                                        ],
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ]
                                ],
                                'wrapper-add_affiliation' => [
                                        '#type' => 'fieldset',
                                        '#collapsible' => true,
                                        '#collapsed' => true,
                                        '#title' => (string)$this->t('Additional affiliations'),
                                        'add_affiliations' => [
                                                '#type' => 'table',
                                                '#header' => [
                                                        ['data' => (string)$this->t('Affiliation'), 'width' => '90%'],
                                                        ['data' => (string)$this->t('Show'), 'width' => '5%'],
                                                        ['data' => (string)$this->t('Weight'), 'width' => '5%']
                                                ]
                                        ],
                                        '#prefix' => '<div id="add_affiliations_'.$i.'">',
                                        '#suffix' => '</div>'
                                ]
                        ];

                        $affIndex = 0;
                        if(!$affiliations) $affiliations = [];

                        foreach ($affiliations ?? [] as $affiliation) {

                                $affString = implode('; ', [
                                        $affiliation['department'],
                                        $affiliation['institute'],
                                        $affiliation['city'],
                                        Publisso::tools()->getCountry($affiliation['country'])
                                ]);

                                $form['wrapper_authors_db']['authors'][$i]['wrapper-add_affiliation']['add_affiliations'][$affIndex] = [
                                        [
                                                'affiliation' => [
                                                        '#type'          => 'textfield',
                                                        '#default_value' => $affString,
                                                        '#attributes'    => [
                                                                'title' => $affString,
                                                                'readonly' => 'readonly'
                                                        ]
                                                ]
                                        ],
                                        [
                                                'show' => [
                                                        '#type'          => 'checkbox',
                                                        '#default_value' => $author['add_affiliations'][$affIndex]['show'] ?? 0,
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'prevent'         => 'blur',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                ]
                                        ],
                                        [
                                                'weight' => [
                                                        '#type'          => 'number',
                                                        '#default_value' => $author['add_affiliations'][$affIndex]['weight'] ?? null,
                                                        '#attributes'    => [
                                                                'style' => 'width: 5em;'
                                                        ],
                                                        '#ajax'          => [
                                                                'event'           => 'change',
                                                                'prevent'         => 'blur',
                                                                'progress'        => [
                                                                        'type' => 'none'
                                                                ],
                                                                'callback'        => '::autosave',
                                                                'disable-refocus' => true
                                                        ],
                                                ]
                                        ]
                                ];
                                $affIndex++;
                        }
                }
        }

        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormAuthorsAdd(array &$form, FormStateInterface $form_state) {

                $authors = json_decode($this->workflow()->getDataElement('author_add'), 1) ?? [];

                if(!$form_state->has('cntAuthorsAdd')) $form_state->set('cntAuthorsAdd', count($authors));
                if($form_state->get('cntAuthorsAdd') < 1) $form_state->set('cntAuthorsAdd', 1);

                $form['wrapper_authors_add'] = [
                        '#type' => 'fieldset',
                        '#collapsible' => true,
                        '#title' => (string) $this->t('Additional author(s)'),
                        '#tree' => true,
                        'authors' => [
                                '#type' => 'container',
                                '#prefix' => '<div id="wrapper_authors_add">',
                                '#suffix' => '</div>'
                        ]
                ];

                $form['wrapper_authors_add']['btn_add_author'] = [
                        '#type' => 'submit',
                        '#value' => (string) $this->t('Add box'),
                        '#prefix' => '<br>',
                        '#limit_validation_errors' => [],
                        '#submit' => ['::addAuthorAdd'],
                        '#ajax' => [
                                'wrapper' => 'wrapper_authors_add',
                                'callback' => '::addAuthorAddCallback'
                        ]
                ];

                for($i = 0; $i < $form_state->get('cntAuthorsAdd'); $i++){

                        $addAffiliations = $authors[$i]['add_affiliations'] ?? [];

                        if(!$form_state->has('cntAuthorsAdd'.$i.'Affiliations')) $form_state->set('cntAuthorsAdd'.$i.'Affiliations', count($addAffiliations));

                        $form['wrapper_authors_add']['authors'][$i] = [
                                '#type' => 'fieldset',
                                '#collapsible' => true,
                                '#title' => (string)$this->t('Additional author #@nr', ['@nr' => $i + 1]),
                                'firstname' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('First name'),
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $authors[$i]['firstname'] ?? null
                                ],
                                'lastname' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Last name'),
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $authors[$i]['lastname'] ?? null
                                ],
                                'affiliation' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Affiliation'),
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $authors[$i]['affiliation'] ?? null
                                ],
                                'add_affiliations' => [
                                        '#type' => 'container',
                                        '#prefix' => '<div id="wrapper_author_add_'.$i.'_affiliations">',
                                        '#suffix' => '<br></div>',
                                        'affiliations' => []
                                ],
                                'btn_add_affiliation' => [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Add affiliation'),
                                        '#suffix' => '<br><br>',
                                        '#name' => 'author_add_'.$i.'_affiliation_add',
                                        '#submit' => ['::AuthorAddAffiliationAdd'],
                                        '#attributes' => [
                                                'number' => $i
                                        ],
                                        '#limit_validation_errors' => [],
                                        '#ajax' => [
                                                'callback' => '::AuthorAddAffiliationAddCallback',
                                                'wrapper' => 'wrapper_author_add_'.$i.'_affiliations'
                                        ]
                                ],
                                'weight' => [
                                        '#type' => 'number',
                                        '#title' => (string)$this->t('Position'),
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $authors[$i]['weight'] ?? null
                                ]
                        ];

                        for($ii = 0; $ii < $form_state->get('cntAuthorsAdd'.$i.'Affiliations'); $ii++){

                                $form['wrapper_authors_add']['authors'][$i]['add_affiliations'][$ii] = [
                                        '#type' => 'textfield',
                                        '#ajax'          => [
                                                'event'           => 'change',
                                                'prevent'         => 'blur',
                                                'progress'        => [
                                                        'type' => 'none'
                                                ],
                                                'callback'        => '::autosave',
                                                'disable-refocus' => true
                                        ],
                                        '#default_value' => $addAffiliations[$ii] ?? null
                                ];
                        }
                }
        }

        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthorDB(array &$form, FormStateInterface $form_state){
                $form_state->set('cntAuthorsDB', $form_state->get('cntAuthorsDB') + 1);
                $form_state->setRebuild();
        }

        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorDBCallback(array &$form, FormStateInterface $form_state){
                return $form['wrapper_authors_db']['authors'];
        }

        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthorAdd(array &$form, FormStateInterface $form_state){
                $form_state->set('cntAuthorsAdd', $form_state->get('cntAuthorsAdd') + 1);
                $form_state->setRebuild();
        }

        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorAddCallback(array &$form, FormStateInterface $form_state){
                return $form['wrapper_authors_add']['authors'];
        }

        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed|string[]
         */
        public function autosave(array &$form, FormStateInterface $form_state){
                return SubmitBookChapterHelper::autosave($form, $form_state, $this->workflow());
        }

        protected function workflow() :? ChapterWorkflow{
                return $this->workflow;
        }

        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function AuthorAddAffiliationAdd(array &$form, FormStateInterface $form_state){

                $number = $form_state->getTriggeringElement()['#attributes']['number'];
                $form_state->set('cntAuthorsAdd'.$number.'Affiliations', $form_state->get('cntAuthorsAdd'.$number.'Affiliations') + 1);
                $form_state->setRebuild();
        }

        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function AuthorAddAffiliationAddCallback(array &$form, FormStateInterface $form_state){
                $number = $form_state->getTriggeringElement()['#attributes']['number'];
                return $form['wrapper_authors_add']['authors'][$number]['add_affiliations'];
        }
}
