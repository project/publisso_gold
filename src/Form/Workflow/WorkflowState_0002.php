<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\Workflow\WorkflowState_0002.
         */
        namespace Drupal\publisso_gold\Form\Workflow;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\Workflow\Comment;

        /**
         * Provides a simple example form.
         */
        class WorkflowState_0002 extends FormBase {
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId() {
                        return 'workflow.WorkflowState_0002';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                        
                        if(array_key_exists('wf_id', $args)){
        
                                if(!$form_state->has('wf_id')) $form_state->set('wf_id', $args['wf_id']);
                                if(!$form_state->get('wf_id')) return $form;
                                
                                $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                                #$workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($form_state->get('wf_id'));
        
                                $actionItems = $workflow->getSchemaItemActions(null, true);
                                $options = [];
        
                                $comment_options = [];
        
                                foreach($actionItems as $item){
                                        $options[$item['schema_follower']] = (string)$this->t($item['schema_select_action']);
                                        if($item['schema_comment_required'] == 1) $comment_options[] = $item['schema_follower'];
                                }
        
                                $comment_required = [];
                                $not_comment_required = [];
                                foreach($comment_options as $_){
                
                                        if(count($comment_required)) $comment_required[] = 'or';
                                        $comment_required[] = [':input[name="follower"]' => array('value' => $_)];
                                        $not_comment_required[] = [':input[name="follower"]' => array('!value' => $_)];
                                }
        
                                $form['follower'] = [
                                        '#type' => 'select',
                                        '#title' => (string)$this->t('Select action'),
                                        '#required' => true,
                                        '#options' => $options
                                ];
        
                                $medium = $workflow->getMedium();
                                
                                $eicList= [];
                                foreach($medium->readEditorsInChief() as $eic){
                                        if(!in_array($eic->getElement('id'), $eicList)){
                                                $eicList[$eic->getElement('id')] = $eic->profile->getReadableFullName();
                                        }
                                }
        
                                $form['eic-list'] = [
                                        '#type' => 'fieldset',
                                        '#title' => t ( 'Assign EiC' ),
                                        'content' => [
                        
                                                'eic' => [
                                                        '#title' => t ( 'Available users for EiC' ),
                                                        '#type' => 'select',
                                                        '#multiple' => true,
                                                        '#options' => $eicList,
                                                        '#default_value' => explode ( ',', $workflow->getElement('assigned_to_eic') ),
                                                        '#size' => 20,
                                                        '#states'    => array(
                                                                'required'     => [':input[name="follower"]' => [['value' => 6], ['value' => 21], ['value' => 3]]],
                                                        )
                                                ],
                                        ],
                                        '#states'    => array(
                                                'visible'     => [':input[name="follower"]' => [['value' => 6], ['value' => 21], ['value' => 3]]]
                                        )
                                ];
        
                                $form['comment_for_author'] = [
                                        '#type' => 'textarea',
                                        '#title' => (string)$this->t('Comment for author'),
                                        '#states' => [
                                                'visible'     => [':input[name="follower"]' => array('value' => 4)],
                                                'required'     => [':input[name="follower"]' => array('value' => 4)],
                                                'invisible'   => [':input[name="follower"]' => array('!value' => 4)],
                                                'optional'   => [':input[name="follower"]' => array('!value' => 4)],
                                        ],
                                        '#placeholder' => (string)$this->t('Please note: your comment may be also visible to others involved in the book publication.')
                                ];
        
                                $form['comment'] = [
                                        '#type' => 'textarea',
                                        '#title' => (string)$this->t('Comment for editorial office'),
                                        '#states' => [
                                                'visible'     => [':input[name="follower"]' => array('value' => 21)],
                                                'invisible'   => [':input[name="follower"]' => array('!value' => 21)],
                                        ],
                                        '#placeholder' => (string)$this->t('Please note: your comment may be also visible to others involved in the book publication.')
                                ];
                                
                                $form['actions'] = [
                                        '#type' => 'actions'
                                ];
                                
                                $form['actions']['submit'] = [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Submit'),
                                        '#suffix' => '<br><br>'
                                ];
                        }
                        else{
                                \Drupal::service('messenger')->addError('No item provided!');
                        }
        
                        return $form;
                }
                
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm(array &$form, FormStateInterface $form_state) {
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm(array &$form, FormStateInterface $form_state) {
                        
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        
                        switch($form_state->getValue('follower')){
                                
                                case 4: //revision
                                        $comment = $form_state->getValue( 'comment_for_author' );
        
                                        if ( !empty( $comment ) ) {
                                                Comment::new( $form_state->get( 'wf_id' ), $workflow->getElement( 'schema_identifier' ), $comment, null, null, $workflow->getSchemaCurrentRole(), 'submitting_author' );
                                        }
                                        break;
                                        
                                case 21: //accept
                                        $comment = $form_state->getValue( 'comment' );
        
                                        if ( !empty( $comment ) ) {
                                                Comment::new( $form_state->get( 'wf_id' ), $workflow->getElement( 'schema_identifier' ), $comment, null, null, $workflow->getSchemaCurrentRole() );
                                        }
                                        break;
                                        
                                case 6: //assign eic
                                case 3: //reject
                                case 21: //accept
                                        $workflow->setElement('assigned_to_eic', implode(',', $form_state->getValue('eic')));
                                        break;
                                        
                        }
        
                        $workflow->newSchemaState($form_state->getValue('follower'));
                        $workflow->setElement ( 'editor_last_edited', Publisso::currentUser()->getId() );
                        $workflow->unlock ();
        
                        $addParams = [];
                        $fragment = [];
                        if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                                $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                                $fragment = ['fragment' => 'rwDashboardItem-'.$form_state->get('wf_id')];
                                \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                        }
                        if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardItem')) {
                                $addParams[ 'item' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardItem' );
                                \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardItem');
                        }
        
                        $form_state->setRedirect('publisso_gold.dashboard', $addParams, $fragment);
                        return $form;
                }
        }
