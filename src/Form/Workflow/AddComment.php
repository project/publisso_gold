<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\Workflow\AddComment.
         */
        namespace Drupal\publisso_gold\Form\Workflow;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Controller\Publisso;
        use \Drupal\publisso_gold\Controller\Workflow\Comment;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        
        /**
         * Provides a simple example form.
         */
        class AddComment extends FormBase {
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId() {
                        return 'workflow.AddComment';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                        
                        if(array_key_exists('wf_id', $args)){
                                
                                $form_state->set('wf_id', $args['wf_id']);
                                
                                $form['wrapper_comment'] = [
                                        '#type'         => 'details',
                                        '#collapsed'    => true,
                                        '#title'        => (string)$this->t('Add comment')
                                ];
                                $form['wrapper_comment']['comment'] = [
                                        '#type' => 'textarea',
                                        '#title' => (string)$this->t('Comment'),
                                        '#required' => true,
                                        '#limit_validation_error' => ['comment']
                                ];
                                
                                $form['wrapper_comment']['receiver_role'] = [
                                        '#type' => 'select',
                                        '#title' => (string)$this->t('Receiver role'),
                                        '#multiple' => true,
                                        '#required' => true,
                                        '#options' => [
                                                ''                      => (string)$this->t('All but author'),
                                                'publisso'              => (string)$this->t('PUBLISSO'),
                                                'editorial_office'      => (string)$this->t('Editorial office'),
                                                'editors_in_chief'      => (string)$this->t('Editor in chief'),
                                                'editors'               => (string)$this->t('Editor'),
                                                'submitting_author'     => (string)$this->t('Submitting author')
                                        ]
                                ];
                                
                                $form['wrapper_comment']['submit'] = [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Save Comment'),
                                        '#suffix' => '<br><br>'
                                ];
                        }
        
                        return $form;
                }
                
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm(array &$form, FormStateInterface $form_state) {
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm(array &$form, FormStateInterface $form_state) {
                        
                        $tools    = \Drupal::service('publisso_gold.tools');
                        $session  = \Drupal::service('session');
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        
                        $receiver_role = !empty($form_state->getValue('receiver_role')) ? $form_state->getValue('receiver_role') : null;
                        $workflow->getSchemaCurrentRole();
                        $comment = Comment::new($form_state->get('wf_id'), $workflow->getElement('schema_identifier'), $form_state->getValue('comment'), null, null, $workflow->getSchemaCurrentRole(), implode(',', array_filter($receiver_role)));
                        
                        if($comment->getElement('id')) {
                                
                                \Drupal::service( 'messenger' )->addMessage( 'Comment saved' );
                                
                                if($workflow->getElement('state') == 'in review' && in_array('submitting_author', $receiver_role)){
        
                                        $doc = new \DOMDocument();
                                        $doc->loadXML( $workflow->getReviewsheet() );
                                        $xpath = new \DOMXpath( $doc );
        
                                        if ( !$xpath->evaluate( '/reviewsheet/comments' )->length ) {
                
                                                //if no comments added to the review, create root-element "comments"
                                                $tools->DOMAppendChild( 'comments', '', [], $doc, $xpath->evaluate( '/reviewsheet' )[ 0 ] );
                                                //append the comments for the author
                                                $tools->DOMAppendChild( 'comment_for_author', $form_state->getValue('comment'), [], $doc, $xpath->evaluate( '/reviewsheet/comments' )[ 0 ] );
                                        }
                                        else {
                                                //if comments are already saved - update
                                                //check, if comment for the author already exists (normally impossible, but we'll be sure),
                                                if ( !( $xpath->evaluate( '/reviewsheet/comments/comment_for_author' )->length ) ) {
                                                        //append node for comment for author
                                                        $tools->DOMAppendChild( 'comment_for_author', $form_state->getValue('comment'), [], $doc, $xpath->evaluate( '/reviewsheet/comments' )[ 0 ] );
                                                }
                                                else {
                                                        //update node comment for author
                                                        $xpath->evaluate( '/reviewsheet/comments/comment_for_author' )[ 0 ]->nodeValue .= "\n".$form_state->getValue('comment');
                                                }
                                        }
                                        
                                        //save comment for the author
                                        if ( $xpath->evaluate( '/reviewsheet/comments/comment_for_author' )->length ) {
                
                                                $comment = $xpath->evaluate( '/reviewsheet/comments/comment_for_author' )[ 0 ]->nodeValue;
                
                                                if ( !empty( $comment ) ) {
                        
                                                        $comment = [
                                                                'cfa_created_by_uid' => $session->get( 'user' )[ 'id' ],
                                                                'cfa_for_uid'        => $workflow->getElement( 'created_by_uid' ),
                                                                'cfa_comment'        => base64_encode( $comment ),
                                                                'cfa_wfid'           => $form_state->get( 'wf_id' ),
                                                        ];
                        
                                                        \Drupal::database()
                                                               ->insert( 'rwPubgoldWorkflowCommentsForAuthors' )
                                                               ->fields( $comment )
                                                               ->execute()
                                                        ;
                                                }
                                        }
        
                                        $workflow->updateReview( $doc->saveXML() );
                                }
                        }
                        else {
                                \Drupal::service( 'messenger' )->addError( 'Comment not saved' );
                        }
                        
                        return $form;
                }
        }
