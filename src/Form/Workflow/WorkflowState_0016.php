<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowDecisionEic.
         */
        
        namespace Drupal\publisso_gold\Form\Workflow;
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Database\Connection;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\Workflow\Comment;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        
        /**
         * Provides a simple example form.
         */
        class WorkflowState_0016 extends FormBase {
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'WorkflowState_0016';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state, $args = [] ) {
                        
                        if(!$form_state->has('wf_id')) $form_state->set('wf_id', $args['wf_id']);
                        
                        $workflow       = WorkflowManager::getItem($form_state->get('wf_id'));
                        $actionItems    = $workflow->getSchemaItemActions(null, true);
                        
                        $options = [];
                        foreach($actionItems as $item){
                                $options[$item['schema_follower']] = (string)$this->t($item['schema_select_action']);
                        }
                        
                        $form['follower'] = [
                                '#type'         => 'select',
                                '#title'        => (string)$this->t('Select action'),
                                '#options'      => $options,
                                '#required'     => true
                        ];
                        
                        $form['comment'] = [
                                '#type' => 'textarea',
                                '#title' => (string)$this->t('Comment for editorial office'),
                                '#states' => [
                                        'visible' => [
                                                ':input[name="follower"]' => ['value' => 21]
                                        ]
                                ]
                        ];
                        $form['comment_for_author'] = [
                                '#type' => 'textarea',
                                '#title' => (string)$this->t('Comment for author'),
                                '#states' => [
                                        'required' => [
                                                ':input[name="follower"]' => ['value' => 17]
                                        ],
                                        'visible' => [
                                                ':input[name="follower"]' => ['value' => 17]
                                        ]
                                ]
                        ];
                
                        $form[ 'submit' ] = [
                                '#type'  => 'submit',
                                '#value' => (string)t( 'Submit' ),
                        ];
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        
                        switch($form_state->getValue('follower')){
                                
                                case 17:
                                        $comment = $form_state->getValue('comment_for_author');
                                        Comment::new( $form_state->get( 'wf_id' ), $workflow->getElement( 'schema_identifier' ), $comment, null, null, $workflow->getSchemaCurrentRole(), 'submitting_author' );
                                        break;
                                        
                                case 21:
                                        $comment = $form_state->getValue('comment');
                                        Comment::new( $form_state->get( 'wf_id' ), $workflow->getElement( 'schema_identifier' ), $comment, null, null, $workflow->getSchemaCurrentRole());
                                        break;
                        }
                        
                        $workflow->newSchemaState($form_state->getValue('follower'));
                        //save workflow
                        $workflow->setElement( 'eic_last_edited', Publisso::currentUser()->getId() );
                        //unlock workflow
                        $workflow->unlock();
        
                        $addParams = [];
                        if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                                $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                                \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                        }
        
                        $form_state->setRedirect('publisso_gold.dashboard', $addParams, ['fragment' => 'rwDashboardItem-'.$form_state->get('wf_id')]);
                        
                        return $form;
                }
        }
