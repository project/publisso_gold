<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowEicClearedForPublication.
 */
namespace Drupal\publisso_gold\Form\Workflow;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Provides a simple example form.
 */
class ReadyForPublication extends FormBase {
        private $modname = 'publisso_gold';
        private $database;
        private $modpath;
        private $num_corporations;
        public function __construct(Connection $database) {
                $this->database = $database;
        }
        
        /**
         * @param ContainerInterface $container
         * @return ReadyForPublication|static
         */
        public static function create(ContainerInterface $container) {
                return new static ( $container->get ( 'database' ) );
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function getFormId() {
                return 'ReadyForPublication';
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                $texts = \Drupal::service('publisso_gold.texts');
                if(!$form_state->get('wf_id')) $form_state->set('wf_id', $args['wf_id']);
                
                $wf_id = $form_state->get ( 'wf_id' );
                $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($wf_id);
                
                if ($wf_id) {
                        
                        $form_state->set ( 'wf_id', $wf_id );
                        
                        if ($workflow->getDataElement ( 'cp_id' )) {
                                
                                $url = Url::fromRoute ( 'publisso_gold.export.chapter', [
                                        'cp_id' => $workflow->getDataElement ( 'cp_id' )
                                ] );
                                
                                $form ['pdf'] = [
                                        '#type' => 'link',
                                        '#title' => ( string ) $this->t ( 'Download as PDF' ),
                                        '#url' => $url,
                                        '#prefix' => '<span class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;',
                                        '#suffix' => '<br><br>',
                                        '#attributes' => [
                                                'class' => [
                                                        'download-pdf'
                                                ]
                                        ]
                                ];
        
                                $form['pdf_container']['create_pdf'] = [
                
                                        '#type' => 'button',
                                        '#value' => (string)$this->t('Create final PDF'),
                                        '#suffix' => '&nbsp;&nbsp;&nbsp;',
                                        '#ajax' => [
                                                'callback' => '::createFinalPDF',
                                        ]
                                ];
        
        
                                $form['save_pdf'] = [
                                        '#type' => 'checkbox',
                                        '#default_value' => true,
                                        '#title' => (string)t('Publish final created PDF or create one if not already done.'),
                                        '#suffix' => '<hr>'
                                ];
        
                                $form['published'] = [
                                        '#type'   => 'date',
                                        '#title'  => (string)t($texts->get('workflow.publish_chapter.publish_date', 'fc')),
                                        '#suffix' => '<hr>',
                                        '#default_value' => $workflow->getDataElement('published') ?? ''
                                ];
        
                                $form['send_mail'] = [
                                        '#type' => 'checkbox',
                                        '#default_value' => true,
                                        '#title' => (string)t('Send infomail on publish')
                                ];
                        }
                        
                        $form ['submit'] = [
                                '#type' => 'submit',
                                '#value' => ( string ) t ( 'Publish chapter' ),
                                '#button_type' => 'success',
                                '#prefix' => '<br>',
                                '#suffix' => '<br><br>'
                        ];
                }
                
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function validateForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                
                $session = \Drupal::service ( 'session' );
                
                $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($form_state->get('wf_id'));
        
                if($form_state->getValue('published')) $workflow->setDataElement('published', $form_state->getValue('published'));
                $cp_id = $workflow->publish($form_state->getValue('send_mail') == 1);
                $workflow->newSchemaState($workflow->getSchemaDefaultFollower('schema_follower'));
                
                //Lösche alle PDF, die während des Workflows erstellt wurden
                if($workflow->getDataElement('cp_id')){
                        $chapter = new \Drupal\publisso_gold\Controller\Bookchapter($workflow->getDataElement('cp_id'));
                
                        if($chapter->getElement('pdf_blob_id')){
                                $blob = new \Drupal\publisso_gold\Controller\Blob($chapter->getElement('pdf_blob_id'));
                                $blob->delete($chapter->getElement('pdf_blob_id'));
                                $chapter->setElement('pdf_blob_id', null);
                        }
                }
        
                //erstelle neues PDF oder speicher ein vorher erstelltes, wenn gewünscht
                if($form_state->getValue('save_pdf')){
                
                        $tempstore = \Drupal::service('user.private_tempstore')->get('pg_pdf_bin');
                
                        if($tempstore->get('name') && $tempstore->get('type') && $tempstore->get('data')){
                        
                                $chapter = new \Drupal\publisso_gold\Controller\Bookchapter($cp_id);
                                $blob = new \Drupal\publisso_gold\Controller\Blob();
                                $blob->create($tempstore->get('data'), $tempstore->get('type'), serialize(['name' => $tempstore->get('name')]));
                                $chapter->setElement('pdf_blob_id', $blob->getId());
                                $tempstore->delete('name');
                                $tempstore->delete('type');
                                $tempstore->delete('data');
                        }
                        else{
                                $url = \Drupal\Core\Url::fromRoute('publisso_gold.export.chapter', ['cp_id' => $cp_id, 'type' => 'pdf']);
                                $url->setAbsolute();
                                \Drupal::httpClient()->request('GET', $url->toString(), ['verify' => false, 'headers' => ['Content-Type' => 'application/text'], 'http_errors' => false]);
                        }
                }
        
                $workflow->newSchemaState($workflow->getSchemaDefaultFollower('schema_follower'));
                $workflow->unlock();
        
                $addParams = [];
                if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                        $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                        \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                }
        
                $form_state->setRedirect('publisso_gold.dashboard', $addParams, ['fragment' => 'rwDashboardItem-'.$form_state->get('wf_id')]);
                
                return $form;
        }
        
        /**
         * @param null $tmpl
         * @param null $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl = NULL, $vars = NULL) {
                if ($tmpl == NULL || $vars == NULL) {
                        // set Site-Vars
                        $this->tmpl = str_replace ( array_keys ( $this->tmpl_vars ), array_values ( $this->tmpl_vars ), $this->tmpl );
                        
                        // remove unused vars
                        $this->tmpl = preg_replace ( '(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl );
                } else {
                        // set Site-Vars
                        $tmpl = str_replace ( array_keys ( $vars ), array_values ( $vars ), $tmpl );
                        
                        // remove unused vars
                        return preg_replace ( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
                }
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return AjaxResponse
         * @throws \Exception
         */
        public function createFinalPDF(array &$form, FormStateInterface $form_state){
                
                $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                
                if(!$workflow->getDataElement('cp_id')){
                        $workflow->publish(false, true);
                }
                
                $chapter = new \Drupal\publisso_gold\Controller\Bookchapter($workflow->getDataElement('cp_id'));
                
                if(!empty($workflow->getDataElement('references'))){
                        
                        $chapter->setElement('chapter_text', base64_encode(\Drupal::service('publisso_gold.tools')->mapReferences(
                                base64_decode($workflow->getDataElement('chapter_text')), $workflow->getDataElement('references'))));
                }
                
                $url = \Drupal\Core\Url::fromRoute('publisso_gold.export.chapter.volatile', ['cp_id' => $chapter->getElement('id')]);
                $url->setAbsolute();
                $response = new AjaxResponse();
                $command = new InvokeCommand(
                        'html',
                        'trigger',
                        [
                                'openPDF',
                                [
                                        'url' => $url->toString()
                                ]
                        ]
                );
                $response->addCommand($command);
                return $response;
        }
}
