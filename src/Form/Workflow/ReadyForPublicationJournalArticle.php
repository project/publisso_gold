<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowEicClearedForPublication.
 */
namespace Drupal\publisso_gold\Form\Workflow;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\publisso_gold\Controller\Blob;
use Drupal\publisso_gold\Controller\Manager\ArticleManager;
use Drupal\publisso_gold\Controller\Manager\BlobManager;
use Drupal\publisso_gold\Controller\Manager\JournalManager;
use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
use Drupal\publisso_gold\Controller\Publisso;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Provides a simple example form.
 */
class ReadyForPublicationJournalArticle extends FormBase {
        private $modname = 'publisso_gold';
        private $database;
        private $modpath;
        private $num_corporations;
        
        protected $workflow, $article, $journal, $setup, $tools, $texts;
        
        public function __construct(Connection $database) {
                $this->database = $database;
        }
        
        /**
         * @param ContainerInterface $container
         * @return ReadyForPublicationJournalArticle|static
         */
        public static function create(ContainerInterface $container) {
                return new static ( $container->get ( 'database' ) );
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function getFormId() {
                return 'ReadyForPublicationJournalArticle';
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
        
                if(!$form_state->get('wf_id')) $form_state->set('wf_id', $args['wf_id']);
                if(!$form_state->get('wf_id')) return $form;
                
                $this->workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                $this->journal = JournalManager::getJournal($this->workflow->getDataElement('jrn_id'));
                $this->article = ArticleManager::getArticle($this->workflow->getDataElement('jrna_id'));
                $this->setup = Publisso::setup();
                $this->tools = Publisso::tools();
                $this->texts = Publisso::text();
                
                $url = Url::fromRoute ( 'publisso_gold.export.pdf.article', [
                        'jrna_id' => $this->article->getElement('id')
                ] );
                
                $form ['pdf'] = [
                        '#type' => 'link',
                        '#title' => ( string ) $this->t ( 'Download as PDF' ),
                        '#url' => $url,
                        '#prefix' => '<span class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;',
                        '#suffix' => '<br><br>',
                        '#attributes' => [
                                'class' => [
                                        'download-pdf'
                                ]
                        ]
                ];

                $form['pdf_container']['create_pdf'] = [

                        '#type' => 'button',
                        '#value' => (string)$this->t('Create final PDF'),
                        '#suffix' => '&nbsp;&nbsp;&nbsp;',
                        '#ajax' => [
                                'callback' => '::createFinalPDF',
                        ]
                ];


                $form['save_pdf'] = [
                        '#type' => 'checkbox',
                        '#default_value' => true,
                        '#title' => (string)t('Publish final created PDF or create one if not already done.'),
                        '#suffix' => '<hr>'
                ];

                $form['published'] = [
                        '#type'   => 'date',
                        '#title'  => (string)t($this->texts->get('workflow.publish_article.publish_date', 'fc')),
                        '#suffix' => '<hr>',
                        '#default_value' => $this->workflow->getDataElement('published') ?? ''
                ];

                $form['send_mail'] = [
                        '#type' => 'checkbox',
                        '#default_value' => true,
                        '#title' => (string)t('Send infomail on publish')
                ];
                
                $form ['submit'] = [
                        '#type' => 'submit',
                        '#value' => ( string ) t ( 'Publish article' ),
                        '#button_type' => 'success',
                        '#prefix' => '<br>',
                        '#suffix' => '<br><br>'
                ];
                
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function validateForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                
                if($form_state->getValue('published')) $this->workflow->setDataElement('published', $form_state->getValue('published'));
                $this->workflow->publish($form_state->getValue('send_mail') == 1);
                
                /*
                //Lösche alle PDF, die während des Workflows erstellt wurden
                if(null !== ($blob = BlobManager::get($this->article->getElement('pdf_blob_id')))){
                        $blob->delete($this->article->getElement('pdf_blob_id'));
                        $this->article->setElement('pdf_blob_id', null);
                }
        
                //erstelle neues PDF oder speichere ein vorher erstelltes, wenn gewünscht
                if($form_state->getValue('save_pdf')){
                
                        $tempstore = \Drupal::service('user.private_tempstore')->get('pg_pdf_bin');
                
                        if($tempstore->get('name') && $tempstore->get('type') && $tempstore->get('data')){
        
                                $blob = new Blob();
                                $blob->create($tempstore->get('data'), $tempstore->get('type'), serialize(['name' => $tempstore->get('name')]));
                                $this->article->setElement('pdf_blob_id', $blob->getId());
                                $tempstore->delete('name');
                                $tempstore->delete('type');
                                $tempstore->delete('data');
                        }
                        else{
                                $url = Url::fromRoute('publisso_gold.export.article', ['jrna_id' => $this->article->getElement('id'), 'type' => 'pdf']); //Todo: change to article
                                $url->setAbsolute();
                                \Drupal::httpClient()->request('GET', $url->toString(), ['headers' => ['Content-Type' => 'application/text'], 'http_errors' => false]);
                        }
                }
                */
                
                $this->workflow->newSchemaState($this->workflow->getSchemaDefaultFollower('schema_follower'));
                
                $this->workflow->unlock();
        
                $addParams = [];
                if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                        $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                        \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                }
        
                $form_state->setRedirect('publisso_gold.dashboard', $addParams, ['fragment' => 'rwDashboardItem-'.$form_state->get('wf_id')]);
                
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return AjaxResponse
         */
        public function createFinalPDF(array &$form, FormStateInterface $form_state){
                
                
                if(!$this->workflow->getDataElement('jrna_id')){
                        $this->workflow->publish(false, true);
                }
                
                if(!empty($this->workflow->getDataElement('references'))){
                        
                        $this->article->setElement('article_text', base64_encode($this->tools->mapReferences(
                                base64_decode($this->workflow->getDataElement('article_text')), $this->workflow->getDataElement('references'))));
                }
                
                $url = Url::fromRoute('publisso_gold.export.article.volatile', ['jrna_id' => $this->article->getElement('id')]);
                $url->setAbsolute();
                $response = new AjaxResponse();
                $command = new InvokeCommand(
                        'html',
                        'trigger',
                        [
                                'openPDF',
                                [
                                        'url' => $url->toString()
                                ]
                        ]
                );
                $response->addCommand($command);
                return $response;
        }
}
