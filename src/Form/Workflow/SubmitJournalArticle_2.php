<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\Workflow\SubmitJournalArticle_2.
 */
namespace Drupal\publisso_gold\Form\Workflow;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\publisso_gold\Controller\Manager\JournalManager;
use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
use Drupal\publisso_gold\Controller\Medium\Journal;
use Drupal\publisso_gold\Controller\Workflow\ArticleWorkflow;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides article create form.
 */
class SubmitJournalArticle_2 extends FormBase {
        
        protected $workflow = null;
        protected $journal  = null;
        
        /**
         * @return string
         */
        public function getFormId() {
                return 'SubmitJournalArticle_2';
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @return array
         * @throws \Exception
         */
        public function buildForm(array $form, FormStateInterface $form_state, array $args = []) {
                
                $this->preBuild($form_state, $args);
                if(!$this->workflow()){ $this->messenger()->addError('Couldnt load item!'); return $form; }
        
                $this->getForm          ($form, $form_state);
                $this->getActions       ($form, $form_state);
                
                $form['#cache'] = ['max-age' => 0];
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                
                SubmitJournalArticleHelper::finishSubmission($form_state, $this->journal);
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function proceedStep1(array &$form, FormStateInterface $form_state){
        
                $this->saveForm($form, $form_state);
                $ts = \Drupal::service('tempstore.private')->get('publisso_gold');
                $ts->set('overwrite-SubmitJournalArticle_1', 'Form\Workflow\SubmitJournalArticle_1');
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function proceedStep3(array &$form, FormStateInterface $form_state){
                
                $ts = \Drupal::service('tempstore.private')->get('publisso_gold');
                $ts->set('overwrite-SubmitJournalArticle_1', 'Form\Workflow\SubmitJournalArticle_3');
                $this->saveForm($form, $form_state);
                return $form;
        }
        
        /**
         * @param FormStateInterface $form_state
         * @param array $args
         * @throws \Exception
         */
        protected function preBuild(FormStateInterface $form_state, array $args = []){
                if(!$form_state->get('wf_id')) $form_state->set('wf_id', $args['wf_id']);
                $this->setWorkflow($form_state->get('wf_id'));
                $this->setJournal();
                if($this->workflow()) $this->workflow()->publish(false, true);
        }
        
        /**
         * @param int $wf_id
         */
        protected function setWorkflow(int $wf_id){
                if(!$this->workflow) $this->workflow = WorkflowManager::getItem($wf_id);
        }
        
        protected function setJournal(){
                $this->journal = JournalManager::getJournal($this->workflow()->getDataElement('jrn_id'));
        }
        
        protected function workflow() :? ArticleWorkflow {
                return $this->workflow;
        }
        
        protected function journal() : Journal {
                return $this->journal;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getActions(array &$form, FormStateInterface $form_state){
                $form['actions'] = [
                        '#type' => 'actions',
                        'back_to_journal' => [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Back to journal'),
                                '#submit' => ['::saveForm', '::backToJournal'],
                                '#button_type' => 'warning'
                        ],
                        'back_to_dashboard' => [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Back to dashboard'),
                                '#submit' => ['::saveForm', '::backToDashboard'],
                                '#button_type' => 'warning'
                        ],
                        'preview' => [
                                '#type' => 'button',
                                '#value' => (string)$this->t('Preview'),
                                '#button_type' => 'primary',
                                '#ajax' => [
                                        'callback' => '::preview'
                                ]
                        ],
                        'back' => [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Back'),
                                '#button_type' => 'danger',
                                '#submit' => ['::proceedStep1'],
                                '#limit_validation_errors' => []
                        ],
                        'step3' => [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Continue submission'),
                                '#button_type' => 'success',
                                '#submit' => ['::proceedStep3'],
                                '#states' => [
                                        'visible' => [
                                                ':input[name="add_files"]' => ['checked' => true]
                                        ]
                                ],
                                '#limit_validation_errors' => []
                        ],
                        'finish' => [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Finish'),
                                '#button_type' => 'success',
                                '#states' => [
                                        'visible' => [
                                                ':input[name="add_files"]' => ['checked' => false]
                                        ]
                                ],
                        ]
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getForm(array &$form, FormStateInterface $form_state){
                
                $this->getFormHeader                    ($form, $form_state);
                $this->getFormConflictOfInterest        ($form, $form_state);
                $this->getFormFunding                   ($form, $form_state);
                $this->getFormReviewerSuggestion        ($form, $form_state);
                $this->getFormAddFiles                  ($form, $form_state);
                $this->getFormAcceptPolicy              ($form, $form_state);
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormHeader(array &$form, FormStateInterface $form_state){
                
                $form ['mu_site_headline'] = [
                        '#markup' => (string)$this->t(($this->workflow()->getElement('state') == 'running submission' ? 'Add' : 'Edit').' Article'),
                        '#prefix' => '<h1>',
                        '#suffix' => '</h1>'
                ];
                
                $form['mu_journal_title'] = [
                        '#markup' => $this->journal()->getElement('title'),
                        '#prefix' => '<h2>',
                        '#suffix' => '</h2>'
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormReviewerSuggestion(array &$form, FormStateInterface $form_state){
                
                if($this->journal()->getControlElement('reviewer_suggestion')){
                        
                        $form['reviewer_suggestion'] = [
                                '#type'  => 'textfield',
                                '#title' => t('Reviewer suggestion'),
                                '#placeholder' => (string)$this->t('Enter the names of potential reviewers of your article here'),
                                '#ajax'         => [
                                        'event'         => 'change',
                                        'prevent'       => 'blur',
                                        'progress'      => [
                                                'type'          => 'none'
                                        ],
                                        'callback'      => '::autosave',
                                        'disable-refocus'       => true
                                ],
                                '#default_value' => $this->workflow()->getDataElement('reviewer_suggestion')
                        ];
                }
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormConflictOfInterest(array &$form, FormStateInterface $form_state){
                //conflict of interest
                $form['conflict_of_interest'] = [
                        '#type'         => 'checkbox',
                        '#title'        => (string)t('Conflict of interest'),
                        '#prefix'       => (string)$this->t('Here you may add if there’s any conflict of interest to declare, which might be relevant regarding the development of your article. If there’s anything to declare you can add a description in the appearing box.'),
                        '#ajax'         => [
                                'event'         => 'change',
                                'prevent'       => 'blur',
                                'progress'      => [
                                        'type'          => 'none'
                                ],
                                'callback'      => '::autosave',
                                'disable-refocus'       => true,
                                'suppress_required_fields_validation' => true
                        ],
                        '#default_value' => $this->workflow()->getDataElement('conflict_of_interest')
                ];
        
                $form['conflict_of_interest_text'] = [
                        '#type'          => 'textfield',
                        '#title'         => (string)t('Description of conflict of interest'),
                        '#maxlength'     => 256,
                        '#states'        => array(
                                'visible'       => array(   // action to take.
                                                            ':input[name="conflict_of_interest"]' => array('checked' => true),
                                ),
                                'invisible'     => array(   // action to take.
                                                            ':input[name="conflict_of_interest"]' => array('checked' => false),
                                )
                        ),
                        '#ajax'         => [
                                'event'         => 'change',
                                'prevent'       => 'blur',
                                'progress'      => [
                                        'type'          => 'none'
                                ],
                                'callback'      => '::autosave',
                                'disable-refocus'       => true
                        ],
                        '#default_value' => $this->workflow()->getDataElement('conflict_of_interest_text')
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormFunding(array &$form, FormStateInterface $form_state){
        
                $form['funding'] = [
                        '#type'         => 'checkbox',
                        '#title'        => t('Funds received'),
                        '#prefix'       => (string)$this->t('If you received any funds please add the information here.'),
                        '#ajax'         => [
                                'event'         => 'change',
                                'prevent'       => 'blur',
                                'progress'      => [
                                        'type'          => 'none'
                                ],
                                'callback'      => '::autosave',
                                'disable-refocus'       => true
                        ],
                        '#default_value' => $this->workflow()->getDataElement('funding')
                ];
        
                $form['funding_name'] = [
                        '#type'      => 'textfield',
                        '#title'     => (string)$this->t('Funding name'),
                        '#maxlength' => 256,
                        '#states'    => array(
                                'visible'   => array(   // action to take.
                                                        ':input[name="funding"]' => array('checked' => true),
                                ),
                                'invisible' => array(   // action to take.
                                                        ':input[name="funding"]' => array('checked' => false),
                                )
                        ),
                        '#ajax'         => [
                                'event'         => 'change',
                                'prevent'       => 'blur',
                                'progress'      => [
                                        'type'          => 'none'
                                ],
                                'callback'      => '::autosave',
                                'disable-refocus'       => true
                        ],
                        '#default_value' => $this->workflow()->getDataElement('funding_name')
                ];
        
                $form['funding_id'] = [
                        '#type'   => 'textfield',
                        '#title'  => (string)t('Funding ID'),
                        '#states' => array(
                                'visible'   => array(   // action to take.
                                                        ':input[name="funding"]' => array('checked' => true),
                                ),
                                'invisible' => array(   // action to take.
                                                        ':input[name="funding"]' => array('checked' => false),
                                )
                        ),
                        '#ajax'         => [
                                'event'         => 'change',
                                'prevent'       => 'blur',
                                'progress'      => [
                                        'type'          => 'none'
                                ],
                                'callback'      => '::autosave',
                                'disable-refocus'       => true
                        ],
                        '#default_value' => $this->workflow()->getDataElement('funding_id')
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormAddFiles(array &$form, FormStateInterface $form_state){
                
                $form['add_files'] = [
                        '#type'  => 'checkbox',
                        '#title' => (string)$this->t('I would like to upload (additional) files'),
                        '#ajax'         => [
                                'event'         => 'change',
                                'prevent'       => 'blur',
                                'progress'      => [ 'type' => 'none' ],
                                'callback'      => '::autosave',
                                'disable-refocus'       => true
                        ],
                        '#default_value' => $this->workflow()->getDataElement('add_files')
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormAcceptPolicy(array &$form, FormStateInterface $form_state){
                
                $form['accept_policy'] = [
                        '#type'     => 'checkbox',
                        '#prefix'   => (string)$this->t('Please read the policy (@policy_de, @policy_en) and accept the author\'s contract (legally binding @contract_de, @contract_en) to conclude the submission.', [
                                '@policy_en' => Link::fromTextAndUrl($this->t('English'), Url::fromUri('https://www.publisso.de/en/publishing/books/books-policy/', ['attributes' => ['target' => '_blank']]))->toString(),
                                '@policy_de' => Link::fromTextAndUrl($this->t('German'), Url::fromUri('https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/' ,['attributes' => ['target' => '_blank']]))->toString(),
                                '@contract_en' => Link::fromTextAndUrl($this->t('translated version'), Url::fromUri('https://www.publisso.de/en/publishing/books/books-policy/#c5189', ['attributes' => ['target' => '_blank']]))->toString(),
                                '@contract_de' => Link::fromTextAndUrl($this->t('German version'), Url::fromUri('https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/#c5185', ['attributes' => ['target' => '_blank']]))->toString(),
                        ]),
                        '#title'    => (string)t('I accept the author\'s contract</a>'),
                        '#required' => true,
                        '#access' => !$this->workflow()->getDataElement('editorial'),
                        '#ajax'         => [
                                'event'         => 'change',
                                'prevent'       => 'blur',
                                'progress'      => [
                                        'type'          => 'none'
                                ],
                                'callback'      => '::autosave',
                                'disable-refocus'       => true
                        ],
                        '#default_value' => $this->workflow()->getDataElement('accept_policy')
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed|string[]
         */
        public function autosave(array &$form, FormStateInterface $form_state){
                return SubmitJournalArticleHelper::autosave($form, $form_state, $this->workflow);
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function saveForm(array &$form, FormStateInterface $form_state){
                
                foreach(SubmitJournalArticleHelper::getSimpleValues() as $field){
                        if($form_state->hasValue($field)) $this->workflow()->setDataElement($field, $form_state->getValue($field));
                }
                
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function backToJournal(array &$form, FormStateInterface $form_state){
                $form_state->setRedirectUrl($this->journal()->getUrl());
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function backToDashboard(array &$form, FormStateInterface $form_state){
                $form_state->setRedirect('publisso_gold.dashboard');
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return \Drupal\Core\Ajax\AjaxResponse
         */
        public function preview(array &$form, FormStateInterface $form_state){
                return SubmitJournalArticleHelper::preview($form_state);
        }
}
