<?php
/**
* @file
* Contains \Drupal\publisso_gold\Form\displayReviewsheetSkel.
*/
namespace Drupal\publisso_gold\Form\Workflow;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
* Provides a simple example form.
*/
class displayReviewsheetSkel extends FormBase {

        /**
        * {@inheritdoc}
        */
        public function getFormId() {
                return 'displayReviewsheetSkel';
        }

        /**
        * {@inheritdoc}
        */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($args['wf_id']);
                $form['data'] = $workflow->getReviewsheetForm($args['args']);
                $form['#cache']['max-age'] = 0;
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function validateForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
}
