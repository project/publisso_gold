<?php
        
        
        namespace Drupal\publisso_gold\Form\Workflow;

        use Drupal\Core\Ajax\AjaxResponse;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Classes\Ajax\OpenUrlCommand;
        use Drupal\publisso_gold\Controller\Bookchapter;
        use Drupal\publisso_gold\Controller\Manager\ChapterManager;
        use Drupal\publisso_gold\Controller\Manager\UserManager;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\User;
        use Drupal\publisso_gold\Controller\Workflow\ChapterWorkflow;
        use Drupal\publisso_gold\Controller\Workflow\WorkflowInfoMail;

        /**
         * Class SubmitBookChapterHelper
         * @package Drupal\publisso_gold\Form\Workflow
         */
        class SubmitBookChapterHelper {
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed|string[]
                 */
                public static function autosave(array &$form, FormStateInterface $form_state){
                        
                        self::saveUserInput($form_state->getUserInput(), $form_state->get('wf_id'));
                        
                        $te = $form_state->getTriggeringElement();
                        $form_state->setRebuild();
                        
                        if ($te['#array_parents'][0] == 'wrapper_authors_db' && $te['#array_parents'][3] == 'author') {
                                $i = $te['#array_parents'][2];
                                
                                if(isset($form['wrapper_authors_db']['authors'][$i]['wrapper-add_affiliation']['add_affiliations'][0]))
                                        $form['wrapper_authors_db']['authors'][$i]['wrapper-add_affiliation']['#collapsed'] = false;
                                
                                return $form['wrapper_authors_db']['authors'][$i]['wrapper-add_affiliation'];
                        }
                        
                        return ['#markup' => ''];
                }
        
                /**
                 * @param $var
                 * @return bool
                 */
                private static function filterAuthorsDB($var){
                        return !empty($var['author']);
                }
        
                public static function getAuthorFromString(string $authorString) :? User {
                
                        $author           = $authorString;
                        [$author, $data] = preg_split('/;\s{1,}/', $author, 2);
                
                        if(!empty($author)){
                                $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                $found = 0;
                                $last_id = null;
                                $string = '';
                        
                                foreach($res as $v){
                                        $string .= ' || '.implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, Publisso::tools()->getCountry($v->up_country)])).' | '.implode( '; ', [$author, $data]);
                                        if(
                                                implode( '; ', [$author, $data])
                                                ==
                                                implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, Publisso::tools()->getCountry($v->up_country)]))
                                        ){
                                                $found++;
                                                $last_id = $v->up_uid;
                                        }
                                }
                        
                                if($found === 1){
                                
                                        return new User($last_id);
                                }
                        }
                
                        return null;
                }
                
                public static function getSimpleValues() : array {
        
                        return [
                                'title',
                                'title_short',
                                'keywords',
                                'abstract',
                                'chapter_text',
                                'references',
                                'conflict_of_interest',
                                'conflict_of_interest_text',
                                'funding',
                                'funding_id',
                                'funding_name',
                                'accept_policy',
                                'reviewer_suggestion',
                                'add_files'
                        ];
                }
        
                /**
                 * @param array $userInput
                 * @param $wf_id
                 */
                public static function saveUserInput(array $userInput, $wf_id){
                        
                        $workflow = WorkflowManager::getItem($wf_id);
                        $simpleValues = self::getSimpleValues();
                        
                        foreach($userInput as $field => $value){
                
                                if(in_array($field, $simpleValues)){
                                        $workflow->setDataElement($field, $value, false);
                                }
                                else{
                                        if($field == 'wrapper_authors_db'){
                                
                                                $authors = self::getAuthorsDBFromForm($value);
                                                $workflow->setDataElement('author_db', json_encode($authors), false);
                                        }
                                        
                                        if($field == 'wrapper_authors_add'){
                                                $workflow->setDataElement('author_add', json_encode($value['authors']), false);
                                        }
                                        
                                        $chapter = null;
                                        self::mergeAuthors($workflow, $chapter, false);
                                        
                                        if($field == 'wrapper_corporations'){
                                                $workflow->setDataElement('corporations', json_encode($value['corporation']), false);
                                        }
                                }
        
                                $workflow->save();
                        }
                }
        
                /**
                 * @param FormStateInterface $form_state
                 * @param $book
                 * @return bool
                 * @throws \Exception
                 */
                public static function finishSubmission(FormStateInterface $form_state, &$book) : bool {
                        
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        if(!$workflow) return false;
                        
                        $chapter = null;
                        self::mergeAuthors($workflow, $chapter, true);
                        
                        //keywords
                        $keywords = array_filter(array_map('trim', preg_split('/[,;]/', $workflow->getDataElement('keywords'))));
                        $workflow->setDataElement('keywords', json_encode($keywords), false);
                        
                        $workflow->publish(false, true);
                        
                        if($workflow->hasDataElement('abstract')) $workflow->setDataElement('abstract', base64_encode($workflow->getDataElement('abstract')));
                        $workflow->setDataElement('chapter_text', base64_encode($workflow->getDataElement('chapter_text')));
                        
                        $follower_state = $workflow->getSchemaDefaultFollower('schema_follower') ?? null;
                        $new_status     = $workflow->getSchemaFollowers($follower_state)[0]['schema_status'];
                        $mailtemplate   = $workflow->getSchemaItemByIdentifierAndFollower($workflow->getElement('schema_identifier'), $follower_state)['schema_mailtemplate_leave'];
        
                        if(!$follower_state){
                                \Drupal::service('messenger')->addError((string)\Drupal::service('string_translation')->translate('Cant proceed workflow (no follower determined)!'));
                                return false;
                        }
        
                        //read Editorial Office
                        $eo = $book->readEditorialOffice();
        
                        $assigned_users = [];
        
                        if(count($eo) > 0){ //submit to editorial office
                
                                $submission_assign = '';
                
                                foreach($eo as $_){
                        
                                        $submission_assign .= (!empty($submission_assign) ? ',' : '') . 'u:'.$_->getElement('id');
                        
                                        if(!in_array($_->getElement('id'), $assigned_users))
                                                $assigned_users[] = $_->getElement('id');
                                }
        
                                $eo_str = '';
        
                                foreach($eo as $_){
                                        $eo_str .= (!empty($eo_str) ? ',' : '') . $_->getElement('id');
                                }
        
                                $workflow->setElement('assigned_to_eo', $eo_str, false);
                        }
                        else{ //submission to publisso
                                $role_id = \Drupal::service('publisso_gold.setup')->getValue('default.admin.role.id');
                                $submission_assign = "r:$role_id";
                
                                //set these users as eo
                                $assigned_users = \Drupal::service('publisso_gold.tools')->getUserIDsByRole($role_id);
                
                                foreach($assigned_users as $_)
                                        $eo[] = UserManager::getUserFromID($_);
                        }
        
                        $workflow->setElement('assigned_to', $submission_assign);
                        $workflow->setElement('state', $new_status);
                        $workflow->setElement('schema_identifier', $follower_state);
        
                        /**
                         * if editorial workflow, the first author "is_sorresponding" will
                         * be set as creator
                         **/
                        if($workflow->getDataElement('editorial')){
                
                                foreach(json_decode($workflow->getDataElement('author_db'), true) as $_){
                                        
                                        if($_['is_corresponding'] && $_['uid']){
                                                $workflow->setElement('created_by_uid', $_['uid'], false);
                                                break;
                                        }
                                }
                        }
                        
                        $workflow->setElement('modified', date('Y-m-d H:i:s'), false);
                        $workflow->save();
                        $workflow->unlock();
        
                        if($mailtemplate) {
                                $mail = new WorkflowInfoMail($workflow, $mailtemplate);
                                if ($mail) $mail->send();
                        }
                        
                        $ts = \Drupal::service('tempstore.private')->get('publisso_gold');
                        $ts->delete('overwrite-SubmitBookChapter_1');
                        $form_state->setRedirect('publisso_gold.dashboard');
                        return true;
                }
        
                /**
                 * @param FormStateInterface $form_state
                 * @return AjaxResponse
                 * @throws \Exception
                 */
                public static function preview(FormStateInterface $form_state){
                        
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        
                        if($workflow) {
                                
                                $workflow->publish(false, true);
                                $chapter = ChapterManager::getChapter($workflow->getDataElement('cp_id'));
                                
                                if($chapter->getElement('abstract')) $chapter->setElement('abstract', base64_encode($chapter->getElement('abstract')));
                                if($chapter->getElement('chapter_text')) $chapter->setElement('chapter_text', base64_encode($chapter->getElement('chapter_text')));
                                
                                $keywords = array_filter(array_map('trim', preg_split('/[,;]/', $workflow->getDataElement('keywords'))));
                                $chapter->setElement('keywords', json_encode($keywords), false);
                                
                                self::mergeAuthors($workflow, $chapter, true);
                                
                                $response = new AjaxResponse();
                                $response->addCommand(new OpenUrlCommand(null, $chapter->getLink(), '_blank'));
                                return $response;
                        }
                }
                
                public static function getAuthorsDBFromForm(array &$formElement) : array {
                        
                        $authors = [];
        
                        foreach($formElement['authors'] as $author){
                
                                if($author['author']){
                        
                                        $index = count($authors);
                        
                                        $authors[$index] = [
                                                'author' => $author['author'],
                                                'weight' => $author['weight'],
                                                'is_corresponding' => $author['is_corresponding'],
                                                'add_affiliations' => []
                                        ];
                                        
                                        if(null !== ($user = self::getAuthorFromString($author['author'])))
                                                $authors[$index]['uid'] = $user->getId();
                                        
                                        if(is_array($author['wrapper-add_affiliation']['add_affiliations'])){
                                
                                                foreach($author['wrapper-add_affiliation']['add_affiliations'] as $addAffiliation){
                                        
                                                        if(count($addAffiliation) && $addAffiliation[0]['affiliation']){
                                                                $authors[$index]['add_affiliations'][] = [
                                                                        'affiliation' => $addAffiliation[0]['affiliation'],
                                                                        'show' => $addAffiliation[1]['show'] ?? 0,
                                                                        'weight' => $addAffiliation[2]['weight'] ?? null
                                                                ];
                                                        }
                                                }
                                        }
                                }
                        }
                        
                        return $authors;
                }
        
                /**
                 * @param ChapterWorkflow $workflow
                 * @param Bookchapter|null $chapter
                 * @param bool $autosave
                 */
                public static function mergeAuthors(ChapterWorkflow &$workflow, Bookchapter &$chapter = null, bool $autosave = true){
                        
                        $authors = [];
        
                        foreach(json_decode($workflow->getDataElement('author_db'), true) as $item){
                
                                $author           = $item['author'];
                                $is_corresponding = $item['is_corresponding'];
                                $weight           = $item['weight'];
                                $addAffiliations  = $item['add_affiliations'] ?? [];
                
                                $user = self::getAuthorFromString($author);
                
                                if($user){
                        
                                        $authors[] = [
                                                'weight'             => $weight,
                                                'is_corresponding'   => $is_corresponding,
                                                'firstname'          => $user->profile->getElement('firstname'),
                                                'lastname'           => $user->profile->getElement('lastname'),
                                                'affiliation'        => $user->profile->getElement('institute'),
                                                'uid'                => $user->getElement('id'),
                                                'profile_archive_id' => $user->profile->makeSnapshot(Publisso::currentUser()->getId()),
                                                'add_affiliations'   => $addAffiliations
                                        ];
                                }
                        }
        
                        /**
                         * add the additional authors to workflow
                         **/
                        foreach(json_decode($workflow->getDataElement('author_add') ?? [], true) as $item){
                
                                if(!(empty($item['firstname']) || empty($item['lastname']) || empty($item['affiliation'])))
                                        $authors[] = array_map('trim', $item);
                        }
                        
                        $workflow->setDataElement('authors', json_encode($authors), $autosave);
                        if($chapter) $chapter->setElement('authors', json_encode($authors), $autosave);
                }
        }