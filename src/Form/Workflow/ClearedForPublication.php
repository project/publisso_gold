<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowEicClearedForPublication.
 */
namespace Drupal\publisso_gold\Form\Workflow;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Provides a simple example form.
 */
class ClearedForPublication extends FormBase {
        private $modname = 'publisso_gold';
        private $database;
        private $modpath;
        private $num_corporations;
        public function __construct(Connection $database) {
                $this->database = $database;
        }
        
        /**
         * @param ContainerInterface $container
         * @return ClearedForPublication|static
         */
        public static function create(ContainerInterface $container) {
                return new static ( $container->get ( 'database' ) );
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function getFormId() {
                return 'ClearedForPublication';
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                if(!$form_state->get('wf_id')) $form_state->set('wf_id', $args['wf_id']);
                
                $wf_id = $form_state->get ( 'wf_id' );
                $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($wf_id);
                
                if ($wf_id) {
                        
                        $form_state->set ( 'wf_id', $wf_id );
                        
                        if ($workflow->getDataElement ( 'cp_id' )) {
                                
                                $url = Url::fromRoute ( 'publisso_gold.export.chapter', [
                                        'cp_id' => $workflow->getDataElement ( 'cp_id' )
                                ] );
                                
                                $form ['pdf'] = [
                                        '#type' => 'link',
                                        '#title' => ( string ) $this->t ( 'Download as PDF' ),
                                        '#url' => $url,
                                        '#prefix' => '<span class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;',
                                        '#suffix' => '<br><br>',
                                        '#attributes' => [
                                                'class' => [
                                                        'download-pdf'
                                                ]
                                        ]
                                ];
                        }
                        
                        $form ['submit'] = [
                                '#type' => 'submit',
                                '#value' => ( string ) t ( 'Set ready for publication' ),
                                '#button_type' => 'success',
                                '#prefix' => '<br>',
                                '#suffix' => '<br><br>'
                        ];
                }
                
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function validateForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                
                $session = \Drupal::service ( 'session' );
                
                $workflow = \Drupal::service('publisso_gold.tools')->getWorkflowItem($form_state->get('wf_id'));
                $workflow->newSchemaState($workflow->getSchemaDefaultFollower('schema_follower'));
                $workflow->unlock();
        
                $addParams = [];
                if(\Drupal::service('tempstore.private')->get('publisso_gold')->get('dashboardPage')) {
                        $addParams[ 'page' ] = \Drupal::service( 'tempstore.private' )->get( 'publisso_gold' )->get( 'dashboardPage' );
                        \Drupal::service('tempstore.private')->get('publisso_gold')->delete('dashboardPage');
                }
        
                $form_state->setRedirect('publisso_gold.dashboard', $addParams, ['fragment' => 'rwDashboardItem-'.$form_state->get('wf_id')]);
                
                return $form;
        }
        
        /**
         * @param null $tmpl
         * @param null $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl = NULL, $vars = NULL) {
                if ($tmpl == NULL || $vars == NULL) {
                        // set Site-Vars
                        $this->tmpl = str_replace ( array_keys ( $this->tmpl_vars ), array_values ( $this->tmpl_vars ), $this->tmpl );
                        
                        // remove unused vars
                        $this->tmpl = preg_replace ( '(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl );
                } else {
                        // set Site-Vars
                        $tmpl = str_replace ( array_keys ( $vars ), array_values ( $vars ), $tmpl );
                        
                        // remove unused vars
                        return preg_replace ( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
                }
        }
}
