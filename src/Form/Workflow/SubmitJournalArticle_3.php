<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\Workflow\SubmitJournalArticle_3.
 */
namespace Drupal\publisso_gold\Form\Workflow;

use Drupal\file\Entity\File;
use Drupal\publisso_gold\Controller\Blob;
use Drupal\publisso_gold\Controller\Manager\JournalManager;
use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
use Drupal\publisso_gold\Controller\Medium\Journal;
use Drupal\publisso_gold\Controller\Workflow\ArticleWorkflow;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides article create form.
 */
class SubmitJournalArticle_3 extends FormBase {
        
        protected $workflow = null;
        protected $journal     = null;
        
        /**
         * @return string
         */
        public function getFormId() {
                return 'SubmitJournalArticle_3';
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @return array
         * @throws \Exception
         */
        public function buildForm(array $form, FormStateInterface $form_state, array $args = []) {
                
                $this->preBuild($form_state, $args);
                if(!$this->workflow()){ $this->messenger()->addError('Coulnd load item!'); return $form; }
                
                $this->getForm   ($form, $form_state);
                $this->getActions($form, $form_state);
                
                $form['#cache'] = ['max-age' => 0];
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Drupal\Core\Entity\EntityStorageException
         */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                
                $blobIds = [];
                $wfBlobs = unserialize($this->workflow()->getDataElement('files'));
                if(!is_array($wfBlobs)) $wfBlobs = [];
        
                if($form_state->hasValue('files_fieldset')){
                
                        foreach($form_state->getValue('files_fieldset')['file'] as $_file){
                        
                                if(count($_file['file'])){
                                
                                        $file = File::load($_file['file'][0]);
                                        
                                        $file_meta = serialize($_file['meta']);
                                        $uri       = $file->toArray()['uri'][0]['value'];
                                        $content   = @file_get_contents(\Drupal::service('file_system')->realpath($uri));
                                        $mime      = $file->toArray()['filemime'][0]['value'];

                                        $blob = new Blob();
                                        $blob->create($content, $mime, $file_meta);
                                        $blobIds[] = $blob->getId();
                                        
                                        $file->delete();
                                }
                        }
                }
        
                $this->workflow()->setDataElement('files', serialize(array_unique(array_merge($wfBlobs, $blobIds))));
                SubmitJournalArticleHelper::finishSubmission($form_state, $this->journal);
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function proceedStep2(array &$form, FormStateInterface $form_state){
                
                $ts = \Drupal::service('tempstore.private')->get('publisso_gold');
                $ts->set('overwrite-SubmitJournalArticle_1', 'Form\Workflow\SubmitJournalArticle_2');
                return $form;
        }
        
        /**
         * @param FormStateInterface $form_state
         * @param array $args
         * @throws \Exception
         */
        protected function preBuild(FormStateInterface $form_state, array $args = []){
        
                if(!$form_state->get('wf_id')) $form_state->set('wf_id', $args['wf_id']);
                $this->setWorkflow($form_state->get('wf_id'));
                $this->setJournal();
                if($this->workflow()) $this->workflow()->publish(false, true);
        }
        
        /**
         * @param int $wf_id
         */
        protected function setWorkflow(int $wf_id){
                if(!$this->workflow) $this->workflow = WorkflowManager::getItem($wf_id);
        }
        
        protected function setJournal(){
                $this->journal = JournalManager::getJournal($this->workflow()->getDataElement('jrn_id'));
        }
        
        protected function workflow() :? ArticleWorkflow {
                return $this->workflow;
        }
        
        protected function journal() : Journal {
                return $this->journal;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getActions(array &$form, FormStateInterface $form_state){
                $form['actions'] = [
                        '#type' => 'actions',
                        'back' => [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Back'),
                                '#button_type' => 'warning',
                                '#submit' => ['::proceedStep2'],
                                '#limit_validation_errors' => []
                        ],
                        'finish' => [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Finish'),
                                '#button_type' => 'success',
                        ]
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getForm(array &$form, FormStateInterface $form_state){
                
                $this->getFormHeader($form, $form_state);
                $this->getFormFiles( $form, $form_state);
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormHeader(array &$form, FormStateInterface $form_state){
                
                $form ['mu_site_headline'] = [
                        '#markup' => (string)$this->t(($this->workflow()->getElement('state') == 'running submission' ? 'Add' : 'Edit').' Article'),
                        '#prefix' => '<h1>',
                        '#suffix' => '</h1>'
                ];
                
                $form['mu_journal_title'] = [
                        '#markup' => $this->journal()->getElement('title'),
                        '#prefix' => '<h2>',
                        '#suffix' => '</h2>'
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function getFormFiles(array &$form, FormStateInterface $form_state){
        
                $form['description'] = array(
                        '#markup' => (string)$this->t('Here you can upload additional Files to your submission'),
                        '#prefix' => '<h3>',
                        '#suffix' => '</h3>'
                );
        
                $form['files_fieldset'] = [
                        '#type' => 'fieldset',
                        '#tree' => true,
                        '#title' => (string)$this->t('Upload additional Files'),
                        '#prefix' => '<div id="files-fieldset-wrapper">',
                        '#suffix' => '<br></div>',
                ];
        
                if(!$form_state->has('numFiles')) $form_state->set('numFiles', 1);
                $numFiles = $form_state->get('numFiles');
        
                for ($i = 0; $i < $numFiles; $i++) {
                
                        $form['files_fieldset']['file'][$i] = [
                        
                                '#type' => 'fieldset',
                                'file'  => [
                                        '#type'                 => 'managed_file',
                                        '#title'                => (string)$this->t('File').' #'.$i,
                                        '#required'             => true,
                                        '#upload_validators'    => [
                                                'file_validate_extensions' => \Drupal::service('publisso_gold.setup')->getValue('file.upload.valid_extensions') ? [\Drupal::service('publisso_gold.setup')->getValue('file.upload.valid_extensions')] : array('gif png jpg jpeg doc docx xls xlsx midi mp4 mp3 avi mpg mpeg wav mov mid odt ods odp pdf'),
                                                'file_validate_size' => array(30 * 1024 * 1024),
                                        ],
                                        '#progress_indicator'   => 'bar',
                                        '#progress_message'     => (string)$this->t('Uploading file...')
                                ],
                                'meta'  => [
                                        'name' => [
                                                '#type'         => 'textfield',
                                                '#title'        => (string)$this->t('Filename'),
                                                '#required'     => true
                                        ],
                                        'description'   => [
                                                '#type'         => 'textfield',
                                                '#title'        => (string)$this->t('Description')
                                        ]
                                ]
                        ];
                }
        
                $form['files_fieldset']['actions']['add_name'] = [
                        '#type' => 'submit',
                        '#value' => t('Add file'),
                        '#submit' => array('::addFile'),
                        '#prefix' => '<br>',
                        '#limit_validation_errors' => [],
                        '#ajax' => [
                                'callback' => '::addFileCallback',
                                'wrapper' => 'files-fieldset-wrapper',
                                'progress' => [
                                        'type' => 'none'
                                ]
                        ],
                ];
        
                if($numFiles > 1){
                
                        $form['files_fieldset']['actions']['remove_file'] = [
                                '#type' => 'submit',
                                '#value' => t('Remove last'),
                                '#submit' => array('::removeFile'),
                                '#prefix' => '&nbsp;&nbsp;&nbsp;',
                                '#ajax' => [
                                        'callback' => '::addFileCallback',
                                        'wrapper' => 'files-fieldset-wrapper',
                                        'progress' => [
                                                'type' => 'none'
                                        ]
                                ],
                                '#limit_validation_errors' => []
                        ];
                }
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed|string[]
         */
        public function autosave(array &$form, FormStateInterface $form_state){
                return SubmitJournalArticleHelper::autosave($form, $form_state, $this->workflow);
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function removeFile(array &$form, FormStateInterface $form_state){
                $form_state->set('numFiles', $form_state->get('numFiles') - 1);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addFile(array &$form, FormStateInterface $form_state){
                $form_state->set('numFiles', $form_state->get('numFiles') + 1);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addFileCallback(array &$form, FormStateInterface $form_state){
                return $form['files_fieldset'];
        }
}
