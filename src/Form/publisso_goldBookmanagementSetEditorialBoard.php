<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldBookmanagementSetEditorialBoard.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Mail;
        use Drupal\Core\Database\Connection;
        use Drupal\file\Element;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Symfony\Component\HttpFoundation\File;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldBookmanagementSetEditorialBoard extends FormBase {
                
                private $modname = 'publisso_gold';
                private $database;
                private $modpath;
                
                public function __construct ( Connection $database ) {
                        $this->database = $database;
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return publisso_goldBookmanagementSetEditorialBoard|static
                 */
                public static function create (ContainerInterface $container ) {
                        return new static( $container->get( 'database' ) );
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'publisso_goldbookmanagementseteditorialboard';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state, $bk_id = null ) {
                        
                        
                        $this->modpath = drupal_get_path( 'module', $this->modname );
                        require_once( $this->modpath . '/inc/publisso_gold.lib.inc.php' );
                        
                        if ( !$this->database )
                                $this->database = \Drupal::database();
                        
                        if ( !$form_state->get( 'bk_id' ) )
                                $form_state->set( 'bk_id', $bk_id );
                        
                        if ( !$form_state->get( 'bk_id' ) ) {
                                \Drupal::service( 'messenger' )->addError( (string)$this->t( 'No book given!' ) );
                                return $form;
                        }
                        
                        $rebuildInfo = $form_state->getRebuildInfo();
                        
                        //get the book
                        $book = $this->database->select( 'rwPubgoldBooks', 'bk' )->fields( 'bk', [] )->condition( 'bk_id', $form_state->get( 'bk_id' ), '=' )->execute()->fetchAssoc();
                        
                        //get the editors
                        $editors = getBookEditors( $this->database, $bk_id );
                        
                        foreach ( getBookEditorsInChief( $this->database, $bk_id ) as $uid => $_ ) {
                                if ( !array_key_exists( $uid, $editors ) ) {
                                        $editors[ $uid ] = $_;
                                }
                        }
                        
                        //process editorial board
                        $eb = json_decode( $book[ 'bk_editorial_board' ], 1 );
                        $ary_eb = (array)$eb;
                        
                        if ( array_key_exists( 'main', $ary_eb ) ) {
                                $ary_eb = $ary_eb[ 'main' ];
                        }
                        
                        uasort( $ary_eb, [ 'self', 'cmp' ] );
                        
                        if ( !( is_array( $eb ) || is_object( $eb ) ) ) {
                                $eb = [];
                        }
                        
                        if ( array_key_exists('annotation', $eb) ) {
                                $annotation = $eb['annotation'];
                        }
                        
                        if ( array_key_exists('main', $eb)) {
                                $eb = $eb['main'];
                        }
                        
                        foreach ( $eb as $eb_uid => $eb_user ) {
                                if ( array_key_exists('show_in_editorial_board', $eb_user) &&
                                     $eb_user['show_in_editorial_board'] == 0 )
                                        unset( $eb[$eb_uid] );
                        }
                        
                        //get userdata of editors
                        foreach ( $editors as $k => $_ ) {
                                $editors[ $k ] = getAllUserData( $this->database, $_[ 'up_uid' ] );
                        }
                        
                        
                        $form[ 'title' ] = [
                                '#type'   => 'markup',
                                '#markup' => t( 'Set editorial board for' ) . '<h2>' . $book[ 'bk_title' ] . '</h2>',
                        ];
                        
                        $availableData = [
                                'profile_graduation'              => t( 'Graduation' ),
                                'profile_lastname'                => t( 'Lastname' ),
                                'profile_firstname'               => t( 'Firstname' ),
                                'profile_graduation_suffix'       => t( 'Graduation suffix' ),
                                'profile_email'                   => t( 'Email' ),
                                'profile_country'                 => t( 'Country' ),
                                'profile_street'                  => t( 'Street' ),
                                'profile_postal_code'             => t( 'Postal code' ),
                                'profile_city'                    => t( 'City' ),
                                'profile_department'              => t( 'Department' ),
                                'profile_orcid'                   => t( 'ORCID' ),
                                'profile_institute'               => t( 'Institute' ),
                                'profile_telephone'               => t( 'Telephone' ),
                                'profile_correspondence_language' => t( 'Correspondence language' ),
                                'profile_salutation'              => t( 'Salutation' ),
                        ];
                        
                        $form[ 'eb_board' ] = [
                                '#type'        => 'fieldset',
                                '#title'       => t( 'Editorial board' ),
                                '#collapsible' => true,
                                '#collapsed'   => true,
                                'annotation'   => [
                                        '#type'          => 'textfield',
                                        '#title'         => t( 'Annotation' ),
                                        '#default_value' => $annotation ? $annotation : '',
                                ],
                                'content'      => [],
                        ];
                        
                        foreach ( $editors as $_ ) {
                                
                                $form[ 'eb_board' ][ 'content' ][ 'userdata' . $_[ 'profile_userid' ] ] = [
                                        
                                        '#type'                                                       => 'details',
                                        '#open'                                                       => false,
                                        '#description'                                                => t( 'Select to display data of ' ) . $_[ 'profile_lastname' ],
                                        '#title'                                                      => $_[ 'profile_lastname' ] . ', ' . $_[ 'profile_firstname' ],
                                        'eb::' . $_[ 'profile_userid' ] . '::show_in_editorial_board' => [
                                                '#type'          => 'checkbox',
                                                '#title'         => t( 'Show in editorial-board?' ),
                                                '#default_value' => is_array( $eb ) && array_key_exists($_['profile_userid'], $eb) &&
                                                                    $eb[$_[ 'profile_userid'
                                                ]]['show_in_editorial_board'] ? 1 : 0,
                                                '#suffix'        => '<hr>',
                                        ],
                                ];
                                
                                foreach ( $_ as $k => $v ) {
                                        
                                        if ( $k == 'profile_country' )
                                                $v = getCountry( $v );
                                        
                                        if ( $k == 'profile_salutation' )
                                                $v = getSalutation( $v );
                                        
                                        if ( $k == 'profile_correspondence_language' )
                                                $v = getCorrespondenceLanguages( $v );
                                        
                                        if ( in_array( $k, array_keys( $availableData ) ) ) {
                                                $form[ 'eb_board' ][ 'content' ][ 'userdata' . $_[ 'profile_userid' ] ][ 'eb::' . $_[ 'profile_userid' ] . '::' . $k ] = [
                                                        '#type'          => 'checkbox',
                                                        '#title'         => '<span class="rwAlert">' . $availableData[ $k ] . ':</span> ' . $v,
                                                        '#default_value' => is_array( $eb ) && array_key_exists($_['profile_userid'], $eb) &&
                                                                            $eb[$_[ 'profile_userid' ]][$k] == 1 ? 1
                                                                                    : 0,
                                                ];
                                        }
                                }
                                
                                $form[ 'eb_board' ][ 'content' ][ 'userdata' . $_[ 'profile_userid' ] ][ 'eb::' . $_[ 'profile_userid' ] . '::notes' ] = [
                                        '#type'          => 'textfield',
                                        '#title'         => t( 'Notes' ),
                                        '#default_value' => is_object( $eb ) && $eb->$_[ 'profile_userid' ]->notes ? $eb->$_[ 'profile_userid' ]->notes : '',
                                        '#prefix'        => '<hr>',
                                ];
                                
                                $form[ 'eb_board' ][ 'content' ][ 'userdata' . $_[ 'profile_userid' ] ][ 'eb::' . $_[ 'profile_userid' ] . '::weight' ] = [
                                        '#type'          => 'number',
                                        '#size'          => 3,
                                        '#title'         => t( 'Position/Weight' ),
                                        '#default_value' => is_object( $eb ) && $eb->$_[ 'profile_userid' ]->weight ? $eb->$_[ 'profile_userid' ]->weight : 0,
                                        '#prefix'        => '<hr>',
                                ];
                        }
                        
                        //advisory boards
                        
                        $eb = json_decode( $book[ 'bk_editorial_board' ], true );
                        
                        if ( array_key_exists( 'advisory', $eb ) && (!array_key_exists('is_rebuild', $rebuildInfo) ||
                        !$rebuildInfo[ 'is_rebuild' ])
                ) {
                                $ab = $eb[ 'advisory' ];
                                $form_state->set( 'ab_counter', count( $ab ) );
                        }
                        
                        if ( !$form_state->get( 'ab_counter' ) ) {
                                $form_state->set( 'ab_counter', 0 );
                        }
                        
                        $form[ 'advisoryBoards' ] = [
                                '#tree'   => true,
                                '#prefix' => '<div id="advisory-board-wrapper">',
                                '#suffix' => '</div>',
                        ];
                        
                        for ( $i = 0; $i < $form_state->get( 'ab_counter' ); $i++ ) {
                                
                                $form[ 'advisoryBoards' ][ $i ] = [
                                        '#type'        => 'fieldset',
                                        '#title'       => t( 'Advisory board' ) . ' #' . ( $i + 1 ),
                                        '#collapsible' => true,
                                        '#collapsed'   => true,
                                        'content'      => [
                                                'name'       => [
                                                        '#type'  => 'textfield',
                                                        '#title' => t( 'Board name' ),
                                                ],
                                                'annotation' => [
                                                        '#type'  => 'textfield',
                                                        '#title' => t( 'Annotation' ),
                                                ],
                                        ],
                                ];
                                
                                if ( array_key_exists( $i, $ab ) ) {
                                        $form[ 'advisoryBoards' ][ $i ][ 'content' ][ 'name' ][ '#default_value' ] = $ab[ $i ][ 'name' ];
                                        $form[ 'advisoryBoards' ][ $i ][ 'content' ][ 'annotation' ][ '#default_value' ] = $ab[ $i ][ 'annotation' ];
                                }
                                
                                foreach ( $editors as $_ ) {
                                        
                                        $form[ 'advisoryBoards' ][ $i ][ 'content' ][ 'userdata' . $_[ 'profile_userid' ] ] = [
                                                
                                                '#type'                                                       => 'details',
                                                '#open'                                                       => false,
                                                '#description'                                                => t( 'Select to display data of ' ) . $_[ 'profile_lastname' ],
                                                '#title'                                                      => $_[ 'profile_lastname' ] . ', ' . $_[ 'profile_firstname' ],
                                                'eb::' . $_[ 'profile_userid' ] . '::show_in_editorial_board' => [
                                                        '#type'          => 'checkbox',
                                                        '#title'         => t( 'Show in editorial-board?' ),
                                                        '#default_value' => ( array_key_exists( $i, $ab ) && array_key_exists( $_[ 'profile_userid' ], $ab[ $i ][ 'user' ] ) && $ab[ $i ][ 'user' ][ $_[ 'profile_userid' ] ][ 'show_in_editorial_board' ] == 1 ) ? 1 : 0,
                                                        '#suffix'        => '<hr>',
                                                ],
                                        ];
                                        
                                        foreach ( $_ as $k => $v ) {
                                                
                                                if ( $k == 'profile_country' )
                                                        $v = getCountry( $v );
                                                
                                                if ( $k == 'profile_salutation' )
                                                        $v = getSalutation( $v );
                                                
                                                if ( $k == 'profile_correspondence_language' )
                                                        $v = getCorrespondenceLanguages( $v );
                                                
                                                if ( in_array( $k, array_keys( $availableData ) ) ) {
                                                        $form[ 'advisoryBoards' ][ $i ][ 'content' ][ 'userdata' . $_[ 'profile_userid' ] ][ 'eb::' . $_[ 'profile_userid' ] . '::' . $k ] = [
                                                                '#type'          => 'checkbox',
                                                                '#title'         => '<span class="rwAlert">' . $availableData[ $k ] . ':</span> ' . $v,
                                                                '#default_value' => ( array_key_exists( $i, $ab ) && array_key_exists( $_[ 'profile_userid' ], $ab[ $i ][ 'user' ] ) && $ab[ $i ][ 'user' ][ $_[ 'profile_userid' ] ][ $k ] == 1 ) ? 1 : 0,
                                                        ];
                                                }
                                        }
                                        
                                        $form[ 'advisoryBoards' ][ $i ][ 'content' ][ 'userdata' . $_[ 'profile_userid' ] ][ 'eb::' . $_[ 'profile_userid' ] . '::notes' ] = [
                                                '#type'          => 'textfield',
                                                '#title'         => t( 'Notes' ),
                                                '#default_value' => ( array_key_exists( $i, $ab ) && array_key_exists( $_[ 'profile_userid' ], $ab[ $i ][ 'user' ] ) ) ? $ab[ $i ][ 'user' ][ $_[ 'profile_userid' ] ][ 'notes' ] : '',
                                                '#prefix'        => '<hr>',
                                        ];
                                        
                                        $form[ 'advisoryBoards' ][ $i ][ 'content' ][ 'userdata' . $_[ 'profile_userid' ] ][ 'eb::' . $_[ 'profile_userid' ] . '::weight' ] = [
                                                '#type'          => 'number',
                                                '#size'          => 3,
                                                '#title'         => t( 'Position/Weight' ),
                                                '#default_value' => ( array_key_exists( $i, $ab ) && array_key_exists( $_[ 'profile_userid' ], $ab[ $i ][ 'user' ] ) ) ? $ab[ $i ][ 'user' ][ $_[ 'profile_userid' ] ][ 'weight' ] : 0,
                                                '#prefix'        => '<hr>',
                                        ];
                                }
                        }
                        
                        $form[ 'addBoard' ] = [
                                '#type'        => 'submit',
                                '#value'       => t( 'Add advisory-board' ),
                                '#button_type' => 'primary',
                                '#submit'      => [ '::addAdvisoryBoard' ],
                                '#prefix'      => '<br>',
                                '#suffix'      => '&nbsp;&nbsp;&nbsp;'
                                /*
                                '#ajax' => array(
                      'callback' => '::addAdvisoryBoardCallback',
                      'wrapper' => 'advisory-board-wrapper',
                      'effect' => 'fade',
                    )*/
                        ];
                        
                        $form[ 'submit' ] = [
                                '#type'   => 'submit',
                                '#value'  => t( 'save' ),
                                '#suffix' => '<br><br><br>',
                        ];
                        
                        $form_state->setCached( false );
                        $form[ '#cache' ][ 'max-age' ] = 0;
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function addAdvisoryBoard (array &$form, FormStateInterface $form_state ) {
                        
                        $this->modpath = drupal_get_path( 'module', $this->modname );
                        require_once( $this->modpath . '/inc/publisso_gold.lib.inc.php' );
                        
                        $form_state->set( 'ab_counter', $form_state->get( 'ab_counter' ) + 1 );
                        $book = new \Drupal\publisso_gold\Controller\Book( $form_state->get( 'bk_id' ) );
                        
                        $editors = $book->readEditors();
                        
                        //get the editors
                        $editors = getBookEditors( \Drupal::database(), $form_state->get( 'bk_id' ) );
                        //get userdata of editors
                        
                        $availableData = [
                                'profile_graduation'              => t( 'Graduation' ),
                                'profile_lastname'                => t( 'Lastname' ),
                                'profile_firstname'               => t( 'Firstname' ),
                                'profile_graduation_suffix'       => t( 'Graduation suffix' ),
                                'profile_email'                   => t( 'Email' ),
                                'profile_country'                 => t( 'Country' ),
                                'profile_street'                  => t( 'Street' ),
                                'profile_postal_code'             => t( 'Postal code' ),
                                'profile_city'                    => t( 'City' ),
                                'profile_department'              => t( 'Department' ),
                                'profile_orcid'                   => t( 'ORCID' ),
                                'profile_institute'               => t( 'Institute' ),
                                'profile_telephone'               => t( 'Telephone' ),
                                'profile_correspondence_language' => t( 'Correspondence language' ),
                                'profile_salutation'              => t( 'Salutation' ),
                        ];
                        
                        $form[ 'advisoryBoards' ][ $form_state->get( 'ab_counter' ) ] = [
                                '#type'        => 'fieldset',
                                '#title'       => t( 'Advisory board' ) . ' #' . $form_state->get( 'ab_counter' ),
                                '#collapsible' => true,
                                '#collapsed'   => true,
                                'content'      => [
                                        'name'       => [
                                                '#type'  => 'textfield',
                                                '#title' => t( 'Board name' ),
                                        ],
                                        'annotation' => [
                                                '#type'  => 'textfield',
                                                '#title' => t( 'Annotation' ),
                                        ],
                                ],
                        ];
                        
                        foreach ( $editors as $_ ) {
                                
                                $form[ 'advisoryBoardWrapper' ][ $form_state->get( 'ab_counter' ) ][ 'content' ][ 'userdata' . $_[ 'profile_userid' ] ] = [
                                        
                                        '#type'                                                       => 'details',
                                        '#open'                                                       => false,
                                        '#description'                                                => t( 'Select to display data of ' ) . $_[ 'profile_lastname' ],
                                        '#title'                                                      => $_[ 'profile_lastname' ] . ', ' . $_[ 'profile_firstname' ],
                                        'eb::' . $_[ 'profile_userid' ] . '::show_in_editorial_board' => [
                                                '#type'          => 'checkbox',
                                                '#title'         => t( 'Show in editorial-board?' ),
                                                '#default_value' => is_object( $eb ) && $eb->$_[ 'profile_userid' ]->show_in_editorial_board ? 1 : 0,
                                                '#suffix'        => '<hr>',
                                        ],
                                ];
                                
                                foreach ( $_ as $k => $v ) {
                                        
                                        if ( $k == 'profile_country' )
                                                $v = getCountry( $v );
                                        
                                        if ( $k == 'profile_salutation' )
                                                $v = getSalutation( $v );
                                        
                                        if ( $k == 'profile_correspondence_language' )
                                                $v = getCorrespondenceLanguages( $v );
                                        
                                        if ( in_array( $k, array_keys( $availableData ) ) ) {
                                                $form[ 'advisoryBoardWrapper' ][ 'content' ][ $form_state->get( 'ab_counter' ) ][ 'content' ][ 'userdata' . $_[ 'profile_userid' ] ][ 'eb::' . $_[ 'profile_userid' ] . '::' . $k ] = [
                                                        '#type'          => 'checkbox',
                                                        '#title'         => '<span class="rwAlert">' . $availableData[ $k ] . ':</span> ' . $v,
                                                        '#default_value' => is_object( $eb ) && $eb->$_[ 'profile_userid' ]->$k == 1 ? 1 : 0,
                                                ];
                                        }
                                }
                                
                                $form[ 'advisoryBoards' ][ $form_state->get( 'ab_counter' ) ][ 'content' ][ 'userdata' . $_[ 'profile_userid' ] ][ 'eb::' . $_[ 'profile_userid' ] . '::weight' ] = [
                                        '#type'          => 'number',
                                        '#size'          => 3,
                                        '#title'         => t( 'Position/Weight' ),
                                        '#default_value' => is_object( $eb ) && $eb->$_[ 'profile_userid' ]->weight ? $eb->$_[ 'profile_userid' ]->weight : 0,
                                        '#prefix'        => '<hr>',
                                ];
                        }
                        
                        $form_state->addRebuildInfo( 'is_rebuild', true );
                        $form_state->setRebuild();
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addAdvisoryBoardCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'advisoryBoards' ];
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $advisoryBoards = [];
        
                        if ( $form_state->hasValue('adisoryBoards') && is_array($form_state->getValue('advisoryBoards')) ) {
                                foreach ( $form_state->getValue( 'advisoryBoards' ) as $advisoryBoard ) {
                                        
                                        $advisoryBoard = $advisoryBoard[ 'content' ];
                                        $ary = [];
                                        $ary[ 'user' ] = [];
                                        
                                        foreach ( $advisoryBoard as $k => $v ) {
                                                
                                                if ( $k == 'name' ) {
                                                        
                                                        if ( empty( $v ) ) continue 2;
                                                        $ary[ 'name' ] = $v;
                                                }
                                                
                                                if ( $k == 'annotation' )
                                                        $ary[ 'annotation' ] = $v;
                                                
                                                if ( preg_match( '/^(userdata)(\d+)/', $k, $matches ) ) {
                                                        
                                                        $uid = $matches[ 2 ];
                                                        //$user = new \Drupal\publisso_gold\Controller\User($uid);
                                                        
                                                        $show = [];
                                                        $no_show = [];
                                                        
                                                        foreach ( $v as $_k => $_v ) {
                                                                
                                                                if ( substr( $_k, 0, 4 ) == 'eb::' ) {
                                                                        $_k = explode( '::', $_k );
                                                                        $show[ $_k[ 1 ] ][ $_k[ 2 ] ] = $_v;
                                                                }
                                                        }
                                                        
                                                        foreach ( $show as $uid => $values ) {
                                                                
                                                                if ( $show[ $uid ][ 'show_in_editorial_board' ] == 0 )
                                                                        $no_show[] = $uid;
                                                        }
                                                        
                                                        foreach ( $no_show as $uid ) {
                                                                unset( $show[ $uid ] );
                                                        }
                                                        
                                                        foreach ( $show as $uid => $array ) {
                                                                if ( count( $array ) )
                                                                        $ary[ 'user' ][ $uid ] = $array;
                                                        }
                                                        
                                                }
                                        }
                                        
                                        $advisoryBoards[] = $ary;
                                }
                        }
                        
                        $show = [];
                        $no_show = [];
                        $values = $form_state->getValues();
                        
                        foreach ( $values as $k => $v ) {
                                
                                if ( substr( $k, 0, 4 ) == 'eb::' ) {
                                        $k = explode( '::', $k );
                                        $show[ $k[ 1 ] ][ $k[ 2 ] ] = $v;
                                }
                                
                                if ( $k == 'annotation' ) {
                                        $annotation = $v;
                                }
                        }
                        
                        foreach ( $show as $uid => $values ) {
                                
                                if ( $show[ $uid ][ 'show_in_editorial_board' ] == 0 )
                                        $no_show[] = $uid;
                        }
                        
                        foreach ( $no_show as $uid ) {
                                unset( $show[ $uid ] );
                        }
                        
                        $eb = [];
                        $eb[ 'main' ] = $show;
                        $eb[ 'annotation' ] = $annotation;
                        $eb[ 'advisory' ] = $advisoryBoards;
                        
                        $book = new \Drupal\publisso_gold\Controller\Book( $form_state->get( 'bk_id' ) );
                        $book->setElement( 'editorial_board', json_encode( $eb ) );
                        
                        $form_state->setRedirect( 'publisso_gold.books_book.editorial_board', [ 'bk_id' => $form_state->get( 'bk_id' ) ] );
                        
                        return $form;
                }
                
                /**
                 *{@inheritdoc}
                 */
                public function enableUser ( array &$form, FormStateInterface $form_state ) {
                
                }
        
                /**
                 * @param $a
                 * @param $b
                 * @return int
                 */
                private static function cmp ($a, $b ) {
                        if(is_array($a)) $a = (object)$a;
                        if(is_array($b)) $b = (object)$b;
                        $aw = $a->weight;
                        $bw = $b->weight;
                        return $aw <=> $bw;
                }
        }
