<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\addNewsletter.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;
use Drupal\publisso_gold\Controller\PrivacyProtection;

/**
 * Provides a simple example form.
 */
class addNewsletter extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(){
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'addNewsletter';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
        
        $session    = \Drupal::service('session');
        $texts      = \Drupal::service('publisso_gold.texts');
        $formFields = \Drupal::service('publisso_gold.form.fields');
        
        foreach(['recipients', 'recipients-extra', 'subject', 'body'] as $_){
                $form['form'][$_] = $formFields->getField('add_newsletter.'.$_ , $_args);
                $form['form']['#tree'] = true;
        }
        
        $form['submit'] = [
                '#type' => 'submit',
                '#value' => (string)t($texts->get('newsletter.add.action.send', 'fc')),
                '#prefix' => '<hr>',
                '#suffix' => '<br><br>',
                '#button_type' => 'success'
        ];
        
        $form['#cache']['max-age'] = 0;
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $session    = \Drupal::service('session');
        $tools      = \Drupal::service('publisso_gold.tools');
        $data       = $form_state->getValue('form');
        $subject    = $data['subject'];
        $body       = $data['body'];
        $recipients = [];
        
        switch($data['recipients']){
                
                case 'all':
                        $recipients = \Drupal::database()->select('rwPubgoldUserProfiles', 't')
                                ->fields('t', 'up_uid')->execute()->fetchCol();
                        break;
                
                case 'administrators':
                        $uids = array_filter(\Drupal::service('publisso_gold.tools')->getUserIDsByRole('administrator'));
                        if(count($uids)){
                                $recipients = \Drupal::database()->select('rwPubgoldUserProfiles', 't')
                                ->fields('t', ['up_uid'])->condition('up_uid', $uids, 'IN')->execute()->fetchCol();
                        }
                        break;
                
                case 'publisso':
                        $uids = array_filter(\Drupal::service('publisso_gold.tools')->getUserIDsByRole('publisso'));
                        if(count($uids)){
                                $recipients = \Drupal::database()->select('rwPubgoldUserProfiles', 't')
                                ->fields('t', ['up_uid'])->condition('up_uid', $uids, 'IN')->execute()->fetchCol();
                        }
                        break;
                case 'manual':
                        break;
                default:
                        $uids = array_filter(array_merge(   
                                \Drupal::service('publisso_gold.tools')->getUserIDsByRole('administrator'),
                                \Drupal::service('publisso_gold.tools')->getUserIDsByRole('publisso')
                        ));
                        
                        if(count($uids)){
                                $recipients = \Drupal::database()->select('rwPubgoldUserProfiles', 't')
                                ->fields('t', ['up_uid'])->condition('up_uid', $uids, 'NOT IN')->execute()->fetchCol();
                        }
        }
        
        $recipientsExtra = preg_split('/,|;|\n/', $data['recipients-extra']);
        foreach($recipientsExtra as $_){
                $recipients[] = $_;
        }
        
        
        
        $recipients = array_filter(array_unique(array_map('trim', $recipients)));
        
        $nlID = \Drupal::database()->insert('rwPubgoldNewsletter')->fields([
                'created_by_uid' => $session->get('user')['id'],
                'subject'        => $subject,
                'body'           => $body,
                'recipient'      => serialize(
                                array_merge(
                                        [$form['form']['recipients']['#options'][$data['recipients']]], 
                                        $recipientsExtra
                                )
                )
        ])->execute();
        
        foreach($recipients as $_){
                
                $_subject = $subject;
                $_body    = $body;
                
                if(preg_match('/^\d+$/', $_)){
                        
                        $user = new \Drupal\publisso_gold\Controller\User($_);
                        $_ = $user->profile->getElement('email');
                        
                        $replaces = [];
                        
                        foreach($user->getKeys() as $key){
                                $replaces["::user.$key::"] = $user->getElement($key);
                        }
                        
                        foreach($user->profile->getKeys() as $key){
                                
                                switch($key){
                                        
                                        case 'country':
                                                $replaces["::user.profile.$key::"] = $tools->getCountry($user->profile->getElement($key));
                                                break;
                                        
                                        case 'salutation':
                                                $replaces["::user.profile.$key::"] = $tools->getSalutation($user->profile->getElement($key));
                                                break;
                                        
                                        case 'area_of_expertise':
                                                $replaces["::user.profile.$key::"] = implode(',', $user->profile->getAreasOfExpertise());
                                                break;
                                        
                                        case 'correspondence_language':
                                                $replaces["::user.profile.$key::"] = \Drupal::service('language_manager')->getLanguageName($user->profile->getElement($key));
                                                break;
                                        default:
                                                $replaces["::user.profile.$key::"] = $user->profile->getElement($key);
                                }
                        }
                        error_log(print_r($replaces, 1));
                        $_subject = str_replace(array_keys($replaces), array_values($replaces), $_subject);
                        $_body    = str_replace(array_keys($replaces), array_values($replaces), $_body   );
                }
                
                \Drupal::database()->insert('rwPubgoldMaillog')->fields([
                        'recipient'      => $_,
                        'subject'        => $_subject,
                        'body'           => $_body,
                        'init_process'   => 'newsletter',
                        'to_uid'         => $user ? $user->getElement('id') : null,
                        'created_by_uid' => $session->get('user')['id'],
                        'init_id'        => $nlID
                ])->execute();
                
                $tools->sendMail($_, $_subject, $_body);
        }
        
        $form_state->setRedirect('publisso_gold.newsletter');
        
        return $form;
    }
}
