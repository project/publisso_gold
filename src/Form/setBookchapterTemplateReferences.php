<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\setBookchapterTemplateReferences.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Database\Connection;
        use Drupal\file\Element;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Symfony\Component\HttpFoundation\File;
        use Drupal\publisso_gold\Controller\BookchapterTemplate;
        
        /**
         * Provides a simple example form.
         */
        class setBookchapterTemplateReferences extends FormBase {
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'setBookchapterTemplateReferences';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state, $args = [] ) {
                        
                        if ( !$form_state->get( 'templateID' ) ) $form_state->set( 'templateID', $args[ 'templateID' ] );
                        
                        $session = \Drupal::service( 'session' );
                        
                        //load template
                        $template = new \Drupal\publisso_gold\Controller\BookchapterTemplate( null, $form_state->get( 'templateID' ) );
                        
                        if ( !$template->getTemplateArticle() ) {
                                if ( !$template->createTemplateArticle() ) {
                                        return $form;
                                }
                        }
                        
                        $xml = new \DOMDocument();
                        $xml->loadXML($template->getTemplateArticle());
                        $xpt = new \DOMXpath($xml);
                        
                        //sind datenversionen vorhanden?
                        $references = '';
                        $nl = $xpt->evaluate('/root/references/reference');
                        if($nl->length){
                                
                                $i = 1;
                                foreach($nl as $refNode){
                                       $references .= "[".($i++)."] ".trim($refNode->nodeValue)."\n";
                                }
                        }
        
                        $form['references'] = [
                                '#type'   => 'textarea',
                                '#title'  => (string)t('References'),
                                '#rows'   => 10,
                                '#cols'   => 60,
                                '#prefix' => '<br>',
                                '#id'     => 'ta-rwReferences',
                                '#default_value' => trim($references)
                        ];
                        
                        $form[ 'submit' ] = [
                                '#type'   => 'submit',
                                '#value'  => (string)t( 'Save' ),
                                '#suffix' => '<br><br><br>',
                                '#prefix' => '<br>',
                                '#id'     => 'submit_button'
                        ];
                        
                        $form[ '#cache' ][ 'max-age' ] = 0;
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $this->storeReferences($form_state->getValue('references'), $form_state->get( 'templateID' ));
                        $form_state->setRedirect('publisso_gold.book.template', ['id' => $form_state->get
                        ('templateID'), 'action' => 'fill']);
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 * @throws \Exception
                 */
                public function autosave(array &$form, FormStateInterface $form_state){
                        
                        $ui = $form_state->getUserInput();
                        $this->storeReferences($ui['references'], $form_state->get( 'templateID' ));
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param $references
                 * @param $templateID
                 * @return false
                 * @throws \Exception
                 */
                private function storeReferences($references, $templateID){
                        
                        $tools = \Drupal::service('publisso_gold.tools');
                        //load template
                        $template = new \Drupal\publisso_gold\Controller\BookchapterTemplate( null, $templateID );
        
                        if ( !$template->getTemplateArticle() ) {
                                if ( !$template->createTemplateArticle() ) {
                                        return false;
                                }
                        }
        
                        $xml = new \DOMDocument();
                        $xml->formatOutput = true;
                        $xml->loadXML($template->getTemplateArticle());
                        $xpt = new \DOMXpath($xml);
        
                        //ist ein references-Knoten vorhanden? Wenn nicht, dann erstellen (/root/references)
                        if(!$xpt->evaluate('/root/references')){
                                $tools->DOMAppendChild('references', '', [], $xml, $xpt->evaluate('/root'));
                        }
                        
                        foreach($xpt->evaluate('/root/references/reference') as $node){
                                $xpt->evaluate('/root/references')[0]->removeChild($node);
                        }
                        
                        foreach(array_filter(array_map("trim", explode("\n", $references))) as $ref){
                                
                                if(preg_match('/\[(\d+?)\](\s+)?(.+?)$/i', $ref, $matches)){
                                        
                                        $refString = $matches[3];
                                        $refNode = $xpt->evaluate('/root/references')[0];
                                        
                                        if(!$xpt->evaluate('/root/references/reference[@md5="'.md5($refString).'"]')->length){
                                                $valueNode = \Drupal::service('publisso_gold.tools')->DOMAppendChild('reference', '', ['md5' => md5($refString)], $xml, $refNode);
                                                \Drupal::service('publisso_gold.tools')->appendCdata($xml, $valueNode, $refString);
                                        }
                                }
                        }
                        
                        $template->saveTemplateArticle($xml->saveXML());
                }
        }
