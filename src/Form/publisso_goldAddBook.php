<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldAddBook.
 */
namespace Drupal\publisso_gold\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldAddBook extends FormBase {
	private $modname = 'publisso_gold';
	private $database;
	private $modpath;
	private $num_corporations;
	public function __construct(Connection $database) {
		$this->database = $database;
	}
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldAddBook|static
         */
        public static function create(ContainerInterface $container) {
		return new static ( $container->get ( 'database' ) );
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function getFormId() {
		return 'publisso_goldaddbook';
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {
		$texts = \Drupal::service ( 'publisso_gold.texts' );
		
		if ($this->modname && ! $form_state->get ( 'modname' )) {
			$form_state->set ( 'modname', $this->modname );
		}
		
		if (! $this->modname && $form_state->get ( 'modname' )) {
			$this->modname = $form_state->get ( 'modname' );
		}
		
		$this->modpath = drupal_get_path ( 'module', $this->modname );
		
		if ($this->modpath && ! $form_state->get ( 'modpath' )) {
			$form_state->set ( 'modpath', $this->modpath );
		}
		
		if (! $this->modpath && $form_state->get ( 'modpath' )) {
			$this->modpath = $form_state->get ( 'modpath' );
		}
		
		require_once ($this->modpath . '/inc/publisso_gold.lib.inc.php');
		
		if (! $this->database)
			$this->database = \Drupal::database ();
		
		$rp = [ ];
		
		// get review-pages
		foreach ( getReviewpages () as $page ) {
			$rp [$page->rp_id] = $page->rp_name;
		}
		
		$availThemes = [ ];
		foreach ( \Drupal::service ( 'theme_handler' )->listInfo () as $name => $theme ) {
			$availThemes [$name] = $theme->info ['name'];
		}
		
		$form ['site_headline'] = [ 
			'#type' => 'markup',
			'#markup' => '<h1>' . t ( 'Add Book' ) . '</h1>' 
		];
		
		$form ['rp_id'] = [ 
			'#type' => 'select',
			'#title' => ( string ) t ( 'select review-page' ),
			'#options' => $rp,
			'#required' => true 
		];
		
		$form ['language'] = [ 
			'#type' => 'select',
			'#title' => t ( 'Language' ),
			'#options' => json_decode ( $_SESSION ['setup']->getValue ( 'book.available.languages' ), true ),
			'#default_value' => $_SESSION ['setup']->getValue ( 'book.default.language' ) 
		];
		
		$form ['theme'] = [ 
			'#type' => 'select',
			'#title' => ( string ) t ( 'Theme' ),
			'#options' => $availThemes,
			'#required' => true,
			'#default_value' => \Drupal::service ( 'publisso_gold.setup' )->getValue ( 'books.default_theme' ) 
		];
		
		$form ['title'] = [ 
			'#type' => 'textfield',
			'#title' => t ( 'Title' ),
			'#required' => true 
		];
		
		$form ['title_abbr'] = [ 
			'#type' => 'textfield',
			'#title' => t ( 'Title Abbreviation' ),
			'#required' => true 
		];
		
		$form ['logo'] = [ 
			'#type' => 'managed_file',
			'#title' => t ( 'Logo' ),
			'#required' => false 
		];
		
		$form ['cover'] = [ 
			'#type' => 'managed_file',
			'#title' => t ( 'Cover' ),
			'#required' => false 
		];
		
		$form ['description'] = [ 
			'#type' => 'textarea',
			'#title' => t ( 'Description' ),
			'#required' => true,
			'#attributes' => [ 
				'class' => [ 
					\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
				] 
			] 
		];
		
		$form ['citation_note'] = [ 
			'#type' => 'textarea',
			'#title' => ( string ) t ( 'Citation note' ),
			'#required' => true,
			'#attributes' => [ 
				'rows' => 2 
			] 
		];
		
		$form ['edition'] = [ 
			'#type' => 'textfield',
			'#title' => t ( 'Edition' ),
			'#required' => false 
		];
		
		$form ['publication_place'] = [ 
			'#type' => 'textfield',
			'#title' => t ( 'Publication place' ),
			'#required' => false,
			'#default_value' => 'Cologne' 
		];
		
		$form ['publisher'] = [ 
			'#type' => 'textfield',
			'#title' => ( string ) t ( 'Publisher' ),
			'#required' => false,
			'#default_value' => 'PUBLISSO' 
		];
		
		$form ['begin_publication'] = [ 
			'#type' => 'number',
			'#title' => t ( 'Begin Publication' ),
			'#required' => false,
			'#default_value' => date ( "Y" ) 
		];
		
		$form ['isbn'] = [ 
			'#type' => 'textfield',
			'#title' => t ( 'ISBN' ) 
		];
		
		$form ['issn'] = [ 
			'#type' => 'textfield',
			'#title' => t ( 'ISSN' ) 
		];
		
		$form ['ddc'] = [ 
			'#type' => 'textfield',
			'#title' => t ( 'DDC' ),
			'#required' => true 
		];
		
		$form ['funding_name'] = [ 
			'#type' => 'textfield',
			'#title' => ( string ) t ( 'Funding name' ) 
		];
		
		$form ['funding_id'] = [ 
			'#type' => 'textfield',
			'#title' => ( string ) t ( 'Funding ID' ) 
		];
		
		$form ['license'] = [ 
			'#type' => 'textfield',
			'#title' => ( string ) t ( 'License' ) 
		];
		
		$form ['category'] = [ 
			'#type' => 'select',
			'#title' => t ( 'Category' ),
			'#options' => getBookCategories ( $this->database ),
			'#required' => true 
		];
		
		// init counter
		if (! $form_state->get ( 'num_sustaining_members' ))
			$form_state->set ( 'num_sustaining_members', 0 );
		
		$max = $form_state->get ( 'num_sustaining_members' );
		
		$form ['sustaining_members'] = array (
			'#tree' => TRUE,
			'#prefix' => '<div id="sustaining_members-replace">',
			'#suffix' => '</div>' 
		);
		
		for($delta = 0; $delta < $max; $delta ++) {
			
			if (! isset ( $form ['sustaining_members'] [$delta] )) {
				
				$element = array (
					'#type' => 'managed_file' 
				);
				
				if ($delta == 0) {
					$element ['#title'] = t ( 'Sustaining members' );
				}
				
				$form ['sustaining_members'] [$delta] = $element;
			}
		}
		
		$form ['addSustainingMembers'] = array (
			'#type' => 'submit',
			'#name' => 'addSustainingMembers',
			'#value' => t ( 'Add sustaining member' ),
			'#submit' => array (
				'::addMoreSMSubmit' 
			),
			'#suffix' => '<br><br>',
			'#prefix' => '<br>',
			'#limit_validation_errors' => [ ],
			
			'#ajax' => array (
				'callback' => '::addMoreSMCallback',
				'wrapper' => 'sustaining_members-replace',
				'effect' => 'fade' 
			) 
		
		);
		
		// init counter
		if (! $form_state->get ( 'num_corporation' ))
			$form_state->set ( 'num_corporation', 0 );
		
		$max = $form_state->get ( 'num_corporation' );
		
		$form ['corporation'] = array (
			'#tree' => TRUE,
			'#prefix' => '<div id="corporation-replace">',
			'#suffix' => '</div>' 
		);
		
		for($delta = 0; $delta < $max; $delta ++) {
			
			if (! isset ( $form ['corporation'] [$delta] )) {
				
				$element = array (
					'#type' => 'textfield' 
				);
				
				if ($delta == 0) {
					$element ['#title'] = t ( 'Corporation' );
				}
				
				$form ['corporation'] [$delta] = $element;
			}
		}
		
		$form ['addCorporation'] = array (
			'#type' => 'submit',
			'#name' => 'addCorporation',
			'#value' => t ( 'Add Corporation' ),
			'#submit' => array (
				'::addMoreSubmit' 
			),
			'#suffix' => '<br><br>',
			'#prefix' => '<br>',
			'#limit_validation_errors' => [ ],
			
			'#ajax' => array (
				'callback' => '::addMoreCallback',
				'wrapper' => 'corporation-replace',
				'effect' => 'fade' 
			) 
		
		);
		
		$form ['public_editors'] = [ 
			'#type' => 'textfield',
			'#title' => ( string ) t ( 'Editors (appear in book-header)' ) ,
                        '#maxlength' => 255
		];
		
		$form ['funding_text'] = [ 
			'#type' => 'textfield',
			'#title' => ( string ) t ( 'Funding freetext' ) 
		];
		
		$form ['about'] = [ 
			'#type' => 'textarea',
			'#title' => ( string ) t ( 'About this book' ),
			'#attributes' => [ 
				'class' => [ 
					\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
				] 
			] 
		];
		
		$form ['manuscript_guidelines'] = [ 
			'#type' => 'textarea',
			'#title' => ( string ) t ( 'Manuscript guidelines' ),
			'#attributes' => [ 
				'class' => [ 
					\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
				] 
			] 
		];
		
		$form ['media'] = [ 
			'#type' => 'textarea',
			'#title' => ( string ) t ( 'Media' ),
			'#attributes' => [ 
				'class' => [ 
					\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
				] 
			] 
		];
		
		$form ['imprint'] = [ 
			'#type' => 'textarea',
			'#title' => ( string ) t ( 'Imprint' ),
			'#attributes' => [ 
				'class' => [ 
					\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
				] 
			] 
		];
		
		$form ['mail_signature'] = [ 
			'#type' => 'textarea',
			'#title' => 'EMail Signature',
			'#description' => t ( 'Signature appended to outgoing emails relatet to this book' ) 
		];
		
		$form['public'] = [
		        '#type' => 'checkbox',
                        '#title' => (string)$this->t('Public'),
                        '#description' => (string)$this->t('Indicates whether the book is publicly listed')
                ];
		
		$form ['control'] = [ 
			'#type' => 'fieldset',
			'#title' => t ( 'Control Elements' ),
			'#collapsible' => true,
			'#collapsed' => true,
			'#tree' => true,
			'#prefix' => '<br>',
			'elements' => [ 
				
				'review_change_origtext' => [ 
					'#type' => 'checkbox',
					'#title' => t ( 'Allow changes in Origtext' ) 
				],
				
				'review_type' => [ 
					'#type' => 'select',
					'#title' => t ( 'Review Type' ),
					'#options' => [ 
						'op' => t ( 'Open' ),
						'sb' => t ( 'Single Blind' ),
						'db' => t ( 'Double Blind' ) 
					],
					'#required' => true,
					'#default_value' => 'op' 
				],
				
				'time_for_review' => [ 
					'#type' => 'number',
					'#size' => 3,
					'#title' => t ( 'Time for Review (Days)' ),
					'#default_value' => 0 
				],
				
				'time_for_revision' => [ 
					'#type' => 'number',
					'#size' => 3,
					'#title' => t ( 'Time for Revision (Days)' ),
					'#default_value' => 0 
				],
				
			        'abstract_required' => [
			                '#type' => 'select',
			                '#options' => [
			                        0 => (string)t($texts->get('bookmanagement.control.abstract_required.option.no', 'fc')),
			                        1 => (string)t($texts->get('bookmanagement.control.abstract_required.option.yes', 'fc')),
			                        2 => (string)t($texts->get('bookmanagement.control.abstract_required.option.optional', 'fc'))
			                ],
			                '#title' => t('Abstract required')
			        ],
				
				'char_limit_abstract' => [ 
					'#type' => 'number',
					'#title' => t ( 'Character limit abstract' ),
					'#default_value' => 0 
				],
				
				'char_limit_text' => [ 
					'#type' => 'number',
					'#title' => t ( 'Character limit text' ),
					'#default_value' => 0 
				],
				
				'reviewer_suggestion' => [ 
					'#type' => 'checkbox',
					'#title' => t ( 'Reviewer Suggestion' ) 
				],
				
				'allow_invite_users' => [ 
					'#type' => 'checkbox',
					'#title' => t ( 'Special invitation workflow' )
				],
				
				'cn_version_string' => [ 
					'#type' => 'select',
					'#options' => [ 
						'' => ( string ) t ( $texts->get ( 'book.control.cn_version_string.option.empty', 'fc' ) ),
						'chapter.field.published' => ( string ) t ( $texts->get ( 'book.control.cn_version_string.option.published', 'fc' ) ),
						'chapter.field.version' => ( string ) t ( $texts->get ( 'book.control.cn_version_string.option.version', 'fc' ) ) 
					],
					'#default_value' => 'chapter.field.version',
					'#title' => ( string ) t ( $texts->get ( 'book.control.cn_version_string.title', 'fc' ) ) 
				],
				
				'cn_version_string_prefix' => [ 
					'#type' => 'textfield',
					'#title' => ( string ) t ( $texts->get ( 'book.control.cn_version_string_prefix.title', 'fc' ) ) 
				] ,
                                
                                'show_submission_date' => [
                                        '#type'                 => 'checkbox',
                                        '#title'                => t('Show submission date')
                                ],
                                
                                'show_revision_date' => [
                                        '#type'                 => 'checkbox',
                                        '#title'                => t('Show revision date')
                                ],
                                
                                'show_acceptance_date' => [
                                        '#type'                 => 'checkbox',
                                        '#title'                => t('Show acceptance date')
                                ],
                                
                                'show_publication_date' => [
                                        '#type'                 => 'checkbox',
                                        '#title'                => t('Show publication date')
                                ],
                                
                                'search_livivo_enabled' => [
                                        '#type'  => 'checkbox',
                                        '#title' => t( 'Enable Livivo-Search' ),
                                ],
                                
                                'search_pubmed_enabled' => [
                                        '#type'  => 'checkbox',
                                        '#title' => t( 'Enable Pubmed-Search' ),
                                ]
			] 
		];
		
		$form ['submit'] = [ 
			'#type' => 'submit',
			'#value' => t ( 'Create Book' ),
			'#prefix' => '<br>',
			'#suffix' => '<br><br><br>' 
		];
		
		$form ['#attributes'] = array (
			'enctype' => 'multipart/form-data' 
		);
		
		$form ['#attached'] = [ 
			'library' => [ 
				'publisso_gold/default' 
			] 
		];
		
		$form ['#cache'] ['max-age'] = 0;
		
		$form_state->setCached ( false );
		
		return $form;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {
		if (! preg_match ( '/^\d{4}$/', $form_state->getValue ( 'begin_publication' ) )) {
			$form_state->setErrorByName ( 'begin_publication', ( string ) t ( 'In field "begin publication" enter 4 digits!' ) );
		}
		
		return $form;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {
		$control = $form_state->getValue ( 'control' );
		
		// process file-upload
		$cover = array ();
		$cover ['content'] = NULL;
		$cover ['type'] = NULL;
		
		$logo = array ();
		$logo ['content'] = NULL;
		$logo ['type'] = NULL;
		
		$fid = $form_state->getValue ( 'cover' );
		
		if ($fid) {
			$cover = \Drupal::database ()->select ( 'file_managed', 'f' )->condition ( 'f.fid', $fid, '=' )->fields ( 'f', array () )->execute ()->fetchAssoc ();
			$cover ['path'] = str_replace ( 'temporary:/', '/tmp', $cover ['uri'] );
			$cover ['content'] = '0x' . bin2hex ( file_get_contents ( $cover ['path'] ) );
			$cover ['type'] = $cover ['filemime'];
		}
		
		// echo '<pre>'.print_r($cover, 1).'</pre>'; phpinfo(); exit();
		$fid = $form_state->getValue ( 'logo' );
		if ($fid) {
			$logo = \Drupal::database ()->select ( 'file_managed', 'f' )->condition ( 'f.fid', $fid, '=' )->fields ( 'f', array () )->execute ()->fetchAssoc ();
			$logo ['path'] = str_replace ( 'temporary:/', '/tmp', $logo ['uri'] );
			$logo ['content'] = '0x' . bin2hex ( file_get_contents ( $logo ['path'] ) );
			$logo ['type'] = $logo ['filemime'];
		}
		
		$sustaining_members = $form_state->getValue ( 'sustaining_members' );
		$sustaining_members_images = [ ];
		
		foreach ( $sustaining_members as $_ ) {
			
			$fid = $_ [0];
			
			if ($fid) {
				$result = \Drupal::database ()->select ( 'file_managed', 'f' )->condition ( 'f.fid', $fid, '=' )->fields ( 'f', array () )->execute ()->fetchAssoc ();
				$result ['path'] = str_replace ( 'temporary:/', '/tmp', $result ['uri'] );
				preg_match ( '/(.+)\.{1}(.+$)/i', $result ['filename'], $matches );
				$sustaining_members_images [] = '0x' . bin2hex ( file_get_contents ( $result ['path'] ) );
			}
		}
		
		// save book in database -> gets last_insert_id
		$corporation = $form_state->getValue ( 'corporation' );
		
		$bk_id = \Drupal::database ()->insert ( 'rwPubgoldBooks' )->fields ( array (
			'bk_title' => $form_state->getValue ( 'title' ),
			'bk_title_abbr' => $form_state->getValue ( 'title_abbr' ),
			'bk_logo' => $logo ['content'],
			'bk_cover' => $cover ['content'],
			'bk_description' => base64_encode ( $form_state->getValue ( 'description' ) ),
			'bk_corporation' => $corporation ? json_encode ( $corporation ) : null,
			'bk_citation_note' => $form_state->getValue ( 'citation_note' ),
			'bk_edition' => $form_state->getValue ( 'edition' ),
			'bk_publication_place' => $form_state->getValue ( 'publication_place' ),
			'bk_publisher' => $form_state->getValue ( 'publisher' ),
			'bk_begin_publication' => $form_state->getValue ( 'begin_publication' ) ? $form_state->getValue ( 'begin_publication' ) : NULL,
			'bk_issn' => $form_state->getValue ( 'issn' ),
			'bk_isbn' => $form_state->getValue ( 'isbn' ),
			'bk_ddc' => $form_state->getValue ( 'ddc' ),
			'bk_bcatid' => $form_state->getValue ( 'category' ),
			'bk_rpid' => $form_state->getValue ( 'rp_id' ),
			'bk_license' => $form_state->getValue ( 'license' ),
			'bk_funding_name' => $form_state->getValue ( 'funding_name' ),
			'bk_funding_id' => $form_state->getValue ( 'funding_id' ),
			'bk_cover_type' => $cover ['type'],
			'bk_logo_type' => $logo ['type'],
			'bk_imprint' => base64_encode ( $form_state->getValue ( 'imprint' ) ),
			'bk_about' => base64_encode ( $form_state->getValue ( 'about' ) ),
			'bk_media' => base64_encode ( $form_state->getValue ( 'media' ) ),
			'bk_manuscript_guidelines' => base64_encode ( $form_state->getValue ( 'manuscript_guidelines' ) ),
			'bk_public_editors' => $form_state->getValue ( 'public_editors' ),
			'bk_funding_text' => $form_state->getValue ( 'funding_text' ),
			'bk_control' => json_encode ( $control ['elements'] ),
			'bk_mail_signature' => $form_state->getValue ( 'mail_signature' ),
			'bk_language' => $form_state->getValue ( 'language' ),
			'bk_theme' => $form_state->getValue ( 'theme' ) ,
                        'bk_public' => $form_state->getValue('public') ?? 0
		) )->execute ();
		
		// save sustaining members
		foreach ( $sustaining_members_images as $_ ) {
			\Drupal::database ()->insert ( 'rwPubgoldBooksSustainingMembers' )->fields ( [ 
				'bsm_bkid' => $bk_id,
				'bsm_image' => $_ 
			] )->execute ();
		}
		
		// $form_state->cleanValues();
		// drupal_set_message(t('Book successfully created!'), 'status');
		$form_state->setRedirect ( 'publisso_gold.books_book', [ 
			'bk_id' => $bk_id 
		] );
		return $form;
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function add_corporation(array &$form, FormStateInterface $form_state) {
		$form ['corporation'] [] = [ 
			'#type' => 'textfield',
			'#title' => t ( 'Corporation' ) 
		];
		
		$form_state->setRebuild ();
		return $form;
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addMoreSubmit(array &$form, FormStateInterface $form_state) {
		$form_state->set ( 'num_corporation', $form_state->get ( 'num_corporation' ) + 1 );
		$form ['corporation'] [] = [ 
			'#type' => 'textfield',
			'#title' => t ( 'Corporation' ) 
		];
		
		$form_state->setRebuild ();
		$form_state->setRebuild ();
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addMoreCallback(array &$form, FormStateInterface $form_state) {
		return $form ['corporation'];
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function add_sustaining_members(array &$form, FormStateInterface $form_state) {
		$form ['sustaining_members'] [] = [ 
			'#type' => 'textfield',
			'#title' => t ( 'Sustaining members' ) 
		];
		
		$form_state->setRebuild ();
		return $form;
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addMoreSMSubmit(array &$form, FormStateInterface $form_state) {
		$form_state->set ( 'num_sustaining_members', $form_state->get ( 'num_sustaining_members' ) + 1 );
		$form ['sustaining_members'] [] = [ 
			'#type' => 'textfield',
			'#title' => t ( 'Sustaining members' ) 
		];
		
		$form_state->setRebuild ();
		$form_state->setRebuild ();
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addMoreSMCallback(array &$form, FormStateInterface $form_state) {
		return $form ['sustaining_members'];
	}
}
