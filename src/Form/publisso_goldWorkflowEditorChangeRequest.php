<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowEditorChangeRequest.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldWorkflowEditorChangeRequest extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldWorkflowEditorChangeRequest|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldworkfloweditorchangerequest';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $this->modpath = drupal_get_path('module', $this->modname);
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
        //expect build-info-args as array
        $build_info = $form_state->getBuildInfo();
        $args = $build_info['args'];
        
        $form_state->set('bk_id', $args[0]['bk_id']);
        $form_state->set('jrn_id', $args[0]['jrn_id']);
        $form_state->set('cf_id', $args[0]['cf_id']);
        $form_state->set('wf_id', $args[0]['wf_id']);
        
        if(!$form_state->get('bk_id')){
            //drupal_set_message(t('No book given!'), 'error');
            //return array();
        }
        
        if($form_state->get('bk_id'))
            $book = $this->database->select('rwPubgoldBooks', 'bk')->fields('bk', [])->condition('bk_id', $form_state->get('bk_id'), '=')->execute()->fetchAssoc();
        
        if($form_state->get('jrn_id'))
            $journal = $this->database->select('rwPubgoldJournals', 'jrn')->fields('jrn', [])->condition('jrn_id', $form_state->get('jrn_id'), '=')->execute()->fetchAssoc();
        
        if($form_state->get('cf_id'))
            $conference = $this->database->select('rwPubgoldConferences', 'cf')->fields('cf', [])->condition('cf_id', $form_state->get('cf_id'), '=')->execute()->fetchAssoc();
        
        if($form_state->get('wf_id')){
            $workflow = getDashboard(\Drupal::Database(), $form_state->get('wf_id'));
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            
            if(isset($book))
                $chapter_text = json_decode(base64_decode($workflow_data->chapter_text));
            elseif(isset($journal))
                $article_text = json_decode(base64_decode($workflow_data->article_text));
            elseif(isset($conference))
                $paper_text = json_decode(base64_decode($workflow_data->paper_text));
            
            $abstract = json_decode(base64_decode($workflow_data->abstract));
            $errata = json_decode(base64_decode($workflow_data->errata));
            $correction = json_decode(base64_decode($workflow_data->correction));
            $comment = json_decode(base64_decode($workflow_data->comment));
            
            $delta = ['workflow', 'workflow_data'];
            foreach($delta as $_) $form_state->set($_, $$_);
        }
        
        $form = \Drupal::formBuilder()
            ->getForm(
                "Drupal\\".$this->modname."\Form\publisso_goldBookmanagementAddChapter",
                [
                    'bk_id' => $form_state->get('bk_id'),
                    'wf_id' => $form_state->get('wf_id')
                ]
            );
        
        $author = getAllUserData(\Drupal::database(), $workflow_data->author_uid);
        
        if(isset($book)){
            
            $form = [
                
                'site_headline' => [
                '#type' => 'markup',
                '#markup' => '<h1>'.t('Add Chapter').'</h1>'
                ],
                
                'book_title' => [
                    '#type' => 'markup',
                    '#markup' => '<h2>'.$book['bk_title'].'</h2>'
                ],
                
                'author' => [
                    '#type' => 'markup',
                    '#markup' => $author['profile_graduation'].' '.$author['profile_lastname'].', '.$author['profile_firstname'].' '.$author['profile_graduation_suffix'],
                    '#prefix' => '<h4>'.(string)t('Author').': ',
                    '#suffix' => '</h4>'
                ],
                
                'title' => [
                    '#type' => 'textfield',
                    '#title' => t('Title'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->title : ''
                ],
                
                'license' => [
                    '#type' => 'textfield',
                    '#title' => t('License'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->license : ''
                ],
                
                'abstract' => [
                    '#type' => 'text_format',
                    '#title' => t('Abstract'),
                    '#required' => true,
                    '#format' => isset($abstract) ? $abstract->format : 'basic_html',
                    '#default_value' => isset($abstract) ? $abstract->value : ''
                ],
                
                'publication_year' => [
                    '#type' => 'textfield',
                    '#title' => t('Publication Year'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->publication_year : ''
                ],
                
                'publication_place' => [
                    '#type' => 'textfield',
                    '#title' => t('Publication Place'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->publication_place : ''
                ],
                
                'publisher' => [
                    '#type' => 'textfield',
                    '#title' => t('Publisher'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->publisher : ''
                ],
                
                'doi' => [
                    '#type' => 'textfield',
                    '#title' => t('DOI'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->doi : ''
                ],
                
                'ddc' => [
                    '#type' => 'textfield',
                    '#title' => t('DDC'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->ddc : ''
                ],
                
                'corresponding_author' => [
                    '#type' => 'textfield',
                    '#title' => t('Corresponding Author'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->corresponding_author : ''
                ],
                
                'chapter_text' => [
                    '#type' => 'text_format',
                    '#title' => t('Chapter Text'),
                    '#required' => true,
                    '#rows' => 50,
                    '#format' => isset($chapter_text) ? $chapter_text->format : 'full_html',
                    '#default_value' => isset($chapter_text) ? $chapter_text->value : ''
                ],
                
                'urn' => [
                    '#type' => 'textfield',
                    '#title' => t('URN'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->urn : ''
                ],
                
                'keywords' => [
                    '#type' => 'textfield',
                    '#title' => t('Keywords'),
                    '#default_value' => isset($workflow_data) ? implode(', ', json_decode($workflow_data->keywords)) : ''
                ],
                
                'funding_number' => [
                    '#type' => 'textfield',
                    '#title' => t('Funding Number'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->funding_number : ''
                ],
                
                'funding_id' => [
                    '#type' => 'textfield',
                    '#title' => t('Funding ID'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->funding_id : ''
                ],
                /*
                'description_errata' => [
                    '#type' => 'markup',
                    '#markup' => '<span class="rwAlert">'.t('You can set Version OR Errata OR Correction!').'</span>'
                ],
                */
                'version' => [
                    '#type' => 'textfield',
                    '#title' => t('Version'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->version : ''
                ],
                /*
                'errata' => [
                    '#type' => 'text_format',
                    '#title' => t('Errata'),
                    '#format' => 'full_html',
                    '#format' => isset($errata) ? $errata->format : 'basic_html',
                    '#default_value' => isset($errata) ? $errata->value : ''
                ],
                
                'correction' => [
                    '#type' => 'text_format',
                    '#title' => t('Correction'),
                    '#format' => 'full_html',
                    '#format' => isset($correction) ? $correction->format : 'basic_html',
                    '#default_value' => isset($correction) ? $correction->value : ''
                ],
                */
                'comment' => [
                    '#type' => 'text_format',
                    '#format' => 'basic_html',
                    '#title' => (string)t('Comment'),
                    '#format' => isset($comment) ? $comment->format : 'basic_html',
                    '#default_value' => isset($comment) ? $comment->value : ''
                ],
                
                'submit' => [
                    '#type' => 'submit',
                    '#value' => (string)t('Request clearing from EiC')
                ]
                
            ];
        }
        
        if(isset($journal)){
            
            $form = [
                
                'site_headline' => [
                '#type' => 'markup',
                '#markup' => '<h1>'.t('Add Article').'</h1>'
                ],
                
                'journal_title' => [
                    '#type' => 'markup',
                    '#markup' => '<h2>'.$journal['jrn_title'].'</h2>'
                ],
                
                'author' => [
                    '#type' => 'markup',
                    '#markup' => $author['profile_graduation'].' '.$author['profile_lastname'].', '.$author['profile_firstname'].' '.$author['profile_graduation_suffix'],
                    '#prefix' => '<h4>'.(string)t('Author').': ',
                    '#suffix' => '</h4>'
                ],
                
                'title' => [
                    '#type' => 'textfield',
                    '#title' => t('Title'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->title : ''
                ],
                
                'title_translated' => [
                    '#type' => 'textfield',
                    '#title' => t('Title translated'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->title_translated : ''
                ],
                
                'license' => [
                    '#type' => 'textfield',
                    '#title' => t('License'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->license : ''
                ],
                
                'abstract' => [
                    '#type' => 'text_format',
                    '#title' => t('Abstract'),
                    '#required' => true,
                    '#format' => isset($abstract) ? $abstract->format : 'basic_html',
                    '#default_value' => isset($abstract) ? $abstract->value : ''
                ],
                
                'publication_place' => [
                    '#type' => 'textfield',
                    '#title' => t('Publication Place'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->publication_place : ''
                ],
                
                'authors' => [
                    '#type' => 'select',
                    '#multiple' => true,
                    '#options' => getUsersByRole(\Drupal::database(), [2,3,4,5,6,7]),
                    '#title'=> (string)t('Authors'),
                    '#default_value' => isset($workflow_data) ? explode(',', $workflow_data->authors) : []
                ],
                
                'corporation' => [
                    '#type' => 'textfield',
                    '#title' => t('Corporation'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->corporation : ''
                ],
                
                'conflict_of_interest' => [
                    '#type' => 'checkbox',
                    '#title' => (string)t('Conflict of interest'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->conflict_of_interest : 0
                ],
                
                'conflict_of_interest_text' => [
                    '#type' => 'textfield',
                    '#title' => t('Description conflict of interest'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->conflict_of_interest_text : ''
                ],
                
                'accept_policy' => [
                    '#type' => 'checkbox',
                    '#title' => (string)t('I accept the policy and the author\'s contract'),
                    '#attributes' => array('disabled' => 'disabled'),
                    '#value' => $workflow_data->accept_policy
                ],
                
                'doi' => [
                    '#type' => 'textfield',
                    '#title' => t('DOI'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->doi : ''
                ],
                
                'ddc' => [
                    '#type' => 'textfield',
                    '#title' => t('DDC'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->ddc : ''
                ],
                
                'corresponding_authors' => [
                    '#type' => 'select',
                    '#multiple' => true,
                    '#options' => getUsersByRole(\Drupal::database(), [2,3,4,5,6,7]),
                    '#title' => t('Corresponding Author(s)'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? explode(',', $workflow_data->corresponding_authors) : ''
                ],
                
                'article_text' => [
                    '#type' => 'text_format',
                    '#title' => t('Article Text'),
                    '#required' => true,
                    '#rows' => 50,
                    '#format' => isset($article_text) ? $article_text->format : 'full_html',
                    '#default_value' => isset($article_text) ? $article_text->value : ''
                ],
                
                'urn' => [
                    '#type' => 'textfield',
                    '#title' => t('URN'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->urn : ''
                ],
                
                'keywords' => [
                    '#type' => 'textfield',
                    '#title' => t('Keywords'),
                    '#default_value' => isset($workflow_data) ? implode(', ', json_decode($workflow_data->keywords)) : ''
                ],
                
                'funding_name' => [
                    '#type' => 'textfield',
                    '#title' => t('Funding Name'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->funding_name : ''
                ],
                
                'funding_id' => [
                    '#type' => 'textfield',
                    '#title' => t('Funding ID'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->funding_id : ''
                ],
                
                'article_type' => [
                    '#type' => 'textfield',
                    '#title' => t('Article-type'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->article_type : ''
                ],
                
                'doc_number' => [
                    '#type' => 'textfield',
                    '#title' => t('docNumber'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->doc_number : ''
                ],
                
                'description_errata' => [
                    '#type' => 'markup',
                    '#markup' => '<span class="rwAlert">'.t('You can set Version OR Errata OR Correction!').'</span>'
                ],
                
                'version' => [
                    '#type' => 'textfield',
                    '#title' => t('Version'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->version : ''
                ],
                /*
                'errata' => [
                    '#type' => 'text_format',
                    '#title' => t('Errata'),
                    '#format' => 'full_html',
                    '#format' => isset($errata) ? $errata->format : 'full_html',
                    '#default_value' => isset($errata) ? $errata->value : ''
                ],
                
                'correction' => [
                    '#type' => 'text_format',
                    '#title' => t('Correction'),
                    '#format' => 'full_html',
                    '#format' => isset($correction) ? $correction->format : 'full_html',
                    '#default_value' => isset($correction) ? $correction->value : ''
                ],
                */
                'comment' => [
                    '#type' => 'text_format',
                    '#format' => 'basic_html',
                    '#title' => (string)t('Comment'),
                    '#format' => isset($comment) ? $comment->format : 'full_html',
                    '#default_value' => isset($comment) ? $comment->value : ''
                ],
                
                'submit' => [
                    '#type' => 'submit',
                    '#value' => (string)t('Request clearing from EiC')
                ]
                
            ];
        }
        
        if(isset($conference)){
            
            $form = [
                
                'site_headline' => [
                '#type' => 'markup',
                '#markup' => '<h1>'.t('Add Article').'</h1>'
                ],
                
                'conference_title' => [
                    '#type' => 'markup',
                    '#markup' => '<h2>'.$conference['cf_title'].'</h2>'
                ],
                
                'author' => [
                    '#type' => 'markup',
                    '#markup' => $author['profile_graduation'].' '.$author['profile_lastname'].', '.$author['profile_firstname'].' '.$author['profile_graduation_suffix'],
                    '#prefix' => '<h4>'.(string)t('Author').': ',
                    '#suffix' => '</h4>'
                ],
                
                'title' => [
                    '#type' => 'textfield',
                    '#title' => t('Title'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->title : ''
                ],
                
                'title_translated' => [
                    '#type' => 'textfield',
                    '#title' => t('Title translated'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->title_translated : ''
                ],
                
                'article_type' => [
                    '#type' => 'textfield',
                    '#title' => t('Article type'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->article_type : ''
                ],
                
                'abstract' => [
                    '#type' => 'text_format',
                    '#title' => t('Abstract'),
                    '#required' => true,
                    '#format' => isset($abstract) ? $abstract->format : 'basic_html',
                    '#default_value' => isset($abstract) ? $abstract->value : ''
                ],
                
                'volume' => [
                    '#type' => 'textfield',
                    '#title' => t('Volume'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->volume : ''
                ],
                
                'license' => [
                    '#type' => 'textfield',
                    '#title' => t('Licence'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->license : ''
                ],
                
                'publication_year' => [
                    '#type' => 'textfield',
                    '#title' => t('Publication Year'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->publication_year : ''
                ],
                
                'publication_place' => [
                    '#type' => 'textfield',
                    '#title' => t('Publication Place'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->publication_place : ''
                ],
                
                'publisher' => [
                    '#type' => 'textfield',
                    '#title' => t('Publisher'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->publisher : ''
                ],
                
                'doi' => [
                    '#type' => 'textfield',
                    '#title' => t('DOI'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->doi : ''
                ],
                
                'ddc' => [
                    '#type' => 'textfield',
                    '#title' => t('DDC'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->ddc : ''
                ],
                
                'corresponding_author' => [
                    '#type' => 'textfield',
                    '#title' => t('Corresponding Author'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->corresponding_author : ''
                ],
                
                'article_text' => [
                    '#type' => 'text_format',
                    '#title' => t('Chapter Text'),
                    '#required' => true,
                    '#rows' => 50,
                    '#format' => isset($article_text) ? $article_text->format : 'full_html',
                    '#default_value' => isset($article_text) ? $article_text->value : ''
                ],
                
                'urn' => [
                    '#type' => 'textfield',
                    '#title' => t('URN'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->urn : ''
                ],
                
                'keywords' => [
                    '#type' => 'textfield',
                    '#title' => t('Keywords'),
                    '#default_value' => isset($workflow_data) ? implode(', ', json_decode($workflow_data->keywords)) : ''
                ],
                
                'funding_name' => [
                    '#type' => 'textfield',
                    '#title' => t('Funding name'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->funding_name : ''
                ],
                
                'funding_id' => [
                    '#type' => 'textfield',
                    '#title' => t('Funding ID'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->funding_id : ''
                ],
                
                'issue' => [
                    '#type' => 'textfield',
                    '#title' => t('Issue'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->issue : ''
                ],
                
                'issue_title' => [
                    '#type' => 'textfield',
                    '#title' => t('Issue title'),
                    '#required' => true,
                    '#default_value' => isset($workflow_data) ? $workflow_data->issue_title : ''
                ],
                
                'description_errata' => [
                    '#type' => 'markup',
                    '#markup' => '<span class="rwAlert">'.t('You can set Version OR Errata OR Correction!').'</span>'
                ],
                
                'version' => [
                    '#type' => 'textfield',
                    '#title' => t('Version'),
                    '#default_value' => isset($workflow_data) ? $workflow_data->version : ''
                ],
                /*
                'errata' => [
                    '#type' => 'text_format',
                    '#title' => t('Errata'),
                    '#format' => 'full_html'
                ],
                
                'correction' => [
                    '#type' => 'text_format',
                    '#title' => t('Correction'),
                    '#format' => 'full_html'
                ],
                */
                'comment' => [
                    '#type' => 'text_format',
                    '#format' => 'basic_html',
                    '#title' => (string)t('Comment')
                ],
                
                'submit' => [
                    '#type' => 'submit',
                    '#value' => (string)t('Request clearing from EiC')
                ]
                
            ];
        }
        
        $form['#cache']['max-age'] = 0;
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $delta = ['workflow', 'workflow_data'];
        foreach($delta as $_) $$_ = $form_state->get($_);
        
        //save comment if esists
        if($form_state->getValue('comment')){
            
            $comment = $form_state->getValue('comment');
            
            if($comment['value'] != ''){
                
                $comment = base64_encode(json_encode($comment));
                
                \Drupal::database()
                    ->insert('rwPubgoldWorkflowComments')
                    ->fields([
                        'wfc_created_by_uid' => $_SESSION['user']['id'],
                        'wfc_comment' => $comment,
                        'wfc_wfid' => $workflow->wf_id
                    ])
                    ->execute();
            }
        }
        
        //prepare workflow for history
        $workflow_history = array();
        $workflow_history['wfh_created_by_uid'] = $_SESSION['user']['id'];
        
        foreach($workflow as $k => $v){
            
            if($k == 'wf_id'){
                $key = 'wfh_wfid';
            }
            else{
                
                $key = explode("_", $k, 2);
                $key = $key[0].'h_'.$key[1];
                
            }
            
            $workflow_history[$key] = $v;
        }
        
        \Drupal::database()->insert('rwPubgoldWorkflowHistory')->fields($workflow_history)->execute();
        
        foreach($form_state->getValues() as $k => $v)
          $workflow_data->$k = $v;
        
        $workflow_data->abstract               = base64_encode(json_encode($form_state->getValue('abstract')));
        $workflow_data->chapter_text           = base64_encode(json_encode($form_state->getValue('chapter_text')));
        $workflow_data->article_text           = base64_encode(json_encode($form_state->getValue('article_text')));
        $workflow_data->paper_text             = base64_encode(json_encode($form_state->getValue('paper_text')));
        $workflow_data->errata                 = base64_encode(json_encode($form_state->getValue('errata')));
        $workflow_data->correction             = base64_encode(json_encode($form_state->getValue('correction')));
        $workflow_data->license                = $form_state->getValue('license');
        $workflow_data->publication_year       = $form_state->getValue('publication_year');
        $workflow_data->publication_place      = $form_state->getValue('publication_place');
        $workflow_data->publisher              = $form_state->getValue('publisher');
        $workflow_data->doi                    = $form_state->getValue('doi');
        $workflow_data->ddc                    = $form_state->getValue('ddc');
        $workflow_data->corresponding_authors  = implode(',', $form_state->getValue('corresponding_authors'));
        $workflow_data->chapter_text           = base64_encode(json_encode($form_state->getValue('chapter_text')));
        $workflow_data->urn                    = $form_state->getValue('urn');
        $workflow_data->keywords               = json_encode(preg_split('/[^a-zA-Z0-9äöüÄÖÜß]+/', $form_state->getValue('keywords')));
        $workflow_data->funding_number         = $form_state->getValue('funding_number');
        $workflow_data->funding_id             = $form_state->getValue('funding_id');
        $workflow_data->version                = $form_state->getValue('version');
        $workflow_data->title                  = $form_state->getValue('title');
        $workflow_data->title_translated       = $form_state->getValue('title_translated');
        $workflow_data->state                  = 'editor clearing request';
        $workflow_data->authors                = implode(',', $form_state->getValue('authors'));
        $workflow_data->corporation            = $form_state->getValue('corporation');
        $workflow_data->conflict_of_interest   = $form_state->getValue('conflict_of_interest');
        $workflow_data->conflict_of_interest_text = $form_state->getValue('conflict_of_interest_text');
        $workflow_data->author_contract        = $form_state->getValue('author_contract');
        $workflow_data->patients_rights        = $form_state->getValue('patients_rights');
        $workflow_data->article_type           = $form_state->getValue('article_type');
        $workflow_data->doc_number             = $form_state->getValue('doc_number');

        $workflow->wf_data = base64_encode(json_encode($workflow_data));
        
        \Drupal::database()
            ->update('rwPubgoldWorkflow')
            ->fields([
                'wf_data' => $workflow->wf_data,
                'wf_assigned_to' => 'u:'.$workflow->wf_eic_last_edited,
                'wf_locked' => 0,
                'wf_locked_by_uid' => null
            ])
            ->condition('wf_id', $form_state->get('wf_id'), '=')
            ->execute();
        
        return $form;
    }
}
