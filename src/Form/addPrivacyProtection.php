<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\addPrivacyProtection.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;
use Drupal\publisso_gold\Controller\PrivacyProtection;

/**
 * Provides a simple example form.
 */
class addPrivacyProtection extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(){
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'addPrivacyProtection';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
        
        $session        = \Drupal::service('session');
        $texts          = \Drupal::service('publisso_gold.texts');
        $formFields     = \Drupal::service('publisso_gold.form.fields');
        
        $form['language'] = $formFields->getField('add_pp_information.language', $args);
        $form['valid_from' ] = $formFields->getField('add_pp_information.valid_from' , $_args);
        $form['valid_from']['#default_value'] = date('Y-m-d');
        $form['description'] = $formFields->getField('add_pp_information.description', $_args);
        $form['text'       ] = $formFields->getField('add_pp_information.text'       , $_args);
        
        $form['back'] = [
                '#type'                    => 'submit',
                '#button_type'             => 'warning',
                '#value'                   => (string)$this->t($texts->get('form.add_pp_information.btn.back', 'fc')),
                '#prefix'                  => '<hr>',
                '#limit_validation_errors' => [],
                '#submit'                  => ['::back']
        ];
        
        $form['submit'] = [
                '#type'   => 'submit',
                '#value'  => (string)$this->t($texts->get('global.save', 'fc')),
                '#prefix' => '&nbsp;&nbsp;&nbsp;',
                '#suffix' => '<br><br>'
        ];
        
        $form['#cache']['max-age'] = 0;
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $texts  = \Drupal::service('publisso_gold.texts');
        $pp     = new PrivacyProtection(\Drupal::service('session'), $texts, \Drupal::service('publisso_gold.tools'));
        
        if($pp->createNew($valid_from, $form_state->getValue('description'), $form_state->getValue('text'), $form_state->getValue('language'))){
                \Drupal::service('messenger')->addMessage((string)$this->t($texts->get('add_pp_information.save.success', 'fc')));
                $form_state->setRedirect('publisso_gold.privacy_protection');
        }
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function back(array &$form, FormStateInterface $form_state){
        $form_state->setRedirect('publisso_gold.privacy_protection');
    }
}
