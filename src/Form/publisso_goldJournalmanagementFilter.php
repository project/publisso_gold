<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldJournalmanagementFilter.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Form\FormState;
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Mail;
        use Drupal\Core\Url;
        use Drupal\Core\Link;
        use Drupal\Core\Database\Connection;
        use Drupal\file\Element;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Symfony\Component\HttpFoundation\File;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldJournalmanagementFilter extends FormBase {
                
                const __ACCESS_DENIED__ = 'You can not access this page. You may need to log in first.';
                
                private $modname         = 'publisso_gold';
                private $database;
                private $modpath;
                private $user            = null;
                private $selected_bookid = '';
                private $selected_author = '';
                private $selected_editor = '';
                private $modaccessweights;
                
                public function __construct ( Connection $database ) {
                        $this->database = $database;
                        $this->user = $this->database->select( 'rwpgvwUserProfiles', 'u' )->fields( 'u', [] )->condition( 'id', \Drupal::service( 'session' )->get( 'user' )[ 'id' ], '=' )->execute()->fetchAssoc();
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return publisso_goldJournalmanagementFilter|static
                 */
                public static function create (ContainerInterface $container ) {
                        return new static( $container->get( 'database' ) );
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'publisso_goldjournalmanagementfilter';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state ) {
                        
                        if ( !$this->database ) $this->database = \Drupal::database();
                        
                        if ( !$this->user ) $this->user = $this->database->select( 'rwpgvwUserProfiles', 'u' )->fields( 'u', [] )->condition( 'id', \Drupal::service( 'session' )->get( 'user' )[ 'id' ], '=' )->execute()->fetchAssoc();
                        
                        $this->modpath = drupal_get_path( 'module', $this->modname );
                        require_once( $this->modpath . '/inc/publisso_gold.lib.inc.php' );
                        
                        $journals = [];
                        
                        foreach ( getJournals( \Drupal::database() ) as $journal ) {
                                
                                $journals[ $journal->jrn_id ] = [ 'jrn_title' => $journal->jrn_title, 'jrn_bibliographic_abbr' => $journal->jrn_title_abbr ];
                        }
                        
                        //get template(s)
                        $tmpl_head = file_get_contents( $this->modpath . '/inc/publisso_gold_journalmanagement_journallist_head.tmpl.inc.php' );
                        $tmpl_data = file_get_contents( $this->modpath . '/inc/publisso_gold_journalmanagement_journallist_data.tmpl.inc.php' );
                        $tmpl_foot = file_get_contents( $this->modpath . '/inc/publisso_gold_journalmanagement_journallist_foot.tmpl.inc.php' );
                        
                        $markup = '';
                        $tmpl_vars = [];
                        $markup .= $tmpl_head;
                        
                        foreach ( $journals as $jrn_id => $_ ) {
                                
                                $tmpl = $tmpl_data;
                                $vars = [];
                                
                                $vars[ '::title::' ] = $_[ 'jrn_title' ];
                                $vars[ '::bibliographic_abbr::' ] = $_[ 'jrn_title_abbr' ] ?? '';
                                $vars[ '::lnk_journal::' ] = Link::fromTextAndUrl( (string)t( 'View Journal' ), Url::fromRoute( 'publisso_gold.journals_journal', [ 'jrn_id' => $jrn_id ] ) )->toString();
                                $vars[ '::lnk_journal_edit::' ] = Link::fromTextAndUrl( (string)$this->t( 'Edit Journal' ), Url::fromRoute( 'publisso_gold.edit_journal', [ 'jrn_id' => $jrn_id ] ) )->toString();
                                $vars[ '::jrn_id::' ] = $jrn_id;
                                $vars[ '::lnk_journal_addvolume::' ] = Link::fromTextAndUrl( (string)t( 'Add Volume' ), Url::fromRoute( 'publisso_gold.journalmanagement.volume.add', [ 'jrn_id' => $jrn_id ] ) )->toString();
                                $vars[ '::lnk_journal_addissue::' ] = Link::fromTextAndUrl( (string)t( 'Add Issue' ), Url::fromRoute( 'publisso_gold.journalmanagement.issue.add', [ 'jrn_id' => $jrn_id ] ) )->toString();
                                $vars[ '::lnk_journal_assroles::' ] = Link::fromTextAndUrl( (string)t( 'Assign Roles' ), Url::fromRoute( 'publisso_gold.journal.management.roles.assign', [ 'jrn_id' => $jrn_id ] ) )->toString();
                                
                                $tmpl = $this->renderVars( $tmpl, $vars );
                                $markup .= $tmpl;
                        }
                        
                        $markup .= $tmpl_foot;
                        
                        $tmpl_vars[ '::txt_hl_title::' ] = (string)t( 'Title' );
                        $tmpl_vars[ '::txt_hl_bibliographic_abbr::' ] = (string)t( 'Bibliographic Abbr.' );
                        
                        $markup = $this->renderVars( $markup, $tmpl_vars );
                        
                        $form[ 'journals' ] = [ '#type' => 'markup', '#markup' => $markup ];
                        
                        $form[ '#cache' ] = [ 'max-age' => 0 ];
                        
                        return $form;
                }
                
                private function getModuleAccessPermissions () {
                        
                        $result = $this->database->select( 'rwPubgoldModuleaccess', 'ma' )->fields( 'ma', [] )->execute()->fetchAll();
                        
                        foreach ( $result as $res ) {
                                $this->modaccessweights[ $res->mod_name ] = $res->mod_minaccessweight;
                        }
                }
        
                /**
                 * @param $tmpl
                 * @param array $vars
                 * @return string|string[]|null
                 */
                private function renderVars ($tmpl, $vars = [] ) {
                        
                        //set Site-Vars
                        $tmpl = str_replace( array_keys( $vars ), array_values( $vars ), $tmpl );
                        
                        //remove unused vars
                        $tmpl = preg_replace( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
                        
                        return $tmpl;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function apply_filter (array &$form, FormStateInterface $form_state ) {
                        
                        $form_state->setRebuild();
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                
                }
        
                /**
                 * @param $modname
                 * @return bool
                 */
                private function userHasAccessRight ($modname ) {
                        
                        if ( !array_key_exists( $modname, $this->modaccessweights ) ) {
                                return false;
                        }
                        return $this->modaccessweights[ $modname ] <= \Drupal::service( 'session' )->get( 'user' )[ 'weight' ];
                }
        }
