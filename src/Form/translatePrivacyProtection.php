<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\translatePrivacyProtection.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;
use Drupal\publisso_gold\Controller\PrivacyProtection;

/**
 * Provides a simple example form.
 */
class translatePrivacyProtection extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(){
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'translatePrivacyProtection';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
        
        $session = \Drupal::service('session');
        $texts = \Drupal::service('publisso_gold.texts');
        $tools = \Drupal::service('publisso_gold.tools');
        $formFields = \Drupal::service('publisso_gold.form.fields');
        
        if(!$form_state->has('date')) $form_state->set('date', $args['date']);
        $date = $form_state->get('date');
        
        $pp = new \Drupal\publisso_gold\Controller\PrivacyProtection($session, $texts, $tools);
        $args['strings']['@language'] = $pp->getDefaultLanguage($date) ? \Drupal::service('language_manager')->getLanguageName($pp->getDefaultLanguage($date)) : 'undefined';
        
        $langExists = [];
        $langExists[$pp->getDefaultLanguage($date)] = \Drupal::service('language_manager')->getLanguageName($pp->getDefaultLanguage($date));
        foreach($pp->getTranslations($date) as $lang) $langExists[$lang] = \Drupal::service('language_manager')->getLanguageName($lang);
        
        $form['orig_text'  ] = $formFields->getField('add_pp_information.orig_text'  , $args);
        $form['language'   ] = $formFields->getField('add_pp_information.language'   , $args);
        $form['text'       ] = $formFields->getField('add_pp_information.text'       , $args);
        
        $form['language']['#options'] = array_diff($form['language']['#options'], $langExists);
        
        $form['submit'] = [
                '#type' => 'submit',
                '#value' => (string)t('Save'),
                '#prefix' => '<hr>',
                '#suffix' => '<br><br>'
        ];
        
        $form['#cache']['max-age'] = 0;
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $texts  = \Drupal::service('publisso_gold.texts');
        $pp     = new PrivacyProtection(\Drupal::service('session'), $texts, \Drupal::service('publisso_gold.tools'));
        $date   = $form_state->get('date');
        
        $pp->setTranslation($form_state->getValue('language'), $form_state->getValue('text'), $date);
        $form_state->setRedirect('publisso_gold.privacy_protection', ['action' => 'list']);
        
        return $form;
    }
}
