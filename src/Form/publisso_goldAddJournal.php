<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldAddJournal.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldAddJournal extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $num_corporations;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldAddJournal|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldaddjournal';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        if($this->modname && !$form_state->get('modname')){
            $form_state->set('modname', $this->modname);
        }
        
        if(!$this->modname && $form_state->get('modname')){
            $this->modname = $form_state->get('modname');
        }
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
        $rp = [];
        
        //get review-pages
        foreach(getReviewpages() as $page){
          $rp[$page->rp_id] = $page->rp_name;
        }
        
        $form['site_headline'] = [
            '#type' => 'markup',
            '#markup' => '<h1>'.t('Add Journal').'</h1>'
        ];
        
        $form['rp_id'] = [
            '#type' => 'select',
            '#title' => (string)t('select review-page'),
            '#options' => $rp,
            '#required' => true
        ];
        
        $form['title'] = [
            '#type' => 'textfield',
            '#title' => t('Title'),
            '#required' => true
        ];
        
        $form['bibliographic_abbr'] = [
            '#type' => 'textfield',
            '#title' => t('Title abbreviation'),
            '#required' => true
        ];
        
        $form['logo'] = [
            '#type' => 'managed_file',
            '#title' => t('Logo'),
            '#required' => false
        ];
        
        $form['cover'] = [
            '#type' => 'managed_file',
            '#title' => t('Cover'),
            '#required' => false
        ];
        
        //init counter
        if(!$form_state->get('num_sustaining_members'))
           $form_state->set('num_sustaining_members', 0);
        
        $max = $form_state->get('num_sustaining_members');
        
        $form['sustaining_members'] = array(
          '#tree' => TRUE,
          '#prefix' => '<div id="sustaining_members-replace">',
          '#suffix' => '</div>'
        );
        
        for ($delta = 0; $delta < $max; $delta++) {
            
            if (!isset($form['sustaining_members'][$delta])) {
                
                $element = array(
                    '#type' => 'managed_file'
                );
                
                if($delta == 0){
                    $element['#title'] = t('Sustaining members');
                }
                
                $form['sustaining_members'][$delta] = $element;
            }
        }
        
        $form['addSustainingMembers'] = array(
            '#type' => 'submit',
            '#name' => 'addSustainingMembers',
            '#value' => t('Add sustaining member'),
            '#submit' => array('::addMoreSMSubmit'),
            '#suffix' => '<br><br>',
            '#prefix' => '<br><br>',
            
            '#ajax' => array(
              'callback' => '::addMoreSMCallback',
              'wrapper' => 'sustaining_members-replace',
              'effect' => 'fade',
            ),
            
        );
        
        //init counter
        if(!$form_state->get('num_corporation'))
           $form_state->set('num_corporation', 0);
        
        $max = $form_state->get('num_corporation');
        
        $form['corporation'] = array(
          '#tree' => TRUE,
          '#prefix' => '<div id="corporation-replace">',
          '#suffix' => '</div>'
        );
        
        for ($delta = 0; $delta < $max; $delta++) {
            
            if (!isset($form['corporation'][$delta])) {
                
                $element = array(
                    '#type' => 'textfield'
                );
                
                if($delta == 0){
                    $element['#title'] = t('Corporation');
                }
                
                $form['corporation'][$delta] = $element;
            }
        }
        
        $form['addCorporation'] = array(
            '#type' => 'submit',
            '#name' => 'addCorporation',
            '#value' => t('Add Corporation'),
            '#submit' => array('::addMoreSubmit'),
            
            '#ajax' => array(
              'callback' => '::addMoreCallback',
              'wrapper' => 'corporation-replace',
              'effect' => 'fade',
            ),
            
        );
        
        $form['volume'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Volume'),
            '#required' => true
        ];
        
        $form['volume_title'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Volume title'),
            '#required' => false
        ];
        
        $form['issue'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Issue'),
            '#required' => false
        ];
        
        $form['issue_title'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Issue title'),
            '#required' => false
        ];
        
        $form['publication_place'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Publication place'),
            '#required' => false,
            '#default_value' => 'Cologne'
        ];
        
        $form['publisher'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Publisher'),
            '#required' => false,
            '#default_value' => 'PUBLISSO'
        ];
        
        
        $form['issn'] = [
            '#type' => 'textfield',
            '#title' => t('ISSN')
        ];
        
        $form['ddc'] = [
            '#type' => 'textfield',
            '#title' => t('DDC'),
            '#required' => true
        ];
        
        $form['category'] = [
            '#type' => 'select',
            '#title' => t('Category'),
            '#options' => getBookCategories(\Drupal::database()),
            '#required' => true
        ];
        
        $form['editorial_office'] = [
            '#type' => 'select',
            '#multiple' => true,
            '#options' => getUsersByRole(\Drupal::database(), [2,3,4,5,6,7]),
            '#title' => (string)t('Editorial Office'),
            '#required'=> true
        ];
        
        $form['editors_in_chief'] = [
            '#type' => 'select',
            '#multiple' => true,
            '#options' => getUsersByRole(\Drupal::database(), [2,3,4,5,6,7]),
            '#title' => (string)t('Editors-in-Chief'),
            '#required' => true
        ];
        
        $form['editors'] = [
            '#type' => 'select',
            '#title' => t('Editor(s)'),
            '#required' => true,
            '#multiple' => true,
            '#options' => getUsersByRole($this->database, [2,3,4,5,6,7])
        ];
        
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => t('Create Journal')
        ];
        
        $form['#attributes'] = array(
            'enctype' => 'multipart/form-data'
        );
        
        $form['#cache']['max-age'] = 0;
        
        $form_state->setCached(false);
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        //process file-upload
        $logo = array();
        $logo['content'] = NULL;
        $logo['type'] = NULL;
        
        $cover = array();
        $cover['content'] = NULL;
        $cover['type'] = NULL;
        
        $fid = $form_state->getValue('logo');
        if($fid){
            $logo = \Drupal::database()->select('file_managed', 'f')
              ->condition('f.fid', $fid, '=')
              ->fields('f', array())
              ->execute()->fetchAssoc();
            $logo['path'] = str_replace('temporary:/', '/tmp', $logo['uri']);
            $logo['content'] = bin2hex(file_get_contents($logo['path']));
        }
        
        $fid = $form_state->getValue('cover');
        if($fid){
            $cover = \Drupal::database()->select('file_managed', 'f')
              ->condition('f.fid', $fid, '=')
              ->fields('f', array())
              ->execute()->fetchAssoc();
            $cover['path'] = str_replace('temporary:/', '/tmp', $cover['uri']);
            $cover['content'] = bin2hex(file_get_contents($cover['path']));
        }
        
        $sustaining_members = $form_state->getValue('sustaining_members');
        $sustaining_members_images = [];
        
        foreach($sustaining_members as $_){
            
            $fid = $_[0];
            
            if($fid){
                $result = \Drupal::database()->select('file_managed', 'f')
                    ->condition('f.fid', $fid, '=')
                    ->fields('f', array())
                    ->execute()->fetchAssoc();
                  $result['path'] = str_replace('temporary:/', '/tmp', $result['uri']);
                  preg_match('/(.+)\.{1}(.+$)/i', $result['filename'], $matches);
                  $sustaining_members_images[] = '0x'.bin2hex(file_get_contents($result['path']));
            }
        }
        
        //save journal in database -> gets last_insert_id
        $corporation = $form_state->getValue('corporation');
        
        $jrn_id = \Drupal::database()
            ->insert('rwPubgoldJournals')
            ->fields([
                    'jrn_title'                 => $form_state->getValue('title'),
                    'jrn_bibliographic_abbr'    => $form_state->getValue('bibliographic_abbr'),
                    'jrn_issn'                  => $form_state->getValue('issn'),
                    'jrn_corporation'           => count($corporation) ? json_encode($corporation) : null,
                    'jrn_logo'                  => ($logo['content'] ? '0x'.$logo['content'] : null),
                    'jrn_cover'                 => ($cover['content'] ? '0x'.$cover['content'] : null),
                    'jrn_volume'                => $form_state->getValue('volume'),
                    'jrn_volume_title'          => $form_state->getValue('volume_title'),
                    'jrn_issue'                 => $form_state->getValue('issue'),
                    'jrn_issue_title'           => $form_state->getValue('issue_title'),
                    'jrn_publication_place'     => $form_state->getValue('publication_place'),
                    'jrn_publisher'             => $form_state->getValue('publisher'),
                    'jrn_ddc'                   => $form_state->getValue('ddc'),
                    'jrn_bcatid'                => $form_state->getValue('category'),
                    'jrn_rpid'                  => $form_state->getValue('rp_id')
            ])
            ->execute();
        
        //save editors-in-chief
        foreach($form_state->getValue('editors_in_chief') as $_){
            \Drupal::database()
                ->insert('rwPubgoldJournalsEditorsInChief')
                ->fields([
                    'jeic_jrnid' => $jrn_id,
                    'jeic_uid' => $_
                ])
                ->execute();
        }
        
        //save editorial office
        foreach($form_state->getValue('editorial_office') as $_){
            \Drupal::database()
                ->insert('rwPubgoldJournalsEditorialOffice')
                ->fields([
                    'jeo_jrnid' => $jrn_id,
                    'jeo_uid' => $_
                ])
                ->execute();
        }
        
        //save editors
        foreach($form_state->getValue('editors') as $_){
            \Drupal::database()
                ->insert('rwPubgoldJournalEditors')
                ->fields([
                    'je_journalid' => $jrn_id,
                    'je_uid' => $_
                ])
                ->execute();
        }
        
        //save sustaining members
        foreach($sustaining_members_images as $_){
            \Drupal::database()
                ->insert('rwPubgoldJournalsSustainingMembers')
                ->fields([
                    'jsm_jrnid' => $jrn_id,
                    'jsm_image' => $_
                ])
                ->execute();
        }
        
        #$form_state->cleanValues();
        #drupal_set_message(t('Journal successfully created!'), 'status');
        $form_state->setRedirect('publisso_gold.journalmanagement_journal', ['jrn_id' => $jrn_id]);
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function add_corporation(array &$form, FormStateInterface $form_state){
        
        $form['corporation'][] = [
            '#type' => 'textfield',
            '#title' => t('Corporation')
        ];
        
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addMoreSubmit(array &$form, FormStateInterface $form_state) {
        
        $form_state->set('num_corporation', $form_state->get('num_corporation') + 1);
        $form['corporation'][] = [
            '#type' => 'textfield',
            '#title' => t('Corporation')
        ];
        
        $form_state->setRebuild();
        $form_state->setRebuild();
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addMoreCallback(array &$form, FormStateInterface $form_state) {
        return $form['corporation'];
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function add_sustaining_members(array &$form, FormStateInterface $form_state){
        
        $form['sustaining_members'][] = [
            '#type' => 'textfield',
            '#title' => t('Sustaining members')
        ];
        
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addMoreSMSubmit(array &$form, FormStateInterface $form_state) {
        
        $form_state->set('num_sustaining_members', $form_state->get('num_sustaining_members') + 1);
        $form['sustaining_members'][] = [
            '#type' => 'textfield',
            '#title' => t('Sustaining members')
        ];
        
        $form_state->setRebuild();
        $form_state->setRebuild();
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addMoreSMCallback(array &$form, FormStateInterface $form_state) {
        return $form['sustaining_members'];
    }
}
