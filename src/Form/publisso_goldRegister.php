<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldRegister.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Mail;
        use Drupal\Core\Link;
        use Drupal\Core\Url;
        use Drupal\Core\Database\Connection;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_sso\Classes\SSO;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldRegister extends FormBase {
                
                private $modname = 'publisso_gold';
                private $database;
                private $modpath;
                
                public function __construct ( Connection $database ) {
                        $this->database = $database;
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return publisso_goldRegister|static
                 */
                public static function create (ContainerInterface $container ) {
                        return new static( $container->get( 'database' ) );
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'publisso_goldregister';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state ) {
                        
                        $t = \Drupal::service( 'publisso_gold.texts' );
                        
                        $this->modpath = drupal_get_path( 'module', $this->modname );
                        require_once( $this->modpath . '/inc/publisso_gold.lib.inc.php' );
                        
                        $form[ 'username' ] = [
                                '#type'     => 'textfield',
                                '#title'    => t( 'Username' ),
                                '#required' => true,
                        ];
                        
                        $form[ 'info' ] = [
                                '#type'     => 'inline_template',
                                '#template' => '
                        <hr>
                        The password must meet the following requirements:<br>
                        <ul style="margin-left: 50px !important;">
                                <li>at least 8 characters</li>
                                <li>letters and numbers</li>
                                <li>punctuation (allowed: ,.;:-_!?&%$=#+*~)</li>
                        </ul>
                        If the password does not meet the requirements, an error is returned!
                ',
                        ];
                        
                        $form[ 'password' ] = [
                                '#type'     => 'password_confirm',
                                '#required' => true,
                                '#size'     => '150%',
                                '#suffix'   => '<hr>',
                        ];
                        
                        $form[ 'email' ] = [
                                '#type'     => 'email',
                                '#title'    => t( 'Emailaddress' ),
                                '#required' => true,
                        ];
                        
                        $form[ 'firstname' ] = [
                                '#type'     => 'textfield',
                                '#title'    => t( 'First Name' ),
                                '#required' => true,
                        ];
                        
                        $form[ 'lastname' ] = [
                                '#type'     => 'textfield',
                                '#title'    => t( 'Last Name' ),
                                '#required' => true,
                        ];
                        
                        $form[ 'form' ][ 'fs_pp' ] = [
                                '#tree'   => true,
                                '#type'   => 'fieldset',
                                '#title'  => (string)$this->t( $t->get( 'userprofile.edit.pp.headline', 'fc' ) ),
                                'content' => [
                                        'pp'     => [
                                                'desc'  => [
                                                        '#markup' => nl2br(
                                                                (string)$this->t(
                                                                        $t->get( 'userprofile.edit.pp.description' ),
                                                                        [
                                                                                '@link_pp' => Link::fromTextAndUrl(
                                                                                        (string)$this->t( $t->get( 'userprofile.edit.pp.description.link' ) ),
                                                                                        Url::fromRoute( 'publisso_gold.privacy_protection', [ 'action' => 'view' ],
                                                                                                        [ 'attributes' => [ 'target' => '_blank' ] ]
                                                                                        )
                                                                                )->toString(),
                                                                        ]
                                                                )
                                                        ),
                                                ],
                                                'field' => [
                                                        '#type'     => 'checkbox',
                                                        '#title'    => (string)$this->t( $t->get( 'userprofile.edit.pp.agree.label', 'fc' ) ),
                                                        '#required' => true,
                                                        '#suffix'   => '<br>',
                                                ],
                                        ],
                                        'boards' => [
                                                'desc'  => [
                                                        '#markup' => nl2br( (string)$this->t( $t->get( 'userprofile.edit.boards.description' ) ) ),
                                                        '#suffix' => '<br>',
                                                ],
                                                'field' => [
                                                        '#type'  => 'checkbox',
                                                        '#title' => (string)$this->t( $t->get( 'userprofile.edit.pp.agree.label', 'fc' ) ),
                                                ],
                                        ],
                                ],
                        ];
                        
                        $form[ 'submit' ] = [
                                '#type'  => 'submit',
                                '#value' => t( 'Submit' ),
                        ];
                        
                        $form[ 'space' ] = [
                                '#type'   => 'markup',
                                '#markup' => '<br><br>',
                        ];
                        
                        $form[ '#cache' ][ 'max-age' ] = 0;
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $lp = \Drupal::service( 'publisso_gold.login_provider' );
                        $sso = \Drupal::service( 'publisso_gold.sso_provider' );
                        
                        if ( !\Drupal::service( 'email.validator' )->isValid( $form_state->getValue( 'email' ) ) ) {
                                $form_state->setErrorByName( 'password', t( 'Please enter a valid email-address!' ) );
                        }
                        
                        if ( $lp->emailExists( $form_state->getValue( 'username' ) ) ) {
                                $form_state->setErrorByName( 'username', t( 'This username already exists (local)!' ) );
                        }
                        
                        if ( $lp->emailExists( $form_state->getValue( 'email' ) ) ) {
                                $form_state->setErrorByName( 'email', t( 'This email already exists (local)!' ) );
                        }
                        
                        if ( Publisso::setup()->getValue('system.sso.active') && SSO::ssoAvailable() ) {
                                
                                #$ret = $sso->emailExists( $form_state->getValue( 'username' ) );
                                $ret = SSO::emailExists($form_state->getValue('username'));
                                if ( $ret === null ) $form_state->setErrorByName( 'username', t( 'Failed to check username!' ) );
                                elseif ( $ret === true ) $form_state->setErrorByName( 'username', t( 'This username already exists (zbmed)!' ) );
                                
                                #$ret = $sso->emailExists( $form_state->getValue( 'email' ) );
                                $ret = SSO::emailExists($form_state->getValue('email'));
                                
                                if ( $ret === null ) $form_state->setErrorByName( 'email', t( 'Failed to check email!' ) );
                                elseif ( $ret === true ) $form_state->setErrorByName( 'email', t( 'This email already exists (zbmed)!' ) );
                        }
                        
                        if ( \Drupal::service( 'publisso_gold.tools' )->validatePasswordStrength( $form_state->getValue( 'password' ) )->code != 200 ) {
                                $form_state->setErrorByName( 'password', t( \Drupal::service( 'publisso_gold.tools' )->validatePasswordStrength( $form_state->getValue( 'password' ) )->msg ) );
                        }
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $uuid = md5( (int)uniqid() * time() );
                        $mail_system = \Drupal::service( 'plugin.manager.mail' )->getInstance( [ 'module' => $this->modname, 'key' => $uuid ] );
                        $tools = \Drupal::service( 'publisso_gold.tools' );
                        $messenger = \Drupal::service( 'messenger' );
                        
                        $url = Url::fromRoute( 'publisso_gold.confirm_register', [ 'token' => $uuid ] );
                        $url->setAbsolute();
                        $link = $url->toString();
                        
                        $pwEnc = $tools->getDBEncPassword( $form_state->getValue( 'password' ) );
                        //$link = 'http://'.$_SERVER['SERVER_NAME'].'/publisso_gold/confirm_register?regtoken='.$uuid;
                        
                        $data = [
                                'rp_uuid'                  => $uuid,
                                'rp_username'              => $form_state->getValue( 'username' ),
                                'rp_password'              => $pwEnc,
                                'rp_firstname'             => $form_state->getValue( 'firstname' ),
                                'rp_lastname'              => $form_state->getValue( 'lastname' ),
                                'rp_email'                 => $form_state->getValue( 'email' ),
                                'rp_editor'                => $form_state->getValue( 'editor' ) ? 1 : 0,
                                'rp_reviewer'              => $form_state->getValue( 'reviewer' ) ? 1 : 0,
                                'rp_reg_notification_sent' => 1,
                                'rp_password_clear'        => $form_state->getValue( 'password' ),
                                'rp_pp'                    => $form_state->getValue( 'fs_pp' )[ 'content' ][ 'pp' ][ 'field' ],
                                'rp_boards'                => $form_state->getValue( 'fs_pp' )[ 'content' ][ 'boards' ][ 'field' ],
                        ];
                        
                        $this->database->insert( 'rwPubgoldRegisterPending' )->fields( $data )->execute();
                        
                        $mail = (string)$this->t( "Thank you for registering at \"PUBLISSO\"!" ) . "\n"
                                . "\n"
                                . t( "To confirm your registration click the link below or copy it to the address-bar in your browser:" ) . "\n"
                                . $link . "\n"
                                . "\n"
                                . t( "Kindly regards" ) . "\n"
                                . t( "Your Publisso - Team" );
                        
                        $params = [
                                'headers' => [
                                        'MIME-Version'              => '1.0',
                                        'Content-Type'              => 'text/plain; charset=UTF-8;',
                                        'Content-Transfer-Encoding' => 'Base64',
                                        'X-Mailer'                  => 'Publisso Mailer',
                                        'From'                      => 'PUBLISSO <livingbooks@zbmed.de>',
                                ],
                                'to'      => $form_state->getValue( 'email' ),
                                'body'    => base64_encode( $mail ),
                                'subject' => (string)$this->t( 'Registration for PUBLISSO Books"' ),
                        ];
                        
                        $mail_system->mail( $params );
                        $form_state->cleanValues();
                        $messenger->addMessage( (string)$this->t( 'Thank you for registration! We\'ve sent an email with further informations!' ) );
                }
        
                /**
                 * @param $element
                 */
                public function fieldPWConfirm ($element ) {
                        
                        //$element['length'] = 20;
                        //return $element;
                }
        }
