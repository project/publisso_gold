<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldUsermanagementUser.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormState;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Url;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldUsermanagementUser extends FormBase {
    
    const __ACCESS_DENIED__             = 'You can not access this page. You may need to log in first.';
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $user = NULL;
    private $selected_roleid = '';
    private $selected_startletter = '';
    private $modaccessweights;
    
    public function __construct(Connection $database){
        $this->database = $database;
        $this->user = $this->database->select('rwpgvwUserProfiles', 'u')->fields('u', [])->condition('id', \Drupal::service('session')->get('user')['id'], '=')->execute()->fetchAssoc();
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldUsermanagementUser|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'publisso_goldusermanagementuser';
  }

        /**
        * {@inheritdoc}
        */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {

                $session = \Drupal::service('session');
                $currentUser = $session->get('user');

                if(!$form_state->has('uid')) $form_state->set('uid', $args['uid']);

                //get userroles less or equal my own, except guest
                $result = \Drupal::database()->select('rwPubgoldRoles', 'ur')->fields('ur', [])->condition('weight', 10, '>')->condition('weight', $currentUser['weight'], '<=')->execute()->fetchAll();
                
                $userroles = array();
                foreach($result as $res){
                        $userroles[$res->id] = $res->name;
                }

                $user = new \Drupal\publisso_gold\Controller\User($form_state->get('uid'));
                
                if($currentUser['weight'] >= $user->role['weight']){
                        
                        $form['roleid'] = array(
                                '#type' => 'select',
                                '#title' => t('Userrole'),
                                '#options' => $userroles,
                                '#default_value' => $user->role['id']
                        );
                        
                        $form['locked'] = array(
                                '#type' => 'checkbox',
                                '#title' => t('deny login'),
                                '#default_value' => $user->getElement('locked') & $user->getElement('active')
                        );
                        
                        //user kan sich nicht selbst sperren
                        if($user->getElement('id') == $currentUser['id']){
                                $form['locked']['#disabled'] = true;
                        }

                        $form['submit'] = array(
                                '#type' => 'submit',
                                '#value' => t('Save')
                        );
                }

                $form['#cache']['max-age'] = 0;
                return $form;
        }
  
    

        /**
        * {@inheritdoc}
        */
        public function validateForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                
                $user = new \Drupal\publisso_gold\Controller\User($form_state->get('uid'));
                
                if($user && $user->getElement('id')){
                
                        if(!$user->setRole($form_state->getValue('roleid'))){
                                \Drupal::service('messenger')->addError((string)$this->t('Failed to set userrole. See logfile for further information!'));
                        }
                        
                        $form_state->getValue('locked') ? $user->lock() : $user->unlock();
                }
                $form_state->setRebuild();
        }    
}
