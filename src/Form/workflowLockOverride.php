<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\workflowLockOverride.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class workflowLockOverride extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $currentUser;
    
    public function __construct(){
        $this->currentUser = new \Drupal\publisso_gold\Controller\User($_SESSION['user']['id']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'workflowLockOverride';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
        
        $session = \Drupal::service('session');
        $form_state->set('wf_id', $args['wf_id']);
        
        $form['message'] = [
                '#markup' => '<div>&nbsp;</div>','#prefix'=>'<div id="wrpMessage">','#suffix'=>'</div>'
        ];
        
        if(!\Drupal::service('publisso_gold.tools')->userHasAccessRight('wflockinfowrite'))
                return \Drupal::service('publisso_gold.tools')->accessDenied();
        
        $form['info'] = [
                '#type' => 'inline_template',
                '#template' => '
                        <hr>
                        '.((string)t('Do you really want to unlock this item? Please make sure that nobody works anymore.')).'
                        <hr>
                '
        ];
        
        $form['save'] = array(
            '#type' => 'submit',
            '#value' => t('Unlock item'),
            '#button_type' => 'success',
            '#ajax' => [
                'wrapper' => 'wrpMessage',
                'callback' => '::unlockItem'
            ]
        );
        
        $form['#cache']['max-age'] = 0;
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        return $form;
    }

    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return string[]
         */
        public function unlockItem(array $form, FormStateInterface $form_state){
        
        $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
        $workflow->unlock(true);
        
        if($workflow->locked)
                return ['#markup' => '<div class="rwAlert">'.((string)t('Item @id can not set to unlocked!', ['@id' => $workflow->getElement('id')])).'</div>','#prefix'=>'<div id="wrpMessage">','#suffix'=>'</div>'];
        else
                return ['#markup' => '<div class="rwSuccess">'.((string)t('Item @id successfully unlocked!', ['@id' => $workflow->getElement('id')])).'</div>','#prefix'=>'<div id="wrpMessage">','#suffix'=>'</div>'];
    }
}
