<?php
/**
* @file
* Contains \Drupal\publisso_gold\Form\displayReviewsheetSkel.
*/
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
* Provides a simple example form.
*/
class displayReviewsheetSkel extends FormBase {

        /**
        * {@inheritdoc}
        */
        public function getFormId() {
                return 'displayReviewsheetSkel';
        }

        /**
        * {@inheritdoc}
        */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($args['wf_id']);
                $form['data'] = $workflow->getReviewsheetForm(['state' => ['rejected', 'finished']]);
                $form['#cache']['max-age'] = 0;
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function validateForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }

        /**
        * {@inheritdoc}
        */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
}
