<?php
        
        
        namespace Drupal\publisso_gold\Form\AuthorsContracts;
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Classes\AuthorsContract;

        /**
         * Class DeleteAuthorsContract
         * @package Drupal\publisso_gold\Form\AuthorsContracts
         */
        class DeleteAuthorsContract extends FormBase {
        
                /**
                 * @return string
                 */
                public function getFormId () {
                        return 'DeleteAuthorsContract';
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @param null $name
                 * @return array
                 */
                public function buildForm (array $form, FormStateInterface $form_state, $name = null) {
                        
                        if(!$form_state->has('name')) $form_state->set('name', $name);
                        
                        if(!$form_state->get('name')){
                                \Drupal::service('messenger')->addError((string)$this->t('Error: Entity not found!'));
                                return $form;
                        }
                        
                        $form['confirm'] = [
                                '#markup' => (string)$this->t('Do you really want to delete the authors-contract "@name"?', ['@name' => $form_state->get('name')]),
                                '#prefix' => '<div class="alert alert-danger">',
                                '#suffix' => '</div>'
                        ];
                        
                        $form['back'] = [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('No, I would rather go back'),
                                '#submit' => ['::back'],
                                '#button_type' => 'success',
                                '#suffix' => '&nbsp;&nbsp;&nbsp;'
                        ];
                        
                        $form['submit'] = [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Yes, delete!'),
                                '#submit' => ['::submitForm', '::back']
                        ];
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array|void
                 */
                public function validateForm (array &$form, FormStateInterface $form_state ) {
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function back (array &$form, FormStateInterface $form_state ) {
                        $form_state->setRedirect('publisso_gold.authorscontracts');
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function submitForm (array &$form, FormStateInterface $form_state ) {
                        $ac = AuthorsContract::load($form_state->get('name')); $ac->delete();
                }
        }
