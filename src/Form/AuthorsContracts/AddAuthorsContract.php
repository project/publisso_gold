<?php
        
        
        namespace Drupal\publisso_gold\Form\AuthorsContracts;
        
        
        use Drupal\Core\Ajax\AjaxResponse;
        use Drupal\Core\Ajax\HtmlCommand;
        use Drupal\Core\Ajax\InvokeCommand;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\file\Entity\File;
        use Drupal\publisso_gold\Classes\AuthorsContract;
        use Drupal\publisso_gold\Controller\Blob;

        /**
         * Class AddAuthorsContract
         * @package Drupal\publisso_gold\Form\AuthorsContracts
         */
        class AddAuthorsContract extends FormBase {
        
                /**
                 * @return string
                 */
                public function getFormId () {
                        return 'AddAuthorsContract';
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function buildForm (array $form, FormStateInterface $form_state ) {
                        
                        $form['container_name'] = [
                                'name'=> [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Name'),
                                        '#required' => true,
                                        '#ajax' => [
                                                'event'         => 'change',
                                                'prevent'       => 'blur',
                                                'progress'      => [
                                                        'type'          => 'none'
                                                ],
                                                'disable-refocus'       => true,
                                                'callback' => '::validateName',
                                                'wrapper' => 'name-validation-message',
                                        ]
                                ],
                                'message' => [
                                        '#markup' => (string)$this->t('The availability of the name is checked when leaving the field...'),
                                        '#prefix' => '<div id="name-validation-message" class="alert">',
                                        '#suffix' => '</div>'
                                ]
                        ];
                        
                        
        
                        $form['description'] = [
                                '#type' => 'textfield',
                                '#title' => (string)$this->t('Description'),
                                '#maxlength' => 300
                        ];
                        
                        $form['file'] = [
                                '#type' => 'managed_file',
                                '#title' => (string)$this->t('Authors contract (*.pdf-file)'),
                                '#required' => true,
                                '#upload_validators'    => [
                                        'file_validate_extensions' => ['pdf'],
                                        // Pass the maximum file size in bytes
                                        'file_validate_size' => array(30 * 1024 * 1024),
                                ],
                                '#progress_indicator' => 'bar',
                                '#progress_message' => (string)$this->t('Uploading...')
                        ];
                        
                        $form['submit'] = [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Submit')
                        ];
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return AjaxResponse
                 */
                public function validateName(array &$form, FormStateInterface $form_state){
        
                        $response = new AjaxResponse();
                        
                        if($form_state->getValue('name')) {
                                if ( AuthorsContract::load( $form_state->getValue( 'name' ) )->exists() ) {
                                        $string = (string)$this->t( 'This name already exists an will cause an error on submit!' );
                                        $response->addCommand( new InvokeCommand( '#name-validation-message', 'removeClass', [ 'alert alert-success' ] ) );
                                        $response->addCommand( new InvokeCommand( '#name-validation-message', 'addClass', [ 'alert alert-danger' ] ) );
                                }
                                else {
                                        $string = (string)$this->t( 'This name is available!' );
                                        $response->addCommand( new InvokeCommand( '#name-validation-message', 'removeClass', [ 'alert alert-danger' ] ) );
                                        $response->addCommand( new InvokeCommand( '#name-validation-message', 'addClass', [ 'alert alert-success' ] ) );
                                }
                        }
                        else{
                                $string = (string)$this->t( 'The availability of the name is checked when leaving the field...' );
                                $response->addCommand( new InvokeCommand( '#name-validation-message', 'removeClass', [ 'alert-success alert-danger' ] ) );
                        }
                        
                        $response->addCommand(new HtmlCommand('#name-validation-message', $string));
                        return $response;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array|void
                 */
                public function validateForm (array &$form, FormStateInterface $form_state ) {
                        
                        if(AuthorsContract::load( $form_state->getValue( 'name' ) )->exists()){
                                $form_state->setErrorByName('name', (string)$this->t('This name already exists!'));
                        }
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 * @throws \Exception
                 */
                public function submitForm (array &$form, FormStateInterface $form_state ) {
                        
                        $name           = $form_state->getValue('name');
                        $description    = $form_state->getValue('description');
        
                        $fileID = $form_state->getValue('file')[0];
                        $file   = File::load($fileID);
        
                        $content  = file_get_contents(\Drupal::service('file_system')->realpath($file->toArray()['uri'][0]['value']));
                        $filemime = $file->toArray()['filemime'][0]['value'];
                        $filename = $file->toArray()['filename'][0]['value'];
        
                        $blob = new Blob();
                        $blob->create($content, $filemime, ['name' => $filename, 'description' => $description]);
                        
                        if(false === ($ac = AuthorsContract::create($name, $blob->getId(), $description))){
                                $blob->delete();
                                \Drupal::service('messenger')->addError((string)$this->t('Error while saving data! See error-logs for details!'));
                                return $form;
                        }
                        
                        $form_state->setRedirect('publisso_gold.authorscontracts');
                }
        }
