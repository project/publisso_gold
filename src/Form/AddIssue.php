<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\AddIssue.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class AddIssue extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $num_corporations;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return AddIssue|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'AddIssue';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
        
        $form_state->set('jrn_id', $args['jrn_id']);
        $journal = new \Drupal\publisso_gold\Controller\Journal($form_state->get('jrn_id'));
        
        if(empty($journal->getElement('id'))){
                return [
                        '#markup' => (string)t('Can\'t load journal #@jrn_id!', ['@jrn_id' => $form_state->get('jrn_id')]),
                        '#prefix' => '<div class="rwError">',
                        '#suffix' => '</div>'
                ];
        }
        
        $form_state->set('simpleValues', [
                'year'  => 'year', 
                'title' => 'title'
        ]);
        
        if($this->modname && !$form_state->get('modname')){
            $form_state->set('modname', $this->modname);
        }
        
        if(!$this->modname && $form_state->get('modname')){
            $this->modname = $form_state->get('modname');
        }
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
        $form['#tree'] = true;
        
        $form['site_headline'] = [
            '#type'     => 'markup',
            '#markup'   => '<h1>'.t('Add Issue for "@title"', ['@title' => $journal->getElement('title')]).'</h1>'
        ];
        
        $form['volume'] = [
                '#type'          => 'select',
                '#required'      => true,
                '#title'         => (string)t('Volume'),
                '#options'       => \Drupal::service('publisso_gold.tools')->getJournalVolumes($form_state->get('jrn_id'))
        ];
        
        $form['number'] = [
                '#type'         => 'number',
                '#min'          => 1,
                '#required'     => true,
                '#title'        => (string)t('Number')
        ];
        
        $form['title'] = [
                '#type'  => 'textfield',
                '#title' => (string)t('Title')
        ];
        
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => t('Create issue'),
            '#prefix' => '<br>',
            '#suffix' => '<br><br><br>',
            '#disabled' => false
        ];
        
        $form['#attributes'] = array(
            'enctype' => 'multipart/form-data'
        );
        
		$form['#attached'] = [
			'library' => [
				'publisso_gold/default'
			]
		];
		
        $form['#cache']['max-age'] = 0;
        
        $form_state->setCached(false);
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        if(\Drupal::service('publisso_gold.tools')->issueExists($form_state->get('jrn_id'), $form_state->getValue('volume'), $form_state->getValue('number')))
                $form_state->setErrorByName('number', (string)t('This issue already exists!'));
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $issue = new \Drupal\publisso_gold\Controller\Issue();
        $issue->create($form_state->get('jrn_id'), $form_state->getValue('volume'));
        
        if($form_state->getValue('number')) $issue->setElement('number', $form_state->getValue('number'));
        if($form_state->getValue('title'))  $issue->setElement('title' , $form_state->getValue('title'));
        
        drupal_set_message((string)t('Issue created'));
        $form_state->setRedirect('publisso_gold.journalmanagement');
        return $form;
    }
}
