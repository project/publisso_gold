<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldTableOfContentButtonAdd.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldTableOfContentButtonAdd extends FormBase {
    
    private $modname = 'publisso_gold';
    private $modpath;
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldtableofcontentbuttonadd';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        if($this->modname && !$form_state->get('modname')){
            $form_state->set('modname', $this->modname);
        }
        
        if(!$this->modname && $form_state->get('modname')){
            $this->modname = $form_state->get('modname');
        }
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        $form['new_toc']['submit'] = [
            '#type'     => 'submit',
            '#value'    => (string)t('Create table of content')
        ];
        
        $form['#cache']['max-age'] = 0;
        $form_state->setCached(false);
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $form_state->setRedirect('publisso_gold.table_of_content.add');
        //return $form;
    }
}
