<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\agreePrivacyProtection.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;
use Drupal\publisso_gold\Controller\PrivacyProtection;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a simple example form.
 */
class agreePrivacyProtection extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(){
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'agreePrivacyProtection';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
        
        $session = \Drupal::service('session');
        $formFields = \Drupal::service('publisso_gold.form.fields');
        $texts = \Drupal::service('publisso_gold.texts');
        
        $form['agree'] = $formFields->getField('agree_pp_information.agree' , $_args);
        
        $form['submit'] = [
                '#type'   => 'submit',
                '#value'  => (string)$this->t($texts->get('global.save', 'fc')),
                '#prefix' => '<hr>',
                '#suffix' => '&nbsp;&nbsp;&nbsp;'
        ];
        
        $form['delete'] = [
                '#type'       => 'link',
                '#title'      => (string)$this->t($texts->get('agree_pp_information.btn.delete_account', 'fc')),
                '#suffix'     => '<br><br>',
                '#url'        => Url::fromRoute('publisso_gold.userprofile.delete'),
                '#attributes' => [
                        'class' => [
                                'btn', 'btn-danger', 'use-ajax'
                        ],
                        'ajax-dialog-type' => 'modal'
                ]
        ];
        
        $form['#cache']['max-age'] = 0;
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $texts   = \Drupal::service('publisso_gold.texts');
        $session = \Drupal::service('session');
        
        $user = new \Drupal\publisso_gold\Controller\User($session->get('user')['id']);
        $user->profile->setElement('agree_privacy_protection', date('Y-m-d H:i:s'));
        
        $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
        $url = $tempstore->get('back_url');
        
        if($url){
                $tempstore->delete('back_url');
                $form_state->setRedirect($url['url'], $url['params']);
        }
        
        $form_state->setRedirect('publisso_gold.dashboard');
        return $form;
    }
}
