<?php
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\PublissoGold;

        /**
         * Class RemapReferences
         * @package Drupal\publisso_gold\Form
         */
        class RemapReferences extends FormBase {
        
                /**
                 * @inheritDoc
                 */
                public function getFormId() {
                        return 'RemapReferences';
                }
        
                /**
                 * @inheritDoc
                 */
                public function buildForm(array $form, FormStateInterface $form_state) {
                        
                        $form['type'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('Type'),
                                '#options' => [
                                        'manual' => (string)$this->t('Manual')
                                ]
                        ];
                        
                        $form['text'] = [
                                '#type' => 'textarea',
                                '#title' => (string)$this->t('Text'),
                                '#states' => [
                                        'visible' => [
                                                ':input[name="type"]' => ['value' => 'manual']
                                        ],
                                        'required' => [
                                                ':input[name="type"]' => ['value' => 'manual']
                                        ]
                                ]
                        ];
        
                        $form['references'] = [
                                '#type' => 'textarea',
                                '#title' => (string)$this->t('References'),
                                '#states' => [
                                        'visible' => [
                                                ':input[name="type"]' => ['value' => 'manual']
                                        ],
                                        'required' => [
                                                ':input[name="type"]' => ['value' => 'manual']
                                        ]
                                ]
                        ];
                        
                        $form['wrapper_result'] = [
                                '#type' => 'container',
                                '#prefix' => '<div id="wrapper_result">',
                                '#suffix' => '<br></div>'
                        ];
                        
                        if($form_state->hasValue('text') && $form_state->hasValue('references')){
        
                                $text = Publisso::tools()->mapReferences(
                                        $form_state->getValue('text'),
                                        $form_state->getValue('references')
                                );
                                
                                Publisso::log($text);
                                
                                $references = Publisso::tools()->getReferences(
                                        $text,
                                        $form_state->getValue('references')
                                );
                                
                                $form['wrapper_result']['result'] = [
                                        '#tree' => true,
                                        '#type' => 'fieldset',
                                        '#title' => (string)$this->t('Result'),
                                        'text' => [
                                                '#type' => 'textarea',
                                                '#title' => (string)$this->t('Text'),
                                                '#value' => $text
                                        ],
                                        'references' => [
                                                '#type' => 'textarea',
                                                '#title' => (string)$this->t('References'),
                                                '#value' => $references
                                        ]
                                ];
                        }
                        
                        $form['actions'] = [
                                '#type' => 'actions',
                                'submit' => [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Submit'),
                                        '#ajax' => [
                                                'wrapper' => 'wrapper_result',
                                                'callback' => '::submitFormCallback'
                                        ]
                                ]
                        ];
                        
                        return $form;
                }
        
                /**
                 * @inheritDoc
                 */
                public function submitForm(array &$form, FormStateInterface $form_state) {
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function submitFormCallback(array &$form, FormStateInterface $form_state){
                        return $form['wrapper_result'];
                }
        }