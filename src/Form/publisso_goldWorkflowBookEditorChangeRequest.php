<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowBookEditorChangeRequest.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Url;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldWorkflowBookEditorChangeRequest extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldWorkflowBookEditorChangeRequest|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldworkflowbookeditorchangerequest';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $this->modpath = drupal_get_path('module', $this->modname);
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
        //expect build-info-args as array, index 0 is bk_id
        $build_info = $form_state->getBuildInfo();
        $args = $build_info['args'];
        
        if(array_key_exists(0, $args) && array_key_exists('bk_id', $args[0]))
          $form_state->set('bk_id', $args[0]['bk_id']);
        
        if(array_key_exists(0, $args) && array_key_exists('wf_id', $args[0])){
          $form_state->set('wf_id', $args[0]['wf_id']);
          
          $comments = \Drupal::database()->select('rwPubgoldWorkflowCommentsForAuthors', 'cfa')
              ->fields('cfa', [])
              ->condition('cfa_wfid', $form_state->get('wf_id'), '=')
              ->condition('cfa_for_uid', $_SESSION['user']['id'], '=')
              ->execute()
              ->fetchAll();
          
          $renderComments = [];
          $c = 0;
          foreach($comments as $_){
            
            if($c === 0){
              $renderComments = [
                '#type' => 'details',
                '#open' => false,
                '#title' => (string)t('Comment(s) for author'),
                '#description' => (string)t('These comments are only available for you during current workflow. Nobody else can see these comments!'),
                'content' => []
              ];
            }
            
            $_->cfa_comment = base64_decode($_->cfa_comment);
            
            $renderComments['content'][] = [
              '#type' => 'details',
              '#open' => false,
              '#title' => (string)t('Comment').' #' . ($c + 1),
              'content' => [
                  '#type' => 'markup',
                  '#markup' => $_->cfa_comment
              ]
            ];
            
            $c++;
          }
        }
        
        if(!$form_state->get('bk_id')){
            drupal_set_message(t('No book given!'), 'error');
            return array();
        }
        
        if(!$form_state->get('step'))
            $form_state->set('step', 1);
        
        $book = $this->database->select('rwPubgoldBooks', 'bk')->fields('bk', [])->condition('bk_id', $form_state->get('bk_id'), '=')->execute()->fetchAssoc();
        $objBook = new \Drupal\publisso_gold\Controller\Book($form_state->get('book'));
		
        if($form_state->get('wf_id') && !$form_state->get('form_data')){
            
            $workflow = getDashboard(\Drupal::Database(), $form_state->get('wf_id'));
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
			
			$new_workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
            
            $chapter_text = base64_decode($workflow_data->chapter_text);
            $abstract = base64_decode($workflow_data->abstract);
            
            $delta = ['workflow', 'workflow_data'];
            foreach($delta as $_) $form_state->set($_, $$_);
			
			$ary = [];
				
			foreach($new_workflow->getDataKeys() as $key){
				$ary[$key] = $new_workflow->getDataElement($key);
			}
			
			//special Chapter_text and abstract
			$ary['chapter_text'] = base64_decode($ary['chapter_text']);
			$ary['abstract'] 	 = base64_decode($ary['abstract']    );
			
			$ary['authors'] = json_decode($ary['authors'], true);
			$ary['keywords'] = implode('; ', array_filter(json_decode($ary['keywords'])));
			
			$form_state->set('authors', $ary['authors']);
			
			$cnt_authors = 0;
			
			foreach($ary['authors'] as $_){
				if($_['uid']){
					$cnt_authors++;
				}
			}
			
			$form_state->set('count_authors', $cnt_authors);
			
			$form_state->set('num_authors', count($ary['authors']) - $cnt_authors);
			$form_state->set('form_data', $ary);
			
			$corporations = [];
			foreach($ary['corporations'] as $k => $v){
				
				if(preg_match('/^\d+$/', $k))
						$corporations[$k] = $v;
			}
			$form_state->set('corporations', $corporations);
			$form_state->set('count_corporations', count($corporations));
        }
		
        $form['comments_for_author'] = $renderComments;
        
        $form ['site_headline'] = [
            '#type' => 'markup',
            '#markup' => '<h1>'.t('Editor change request').'</h1>'
        ];
            
        $form['book_title'] = [
            '#type' => 'markup',
            '#markup' => '<h2>'.$book['bk_title'].'</h2>'
        ];
        
// title-field            
        $form['title'] = [
            '#type' => 'textfield',
            '#title' => t('Title'),
            '#required' => true,
            '#access' => $form_state->get('step') == 1 ? true : false
        ];
        
        if(isset($workflow_data->title))
            $form['title']['#default_value'] = $workflow_data->title;
        
        if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            $form['title']['#default_value'] = $fd['title'];
        }
        
// abstract        
        $form['abstract'] = [
            '#type' => 'textarea',
            '#title' => t('Abstract'),
            '#required' => true,
            '#access' => $form_state->get('step') == 1 ? true : false,
			'#default_value' => base64_decode($abstract),
			'#attributes' => [
				'class' => [
					\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
				]
			]
        ];
        
        
        if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            $form['abstract']['#default_value'] = $fd['abstract'];
        }
        
		if($objBook->getControlElement('abstract_required') == 0){
			$form['abstract']['#access'] 	= false;
			$form['abstract']['#required'] 	= false;
			
			$form['desc_abstract']['#access'] 	= false;
			$form['desc_abstract']['#required'] = false;
		}
		
// keywords
        
        $form['keywords'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Keywords'),
            '#prefix' => '<br><br>',
            '#access' => $form_state->get('step') == 1 ? true : false
        ];
        
        if(isset($workflow_data->keywords))
            $form['keywords']['#default_value'] = implode('; ', array_filter(json_decode($workflow_data->keywords)));
        
        if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            $form['keywords']['#default_value'] = $fd['keywords'];
        }
// chapter text        
        $form['chapter_text'] = [
            '#type' => 'textarea',
            '#title' => t('Chapter Text'),
            '#required' => true,
            '#rows' => 50,
            '#prefix' => '<br>',
            '#access' => $form_state->get('step') == 1 ? true : false,
			'#attributes' => [
				'class' => [
					\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
				]
			]
        ];
        
        if(($chapter_text)){
          //$chapter_text = (array)$chapter_text;
          //$form['chapter_text']['#format'] = $chapter_text->format;
          $form['chapter_text']['#default_value'] = $chapter_text;
        }
        else{
          //$form['chapter_text']['#format'] = 'full_html';
        }
        
        if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            /*
			if(!is_array($fd['chapter_text']))
                $chapter_text = json_decode(base64_decode($fd['chapter_text']), true);
            else
                $chapter_text = $fd['chapter_text'];
            
            $form['chapter_text']['#format']        = $chapter_text['format'];
			*/
            $form['chapter_text']['#default_value'] = $fd['chapter_text'];
        }
      
//fieldset authors        
        $form['fs_authors'] = [
            '#type' => 'fieldset',
            '#title' => (string)t('Author(s)'),
            '#prefix' => '<br>',
            'content' => [],
            '#access' => $form_state->get('step') == 1 ? true : false
        ];
        
		$form['fs_authors']['description'] = [
			'#type' => 'markup',
			'#markup' => t('Please choose the authors from the user database. If there are authors who are not yet in the database, please add them with name and affiliation below. The Authors order can be determined on the next page.'),
			'#weight' => -1,
			'#suffix' => '<br><br>'
		];
		
		$form['fs_authors']['content']['authors'] = [
			'#tree' => true,
			'#prefix' => '<div id="authors_replace">',
			'#suffix' => '</div>',
			'#description' => (string)t('Please enter Firstname and Lastname, separated with Whitespace (i.e. John Smith)'),
            '#access' => $form_state->get('step') == 1 ? true : false
		];
		
		$form['fs_authors']['content']['description'] = [
			'#type' => 'markup',
			'#markup' => '<p>'.(string)t('Please enter Firstname and Lastname, separated with Whitespace (i.e. John Smith)').'</p>',
            '#access' => $form_state->get('step') == 1 ? true : false
		];
		
		if(!$form_state->get('count_authors'))
			$form_state->set('count_authors', 3);
		
		if(isset($new_workflow) && !$form_state->get('authors')){
			
			$cnt_authors = 0;
			
			foreach($new_workflow->getDataElement('authors') as $author){
				
				if(!empty($author->uid)) $cnt_authors++;
			}
			
			$form_state->set('authors', $new_workflow->getDataElement('authors'));
			$form_state->set('count_authors', $cnt_authors);
			$form_state->set('num_authors', count(json_decode($new_workflow->getDataElement('authors'), true)) - $cnt_authors);
		}
		
		for($i = 0; $i < $form_state->get('count_authors'); $i++){
			
			$element = [
				'#type' => 'textfield',
				'#autocomplete_route_name' => 'autocomplete.user',
				'#autocomplete_route_parameters' => array(),
                '#access' => $form_state->get('step') == 1 ? true : false
			];
			
			if($form_state->get('authors')){
				
				$fd = $form_state->get('authors');
				$fd[$i] = (array)$fd[$i];
				if((array)$fd[$i]['to_delete'] === true)
					continue;
				
				if(array_key_exists($i, $fd) && array_key_exists('uid', $fd[$i]) && !empty($fd[$i]['uid'])){
					$element['#value'] = strtoupper(base_convert($fd[$i]['uid'], 10, 36)).' | '.$fd[$i]['firstname'].' '.$fd[$i]['lastname'];
					$element['#disabled'] = true;
				}
			}
			
			if($i == 0)
				$element['#title'] = (string)t('Author(s)');
			
			$form['fs_authors']['content']['authors'][$i] = $element;
		}
		
		$form['fs_authors']['content']['add_author'] = [
			'#type' => 'submit',
			'#value' => (string)t('Add Author'),
			'#submit' => ['::add_author'],
			'#suffix' => '<br><br>',
			'#prefix' => '',
			'#ajax' => array(
				'callback' => '::add_author_callback',
				'wrapper' => 'authors_replace',
				'effect' => 'fade'
			),
            '#access' => $form_state->get('step') == 1 ? true : false,
			'#limit_validation_errors' => []
		];
                    
        $form['fs_authors']['content']['more_authors'] = [
            '#tree' => TRUE,
            '#prefix' => '<div id="authors-replace">',
            '#suffix' => '</div>',
            '#access' => $form_state->get('step') == 1 ? true : false,
            '#limit_validation_errors' => []
        ];
        
//more authors
        
        if(!$form_state->get('num_authors'))
           $form_state->set('num_authors', 1);
        /*
        if(isset($workflow_data->more_authors) &&
           count($workflow_data->more_authors)){
            
            $form_state->set('num_authors', count($workflow_data->more_authors));
            $_more_authors = json_decode($workflow_data->more_authors, true);
        }
        */
        $max = $form_state->get('num_authors');
        
        for ($delta = 0; $delta < $max; $delta++) {
            
            if (!isset($form['fs_authors']['content']['more_authors'][$delta])) {
                
                $element = array(
                    '#type' => 'fieldset',
                    '#title' => (string)t('Author').' #'.(string)($delta + 1),
                    'content' => [
                        'more_author_name' => [
                            '#type' => 'textfield',
                            '#title' => (string)t('Name'),
                            '#default_value' => isset($_more_authors) ? $_more_authors[$delta]['more_author_name'] : '',
                            '#access' => $form_state->get('step') == 1 ? true : false
                        ],
                        
                        'more_author_lastname' => [
                            '#type' => 'textfield',
                            '#title' => (string)t('Lastname'),
                            '#default_value' => isset($_more_authors) ? $_more_authors[$delta]['more_author_lastname'] : '',
                            '#access' => $form_state->get('step') == 1 ? true : false
                        ],
                        
                        'more_author_affiliation' => [
                            '#type' => 'textfield',
                            '#title' => (string)t('Affiliation'),
                            '#default_value' => isset($_more_authors) ? $_more_authors[$delta]['more_author_affiliation'] : '',
                            '#access' => $form_state->get('step') == 1 ? true : false
                        ]
                    ]
                );
                
				if($form_state->has('authors')){
					
					$authors = $form_state->get('authors');
					$element['content']['more_author_name'	  	 ]['#default_value'] = $authors[$delta + $form_state->get('count_authors')]->firstname;
					$element['content']['more_author_lastname'	 ]['#default_value'] = $authors[$delta + $form_state->get('count_authors')]->lastname ;
					$element['content']['more_author_affiliation']['#default_value'] = $authors[$delta + $form_state->get('count_authors')]->affiliation;
				}
				
                $form['fs_authors']['content']['more_authors'][$delta] = $element;
            }
        }
        
        //if(!$form_state->get('wf_id')){
            $form['fs_authors']['content']['addAuthor'] = [
                '#type' => 'submit',
                '#name' => 'addAuthor',
                '#value' => t('Add Author'),
                '#submit' => array('::addAuthor'),
                '#suffix' => '<br><br>',
                '#prefix' => '<br><br>',
              
                '#ajax' => array(
                    'callback' => '::addAuthorCallback',
                    'wrapper' => 'authors-replace',
                    'effect' => 'fade'
                ),
                
                '#access' => $form_state->get('step') == 1 ? true : false,
                '#limit_validation_errors' => []
              
          ];
        //}
// corporation      

		$form['fs_authors']['content']['corporations_description'] = [
			'type' => 'markup',
			'#markup' => t('Here you can add corporate authors (working groups etc.). The corporate authors will appear after the personal authors in the order entered.'),
			'#suffix' => '<br><br>',
            '#access' => $form_state->get('step') == 1 ? true : false
		];

		$form['fs_authors']['content']['corporations'] = [
			'#tree' => true,
			'#prefix' => '<div id="corporations_replace">',
			'#suffix' => '</div>',
            '#access' => $form_state->get('step') == 1 ? true : false
		];
		
		if(!$form_state->get('count_corporations'))
			$form_state->set('count_corporations', 1);
		
		if(isset($new_workflow) && !$form_state->get('corporations')){
			
			$form_state->set('corporations', json_decode($new_workflow->getDataElement('corporations'), true));
			$form_state->set('count_corporations', count($form_state->get('corporations')));
		}
		
		for($i = 0; $i < $form_state->get('count_corporations'); $i++){
			
			$element = [
				'#type' => 'textfield',
                '#access' => $form_state->get('step') == 1 ? true : false
			];
			
			if($form_state->get('corporations')){
				
				$fd = $form_state->get('corporations');
				
				if($fd[$i]['to_delete'] === true)
					continue;
				
				$element['#default_value'] = $fd[$i];
			}
			
			if($i == 0)
				$element['#title'] = (string)t('Corporation(s)');
			
			$form['fs_authors']['content']['corporations'][$i] = $element;
		}
		
		$form['fs_authors']['content']['corporations']['content']['add_corporation'] = [
			'#type' => 'submit',
			'#value' => (string)t('Add Corporation'),
			'#submit' => ['::add_corporation'],
			'#suffix' => '<br><br>',
			'#prefix' => '<br>',
			'#ajax' => array(
				'callback' => '::add_corporation_callback',
				'wrapper' => 'corporations_replace',
				'effect' => 'fade'
			),
            '#access' => $form_state->get('step') == 1 ? true : false,
            '#limit_validation_errors' => []
		];

// doi
        $form['doi'] = [
            '#type' => 'textfield',
            '#title' => (string)t('DOI'),
            '#access' => $form_state->get('step') == 1 ? true : false
        ];
        
        if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            $form['doi']['#default_value'] = $fd['doi'];
        }

// version
        $form['version'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Version'),
            '#access' => $form_state->get('step') == 1 ? true : false
        ];
        
        if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            $form['version']['#default_value'] = $fd['version'];
        }
        
// conflict of interest
            
            $form['conflict_of_interest'] = [
                '#type' => 'checkbox',
                '#title' => (string)t('Conflict of interest'),
                '#default_value' => isset($workflow_data) ? $workflow_data->conflict_of_interest : '',
            '#access' => $form_state->get('step') == 1 ? true : false
            ];
            
            if($form_state->get('form_data')){
                $fd = $form_state->get('form_data');
                $form['conflict_of_interest']['#default_value'] = $fd['conflict_of_interest'];
            }

// conflict of interest - text
            
            $form['conflict_of_interest_text'] = [
                '#type' => 'textfield',
                '#title' => (string)t('Description of conflict of interest'),
                '#default_value' => isset($workflow_data) ? $workflow_data->conflict_of_interest_text : '',
                '#states' => array(
                    'visible' => array(   // action to take.
                        ':input[name="conflict_of_interest"]' => array('checked' => true),
                    ),
                    'invisible' => array(   // action to take.
                        ':input[name="conflict_of_interest"]' => array('checked' => false),
                    )
                ),
                '#access' => $form_state->get('step') == 1 ? true : false
            ];
            
            if($form_state->get('form_data')){
                $fd = $form_state->get('form_data');
                $form['conflict_of_interest_text']['#default_value'] = $fd['conflict_of_interest_text'];
            }

        
// funding
            
            $form['funding'] = [
                '#type' => 'checkbox',
                '#title' => t('Funds received'),
                '#default_value' => isset($workflow_data) ? $workflow_data->funding : '',
            '#access' => $form_state->get('step') == 1 ? true : false
            ];
            
            if($form_state->get('form_data')){
                $fd = $form_state->get('form_data');
                $form['funding']['#default_value'] = $fd['funding'];
            }
            
// funding name		

            $form['funding_name'] = [
                '#type' => 'textfield',
                '#title' => (string)t('Funding name'),
                '#default_value' => isset($workflow_data) ? $workflow_data->funding_name : '',
                '#states' => array(
                    'visible' => array(   // action to take.
                        ':input[name="funding"]' => array('checked' => true),
                    ),
                    'invisible' => array(   // action to take.
                        ':input[name="funding"]' => array('checked' => false),
                    )
                ),
                '#access' => $form_state->get('step') == 1 ? true : false
            ];
            
            if($form_state->get('form_data')){
                $fd = $form_state->get('form_data');
                $form['funding_name']['#default_value'] = $fd['funding_name'];
            }
            
// funding id
            
            $form['funding_id'] = [
                '#type' => 'textfield',
                '#title' => (string)t('Funding ID'),
                '#default_value' => isset($workflow_data) ? $workflow_data->funding_id : '',
                '#states' => array(
                    'visible' => array(   // action to take.
                        ':input[name="funding"]' => array('checked' => true),
                    ),
                    'invisible' => array(   // action to take.
                        ':input[name="funding"]' => array('checked' => false),
                    )
                ),
                '#access' => $form_state->get('step') == 1 ? true : false
            ];
            
            if($form_state->get('form_data')){
                $fd = $form_state->get('form_data');
                $form['funding_id']['#default_value'] = $fd['funding_id'];
            }
  
// license

        $form['license'] = [
            '#type' => 'textfield',
            '#title' => t('License'),
            '#required' => true,
            '#default_value' => isset($workflow_data->license) ? $workflow_data->license : '',
            '#access' => $form_state->get('step') == 1 ? true : false
        ];
        
        if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            $form['license']['#default_value'] = $fd['license'];
        }

// publication place
        
            $form['publication_place'] = [
                '#type' => 'textfield',
                '#title' => t('Publication place'),
                '#required' => true,
                '#default_value' => $book['bk_publication_place'] /*isset($workflow_data->publication_year) ? $workflow_data->publication_year : ''*/,
            '#access' => $form_state->get('step') == 1 ? true : false
            ];
            
        if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            if(!empty($fd['publication_place']))$form['publication_place']['#default_value'] = $fd['publication_place'];
        }

// publication year
            
            $form['publication_year'] = [
                '#type' => 'textfield',
                '#title' => t('Publication year'),
                '#required' => true,
                '#default_value' => $book['bk_begin_publication'] /*isset($workflow_data->publication_year) ? $workflow_data->publication_year : ''*/,
            '#access' => $form_state->get('step') == 1 ? true : false
            ];
            
            if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            if(!empty($fd['publication_year']))$form['publication_year']['#default_value'] = $fd['publication_year'];
        }

// sub category
            
            $form['sub_category'] = [
                '#type' => 'textfield',
                '#title' => t('Subcategory'),
                '#required' => true,
                '#default_value' => $book['bk_sub_category'] /*isset($workflow_data->sub_category) ? $workflow_data->sub_category : ''*/,
            '#access' => $form_state->get('step') == 1 ? true : false
            ];
            
            if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            $form['sub_category']['#default_value'] = $fd['sub_category'];
        }

        // accept policy        
        $form['accept_policy'] = [
            '#type' => 'checkbox',
            '#title' => (string)t('I accept the policy and the author\'s contract'),
            '#required' => true,
            '#access' => $form_state->get('step') == 1 ? true : false,
            '#disabled' => true
        ];
        
        if(isset($workflow_data->accept_policy)){
          $form['accept_policy']['#default_value'] = $workflow_data->accept_policy;
        }
        
        if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            $form['accept_policy']['#default_value'] = $fd['accept_policy'];
        }
        
// comment
            
            $form['comment'] = [
                '#type' => 'textarea',
                '#title' => (string)t('Comment'),
				'#access' => $form_state->get('step') == 1 ? true : false,
				'#suffix' => '<br>'
            ];
            
            if($form_state->get('form_data')){
            $fd = $form_state->get('form_data');
            
            //$form['comment']['#format']        = $fd['comment']['format'];
            $form['comment']['#default_value'] = $fd['comment'];
        }
        
        if($form_state->get('step') == 2){
            
            if(is_array($form_state->get('authors'))){
                
                $form['authors'] = [
                    '#type' => 'table',
                    '#caption' => (string)t('Please weight and mark corresponding!'),
                    '#header' => [
                        (string)t('Firstname'),
                        (string)t('Lastname'),
                        (string)t('Affiliation'),
                        (string)t('Weight'),
                        (string)t('Is corresponding author?'),
						(string)t('Delete item'),
                        ''
                    ]
                ];
                
                $i = 0;
                
                foreach($form_state->get('authors') as $_){
                    
                    $form['authors'][$i]['mu_firstname'] = [
                        '#type' => 'markup',
                        '#markup' => $_['firstname']
                    ];
                    
                    $form['authors'][$i]['mu_lastname'] = [
                        '#type' => 'markup',
                        '#markup' => $_['lastname']
                    ];
                    
                    $form['authors'][$i]['mu_affiliation'] = [
                        '#type' => 'markup',
                        '#markup' => $_['affiliation']
                    ];
                    
                    $form['authors'][$i]['weight'] = [
                        '#type' => 'number',
                        '#default_value' => $_['weight'],
                        '#min' => 1,
                        '#max' => 20,
                        '#step' => 1
                    ];
                    
                    $form['authors'][$i]['is_corresponding'] = [
                        '#type' => 'checkbox',
                        '#disabled' => $_['uid'] ? false: true,
                        '#default_value' => $_['is_corresponding']
                    ];
                    
					$form['authors'][$i]['to_delete'] = [
                        '#type' => 'checkbox'
                    ];
                    
                    $form['authors'][$i]['firstname'] = [
                        '#type' => 'hidden',
                        '#value' => $_['firstname']
                    ];
                    
                    $form['authors'][$i]['lastname'] = [
                        '#type' => 'hidden',
                        '#value' => $_['lastname']
                    ];
                    
                    $form['authors'][$i]['affiliation'] = [
                        '#type' => 'hidden',
                        '#value' => $_['affiliation']
                    ];
					
                    $form['authors'][$i]['uid'] = [
                        '#type' => 'hidden',
                        '#value' => $_['uid']
                    ];
                    
                    $i++;
                }
            }
        }
		
        $plink = Url::fromRoute('publisso_gold.book.chapter.preview',  ['bk_id' => $form_state->get('bk_id')]);
		$plink = $plink->toString();
		
		$form['preview'] = [
			'#type' => 'button',
			'#value' => t('Preview'),
			'#attributes' => [
				'onclick' => 'return previewBookchapter(\''.$plink.'\', \'publisso-goldbookmanagementformaladaptbookchapter\')'
			],
			'#button_type' => 'primary',
			'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
			'#access' => $form_state->get('step') == 1 ? true: false
		];
		
        if($form_state->get('step') > 1){
            $form['back'] = [
                '#type' => 'submit',
                '#value' => (string)t('Back'),
                '#submit' => ['::back'],
				'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
				'#limit_validation_errors' => []
            ];  
        }
        
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => t($form_state->get('step') == 1 ? (string)t('Save & Proceed') : ($form_state->get('wf_id') ? 'Save' : 'Save chapter'))
        ];
		
		$form['store_data_control_key'] = [
			'#type' => 'hidden',
			'#value' => 'previewchapter'
		];
        
		$form['#attached'] = [
			'library' => [
				'publisso_gold/default'
			]
		];
		
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        foreach($form_state->getValue('authors') as $_){
            
            list($id, $author) = explode('|', $_);
            
            if($id){
                
                $id = base_convert(trim($id), 36, 10);
                $author = trim($author);
                
                $user = new \Drupal\publisso_gold\Controller\User($id);
                
                if(!($user->profile->getElement('firstname').' '.$user->profile->getElement('lastname') == $author)){
                    
                    $form_state->setErrorByName('authors', (string)t('Please use only suggested values!'));
                }
            }
        }
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function back(array &$form, FormStateInterface $form_state){
        
		$authors = $form_state->get('authors');
		
		$i = 0;
		
		foreach($_REQUEST['authors'] as $k => $v){
			
			$authors[$i]['is_corresponding'] = $v['is_corresponding'] ? true : false;
			$authors[$i]['to_delete'       ] = $v['to_delete'       ] ? true : false;
			$i++;
		}
		
		$new_authors = [];
		$count_authors = $num_authors = 0;
		
		foreach($authors as $_){
			
			if(!$_['to_delete']){
				$new_authors[] = $_;
				!empty($_['uid']) ? $count_authors++ : $num_authors++;
			}
		}
        
		$form_state->set('num_authors', $num_authors);
		$form_state->set('count_authors', $count_authors);
		$form_state->set('authors', $new_authors);
        
        $form_state->set('step', $form_state->get('step') - 1);
        $form_state->setRebuild();
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
		$objWorkflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
		$book = new \Drupal\publisso_gold\Controller\Book($objWorkflow->getDataElement('bk_id'));
		
		if($form_state->get('step') == 1){
            
            $form_state->set('step', $form_state->get('step') + 1);
			
            $authors = [];
            $weight = 0;
            
            foreach($form_state->getValue('authors') as $_){
                
                list($id, $author) = explode('|', $_);
                $id = base_convert(trim($id), 36, 10);
                
                if($id){
                    
                    $author = new \Drupal\publisso_gold\Controller\User($id);
                    
                    $authors[] = [
                        'firstname'         => $author->profile->getElement('firstname'),
                        'lastname'          => $author->profile->getElement('lastname'),
                        'affiliation'       => $author->profile->getElement('institute'),
                        'uid'               => $author->getElement('id'),
                        'is_corresponding'  => false,
                        'weight'            => ++$weight
                    ];
                }
            }
            
            foreach($form_state->getValue('more_authors') as $_){
                
                if(!(empty($_['content']['more_author_name']) || empty($_['content']['more_author_lastname']) || empty($_['content']['more_author_affiliation']))){
                
                    $authors[] = [
                        'firstname'         => $_['content']['more_author_name'],
                        'lastname'          => $_['content']['more_author_lastname'],
                        'affiliation'       => $_['content']['more_author_affiliation'],
                        'uid'               => null,
                        'is_corresponding'  => false,
                        'weight'            => ++$weight
                    ];
                }
            }
            
			$weight = 0;
			/*
			foreach($form_state->getValue('corporations') as $_){
                
				if(!empty($_)){
					$corporations[] = $_;
				}
            }
			*/
            $form_state->set('form_data', $form_state->getValues());
            $form_state->set('authors', $authors);
			$form_state->set('corporations', $form_state->getValue('corporations'));
			
            $form_state->setRebuild();
            return $form;
        }
        elseif($form_state->get('step') == 2){
            
            //save comment if esists

            $comment = $form_state->getValue('comment');
            
            if($comment != ''){
                
                $comment = base64_encode($comment);
                
                \Drupal::database()
                    ->insert('rwPubgoldWorkflowComments')
                    ->fields([
                        'wfc_created_by_uid' => $_SESSION['user']['id'],
                        'wfc_comment' => $comment,
                        'wfc_wfid' => $objWorkflow->getElement('id')
                    ])
                    ->execute();
            }
			
			//historize workflow
            $objWorkflow->historize();
            
            
// -- Store Formdata            

            //title
            $objWorkflow->setDataElement('title', $form_state->getValue('title'));
            
            //abstract
            $element                = 'abstract';
            $$element               = $form_state->getValue($element);
            //$$element      			= normalizeInlineImageLinks($$element);
            $objWorkflow->setDataElement($element, base64_encode($$element));
            
            //chapter_text
            $element                = 'chapter_text';
            $$element               = $form_state->getValue($element);
            //$$element      			= normalizeInlineImageLinks($$element);
            $objWorkflow->setDataElement($element, base64_encode($$element));
            
            //authors
            $element                = 'authors';
            $$element               = $form_state->getValue($element);
            $objWorkflow->setDataElement($element, json_encode($$element));
            
            //keywords
            $element                = 'keywords';
            $$element               = $form_state->getValue($element);
            $$element               = explode(';', $$element);
            $$element               = array_map("trim", $$element);
            $objWorkflow->setDataElement($element, json_encode($$element));
            
            //all other elements
            $elements = [
                'accept_policy',
                'corporation',
                'conflict_of_interest',
                'conflict_of_interest_text',
                'funding_name',
                'funding_id',
                'doi',
                'license',
                'version',
                'publication_place',
                'publication_year',
                'sub_category'
            ];
            
            foreach($elements as $element)
                $objWorkflow->setDataElement($element, $form_state->getValue($element));
            
// -- Store Formdata --            

// -- Assign Submission
		
			//Adaption finished
			$objWorkflow->setState('editor clearing request');
			//unlock workflow
			$objWorkflow->unlock();
            
            //save workflow
            $objWorkflow->setElement('modified', date('Y-m-d H:i:s'));
            $objWorkflow->save();
            
// -- Assign Submission --
            
            $form_state->setRedirect('publisso_gold.dashboard');
            
            return $form;
        }
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthor(array &$form, FormStateInterface $form_state) {
        
        $form_state->set('num_authors', $form_state->get('num_authors') + 1);
        
        $form['fs_authors']['content']['more_authors'][] = [
            '#type' => 'fieldset',
            '#title' => (string)t('Author').' #'.$form_state->get('num_authors'),
            'content' => [
                'more_author_name' => [
                    '#type' => 'textfield',
                    '#title' => (string)t('Name')
                ],
                
                'more_author_lastname' => [
                    '#type' => 'textfield',
                    '#title' => (string)t('Lastname')
                ],
                
                'more_author_affiliation' => [
                    '#type' => 'textfield',
                    '#title' => (string)t('Affiliation')
                ]
            ]
        ];
        
        $form_state->setRebuild();
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function add_corporation(array &$form, FormStateInterface $form_state) {
        
        $form_state->set('count_corporations', $form_state->get('count_corporations') + 1);
        
        $element = [
			'#type' => 'textfield'
        ];
        
		$form['fs_authors']['content']['corporations'][$form_state->get('count_corporations')] = $element;
		
        $form_state->setRebuild();
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function add_corporation_callback(array &$form, FormStateInterface $form_state){
		return $form['fs_corporations']['content']['corporations'];
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function add_author(array &$form, FormStateInterface $form_state) {
        
        $form_state->set('count_authors', $form_state->get('count_authors') + 1);
        
        $element = [
			'#description' => (string)t('Please enter Firstname and Lastname, separated with Whitespace (i.e. John Smith)'),
			'#type' => 'textfield',
			'#autocomplete_route_name' => 'autocomplete.user',
			'#autocomplete_route_parameters' => array()
        ];
        
		$form['fs_authors']['content']['authors'][$form_state->get('count_authors')] = $element;
		
        $form_state->setRebuild();
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function add_author_callback(array &$form, FormStateInterface $form_state){
		return $form['fs_authors']['content']['authors'];
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function add_coauthor(array &$form, FormStateInterface $form_state) {
        
        $form_state->set('count_coauthors', $form_state->get('count_coauthors') + 1);
        
        $element = [
			'#description' => (string)t('Please enter Firstname and Lastname, separated with Whitespace (i.e. John Smith)'),
			'#type' => 'textfield',
			'#autocomplete_route_name' => 'autocomplete.user',
			'#autocomplete_route_parameters' => array()
        ];
        
		$form['fs_coauthors']['content']['coauthors'][$form_state->get('count_coauthors')] = $element;
		
        $form_state->setRebuild();
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function add_coauthor_callback(array &$form, FormStateInterface $form_state){
		return $form['fs_coauthors']['content']['coauthors'];
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorCallback(array &$form, FormStateInterface $form_state) {
        return $form['fs_authors']['content']['more_authors'];
    }
}
