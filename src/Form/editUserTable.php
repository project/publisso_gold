<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\editUserTable.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;
use Drupal\publisso_gold\Controller\UserTable;

/**
 * Provides a simple example form.
 */
class editUserTable extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(){
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'editUserTable';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
        
        if(!$form_state->get('tableName')) $form_state->set('tableName', $args['tableName']);
        
        $session = \Drupal::service('session');
        
        //load tables
        $userTables = new \Drupal\publisso_gold\Controller\UserTable();
        
        if(!$userTables->tableExists($form_state->get('tableName'))){
                return [
                        'message' => [
                                '#markup' => (string)t("The table @tableName couldn't be found!", ['@tableName' => "»".$form_state->get('tableName')."«"]),
                                '#prefix' => '<h4>',
                                '#suffix' => '</h4>'
                        ],
                        '#cache' => [
                                'max-age' => 0
                        ]
                ];
        }
        
        //load table
        if(($table = $userTables->getTable($form_state->get('tableName'))) === false){
                return [
                        'message' => [
                                '#markup' => (string)t("The table @tableName couldn't be found!", ['@tableName' => "»".$form_state->get('tableName')."«"]),
                                '#prefix' => '<h4>',
                                '#suffix' => '</h4>'
                        ],
                        '#cache' => [
                                'max-age' => 0
                        ]
                ];
        }
        
        //load table-metadata
        if(($meta = $userTables->getTableMeta($form_state->get('tableName'))) === false){
                return [
                        'message' => [
                                '#markup' => (string)t("The metadata for table @tableName couldn't be found!", ['@tableName' => "»".$form_state->get('tableName')."«"]),
                                '#prefix' => '<h4>',
                                '#suffix' => '</h4>'
                        ],
                        '#cache' => [
                                'max-age' => 0
                        ]
                ];
        }
        
        $tblAry = $userTables->getTableArray($table, $meta);
        
        $headers = [];
        
        foreach($tblAry['headers'] as $head){
                $headers[] = $head['title'];
        }
        
        if(!$form_state->has('headers')) $form_state->set('headers', $headers);
        
        //remove empty rows
        $tblAry['rows'] = array_values($tblAry['rows']);
        
        if(!$form_state->has('colNr')) $form_state->set('colNr', count($tblAry['headers']));
        if(!$form_state->has('rowNr')) $form_state->set('rowNr', count($tblAry['rows']));
        
        $form['mu_headline'] = [
                '#markup' => (string)t('Edit table @tableName', ['@tableName' => '»'.$form_state->get('tableName').'«']),
                '#prefix' => '<h3>',
                '#suffix' => '</h3>'
        ];
        
        $form['mu_hint'] = [
                '#markup' => 'Empty rows will be ignored.',
                '#prefix' => '<hr>',
                '#suffix' => '<hr>'
        ];
        
        $form['table-wrapper'] = [
                '#type' => 'container',
                '#prefix' => '<div id="table-wrapper">',
                '#suffix' => '</div>'
        ];
        
        $form['table-wrapper']['table'] = [
                '#type' => 'table',
                '#header' => $headers
        ];
        
        for($i = 0; $i < $form_state->get('rowNr'); $i++){
                
                for($ii = 0; $ii < $form_state->get('colNr'); $ii++){
                        
                        $form['table-wrapper']['table'][$i][$ii] = [
                                '#type' => $i == 0 ? 'textfield' : 'textarea',
                                '#default_value' => $tblAry['rows'][$i][$ii],
                                '#rows' => 1
                        ];
                }
        }
        
        $form['removeRow'] = [
                '#type' => 'submit',
                '#submit' => ['::removeRow'],
                '#value' => (string)t('Remove last row'),
                '#ajax' => [
                        'wrapper' => 'table-wrapper',
                        'callback' => '::removeRowCallback',
                        'progress' => [
                                'type' => NULL,
                                'message' => NULL,
                        ]
                ],
                '#suffix' => '&nbsp;&nbsp;&nbsp;',
                '#limit_validation_errors' => []
        ];
        
        $form['addRow'] = [
                '#type' => 'submit',
                '#submit' => ['::addRow'],
                '#value' => (string)t('Add row'),
                '#ajax' => [
                        'wrapper' => 'table-wrapper',
                        'callback' => '::addRowCallback',
                        'progress' => [
                                'type' => NULL,
                                'message' => NULL,
                        ]
                ],
                '#suffix' => '<br><br>',
                '#limit_validation_errors' => []
        ];
        
        $form['submit'] = [
                '#type' => 'submit',
                '#value' => (string)t('Save'),
                '#prefix' => '<hr>',
                '#suffix' => '<br><br>'
        ];
        
        $form['#cache']['max-age'] = 0;
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $node = '
                <data version="" timestamp="::timestamp::">
                        ::rows::
                </data>
        ';

        $tableData      = $form_state->getValue('table');
        $tableName      = $form_state->getValue('name');
        $session        = \Drupal::service('session');
        $tableHeaders   = $form_state->get('headers');
        
        $dataMarkup = '';
        
        foreach($tableData as $weight => $row){
                
                $tmpl = '
                        <row weight="'.$weight.'">
                ';
                
                $allEmpty = true;
                
                foreach($row as $id => $cell){
                        
                        if(!empty($cell)) $allEmpty = false;
                        $tmpl .= '
                              <cell for="'.md5($tableHeaders[$id]).'">'.trim($cell).'</cell>  
                        ';
                }
                
                $tmpl .= '
                        </row>
                ';
                
                if(!$allEmpty) $dataMarkup .= $tmpl;
        }
        
        $node = str_replace('::rows::', $dataMarkup, $node);
        $node = str_replace('::timestamp::', time(), $node);
        
        $userTable = new UserTable();
        $userTable->modifyTable($form_state->get('tableName'), $node);
        
        $form_state->setRedirect('publisso_gold.user.table', ['action' => 'show', 'name' => $form_state->get('tableName')]);
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function addRow(array &$form, FormStateInterface $form_state){
        $rowNr = $form_state->get('rowNr');
        $rowNr += 1;
        $form_state->set('rowNr', $rowNr);
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addRowCallback(array &$form, FormStateInterface $form_state){
        return $form['table-wrapper'];
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function removeRow(array &$form, FormStateInterface $form_state){
        $rowNr = $form_state->get('rowNr');
        $rowNr -= 1;
        $form_state->set('rowNr', $rowNr < 1 ? 1 : $rowNr);
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function removeRowCallback(array &$form, FormStateInterface $form_state){
        return $form['table-wrapper'];
    }
}
