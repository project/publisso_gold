<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\FormalAdaptJournalArticle.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;
use Drupal\Core\Url;

/**
 * Provides chapter create form.
 */
class FormalAdaptJournalArticle extends FormBase {

        private $modname = 'publisso_gold';
        private $database;
        private $modpath;
        private $modaccessweights = [];
        private $simpleValues;
        
        const __ACCESS_DENIED__             = 'You can not access this page. You may need to log in first.';
        
        /**
         * {@inheritdoc}
         */
        public function getFormId() {
                return 'formaladaptjournalarticle';
        }
  
        /**
         * {@inheritdoc}
         */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                //$form_state->set('form_data', unserialize(base64_decode('YTo2OntzOjU6InRpdGxlIjtzOjE4OiJUaXRsZSBvZiBDaGFwdGVyIDEiO3M6ODoia2V5d29yZHMiO3M6MzE6IktleXdvcmQgMTsgS2V5d29yZCAyOyBLZXl3b3JkIDMiO3M6MTI6ImNoYXB0ZXJfdGV4dCI7czo1MzM6IjxwPiZiZHF1bztEbyB5b3Ugc2VlIGFueSA8c3BhbiBzdHlsZT0iY29sb3I6ICNmZjAwMDA7Ij48c3Ryb25nPlRlbGV0dWJiaWVzPC9zdHJvbmc+PC9zcGFuPiBpbiBoZXJlPyBEbyB5b3Ugc2VlIGEgc2xlbmRlciBwbGFzdGljIHRhZyBjbGlwcGVkIHRvIG15IHNoaXJ0IHdpdGggbXkgbmFtZSBwcmludGVkIG9uIGl0PyBEbyB5b3Ugc2VlIGEgbGl0dGxlIEFzaWFuIGNoaWxkIHdpdGggYSBibGFuayBleHByZXNzaW9uIG9uIGhpcyBmYWNlIHNpdHRpbmcgb3V0c2lkZSBvbiBhIG1lY2hhbmljYWwgaGVsaWNvcHRlciB0aGF0IHNoYWtlcyB3aGVuIHlvdSBwdXQgcXVhcnRlcnMgaW4gaXQ/IDxzcGFuIHN0eWxlPSJiYWNrZ3JvdW5kLWNvbG9yOiAjZmYwMDAwOyI+Tm88L3NwYW4+PyBXZWxsLCB0aGF0J3Mgd2hhdCB5b3Ugc2VlIGF0IGEgdG95IHN0b3JlLiBBbmQgeW91IG11c3QgdGhpbmsgeW91J3JlIGluIGEgdG95IHN0b3JlLCBiZWNhdXNlIHlvdSdyZSBoZXJlIHNob3BwaW5nIGZvciBhbiBpbmZhbnQgbmFtZWQgSmViLiZsZHF1bzs8L3A+IjtzOjk6ImF1dGhvcl9kYiI7YTo1OntpOjA7czoxODoiMSB8IFN0ZWZhbiBTZWxpZ2VyIjtpOjE7czoxMzoiUSB8IEMuIFNjaHVseiI7aToyO3M6MTc6IlAgfCBVcnN1bGEgQXJuaW5nIjtpOjM7czoxNzoiTyB8IEthdGphIFBsZXRzY2giO2k6NDtzOjIzOiIxVCB8IENvbnN0YW56ZSBCZXJpbmdlciI7fXM6MTA6ImF1dGhvcl9hZGQiO2E6Mjp7aTowO2E6Mzp7czo5OiJmaXJzdG5hbWUiO3M6NjoiS3V0dGVsIjtzOjg6Imxhc3RuYW1lIjtzOjg6IkRhZGRlbGR1IjtzOjExOiJhZmZpbGlhdGlvbiI7czo4OiJGaXNodG93biI7fWk6MTthOjM6e3M6OToiZmlyc3RuYW1lIjtzOjQ6IkthcmwiO3M6ODoibGFzdG5hbWUiO3M6NToiS25hY2siO3M6MTE6ImFmZmlsaWF0aW9uIjtzOjExOiJFbnRlbmhhdXNlbiI7fX1zOjExOiJjb3Jwb3JhdGlvbiI7YToyOntpOjA7czoxMzoiQ29ycG9yYXRpb24gSSI7aToxO3M6MTQ6IkNvcnBvcmF0aW9uIElJIjt9fQ==')));
                
                $form_state->set('simpleValues', [
                        'title'                    ,
                        'conflict_of_interest'     ,
                        'conflict_of_interest_text',
                        'funding'                  ,
                        'funding_name'             ,
                        'funding_id'               ,
                        'reviewer_suggestion'      ,
                        'accept_policy'            ,
                        'add_files'                ,
                        'doi'                      ,
                        'version'                  ,
                        'license'                  ,
                        'publication_place'        ,
                        'publication_year'         ,
                        'publisher'                ,
                        'sub_category'             ,
                        'comment'                  ,
                        'volume'                   ,
                        'issue'                    ,
                        'article_type'             ,
                        'references'               ,
                        'docno'                    ,
                        'received'                 ,
                        'revised'                  ,
                        'accepted'                 ,
                        'published'
                ]);
                
                /**
                 * link some libraries
                 */
                $this->modpath = drupal_get_path('module', $this->modname);
                require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
                
                /**
                 * try to load journal. in case of failure, throw error-message
                 */
                $ret = $this->getJournal(array_key_exists('jrn_id', $args) ? $args['jrn_id'] : null);
                
                if($ret['journal'] === false){
                        return [
                                '#markup' => (string)$this->t('ERROR: '.$ret['msg'])
                        ];
                }
                
                $journal = $ret['journal'];
                if(!$form_state->has('journal')) $form_state->set('journal', $journal);
                
                /**
                 * try to load workflow. in case of failure, throw error-message
                 */
                $ret = $this->getWorkflow(array_key_exists('wf_id', $args) ? $args['wf_id'] : null);
                
                if($ret['workflow'] === false){
                        return [
                                '#markup' => (string)$this->t('ERROR: '.$ret['msg'])
                        ];
                }
                
                $workflow = $ret['workflow'];
                if(!$form_state->has('workflow')) $form_state->set('workflow', $workflow);
                
                /**
                 * workflow has to be set in revision-state. if not, abort.
                 */
                $allowedStates = [
                        'accepted',
                        'author change request',
                        'editor change request'
                ];
                if(!in_array($form_state->get('workflow')->getDataElement('state'), $allowedStates)){
                        return [
                                '#markup' => (string)$this->t('ERROR: Workflow has a wrong state (@state)!', ['@state' => $form_state->get('workflow')->getDataElement('state')])
                        ];
                }
                
                /**
                 * set some global parameters
                 */
                
                $link_book = Url::fromRoute('publisso_gold.journals_journal', ['jrn_id' => $journal->getElement('id')])->toString();
                $language = \Drupal::languageManager()->getCurrentLanguage();
                $this->database = \Drupal::database();
        
                //Link1 (uri): Authors contract
                //Link2 (uri_1): Book policy
                $uri_de         = 'https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/#c5185';
		$url_de         = Url::fromUri($uri_de);
		$uri_1_de       = 'https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/';
		$url_1_de 	= Url::fromUri($uri_1_de);
		$text_de        = 'German';
	
		$uri_en         = 'https://www.publisso.de/en/publishing/books/books-policy/#c5189';
		$url_en 	= Url::fromUri($uri_en);
		$uri_1_en       = 'https://www.publisso.de/en/publishing/books/books-policy/';
		$url_1_en 	= Url::fromUri($uri_1_en);
		$text_en        = 'English';
                
                //initiate steps if not done
                if(!$form_state->get('step')) $form_state->set('step', 1);
                
//load workflow-data unless already done

                if(!$form_state->get('wf_data_loaded') === true){
                        
                        $form_data = [
                        ];
                        
                        $key = 'abstract';
                        $form_data[$key] = base64_decode($form_state->get('workflow')->getDataElement($key));
                        
                        $key = 'article_text';
                        $form_data[$key] = base64_decode($form_state->get('workflow')->getDataElement($key));
                        
                        $key = 'keywords';
                        $form_data[$key] = implode('; ', json_decode($form_state->get('workflow')->getDataElement($key), true));
                        
                        $form_data['author_db' ] = [];
                        $form_data['author_add'] = [];
                        
                        foreach(json_decode($form_state->get('workflow')->getDataElement('authors'), true) as $author){
                                
                                if(array_key_exists('uid', $author)){
                                        
                                        $user = new \Drupal\publisso_gold\Controller\User($author['uid']);
                                        
                                        if($user && $user->getElement('id')){
                                                
                                                $ary = [];
                                                
                                                $ary['author'] = implode(
                                                        '; ',
                                                        array_filter([
                                                                implode(
                                                                        ' ',
                                                                        array_filter([
                                                                                $user->profile->getElement('firstname'),
                                                                                $user->profile->getElement('lastname' )
                                                                        ])
                                                                ),
                                                                $user->profile->getElement('department'),
                                                                $user->profile->getElement('institute' ),
                                                                getCountry($user->profile->getElement('country'))
                                                        ])
                                                );
                                                
                                                $ary['is-corresponding' ] = $author['is_corresponding'  ];
                                                $ary['weight'           ] = $author['weight'            ];
                                                
                                                $form_data['author_db'][] = $ary;
                                        }
                                }
                                else{
                                        $form_data['author_add'][] = $author;
                                }
                        }
                        
                        $form_data['corporation'] = json_decode($form_state->get('workflow')->getDataElement('corporations'), true);
                        
                        //load simple data
                        
                        foreach($form_state->get('simpleValues') as $key)
                                $form_data[$key] = $form_state->get('workflow')->getDataElement($key);
                        

                        $form_state->set('form_data', $form_data);
                        $form_state->set('wf_data_loaded', true);
                }
                
                /*#############################################################################################*/
                
                /**
                 * show the form.
                 * at this point should all parameters set
                 * and only form-depending things processed
                 */
                $form['#tree'] = true;
                
                $form['mu_comments'] = $this->getCommentsForAuthor($form_state->get('workflow')->getElement('id'));
                $form['mu_files'   ] = $this->getAssignedFiles($form_state->get('workflow')->getElement('id'));
                
                $form ['mu_site_headline'] = [
                        '#markup' => (string)$this->t('Formal adapt chapter'),
                        '#prefix' => '<h1>',
                        '#suffix' => '</h1>'
                ];
                
                $form['mu_journal_title'] = [
                        '#markup' => $journal->getElement('title'),
                        '#prefix' => '<h2>',
                        '#suffix' => '</h2>'
                ];
                
                /**
                 * form step 1
                 */
/* Step 1 */    //if($form_state->get('step') == 1):
                        
                        //assign article to volume/issue
                        $form['volume'] = [
                                '#type'         => 'select',
                                '#title'        => (string)t('Volume'),
                                '#options'      => \Drupal::service('publisso_gold.tools')->getJournalVolumes($form_state->get('journal')->getElement('id')),
                                '#required'     => true,
                                '#ajax'         => [
                                        'effect'        => 'fade',
                                        'event'         => 'change',
                                        'callback'      => '::setVolID',
                                        'wrapper'       => 'wrapper-issue',
                                        'progress'      => [
                                                'type'          => 'none',
                                                'message'       => NULL
                                        ]
                                ]
                        ];
                        
                        if($form_state->get('form_data')['volume']) $form['volume']['#default_value'] = $form_state->get('form_data')['volume'];
                        
                        $issues = [];
                        
                        if($form_state->get('form_data')['volume']){
                                $issues = \Drupal::service('publisso_gold.tools')->getVolumeIssues($form_state->get('form_data')['volume']);
                        }
                        
                        if($form_state->hasValue('volume')){
                                $issues = \Drupal::service('publisso_gold.tools')->getVolumeIssues($form_state->getValue('volume'));
                        }
                        
                        $form['wrapper-issue'] = [
                                '#type'         => 'container',
                                '#prefix'       => '<div id="wrapper-issue">',
                                '#suffix'       => '</div>',
                                'content'       => [
                                        'issue'         => [
                                                '#type'         => 'select',
                                                '#title'        => (string)t('Issue'),
                                                '#options'      => $issues,
                                                '#suffix'       => '<br>',
                                                '#validated'    => true
                                        ]
                                ]
                        ];
                        
                        if($form_state->get('form_data')['issue']) $form['wrapper-issue']['content']['issue']['#default_value'] = $form_state->get('form_data')['issue'];
                        
                        //article-title
                        $form['title'] = [
                                '#type'         => 'textfield',
                                '#title'        => t('Title'),
                                '#required'     => true,
                                '#placeholder'  => (string)$this->t('Enter the title of your article here'),
                                '#maxlength'    => 255
                        ];
                        
                        if($form_state->get('form_data')['title']) $form['title']['#default_value'] = $form_state->get('form_data')['title'];
                        
                        //article-type
                        $form['article_type'] = [
                                '#type'         => 'select',
                                '#title'        => (string)t('Article type'),
                                '#options'      => \Drupal::service('publisso_gold.tools')->getArticleTypes(),
                                '#default_value'=> $form_state->get('form_data')['article_type']
                        ];
                        
                        //abstract (if required)
                        $form['abstract'] = [
                                '#type'         => 'textarea',
                                '#title'        => (string)$this->t('Abstract'),
                                '#required'     => true,
                                '#prefix'       => (string)$this->t('If an abstract is required or optional, please enter it here.'),
                                '#rows'         => 20,
                                '#attributes'   => [
                                        'class'         => [
                                        	\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
                                        ]
                                ],
                                '#suffix'       => '<br>'
                        ];
                        
                        if($form_state->get('form_data')['abstract']) $form['abstract']['#default_value'] = $form_state->get('form_data')['abstract'];

                        
                        //keywords
                        $form['keywords'] = [
                                '#type'                 => 'textfield',
                                '#title'                => (string)t('Keywords'),
                                '#default_value'        => implode(';', json_decode($form_state->get('form_data')['keywords'])),
                                '#placeholder'          => (string)$this->t('Enter the keywords separated with a semicolon here')
                        ];
                        
                        if($form_state->get('form_data')['keywords']) $form['keywords']['#default_value'] = $form_state->get('form_data')['keywords'];
                        
                        //article text
                        $form['article_text'] = [
                                '#type'         => 'textarea',
                                '#title'        => t('Main Text'),
                                '#required'     => true,
                                '#rows'         => 50,
                                '#attributes'   => [
                                        'class'         => [
                                        	\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
                                        ]
                                ]
                        ];
                        
                        if($form_state->get('form_data')['article_text']) $form['article_text']['#default_value'] = $form_state->get('form_data')['article_text'];
                        
                        $form['references'] = [
                                '#type'   => 'textarea',
                                '#title'  => (string)t('References'),
                                '#rows'   => 10,
                                '#cols'   => 60,
                                '#prefix' => '<br>',
                                '#id'     => 'ta-rwReferences'
                        ];
                        
                        if($form_state->get('form_data')['references']) $form['references']['#default_value'] = $form_state->get('form_data')['references'];
                        
                        //authors
                        if(count($form_state->get('form_data')['author_db' ]) && $form_state->getRebuildInfo('step1') != true) $form_state->set('cntAuthorsDB' , count($form_state->get('form_data')['author_db' ]));
                        if(count($form_state->get('form_data')['author_add']) && $form_state->getRebuildInfo('step1') != true) $form_state->set('cntAuthorsAdd', count($form_state->get('form_data')['author_add']));
                        if(!$form_state->has('cntAuthorsDB' ) || $form_state->get('cntAuthorsDB') < 3){ $form_state->set('cntAuthorsDB' , 3); }
                        if(!$form_state->has('cntAuthorsAdd')){ $form_state->set('cntAuthorsAdd', 1); }
                        
                        $form['fs_authors'] = [
                                '#type' => 'fieldset',
                                '#title' => (string)$this->t('Author(s)'),
                                '#prefix' => '<br>',
                                'content' => [
                                        'description' => [
                                                '#markup' => (string)$this->t('Enter one author per box. Authors who are not yet in the database can be added as additional authors'),
                                                '#suffix' => '<br><br>'
                                        ],
                                        'author-db' => [
                                                '#type'   => 'container',
                                                '#prefix' => '<div id="author-db-wrapper">',
                                                '#suffix' => '</div>',
                                                '#tree'   => true,
                                                'content' => [
                                                        '#type' => 'table',
                                                        '#header' => [
                                                                (string)$this->t('Author(s)'),
                                                                (string)$this->t('Corresponding'),
                                                                (string)$this->t('Position')
                                                        ]
                                                ]
                                        ],
                                        'author-db-add' => [
                                                '#type'                    => 'submit',
                                                '#button_type'             => 'warning',
                                                '#value'                   => (string)$this->t('add box'),
                                                '#limit_validation_errors' => [],
                                                '#submit'                  => ['::addAuthorDB'],
                                                '#prefix'                  => '',
                                                '#suffix'                  => '<br><br>',
                                                '#description'             => (string)$this->t('Add further authors'),
                                                '#ajax'                    => [
                                                        'wrapper'       => 'author-db-wrapper',
                                                        'effect'        => 'fade',
                                                        'callback'      => '::addAuthorDBCallback'
                                                ]
                                        ],
                                        'author-add' => [
                                                '#type'    => 'container',
                                                '#prefix'  => '<div id="author-add-wrapper">',
                                                '#suffix'  => '</div>',
                                                '#tree'    => true,
                                                'content' => []
                                        ],
                                        'author-add-add' => [
                                                '#type'                    => 'submit',
                                                '#button_type'             => 'warning',
                                                '#value'                   => (string)$this->t('add box '), //Space is important!
                                                '#limit_validation_errors' => [],
                                                '#submit'                  => ['::addAuthorAdd'],
                                                '#prefix'                  => '<br>',
                                                '#description'             => (string)$this->t('For more additional authors, add more boxes'),
                                                '#ajax'                    => [
                                                        'wrapper'       => 'author-add-wrapper',
                                                        'effect'        => 'fade',
                                                        'callback'      => '::addAuthorAddCallback'
                                                ]
                                        ]
                                ]
                        ];
                        
                        for($i = 0; $i < $form_state->get('cntAuthorsDB'); $i++){
                                
                                $form['fs_authors']['content']['author-db']['content'][$i] = [
                                        'author'           => [
                                                '#type'                          => 'textfield',
                                                '#autocomplete_route_name'       => 'autocomplete.user.public',
                                                '#autocomplete_route_parameters' => array(),
                                                '#title'                         => (string)$this->t('Author(s)'),
                                                '#title_display'                 => 'invisible',
                                                '#placeholder'                   => (string)$this->t('Enter at least 3 characters of the name and choose from the database'),
                                                '#description'                   => (string)$this->t('Matching entries from the database will be suggested automatically'),
                                                '#maxlength'                     => 256,
                                                '#id' => 'author_db_'.$i
                                        ],
                                        'is-corresponding' => [
                                                '#type' => 'checkbox'
                                        ],
                                        'weight'           => [
                                                '#type'          => 'number',
                                                '#size'          => 3,
                                                '#min'           => 1,
                                                '#max'           => 100,
                                                '#description'   => (string)$this->t('The authors’ order can be determined here')
                                        ]
                                ];
                                
                                if(isset($form_state->get('form_data')['author_db'][$i]['author'])){
                                        $form['fs_authors']['content']['author-db']['content'][$i]['author'          ]['#default_value'] = $form_state->get('form_data')['author_db'][$i]['author'          ];
                                        $form['fs_authors']['content']['author-db']['content'][$i]['is-corresponding']['#default_value'] = $form_state->get('form_data')['author_db'][$i]['is-corresponding'];
                                        $form['fs_authors']['content']['author-db']['content'][$i]['weight'          ]['#default_value'] = $form_state->get('form_data')['author_db'][$i]['weight'          ];
                                }
                        }
                        
                        for($i = 0; $i < $form_state->get('cntAuthorsAdd'); $i++){
                                
                                $form['fs_authors']['content']['author-add']['content']['author-add-'.$i] = [
                                        '#type'                          => 'fieldset',
                                        '#tree'                          => true,
                                        '#title'                         => (string)$this->t('Additional author @nr', ['@nr' => ($i + 1)]),
                                        'content' => [
                                                'firstname'   => [
                                                        '#type' => 'textfield',
                                                        '#title' => (string)$this->t('Firstname'),
                                                        '#placeholder' => (string)$this->t('Enter the firstname of additional author #@nr', ['@nr' => ($i + 1)])
                                                ],
                                                'lastname'    => [
                                                        '#type' => 'textfield',
                                                        '#title' => (string)$this->t('Lastname'),
                                                        '#placeholder' => (string)$this->t('Enter the lastname of additional author #@nr', ['@nr' => ($i + 1)])
                                                ],
                                                'affiliation' => [
                                                        '#type' => 'textfield',
                                                        '#title' => (string)$this->t('Affiliation'),
                                                        '#placeholder' => (string)$this->t('Enter the affiliation of additional author #@nr', ['@nr' => ($i + 1)])
                                                ],
                                                'weight'      => [
                                                        '#type'          => 'number',
                                                        '#size'          => 3,
                                                        '#title'         => (string)$this->t('Position')
                                                ]
                                        ]
                                ];
                                
                                if(isset($form_state->get('form_data')['author_add'][$i])){
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['firstname'  ]['#default_value'] = $form_state->get('form_data')['author_add'][$i]['firstname'  ];
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['lastname'   ]['#default_value'] = $form_state->get('form_data')['author_add'][$i]['lastname'   ];
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['affiliation']['#default_value'] = $form_state->get('form_data')['author_add'][$i]['affiliation'];
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['weight'     ]['#default_value'] = $form_state->get('form_data')['author_add'][$i]['weight'     ];
                                }
                        }
                        
                        //corporations
                        if(count($form_state->get('form_data')['corporation']) && $form_state->getRebuildInfo('step1') != true) $form_state->set('cntCorporations' , count($form_state->get('form_data')['corporation']));
                        if(!$form_state->has('cntCorporations')){ $form_state->set('cntCorporations', 1); }
                        
                        $form['corporations'] = [
                                '#type'   => 'container',
                                '#prefix' => '<div id="corporations-wrapper">',
                                '#suffix' => '</div>',
                                '#tree'   => true,
                                'content' => []
                        ];
                        
                        $form['corporations-add'] = [
                                '#type'                    => 'submit',
                                '#button_type'             => 'warning',
                                '#value'                   => (string)$this->t('add corporation'), //Space is important!
                                '#limit_validation_errors' => [],
                                '#submit'                  => ['::addCorporation'],
                                '#prefix'                  => '<br>',
                                '#description'             => (string)$this->t('Add more corporations'),
                                '#suffix'                  => '<br><br>',
                                '#ajax'                    => [
                                        'wrapper'       => 'corporations-wrapper',
                                        'effect'        => 'fade',
                                        'callback'      => '::addCorporationCallback'
                                ]
                        ];
                        
                        for($i = 0; $i < $form_state->get('cntCorporations'); $i++){
                                
                                $form['corporations']['content'][$i] = [
                                        '#type'          => 'textfield',
                                        '#title'         => (string)$this->t('Corporation(s)'),
                                        '#title_display' => $i ? 'invisible' : 'before',
                                        '#placeholder'   => (string)$this->t('Enter corporations that should appear as authors here'),
                                        '#description'   => (string)$this->t('One corporation per box')
                                ];
                                
                                if(isset($form_state->get('form_data')['corporation'][$i])) $form['corporations']['content'][$i]['#default_value'] = $form_state->get('form_data')['corporation'][$i];
                        }
                        
                        $form['doi'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t('DOI'),
                                '#default_value' => $form_state->get('form_data')['doi']
                        ];
                        
                        $form['docno'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t('DocNo'),
                                '#default_value' => $form_state->get('form_data')['docno']
                        ];
                        
                        $form['version'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t('Version'),
                                '#default_value' => $form_state->get('form_data')['version']
                        ];
                        
/* Step 2 */    //elseif($form_state->get('step') == 2):
                        
                        //conflict of interest
                        $form['conflict_of_interest'] = [
                                '#type'         => 'checkbox',
                                '#title'        => (string)t('Conflict of interest'),
                                '#prefix'       => (string)$this->t('Here you may add if there’s any conflict of interest to declare, which might be relevant regarding the development of your chapter. If there’s anything to declare you can add a description in the appearing box.')
                        ];
                        
                        if(isset($form_state->get('form_data')['conflict_of_interest'])) $form['conflict_of_interest']['#default_value'] = $form_state->get('form_data')['conflict_of_interest'];
                        
                        $form['conflict_of_interest_text'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t('Description of conflict of interest'),
                                '#maxlength'     => 256,
                                '#default_value' => isset($workflow_data) ? $workflow_data->conflict_of_interest_text : '',
                                '#states'        => array(
                                        'visible'       => array(   // action to take.
                                                ':input[name="conflict_of_interest"]' => array('checked' => true),
                                        ),
                                        'invisible'     => array(   // action to take.
                                                ':input[name="conflict_of_interest"]' => array('checked' => false),
                                        )
                                )
                        ];
                        
                        if(isset($form_state->get('form_data')['conflict_of_interest_text'])) $form['conflict_of_interest_text']['#default_value'] = $form_state->get('form_data')['conflict_of_interest_text'];
                        
                        //funding
                        $form['funding'] = [
                                '#type'         => 'checkbox',
                                '#title'        => t('Funds received'),
                                '#prefix'       => (string)$this->t('If you received any funds please add the information here.')
                        ];
                        
                        if(isset($form_state->get('form_data')['funding'])) $form['funding']['#default_value'] = $form_state->get('form_data')['funding'];
                        
                        $form['funding_name'] = [
                                '#type'      => 'textfield',
                                '#title'     => (string)$this->t('Funding name'),
                                '#maxlength' => 256,
                                '#states'    => array(
                                        'visible'   => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => true),
                                        ),
                                        'invisible' => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => false),
                                        )
                                ),
                        ];
                        
                        if(isset($form_state->get('form_data')['funding_name'])) $form['funding_name']['#default_value'] = $form_state->get('form_data')['funding_name'];
                        
                        $form['funding_id'] = [
                                '#type'   => 'textfield',
                                '#title'  => (string)t('Funding ID'),
                                '#states' => array(
                                        'visible'   => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => true),
                                        ),
                                        'invisible' => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => false),
                                        )
                                ),
                        ];
                        
                        if(isset($form_state->get('form_data')['funding_id'])) $form['funding_id']['#default_value'] = $form_state->get('form_data')['funding_id'];
                        
                        $form['license'] = [
                                '#type'          => 'select',
                                '#title'         => t('License'),
                                '#required'      => true,
                                '#options'       => \Drupal::service('publisso_gold.tools')->getLicenses(),
                                '#default_value' => $form_state->get('form_data')['license']
                        ];
                        
                        $form['publication_place'] = [
                                '#type'          => 'select',
                                '#title'         => (string)$this->t('Publication place'),
                                '#required'      => true,
                                '#options'       => \Drupal::service('publisso_gold.tools')->getPublicationPlaces(),
                                '#default_value' => isset($form_state->get('form_data')['publication_place']) ? $form_state->get('form_data')['publication_place'] : $journal->getElement('publication_place')
                        ];
                        
                        $form['publication_year'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t('Publication year'),
                                '#required'      => true,
                                '#default_value' => isset($form_state->get('form_data')['publication_year']) ? $form_state->get('form_data')['publication_year'] : $journal->getElement('publication_year')
                        ];
                        
                        $form['sub_category'] = [
                                '#type'          => 'textfield',
                                '#title'         => t('Subcategory'),
                                '#required'      => true,
                                '#default_value' => isset($form_state->get('form_data')['sub_category']) ? $form_state->get('form_data')['sub_category'] : $journal->getElement('sub_category')
                        ];
                        
                        $form['reviewer_suggestion'] = [
                                '#type'  => 'textfield',
                                '#title' => t('Reviewer suggestion'),
                                '#placeholder' => (string)$this->t('Enter the names of potential reviewers of your chapter here')
                        ];
                        
                        if(isset($form_state->get('form_data')['reviewer_suggestion'])) $form['reviewer_suggestion']['#default_value'] = $form_state->get('form_data')['reviewer_suggestion'];

                        //accept policy
                        $form['accept_policy'] = [
                                '#type'     => 'checkbox',
                                '#title'    => (string)t('I accept the author\'s contract</a>'),
                                '#required' => true,
                                '#disabled' => true
                        ];
                        
                        if(isset($form_state->get('form_data')['accept_policy'])) $form['accept_policy']['#default_value'] = $form_state->get('form_data')['accept_policy'];
                        true;
                        
/* Step 3 */    //elseif($form_state->get('step') == 3):
                
                        $form['description'] = array(
                                '#markup' => (string)$this->t('Here you can upload additional Files to your submission'),
                                '#prefix' => '<h3>',
                                '#suffix' => '</h3>'
                        );
                        /*
                        $form['files_fieldset'] = [
                                '#type' => 'fieldset',
                                '#title' => (string)$this->t('Upload additional Files'),
                                '#prefix' => '<div id="files-fieldset-wrapper">',
                                '#suffix' => '</div>',
                        ];
                        
                        if(!$form_state->has('numFiles')) $form_state->set('numFiles', 0);
                        $numFiles = $form_state->get('numFiles');
                        
                        for ($i = 0; $i < $numFiles; $i++) {

                                $form['files_fieldset']['file'][$i] = [

                                        '#type' => 'fieldset',
                                        'file'  => [
                                                '#type'                 => 'managed_file',
                                                '#title'                => (string)$this->t('File').' #'.($i + 1),
                                                '#required'             => true,
                                                '#upload_validators'    => [
                                                        'file_validate_extensions' => $_SESSION['setup']->getValue('file.upload.valid_extensions') ? [$_SESSION['setup']->getValue('file.upload.valid_extensions')] : array('gif png jpg jpeg doc docx xls xlsx midi mp4 mp3 avi mpg mpeg wav mov mid odt ods odp pdf'),
                                                        // Pass the maximum file size in bytes
                                                        'file_validate_size' => array(30 * 1024 * 1024),
                                                ],
                                                '#progress_indicator'   => 'bar',
                                                '#progress_message'     => (string)$this->t('Uploading file...')
                                        ],
                                        'meta'  => [
                                                'name' => [
                                                        '#type'         => 'textfield',
                                                        '#title'        => (string)$this->t('Filename'),
                                                        '#required'     => true
                                                ],
                                                'description'   => [
                                                        '#type'         => 'textfield',
                                                        '#title'        => (string)$this->t('Description')
                                                ]
                                        ]
                                ];
                        }
                        
                        $form['files_fieldset']['actions']['add_name'] = [
                                '#type' => 'submit',
                                '#value' => t('Add file'),
                                '#submit' => array('::addOne'),
                                '#prefix' => '<br>',
                                '#limit_validation_errors' => [],
                                '#ajax' => [
                                        'callback' => '::addmoreCallback',
                                        'wrapper' => 'files-fieldset-wrapper',
                                        'effect' => 'fade',
                                        'progress' => [
                                                'type' => 'none'
                                        ]
                                ],
                        ];
                        
                        if($numFiles > 0){

                                $form['files_fieldset']['actions']['remove_file'] = [
                                        '#type' => 'submit',
                                        '#value' => t('Remove last'),
                                        '#submit' => array('::removeCallback'),
                                        '#prefix' => '&nbsp;&nbsp;&nbsp;',
                                        '#ajax' => [
                                                'callback' => '::addmoreCallback',
                                                'wrapper' => 'files-fieldset-wrapper',
                                                'effect' => 'fade',
                                                'progress' => [
                                                        'type' => 'none'
                                                ]
                                        ],
                                        '#limit_validation_errors' => []
                                ];
                        }
                        */
                //endif;
                
                /*#############################################################################################*/
                
                $form['comment'] = [
                        '#type'         => 'textarea',
                        '#title'        => (string)t('Comment'),
                        '#suffix'       => '<br>',
                        '#prefix'       => '<br>',
                        '#default_value'=> $form_state->get('form_data')['comment']
                ];

                $form['override_dates'] = [
                        '#type' => 'details',
                        '#title' => (string)$this->t('Override dates'),
                        '#tree' => false,
                        'dates' => [
                                'received' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Received'),
                                        '#default_value' => $form_state->get('form_data')['received'] ? date("Y-m-d", strtotime($form_state->get('form_data')['received'])) : ($form_state->get('workflow')->getStateChainElement('submission finished') ? date("Y-m-d",strtotime($form_state->get('workflow')->getStateChainElement('submission finished'))) :false) ?? ""
                                ],
                                'revised' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Revised'),
                                        '#default_value' => $form_state->get('form_data')['revised'] ? date("Y-m-d", strtotime($form_state->get('form_data')['revised'])) : ($form_state->get('workflow')->getStateChainElement('revision finished') ? date("Y-m-d",strtotime($form_state->get('workflow')->getStateChainElement('revision finished'))) :false) ?? ""
                                ],
                                'accepted' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Accepted'),
                                        '#default_value' => $form_state->get('form_data')['accepted'] ? date("Y-m-d", strtotime($form_state->get('form_data')['accepted'])) : ($form_state->get('workflow')->getStateChainElement('accepted') ? date("Y-m-d",strtotime($form_state->get('workflow')->getStateChainElement('accepted'))) :false) ?? ""
                                ],
                                'published' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Published'),
                                        '#default_value' => $form_state->get('form_data')['published'] ? date("Y-m-d", strtotime($form_state->get('form_data')['published'])) : ($form_state->get('workflow')->getStateChainElement('published') ? date("Y-m-d",strtotime($form_state->get('workflow')->getStateChainElement('published'))) :false) ?? ""
                                ]
                        ]
                ];

                $form['spacer'] = [
                        '#markup' => '<hr>'
                ];
                
                $form['close'] = [
                        '#type'                         => 'submit',
                        '#default_value'                => (string)$this->t('Back to dashboard'),
                        '#button_type'                  => 'warning',
                        '#suffix'                       => '&nbsp;&nbsp;&nbsp;',
                        '#limit_validation_errors'      => [],
                        '#submit'                       => ['::backToDashboard']
                ];
                
                $form['preview'] = [
                        '#type'                         => 'button',
                        '#value'                        => (string)$this->t('Preview'),
                        '#button_type'                  => 'primary',
                        '#suffix'                       => '&nbsp;&nbsp;&nbsp;',
                        '#limit_validation_errors'      => [],
                        '#description'                  => (string)$this->t('See how your chapter looks like'),
                        '#ajax'                         => [
                                'callback' => '::previewJournalarticle',
                                'progress' => [
                                        'type' => 'none'
                                ]
                        ]
                ];
                
                $form['finish'] = [
                        '#type'         => 'submit',
                        '#value'        => (string)$this->t('Finish'),
                        '#button_type'  => 'success',
                        '#submit'       => ['::processForm']
                ];
                
                $form['store_data_control_key'] = [
                        '#type'         => 'hidden',
                        '#value'        => 'previewarticle'
                ];
                
                $form['ctrl_action'] = [
                        '#type' => 'hidden',
                        '#value' => 'autosave'
                ];
                
                $form['ctrl_type'] = [
                        '#type' => 'hidden',
                        '#value' => 'journalarticle'
                ];
                
                $form['tmp_id'] = [
                        '#type' => 'hidden',
                        '#id' => 'tmp_id'
                ];
                
                if(!empty($form_state->get('form_data')['tmp_id'])){
                        $form['tmp_id']['#value'] = $form_state->get('form_data')['tmp_id'];
                }
                
                $form['jrn_id'] = [
                        '#type' => 'hidden',
                        '#value' => $form_state->get('journal')->getElement('id')
                ];
                
                $form['#cache'] = [
                        'max-age' => 0
                ];
                
                $form_state->setCached(false);
                
                return $form;
        }
        
        /**
         * {@inheritdoc}
         */
        public function validateForm(array &$form, FormStateInterface $form_state){
                
                foreach($form_state->getValue('fs_authors')['content']['author-db']['content'] as $_){

                        list($author, $data) = preg_split('/;\s{1,}/', $_['author'], 2);
                        
                        if(!empty($author)){
                                $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                $found = 0;
                                $last_id = null;
                                $string = '';

                                foreach($res as $v){
                                        $string .= ' || '.implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)])).' | '.implode( '; ', [$author, $data]);
                                        if(
                                                implode( '; ', [$author, $data])
                                                                == 
                                                implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)]))
                                        ){ 
                                                $found++; 
                                                $last_id = $v->up_uid;
                                        }
                                }

                                if($found != 1){
                                        $form_state->setError($form, (string)$this->t('Please use only suggested values in author-field #@i!', ['@i' => $i + 1]));
                                }
                        }
                        $i++;
                }
                
                return $form;
        }
        
        /**
         * {@inheritdoc}
         */
        public function submitForm(array &$form, FormStateInterface $form_state){
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function processForm(array &$form, FormStateInterface $form_state){
                
                //init temp sorage if not done
                if(!$form_state->has('form_data')) $form_state->set('form_data', []);
                
                /**
                 * store formdata in a temporary storage
                 */
                $form_data = $form_state->get('form_data');
                
                foreach($form_state->get('simpleValues') as $key){
                        $form_data[$key] = $form_state->getValue($key);
                }
                
                $form_data['title']             = $form_state->getValue('title');
                $form_data['volume']            = $form_state->getValue('volume');
                $form_data['keywords']          = $form_state->getValue('keywords');
                $form_data['article_text']      = $form_state->getValue('article_text');
                $form_data['article_type']      = $form_state->getValue('article_type');
                $form_data['references']        = $form_state->getValue('references');
                $form_data['docno']             = $form_state->getValue('docno');
                
                if($form_state->getValue('wrapper-issue')['content']['issue'])
                        $form_data['issue'] = $form_state->getValue('wrapper-issue')['content']['issue'];
                
                if($form_state->hasValue('abstract'))
                        $form_data['abstract'] = $form_state->getValue('abstract');
                
                //authors in db
                $form_data['author_db'] = [];
                
                foreach($form_state->getValue('fs_authors')['content']['author-db']['content'] as $authorDB){
                        if(!empty($authorDB['author'])) $form_data['author_db'][] = $authorDB;
                }
                
                //additional authors
                $form_data['author_add'] = [];
                foreach($form_state->getValue('fs_authors')['content']['author-add']['content'] as $authorAdd){
                        if(!empty($authorAdd['content']['firstname'  ]) &&
                           !empty($authorAdd['content']['lastname'   ]) &&
                           !empty($authorAdd['content']['affiliation'])){
                                $form_data['author_add'][] = $authorAdd['content'];
                        }
                }
                
                //corporations
                $form_data['corporation'] = [];
                foreach($form_state->getValue('corporations')['content'] as $corporation){
                        if(!empty($corporation)) $form_data['corporation'][] = $corporation;
                }
                
                //store $form_data
                $form_state->set('form_data', $form_data);
                
                $this->finishAdaption($form, $form_state);
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function processUserInput(array &$form, FormStateInterface $form_state){
                
                //init temp sorage if not done
                if(!$form_state->has('form_data')) $form_state->set('form_data', []);
                
                /**
                 * store formdata in a temporary storage
                 */
                $form_data = $form_state->get('form_data');
                $form_raw  = $form_state->getUserInput();
                
                $form_data['title'       ] = $form_raw['title'          ];
                $form_data['keywords'    ] = $form_raw['keywords'       ];
                $form_data['chapter_text'] = $form_raw['chapter_text'   ];
                $form_data['references'  ] = $form_raw['references'     ];
                $form_data['docno'       ] = $form_raw['docno'          ];
                
                if(array_key_exists('abstract', $form_raw))
                        $form_data['abstract'] = $form_raw['abstract'];
                
                //authors in db
                $form_data['author_db'] = [];
                
                foreach($form_raw['fs_authors']['content']['author-db']['content'] as $authorDB){
                        if(!empty($authorDB['author'])) $form_data['author_db'][] = $authorDB;
                }
                
                //additional authors
                $form_data['author_add'] = [];
                foreach($form_raw['fs_authors']['content']['author-add']['content'] as $authorAdd){
                        if(!empty($authorAdd['content']['firstname'  ]) &&
                           !empty($authorAdd['content']['lastname'   ]) &&
                           !empty($authorAdd['content']['affiliation'])){
                                $form_data['author_add'][] = $authorAdd['content'];
                        }
                }
                
                //corporations
                $form_data['corporation'] = [];
                foreach($form_raw['corporations']['content'] as $corporation){
                        if(!empty($corporation)) $form_data['corporation'][] = $corporation;
                }
                
                foreach($form_state->get('simpleValues') as $key){
                        if(array_key_exists($key, $form_raw)) $form_data[$key] = $form_raw[$key];
                }
                
                //store $form_data
                $form_state->set('form_data', $form_data);
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        protected function finishAdaption(array &$form, FormStateInterface $form_state){
                
                //get the journal
                $journal = $form_state->get('journal');
                
                //get stored form-data
                $form_data = $form_state->get('form_data');
                
                //get the workflow
                $workflow = $form_state->get('workflow');
                $workflow->reload();
                $workflow->historize();
                
                //set the data
                $this->storeFormData2Workflow($workflow, $form_state, true);
                
                $workflow->forward();
                $workflow->save();
                $workflow->unlock();
                $form_state->setRedirect('publisso_gold.dashboard');
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthorDB(array &$form, FormStateInterface $form_state){
                $cntAuthorsDB = $form_state->get('cntAuthorsDB');
                $cntAuthorsDB++;
                $form_state->set('cntAuthorsDB', $cntAuthorsDB);
                $form_state->addRebuildInfo('step1', true);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorDBCallback(array &$form, FormStateInterface $form_state){
                return $form['fs_authors']['content']['author-db'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthorAdd(array &$form, FormStateInterface $form_state){
                $cntAuthorsAdd = $form_state->get('cntAuthorsAdd');
                $cntAuthorsAdd++;
                $form_state->set('cntAuthorsAdd', $cntAuthorsAdd);
                $form_state->addRebuildInfo('step1', true);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorAddCallback(array &$form, FormStateInterface $form_state){
                return $form['fs_authors']['content']['author-add'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addCorporation(array &$form, FormStateInterface $form_state){
                $cntCorporations = $form_state->get('cntCorporations');
                $cntCorporations++;
                $form_state->set('cntCorporations', $cntCorporations);
                $form_state->addRebuildInfo('step1', true);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addCorporationCallback(array &$form, FormStateInterface $form_state){
                error_log('Calling "addCorporationCallback"...');
                return $form['corporations'];
        }
        
        /**
         * @param $jrn_id
         * @return array
         */
        protected function getJournal($jrn_id){
                
                if(empty($jrn_id)){
                        return [
                                'msg' => (string)$this->t('Initiate adaption without journal! Abort!'),
                                'journal' => false
                        ];
                }
                
                //try to load journal. if fails, throw an error!
                $journal = new \Drupal\publisso_gold\Controller\Journal($jrn_id);
                
                if(!count($journal->getKeys())){
                        return [
                                'msg' => (string)$this->t('Journal (@jrn_id) not found! Abort!', ['@jrn_id' => $jrn_id]),
                                'journal' => false
                        ];
                }
                
                return [
                        'msg' => 'OK',
                        'journal' => $journal
                ];
        }
        
        /**
         * @param $wf_id
         * @return array
         */
        protected function getWorkflow($wf_id){
                
                if(empty($wf_id)){
                        return [
                                'msg' => (string)$this->t('Initiate revision without workflow! Abort!'),
                                'workflow' => false
                        ];
                }
                
                //try to load workflow. if fails, throw an error!
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
                
                if(!count($workflow->getKeys())){
                        return [
                                'msg' => (string)$this->t('Workflow (@wf_id) not found! Abort!', ['@wf_id' => $wf_id]),
                                'workflow' => false
                        ];
                }
                
                return [
                        'msg' => 'OK',
                        'workflow' => $workflow
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addOne(array &$form, FormStateInterface $form_state) {
                
                $numFiles = $form_state->get('numFiles');
                $form_state->set('numFiles', ++$numFiles);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addmoreCallback(array &$form, FormStateInterface $form_state) {
                return $form['files_fieldset'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function removeCallback(array &$form, FormStateInterface $form_state) {
                
                $numFiles = $form_state->get('numFiles');
                if($numFiles > 0) $form_state->set('numFiles', --$numFiles);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @return AjaxResponse
         */
        public function delFile(array &$form, FormStateInterface $form_state, $args = []) {
                
                $fid = $form_state->getTriggeringElement()['fid'];
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $response = new AjaxResponse();
                
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                $fids = array_diff($fids, [$fid]);
                $workflow->setDataElement('files', serialize($fids));
                
                //check if file are deleted
                //reload Workflow
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                
                if(array_search($fid, $fids)){
                        
                        $file = new \Drupal\publisso_gold\Controller\Blob($fid);
                        $response->addCommand(new AppendCommand('div#rwBlob-'.$fid, '<div class="rwError">'.((string)$this->t('Error deleting file "'.$file->meta['name'].'"')).'</div>'));
                }
                else{
                        $response->addCommand(new RemoveCommand('div#rwBlob-'.$fid));
                }
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return AjaxResponse
         */
        public function previewJournalarticle(array &$form, FormStateInterface $form_state){
                
                switch($form_state->get('step')){
                        case 1:
                                $this->processStep1($form, $form_state);
                                break;
                        case 2:
                                $this->processStep2($form, $form_state);
                                break;
                        case 3:
                                $this->processStep3($form, $form_state);
                                break;
                }
                
                $response = new AjaxResponse();
                $command = new InvokeCommand(
                        'html',
                        'trigger',
                        [
                                'previewJournalarticle',
                                [
                                        'form' => $this->getFormId(),
                                        'jrn_id'=> $form_state->get('jrn')->getElement('id'),
                                        'data' => base64_encode(json_encode($form_state->get('form_data')))
                                ]
                        ]
                );
                $response->addCommand($command);
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @throws \Exception
         */
        public function backToDashboard(array &$form, FormStateInterface $form_state){
                
                $workflow = $form_state->get('workflow');
                $workflow->reload();
                $workflow->historize();
                
                $call = "processUserInput";
                if(method_exists($this, $call)) $this->$call($form, $form_state);
                
                $this->storeFormData2Workflow($workflow, $form_state);
                
                $workflow->unlock();
                $form_state->setRedirect('publisso_gold.dashboard');
        }
        
        /**
         * @param $workflow
         * @param FormStateInterface $form_state
         * @param false $store_files
         * @throws \Exception
         */
        protected function storeFormData2Workflow(&$workflow, FormStateInterface $form_state, $store_files = false){
                
                $form_data = $form_state->get('form_data');
                
                //title
                $workflow->setDataElement('title', $form_data['title'], false);
                
                //abstract
                $element                = 'abstract';
                $$element               = $form_data[$element];
                $$element               = normalizeInlineImageLinks($$element);
                $workflow->setDataElement($element, base64_encode($$element), false);

                //article_text
                $element                = 'article_text';
                $$element               = $form_data[$element];
                $$element               = normalizeInlineImageLinks($$element);
                $workflow->setDataElement($element, base64_encode($$element), false);
                
                //keywords
                $keywords = array_filter(array_map('trim', explode(';', $form_data['keywords'])));
                $workflow->setDataElement('keywords', json_encode($keywords), false);
                
                //corporations
                $workflow->setDataElement('corporations', json_encode(array_filter(array_map('trim', $form_data['corporation']))), false);
                
                $simpleValues = $form_state->get('simpleValues');
                
                foreach($simpleValues as $key){
                        if(array_key_exists($key, $form_data)) $workflow->setDataElement($key, $form_data[$key], false);
                }
                
                //authors
                $authors = [];
                
                foreach($form_data['author_db'] as $item){
                        
                        $author           = $item['author'];
                        $is_corresponding = $item['is-corresponding'];
                        $weight           = $item['weight'];
                        
                        list($author, $data) = preg_split('/;\s{1,}/', $author, 2);
                        if(!empty($author)){
                                $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                $found = 0;
                                $last_id = null;
                                $string = '';

                                foreach($res as $v){
                                        $string .= ' || '.implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)])).' | '.implode( '; ', [$author, $data]);
                                        if(
                                                implode( '; ', [$author, $data])
                                                                == 
                                                implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)]))
                                        ){ 
                                                $found++; 
                                                $last_id = $v->up_uid;
                                        }
                                }

                                if($found == 1){
                                        
                                        $user = new \Drupal\publisso_gold\Controller\User($last_id);
                                        
                                        $authors[] = [
                                                'weight'             => $weight,
                                                'is_corresponding'   => $is_corresponding,
                                                'firstname'          => $user->profile->getElement('firstname'),
                                                'lastname'           => $user->profile->getElement('lastname'),
                                                'affiliation'        => $user->profile->getElement('institute'),
                                                'uid'                => $user->getElement('id'),
                                                'profile_archive_id' => $user->profile->makeSnapshot($_SESSION['user']['id'])
                                        ];
                                }
                        }
                }
                
                foreach($form_data['author_add'] as $item){
                        $authors[] = array_map('trim', $item);
                }
                
                $workflow->setDataElement('authors', json_encode($authors), false);
                
                if($store_files === true){
                        //files
                        $blobIds = [];
                        $wfBlobs = unserialize($workflow->getDataElement('files'));
                        
                        if(!is_array($wfBlobs)) $wfBlobs = [];
                        
                        if($form_state->hasValue('files_fieldset')){
                                
                                foreach($form_state->getValue('files_fieldset')['file'] as $file){

                                        if(count($file['file'])){

                                                $file_id = $file['file'][0];
                                                $file_meta = serialize($file['meta']);
                                                $file_db = \Drupal::database()->select('file_managed', 't')->fields('t', [])->condition('fid', $file_id, '=')->execute()->fetchAssoc();
                                                $file_type = $file_db['filemime'];
                                                $file_path = $file_db['uri'];

                                                if(preg_match('/^temporary:\/\/(.+)/', $file_path, $matches)){
                                                        $file_content = file_get_contents('/tmp/'.$matches[1]);
                                                }

                                                if($file_type && $file_content){

                                                        $blob = new \Drupal\publisso_gold\Controller\Blob();
                                                        $blob->create($file_content, $file_type, $file_meta);
                                                        $blobIds[] = $blob->getId();
                                                }
                                        }
                                }
                        }
                        
                        $workflow->setDataElement('files', serialize(array_unique(array_merge($wfBlobs, $blobIds))), false);
                }
                $workflow->setElement('modified', date('Y-m-d H:i:s'), false);
                $workflow->save();
        }
        
        /**
         * @param $wf_id
         * @return array
         */
        protected function getCommentsForAuthor($wf_id){
        
                $comments = \Drupal::database()->select('rwPubgoldWorkflowCommentsForAuthors', 'cfa')
                        ->fields('cfa', [])
                        ->condition('cfa_wfid', $wf_id, '=')
                        ->condition('cfa_for_uid', $_SESSION['user']['id'], '=')
                        ->execute()
                        ->fetchAll();

                $renderComments = [];
                $c = 0;
                
                foreach($comments as $_){

                        if($c === 0){
                                $renderComments = [
                                        '#type' => 'details',
                                        '#open' => false,
                                        '#title' => (string)t('Comment(s) for author'),
                                        '#description' => (string)t('These comments are only available for you during current workflow. Nobody else can see these comments!'),
                                        'content' => []
                                ];
                        }

                        $_->cfa_comment = base64_decode($_->cfa_comment);

                        $renderComments['content'][] = [
                                '#type' => 'details',
                                '#open' => false,
                                '#title' => (string)t('Comment').' #' . ($c + 1),
                                'content' => [
                                        '#type' => 'markup',
                                        '#markup' => $_->cfa_comment
                                ]
                        ];

                        $c++;
                }
                
                return $renderComments;
        }
        
        /**
         * @param $wf_id
         * @return array
         */
        protected function getAssignedFiles($wf_id){
                
                $files_tmpl = [];
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
                $fids = $workflow->getDataElement('files');
                if($fids) $fids = unserialize($fids);
                
                if(is_array($fids)){
                        
                        foreach($fids as $fid){
                                
                                $blob = new \Drupal\publisso_gold\Controller\Blob($fid);
                                
                                $files_tmpl[] = [
                                        '#type' => 'fieldset',
                                        '#id' => 'rwBlob-'.$fid,
                                        'content' => [
                                                '#type' => 'link',
                                                '#title' => $blob->meta['name'],
                                                '#url' => Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()]),
                                                '#suffix' => '<br><i>'.$blob->meta['description'].'</i>'
                                        ],
                                        'preview' => preg_match('/^(image|video|audio)\//', $blob->type, $matches) ? [
                                                '#type' => 'inline_template',
                                                '#template' => (
                                                        $matches[1] == 'image' ? '<br><img height="100" src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'">' : (
                                                                $matches[1] == 'video' ? '<br><video height="100" controls><source src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'" type="'.$blob->type.'"></video>' : (
                                                                        $matches[1] == 'audio' ? '<br><audio controls><source src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'" type="'.$blob->type.'"></audio>' : (
                                                                                ''
                                                                        )
                                                                )
                                                        )
                                                )
                                        ] : '',
                                        'action#'.$fid => [
                                                '#type' => 'button',
                                                '#value' => (string)$this->t('Delete File \''.$blob->meta['name'].'\''),
                                                '#prefix' => '<br><br>',
                                                'fid' => $fid,
                                                '#ajax' => [
                                                        'callback' => '::delWorkflowFile',
                                                        'wrapper' => 'rwBlob-'.$fid,
                                                        'effect' => 'slide'
                                                ],
                                                'limit_validation_errors' => []
                                        ]
                                ];
                        }
                }
                
                if(count($files_tmpl)){
                        return [
                            '#type' => 'details',
                            '#title' => t('Assigned files'),
                            '#open' => false,
                            'content' => $files_tmpl,
                            '#prefix' => '<br>'
                        ];
                }
                else{
                        return [];
                }
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @return AjaxResponse
         */
        public function delWorkflowFile(array &$form, FormStateInterface $form_state, $args = []) {
                
                $fid = $form_state->getTriggeringElement()['fid'];
                $workflow = $form_state->get('workflow');
                $response = new AjaxResponse();
                
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                $fids = array_diff($fids, [$fid]);
                $workflow->setDataElement('files', serialize($fids));
                
                //check if file are deleted
                //reload Workflow
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                
                if(array_search($fid, $fids)){
                        
                        $file = new \Drupal\publisso_gold\Controller\Blob($fid);
                        $response->addCommand(new AppendCommand('div#rwBlob-'.$fid, '<div class="rwError">'.((string)$this->t('Error deleting file "'.$file->meta['name'].'"')).'</div>'));
                }
                else{
                        $response->addCommand(new RemoveCommand('div#rwBlob-'.$fid));
                }
                
                $form_state->setRebuild();
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function setVolID(array &$form, FormStateInterface $form_state){
                return $form['wrapper-issue'];
        }
}
