<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldUsermanagementAddUser.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Form\FormState;
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Mail;
        use Drupal\Core\Url;
        use Drupal\Core\Link;
        use Drupal\Core\Database\Connection;
        use Drupal\file\Element;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Symfony\Component\HttpFoundation\File;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldUsermanagementAddUser extends FormBase {
                
                const __ACCESS_DENIED__ = 'You can not access this page. You may need to log in first.';
                
                private $modname = 'publisso_gold';
                private $database;
                private $modpath;
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'publisso_goldusermanagementadduser';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state ) {
                        
                        $this->getModuleAccessPermissions();
                        
                        $this->modpath = drupal_get_path( 'module', $this->modname );
                        require_once( $this->modpath . '/inc/publisso_gold.lib.inc.php' );
                        
                        //if user set as editor, eic or eo
                        
                        $user_has_rights = $_SESSION[ 'user' ][ 'isEditor' ] | $_SESSION[ 'user' ][ 'isEiC' ] | $_SESSION[ 'user' ][ 'isEO' ];
                        
                        if ( !( \Drupal::service( 'publisso_gold.tools' )->userHasAccessRight( 'usermanagement' ) || $user_has_rights ) ) {
                                return [
                                        '#type'     => 'markup',
                                        '#markup'   => self::__ACCESS_DENIED__,
                                        '#attached' => [
                                                'library' => [
                                                        'publisso_gold/default',
                                                ],
                                        ],
                                        '#cache'    => [
                                                'max-age' => 0,
                                        ],
                                ];
                        }
                        
                        if ( !$this->database )
                                $this->database = \Drupal::database();
                        
                        if ( !$this->user )
                                $this->user = $this->database->select( 'rwpgvwUserProfiles', 'u' )->fields( 'u', [] )->condition( 'id', $this->newUserID, '=' )->execute()->fetchAssoc();
                        
                        //get userroles less or equal my own, except guest
                        $userroles = getUserroles( $this->database, 11, $_SESSION[ 'user' ][ 'weight' ] );
                        
                        //get userprofile in preview if exists
                        $user = [];
                        $user = $this
                                ->database
                                ->select( 'rwPubgoldUserprofilePreview', 'pv' )
                                ->fields( 'pv', [] )
                                ->condition( 'pv_creator', $_SESSION[ 'user' ][ 'id' ], '=' )
                                ->execute()
                                ->fetchAssoc()
                        ;
                        
                        $form[ 'roleid' ] = [
                                '#type'          => 'select',
                                '#title'         => t( 'Role' ),
                                '#options'       => $userroles,
                                '#required'      => true,
                                '#default_value' => $user[ 'pv_roleid' ],
                        ];
                        
                        $form[ 'username' ] = [
                                '#type'          => 'textfield',
                                '#title'         => t( 'Login (Username)' ),
                                '#required'      => true,
                                '#default_value' => $user[ 'pv_username' ],
                        ];
                        
                        $form[ 'salutation' ] = [
                                '#type'          => 'select',
                                '#title'         => t( 'Salutation' ),
                                '#options'       => [
                                        0 => t( 'Mr' ),
                                        1 => t( 'Mrs' ),
                                ],
                                '#default_value' => $this->user[ 'pv_salutation' ],
                                '#required'      => true,
                        ];
                        
                        $form[ 'graduation' ] = [
                                '#type'          => 'textfield',
                                '#title'         => t( 'Title prefix' ),
                                '#default_value' => $user[ 'pv_graduation' ],
                        ];
                        
                        $form[ 'firstname' ] = [
                                '#type'          => 'textfield',
                                '#title'         => t( 'First Name' ),
                                '#required'      => true,
                                '#default_value' => $user[ 'pv_firstname' ],
                        ];
                        
                        $form[ 'lastname' ] = [
                                '#type'          => 'textfield',
                                '#title'         => t( 'Last Name' ),
                                '#required'      => true,
                                '#default_value' => $user[ 'pv_lastname' ],
                        ];
                        
                        $form[ 'graduation_suffix' ] = [
                                '#type'          => 'textfield',
                                '#title'         => t( 'Title suffix' ),
                                '#default_value' => $user[ 'pv_graduation_suffix' ],
                        ];
                        
                        $form[ 'email' ] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t( 'Emailaddress' ),
                                '#required'      => true,
                                '#default_value' => $user[ 'pv_email' ],
                        ];
                        
                        $form[ 'telephone' ] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t( 'Telephone' ),
                                '#required'      => false,
                                '#default_value' => $user[ 'pv_telephone' ],
                        ];
                        
                        $form[ 'orcid' ] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t( 'ORCID' ),
                                '#default_value' => $this->user[ 'pv_orcid' ],
                        ];
                        
                        $form[ 'picture' ] = [
                                '#type'              => 'managed_file',
                                '#title'             => (string)t( 'Profile picture' ),
                                '#upload_validators' => [
                                        'file_validate_extensions' => \Drupal::service( 'publisso_gold.setup' )->getValue( 'image.upload.valid_extensions' ) ? [ \Drupal::service( 'publisso_gold.setup' )->getValue( 'image.upload.valid_extensions' ) ] : [ 'gif png jpg jpeg' ],
                                        // Pass the maximum file size in bytes
                                        'file_validate_size'       => [ 30 * 1024 * 1024 ],
                                ],
                        ];
                        
                        $urlPicture = Url::fromRoute( 'publisso_gold.getPicture', [ 'type' => 'up', 'id' => 0 ] );
                        $form[ 'show_picture' ] = [
                                '#type'   => 'markup',
                                '#markup' => '<img src="' . $urlPicture->toString() . '" class="rwUserpic">',
                        ];
                        
                        $form[ 'institute' ] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t( 'Institution' ),
                                '#required'      => false,
                                '#default_value' => $user[ 'pv_institute' ],
                        ];
                        
                        $form[ 'department' ] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t( 'Department' ),
                                '#required'      => false,
                                '#default_value' => $this->user[ 'pv_institute' ],
                        ];
                        
                        $form[ 'fs_area_of_expertise' ] = [
                                '#type'  => 'fieldset',
                                '#title' => t( 'Area of Expertise' ),
                                
                                'area_of_expertise' => [
                                        '#type'    => 'select',
                                        '#title'   => (string)t( 'Area of Expertise' ),
                                        '#options' => getAreasOfExpertise( $this->database ),
                                ],
                                
                                'pv_area_of_expertise' => [
                                        '#type'          => 'textfield',
                                        '#title'         => (string)t( 'Area of Expertise Freetext' ),
                                        '#default_value' => $this->user[ 'pv_area_of_expertise' ],
                                ],
                        ];
                        
                        $form[ 'correspondence_language' ] = [
                                '#type'          => 'select',
                                '#title'         => t( 'Correspondence Language' ),
                                '#options'       => getCorrespondenceLanguages(),
                                '#default_value' => $this->user[ 'pv_correspondence_language' ],
                        ];
                        
                        $form[ 'country' ] = [
                                '#type'          => 'select',
                                '#title'         => (string)t( 'Country' ),
                                '#options'       => getCountrylist(),
                                '#required'      => false,
                                '#default_value' => $user[ 'pv_country' ],
                        ];
                        
                        $form[ 'zip' ] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t( 'Postal code' ),
                                '#required'      => false,
                                '#default_value' => $user[ 'pv_postal_code' ],
                        ];
                        
                        $form[ 'city' ] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t( 'City' ),
                                '#required'      => false,
                                '#default_value' => $user[ 'pv_city' ],
                        ];
                        
                        $form[ 'street' ] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t( 'Street & Number' ),
                                '#required'      => false,
                                '#default_value' => $user[ 'pv_street' ],
                        ];
                        
                        $form[ 'discard' ] = [
                                '#type'        => 'submit',
                                '#value'       => (string)$this->t( 'Cancel' ),
                                '#submit'      => [ '::discardProfile' ],
                                '#suffix'      => '&nbsp;&nbsp;&nbsp;',
                                '#button_type' => 'danger',
                        ];
                        
                        $form[ 'submit' ] = [
                                '#type'        => 'submit',
                                '#value'       => (string)$this->t( 'Preview' ),
                                '#suffix'      => '<br><br>',
                                '#button_type' => 'primary',
                        ];
                        
                        $form[ '#attributes' ] = [
                                'enctype' => 'multipart/form-data',
                        ];
                        
                        $form[ '#cache' ][ 'max-age' ] = 0;
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        if ( !\Drupal::service( 'email.validator' )->isValid( $form_state->getValue( 'email' ) ) ) {
                                $form_state->setErrorByName( 'password', t( 'Please enter a valid email-address!' ) );
                        }
                        
                        //Username in pending registrations?
                        $data = \Drupal::database()->select( 'rwPubgoldRegisterPending', 't' )
                                       ->fields( 't', [ 'rp_username' ] )
                                       ->condition( 't.rp_username', $form_state->getValue( 'username' ), '=' )
                                       ->execute()->fetchAll()
                        ;
                        
                        if ( count( $data ) ) {
                                $form_state->setErrorByName( 'username', t( 'This username already exists!' ) );
                        }
                        
                        //Username in user-logins?
                        $data = \Drupal::database()->select( 'rwPubgoldUsers', 't' )
                                       ->fields( 't', [ 'user' ] )
                                       ->condition( 't.user', $form_state->getValue( 'username' ), '=' )
                                       ->execute()->fetchAll()
                        ;
                        
                        if ( count( $data ) ) {
                                $form_state->setErrorByName( 'username', t( 'This username already exists!' ) );
                        }
                        
                        //Email in user profiles?
                        $data = \Drupal::database()->select( 'rwPubgoldUserProfiles', 't' )
                                       ->fields( 't', [ 'up_email' ] )
                                       ->condition( 't.up_email', $form_state->getValue( 'email' ), '=' )
                                       ->execute()->fetchAll()
                        ;
                        
                        if ( count( $data ) ) {
                                $form_state->setErrorByName( 'email', t( 'This email-address already exists!' ) );
                        }
                        
                        //Email in pending registrations?
                        $data = \Drupal::database()->select( 'rwPubgoldRegisterPending', 't' )
                                       ->fields( 't', [ 'rp_email' ] )
                                       ->condition( 't.rp_email', $form_state->getValue( 'email' ), '=' )
                                       ->execute()->fetchAll()
                        ;
                        
                        if ( count( $data ) ) {
                                $form_state->setErrorByName( 'email', t( 'This email-address already exists!' ) );
                        }
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function discardProfile (array &$form, FormStateInterface $form_state ) {
                        
                        $user = \Drupal::service( 'session' )->get( 'user' );
                        $this
                                ->database
                                ->delete( 'rwPubgoldUserprofilePreview' )
                                ->condition( 'pv_creator', $user[ 'id' ] )
                                ->execute()
                        ;
                        
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $session = \Drupal::service( 'session' );
                        $file = [];
                        $file[ 'content' ] = null;
                        $file[ 'type' ] = null;
                        
                        //first delete profile in preview, if exists
                        \Drupal::database()
                               ->delete( 'rwPubgoldUserprofilePreview' )
                               ->condition( 'pv_creator', $_SESSION[ 'user' ][ 'id' ], '=' )
                               ->execute()
                        ;
                        
                        $aoe = $form_state->getValue( 'area_of_expertise' );
                        if ( !is_array( $aoe ) ) {
                                $aoe = [ $aoe ];
                        }
                        
                        $data = [
                                'pv_area_of_expertise_aoeid' => json_encode( $aoe ),
                                'pv_graduation_suffix'       => $form_state->getValue( 'graduation_suffix' ),
                                'pv_department'              => $form_state->getValue( 'department' ),
                                'pv_area_of_expertise'       => $form_state->getValue( 'pv_area_of_expertise' ),
                                'pv_salutation'              => $form_state->getValue( 'salutation' ),
                                'pv_orcid'                   => $form_state->getValue( 'orcid' ),
                                'pv_correspondence_language' => $form_state->getValue( 'correspondence_language' ),
                                'pv_telephone'               => $form_state->getValue( 'telephone' ),
                                'pv_username'                => $form_state->getValue( 'username' ),
                                'pv_roleid'                  => $form_state->getValue( 'roleid' ),
                                'pv_firstname'               => $form_state->getValue( 'firstname' ),
                                'pv_lastname'                => $form_state->getValue( 'lastname' ),
                                'pv_email'                   => $form_state->getValue( 'email' ),
                                'pv_graduation'              => $form_state->getValue( 'graduation' ),
                                'pv_city'                    => $form_state->getValue( 'city' ),
                                'pv_country'                 => $form_state->getValue( 'country' ),
                                'pv_institute'               => $form_state->getValue( 'institute' ),
                                'pv_street'                  => $form_state->getValue( 'street' ),
                                'pv_postal_code'             => $form_state->getValue( 'zip' ),
                                'pv_creator'                 => $session->get( 'user' )[ 'id' ],
                        ];
                        
                        //if uploaded file, process data (determine image and get image-data)
                        if ( count( $form_state->getValue( 'picture' ) ) ) {
                                
                                if ( false !== ( $ret = \Drupal::service( 'publisso_gold.tools' )->processUploadedUserAvatar( $form_state->getValue( 'picture' )[ 0 ] ) ) ) {
                                        $data[ 'pv_picture' ] = $ret[ 1 ];
                                        $data[ 'pv_picture_type' ] = $ret[ 0 ];
                                }
                        }
                        
                        \Drupal::database()
                               ->insert( 'rwPubgoldUserprofilePreview' )
                               ->fields( $data )
                               ->execute()
                        ;
                        
                        $form_state->setRedirect( 'publisso_gold.adduser_preview' );
                }
                
                private function getModuleAccessPermissions () {
                        
                        $result = \Drupal::database()
                                         ->select( 'rwPubgoldModuleaccess', 't' )
                                         ->fields( 't', [] )
                                         ->execute()
                                         ->fetchAll()
                        ;
                        
                        foreach ( $result as $res ) {
                                $this->modaccessweights[ $res->mod_name ] = $res->mod_minaccessweight;
                        }
                }
        
                /**
                 * @param $modname
                 * @return bool
                 */
                private function userHasAccessRight ($modname ) {
                        
                        if ( !array_key_exists( $modname, $this->modaccessweights ) ) {
                                return false;
                        }
                        
                        return $this->modaccessweights[ $modname ] <= $_SESSION[ 'user' ][ 'weight' ];
                }
        }
