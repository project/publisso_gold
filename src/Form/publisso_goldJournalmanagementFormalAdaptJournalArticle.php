<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldJournalmanagementFormalAdaptJournalArticle.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldJournalmanagementFormalAdaptJournalArticle extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldJournalmanagementFormalAdaptJournalArticle|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldjournalmanagementformaladaptjournalarticle';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $this->modpath = drupal_get_path('module', $this->modname);
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
        //expect build-info-args as array
        $build_info = $form_state->getBuildInfo();
        $args = $build_info['args'];
        
        $form_state->set('jrn_id', $args[0]['jrn_id']);
        $form_state->set('wf_id', $args[0]['wf_id']);
        
        if(!$form_state->get('jrn_id')){
            drupal_set_message(t('No journal given!'), 'error');
            return array();
        }
        
        $journal = getJournal(\Drupal::database(), $form_state->get('jrn_id'));
        
        if($form_state->get('wf_id')){
            $workflow = getDashboard(\Drupal::Database(), $form_state->get('wf_id'));
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            $article_text = json_decode(base64_decode($workflow_data->article_text));
            $abstract = json_decode(base64_decode($workflow_data->abstract));
            
            $delta = ['workflow', 'workflow_data'];
            foreach($delta as $_) $form_state->set($_, $$_);
        }
        
        $author = getAllUserData(\Drupal::database(), $workflow_data->author_uid);
        
        $more_authors = [];
        
        if(isset($workflow_data->more_authors)){
          
          $_ = json_decode($workflow_data->more_authors);
          $delta = 1;
          foreach($_ as $_author){
            
            $more_authors['content'][] = array(
                '#type' => 'fieldset',
                '#title' => (string)t('Author').' #'.(string)($delta + 1),
                'content' => [
                    'more_author_name~'.$delta => [
                        '#type' => 'textfield',
                        '#title' => (string)t('Name'),
                        '#default_value' => $_author->more_author_name
                    ],
                    
                    'more_author_lastname~'.$delta => [
                        '#type' => 'textfield',
                        '#title' => (string)t('Lastname'),
                        '#default_value' => $_author->more_author_lastname
                    ],
                    
                    'more_author_affiliation~'.$delta => [
                        '#type' => 'textfield',
                        '#title' => (string)t('Affiliation'),
                        '#default_value' => $_author->more_author_affiliation
                    ]
                ]
            );
            
            $delta++;
          }
        }
        
        $form = [
            
            'site_headline' => [
            '#type' => 'markup',
            '#markup' => '<h1>'.t('Add Chapter').'</h1>'
            ],
            
            'journal_title' => [
                '#type' => 'markup',
                '#markup' => '<h2>'.$journal['jrn_title'].'</h2>'
            ],
            
            'author' => [
                '#type' => 'markup',
                '#markup' => $author['profile_graduation'].' '.$author['profile_lastname'].', '.$author['profile_firstname'].' '.$author['profile_graduation_suffix'],
                '#prefix' => '<h4>'.(string)t('Author').': ',
                '#suffix' => '</h4>'
            ],
            
            'title' => [
                '#type' => 'textfield',
                '#title' => t('Title'),
                '#required' => true,
                '#default_value' => isset($workflow_data->title) ? $workflow_data->title : ''
            ],
            
            'title_translated' => [
                '#type' => 'textfield',
                '#title' => t('Title translated'),
                '#required' => true,
                '#default_value' => isset($workflow_data->title_translated) ? $workflow_data->title_translated : ''
            ],
            
            'article_type' => [
                '#type' => 'textfield',
                '#title' => t('Article type'),
                '#required' => true,
                '#default_value' => isset($workflow_data->article_type) ? $workflow_data->article_type : ''
            ],
            
            'abstract' => [
                '#type' => 'text_format',
                '#title' => t('Abstract'),
                '#required' => true,
                '#format' => isset($abstract) ? $abstract->format : 'basic_html',
                '#default_value' => isset($abstract) ? $abstract->value : ''
            ],
            
            'chapter_text' => [
                '#type' => 'text_format',
                '#title' => t('Article Text'),
                '#required' => true,
                '#rows' => 50,
                '#format' => isset($article_text) ? $article_text->format : 'full_html',
                '#default_value' => isset($article_text) ? $article_text->value : ''
            ],
            
            'authors' => [
                '#type' => 'select',
                '#multiple' => true,
                '#options' => getUsersByRole(\Drupal::database(), [2,3,4,5,6,7]),
                '#title'=> (string)t('Authors'),
                '#default_value' => isset($workflow_data->authors) ? explode(',', $workflow_data->authors) : []
            ],
            
            'more_authors' => $more_authors,
            
            'corresponding_authors' => [
                '#type' => 'select',
                '#multiple' => true,
                '#options' => getUsersByRole(\Drupal::database(), [2,3,4,5,6,7]),
                '#title' => t('Corresponding Author(s)'),
                '#required' => true,
                '#default_value' => isset($workflow_data->corresponding_authors) ? explode(',', $workflow_data->corresponding_authors) : ''
            ],
            
            'corporation' => [
                '#type' => 'textfield',
                '#title' => t('Corporation'),
                '#default_value' => isset($workflow_data->corporation) ? $workflow_data->corporation : ''
            ],
            
            'conflict_of_interest' => [
                '#type' => 'checkbox',
                '#title' => (string)t('Conflict of interest'),
                '#default_value' => isset($workflow_data->conflict_of_interest) ? $workflow_data->conflict_of_interest : 0,
                '#required' => true
            ],
            
            'conflict_of_interest_text' => [
                '#type' => 'textfield',
                '#title' => t('Description conflict of interest'),
                '#default_value' => isset($workflow_data->conflict_of_interest_text) ? $workflow_data->conflict_of_interest_text : ''
            ],
            
            'author_contract' => [
                '#type' => 'checkbox',
                '#title' => (string)t('Author contract'),
                '#default_value' => isset($workflow_data->author_contract) ? $workflow_data->author_contract : 0,
                '#required' => true
            ],
            
            'patients_rights' => [
                '#type' => 'checkbox',
                '#title' => (string)t('Patients rights'),
                '#default_value' => isset($workflow_data->patients_rights) ? $workflow_data->patients_rights : 0,
                '#required' => true
            ],
            
            'funding_name' => [
                '#type' => 'textfield',
                '#title' => t('Funing Name'),
                '#default_value' => isset($workflow_data->funding_name) ? $workflow_data->funding_name : ''
            ],
            
            'funding_id' => [
                '#type' => 'textfield',
                '#title' => t('Funing ID'),
                '#default_value' => isset($workflow_data->funding_id) ? $workflow_data->funding_id : ''
            ],
            
            'keywords' => [
                '#type' => 'textfield',
                '#title' => t('Keywords'),
                '#default_value' => isset($workflow_data->keywords) ? implode(', ', json_decode($workflow_data->keywords)) : ''
            ],
            
            'license' => [
                '#type' => 'textfield',
                '#title' => t('License'),
                '#required' => true,
                '#default_value' => isset($workflow_data->license) ? $workflow_data->license : ''
            ],
            
            'publication_year' => [
                '#type' => 'textfield',
                '#title' => t('Publication Year'),
                '#required' => true,
                '#default_value' => isset($workflow_data->publication_year) ? $workflow_data->publication_year : ''
            ],
            
            'sub_category' => [
                '#type' => 'textfield',
                '#title' => t('Subcategory'),
                '#required' => true,
                '#default_value' => isset($workflow_data->sub_category) ? $workflow_data->sub_category : ''
            ],
            
            'comment' => [
                '#type' => 'text_format',
                '#format' => 'basic_html',
                '#title' => (string)t('Comment')
            ],
            
            'submit' => [
                '#type' => 'submit',
                '#value' => (string)t('Request clearing from author')
            ]
            
        ];
        
        $form_state->disableCache();
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $delta = ['workflow', 'workflow_data'];
        foreach($delta as $_) $$_ = $form_state->get($_);
        
        //save comment if exists
        if($form_state->getValue('comment')){
            
            $comment = $form_state->getValue('comment');
            
            if($comment['value'] != ''){
                
                $comment = base64_encode(json_encode($comment));
                
                \Drupal::database()
                    ->insert('rwPubgoldWorkflowComments')
                    ->fields([
                        'wfc_created_by_uid' => $_SESSION['user']['id'],
                        'wfc_comment' => $comment,
                        'wfc_wfid' => $workflow->wf_id
                    ])
                    ->execute();
            }
        }
        
        //prepare workflow for history
        $workflow_history = array();
        $workflow_history['wfh_created_by_uid'] = $_SESSION['user']['id'];
        
        foreach($workflow as $k => $v){
            
            if($k == 'wf_id'){
                $key = 'wfh_wfid';
            }
            else{
                
                $key = explode("_", $k, 2);
                $key = $key[0].'h_'.$key[1];
                
            }
            
            $workflow_history[$key] = $v;
        }
        
        \Drupal::database()->insert('rwPubgoldWorkflowHistory')->fields($workflow_history)->execute();
        
        foreach($form_state->getValues() as $k => $v){
            $workflow_data->$k = $v;
        }
        
        $workflow_data->abstract               = base64_encode(json_encode($form_state->getValue('abstract')));
        $workflow_data->article_text           = base64_encode(json_encode($form_state->getValue('article_text')));
        $workflow_data->errata                 = base64_encode(json_encode($form_state->getValue('errata')));
        $workflow_data->correction             = base64_encode(json_encode($form_state->getValue('correction')));
        $workflow_data->license                = $form_state->getValue('license');
        $workflow_data->publication_place      = $form_state->getValue('publication_place');
        $workflow_data->publisher              = $form_state->getValue('publisher');
        $workflow_data->doi                    = $form_state->getValue('doi');
        $workflow_data->ddc                    = $form_state->getValue('ddc');
        $workflow_data->corresponding_authors  = implode(',', $form_state->getValue('corresponding_authors'));
        $workflow_data->urn                    = $form_state->getValue('urn');
        $workflow_data->keywords               = json_encode(preg_split('/[^a-zA-Z0-9äöüÄÖÜß]+/', $form_state->getValue('keywords')));
        $workflow_data->funding_name           = $form_state->getValue('funding_name');
        $workflow_data->funding_id             = $form_state->getValue('funding_id');
        $workflow_data->version                = $form_state->getValue('version');
        $workflow_data->jrn_id                 = $form_state->get('jrn_id');
        $workflow_data->title                  = $form_state->getValue('title');
        $workflow_data->title_translated       = $form_state->getValue('title_translated');
        $workflow_data->article_type           = $form_state->getValue('article_type');
        $workflow_data->doc_number             = $form_state->getValue('doc_number');
        $workflow_data->state                  = 'author clearing request';
        $workflow_data->authors                = implode(',', $form_state->getValue('authors'));
        $workflow_data->corporation            = $form_state->getValue('corporation');
        $workflow_data->conflict_of_interest   = $form_state->getValue('conflict_of_interest');
        $workflow_data->conflict_of_interest_text = $form_state->getValue('conflict_of_interest_text');
        $workflow_data->author_contract        = $form_state->getValue('author_contract');
        $workflow_data->patients_rights        = $form_state->getValue('patients_rights');

        $workflow->wf_data = base64_encode(json_encode($workflow_data));
        
        \Drupal::database()
            ->update('rwPubgoldWorkflow')
            ->fields([
                'wf_data' => $workflow->wf_data,
                'wf_assigned_to' => 'u:'.$workflow_data->author_uid,
                'wf_locked' => 0,
                'wf_locked_by_uid' => null
            ])
            ->condition('wf_id', $form_state->get('wf_id'), '=')
            ->execute();
        
        return $form;
    }
}
