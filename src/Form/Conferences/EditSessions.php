<?php
        
        
        namespace Drupal\publisso_gold\Form\Conferences;
        
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Controller\Manager\AbstractManager;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class EditSessions
         * @package Drupal\publisso_gold\Form\Conferences
         */
        class EditSessions extends FormBase {
        
                /**
                 * @return string
                 */
                public function getFormId () {
                        return 'Conference-Edit-Sessions';
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @param array $args
                 * @return array
                 */
                public function buildForm (array $form, FormStateInterface $form_state, array $args = [] ) {
                        
                        foreach($args as $k => $v){
                                if(!$form_state->has($k)) $form_state->set($k, $v);
                        }
                        
                        if(!$form_state->has('cf_id')){
                                \Drupal::service('messenger')->addError((string)$this->t('No conference-id provided!'));
                                return $form;
                        }
                        
                        $conference = ConferenceManager::getConference($form_state->get('cf_id'));
                        if(!$conference){
                                return $form;
                        }
                        
                        $render = \Drupal::service( 'renderer' );
        
                        $form[ 'table-row' ] = [
                                '#type'      => 'table',
                                '#header'    => [
                                        $this
                                                ->t( 'Type' ),
                                        $this
                                                ->t( 'Description' ),
                                        $this
                                                ->t( 'Weight' ),
                                        $this
                                                ->t( 'Parent' ),
                                        ''
                                ],
                                '#empty'     => $this
                                        ->t( 'Sorry, There are no items!' ),
                                // TableDrag: Each array value is a list of callback arguments for
                                // drupal_add_tabledrag(). The #id of the table is automatically
                                // prepended; if there is none, an HTML ID is auto-generated.
                                '#tabledrag' => [
                                        [
                                                'action'       => 'match',
                                                'relationship' => 'parent',
                                                'group'        => 'row-pid',
                                                'source'       => 'row-id',
                                                'hidden'       => true,
                                                /* hides the WEIGHT & PARENT tree columns below */
                                                'limit'        => false,
                                        ],
                                        [
                                                'action'       => 'order',
                                                'relationship' => 'sibling',
                                                'group'        => 'row-weight',
                                        ],
                                ],
                                '#prefix'    => '<div id="div-table">',
                                '#suffix'    => '</div>'
                        ];
        
                        // Build the table rows and columns.
        
                        if ( !$form_state->has( 'results' ) ) {
                
                                $results = [];
                
                                $form_state->set( 'lastID', 0 );
                                $form_state->set( 'results', $results );
                        }
                        
                        $dayCnt = $sessionCnt = $abstractCnt = 1;
                        $results = $conference->getSessions(true);
                        
                        //count result-types and calculate depth
                        #Publisso::log(print_r($results, 1));
                        foreach ( $results as $id => $row ) {
                                $row->cnt = ${$row->type."Cnt"}++;
                                $results[ $id ]->depth = $this->getItemDepth( $row, 0, $results);
                        }
                        
                        foreach ( $results as $row ) {
                
                                // TableDrag: Mark the table row as draggable.
                                $form[ 'table-row' ][ $row->id ][ '#attributes' ][ 'class' ][] = 'draggable';
                
                                // Indent item on load.
                                if ( isset( $row->depth ) && $row->depth > 0 ) {
                                        $indentation = [
                                                '#theme' => 'indentation',
                                                '#size'  => $row->depth,
                                        ];
                                }
                
                                // Some table columns containing raw markup.
                
                                $form[ 'table-row' ][ $row->id ][ 'name' ] = [
                                        '#markup' => (string)$this->t('@type @nr', ['@type' => ucfirst($row->type), '@nr' => $row->cnt]),
                                        '#prefix' => !empty( $indentation ) ? $render
                                                ->render( $indentation ) : '',
                                ];
                
                                if ( $row->type == 'day' ) {
                                        $form[ 'table-row' ][ $row->id ][ 'object' ] = [
                                                '#type'     => 'date',
                                                '#required' => true,
                                                '#title'    => 'Date:',
                                                '#default_value' => $row->day
                                        ];
                                }
                                elseif ( $row->type == 'session' ) {
                                        $form[ 'table-row' ][ $row->id ][ 'object' ] = [
                                                '#type'   => 'container',
                                                'content' => [
                                                        'title'      => [
                                                                '#type'     => 'textfield',
                                                                '#required' => true,
                                                                '#title'    => 'Title:',
                                                                '#default_value' => $row->title
                                                        ],
                                                        'time_start' => [
                                                                '#type'        => 'textfield',
                                                                '#title'       => 'From:',
                                                                '#placeholder' => 'HH:MM',
                                                                '#size'        => 6,
                                                                '#default_value' => $row->from ? date('H:i', strtotime($row->from)) : null
                                                        ],
                                                        'time_end'   => [
                                                                '#type'        => 'textfield',
                                                                '#title'       => 'To:',
                                                                '#placeholder' => 'HH:MM',
                                                                '#size'        => 6,
                                                                '#default_value' => $row->to ? date('H:i', strtotime($row->to)) : null
                                                        ]
                                                ],
                                                '#prefix' => '<div class="form-wrapper-childs-left">',
                                                '#suffix' => '</div>'
                                        ];
                                }
                                elseif ( $row->type == 'abstract' ) {
                                        $form[ 'table-row' ][ $row->id ][ 'object' ] = [
                                                '#type'   => 'container',
                                                'content' => [
                                                        'title'      => [
                                                                '#markup' => $row->title
                                                        ]
                                                ],
                                                '#prefix' => '<div class="form-wrapper-childs-left">',
                                                '#suffix' => '</div>'
                                        ];
                                }
                                
                                // This is hidden from #tabledrag array (above).
                                // TableDrag: Weight column element.
                                $form[ 'table-row' ][ $row->id ][ 'weight' ] = [
                                        '#type'          => 'weight',
                                        '#title'         => $this
                                                ->t( 'Weight for ID @id', [
                                                        '@id' => $row->id,
                                                ]
                                                ),
                                        '#title_display' => 'invisible',
                                        '#default_value' => $row->weight,
                                        // Classify the weight element for #tabledrag.
                                        '#attributes'    => [
                                                'class' => [
                                                        'row-weight',
                                                ],
                                        ],
                                ];
                                $form[ 'table-row' ][ $row->id ][ 'parent' ][ 'id' ] = [
                                        '#parents'    => [
                                                'table-row',
                                                $row->id,
                                                'id',
                                        ],
                                        '#type'       => 'hidden',
                                        '#value'      => $row->id,
                                        '#attributes' => [
                                                'class' => [
                                                        'row-id',
                                                ],
                                        ],
                                ];
                
                                $form[ 'table-row' ][ $row->id ][ 'parent' ][ 'type' ] = [
                                        '#parents'    => [
                                                'table-row',
                                                $row->id,
                                                'type',
                                        ],
                                        '#type'       => 'hidden',
                                        '#value'      => $row->type,
                                        '#attributes' => [
                                                'class' => [
                                                        'row-id',
                                                ],
                                        ],
                                ];
                
                                $form[ 'table-row' ][ $row->id ][ 'parent' ][ 'pid' ] = [
                                        '#parents'       => [
                                                'table-row',
                                                $row->id,
                                                'pid',
                                        ],
                                        '#type'          => 'number',
                                        '#size'          => 3,
                                        '#min'           => 0,
                                        '#title'         => $this
                                                ->t( 'Parent ID' ),
                                        '#title_display' => 'invisible',
                                        '#default_value' => $row->parent,
                                        '#attributes'    => [
                                                'class' => [
                                                        'row-pid',
                                                ],
                                        ],
                                ];
                
                                $form[ 'table-row' ][ $row->id ][ 'delete' . $row->id ] = [
                                        '#type'                    => 'submit',
                                        '#value'                   => (string)$this->t('Delete @type @nr', ['@type' => ucfirst($row->type), '@nr' => $row->cnt]),
                                        '#button_type'             => 'danger',
                                        '#attributes'              => [
                                                'style' => [
                                                        'width: 200px;'
                                                ]
                                        ],
                                        '#limit_validation_errors' => [],
                                        '#submit'                  => [ '::removeItem' ],
                                        '#ajax'                    => [
                                                'wrapper'  => 'div-table',
                                                'callback' => '::removeItemCallback'
                                        ],
                                        '#id'                      => 'delete|' . $row->id
                                ];
                        }
                        $form[ 'actions' ] = [
                                '#type' => 'actions',
                                '#suffix' => '<br><br>'
                        ];
                        
                        $form['actions']['cancel'] = [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Cancel'),
                                '#submit' => ['::cancelForm'],
                                '#button_type' => 'danger'
                        ];
                        
                        $form[ 'actions' ][ 'submit' ] = [
                                '#type'  => 'submit',
                                '#value' => $this
                                        ->t( 'Save All Changes' ),
                        ];
        
                        $form[ 'actions' ][ 'addDay' ] = [
                                '#type'                    => 'submit',
                                '#value'                   => 'Add day',
                                '#submit'                  => [ '::addDay' ],
                                '#limit_validation_errors' => [],
                                '#ajax'                    => [
                                        'wrapper'  => 'div-table',
                                        'callback' => '::addDayCallback'
                                ],
                                '#button_type' => 'info'
                        ];
        
                        $form[ 'actions' ][ 'addSession' ] = [
                                '#type'                    => 'submit',
                                '#value'                   => 'Add session',
                                '#submit'                  => [ '::addSession' ],
                                '#limit_validation_errors' => [],
                                '#ajax'                    => [
                                        'wrapper'  => 'div-table',
                                        'callback' => '::addSessionCallback'
                                ],
                                '#button_type' => 'info'
                        ];
                        
                        $form['#cache'] = [
                                'max-age' => 0
                        ];
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function cancelForm(array &$form, FormStateInterface $form_state){
                        $form_state->setRedirect('publisso_gold.conferencemanagement', ['action' => 'overview']);
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function removeItem(array &$form, FormStateInterface $form_state){
                        
                        $id = explode('|', $form_state->getTriggeringElement()['#id'])[1];
                        $qry = \Drupal::database()->delete('rwPubgoldConferenceSessions');
                        $condition = $qry->orConditionGroup()->condition('id', $id, '=')->condition('parent', $id, '=');
                        $qry->condition($condition)->execute();
                        ConferenceManager::getConference($form_state->get('cf_id'))->renewTOC();
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function removeItemCallback(array &$form, FormStateInterface $form_state){
                        return $form['table-row'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @throws \Exception
                 */
                public function addSession(array &$form, FormStateInterface $form_state){
                        
                        $qry = \Drupal::database()->select('rwPubgoldConferenceSessions', 't');
                        $qry->addExpression('case when isnull(MAX(weight)) then 0 else max(weight) end', 'max_weight');
                        $qry->condition('cf_id', $form_state->get('cf_id'));
                        $maxWeight = $qry->execute()->fetchField();
        
                        $qry = \Drupal::database()->insert('rwPubgoldConferenceSessions');
                        $qry->fields(['cf_id' => $form_state->get('cf_id'), 'type' => 'session', 'weight' => $maxWeight + 1]);
        
                        $qry->execute();
                        ConferenceManager::getConference($form_state->get('cf_id'))->renewTOC();
                        $form_state->setRebuild();
                }
        
                /**
                 * @param $row
                 * @param $depth
                 * @param $results
                 * @return mixed
                 */
                private function getItemDepth($row, $depth, $results){
                        
                        if(!$row->parent) return $depth;
                        else return $this->getItemDepth($results[$row->parent], $depth + 1, $results);
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addSessionCallback(array &$form, FormStateInterface $form_state){
                        return $form['table-row'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @throws \Exception
                 */
                public function addDay(array &$form, FormStateInterface $form_state){
                        
                        $qry = \Drupal::database()->select('rwPubgoldConferenceSessions', 't');
                        $qry->addExpression('case when isnull(MAX(weight)) then 0 else max(weight) end', 'max_weight');
                        $qry->condition('cf_id', $form_state->get('cf_id'));
                        $maxWeight = $qry->execute()->fetchField();
                        
                        $qry = \Drupal::database()->insert('rwPubgoldConferenceSessions');
                        $qry->fields(['cf_id' => $form_state->get('cf_id'), 'type' => 'day', 'weight' => $maxWeight + 1]);
                        
                        $qry->execute();
                        ConferenceManager::getConference($form_state->get('cf_id'))->renewTOC();
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addDayCallback(array &$form, FormStateInterface $form_state){
                        return $form['table-row'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function submitForm (array &$form, FormStateInterface $form_state ) {
                        
                        foreach($form_state->getValue('table-row') as $id => $row){
                                Publisso::log(print_r($row, 1));
                                $fields = [];
                                
                                switch($row['type']){
                                        
                                        case 'session':
                                                if(!empty($row['object']['content']['title'     ])) $fields['title'] = $row['object']['content']['title'     ];
                                                $fields['from' ] = !empty($row['object']['content']['time_start']) ? $row['object']['content']['time_start'] : null;
                                                $fields['to'   ] = !empty($row['object']['content']['time_end']) ? $row['object']['content']['time_end'  ] : null;
                                                break;
                                        
                                        case 'abstract':
                                                $ca_id = \Drupal::database()->select('rwPubgoldConferenceSessions', 't')->fields('t', ['ca_id'], '=')->condition('id', $row['id'], '=')->execute()->fetchField();
                                                $abstract = AbstractManager::getAbstract($ca_id);
                                                $abstract->reassignSession($row['id'], $row['weight']);
                                                break;
                                                
                                        case 'day':
                                                $fields['day'] = $row['object'];
                                                break;
                                }
                                
                                if($row['pid']) $fields['parent'] = $row['pid'];
                                $fields['weight'] = $row['weight'];
                                \Drupal::database()->update('rwPubgoldConferenceSessions')->fields($fields)->condition('id', $id, '=')->execute();
                        }
                        
                        ConferenceManager::getConference($form_state->get('cf_id'))->renewTOC();
                        \Drupal::service('messenger')->addMessage('Successfully saved!');
                        
                        return $form;
                }
        }
