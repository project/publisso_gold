<?php
        
        
        namespace Drupal\publisso_gold\Form\Conferences;
        
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Classes\AuthorsContract;
        use Drupal\publisso_gold\Controller\Manager\AuthorsContractManager;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\ReviewsheetManager;
        use Drupal\publisso_gold\Controller\Medium\Conference;
        use Drupal\publisso_gold\Controller\Reviewsheet;

        /**
         * Class EditConference
         * @package Drupal\publisso_gold\Form\Conferences
         */
        class EditConference extends FormBase {
                
                private $texts;
                private $tools;
                private $setup;
                private $session;
        
                /**
                 * @return string
                 */
                public function getFormId () {
                        return 'EditConference';
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @param array $args
                 * @return array
                 */
                public function buildForm (array $form, FormStateInterface $form_state, $args = [] ) {
                        
                        if(!$form_state->has('cf_id')) $form_state->set('cf_id', $args['cf_id']);
                        $conference = ConferenceManager::getConference($form_state->get('cf_id'));
                        
                        foreach(['texts', 'setup', 'tools'] as $_) $this->$_ = \Drupal::service('publisso_gold.'.$_);
                        $this->session = \Drupal::service('session');
        
                        $availDDC = \Drupal::service('publisso_gold.tools')->getDDCs();
                        foreach($availDDC as $k => $v){
                
                                if(preg_match('/^(\d+)\s+(.+)$/', $v, $matches)){
                                        $availDDC[$matches[1]] = $matches[0];
                                }
                
                                unset($availDDC[$k]);
                        }
        
                        $availLicenses = \Drupal::service('publisso_gold.tools')->getLicenses();
                        $availCategories = \Drupal::service('publisso_gold.tools')->getCategories();
                        
                        $rps = [];
                        
                        foreach (ReviewsheetManager::getList() as $rp){
                                $rps[$rp->rp_id] = $rp->rp_name;
                        }
                        
                        $form['headline'] = [
                                '#markup' => (string)$this->t($this->texts->get('conferencemanagement.edit.frm.headline')),
                                '#prefix' => '<h1>',
                                '#suffix' => '</h1>'
                        ];
                        
                        $form['title'] = [
                                '#type'         => 'textfield',
                                '#title'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.field.title')),
                                '#maxlength'    => 500,
                                '#required'     => true,
                                '#default_value'=> $conference->getElement('title')
                        ];
                        
                        $form['reviewsheet'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('Select reviewsheet'),
                                '#options' => $rps,
                                '#default_value' => $conference->getElement('rpid'),
                                '#required' => true
                        ];
                        
                        $form['ddc'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('DDC'),
                                '#required' => true,
                                '#options' => $availDDC,
                                '#default_value' => $conference->getElement('ddc')
                        ];
        
                        $form['license'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('License'),
                                '#required' => true,
                                '#options' => $availLicenses,
                                '#default_value' => $conference->getElement('license')
                        ];
        
                        $form['category'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('Subject'),
                                '#required' => true,
                                '#options' => $availCategories,
                                '#default_value' => $conference->getElement('category')
                        ];
                        
                        $form['type'] = [
                                '#type' => 'radios',
                                '#title' => (string)$this->t('Page type'),
                                '#required' => true,
                                '#options' => [
                                        'periodical' => (string)$this->t('periodical'),
                                        'sporadic' => (string)$this->t('sporadic')
                                ],
                                '#default_value' => $conference->getElement('pagetype')
                        ];
        
                        $form['title_series'] = [
                                '#type' => 'textfield',
                                '#title' => (string)$this->t('Series Title'),
                                '#states' => [
                                        'visible'     => [':input[name="type"]' => array('value' => 'periodical')],
                                        'invisible'   => [':input[name="type"]' => array('!value' => 'periodical')],
                                ]
                        ];
                        
                        $acs = [];
                        foreach(AuthorsContractManager::getNameList() as $_){
                                $ac = AuthorsContract::load($_);
                                $acs[$ac->getName()] = $ac->getName();
                        }
        
                        $form['authors_contract'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('Authors contract'),
                                '#options' => $acs,
                                '#default_value' => $conference->getElement('authors_contract')
                        ];
                        
                        $eo = $conference->readEditorialOffice();
                        $cm = $conference->readConferenceManager();
                        if(!$form_state->has('cntEO')) $form_state->set('cntEO', count($eo));
                        if(!$form_state->has('cntCM')) $form_state->set('cntCM', count($cm));
                        
                        $form['roles'] = [
                                '#type'         => 'fieldset',
                                '#title'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.fieldset.roles.title')),
                                'editorial_office'      => [
                                        '#type'         => 'fieldset',
                                        '#title'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.fieldset.roles.fieldset.editorial_office.title')),
                                        'content'       => [
                                                '#type'         => 'container',
                                                '#prefix'       => '<div id="wrapper-eo">',
                                                '#suffix'       => '</div>',
                                                'eoUsers'       => [
                                                        '#tree'         => true
                                                ],
                                                'add'           => [
                                                        '#type'         => 'submit',
                                                        '#value'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.fieldset.roles.fieldset.editorial_office.btn.add')),
                                                        '#submit'       => ['::addEO'],
                                                        '#ajax'         => [
                                                                'callback'      => '::addEOCallback',
                                                                'wrapper'       => 'wrapper-eo'
                                                        ],
                                                        '#limit_validation_errors' => []
                                                ]
                                        ],
                                        
                                ],
                                'conference_manager'      => [
                                        '#type'         => 'fieldset',
                                        '#title'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.fieldset.roles.fieldset.conference_manager.title')),
                                        'content'       => [
                                                '#type'         => 'container',
                                                '#prefix'       => '<div id="wrapper-cm">',
                                                '#suffix'       => '</div>',
                                                'cmUsers'       => [
                                                        '#tree'         => true
                                                ],
                                                'add'           => [
                                                        '#type'         => 'submit',
                                                        '#value'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.fieldset.roles.fieldset.conference_manager.btn.add')),
                                                        '#submit'       => ['::addCM'],
                                                        '#ajax'         => [
                                                                'callback'      => '::addCMCallback',
                                                                'wrapper'       => 'wrapper-cm'
                                                        ],
                                                        '#limit_validation_errors' => []
                                                ]
                                        ],

                                ]
                        ];
                        
                        for($nr = 0; $nr < $form_state->get('cntEO'); $nr++){
                                $form['roles']['editorial_office']['content']['eoUsers'][$nr] = [
                                        '#type'                          => 'textfield',
                                        '#autocomplete_route_name'       => 'autocomplete.user',
                                        '#autocomplete_route_parameters' => array(),
                                ];
                        }
        
                        for($nr = 0; $nr < $form_state->get('cntCM'); $nr++){
                                $form['roles']['conference_manager']['content']['cmUsers'][$nr] = [
                                        '#type'                          => 'textfield',
                                        '#autocomplete_route_name'       => 'autocomplete.user',
                                        '#autocomplete_route_parameters' => array(),
                                ];
                        }
                        
                        $i = 0;
                        foreach($cm as $user){
                
                                $str = $user->getElement('id');
                                $str = base_convert($str, 10, 36).' | ';
                                $str .= $user->profile->getReadableName('FL', ' ');
                                $form['roles']['conference_manager']['content']['cmUsers'][$i]['#default_value'] = $str;
                                $i++;
                        }
        
                        $i = 0;
                        foreach($eo as $user){
                
                                $str = $user->getElement('id');
                                $str = base_convert($str, 10, 36).' | ';
                                $str .= $user->profile->getReadableName('FL', ' ');
                                $form['roles']['editorial_office']['content']['eoUsers'][$i]['#default_value'] = $str;
                                $i++;
                        }
                        
                        if($conference->getElement('edited') > -1){
                                
                                $form['publish'] = [
                                        '#type' => 'checkbox',
                                        '#title' => (string)$this->t('Publish Conference'),
                                        '#prefix' => '<hr>',
                                        '#suffix' => '<hr>',
                                        '#default_value' => $conference->getElement('published') ?? 0
                                ];
                        }
                        
                        $form['submit'] = [
                                '#type'         => 'submit',
                                '#value'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.btn.submit')),
                                '#button_type'  => 'success',
                                '#suffix'       => '<br><br>'
                        ];
                        
                        $form['#cache'] = [
                                'max-age' => 0
                        ];
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addEO (array &$form, FormStateInterface $form_state ) {
                        $form_state->set('cntEO', $form_state->get('cntEO') + 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addEOCallback (array &$form, FormStateInterface $form_state ) {
                        return $form['roles']['editorial_office']['content'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addCM (array &$form, FormStateInterface $form_state ) {
                        $form_state->set('cntCM', $form_state->get('cntCM') + 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addCMCallback (array &$form, FormStateInterface $form_state ) {
                        return $form['roles']['conference_manager']['content'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 * @throws \Exception
                 */
                public function submitForm (array &$form, FormStateInterface $form_state ) {
        
                        $conference = ConferenceManager::getConference($form_state->get('cf_id'));
                        
                        $conference->setElement('title', $form_state->getValue('title'), false);
                        
                        if($conference->getElement('edited') == 0) $conference->setElement('edited', 1, false);
                        
                        if(!empty($form_state->getValue('category'))){
                                $conference->setElement('category', $form_state->getValue('category'), false);
                        }
        
                        if(!empty($form_state->getValue('license'))){
                                $conference->setElement('license', $form_state->getValue('license'), false);
                        }
        
                        if(!empty($form_state->getValue('ddc'))){
                                $conference->setElement('ddc', $form_state->getValue('ddc'), false);
                        }
        
                        if(!empty($form_state->getValue('type'))){
                                $conference->setElement('pagetype', $form_state->getValue('type'), false);
                                
                                if($conference->getElement('pagetype') != 'periodical'){
                                        $conference->setElement('title_series', null, false);
                                }
                                elseif(!empty($form_state->getValue('title_series'))){
                                        $conference->setElement('title_series', $form_state->getValue('title_series'), false);
                                }
                        }
        
                        if($form_state->hasValue('authors_contract')){
                                $conference->setElement('authors_contract', $form_state->getValue('authors_contract'));
                        }
        
                        if(!empty($form_state->getValue('reviewsheet'))){
                                $conference->setElement('rpid', $form_state->getValue('reviewsheet'), false);
                        }
                        
                        if($form_state->hasValue('publish')){
                                $conference->setElement('published', $form_state->getValue('publish'));
                        }
                        
                        if(is_array($form_state->getValue('eoUsers'))){
                                
                                $eos = [];
                                
                                foreach($form_state->getValue('eoUsers') as $_){
                                        $_ = explode("|", $_);
                                        $_ = $_[0];
                                        $eos[] = base_convert ( trim ( $_ ), 36, 10 );
                                }
                                
                                $conference->setEditorialOffice($eos);
                        }
        
                        if(is_array($form_state->getValue('cmUsers'))){
                
                                $eos = [];
                
                                foreach($form_state->getValue('cmUsers') as $_){
                                        $_ = explode("|", $_);
                                        $_ = $_[0];
                                        $eos[] = base_convert ( trim ( $_ ), 36, 10 );
                                }
                
                                $conference->setConferenceManager($eos);
                        }
                        
                        if($conference->save()){
                                \Drupal::service('messenger')->addMessage('Changes successfully saved!');
                                $form_state->setRedirect('publisso_gold.conferencemanagement.overview');
                        }
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array|void
                 */
                public function validateForm (array &$form, FormStateInterface $form_state ) {
                        return $form;
                }
        }
