<?php
        
        
        namespace Drupal\publisso_gold\Form\Conferences;
        
        
        use Drupal\Component\Serialization\Json;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Language\LanguageManager;
        use Drupal\file\Entity\File;
        use Drupal\publisso_gold\Controller\Blob;
        use Drupal\publisso_gold\Controller\License;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\WorkflowSchemaManager;
        use Drupal\publisso_gold\Controller\Medium\Conference;
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class CompleteConference
         * @package Drupal\publisso_gold\Form\Conferences
         */
        class CompleteConference extends FormBase {
                
                private $texts;
                private $tools;
                private $setup;
                private $session;
        
                /**
                 * @return string
                 */
                public function getFormId () {
                        return 'CompleteConference';
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @param array $args
                 * @return array
                 */
                public function buildForm (array $form, FormStateInterface $form_state, $args = [] ) {
                        
                        if(!$form_state->has('cf_id')) $form_state->set('cf_id', $args['cf_id']);
                        $conference = ConferenceManager::getConference($form_state->get('cf_id'));
                        $schemas = [];
                        foreach(WorkflowSchemaManager::getSchemas([['schema_medium_type', serialize(['conference'])]]) as $k => $v){
                                $schemas[$k] = $v->schema_description;
                        }
                        
                        $license = new License($conference->getElement('license'));
                        
                        foreach(['texts', 'setup', 'tools'] as $_) $this->$_ = \Drupal::service('publisso_gold.'.$_);
                        $this->session = \Drupal::service('session');
                        
                        $allowedSubmissionTypes = [];
                        $availAwardSubmissionTypes = [];
                        if(!empty($conference->getElement('submission_types_available'))){
                                
                                foreach(($conference->getElement('submission_types_available')) as $v){
                                        $v = trim($v);
                                        $allowedSubmissionTypes[$v] = $availAwardSubmissionTypes[$v] = $v;
                                }
                        }
        
                        $allowedCategories = [];
                        if(!empty($conference->getElement('categories_available'))){
                
                                foreach(($conference->getElement('categories_available')) as $v){
                                        $v = trim($v);
                                        $allowedCategories[$v] = $v;
                                }
                        }
        
        
                        $availLanguages = \Drupal::service('language_manager')->getStandardLanguageList();
                        foreach($availLanguages as $k => $v){
                                $availLanguages[$k] = $v[1].' ('.$v[0].')';
                        }
                        
                        $form['headline'] = [
                                '#markup' => (string)$this->t($this->texts->get('conferencemanagement.complete.frm.headline')),
                                '#prefix' => '<h1>',
                                '#suffix' => '</h1>'
                        ];
                        
                        $form['title'] = [
                                '#markup' => $conference->getElement('title'),
                                '#prefix' => '<h2>',
                                '#suffix' => '</h2>'
                        ];
                        
                        $languages = [];
                        foreach(LanguageManager::getStandardLanguageList() as $langCode => $language){
                                $languages[$langCode] = $language[0].' ('.$language[1].')';
                        }
                        
                        $form['form'] = [
                                '#type' => 'container',
                                '#tree' => true,
                                
                                'title_abbr' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Title abbreviation'),
                                        '#maxlength' => 255,
                                        '#required' => true,
                                        '#default_value' => $conference->getElement('title_abbr')
                                ],
                                
                                'color' => [
                                        '#type' => 'color',
                                        '#title' => (string)$this->t('Theme color'),
                                        '#required' => true,
                                        '#attributes' => [
                                                'style' => [
                                                        'width: 50px; height: 30px;'
                                                ]
                                        ],
                                        '#default_value' => $conference->getElement('color') ?? '#7F96BE'
                                ],
                                
                                'ddc' => [
                                        '#type' => 'item',
                                        '#markup' => \Drupal::service('publisso_gold.tools')->getDDCByNumber($conference->getElement('ddc'), 'number').' '.\Drupal::service('publisso_gold.tools')->getDDCByNumber($conference->getElement('ddc')),
                                        '#title' => (string)$this->t('DDC').': '
                                ],

                                'license' => [
                                        '#type' => 'item',
                                        '#markup' => $license->getElement('name'),
                                        '#title' => (string)$this->t('License').': '
                                ],

                                'category' => [
                                        '#type' => 'item',
                                        '#markup' => \Drupal::service('publisso_gold.tools')->getCategory($conference->getElement('category')),
                                        '#title' => (string)$this->t('Subject').': '
                                ],
                                
                                'submission_procedure' => [
                                        '#type' => 'select',
                                        '#title' => (string)$this->t('Submission procedure'),
                                        '#required' => true,
                                        '#options' => $schemas,
                                        '#default_value' => $conference->getElement('wfschema')
                                ],
                                
                                'date_start' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Date conference start'),
                                        '#required' => true,
                                        '#default_value' => $conference->getElement('date_start'),
                                        '#prefix' => '<div class="form-wrapper-childs-left">'
                                ],

                                'date_end' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Date conference end'),
                                        '#required' => true,
                                        '#default_value' => $conference->getElement('date_end'),
                                        '#suffix' => '</div><br clear="all">'
                                ],

                                'date_submission_start' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Date submission start'),
                                        '#required' => true,
                                        '#default_value' => $conference->getElement('date_submission_start'),
                                        '#prefix' => '<div class="form-wrapper-childs-left">'
                                ],

                                'date_submission_end' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Date submission end'),
                                        '#required' => true,
                                        '#default_value' => $conference->getElement('date_submission_end'),
                                        '#suffix' => '</div><br clear="all">'
                                ],
                                
                                'logo' => [
                                        '#type' => 'managed_file',
                                        '#title' => (string)$this->t('Upload Logo'),
                                        '#required' => false,
                                        '#upload_validators' => array('file_validate_extensions' => array('jpeg jpg png gif')),
                                        '#default_value' => $conference->getElement('logo') ? [$conference->getElement('logo')] : null,
                                        '#preview' => true,
                                        '#upload_location' => 'public://publisso_gold/conferences/logos/'
                                ],
                                
                                'banner' => [
                                        '#type' => 'managed_file',
                                        '#title' => (string)$this->t('Upload Banner'),
                                        '#required' => false,
                                        '#upload_validators' => array('file_validate_extensions' => array('jpeg jpg png gif')),
                                        '#default_value' => $conference->getElement('banner') ? [$conference->getElement('banner')] : null,
                                        '#preview' => true,
                                        '#upload_location' => 'public://publisso_gold/conferences/banners/'
                                ],
                                
                                'city' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('City'),
                                        '#required' => true,
                                        '#maxlength' => 255,
                                        '#default_value' => $conference->getElement('city')
                                ],
                                
                                'country' => [
                                        '#type' => 'select',
                                        '#title' => (string)$this->t('Country'),
                                        '#required' => true,
                                        '#options' => \Drupal::service('country_manager')->getStandardList(),
                                        '#default_value' => $conference->getElement('country')
                                ],

                                'venue' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Conference venue'),
                                        '#required' => true,
                                        '#maxlength' => 255,
                                        '#default_value' => $conference->getElement('venue')
                                ],
                                
                                'venue_info' => [
                                        '#type' => 'textarea',
                                        '#title' => (string)$this->t('Venue information'),
                                        '#default_value' => $conference->getElement('venue_info')
                                ],

                                'society' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Inviting society'),
                                        '#required' => true,
                                        '#maxlength' => 255,
                                        '#default_value' => $conference->getElement('inviting_society')
                                ],
                                
                                'society_info' => [
                                        '#type' => 'textarea',
                                        '#title' => (string)$this->t('Inviting society information'),
                                        '#default_value' => $conference->getElement('inviting_society_info')
                                ],
                                
                                'language' => [
                                        '#type' => 'select',
                                        '#title' => (string)$this->t('Language'),
                                        '#required' => true,
                                        '#options' => $availLanguages,
                                        '#default_value' => $conference->getElement('language')
                                ],
                                
                                'about' => [
                                        '#type' => 'textarea',
                                        '#title' => (string)$this->t('About'),
                                        '#required' => true,
                                        '#attributes' => [
                                                'class'         => [
                                                        \Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
                                                ]
                                        ],
                                        '#default_value' => $conference->getElement('about')
                                ],
                                
                                'additional_info' => [
                                        '#type' => 'details',
                                        '#title' => (string)$this->t('Additional information (submission guidelines, imprint)'),
                                        'imprint' => [
                                                '#type' => 'textarea',
                                                '#title' => (string)$this->t('Imprint'),
                                                '#required' => false,
                                                '#attributes' => [
                                                        'class'         => [
                                                                \Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
                                                        ]
                                                ],
                                                '#default_value' => $conference->getElement('imprint')
                                        ],
                                        'manuscript_guidelines' => [
                                                '#type' => 'textarea',
                                                '#title' => (string)$this->t('Manuscript guidelines'),
                                                '#required' => false,
                                                '#attributes' => [
                                                        'class'         => [
                                                                \Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
                                                        ]
                                                ],
                                                '#default_value' => $conference->getElement('manuscript_guidelines')
                                        ]
                                ],
                                
                                'allow_multilingual_submissions' => [
                                        '#type' => 'checkbox',
                                        '#title' => (string)$this->t('Allow multilingual submissions'),
                                        '#default_value' => $conference->getElement('allow_multilingual_submissions')
                                ],
                        
                                'allowed_languages' => [
                                        '#type' => 'select',
                                        '#title' => (string)$this->t('Allowed translations'),
                                        '#options' => $languages,
                                        '#multiple' => true,
                                        '#states' => [
                                                'visible' => [
                                                        ':input[name="form[allow_multilingual_submissions]"]' => ['checked' => true],
                                                ],
                                                'required' => [
                                                        ':input[name="form[allow_multilingual_submissions]"]' => ['checked' => true],
                                                ]
                                        ],
                                        '#default_value' => $conference->getElement('allowed_submission_languages')
                                ],
                                
                                'field-translations' => [
                                        '#type' => 'fieldset',
                                        '#title' => (string)$this->t('Field-translation settings'),
                                        '#states' => [
                                                'visible' => [
                                                        ':input[name="form[allow_multilingual_submissions]"]' => ['checked' => true],
                                                ]
                                        ],
                                        'fields' => [
                                                '#type' => 'table',
                                                '#header' => [
                                                        (string)$this->t('Field'),
                                                        (string)$this->t('Translation')
                                                ],
                                                'title' => [
                                                        'data' => ['#markup' => (string)$this->t('Title')],
                                                        'translation' => [
                                                                '#type' => 'select',
                                                                '#options' => [
                                                                        'disabled' => (string)$this->t('Disabled'),
                                                                        'mandatory' => (string)$this->t('Mandatory'),
                                                                        'optional' => (string)$this->t('Optional'),
                                                                ],
                                                                '#default_value' => $conference->getElement('field_translations')['title'] ?? null
                                                        ]
                                                ],
                                                'keywords' => [
                                                        'data' => ['#markup' => (string)$this->t('Keywords')],
                                                        'translation' => [
                                                                '#type' => 'select',
                                                                '#options' => [
                                                                        'disabled' => (string)$this->t('Disabled'),
                                                                        'mandatory' => (string)$this->t('Mandatory'),
                                                                        'optional' => (string)$this->t('Optional'),
                                                                ],
                                                                '#default_value' => $conference->getElement('field_translations')['keywords'] ?? null
                                                        ]
                                                ],
                                                'text' => [
                                                        'data' => ['#markup' => (string)$this->t('Text/References')],
                                                        'translation' => [
                                                                '#type' => 'select',
                                                                '#options' => [
                                                                        'disabled' => (string)$this->t('Disabled'),
                                                                        'mandatory' => (string)$this->t('Mandatory'),
                                                                        'optional' => (string)$this->t('Optional'),
                                                                ],
                                                                '#default_value' => $conference->getElement('field_translations')['text'] ?? null
                                                        ]
                                                ],
                                                'coi' => [
                                                        'data' => ['#markup' => (string)$this->t('Conflict of interest')],
                                                        'translation' => [
                                                                '#type' => 'select',
                                                                '#options' => [
                                                                        'disabled' => (string)$this->t('Disabled'),
                                                                        'mandatory' => (string)$this->t('Mandatory'),
                                                                        'optional' => (string)$this->t('Optional'),
                                                                ],
                                                                '#default_value' => $conference->getElement('field_translations')['coi'] ?? null
                                                        ]
                                                ]
                                        ]
                                ],
                                
                                'review_type' => [
                                        '#type' => 'select',
                                        '#title' => (string)$this->t('Review type'),
                                        '#options' => [
                                                'double blind' => (string)$this->t('Double blind'),
                                                'open' => (string)$this->t('Open'),
                                                'single blind' => (string)$this->t('Single blind')
                                        ],
                                        '#default_value' => $conference->getElement('review_type')
                                ],
                                
                                'funding' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Funding freetext'),
                                        '#maxlength' => 500,
                                        '#default_value' => $conference->getElement('funding_text')
                                ],
                                
                                'wrapper_submission_types' => [
                                        '#type' => 'fieldset',
                                        '#title' => (string)$this->t('Allowed submission-types'),
                                        '#prefix' => '<div id="wrapper-submission-types">',
                                        '#suffix' => '</div><br>',
                                        'submission_types' => [],
                                        'actions' => [
                                                '#type' => 'actions',
                                                'del' => [],
                                                'add' => [
                                                        '#type' => 'submit',
                                                        '#value' => (string)$this->t('Add submission-type'),
                                                        '#submit' => ['::addSubmissionType'],
                                                        '#limit_validation_errors' => [],
                                                        '#ajax' => [
                                                                'wrapper' => 'wrapper-submission-types',
                                                                'callback' => '::addSubmissionTypeCallback',
                                                                'method' => 'html'
                                                        ]
                                                ]
                                        ]
                                ],
                                
                                'wrapper_categories' => [
                                        '#type' => 'fieldset',
                                        '#title' => (string)$this->t('Allowed categories'),
                                        '#prefix' => '<div id="wrapper-categories">',
                                        '#suffix' => '</div><br>',
                                        '#categories' => [],
                                        'actions' => [
                                                '#type' => 'actions',
                                                'del' => [],
                                                'add' => [
                                                        '#type' => 'submit',
                                                        '#value' => (string)$this->t('Add category'),
                                                        '#submit' => ['::addCategory'],
                                                        '#limit_validation_errors' => [],
                                                        '#ajax' => [
                                                                'wrapper' => 'wrapper-categories',
                                                                'callback' => '::addCategoryCallback',
                                                                'method' => 'html'
                                                        ]
                                                ]
                                        ]
                                ],
                                
                                'required_textblocks' => [
                                        '#type' => 'details',
                                        '#title' => (string)$this->t('Set required textblocks'),
                                        '#tree' => true
                                ],
                                
                                'allow_pictures' => [
                                        '#type' => 'checkbox',
                                        '#title' => (string)$this->t('Allow pictures'),
                                        '#default_value' => $conference->getElement('pictures_allowed')
                                ],

                                'allow_tables' => [
                                        '#type' => 'checkbox',
                                        '#title' => (string)$this->t('Allow tables'),
                                        '#default_value' => $conference->getElement('tables_allowed')
                                ],

                                'allow_references' => [
                                        '#type' => 'checkbox',
                                        '#title' => (string)$this->t('Allow references'),
                                        '#default_value' => $conference->getElement('references_allowed')
                                ],

                                'abstract_longversion' => [
                                        '#type' => 'checkbox',
                                        '#title' => (string)$this->t('Longversion of abstract required'),
                                        '#default_value' => $conference->getElement('abstract_longversion_required')
                                ],
                                
                                'max_references' => [
                                        '#type' => 'number',
                                        '#title' => (string)$this->t('Maximum number of references ( -1 for unlimited )'),
                                        '#min' => -1,
                                        '#max' => 9999,
                                        '#default_value' => $conference->getElement('max_references') ?? -1
                                ],
                                
                                'char_limit_abstract' => [
                                        '#type' => 'number',
                                        '#title' => (string)$this->t('Character-limit abstract'),
                                        '#min' => 0,
                                        '#max' => 99999,
                                        '#length' => 5,
                                        '#maxlength' => 5,
                                        '#default_value' => $conference->getElement('char_limit_abstract')
                                ],
                                
                                'time_review' => [
                                        '#type' => 'number',
                                        '#title' => (string)$this->t('Time for review (in days)'),
                                        '#min' => 0,
                                        '#max' => 999,
                                        '#length' => 3,
                                        '#maxlength' => 3,
                                        '#default_value' => $conference->getElement('time_for_review')
                                ],

                                'time_revision' => [
                                        '#type' => 'number',
                                        '#title' => (string)$this->t('Time for revision (in days)'),
                                        '#min' => 0,
                                        '#max' => 999,
                                        '#length' => 3,
                                        '#maxlength' => 3,
                                        '#default_value' => $conference->getElement('time_for_revision')
                                ],
                                
                                'pin' => [
                                        '#type' => 'textfield',
                                        '#title' => (string)$this->t('Submission PIN'),
                                        '#maxlength' => 10,
                                        '#default_value' => $conference->getElement('submission_pin')
                                ],
                                
                                'award' => [
                                        '#type' => 'checkbox',
                                        '#title' => (string)$this->t('Award'),
                                        '#default_value' => $conference->getElement('award')
                                ],
                                
                                'award_wrapper' => [
                                        '#type' => 'fieldset',
                                        '#title' => (string)$this->t('Award information'),
                                        '#states' => [
                                                'visible' => [
                                                        ':input[name="form[award]"]' =>['checked' => true]
                                                ]
                                        ],
                                        'award_for_submission_type' => [
                                                '#type' => 'select',
                                                '#title' => (string)$this->t('For submission type'),
                                                '#options' => $availAwardSubmissionTypes,
                                                '#states' => [
                                                        'required' => [
                                                                ':input[name="form[award]"]' =>['checked' => true]
                                                        ]
                                                ],
                                                '#default_value' => $conference->getElement('award_for_submission_type')
                                        ],
                                        'award_title' => [
                                                '#type' => 'textfield',
                                                '#title' => (string)$this->t('Title'),
                                                '#states' => [
                                                        'required' => [
                                                                ':input[name="form[award]"]' =>['checked' => true]
                                                        ]
                                                ],
                                                '#default_value' => $conference->getElement('award_title')
                                        ],
                                        'award_info' => [
                                                '#type' => 'textfield',
                                                '#title' => (string)$this->t('Additional information'),
                                                '#default_value' => $conference->getElement('award_info')
                                        ]
                                ],
                                
                                'roles-wrapper' => [
                                        '#type' => 'fieldset',
                                        '#collapsible' => true,
                                        '#collapsed' => true,
                                        '#title' => (string)$this->t('Roles'),
                                        'eic-wrapper' => [
                                                '#type' => 'fieldset',
                                                '#collapsible' => true,
                                                '#collapsed' => true,
                                                '#title' => (string)$this->t('Editor(s) in chief'),
                                                'eic-fields' => [
                                                        '#type' => 'container',
                                                        '#prefix' => '<div id="wrapper-eic">',
                                                        '#suffix' => '</div><br>',
                                                        'eic' => []
                                                ],
                                                'actions' => [
                                                        '#type' => 'actions',
                                                        'add' => [
                                                                '#type' => 'submit',
                                                                '#value' => (string)$this->t('Add EiC-field'),
                                                                '#submit' => ['::addEiC'],
                                                                '#ajax' => [
                                                                        'callback' => '::addEiCCallback',
                                                                        'progress' => [
                                                                                'type' => 'none'
                                                                        ],
                                                                        'wrapper' => 'wrapper-eic',
                                                                        'method' => 'html'
                                                                ]
                                                        ]
                                                ]
                                        ],
                                        'editor-wrapper' => [
                                                '#type' => 'fieldset',
                                                '#collapsible' => true,
                                                '#collapsed' => true,
                                                '#title' => (string)$this->t('Editor(s)'),
                                                'editor-fields' => [
                                                        '#type' => 'container',
                                                        '#prefix' => '<div id="wrapper-editor">',
                                                        '#suffix' => '</div><br>',
                                                        'editor' => []
                                                ],
                                                'actions' => [
                                                        '#type' => 'actions',
                                                        'add' => [
                                                                '#type' => 'submit',
                                                                '#value' => (string)$this->t('Add Editor-field'),
                                                                '#submit' => ['::addEditor'],
                                                                '#ajax' => [
                                                                        'callback' => '::addEditorCallback',
                                                                        'progress' => [
                                                                                'type' => 'none'
                                                                        ],
                                                                        'wrapper' => 'wrapper-editor',
                                                                        'method' => 'html'
                                                                ]
                                                        ]
                                                ]
                                        ],
                                        'reviewer-wrapper' => [
                                                '#type' => 'fieldset',
                                                '#collapsible' => true,
                                                '#collapsed' => true,
                                                '#title' => (string)$this->t('Reviewer(s)'),
                                                'reviewer-fields' => [
                                                        '#type' => 'container',
                                                        '#prefix' => '<div id="wrapper-reviewer">',
                                                        '#suffix' => '</div><br>',
                                                        'reviewer' => []
                                                ],
                                                'actions' => [
                                                        '#type' => 'actions',
                                                        'add' => [
                                                                '#type' => 'submit',
                                                                '#value' => (string)$this->t('Add Reviewer-field'),
                                                                '#submit' => ['::addReviewer'],
                                                                '#ajax' => [
                                                                        'callback' => '::addReviewerCallback',
                                                                        'progress' => [
                                                                                'type' => 'none'
                                                                        ],
                                                                        'wrapper' => 'wrapper-reviewer',
                                                                        'method' => 'html'
                                                                ]
                                                        ]
                                                ]
                                        ]
                                ],
                                
                                'email_signature' => [
                                        '#type' => 'textarea',
                                        '#title' => (string)$this->t('Email signature'),
                                        '#default_value' => $conference->getElement('email_signature')
                                ]
                        ];
        
                        if(!$form_state->has('cntSubmissionTypes')) {
                                $form_state->set('cntSubmissionTypes', 0);
                                $setSubmissionTypes = $conference->getElement('submission_types_allowed') ?? [];
                                $setRequiredTextblocks = $conference->getElement('required_textblocks') ?? [];
                                if (count($setSubmissionTypes)) $form_state->set('cntSubmissionTypes', count($setSubmissionTypes));
                        }
                        if($form_state->get('cntSubmissionTypes') > 0) {
                                
                                for ($i = 0; $i < $form_state->get('cntSubmissionTypes'); $i++) {
                                        $form['form']['wrapper_submission_types']['submission_types'][$i] = [
                                                '#type' => 'container',
                                                'submission_type' => [
                                                        '#type'     => 'textfield',
                                                        '#title' => (string)$this->t('Submission-type'),
                                                        '#required' => true,
                                                        '#prefix' => '<div class="col-sm-3">',
                                                        '#suffix' => '</div>',
                                                        '#default_value' => $setSubmissionTypes[$i] ?? null
                                                ],
                                                'required_textblocks' => [
                                                        '#type' => 'textfield',
                                                        '#title' => (string)$this->t('Required textblocks'),
                                                        '#placeholder' => (string)$this->t('Here you can enter necessary blocks of text separated by commas...'),
                                                        '#prefix' => '<div class="col-sm-9">',
                                                        '#suffix' => '</div>',
                                                        '#default_value' => implode(', ', $setRequiredTextblocks[$setSubmissionTypes[$i]] ?? [])
                                                ],
                                                '#prefix' => '<div class="row">',
                                                '#suffix' => '</div><br clear="all">'
                                        ];
                                }
                                
                                $form['form']['wrapper_submission_types']['actions']['del'] = [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Delete last submission-type'),
                                        '#submit' => ['::delSubmissionType'],
                                        '#limit_validation_errors' => [],
                                        '#ajax' => [
                                                'wrapper' => 'wrapper-submission-types',
                                                'callback' => '::delSubmissionTypeCallback',
                                                'method' => 'html'
                                        ]
                                ];
                        }
        
                        if(!$form_state->has('cntCategories')) {
                                $form_state->set('cntCategories', 0);
                                $setCategories = $conference->getElement('categories_allowed') ?? [];
                                if (count($setCategories)) $form_state->set('cntCategories', count($setCategories));
                        }
                        
                        if($form_state->get('cntCategories') > 0) {
                
                                for ($i = 0; $i < $form_state->get('cntCategories'); $i++) {
                                        $form['form']['wrapper_categories']['categories'][$i] = [
                                        
                                                '#type'     => 'textfield',
                                                '#required' => true,
                                                '#default_value' => $setCategories[$i] ?? null
                                        
                                        ];
                                }
                
                                $form['form']['wrapper_categories']['actions']['del'] = [
                                        '#type' => 'submit',
                                        '#value' => (string)$this->t('Delete last category'),
                                        '#submit' => ['::delCategory'],
                                        '#limit_validation_errors' => [],
                                        '#ajax' => [
                                                'wrapper' => 'wrapper-categories',
                                                'callback' => '::delCategoryCallback',
                                                'method' => 'html'
                                        ]
                                ];
                        }
                        
                        $eic = $conference->readEditorsInChief(); if(!$form_state->has('cntEiC')) $form_state->set('cntEiC', count($eic));
                        for($i = 0; $i < $form_state->get('cntEiC'); $i++){
                                
                                $value = $eic[$i] ? strtoupper(base_convert($eic[$i]->getId(), 10, 36)).' | '.$eic[$i]->profile->getReadableName('FL', ' ') : null;
                                $form['form']['roles-wrapper']['eic-wrapper']['eic-fields']['eic'][$i] = [
                                        '#type' => 'textfield',
                                        '#autocomplete_route_name'       => 'autocomplete.user',
                                        '#autocomplete_route_parameters' => array(),
                                        '#default_value' => $value
                                ];
                        }
        
                        $editor = $conference->readEditors(); if(!$form_state->has('cntEditor')) $form_state->set('cntEditor', count($editor));
                        for($i = 0; $i < $form_state->get('cntEditor'); $i++){
                
                                $value = $editor[$i] ? strtoupper(base_convert($editor[$i]->getId(), 10, 36)).' | '.$editor[$i]->profile->getReadableName('FL', ' ') : null;
                                $form['form']['roles-wrapper']['editor-wrapper']['editor-fields']['editor'][$i] = [
                                        '#type' => 'textfield',
                                        '#autocomplete_route_name'       => 'autocomplete.user',
                                        '#autocomplete_route_parameters' => array(),
                                        '#default_value' => $value
                                ];
                        }
        
                        $reviewer = $conference->readReviewers(); if(!$form_state->has('cntReviewer')) $form_state->set('cntReviewer', count($reviewer));
                        for($i = 0; $i < $form_state->get('cntReviewer'); $i++){
                
                                $value = $reviewer[$i] ? strtoupper(base_convert($reviewer[$i]->getId(), 10, 36)).' | '.$reviewer[$i]->profile->getReadableName('FL', ' ') : null;
                                $form['form']['roles-wrapper']['reviewer-wrapper']['reviewer-fields']['reviewer'][$i] = [
                                        '#type' => 'textfield',
                                        '#autocomplete_route_name'       => 'autocomplete.user',
                                        '#autocomplete_route_parameters' => array(),
                                        '#default_value' => $value
                                ];
                        }
        
                        $submissionTypesAvailable = [];
                        
                        if(!empty($conference->getElement('submission_types_available'))) $submissionTypesAvailable = ($conference->getElement('submission_types_available'));
                        
                        if(!count($submissionTypesAvailable)) unset($form['form']['required_textblocks']);
                        else{
                                $requiredTextblocks = ($conference->getElement('required_textblocks') ?? []);
                                foreach($submissionTypesAvailable as $type){
                                        $type = trim($type);
                                        $form['form']['required_textblocks'][$type] = [
                                                '#type' => 'textfield',
                                                '#title' => (string)$this->t('Submission-type: @type', ['@type' => $type]),
                                                '#description' => (string)$this->t('Enter multiple values separated by ";"'),
                                                '#default_value' => $requiredTextblocks[$type] ?? ''
                                        ];
                                }
                        }
                        
                        $form['submit'] = [
                                '#type'         => 'submit',
                                '#value'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.btn.submit')),
                                '#button_type'  => 'success',
                                '#suffix'       => '<br><br>'
                        ];
                        
                        $form['#cache'] = [
                                'max-age' => 0
                        ];
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @throws \Drupal\Core\Entity\EntityStorageException
                 */
                public function submitForm (array &$form, FormStateInterface $form_state ) {
                        
                        $conference = ConferenceManager::getConference($form_state->get('cf_id'));
                        
                        $simpleValues = [
                                'title_abbr'                     => 'title_abbr',
                                'color'                          => 'color',
                                'submission_procedure'           => 'wfschema',
                                'date_start'                     => 'date_start',
                                'date_end'                       => 'date_end',
                                'city'                           => 'city',
                                'country'                        => 'country',
                                'venue'                          => 'venue',
                                'society'                        => 'inviting_society',
                                'language'                       => 'language',
                                'about'                          => 'about',
                                'review_type'                    => 'review_type',
                                'funding'                        => 'funding_text',
                                'allow_pictures'                 => 'pictures_allowed',
                                'allow_tables'                   => 'tables_allowed',
                                'allow_references'               => 'references_allowed',
                                'abstract_longversion'           => 'abstract_longversion_required',
                                'char_limit_abstract'            => 'char_limit_abstract',
                                'time_revision'                  => 'time_for_revision',
                                'time_review'                    => 'time_for_review',
                                'pin'                            => 'submission_pin',
                                'email_signature'                => 'email_signature',
                                'date_submission_start'          => 'date_submission_start',
                                'date_submission_end'            => 'date_submission_end',
                                'venue_info'                     => 'venue_info',
                                'society_info'                   => 'inviting_society_info',
                                'allow_multilingual_submissions' => 'allow_multilingual_submissions',
                                'max_references'                 => 'max_references',
                                'award'                          => 'award'
                        ];
                        
                        //set flag "submission end"
                        if(strtotime($form_state->getValue('form')['date_submission_end']) >= strtotime(date('Y-m-d'))){
                                $conference->setElement('submissions_closed', 0, false);
                        }

                        $form_state->setValue('venue_info'  , substr($form_state->getValue('venue_info'  ), 0, 65535));
                        $form_state->setValue('society_info', substr($form_state->getValue('society_info'), 0, 65535));
                        
                        foreach($simpleValues as $FormField => $DBField){
                                if($form_state->getValue('form')[$FormField] !== ''){
                                        $conference->setElement($DBField, $form_state->getValue('form')[$FormField], false);
                                }
                                else{
                                        $conference->setElement($DBField, null, false);
                                }
                        }
                        
                        if(count($form_state->getValue('form')['wrapper_submission_types']['submission_types'])){
                                
                                $submissionTypes = [];
                                $requiredTextblocks = [];
                                
                                foreach($form_state->getValue('form')['wrapper_submission_types']['submission_types'] as $_){
                                        
                                        $submissionTypes[] = $_['submission_type'];
                                        $_rtb = array_filter(array_map('trim', explode(',', $_['required_textblocks'])));
                                        
                                        if(count($_rtb)){
                                                $requiredTextblocks[$_['submission_type']] = $_rtb;
                                        }
                                }
                                
                                if(count($submissionTypes)) $conference->setElement('submission_types_allowed', serialize($submissionTypes), false);
                                else $conference->setElement('submission_types_allowed', null, false);
                                
                                if(count($requiredTextblocks)) $conference->setElement('required_textblocks', serialize($requiredTextblocks), false);
                                else $conference->setElement('required_textblocks', null, false);
                        }
        
                        if(count($form_state->getValue('form')['wrapper_categories']['categories'])){
                                $conference->setElement('categories_allowed', serialize($form_state->getValue('form')['wrapper_categories']['categories']), false);
                        }
                        
                        if(array_key_exists('required_textblocks', $form_state->getValue('form')) && count($form_state->getValue('form')['required_textblocks'])){
                                $conference->setElement('required_textblocks', ($form_state->getValue('form')['required_textblocks']), false);
                        }
                        
                        if(!empty($form_state->getValue('form')['additional_info']['imprint'])){
                                $conference->setElement('imprint', $form_state->getValue('form')['additional_info']['imprint'], false);
                        }
        
                        if(!empty($form_state->getValue('form')['additional_info']['manuscript_guidelines'])){
                                $conference->setElement('manuscript_guidelines', $form_state->getValue('form')['additional_info']['manuscript_guidelines'], false);
                        }
                        
                        if(array_key_exists('allowed_languages', $form_state->getValue('form'))){
                                $conference->setElement('allowed_submission_languages', serialize(array_keys($form_state->getValue('form')['allowed_languages'])), false);
                        }
                        
                        if(array_key_exists('field-translations', $form_state->getValue('form'))){
                                
                                $fieldTranslations = [];
                                
                                foreach($form_state->getValue('form')['field-translations']['fields'] as $field => $translation){
                                        $fieldTranslations[$field] = $translation['translation'];
                                }
                                
                                $conference->setElement('field_translations', serialize($fieldTranslations), false);
                        }
                        
                        if(array_key_exists('award_wrapper', $form_state->getValue('form'))){
                                
                                foreach($form_state->getValue('form')['award_wrapper'] as $field => $value){
                                        $conference->setElement($field, $value, false);
                                }
                        }
                        
                        foreach(['banner', 'logo'] as $pic) {
                                
                                if ( count( $form_state->getValue( 'form' )[ $pic ] ) ) {
                
                                        $newFile = false;
                
                                        if ( $conference->getElement( $pic ) ) {
                        
                                                if ( $conference->getElement( $pic ) != $form_state->getValue( 'form' )[ $pic ][ 0 ] ) {
                                
                                                        $file = File::load( $conference->getElement( $pic ) );
                                                        $file->delete();
                                                        $newFile = true;
                                                }
                                        }
                                        else {
                                                $newFile = true;
                                        }
                
                                        if ( $newFile ) {
                        
                                                $file_id = $form_state->getValue( 'form' )[ $pic ][ 0 ];
                                                $file = File::load( $file_id );
                                                $file->setPermanent();
                                                $file->save();
                                                $conference->setElement( $pic, $file_id, false );
                                        }
                                }
                                elseif ( !empty( $conference->getElement( $pic ) ) ) {
                
                                        $file_id = $conference->getElement( $pic );
                                        $file = File::load( $file_id );
                                        $file->delete();
                                        $conference->setElement( $pic, null, false );
                                }
                        }
                        
                        //eic
                        if(array_key_exists('eic-fields', $form_state->getValue('form')['roles-wrapper']['eic-wrapper'])){
                                
                                $newEic = [];
                                foreach($form_state->getValue('form')['roles-wrapper']['eic-wrapper']['eic-fields']['eic'] as $_){
                                        if($_) {
                                                $_ = explode( "|", $_ );
                                                $_ = $_[ 0 ];
                                                $newEic[] = base_convert( trim( $_ ), 36, 10 );
                                        }
                                }
                                $conference->setEditorsInChief($newEic);
                        }
        
                        //editor
                        if(array_key_exists('editor-fields', $form_state->getValue('form')['roles-wrapper']['editor-wrapper'])){
                
                                $newEditor = [];
                                foreach($form_state->getValue('form')['roles-wrapper']['editor-wrapper']['editor-fields']['editor'] as $_){
                                        if($_) {
                                                $_ = explode( "|", $_ );
                                                $_ = $_[ 0 ];
                                                $newEditor[] = base_convert( trim( $_ ), 36, 10 );
                                        }
                                }
                                $conference->setEditors($newEditor);
                        }
        
                        if(array_key_exists('reviewer-fields', $form_state->getValue('form')['roles-wrapper']['reviewer-wrapper'])){
                
                                $newReviewer = [];
                                foreach($form_state->getValue('form')['roles-wrapper']['reviewer-wrapper']['reviewer-fields']['reviewer'] as $_){
                                        if($_) {
                                                $_ = explode( "|", $_ );
                                                $_ = $_[ 0 ];
                                                $newReviewer[] = base_convert( trim( $_ ), 36, 10 );
                                        }
                                }
                                $conference->setReviewers($newReviewer);
                        }
                        
                        //reviewer
                        
                        if($conference->getElement('edited') == -1) $conference->setElement('edited', 0, false);
                        if($conference->save()) \Drupal::service('messenger')->addMessage((string)$this->t('Changes successfully saved!'));
                        $form_state->setRedirect('publisso_gold.conferencemanagement.overview');
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array|void
                 */
                public function validateForm (array &$form, FormStateInterface $form_state ) {
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addEiC(array &$form, FormStateInterface $form_state){
                        $form_state->set('cntEiC', $form_state->get('cntEiC') + 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addEiCCallback(array &$form, FormStateInterface $form_state){
                        return $form['form']['roles-wrapper']['eic-wrapper']['eic-fields']['eic'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addEditor(array &$form, FormStateInterface $form_state){
                        $form_state->set('cntEditor', $form_state->get('cntEditor') + 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addEditorCallback(array &$form, FormStateInterface $form_state){
                        return $form['form']['roles-wrapper']['editor-wrapper']['editor-fields']['editor'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addReviewer(array &$form, FormStateInterface $form_state){
                        $form_state->set('cntReviewer', $form_state->get('cntReviewer') + 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addReviewerCallback(array &$form, FormStateInterface $form_state){
                        return $form['form']['roles-wrapper']['reviewer-wrapper']['reviewer-fields']['reviewer'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addSubmissionType(array &$form, FormStateInterface $form_state){
                        $form_state->set('cntSubmissionTypes', $form_state->get('cntSubmissionTypes') + 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addSubmissionTypeCallback(array &$form, FormStateInterface $form_state){
                        return $form['form']['wrapper_submission_types'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function delSubmissionType(array &$form, FormStateInterface $form_state){
                        
                        $form_state->set('cntSubmissionTypes', $form_state->get('cntSubmissionTypes') - 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function delSubmissionTypeCallback(array &$form, FormStateInterface $form_state){
                        return $form['form']['wrapper_submission_types'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addCategory(array &$form, FormStateInterface $form_state){
                        $form_state->set('cntCategories', $form_state->get('cntCategories') + 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addCategoryCallback(array &$form, FormStateInterface $form_state){
                        return $form['form']['wrapper_categories'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function delCategory(array &$form, FormStateInterface $form_state){
                
                        $form_state->set('cntCategories', $form_state->get('cntCategories') - 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function delCategoryCallback(array &$form, FormStateInterface $form_state){
                        return $form['form']['wrapper_categories'];
                }
        }
