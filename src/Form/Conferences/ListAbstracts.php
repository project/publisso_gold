<?php
        
        
        namespace Drupal\publisso_gold\Form\Conferences;
        
        
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Ajax\AjaxResponse;
        use Drupal\Core\Ajax\HtmlCommand;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Link;
        use Drupal\Core\Url;
        use Drupal\file\Entity\File;
        use Drupal\publisso_gold\Classes\Ajax\OpenUrlCommand;
        use Drupal\publisso_gold\Classes\Ajax\TimeoutHtmlCommand;
        use Drupal\publisso_gold\Controller\Manager\AbstractManager;
        use Drupal\publisso_gold\Controller\Manager\FunderManager;
        use Drupal\publisso_gold\Controller\Manager\UserManager;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Symfony\Component\HttpFoundation\Request;

        /**
         * Class ListAbstracts
         * @package Drupal\publisso_gold\Form\Conferences
         */
        class ListAbstracts extends FormBase {
                
                private $conference;
                private $filter;
        
                /**
                 * ListAbstracts constructor.
                 *
                 * @param Request $request
                 */
                public function __construct( Request $request){
                        $this->filter = [];
                        if($request->query->has('filter')) $this->filter = $request->get('filter');
                }
        
                /**
                 * @param ContainerInterface $container
                 *
                 * @return FormBase|static
                 */
                public static function create(ContainerInterface $container){
                        return new static(
                                $container->get('request_stack')->getCurrentRequest()
                        );
                }
        
                /**
                 * @return array
                 */
                private function parseFilter(){
                        $abstractFilter = [];
                        foreach($this->filter as $k => $v){
                                switch($k){
                                        case 'type':
                                                $abstractFilter[] = [
                                                        'ca_submission_type',
                                                        $v,
                                                ];
                                                break;
                        
                                        case 'category':
                                                $abstractFilter[] = [
                                                        'ca_category',
                                                        '%'.$v.'%',
                                                        'LIKE'
                                                ];
                                                break;
                                }
                        }
                        return $abstractFilter;
                }
                
                /**
                 * @param array $form
                 */
                private function setFormFilter( array &$form){
                        
                        $categoriesAvailable = [];
                        if($this->conference->getElement('categories_allowed')) $categoriesAvailable = $this->conference->getElement('categories_allowed');
                        
                        $form['filter-wrapper'] = [
                                '#type' => 'fieldset',
                                '#collapsible' => true,
                                '#collapsed' => !count($this->filter),
                                '#title' => (string)$this->t('Filter'),
                                'filter' => [
                                        '#type' => 'table',
                                        '#title' => 'Filter',
                                        [
                                                ['category' => [
                                                        '#title' => (string)$this->t('Category'),
                                                        '#type' => 'select',
                                                        '#options' => array_combine(array_map("trim", array_merge([''], $categoriesAvailable)), array_map("trim", array_merge([''], $categoriesAvailable))),
                                                        '#ajax' => [
                                                                'event' => 'change',
                                                                'callback' => '::applyFilter',
                                                                'progress' => [
                                                                        'type' => 'none'
                                                                ]
                                                        ],
                                                        '#limit_validation_errors' => [],
                                                        '#value' => $this->filter['category'] ?? null
                                                ], '#wrapper_attributes' => ['style' => ['width: 50%;']]],
                                                ['type' => [
                                                        '#title' => (string)$this->t('Type'),
                                                        '#type' => 'select',
                                                        '#options' => array_combine(array_map("trim", array_merge([''], $this->conference->getElement('submission_types_available') ?? [])), array_map("trim", array_merge([''], $this->conference->getElement('submission_types_available') ?? []))),
                                                        '#ajax' => [
                                                                'event' => 'change',
                                                                'callback' => '::applyFilter',
                                                                'progress' => [
                                                                        'type' => 'none'
                                                                ]
                                                        ],
                                                        '#limit_validation_errors' => [],
                                                        '#value' => $this->filter['type'] ?? null
                                                ]]
                                        ],
                                        '#attributes' => [
                                                'class' => [
                                                        'blank'
                                                ]
                                        ]
                                ]
                        ];
                }
        
                /**
                 * @return array
                 */
                private function getListTableHeaders() :array{
                        return [
                                ['data' => [
                                        '#type' => 'checkbox',
                                        '#name' => 'select_all',
                                        '#attributes' => [
                                                'style' => 'position: relative;'
                                        ]
                                ]],
                                ['data' => [
                                        '#markup' => (string)$this->t('Title')
                                ]],
                                ['data' => [
                                        '#markup' => (string)$this->t('Type')
                                ]],
                                ['data' => [
                                        '#markup' => (string)$this->t('Received')
                                ]],
                                ['data' => [
                                        '#markup' => ''
                                ]]
                        ];
                }
                
                /**
                 * @inheritDoc
                 */
                public function getFormId () {
                        return 'ListAbstracts';
                }
        
                /**
                 * @inheritDoc
                 */
                public function buildForm ( array $form, FormStateInterface $form_state, $args = []) {
                        
                        $currentUser = Publisso::currentUser();
                        if(!$form_state->has('conference')) $form_state->set('conference', $args['conference']);
                        if(!$form_state->has('conference')) return [];
                        $this->conference = $form_state->get('conference');
                        
                        $sessions = [
                                '' => '– '.(string)$this->t('Select session').' –'
                        ];
        
                        foreach($this->conference->getSessions() as $sessionID => $session){
                                $sessions[$sessionID] = implode('', array_fill(0, $session->depth, "····")).($session->day ?? $session->title);
                        }
                        
                        $this->setFormFilter($form);
                        
                        $form['table'] = [
                                '#type' => 'table',
                                '#empty' => (string)$this->t('No abstracts found...'),
                                '#header' => $this->getListTableHeaders()
                        ];
        
                        $sessions = ['' => '– '.(string)$this->t('Select session').' –'];
        
                        foreach($this->conference->getSessions() as $sessionID => $session){
                                $sessions[$sessionID] = implode('', array_fill(0, $session->depth, "····")).($session->day ?? $session->title);
                        }
                        
                        foreach(AbstractManager::getAbstractList($this->conference->getId(), ['ca_id'], $this->parseFilter(), 20) as $row){
                                
                                $id = $row->ca_id;
                                $abstract = AbstractManager::getAbstract($id);
                                $workflow = WorkflowManager::getItem($abstract->getElement('wfid'));
                                $user = UserManager::getUserFromID($abstract->getElement('created_by_uid'));
                                $checkbox = ['#markup' => ''];
                                
                                if(($workflow->userIsAssigned() || $currentUser->isPublisso() || $currentUser->isAdministrator()) && !($workflow->getElement('state') == 'published' || $workflow->getElement('state') == 'running submission')){
                                        $checkbox = [
                                                '#type' => 'checkbox',
                                                '#states' => [
                                                        'checked' => [
                                                                ':input[name="select_all"]' => ['checked' => true]
                                                        ]
                                                ],
                                                '#group' => 'checkboxes'
                                        ];
                                }
                                
                                $selectSession = [
                                        '#markup' => '&nbsp;',
                                        '#default_value' => $abstract->getSessionParent(),
                                        '#ajax' => [
                                                'callback' => '::setSession',
                                                'event' => 'change',
                                                'progress' => [
                                                        'type' => 'none'
                                                ],
                                                'wrapper' => 'message-select-'.$id,
                                                'method' => 'html'
                                        ],
                                        '#wrapper_attributes' => [
                                                'style' => 'display: inline; float: left; margin-right: 10px;'
                                        ]
                                ];
                                if(!$form_state->get('conference')->getElement('published')){
                                        $selectSession = [
                                                '#type' => 'select',
                                                '#options' => $sessions,
                                                '#default_value' => $abstract->getSessionParent(),
                                                '#ajax' => [
                                                        'callback' => '::setSession',
                                                        'event' => 'change',
                                                        'progress' => [
                                                                'type' => 'none'
                                                        ],
                                                        'wrapper' => 'message-select-'.$id,
                                                        'method' => 'html'
                                                ],
                                                '#wrapper_attributes' => [
                                                        'style' => 'display: inline; float: left; margin-right: 10px;'
                                                ]
                                        ];
                                }
                                
                                $form['table'][] = [
                                        'checked#'.$id => $checkbox,
                                        'title' => [
                                                '#markup' => $abstract->getElement('title'),
                                                '#prefix' => '<span class="fakeLink" data-toggle="collapse" data-target="[abstract=\''.$id.'\']">',
                                                '#suffix' => '</span>'
                                        ],
                                        'type' => [
                                                '#markup' => $this->t($abstract->getElement('submission_type'))
                                        ],
                                        'created' => [
                                                '#markup' => $abstract->getElement('created')
                                        ],
                                        'container-select-'.$id => [
                                                'abstract#'.$id => $selectSession,
                                                'message-select-'.$id => [
                                                        '#markup' => '',
                                                        '#prefix' => '<div id="message-select-'.$id.'">',
                                                        '#suffix' => '</div>',
                                                ],
                                                '#wrapper_attributes' => [
                                                        'style' => 'width: 250px;'
                                                ]
                                        ]
                                ];
                                
                                foreach(['keywords', 'category', 'authors_db', 'authors_add', 'corporations', 'attachments'] as $var) $$var = $abstract->getElement($var) ?? [];
                
                                $authors = [];
                                foreach (array_merge(${'authors_db'}, ${'authors_add'}) as $_){
                                        $authors[] = $_['firstname']. ' '. $_['lastname'].'; '.$_['affiliation'];
                                }
                
                                $fundingStr = '';
                                if($abstract->getElement('ctrlFunding')){
                        
                                        foreach($abstract->getFundings() as $funding){
                                
                                                $fundingStr .= (!empty($fundingStr) ? '<br>' : '');
                                                $fundingStr .= '<strong>'.(string)$this->t('Funding #@nr', ['@nr' => $funding->getElement('number')]).'</strong><br>';
                                
                                                switch($funding->getElement('type')){
                                                        case 'manual':
                                                                $fundingStr .= (string)$this->t('Funding-name: @name', ['@name' => $funding->getElement('funder_name')]).'<br>';
                                                                $fundingStr .= (string)$this->t('Funding-ID: @id', ['@id' => $funding->getElement('funder_id')]).'<br>';
                                                                break;
                                        
                                                        case 'stream':
                                                                $fundingStr .= (string)$this->t('Funder: @name', ['@name' => FunderManager::getNormalizedAndIndexedList()[$funding->funder()->getID()]]).'<br>';
                                                                $fundingStr .= (string)$this->t('Award-number: @nr', ['@nr' => $funding->getElement('funder_award_number')]).'<br>';
                                                                $fundingStr .= (string)$this->t('Award-uri: @uri', ['@uri' => $funding->getElement('funder_award_uri')]).'<br>';
                                                                $fundingStr .= (string)$this->t('Award-identifier: @identifier', ['@identifier' => $funding->getElement('funder_award_identifier')]).'<br>';
                                                                $fundingStr .= (string)$this->t('Award-title: @title', ['@title' => $funding->getElement('funder_award_title')]).'<br>';
                                                                $fundingStr .= (string)$this->t('Funding-stream: @stream', ['@stream' => $funding->getElement('funder_funding_stream')]).'<br>';
                                                                break;
                                                }
                                        }
                                }
                
                                $strAttachments = ''; $i = 1;
                                foreach(${'attachments'} as $_){
                        
                                        $file = File::load($_);
                                        if($file) {
                                
                                                $url = Url::fromRoute('publisso_gold.getPicture', ['type' => 'ul', 'id' => $_]);
                                                $url->setOptions(['attributes' => ['target' => '_blank']]);
                                                $strAttachments .= ( !empty( $strAttachments ) ? '<br>' : '' );
                                                $strAttachments .= Link::fromTextAndUrl( (string)$this->t( 'Attachment #@i', [ '@i' => $i++ ] ), $url)->toString() ;
                                        }
                                }
                
                                
                                $form['table'][] = [
                                        [
                                                'data' => [
                                                        'content' => [
                                                                '#type' => 'table',
                                                                '#rows' => [
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Creator')]],
                                                                          'width' => 200],
                                                                         ['data' => [
                                                                                 ['#markup' => $user->profile->getReadableName().' ('.$workflow->getElement('created_by_uid').')']]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Modified')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => $abstract->getElement('modified')]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Keywords')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => implode(', ', array_filter(${'keywords'}))]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Category')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => implode(', ', array_filter(${'category'}))]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Language')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => $this->t(\Drupal::languageManager()->getLanguage($abstract->getElement('language'))->getName())]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Author(s)')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => implode("<br>", $authors)]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Corporation(s)')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => implode("<br>", ${'corporations'})]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Funding(s)')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => $fundingStr]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Award')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => (string)$this->t($abstract->getElement('award') ? 'Yes' : 'No')]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Award-info')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => $abstract->getElement('award_info')]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Conflict of interest')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => $abstract->getElement('conflict_of_interest')]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Attachments')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => $strAttachments]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Main text')]]
                                                                         ],
                                                                         ['data' => [[
                                                                                             '#type' => 'container',
                                                                                             'prefix' => [
                                                                                                     '#markup' => '<span class="fakeLink" data-toggle="collapse" data-target="div[id=\'main_text_'.$id.'\']">'.((string)$this->t('Click to expand/collapse')).'</span>'
                                                                                             ],
                                                                                             'main_text' => [
                                                                                                     '#type' => 'inline_template',
                                                                                                     '#template' => $abstract->getElement('main_text'),
                                                                                                     '#prefix' => '<div class="collapse" id="main_text_'.$id.'">',
                                                                                                     '#suffix' => '</div>'
                                                                                             ]
                                                                                     ]]
                                                                         ]],
                                                                ],
                                                                '#attributes' => [
                                                                        'class' => ['rwTableInline']
                                                                ]
                                                        ],
                                                        '#prefix' => '<div abstract="'.$id.'" class="collapse"',
                                                        '#suffix' => '<div>'
                                                ],
                                                '#wrapper_attributes' => [
                                                        'colspan' => count($form['table']['#header']),
                                                        'style' => 'padding: 0px 5px 0px 5px; margin: 0; border: none;'
                                                ]
                                        ]
                                ];
                        }
                        
                        $form['pager'] = ['#type' => 'pager'];
                        
                        $form[ 'action' ] = [
                                '#type'     => 'select',
                                '#title'    => (string)$this->t( 'Select action' ),
                                '#required' => true,
                                '#options'  => [
                                        'return_to_author' => (string)$this->t('Return to author'),
                                        'assign_eo' => (string)$this->t('Assign editorial office')
                                ]
                        ];
                        
                        $options = [];
                        foreach($this->conference->readEditorialOffice() as $user){
                                $options[$user->getId()] = $user->profile->getReadableName();
                        }
                        
                        $form['eic'] = [
                                '#type' => 'select',
                                '#multiple' => true,
                                '#options' => $options,
                                '#states' => [
                                        'visible' => [
                                                ':input[name="action"]' => ['value' => 'assign_eo']
                                        ],
                                        'required' => [
                                                ':input[name="action"]' => ['value' => 'assign_eo']
                                        ]
                                ],
                                '#title' => (string)$this->t('Select users for editorial office')
                        ];
                        
                        $form['submit'] = [
                                '#type' => 'submit',
                                '#value' => 'Submit'
                        ];
                        
                        $form['#cache'] = [
                                'max-age' => 0
                        ];
                        
                        return $form;
                }
        
                /**
                 * @inheritDoc
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
        
                        foreach($form_state->getValue('table') as $row){
                                
                                foreach($row as $k => $v){
                                        
                                        if(preg_match('/^checked#(\d+)$/', $k, $matches)){
                                                
                                                $id = $matches[1];
                                                $abstract = AbstractManager::getAbstract($id);
                                                $workflow = WorkflowManager::getItem($abstract->getElement('wfid'));
                                
                                                switch($form_state->getValue('action')){
                                        
                                                        case 'return_to_author':
                                                                $workflow->newSchemaState(2);
                                                                break;
                                        
                                                        case 'assign_eo':
                                                                $workflow->setElement('assigned_to_eo', implode(',', array_filter($form_state->getValue('eic'))));
                                                                $workflow->newSchemaState(4);
                                                                break;
                                                }
                                                
                                                $workflow->unlock();
                                        }
                                }
                        }
                        
                        return $form;
                }
        
                /**
                 * @inheritDoc
                 */
                public function setSession ( array &$form, FormStateInterface $form_state ) {
        
                        $te = $form_state->getTriggeringElement();
                        $name = $te['#name'];
                        $value = !empty($te['#value']) ? $te['#value'] : null;
                        $error = false;
                        
                        if(preg_match('/^.+?(abstract#(\d+)).+$/', $name, $matches)){
                                
                                $ca_id = $matches[2];
                                $abstract = AbstractManager::getAbstract($ca_id);
                                
                                if($value) {
                                        if($abstract->getElement('session')){
                                                
                                                if ( !$abstract->reassignSession( $value ) ) {
                                                        Publisso::log( (string)$this->t( 'Cant set session-id for abstract #@id', [ '@id' => $ca_id ] ) );
                                                        $error = true;
                                                }
                                        }
                                        else {
                                                if ( !$abstract->assignSession( $value ) ) {
                                                        Publisso::log( (string)$this->t( 'Cant set session-id for abstract #@id', [ '@id' => $ca_id ] ) );
                                                        $error = true;
                                                }
                                        }
                                }
                                else{
                                        if ( !$abstract->unassignSession( $value ) ) {
                                                Publisso::log( (string)$this->t( 'Cant remove session-id for abstract #@id', [ '@id' => $ca_id ] ) );
                                                $error = true;
                                        }
                                }
                        }
                        else{
                                Publisso::log((string)$this->t('Cant find abstract from field "@name"', ['@name' => $name]));
                                $error = true;
                        }
                        
                        
                        $response = new AjaxResponse();
                        
                        if(!$error) {
                                $response->addCommand( new HtmlCommand( '#message-select-'.$ca_id, '<span style="font-weight: bold; color: darkgreen; font-size: 20px;">√</span>' ) );
                                $response->addCommand( new TimeoutHtmlCommand( '#message-select-'.$ca_id, 2000 ) );
                        }
                        else{
                                $message = '<span style="font-weight: bold; color: darkred; font-size: 20px;">X</span>';
                                $response->addCommand( new HtmlCommand( '#message-select-'.$ca_id, $message ) );
                                $response->addCommand( new TimeoutHtmlCommand( '#message-select-'.$ca_id, 5000 ) );
                        }
                        
                        return $response;
                }
        
                /**
                 * @param array $array
                 * @param FormStateInterface $form_state
                 * @return AjaxResponse
                 */
                public function applyFilter(array &$array, FormStateInterface $form_state){
                        
                        $request = \Drupal::requestStack()->getCurrentRequest();
                        $params  = [];
                        
                        $filter = null;
                        
                        foreach($form_state->getUserInput()['filter'] as $row){
                                
                                foreach($row as $col){
                                        
                                        foreach($col as $name => $value){
                                                
                                                if(!empty($value)) $filter[$name] = $value;
                                        }
                                }
                        }
                        
                        if(count($filter)) $params['filter'] = $filter;
                        $query = UrlHelper::buildQuery($params);
                        
                        $response = new AjaxResponse();
                        $response->addCommand(new OpenUrlCommand(null, $request->getPathInfo().(!!$query ? "?$query" : '')));
                        return $response;
                }
        }