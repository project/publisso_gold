<?php
        
        
        namespace Drupal\publisso_gold\Form\Conferences;
        
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Classes\AuthorsContract;
        use Drupal\publisso_gold\Controller\Manager\AuthorsContractManager;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\ReviewsheetManager;
        use Drupal\publisso_gold\Controller\Medium\Conference;

        /**
         * Class AddConference
         * @package Drupal\publisso_gold\Form\Conferences
         */
        class AddConference extends FormBase {
                
                private $texts;
                private $tools;
                private $setup;
                private $session;
        
                /**
                 * @return string
                 */
                public function getFormId () {
                        return 'AddConference';
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function buildForm (array $form, FormStateInterface $form_state ) {
        
                        foreach(['texts', 'setup', 'tools'] as $_) $this->$_ = \Drupal::service('publisso_gold.'.$_);
                        $this->session = \Drupal::service('session');
        
                        $availDDC = \Drupal::service('publisso_gold.tools')->getDDCs();
                        foreach($availDDC as $k => $v){
                
                                if(preg_match('/^(\d+)\s+(.+)$/', $v, $matches)){
                                        $availDDC[$matches[1]] = $matches[0];
                                }
                
                                unset($availDDC[$k]);
                        }
        
                        $availLicenses = \Drupal::service('publisso_gold.tools')->getLicenses();
                        $availCategories = \Drupal::service('publisso_gold.tools')->getCategories();
        
                        $availLanguages = \Drupal::service('language_manager')->getStandardLanguageList();
                        foreach($availLanguages as $k => $v){
                                $availLanguages[$k] = $v[1].' ('.$v[0].')';
                        }
        
                        $rps = [];
        
                        foreach (ReviewsheetManager::getList() as $rp){
                                $rps[$rp->rp_id] = $rp->rp_name;
                        }
                        
                        $form['headline'] = [
                                '#markup' => (string)$this->t($this->texts->get('conferencemanagement.add.frm.headline')),
                                '#prefix' => '<h1>',
                                '#suffix' => '</h1>'
                        ];
                        
                        $form['title'] = [
                                '#type'         => 'textfield',
                                '#title'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.field.title')),
                                '#maxlength'    => 500,
                                '#required'     => true
                        ];
        
                        $form['reviewsheet'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('Select reviewsheet'),
                                '#options' => $rps,
                                '#required' => true
                        ];
                        
                        $form['ddc'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('DDC'),
                                '#required' => true,
                                '#options' => $availDDC
                        ];
        
                        $form['license'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('License'),
                                '#required' => true,
                                '#options' => $availLicenses
                        ];
        
                        $form['category'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('Subject'),
                                '#required' => true,
                                '#options' => $availCategories
                        ];
                        
                        $form['type'] = [
                                '#type' => 'radios',
                                '#title' => (string)$this->t('Page type'),
                                '#required' => true,
                                '#options' => [
                                        'periodical' => (string)$this->t('periodical'),
                                        'sporadic' => (string)$this->t('sporadic')
                                ]
                        ];
                        
                        $form['title_series'] = [
                                '#type' => 'textfield',
                                '#title' => (string)$this->t('Series Title'),
                                '#states' => [
                                        'visible'     => [':input[name="type"]' => array('value' => 'periodical')],
                                        'invisible'   => [':input[name="type"]' => array('!value' => 'periodical')],
                                ]
                        ];
                        
                        $acs = [];
                        foreach(AuthorsContractManager::getNameList() as $_){
                                $ac = AuthorsContract::load($_);
                                $acs[$ac->getName()] = $ac->getName();
                        }
                        
                        $form['authors_contract'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('Authors contract'),
                                '#options' => $acs
                        ];
                        
                        if(!$form_state->has('cntEO')) $form_state->set('cntEO', 0);
                        if(!$form_state->has('cntEO')) $form_state->set('cntCM', 0);
                        
                        $form['roles'] = [
                                '#type'         => 'fieldset',
                                '#title'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.fieldset.roles.title')),
                                'editorial_office'      => [
                                        '#type'         => 'fieldset',
                                        '#title'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.fieldset.roles.fieldset.editorial_office.title')),
                                        'content'       => [
                                                '#type'         => 'container',
                                                '#prefix'       => '<div id="wrapper-eo">',
                                                '#suffix'       => '</div>',
                                                'eoUsers'       => [
                                                        '#tree'         => true
                                                ],
                                                'add'           => [
                                                        '#type'         => 'submit',
                                                        '#value'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.fieldset.roles.fieldset.editorial_office.btn.add')),
                                                        '#submit'       => ['::addEO'],
                                                        '#ajax'         => [
                                                                'callback'      => '::addEOCallback',
                                                                'wrapper'       => 'wrapper-eo'
                                                        ],
                                                        '#limit_validation_errors' => []
                                                ]
                                        ],
                                        
                                ],
                                'conference_manager'      => [
                                        '#type'         => 'fieldset',
                                        '#title'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.fieldset.roles.fieldset.conference_manager.title')),
                                        'content'       => [
                                                '#type'         => 'container',
                                                '#prefix'       => '<div id="wrapper-cm">',
                                                '#suffix'       => '</div>',
                                                'cmUsers'       => [
                                                        '#tree'         => true
                                                ],
                                                'add'           => [
                                                        '#type'         => 'submit',
                                                        '#value'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.fieldset.roles.fieldset.conference_manager.btn.add')),
                                                        '#submit'       => ['::addCM'],
                                                        '#ajax'         => [
                                                                'callback'      => '::addCMCallback',
                                                                'wrapper'       => 'wrapper-cm'
                                                        ],
                                                        '#limit_validation_errors' => []
                                                ]
                                        ],

                                ]
                        ];
                        
                        for($nr = 0; $nr < $form_state->get('cntEO'); $nr++){
                                $form['roles']['editorial_office']['content']['eoUsers'][$nr] = [
                                        '#type'                          => 'textfield',
                                        '#autocomplete_route_name'       => 'autocomplete.user',
                                        '#autocomplete_route_parameters' => array(),
                                ];
                        }
        
                        for($nr = 0; $nr < $form_state->get('cntCM'); $nr++){
                                $form['roles']['conference_manager']['content']['cmUsers'][$nr] = [
                                        '#type'                          => 'textfield',
                                        '#autocomplete_route_name'       => 'autocomplete.user',
                                        '#autocomplete_route_parameters' => array(),
                                ];
                        }
                        
                        $form['submit'] = [
                                '#type'         => 'submit',
                                '#value'        => (string)$this->t($this->texts->get('conferencemanagement.add.frm.btn.submit')),
                                '#button_type'  => 'success',
                                '#suffix'       => '<br><br>'
                        ];
                        
                        $form['#cache'] = [
                                'max-age' => 0
                        ];
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addEO (array &$form, FormStateInterface $form_state ) {
                        $form_state->set('cntEO', $form_state->get('cntEO') + 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addEOCallback (array &$form, FormStateInterface $form_state ) {
                        return $form['roles']['editorial_office']['content'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addCM (array &$form, FormStateInterface $form_state ) {
                        $form_state->set('cntCM', $form_state->get('cntCM') + 1);
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addCMCallback (array &$form, FormStateInterface $form_state ) {
                        return $form['roles']['conference_manager']['content'];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @throws \Exception
                 */
                public function submitForm (array &$form, FormStateInterface $form_state ) {
                        
                        $conference = ConferenceManager::_create();
                        
                        $conference->setElement('title', $form_state->getValue('title'), false);
                        
                        if(!empty($form_state->getValue('category'))){
                                $conference->setElement('category', ($form_state->getValue('category')), false);
                        }
        
                        if(!empty($form_state->getValue('license'))){
                                $conference->setElement('license', $form_state->getValue('license'), false);
                        }
        
                        if(!empty($form_state->getValue('ddc'))){
                                $conference->setElement('ddc', $form_state->getValue('ddc'), false);
                        }
        
                        if(!empty($form_state->getValue('authors_contract'))){
                                $conference->setElement('authors_contract', $form_state->getValue('authors_contract'), false);
                        }
        
                        if(!empty($form_state->getValue('reviewsheet'))){
                                $conference->setElement('rpid', $form_state->getValue('reviewsheet'), false);
                        }
                        
                        if(!empty($form_state->getValue('type'))){
                                $conference->setElement('pagetype', $form_state->getValue('type'), false);
                
                                if($conference->getElement('pagetype') != 'periodical'){
                                        $conference->setElement('title_series', null, false);
                                }
                                elseif(!empty($form_state->getValue('title_series'))){
                                        $conference->setElement('title_series', $form_state->getValue('title_series'), false);
                                }
                        }
                        
                        if(is_array($form_state->getValue('eoUsers'))){
                                
                                $eos = [];
                                
                                foreach($form_state->getValue('eoUsers') as $_){
                                        $_ = explode("|", $_);
                                        $_ = $_[0];
                                        $eos[] = base_convert ( trim ( $_ ), 36, 10 );
                                }
                                
                                $conference->setEditorialOffice($eos);
                        }
        
                        if(is_array($form_state->getValue('cmUsers'))){
                
                                $eos = [];
                
                                foreach($form_state->getValue('cmUsers') as $_){
                                        $_ = explode("|", $_);
                                        $_ = $_[0];
                                        $eos[] = base_convert ( trim ( $_ ), 36, 10 );
                                }
                
                                $conference->setConferenceManager($eos);
                        }
        
                        if($conference->save()){
                                $this->messenger()->addMessage('Changes successfully saved!');
                                $form_state->setRedirect('publisso_gold.conferencemanagement.overview');
                        }
                        else{
                                $this->messenger()->addError((string)$this->t('Failed to store data! See error-logs for details!'));
                                $form_state->setRebuild();
                        }
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array|void
                 */
                public function validateForm (array &$form, FormStateInterface $form_state ) {
                        return $form;
                }
        }
