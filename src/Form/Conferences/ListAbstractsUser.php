<?php
        
        
        namespace Drupal\publisso_gold\Form\Conferences;
        
        
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Ajax\AjaxResponse;
        use Drupal\Core\Ajax\HtmlCommand;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Link;
        use Drupal\Core\Url;
        use Drupal\file\Entity\File;
        use Drupal\publisso_gold\Classes\Ajax\OpenUrlCommand;
        use Drupal\publisso_gold\Classes\Ajax\TimeoutHtmlCommand;
        use Drupal\publisso_gold\Controller\Manager\AbstractManager;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\FunderManager;
        use Drupal\publisso_gold\Controller\Manager\UserManager;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\Workflow\AbstractWorkflow;
        use Drupal\publisso_gold\Controller\Workflow\Workflow;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Symfony\Component\HttpFoundation\Request;

        /**
         * Class ListAbstractsUser
         * @package Drupal\publisso_gold\Form\Conferences
         */
        class ListAbstractsUser extends FormBase {
                
                private $conference;
                private $filter;
        
                /**
                 * ListAbstracts constructor.
                 *
                 * @param Request $request
                 */
                public function __construct( Request $request){
                        $this->filter = [];
                        if($request->query->has('filter')) $this->filter = $request->get('filter');
                }
        
                /**
                 * @param ContainerInterface $container
                 *
                 * @return FormBase|static
                 */
                public static function create(ContainerInterface $container){
                        return new static(
                                $container->get('request_stack')->getCurrentRequest()
                        );
                }
        
                /**
                 * @return array
                 */
                private function parseFilter(){
                        $abstractFilter = [];
                        foreach($this->filter as $k => $v){
                                switch($k){
                                        case 'type':
                                                $abstractFilter[] = [
                                                        'ca_submission_type',
                                                        $v,
                                                ];
                                                break;
                        
                                        case 'category':
                                                $abstractFilter[] = [
                                                        'ca_category',
                                                        '%'.$v.'%',
                                                        'LIKE'
                                                ];
                                                break;
                                }
                        }
                        return $abstractFilter;
                }
                
                /**
                 * @param array $form
                 */
                private function setFormFilter( array &$form){
                        
                        $form['filter-wrapper'] = [
                                '#type' => 'fieldset',
                                '#collapsible' => true,
                                '#collapsed' => !count($this->filter),
                                '#title' => (string)$this->t('Filter'),
                                'filter' => [
                                        '#type' => 'table',
                                        '#title' => 'Filter',
                                        [
                                                ['category' => [
                                                        '#title' => (string)$this->t('Category'),
                                                        '#type' => 'select',
                                                        '#options' => array_combine(array_map("trim", array_merge([''], $this->conference->getElement('categories_allowed'))), array_map("trim", array_merge([''], $this->conference->getElement('categories_allowed')))),
                                                        '#ajax' => [
                                                                'event' => 'change',
                                                                'callback' => '::applyFilter',
                                                                'progress' => [
                                                                        'type' => 'none'
                                                                ]
                                                        ],
                                                        '#limit_validation_errors' => [],
                                                        '#value' => $this->filter['category'] ?? null
                                                ], '#wrapper_attributes' => ['style' => ['width: 50%;']]],
                                                ['type' => [
                                                        '#title' => (string)$this->t('Type'),
                                                        '#type' => 'select',
                                                        '#options' => array_combine(array_map("trim", array_merge([''], $this->conference->getElement('submission_types_allowed'))), array_map("trim", array_merge([''], $this->conference->getElement('submission_types_allowed')))),
                                                        '#ajax' => [
                                                                'event' => 'change',
                                                                'callback' => '::applyFilter',
                                                                'progress' => [
                                                                        'type' => 'none'
                                                                ]
                                                        ],
                                                        '#limit_validation_errors' => [],
                                                        '#value' => $this->filter['type'] ?? null
                                                ]]
                                        ],
                                        '#attributes' => [
                                                'class' => [
                                                        'blank'
                                                ]
                                        ]
                                ]
                        ];
                }
        
                /**
                 * @return array
                 */
                private function getListTableHeaders() :array{
                        return [
                                ['data' => [
                                        '#type' => 'checkbox',
                                        '#name' => 'select_all',
                                        '#attributes' => [
                                                'style' => 'position: relative;'
                                        ]
                                ]],
                                ['data' => [
                                        '#markup' => (string)$this->t('Title')
                                ]],
                                ['data' => [
                                        '#markup' => (string)$this->t('Type')
                                ]],
                                ['data' => [
                                        '#markup' => (string)$this->t('Received')
                                ]],
                                ['data' => [
                                        '#markup' => ''
                                ]]
                        ];
                }
                
                /**
                 * @inheritDoc
                 */
                public function getFormId () {
                        return 'ListAbstracts';
                }
        
                /**
                 * @inheritDoc
                 */
                public function buildForm ( array $form, FormStateInterface $form_state, $cf_id = null) {
                        
                        $currentUser = Publisso::currentUser();
                        if(!($cf_id || $form_state->has('cf_id'))) return $form;
                        if(!$form_state->has('cf_id')) $form_state->set('cf_id', $cf_id);
                        
                        $this->conference = ConferenceManager::getConference($form_state->get('cf_id'));
                        
                        $sessions = [
                                '' => '– '.(string)$this->t('Select session').' –'
                        ];
        
                        foreach($this->conference->getSessions() as $sessionID => $session){
                                $sessions[$sessionID] = implode('', array_fill(0, $session->depth, "····")).($session->day ?? $session->title);
                        }
                        
                        $this->setFormFilter($form);
                        
                        $form['table'] = [
                                '#type' => 'table',
                                '#empty' => (string)$this->t('No abstracts found...'),
                                '#header' => $this->getListTableHeaders()
                        ];
        
                        $sessions = [
                                '' => '– '.(string)$this->t('Select session').' –'
                        ];
        
                        foreach($this->conference->getSessions() as $sessionID => $session){
                                $sessions[$sessionID] = implode('', array_fill(0, $session->depth, "····")).($session->day ?? $session->title);
                        }
                        
                        $followers = [];
                        
                        foreach(AbstractManager::getAbstractList($this->conference->getId(), ['ca_id'], $this->parseFilter(), 20) as $row){
        
                                $id = $row->ca_id;
                                $abstract = AbstractManager::getAbstract($id);
                                $workflow = WorkflowManager::getItem($abstract->getElement('wfid'));
                                if(!($workflow->userIsAssigned() || $currentUser->isAdministrator() || $currentUser->isPublisso())) continue;
                                
                                $checkedStates = [];
                                
                                $user = UserManager::getUserFromID($workflow->getElement('created_by_uid'));
                                
                                $items = $workflow->getSchemaItemActions(null, true);
                                
                                if($workflow->getElement('state') == 'in review') {
                                        $items = [];
                                        $items[] = $workflow->getSchemaDefaultFollower();
                                }
                                
                                foreach($items as $item){
                                        
                                        if($item['schema_follower']) {
                                                $followers[ $item[ 'schema_follower' ] ] = (string)$this->t( $item[ 'schema_select_action' ] ?? 'n.a.' );
                                                $myFollowers[] = $item[ 'schema_follower' ];
        
                                                $checkedStates[] =
                                                        [ ':input[name="follower"]' => [ 'value' => $item[ 'schema_follower' ] ] ];
                                        }
                                }
                                
                                $checkbox = ['#markup' => ''];
                                
                                if($workflow->userIsAssigned() || $currentUser->isPublisso() || $currentUser->isAdministrator()){
                                        $checkbox = [
                                                '#type' => 'checkbox',
                                                '#group' => 'checkboxes',
                                                '#states' => [
                                                        'checked' => $checkedStates
                                                ]
                                        ];
                                }
                                
                                $form['table'][] = [
                                        'checked#'.$id => $checkbox,
                                        'title' => [
                                                '#markup' => $abstract->getElement('title'),
                                                '#prefix' => '<span class="fakeLink" data-toggle="collapse" data-target="[abstract=\''.$id.'\']">',
                                                '#suffix' => '</span>'
                                        ],
                                        'type' => [
                                                '#markup' => $this->t($abstract->getElement('submission_type') ?? 'n.a.')
                                        ],
                                        'created' => [
                                                '#markup' => $abstract->getElement('created')
                                        ],
                                        'actions' => [
                                                '#type' => 'dropbutton',
                                                '#links' => $this->getActions($workflow)
                                        ]
                                ];
                                
                                foreach(['keywords', 'category', 'authors_db', 'authors_add', 'corporations', 'attachments'] as $var) $$var = $abstract->getElement($var) ?? [];
                
                                $authors = [];
                                foreach (array_merge(${'authors_db'}, ${'authors_add'}) as $_){
                                        $authors[] = $_['firstname']. ' '. $_['lastname'].'; '.$_['affiliation'];
                                }
                
                                $fundingStr = '';
                                if($abstract->getElement('ctrlFunding')){
                        
                                        foreach($abstract->getFundings() as $funding){
                                
                                                $fundingStr .= (!empty($fundingStr) ? '<br>' : '');
                                                $fundingStr .= '<strong>'.(string)$this->t('Funding #@nr', ['@nr' => $funding->getElement('number')]).'</strong><br>';
                                
                                                switch($funding->getElement('type')){
                                                        case 'manual':
                                                                $fundingStr .= (string)$this->t('Funding-name: @name', ['@name' => $funding->getElement('funder_name')]).'<br>';
                                                                $fundingStr .= (string)$this->t('Funding-ID: @id', ['@id' => $funding->getElement('funder_id')]).'<br>';
                                                                break;
                                        
                                                        case 'stream':
                                                                $fundingStr .= (string)$this->t('Funder: @name', ['@name' => FunderManager::getNormalizedAndIndexedList()[$funding->funder()->getID()]]).'<br>';
                                                                $fundingStr .= (string)$this->t('Award-number: @nr', ['@nr' => $funding->getElement('funder_award_number')]).'<br>';
                                                                $fundingStr .= (string)$this->t('Award-uri: @uri', ['@uri' => $funding->getElement('funder_award_uri')]).'<br>';
                                                                $fundingStr .= (string)$this->t('Award-identifier: @identifier', ['@identifier' => $funding->getElement('funder_award_identifier')]).'<br>';
                                                                $fundingStr .= (string)$this->t('Award-title: @title', ['@title' => $funding->getElement('funder_award_title')]).'<br>';
                                                                $fundingStr .= (string)$this->t('Funding-stream: @stream', ['@stream' => $funding->getElement('funder_funding_stream')]).'<br>';
                                                                break;
                                                }
                                        }
                                }
                                
                                $strAttachments = ''; $i = 1;
                                foreach(${'attachments'} as $_){
                        
                                        $file = File::load($_);
                                        if($file) {
                                
                                                $url = Url::fromRoute('publisso_gold.getPicture', ['type' => 'ul', 'id' => $_]);
                                                $url->setOptions(['attributes' => ['target' => '_blank']]);
                                                $strAttachments .= ( !empty( $strAttachments ) ? '<br>' : '' );
                                                $strAttachments .= Link::fromTextAndUrl( (string)$this->t( 'Attachment #@i', [ '@i' => $i++ ] ), $url)->toString() ;
                                        }
                                }
                                
                                $form['table'][] = [
                                        [
                                                'data' => [
                                                        'content' => [
                                                                '#type' => 'table',
                                                                '#rows' => [
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Creator')]],
                                                                          'width' => 200],
                                                                         ['data' => [
                                                                                 ['#markup' => $user->profile->getReadableName().' ('.$workflow->getElement('created_by_uid').')']]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Modified')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => $abstract->getElement('modified')]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Keywords')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => implode(', ', array_filter(${'keywords'}))]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Category')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => implode(', ', array_filter(${'category'}))]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Language')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => $this->t(\Drupal::languageManager()->getLanguage($abstract->getElement('language') ?? 'en')->getName())]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Author(s)')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => implode("<br>", $authors)]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Corporation(s)')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => implode("<br>", ${'corporations'})]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Funding(s)')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => $fundingStr]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Award')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => (string)$this->t($abstract->getElement('award') ? 'Yes' : 'No')]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Award-info')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => $abstract->getElement('award_info')]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Conflict of interest')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => $abstract->getElement('conflict_of_interest')]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Attachments')]]
                                                                         ],
                                                                         ['data' => [
                                                                                 ['#markup' => $strAttachments]]
                                                                         ]],
                                                                        [['data' => [
                                                                                ['#markup' => (string)$this->t('Main text')]]
                                                                         ],
                                                                         ['data' => [[
                                                                                             '#type' => 'container',
                                                                                             'prefix' => [
                                                                                                     '#markup' => '<span class="fakeLink" data-toggle="collapse" data-target="div[id=\'main_text_'.$id.'\']">'.((string)$this->t('Click to expand/collapse')).'</span>'
                                                                                             ],
                                                                                             'main_text' => [
                                                                                                     '#type' => 'inline_template',
                                                                                                     '#template' => $abstract->getElement('main_text'),
                                                                                                     '#prefix' => '<div class="collapse" id="main_text_'.$id.'">',
                                                                                                     '#suffix' => '</div>'
                                                                                             ]
                                                                                     ]]
                                                                         ]],
                                                                ],
                                                                '#attributes' => [
                                                                        'class' => ['rwTableInline']
                                                                ]
                                                        ],
                                                        '#prefix' => '<div abstract="'.$id.'" class="collapse"',
                                                        '#suffix' => '<div>'
                                                ],
                                                '#wrapper_attributes' => [
                                                        'colspan' => count($form['table']['#header']),
                                                        'style' => 'padding: 0px 5px 0px 5px; margin: 0; border: none;'
                                                ]
                                        ]
                                ];
                        }
                        
                        $form['pager'] = ['#type' => 'pager'];
                        if(count($followers)) {
                                
                                $form['follower'] = [
                                        '#title' => (string)$this->t('Select action'),
                                        '#type' => 'select',
                                        '#options' => $followers,
                                        '#required' => true
                                ];
                                
                                //field EIC
                                $options = [];
                                foreach ( $this->conference->readEditorsInChief() as $user ) {
                                        $options[ $user->getId() ] = $user->profile->getReadableName();
                                }
        
                                $form[ 'eic' ] = [
                                        '#type'     => 'select',
                                        '#multiple' => true,
                                        '#options'  => $options,
                                        '#states'   => [
                                                'visible'  => [
                                                        ':input[name="follower"]' => [ 'value' => 5 ]
                                                ],
                                                'required' => [
                                                        ':input[name="follower"]' => [ 'value' => 5 ]
                                                ]
                                        ],
                                        '#title'    => (string)$this->t( 'Select users for editor in chief' )
                                ];
        
                                //field editor
                                $options = [];
                                foreach ( $this->conference->readEditors() as $user ) {
                                        $options[ $user->getId() ] = $user->profile->getReadableName();
                                }
        
                                $form[ 'editor' ] = [
                                        '#type'     => 'select',
                                        '#multiple' => true,
                                        '#options'  => $options,
                                        '#states'   => [
                                                'visible'  => [
                                                        ':input[name="follower"]' => [ 'value' => 6 ], 'or', ':input[name="follower"]' => [ 'value' => 8 ]
                                                ],
                                                'required' => [
                                                        ':input[name="follower"]' => [ 'value' => 6 ], 'or', ':input[name="follower"]' => [ 'value' => 8 ]
                                                ]
                                        ],
                                        '#title'    => (string)$this->t( 'Select users for editor' )
                                ];
        
                                //field reviewer
                                $options = [];
                                foreach ( $this->conference->readReviewers() as $user ) {
                                        $options[ $user->getId() ] = $user->profile->getReadableName();
                                }
        
                                $form[ 'reviewer' ] = [
                                        '#type'     => 'select',
                                        '#multiple' => true,
                                        '#options'  => $options,
                                        '#states'   => [
                                                'visible'  => [
                                                        ':input[name="follower"]' => [ 'value' => 7 ]
                                                ],
                                                'required' => [
                                                        ':input[name="follower"]' => [ 'value' => 7 ]
                                                ]
                                        ],
                                        '#title'    => (string)$this->t( 'Select users for reviewer' )
                                ];
        
                                $form[ 'recommendation' ] = [
                                        '#type'    => 'select',
                                        '#title'   => (string)$this->t( 'Recommendation EiC' ),
                                        '#options' => [
                                                'accept' => (string)$this->t( 'Accept submission' )
                                        ],
                                        '#states'  => [
                                                'visible'  => [
                                                        ':input[name="follower"]' => [ 'value' => 9 ]
                                                ],
                                                'required' => [
                                                        ':input[name="follower"]' => [ 'value' => 9 ]
                                                ]
                                        ]
                                ];
                        }
                        
                        $form['submit'] = [
                                '#type' => 'submit',
                                '#value' => 'Submit'
                        ];
                        
                        $form['#cache'] = [
                                'max-age' => 0
                        ];
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function validateForm (array &$form, FormStateInterface $form_state ) {
        
                        foreach($form_state->getValue('table') as $row) {
        
                                foreach ( $row as $k => $v ) {
                
                                        if ( preg_match( '/^checked#(\d+)$/', $k, $matches ) && !!$v ) {
                                                
                                                $id = $matches[1];
                                                $abstract = AbstractManager::getAbstract($id);
                                                $workflow = WorkflowManager::getItem($abstract->getElement('wfid'));
                                                
                                                $followers = [];
                                                foreach($workflow->getSchemaItemActions(null, true) as $item){
                                                        $followers[] = $item['schema_follower'];
                                                }
                                                
                                                if(!in_array($form_state->getValue('follower'), $followers)){
                                                        $form_state->setErrorByName('checked#'.$id, (string)$this->t('The selected action is not allowed for item "@title"', ['@title' => $abstract->getElement('title')]));
                                                }
                                        }
                                }
                        }
                }
        
                /**
                 * @inheritDoc
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        foreach($form_state->getValue('table') as $row){
                                
                                foreach($row as $k => $v){
                                        
                                        if(preg_match('/^checked#(\d+)$/', $k, $matches)){
                                                
                                                $id = $matches[1];
                                                $abstract = AbstractManager::getAbstract($id);
                                                $workflow = WorkflowManager::getItem($abstract->getElement('wfid'));
                                                
                                                if($workflow->getLock()) {
        
                                                        switch ( $form_state->getValue( 'follower' ) ) {
                
                                                                case 5: //assign eic
                                                                        $workflow->setElement( 'assigned_to_eic', implode( ',', array_filter( $form_state->getValue( 'eic' ) ) ) );
                                                                        break;
        
                                                                case 6: //assign editor
                                                                        $workflow->setElement( 'assigned_to_editor', implode( ',', array_filter( $form_state->getValue( 'editor' ) ) ) );
                                                                        break;
        
                                                                case 7: //assign reviewer
                                                                        #$workflow->setElement( 'assigned_to_reviewer', implode( ',', array_filter( $form_state->getValue( 'reviewer' ) ) ) );
                                                                        
                                                                        foreach(array_filter( $form_state->getValue( 'reviewer' ) ) as $uid){
                                                                                $workflow->newUserReview($uid);
                                                                        }
                                                                        
                                                                        break;
                                                                        
                                                                case 9: //recommendation EiC
                                                                        $workflow->setRecommendation($form_state->getValue('recommendation'));
                                                                        break;
                                                                        
                                                                case 10: //accepted
                                                                        $workflow->setDataElement('accepted', date('Y-m-d'));
                                                                        break;
                                                                        
                                                                case 14: //publish
                                                                        $workflow->publish();
                                                                        break;
                                                        }
        
                                                        $workflow->newSchemaState( $form_state->getValue( 'follower' ) );
                                                        $workflow->unlock();
                                                }
                                        }
                                }
                        }
                        
                        return $form;
                }
        
                /**
                 * @param array $array
                 * @param FormStateInterface $form_state
                 * @return AjaxResponse
                 */
                public function applyFilter(array &$array, FormStateInterface $form_state){
                        
                        $request = \Drupal::requestStack()->getCurrentRequest();
                        $params  = [];
                        
                        $filter = null;
                        
                        foreach($form_state->getUserInput()['filter'] as $row){
                                
                                foreach($row as $col){
                                        
                                        foreach($col as $name => $value){
                                                
                                                if(!empty($value)) $filter[$name] = $value;
                                        }
                                }
                        }
                        
                        if(count($filter)) $params['filter'] = $filter;
                        $query = UrlHelper::buildQuery($params);
                        
                        $response = new AjaxResponse();
                        $response->addCommand(new OpenUrlCommand(null, $request->getPathInfo().(!!$query ? "?$query" : '')));
                        return $response;
                }
                
                private function getActions(Workflow &$workflow) :array {
                        
                        $links = [];
                        
                        switch($workflow->getElement('state')){
                                
                                case 'in review':
                                        $url = Url::fromRoute('publisso_gold.workflow.review', ['wf_id' => $workflow->getElement('id')]);
                                        
                                        if($url->access()) {
                                                $links[] = [
                                                        '#type'       => 'link',
                                                        '#title'      => (string)$this->t( 'Review' ),
                                                        '#url'        => $url,
                                                        '#attributes' => [
                                                                'class' => ['use-ajax'],
                                                                'data-dialog-type' => 'modal',
                                                                'data-dialog-options' => '{"width":1200,"height":800, "title": "'.((string)$this->t('Review \"@title\"', ['@title' => $workflow->getDataElement('title')])).'"}'
                                                        ]
                                                ];
                                        }
        
                                        $previewLink = $this->getPreviewLink($workflow);
                                        
                                        if($previewLink){
                                                $links[] = $previewLink;
                                        }
                                        if(null !== ($link = $this->getCommentsLink($workflow))) $links[] = $link;
                                        break;
        
                                default:
                                        
                                        if(null !== ($link = $this->getAdaptLink($workflow))) $links[] = $link;
                                        if(null !== ($link = $this->getEditLink($workflow))) $links[] = $link;
                                        if(null !== ($link = $this->getPreviewLink($workflow))) $links[] = $link;
                                        if(null !== ($link = $this->getCommentsLink($workflow))) $links[] = $link;
                        }
                        
                        return $links;
                }
        
                /**
                 * @param AbstractWorkflow $workflow
                 * @return array|null
                 */
                private function getCommentsLink(AbstractWorkflow &$workflow){
                        
                        $url = Url::fromRoute('publisso_gold.workflow.abstract.comments', ['wf_id' => $workflow->getElement('id')]);
                        
                        if($url->access()){
                                return [
                                        '#type' => 'link',
                                        '#title' => (string)$this->t('Comments'),
                                        '#url' => $url,
                                        '#attributes' => [
                                                'class' => ['use-ajax'],
                                                'data-dialog-type' => 'modal',
                                                'data-dialog-options' => '{"width":1200,"height":800, "title": "'.((string)$this->t('Formal adapt \"@title\"', ['@title' => $workflow->getDataElement('title')])).'"}'
                                        ]
                                ];
                        }
                        
                        return null;
                }
                
                private function getEditLink(AbstractWorkflow &$workflow) :? array{
                        
                        $url = Url::fromRoute('publisso_gold.workflow.edit', ['wf_id' => $workflow->getElement('id')]);
        
                        if($url->access()) {
                                return [
                                        '#type'       => 'link',
                                        '#title'      => (string)$this->t( 'Edit abstract' ),
                                        '#url'        => $url
                                ];
                        }
                        
                        return null;
                }
                
                private function getAdaptLink(AbstractWorkflow &$workflow) :? array{
                        
                        $url = Url::fromRoute('publisso_gold.workflow.adapt', ['wf_id' => $workflow->getElement('id')]);
        
                        if($url->access()) {
                                return [
                                        '#type'       => 'link',
                                        '#title'      => (string)$this->t( 'Formal adapt' ),
                                        '#url'        => $url,
                                        '#attributes' => [
                                                'class' => ['use-ajax'],
                                                'data-dialog-type' => 'modal',
                                                'data-dialog-options' => '{"width":1200,"height":800, "title": "'.((string)$this->t('Formal adapt \"@title\"', ['@title' => $workflow->getDataElement('title')])).'"}'
                                        ]
                                ];
                        }
                        
                        return null;
                }
                
                private function getPreviewLink(AbstractWorkflow &$workflow) :? array{
        
                        $url = Url::fromRoute('publisso_gold.conferences_conference.abstract', ['cf_id' => $workflow->getDataElement('cf_id'), 'ca_id' => $workflow->getDataElement('ca_id')]);
        
                        if($url->access()) {
                                return [
                                        '#type'       => 'link',
                                        '#title'      => (string)$this->t( 'Preview' ),
                                        '#url'        => $url,
                                        '#attributes' => [
                                                'target' => '_blank'
                                        ]
                                ];
                        }
        
                        return null;
                }
        }