<?php
        
        namespace Drupal\publisso_gold\Form\Conferences;
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Link;
        use Drupal\Core\Url;
        use Drupal\file\Entity\File;
        use Drupal\publisso_gold\Controller\Manager\ConferenceImport;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class ImportConference
         * @package Drupal\publisso_gold\Form\Conferences
         */
        class ImportConference extends FormBase {
                
                protected $importStructures;
                protected $tempStore;
                
                public function __construct(){
                        
                        $rClass = new \ReflectionClass($this);
                        
                        $this->importStructures = [];
                        
                        foreach($rClass->getMethods() as $_){
                                
                                if(preg_match('/^importStructure_(.+)$/', $_->getName(), $matches)){
                                        $this->importStructures[$matches[1]] = implode(' ', explode('_', $matches[1]));
                                }
                        }
        
                        $this->tmpStore = \Drupal::service('tempstore.private')->get('conference.import');
                }
                
                /**
                 * @inheritDoc
                 */
                public function getFormId() {
                        return 'import-conference';
                }
        
                /**
                 * @inheritDoc
                 */
                public function buildForm(array $form, FormStateInterface $form_state, $cf_id = null) {
                        
                        $data = $this->tmpStore->get('data');
                        
                        if(!$data) {
                                
                                $form['method'] = [
                                        '#type'     => 'select',
                                        '#title'    => (string)$this->t('Import structure'),
                                        '#required' => true,
                                        '#options'  => $this->importStructures
                                ];
                                
                                $form['file-wrapper'] = [
                                        '#type'   => 'container',
                                        'file'    => [
                                                '#type'               => 'managed_file',
                                                '#title'              => (string)$this->t('Import archiv'),
                                                '#required'           => true,
                                                '#progress_indicator' => 'bar',
                                                '#progress_message'   => (string)$this->t('Uploading file...'),
                                                '#upload_validators'  => [
                                                        'file_validate_extensions' => ['zip'],
                                                        'file_validate_size'       => [30 * 1024 * 1024],
                                                ]
                                        ],
                                        '#states' => [
                                                'invisible' => [
                                                        ':input[name="method"]' => ['value' => '']
                                                ]
                                        ]
                                ];
                                
                                $form['action'] = [
                                        '#type' => 'hidden',
                                        '#value' => 'preview'
                                ];
                                
                                $form['preview'] = [
                                        '#type'  => 'submit',
                                        '#value' => (string)$this->t('Preview'),
                                        '#button_type' => 'info',
                                        '#name' => 'preview'
                                ];
                        }
                        else{
                                
                                $data = unserialize($data);
                                $files = unserialize($this->tmpStore->get('files'));
                                $tmpDir = $this->tmpStore->get('path');
                                
                                //Conference
                                $form['conference_wrapper'] = [
                                        '#type' => 'details',
                                        '#title' => (string)$this->t('Conference'),
                                        'conference' => []
                                ];
                                
                                $form['conference_wrapper']['conference'] = [
                                        '#type' => 'table',
                                        '#title' => (string)$this->t('Conference'),
                                ];
                                
                                foreach($data['Conference'] as $head => $value){
                                        
                                        if($head == 'logo' && array_key_exists('logo_fid', $data['Conference'])){
                                                $value = Link::fromTextAndUrl($value, Url::fromRoute('publisso_gold.getPicture', ['type' => 'ul', 'id' => $data['Conference']['logo_fid']], ['attributes' => ['target' => '_blank']]))->toString();
                                        }
        
                                        if($head == 'banner' && array_key_exists('banner_fid', $data['Conference'])){
                                                $value = Link::fromTextAndUrl($value, Url::fromRoute('publisso_gold.getPicture', ['type' => 'ul', 'id' => $data['Conference']['banner_fid']], ['attributes' => ['target' => '_blank']]))->toString();
                                        }
                                        
                                        $form['conference_wrapper']['conference'][] = [
                                                ['data' => [
                                                        '#markup' => $head
                                                ]],
                                                ['data' => [
                                                        '#type' => 'inline_template',
                                                        '#template' => $value
                                                ]]
                                        ];
                                }
                                
                                //Sessions
                                $form['sessions_wrapper'] = [
                                        '#type' => 'details',
                                        '#title' => (string)$this->t('Sessions'),
                                        'sessions' => [
                                                '#markup' => print_r($data['SessionsMapped'], 1),
                                                '#prefix' => '<pre>',
                                                '#suffix' => '</pre>'
                                        ]
                                ];
                                
                                $getList = function(&$sessions, string &$list) use (&$getList){
                                        
                                        foreach($sessions as $session){
                                                
                                                $list .= '<li>';
                                                
                                                if(strtolower($session['type']) == 'day'){
                                                        $dayFrom = strtotime($session['day from']);
                                                        $dayTo = strtotime($session['day to']);
                                                        $list .= date('Y-m-d', $dayFrom).' &minus; '.date('Y-m-d', $dayTo);
                                                }
                                                else{
                                                        $list .= $session['title'];
                                                }
                                                
                                                if(array_key_exists('childs', $session) && is_array($session['childs'])){
                                                        $list .= '<ul>';
                                                        $getList($session['childs'], $list);
                                                        $list .= '</ul>';
                                                }
                                                
                                                $list .= '</li>';
                                        }
                                };
                                
                                $list = ''; $getList($data['SessionsMapped'], $list);
                                $list = '<ul>'.$list.'</ul>';
                                
                                $form['sessions_wrapper']['sessions'] = [
                                        '#type' => 'inline_template',
                                        '#template' => $list
                                ];
                                
                                //Abstracts
                                $form['abstracts_wrapper'] = [
                                        '#type' => 'details',
                                        '#title' => (string)$this->t('Abstracts'),
                                        'abstracts' => []
                                ];
                                
                                foreach($data['Abstracts'] as $no => $abstract){
                                        
                                        $form['abstracts_wrapper']['abstracts'][$no] = [
                                                '#type' => 'details',
                                                '#title' => $abstract['title'],
                                                'abstract' => [
                                                        '#type' => 'table',
                                                        '#title' => $abstract['title']
                                                ]
                                        ];
                                        
                                        foreach($abstract as $head => $value){
                                                
                                                if($head == 'keywords'    ) $value = nl2br($value);
                                                if($head == 'category'    ) $value = nl2br($value);
                                                if($head == 'references'  ) $value = nl2br($value);
                                                if($head == 'corporations') $value = nl2br($value);
                                                
                                                if($head == 'attachments' ){
                                                        
                                                        $attachments = array_filter(explode("\n", $value));
                                                        $attachments = array_map("trim", $attachments);
                                                        
                                                        $_ = [];
                                                        foreach($attachments as $attachment){
                                                                
                                                                $attachment = str_replace('file:/', $tmpDir, $attachment);
                                                                if(null !== ($key = array_search($attachment, array_column($files, 'uri')))){
                                                                        
                                                                        if(array_key_exists('fid', $files[$key])){
                                                                                
                                                                                if(null !== ($file = File::load($files[$key]['fid']))) {
        
                                                                                        $url = preg_replace( '|^http(s?):|', '', $file->url() );
                                                                                        $_[] = Link::fromTextAndUrl( $files[ $key ][ 'filename' ], Url::fromUserInput( $url, [ 'attributes' => [ 'target' => '_blank' ] ] ) )->toString();
                                                                                }
                                                                        }
                                                                        else{
                                                                                $_[] = $files[$key]['filename'];
                                                                        }
                                                                }
                                                        }
                                                        $value = nl2br(implode("\n", $_));
                                                }
                                                
                                                if($head == 'authors'){
        
                                                        $_ = '';
                                                        
                                                        foreach($value as $author) {
                                                                
                                                                $_ .= (!empty($_) ? '<br>' : '');
                                                                $_ .= $author['firstname'].' '.$author['lastname'].' (Email: '.$author['email'].')<br>';
                                                                $_ .= (string)$this->t('Presenting: @presenting | Corresponding: @corresponding', [
                                                                        '@presenting'    => (string)$this->t($author['presenting'   ] ? 'Yes' : 'No'),
                                                                        '@corresponding' => (string)$this->t($author['corresponding'] ? 'Yes' : 'No')
                                                                ]);
                                                                
                                                                $_ .= '<br>Affiliation(s):<br>';
                                                                
                                                                foreach($author['affiliations'] as $affiliation){
                                                                        unset($affiliation['no']);
                                                                        $_ .= implode(', ', $affiliation).'<br>';
                                                                }
                                                        }
        
                                                        $value = $_;
                                                }
                                                
                                                $form['abstracts_wrapper']['abstracts'][$no]['abstract'][] = [
                                                        ['data' => [
                                                                '#markup' => $head
                                                        ]],
                                                        ['data' => [
                                                                '#type' => 'inline_template',
                                                                '#template' => $value
                                                        ]]
                                                ];
                                        }
                                }
                                
                                $form['confirm_import'] = [
                                        '#type' => 'select',
                                        '#title' => (string)$this->t('Select action for displayed data'),
                                        '#options' => [
                                                'import' => (string)$this->t('Import'),
                                                'cancel' => (string)$this->t('Cancel')
                                        ]
                                ];
        
                                $form['submit'] = [
                                        '#type'  => 'submit',
                                        '#value' => (string)$this->t('Submit'),
                                        '#name' => 'submit'
                                ];
                        }
        
                        $form['#cache'] = ['max-age' => 0];
                        
                        return $form;
                }
        
                /**
                 * @inheritDoc
                 */
                public function submitForm(array &$form, FormStateInterface $form_state) {
                        
                        if($form_state->hasValue('confirm_import')){
                                
                                switch($form_state->getValue('confirm_import')){
                                        
                                        case 'cancel':
                                                $this->tmpStore->delete('data');
                                                $this->tmpStore->delete('path');
                                                $this->tmpStore->delete('files');
                                                break;
                                }
                        }
                        return $form;
                }
        
                /**
                 * @inheritDoc
                 */
                public function validateForm(array &$form, FormStateInterface $form_state) {
                        
                        if(!$form_state->hasValue('file')) return $form;

                        $importStructure = $form_state->getValue('method');
                        $file = File::load($form_state->getValue('file')[0]);
                        
                        if(!$file) $form_state->setError($form['file-wrapper']['file'], (string)$this->t('Error during uploading file. File not exists!'));
                        
                        $path = \Drupal::service('file_system')->realpath($file->get('uri')->getValue()[0]['value']);
                        
                        $method = "validateImport$importStructure";
                        
                        if(!ConferenceImport::$method($path)) $form_state->setError($form['file-wrapper']['file'], (string)$this->t('Archive validation failed!'));
                        else{
                                $this->messenger()->addStatus((string)$this->t('File successfully checked!'));
                        }
                        return $form;
                }
        
                /**
                 * @param array              $form
                 * @param FormStateInterface $form_state
                 * @param string             $action
                 */
                
                protected function importStructure_PUBLISSO_default(array &$form, FormStateInterface $form_state, string $action){
                
                        switch($action){
                                
                                case 'validate':
                                        ConferenceImport::validateImportPublisso_default();
                                        break;
                                        
                        }
                }
        }