<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldPasswordReset.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormInterface;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Component\Utility\UrlHelper;
        use Drupal\Core\Database\Connection;
        use Drupal\Core\Messenger\Messenger;
        use Drupal\file\Element;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\User;
        use Drupal\publisso_sso\Classes\SSO;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        use Symfony\Component\HttpFoundation\File;
        use Drupal\Core\Url;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldPasswordReset extends FormBase {
                
                private $modname = 'publisso_gold';
                private $database;
                private $modpath;
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'publisso_goldpasswordreset';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state ) {
                        
                        $this->modpath = drupal_get_path( 'module', $this->modname );
                        require_once( $this->modpath . '/inc/publisso_gold.lib.inc.php' );
                        
                        if ( !$this->database )
                                $this->database = \Drupal::database();
                        
                        $form[ 'info' ] = [
                                '#type'   => 'markup',
                                '#markup' => (string)t( '
                Please fill in this form. Afterwards you will receive an email with a link to create a new password.
            '
                                ),
                        ];
                        
                        $form[ 'data' ][ 'username' ] = [
                                '#type'        => 'textfield',
                                '#title'       => (string)t( 'Username (Loginname)' ),
                                '#placeholder' => (string)t( 'username' ),
                        ];
                        
                        $form[ 'data' ][ 'email' ] = [
                                '#type'        => 'textfield',
                                '#title'       => (string)t( 'The email address of your user profile' ),
                                '#placeholder' => (string)t( 'emailaddress' ),
                        ];
                        
                        $form[ 'submit' ] = [
                                '#type'  => 'submit',
                                '#value' => (string)t( 'Send' ),
                        ];
                        
                        $form[ '#cache' ][ 'max-age' ] = 0;
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        //getting submitted fields
                        $username = $email = null;
                        foreach ( $form_state->getValues() as $k => $v ) $$k = $v;
                        
                        //count username/email-combinations
                        $query = \Drupal::database();
                        
                        $query = $query->select( 'rwpgvwAllUserdata', 't' );
                        $query = $query->fields( 't', [ 'login_id' ] );
                        $query = $query->condition( 'login_user', $username );
                        $query = $query->condition( 'profile_email', $email );
                        $query = $query->countQuery();
                        $query = $query->execute();
                        
                        //if no combination found, throw error
                        if ( !$query->fetchField() ) {
                                $form_state->setErrorByName( 'username', (string)t( 'Username/Email-Combination does not exists!' ) );
                        }
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        //getting submitted fields
                        $username = $email = null;
                        foreach ( $form_state->getValues() as $k => $v ) $$k = $v;
                        
                        //get login-id
                        $query = \Drupal::database();
                        
                        $query = $query->select( 'rwpgvwAllUserdata', 't' );
                        $query = $query->fields( 't', [ 'login_id', 'profile_email' ] );
                        $query = $query->condition( 'login_user', $username );
                        $query = $query->condition( 'profile_email', $email );
                        $query = $query->execute();
                        
                        $result = $query->fetchAssoc();
                        
                        if ( Publisso::setup()->getValue('system.sso.active') && SSO::ssoAvailable() ) {
                                
                                $user = new User( $result[ 'login_id' ] );
                                $sso = SSO::resetPassword($user);
                                
                                if($sso->getLastCode() != 200){
                                        \Drupal::messenger()->addError('An error occured while resetting your password. Please contact PUBLISSO-service!');
                                }
                                else{
                                        \Drupal::messenger()->addMessage('Thank you! An email with further information is sent to your emailaddress.');
                                }
                        }
                        else {
                                //generate token
                                $token = sha1( md5( uniqid( $email . '~' . time() . '~' . $username, true ) ) );
                                
                                //store token in pw-reset-table
                                $dbo = \Drupal::database();
                                
                                //delete old entry for this user
                                $query = $dbo->delete( 'rwPubgoldPWReset' );
                                $query = $query->condition( 'pwr_loginid', $result[ 'login_id' ], '=' );
                                $query->execute();
                                
                                //insert new entry
                                $query = $dbo->insert( 'rwPubgoldPWReset' );
                                
                                $query = $query->fields( [
                                                                 'pwr_loginid' => $result[ 'login_id' ],
                                                                 'pwr_token'   => $token,
                                                         ]
                                );
                                
                                $query->execute();
                                
                                //inform user
                                $link = Url::fromRoute( 'publisso_gold.password.set', [ 'token' => $token ], [ 'absolute' => true ] );
                                $link = $link->toString();
                                
                                $mail_header = [ 'MAIL FROM' => 'Publisso <no-reply@publisso.de>' ];
                                $mail_subject = (string)t( 'Publisso - Password reset' );
                                $mail_body = "You requested to reset your password for your Publisso account.\n";
                                $mail_body .= "Please follow this link to proceed:\n";
                                $mail_body .= $link . "\n";
                                $mail_body .= "\n\n";
                                $mail_body .= "If you did not request to reset your password, please ignore this e-mail.\n";
                                $mail_body .= "\n";
                                $mail_body .= "Kind regards\n";
                                $mail_body .= "Your PUBLISSO-Team";
                                
                                sendMail( $result[ 'profile_email' ], $mail_subject, $mail_body, $mail_header );
                                \Drupal::service('messenger')->addMessage( (string)t( 'Thank you! An email with further information is sent to your emailaddress' ));
                        }
                        
                        return $form;
                }
        }
