<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldBookmanagementFilter.
 */
namespace Drupal\publisso_gold\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldBookmanagementFilter extends FormBase {
	const __ACCESS_DENIED__ = 'You can not access this page. You may need to log in first.';
	private $modname = 'publisso_gold';
	private $database;
	private $modpath;
	private $user = NULL;
	private $selected_bookid = '';
	private $selected_author = '';
	private $selected_editor = '';
	private $modaccessweights;
	public function __construct(Connection $database) {
		$this->database = $database;
		$this->user = $this->database->select ( 'rwpgvwUserProfiles', 'u' )->fields ( 'u', [ ] )->condition (
		        'id', \Drupal::service('session')->get('user')['id'], '=' )->execute ()->fetchAssoc ();
	}
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldBookmanagementFilter|static
         */
        public static function create(ContainerInterface $container) {
		return new static ( $container->get ( 'database' ) );
	}
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function getFormId() {
		return 'publisso_goldbookmanagementfilter';
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {
                
                $qry = \Drupal::database()->select('rwPubgoldBooks', 'b')->fields('b', ['bk_id']);
                $pager = $qry->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit(5);
                $pager->orderBy('bk_title', 'ASC');
                
	        $urlAddBook = Url::fromRoute('publisso_gold.bookmanagement_add');
	        
	        $form['list'] = [
	                '#type' => 'table',
                        '#header' => [
                                ['data' => (string)$this->t('Title')],
                                ['data' => (string)$this->t('Title Abbr.')],
                                ['data' => (string)$this->t('Public')],
                                []
                        ],
                        '#rows' => [],
                        '#empty' => (string)$this->t('No books found...')
                ];
	        
	        $form['pager'] = [
	                '#type' => 'pager'
                ];
	        
	        
		$session = \Drupal::service ( 'session' );
		
		if (! $this->database)
			$this->database = \Drupal::database ();
		
		if (! $this->user)
			$this->user = $this->database->select ( 'rwpgvwUserProfiles', 'u' )->fields ( 'u', [ ] )->condition ( 'id', $session->get ( 'user' ) ['id'], '=' )->execute ()->fetchAssoc ();
		
		$this->getModuleAccessPermissions ();
                
                $user = $session->get('user');
                
                if(is_array($user)) {
                        $user_has_rights = $user[ 'isEditor' ] | $user[ 'isEiC' ] | $user[ 'isEO' ];
                }
		
		if (! (\Drupal::service ( 'publisso_gold.tools' )->userHasAccessRight ( 'bookmanagement' ) || $user_has_rights)) {
			return array (
				'#type' => 'markup',
				'#markup' => self::__ACCESS_DENIED__,
				'#attached' => [ 
					'library' => [ 
						'publisso_gold/default' 
					] 
				] 
			);
		}
		
		$this->modpath = drupal_get_path ( 'module', $this->modname );
		require_once ($this->modpath . '/inc/publisso_gold.lib.inc.php');
		
		// get editors
		$result = $this->database->select ( 'rwpgvwBookEditors', 'be' )->fields ( 'be', [ ] )->execute ()->fetchAll ();
		
		$editors = array ();
		
		foreach ( $result as $_ ) {
			$editors [substr ( $_->up_lastname, 0, 1 )] [$_->up_uid] = implode ( [ 
				' ',
				$_->up_graduation,
				implode ( ', ', [ 
					$_->up_lastname,
					$_->up_firstname 
				] ) 
			] );
		}
		
		// get authors
		$result = $this->database->select ( 'rwpgvwBookAuthors', 'ba' )->fields ( 'ba', [ ] )->execute ()->fetchAll ();
		
		$authors = array ();
		
		foreach ( $result as $_ ) {
			$authors [substr ( $_->up_lastname, 0, 1 )] [$_->up_uid] = implode ( [ 
				' ',
				$_->up_graduation,
				implode ( ', ', [ 
					$_->up_lastname,
					$_->up_firstname 
				] ) 
			] );
		}
		
		$form ['filter'] = [ 
			'#type' => 'fieldset',
			'#title' => t ( 'Filter' ) 
		];
		
		$form ['filter'] ['author'] = [ 
			'#type' => 'select',
			'#title' => t ( 'Author' ),
			'#options' => $authors 
		];
		
		$form ['filter'] ['editor'] = [ 
			'#type' => 'select',
			'#title' => t ( 'Editor' ),
			'#options' => $editors 
		];
		
		$form ['filter'] ['markup'] = [ 
			'#type' => 'markup',
			'#markup' => '<br clear="all">' 
		];
		
		$form ['filter'] ['submit'] = [ 
			'#type' => 'submit',
			'#value' => t ( 'Apply Filter' ),
			'#submit' => [ 
				'::apply_filter' 
			] 
		];
		
		unset ( $form ['filter'] );
		
		// get books
		$result = $this->database->select ( 'rwpgvwBooks', 'bk' )->fields ( 'bk', [ 
			'bk_id',
			'bk_title',
			'bk_title_abbr' 
		] );
		
		if ($this->selected_author || $this->selected_editor) {
			
			if ($this->selected_author != '' && $this->selected_editor == '') {
				$result->condition ( 'ba_uid', $this->selected_author, '=' );
			} elseif ($this->selected_author == '' && $this->selected_editor != '') {
				$result->condition ( 'be_uid', $this->selected_editor, '=' );
			} elseif ($this->selected_author != '' && $this->selected_editor != '') {
				$group = $result->orConditionGroup ()->condition ( 'be_uid', $this->selected_editor, '=' )->condition ( 'ba_uid', $this->selected_author, '=' );
				$result->condition ( $group );
			}
		}
		
		$result->groupBy ( 'bk.bk_id' );
		$result->groupBy ( 'bk.bk_title' );
		$result->groupBy ( 'bk.bk_title_abbr' );
		
		$result = $result->execute ()->fetchAll ();
		
		$books = array ();
		foreach ( $result as $res ) {
			
			// get authors
			$authors_string = '';
			$authors = $this->database->select ( 'rwpgvwBookAuthors', 'ba' )->fields ( 'ba', [ ] )->condition ( 'bk_id', $res->bk_id, '=' )->execute ()->fetchAll ();
			
			foreach ( $authors as $author ) {
				$authors_string .= (! empty ( $authors_string ) ? ' | ' : '') . implode ( [ 
					' ',
					$author->up_graduation,
					implode ( ', ', [ 
						$author->up_lastname,
						$author->up_firstname 
					] ) 
				] );
			}
			
			// get editors
			$editors_string = '';
			$editors = $this->database->select ( 'rwpgvwBookEditors', 'be' )->fields ( 'be', [ ] )->condition ( 'bk_id', $res->bk_id, '=' )->execute ()->fetchAll ();
			
			foreach ( $editors as $editor ) {
				$editors_string .= (! empty ( $editors_string ) ? ' | ' : '') . implode ( [ 
					' ',
					$editor->up_graduation,
					implode ( ', ', [ 
						$editor->up_lastname,
						$editor->up_firstname 
					] ) 
				] );
			}
			
			$books [$res->bk_id] = [ 
				$res->bk_title,
				$res->bk_title_abbr,
				$authors_string,
				$editors_string 
			];
		}
		
		// get template(s)
		$tmpl_head = file_get_contents ( $this->modpath . '/inc/publisso_gold_bookmanagement_booklist_head.tmpl.inc.php' );
		$tmpl_data = file_get_contents ( $this->modpath . '/inc/publisso_gold_bookmanagement_booklist_data.tmpl.inc.php' );
		$tmpl_foot = file_get_contents ( $this->modpath . '/inc/publisso_gold_bookmanagement_booklist_foot.tmpl.inc.php' );
		
		$markup = '';
		$tmpl_vars = array ();
		$markup .= $tmpl_head;
		
		foreach ( $result as $_ ) {
			
			$book = new \Drupal\publisso_gold\Controller\Book ( $_->bk_id );
			
			if (! ($session->get ( 'user' ) ['role_id'] == 1 || $session->get ( 'user' ) ['role_id'] == 2 || $book->isUserEditor ( $session->get ( 'user' ) ['id'] ) || $book->isUserEditorInChief ( $session->get ( 'user' ) ['id'] ) || $book->isUserEditorialOffice ( $session->get ( 'user' ) ['id'] )))
				continue;
			
			$editors_string = [ ];
			foreach ( $book->readEditors () as $__ ) {
				$editors_string [] = implode ( ', ', [ 
					$__->profile->getElement ( 'lastname' ),
					$__->profile->getElement ( 'firstname' ) 
				] );
			}
			$editors_string = implode ( '<br>', $editors_string );
			
			$authors_string = [ ];
			foreach ( $book->readAuthors () as $__ ) {
				$authors_string [] = implode ( ', ', [ 
					$__->profile->getElement ( 'lastname' ),
					$__->profile->getElement ( 'firstname' ) 
				] );
			}
			$authors_string = implode ( '<br>', $authors_string );
			
			$tmpl = $tmpl_data;
			$vars = array ();
			
			$vars ['::title::'] = $_->bk_title;
			$vars ['::title_abbr::'] = $_->bk_title_abbr;
			$vars ['::authors::'] = $authors_string;
			$vars ['::editors::'] = $editors_string;
			
			$url = $book->getUrl ();
			$vars ['::lnk_book::'] = \Drupal::l ( t ( 'View Book' ), $url ) . ' |';
			
			if (preg_match ( '/^1|2$/', $session->get ( 'user' ) ['role_id'] )) {
				
				$url = new Url ( 'publisso_gold.book.edit', [ 
					'bk_id' => $book->getElement ( 'id' ) 
				] );
				$vars ['::lnk_book_edit::'] = \Drupal::l ( t ( 'Edit Book' ), $url ) . ' |';
				
				$url = new Url ( 'publisso_gold.bookmanagement_book.delete', [ 
					'bk_id' => $book->getElement ( 'id' ) 
				] );
				$vars ['::lnk_book_delete::'] = \Drupal::l ( t ( 'Delete Book' ), $url ) . ' |';
			} else {
				$vars ['::lnk_book_edit::'] = t ( 'Edit Book' ) . ' |';
				$vars ['::lnk_book_delete::'] = t ( 'Delete Book' ) . ' |';
			}
			
			if (($user_has_rights || $book->isUserEditor ( $session->get ( 'user' ) ['id'] ) || $book->isUserEditorInChief ( $session->get ( 'user' ) ['id'] ) || $book->isUserEditorialOffice ( $session->get ( 'user' ) ['id'] ) || preg_match ( '/^1|2$/', $session->get ( 'user' ) ['role_id'] ))) {
				
				$url = new Url ( 'publisso_gold.book.invite_user', [ 
					'bk_id' => $book->getElement ( 'id' ) 
				] );
				$vars ['::lnk_book_invite_users::'] = \Drupal::l ( t ( 'Invite User' ), $url ) . ' |';
			} else {
				$vars ['::lnk_book_invite_users::'] = t ( 'Invite Users' ) . ' |';
			}
			
			if (($book->isUserEditorialOffice ( $session->get ( 'user' ) ['id'] ) || $book->isUserEditorInChief ( $session->get ( 'user' ) ['id'] ) || preg_match ( '/^1|2$/', $session->get ( 'user' ) ['role_id'] ))) {
				
				$url = new Url ( 'publisso_gold.book.invited_users', [ 
					'bk_id' => $book->getElement ( 'id' ) 
				], [
				        'attributes' => [
				                'onclick' => 'return confirm(\''.((string)$this->t('Do you really want to delete the book "@title"?', ['@title' => $book->getElement('title')])).'\')'
                                        ]
                                ] );
				$vars ['::lnk_book_invited_users::'] = \Drupal\Core\Link::fromTextAndUrl ( t ( 'Sent invitations' ), $url )->toString() . ' |';
			} else {
				$vars ['::lnk_book_invited_users::'] = t ( 'Sent invitations' ) . ' |';
			}
			
			if ($user_has_rights || $book->isUserEditor ( $session->get ( 'user' ) ['id'] ) || $book->isUserEditorInChief ( $session->get ( 'user' ) ['id'] ) || $book->isUserEditorialOffice ( $session->get ( 'user' ) ['id'] ) || preg_match ( '/^1|2$/', $session->get ( 'user' ) ['role_id'] )) {
				$url = new Url ( 'publisso_gold.bookmanagement_book.roles', [ 
					'bk_id' => $book->getElement ( 'id' ) 
				] );
				$vars ['::lnk_book_set_roles::'] = \Drupal::l ( t ( 'Assign Roles' ), $url ) . ' |';
			} else {
				$vars ['::lnk_book_set_roles::'] = t ( 'Assign Roles' ) . ' |';
			}
			
			$vars ['::bk_id::'] = $_->bk_id;
			
			$tmpl = $this->renderVars ( $tmpl, $vars );
			
			$markup .= $tmpl;
		}
		
		$markup .= $tmpl_foot;
		
		$tmpl_vars ['::txt_hl_title::'] = t ( 'Title' );
		$tmpl_vars ['::txt_hl_title_abbr::'] = t ( 'Title Abbr.' );
		$tmpl_vars ['::txt_hl_authors::'] = t ( 'Authors' );
		$tmpl_vars ['::txt_hl_editors::'] = t ( 'Editors' );
		
		$markup = $this->renderVars ( $markup, $tmpl_vars );
		
		$form ['books'] = [ 
			'#type' => 'markup',
			'#markup' => $markup 
		];
		
		$form ['#cache'] = [ 
			'max-age' => 0 
		];
		
		return $form;
	}
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function apply_filter(array &$form, FormStateInterface $form_state) {
		$this->selected_author = $form_state->getValue ( 'author' );
		$this->selected_editor = $form_state->getValue ( 'editor' );
		$form_state->setRebuild ();
	}
        
        /**
         * @param $tmpl
         * @param array $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl, $vars = array()) {
		
		// set Site-Vars
		$tmpl = str_replace ( array_keys ( $vars ), array_values ( $vars ), $tmpl );
		
		// remove unused vars
		$tmpl = preg_replace ( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
		
		return $tmpl;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {
	}
	private function getModuleAccessPermissions() {
		$result = $this->database->select ( 'rwPubgoldModuleaccess', 'ma' )->fields ( 'ma', [ ] )->execute ()->fetchAll ();
		
		foreach ( $result as $res ) {
			$this->modaccessweights [$res->mod_name] = $res->mod_minaccessweight;
		}
	}
        
        /**
         * @param $modname
         * @return bool
         */
        private function userHasAccessRight($modname) {
		$session = \Drupal::service ( 'session' );
		if (! array_key_exists ( $modname, $this->modaccessweights )) {
			return false;
		}
		return $this->modaccessweights [$modname] <= $session->get ( 'user' ) ['weight'];
	}
}
