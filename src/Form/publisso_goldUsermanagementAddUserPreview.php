<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldUsermanagementAddUserPreview.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldUsermanagementAddUserPreview extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $user = NULL;
    
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'publisso_goldusermanagementadduserpreview';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $this->modpath = drupal_get_path('module', $this->modname);
    require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
    
    if(!$this->database)
        $this->database = \Drupal::database();
	
	$form['sent_notification'] = [
		'#type' => 'checkbox',
		'#title' => t('Send account-information-email')
	];
	
    $form['back'] = array(
        '#type' => 'submit',
        '#value' => t('Back'),
        '#submit' => array('::backToForm'),
		'#suffix' => '&nbsp;&nbsp;&nbsp;'
    );
     
    $form['discard'] = array(
        '#type' => 'submit',
        '#value' => t('Discard'),
        '#submit' => array('::discardProfile'),
		'#suffix' => '&nbsp;&nbsp;&nbsp;'
    );
     
    $form['save'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
		'#suffix' => '&nbsp;&nbsp;&nbsp;'
    );
    
    $form['#cache']['max-age'] = 0;
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
  }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function discardProfile(array &$form, FormStateInterface $form_state){
        
        $this
            ->database
            ->delete('rwPubgoldUserprofilePreview')
            ->condition('pv_creator', $_SESSION['user']['id'])
            ->execute();
        
        $form_state->setRedirect('publisso_gold.usermanagement_adduser');
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function backToForm(array &$form, FormStateInterface $form_state){
        
        $form_state->setRedirect('publisso_gold.adduser');
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
		
        $user = $this
                    ->database
                    ->select('rwPubgoldUserprofilePreview', 'pv')
                    ->fields('pv', [])
                    ->condition('pv_creator', $_SESSION['user']['id'], '=')
                    ->execute()
                    ->fetchAssoc();
        
        $user['password_clear'] = genTempPassword();
        $user['password_encrypted'] = '*'.strtoupper(sha1(sha1($user['password_clear'], true)));
        
        //fill login-table
        $user['login_id'] = $this
                        ->database
                        ->insert('rwPubgoldUsers')
                        ->fields(
                            array(
                                'user' => $user['pv_username'],
                                'password' => $user['password_encrypted'],
                                'pw_change_required' => 1,
                                'active' => 1,
								'reg_notification_sent' => $form_state->getValue('sent_notification')
                            )
                        )
                        ->execute();
        
        //set role
        $this
            ->database
            ->insert('rwPubgoldUserroles')
            ->fields(
                array(
                    'role_id' => $user['pv_roleid'],
                    'user_id' => $user['login_id']
                )
            )
            ->execute();
        
        //save userprofile
        $this
            ->database
            ->insert('rwPubgoldUserProfiles')
            ->fields(
                array(
                    'up_city' => $user['pv_city'],
                    'up_country' => $user['pv_country'],
                    'up_edited' => 1,
                    'up_email' => $user['pv_email'],
                    'up_firstname' => $user['pv_firstname'],
                    'up_lastname' => $user['pv_lastname'],
                    'up_graduation' => $user['pv_graduation'],
                    'up_institute' => $user['pv_institute'],
                    'up_picture' => $user['pv_picture'],
                    'up_picture_type' => $user['pv_picture_type'],
                    'up_postal_code' => $user['pv_postal_code'],
                    'up_street' => $user['pv_street'],
                    'up_uid' => $user['login_id'],
                    'up_graduation_suffix' => $user['pv_graduation_suffix'],
                    'up_orcid' => $user['pv_orcid'],
                    'up_telephone' => $user['pv_telephone'],
                    'up_area_of_expertise' => $user['pv_area_of_expertise'],
                    'up_department' => $user['pv_department'],
                    'up_correspondence_language' => $user['pv_correspondence_language'],
                    'up_salutation' => $user['pv_alutation']
                )
            )
            ->execute();
        
        //save areas of expertise
        $aoe = json_decode($user['area_of_expertise_aoeid']);
        
        foreach($aoe as $_){
            $this
                ->database
                ->insert('rwPubgoldUserAreasOfExpertise')
                ->fields(['uaoe_aoeid' => $_, 'uaoe_uid' => $user['login_id']])
                ->execute();
        }
        //delete profile in preview
        $this
            ->database
            ->delete('rwPubgoldUserprofilePreview')
            ->condition('pv_id', $user['pv_id'], '=')
            ->execute();
        
        //inform user about the new profile if checked
		
		if($form_state->getValue('sent_notification') == 1){
			
			$link = 'http://'.$_SERVER['SERVER_NAME'];
						
			foreach(['registration_password', 'registration_info'] as $name){
			
				$vars = [
					'::firstname::' 	=> $user['pv_firstname'],
					'::lastname::' 		=> $user['pv_lastname'],
					'::link::' 			=> $link,
					'::email::'			=> $user['pv_email'],
					'::user::' 			=> $user['pv_username'],
					'::login_link::' 	=> $link.'/publisso_gold/login',
					'::password::' 		=> $user['password_clear'],
				];
				
				sendRegistrationInfoMail($name, $vars);
			}
		}
		
        if(\Drupal::service('publisso_gold.setup')->getValue('system.sso.active') === 'yes'){
                
                \Drupal::service('publisso_gold.tools')->exportUser($user['login_id'], $user['password_clear']);
        }
		
        drupal_set_message(t('Profile saved!'));
        return $form;
    }
}
