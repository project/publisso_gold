<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowAuthorActionAfterAuthorClearingRequest.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;

/**
 * Provides a simple example form.
 */
class publisso_goldWorkflowAuthorActionAfterAuthorClearingRequest extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $num_corporations;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldWorkflowAuthorActionAfterAuthorClearingRequest|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldworkflowauthoractionafterauthorclearingrequest';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        $args = $form_state->getBuildInfo();
        $wf_id = $args['args'][0]['wf_id'];
        
        if(!$form_state->get('wf_id'))
            $form_state->set('wf_id', $wf_id);
        
        if(!$form_state->get('step'))
            $form_state->set('step', 0);
        
        if($wf_id){
            
            $workflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
            
            if($workflow->getDataElement('cp_id')){
                    
                    $url = Url::fromRoute('publisso_gold.export.chapter', ['cp_id' => $workflow->getDataElement('cp_id')]);
                    
                    $form['pdf'] = [
                            '#type'   => 'link',
                            '#title'  => (string)$this->t('Download as PDF'),
                            '#url'    => $url,
                            '#prefix' => '<span class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;',
                            '#suffix' => '<br><br>',
                            '#attributes' => [
                                    'class' => [
                                            'download-pdf'
                                    ]
                            ]
                    ];
            }
            
            $actions = [
                'accept' => (string)t('Accept'),
                'author_change_request' => (string)t('Change-Request to editorial office')
            ];
            
            $form['author_action'] = [
                '#title' => (string)t('select action'),
                '#type' => 'select',
                '#options' => $actions,
                '#required' => true
            ];
            
            if(!$workflow->getDataElement('accept_policy')){
                        
                        $uri_de         = 'https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/#c5185';
                        $url_de         = Url::fromUri($uri_de);
                        $uri_1_de       = 'https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/';
                        $url_1_de 	= Url::fromUri($uri_1_de);
                        $text_de        = 'German';
                
                        $uri_en         = 'https://www.publisso.de/en/publishing/books/books-policy/#c5189';
                        $url_en 	= Url::fromUri($uri_en);
                        $uri_1_en       = 'https://www.publisso.de/en/publishing/books/books-policy/';
                        $url_1_en 	= Url::fromUri($uri_1_en);
                        $text_en        = 'English';
                        
                        $form['policy'] = [
                                '#type' => 'container',
                                '#access' => $form_state->get('step') == 0,
                                '#states'    => array(
                                        'visible'   => array(   // action to take.
                                                ':input[name="author_action"]' => ['value' => 'accept'],
                                        ),
                                        'invisible' => array(   // action to take.
                                                ':input[name="author_action"]' => ['!value' => 'accept'],
                                        ),
                                ),
                                'declaration' => [
                                        '#markup' => (string)$this->t('Please read the policy (<a href="'.$uri_1_de.'" target="_blank">'.$text_de.'</a>, <a href="'.$uri_1_en.'" target="_blank">'.$text_en.'</a>) and accept the author\'s contract (legally binding <a href="'.$uri_de.'" target="_blank">German version</a>, <a href="'.$uri_en.'" target="_blank">translated version</a>) to conclude the submission.')
                                ],
                                'accept_policy' => [
                                        '#type'     => 'checkbox',
                                        '#title'    => (string)t('I accept the author\'s contract'),
                                        '#suffix' => '',
                                        '#states'    => array(
                                                'required' => [
                                                        ':input[name="author_action"]' => ['value' => 'accept']
                                                ],
                                                'optional' => [
                                                        ':input[name="author_action"]' => ['!value' => 'accept'],
                                                ]
                                        )
                                ]
                        ];
            }
            
            $form['preview'] = [
                '#type' => 'link',
                '#url'  => \Drupal\Core\Url::fromRoute('publisso_gold.workflow.preview', ['wf_id' => $form_state->get('wf_id')]),
                '#title'=> (string)t('Preview'),
                '#attributes' => [
                        'class' => [
                                'btn', 'btn-warning'
                        ],
                        'target' => [
                                '_blank'
                        ]
                ],
                '#suffix' => '&nbsp;&nbsp;&nbsp;'
            ];
            
            if($form_state->get('action')){
                
                $action = $form_state->get('action');
                $need_submit = true;
                
                $form['author_action'] = [
                    '#type' => 'markup',
                    '#markup' => (string)t('Choosed action: ').$actions[$action],
                    '#prefix' => '<h3>',
                    '#suffix' => '</h3>'
                ];
                
                if($action == 'author_change_request'){
                    
                    $form['comment'] = [
                        '#title' => (string)t('Comment for editorial office'),
                        '#type' => 'textarea',
                        '#required' => true
                    ];
                }
                
                $form['back'] = [
                    '#type' => 'submit',
                    '#value' => (string)t('Back'),
                    '#submit' => ['::back'],
                    '#limit_validation_errors' => [],
                    '#button_type' => 'warning',
                    '#suffix' => '&nbsp;&nbsp;&nbsp;'
                ];
                
                if($need_submit === true){
                    
                    $form['submit'] = [
                        '#type' => 'submit',
                        '#value' => (string)t('Submit')
                    ];
                }
            }
            
            if($form_state->get('step') == 0){
                
                $form['submit'] = [
                    '#type' => 'submit',
                    '#value' => (string)t('Submit')
                ];
            }
        }
        
        $form['#cache'] = [
            'max-age' => 0
        ];
        
        return $form;
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function back(array $form, FormStateInterface $form_state){
        
        $step = $form_state->get('step');
        
        $form_state->set('action', null);
        $form_state->set('step', --$step);
        $form_state->setRebuild();
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $session = \Drupal::service('session');
        $step = $form_state->get('step');
        $objWorkflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
		
        if($step == 0){
            
            //author accepted -> redirect to Eic with state "editor clearing request"
            if($form_state->getValue('author_action') == 'accept'){
                
				$objWorkflow->setState('editor clearing request');
				$objWorkflow->unlock();
				
                //redirect to dashboard
                $form_state->setRedirect('publisso_gold.dashboard');
            }
            else{
                
                $form_state->set('action', $form_state->getValue('author_action'));
                $form_state->set('step', ++$step);
                $form_state->setRebuild();
            }
        }
        elseif($step == 1 && $form_state->get('action') == 'author_change_request'){
            
            $objWorkflow->setState('author change request');
			$objWorkflow->unlock();
            
            //save comment
            $comment = $form_state->getValue('comment');
            
            if($comment != ''){
                
                $comment = base64_encode($comment);
                
                \Drupal::database()
                    ->insert('rwPubgoldWorkflowComments')
                    ->fields([
                        'wfc_created_by_uid' => $session->get('user')['id'],
                        'wfc_comment' => $comment,
                        'wfc_wfid' => $objWorkflow->getElement('id')
                    ])
                    ->execute();
            }
            
            $form_state->setRedirect('publisso_gold.dashboard');
        }
        
        return $form;
    }
        
        /**
         * @param $tmpl
         * @param $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl, $vars){
        
        $tmpl = (string)$tmpl;
        
        $keys = array_keys($vars);
        $vars = array_values($vars);
        
        //set Site-Vars
        $tmpl = str_replace($keys, $vars, $tmpl);
        
        //remove unused vars
        return preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $tmpl);
     }
}
