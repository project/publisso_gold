<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\FormalAdaptBookChapterErratum.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;

/**
 * Provides chapter create form.
 */
class FormalAdaptBookChapterErratum extends FormBase {

        private $modname = 'publisso_gold';
        private $database;
        private $modpath;
        private $modaccessweights = [];
        
        const __ACCESS_DENIED__             = 'You can not access this page. You may need to log in first.';
        
        /**
         * {@inheritdoc}
         */
        public function __construct(){
        }
        
        /**
         * {@inheritdoc}
         */
        public function getFormId() {
                return 'formaladaptbookchaptererratum';
        }
  
        /**
         * {@inheritdoc}
         */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                $form_state->set('simpleValues', [
                        'title'                    ,
                        'conflict_of_interest'     ,
                        'conflict_of_interest_text',
                        'funding'                  ,
                        'funding_name'             ,
                        'funding_id'               ,
                        'accept_policy'            ,
                        'add_files'                ,
                        'doi'                      ,
                        'version'                  ,
                        'license'                  ,
                        'publication_place'        ,
                        'publication_year'         ,
                        'sub_category'             ,
                        'erratum'                  ,
                        'continue_action'          ,
                        'received'                 ,
                        'revised'                  ,
                        'accepted'                 ,
                        'published'
                ]);
                
                /**
                 * link some libraries
                 */
                $this->modpath = drupal_get_path('module', $this->modname);
                require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
                
                //save biuld-args
                $form_state->set('build_args', $args);
                
                /**
                 * try to load book. in case of failure, throw error-message
                 */
                $ret = $this->getBook(array_key_exists('bk_id', $args) ? $args['bk_id'] : null);
                
                if($ret['book'] === false){
                        return [
                                '#markup' => (string)$this->t('ERROR: '.$ret['msg'])
                        ];
                }
                
                $book = $ret['book'];
                if(!$form_state->has('book') || !is_object($form_state->get('book'))) $form_state->set('book', $book);
                
                /**
                 * try to load workflow. in case of failure, throw error-message
                 */
                $ret = $this->getWorkflow(array_key_exists('wf_id', $args) ? $args['wf_id'] : null);
                
                if($ret['workflow'] === false){
                        return [
                                '#markup' => (string)$this->t('ERROR: '.$ret['msg'])
                        ];
                }
                
                $workflow = $ret['workflow'];
                if(!$form_state->has('workflow') || !is_object($form_state->get('workflow'))) $form_state->set('workflow', $workflow);
                
                /**
                 * workflow has to be set in author change request. if not, abort.
                 */
                $allowedStates = [
                        'author change request',
                        'editor change request'
                ];
                if(!in_array($form_state->get('workflow')->getDataElement('state'), $allowedStates)){
                        return [
                                '#markup' => (string)$this->t('ERROR: Workflow has a wrong state (@state)!', ['@state' => $form_state->get('workflow')->getDataElement('state')])
                        ];
                }
                
                /**
                 * set some global parameters
                 */
                
                $link_book = Url::fromRoute('publisso_gold.books_book', ['bk_id' => $book->getElement('id')])->toString();
                $language = \Drupal::languageManager()->getCurrentLanguage();
                $this->database = \Drupal::database();
        
                //Link1 (uri): Authors contract
                //Link2 (uri_1): Book policy
                $uri_de         = 'https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/#c5185';
		$url_de         = Url::fromUri($uri_de);
		$uri_1_de       = 'https://www.publisso.de/open-access-publizieren/buecher/policy-buecher/';
		$url_1_de 	= Url::fromUri($uri_1_de);
		$text_de        = 'German';
	
		$uri_en         = 'https://www.publisso.de/en/publishing/books/books-policy/#c5189';
		$url_en 	= Url::fromUri($uri_en);
		$uri_1_en       = 'https://www.publisso.de/en/publishing/books/books-policy/';
		$url_1_en 	= Url::fromUri($uri_1_en);
		$text_en        = 'English';
                
                //initiate steps if not done
                if(!$form_state->get('step')) $form_state->set('step', 1);
                
                if($form_state->has('RUID') && $form_state->get('RUID')){
                        
                        $authors = json_decode($form_state->get('workflow')->getDataElement('authors'), true);
                        
                        for($i = 0; $i < count($authors); $i++){
                                
                                if(array_key_exists('uid', $authors[$i]) && $authors[$i]['uid'] == $form_state->get('RUID')){
                                        
                                        $user = new \Drupal\publisso_gold\Controller\User($authors[$i]['uid']);
                                      
                                        $authors[$i]['firstname'     ] = $user->profile->getElement('firstname'      );
                                        $authors[$i]['lastname'      ] = $user->profile->getElement('lastname'       );
                                        $authors[$i]['affiliation'   ] = implode('; ', array_filter([$user->profile->getElement('department'), $user->profile->getElement('institute'), getCountry($user->profile->getElement('country'))]));
                                        break;
                                }
                        }
                        
                        $form_state->get('workflow')->setDataElement('authors', json_encode($authors), false);
                        $form_data = $form_state->get('form_data');
                        
                        $form_data['author_db' ] = [];
                        $form_data['author_add'] = [];
                        
                        foreach(json_decode($form_state->get('workflow')->getDataElement('authors'), true) as $author){
                                
                                if(array_key_exists('uid', $author)){
                                        
                                        $user = new \Drupal\publisso_gold\Controller\User($author['uid']);
                                        
                                        if($user && $user->getElement('id')){
                                                
                                                $ary = [];
                                                
                                                $ary['author'] = implode(
                                                        '; ',
                                                        array_filter([
                                                                implode(
                                                                        ' ',
                                                                        array_filter([
                                                                                $author['firstname'],
                                                                                $author['lastname' ]
                                                                        ])
                                                                ),
                                                                $author['affiliation'],
                                                                $author['institute'  ],
                                                                $author['country'    ]
                                                        ])
                                                );
                                                
                                                $ary['is-corresponding' ] = $author['is_corresponding'  ];
                                                $ary['weight'           ] = $author['weight'            ];
                                                $ary['raw'              ] = $author                      ;
                                                
                                                $form_data['author_db'][] = $ary;
                                        }
                                }
                                else{
                                        $form_data['author_add'][] = $author;
                                }
                        }
                        
                        $form_state->set('form_data', $form_data);
                        $form_state->set('RUID', null);
                }
                
//load workflow-data unless already done
                if(!$form_state->get('wf_data_loaded') === true){
                        
                        $form_data = [
                        ];
                        
                        $key = 'abstract';
                        $form_data[$key] = base64_decode($form_state->get('workflow')->getDataElement($key));
                        
                        $key = 'chapter_text';
                        $form_data[$key] = base64_decode($form_state->get('workflow')->getDataElement($key));
                        
                        $key = 'keywords';
                        $form_data[$key] = implode('; ', json_decode($form_state->get('workflow')->getDataElement($key), true));
                        
                        $form_data['author_db' ] = [];
                        $form_data['author_add'] = [];
                        
                        foreach(json_decode($form_state->get('workflow')->getDataElement('authors'), true) as $author){
                                
                                if(array_key_exists('uid', $author)){
                                        
                                        $user = new \Drupal\publisso_gold\Controller\User($author['uid']);
                                        
                                        if($user && $user->getElement('id')){
                                                
                                                $ary = [];
                                                
                                                $ary['author'] = implode(
                                                        '; ',
                                                        array_filter([
                                                                implode(
                                                                        ' ',
                                                                        array_filter([
                                                                                $author['firstname'],
                                                                                $author['lastname' ]
                                                                        ])
                                                                ),
                                                                $author['affiliation'],
                                                                $author['institute'  ],
                                                                $author['country'    ]
                                                        ])
                                                );
                                                
                                                $ary['is-corresponding' ] = $author['is_corresponding'  ];
                                                $ary['weight'           ] = $author['weight'            ];
                                                $ary['raw'              ] = $author                      ;
                                                
                                                $form_data['author_db'][] = $ary;
                                        }
                                }
                                else{
                                        $form_data['author_add'][] = $author;
                                }
                        }
                        
                        $form_data['corporation'] = json_decode($form_state->get('workflow')->getDataElement('corporations'), true);
                        
                        //load simple data
                        
                        foreach($form_state->get('simpleValues') as $key)
                                $form_data[$key] = $form_state->get('workflow')->getDataElement($key);
                        
                        //echo '<pre>'.print_r($form_state->get('workflow'), 1).'</pre>';
                        $form_state->set('form_data', $form_data);
                        $form_state->set('wf_data_loaded', true);
                }
                
                /*#############################################################################################*/
                
                /**
                 * show the form.
                 * at this point should all parameters set
                 * and only form-depending things processed
                 */
                $form['#tree'] = true;
                
                $form['mu_comments'] = $this->getCommentsForAuthor($form_state->get('workflow')->getElement('id'));
                $form['mu_files'   ] = $this->getAssignedFiles($form_state->get('workflow')->getElement('id'));
                
                $form ['mu_site_headline'] = [
                        '#markup' => (string)$this->t('Set erratum for chapter'),
                        '#prefix' => '<h1>',
                        '#suffix' => '</h1>'
                ];
                
                $form['mu_book_title'] = [
                        '#markup' => $book->getElement('title'),
                        '#prefix' => '<h2>',
                        '#suffix' => '</h2>'
                ];
                
                $form['mu_wfstate'] = [
                        '#markup' => (string)t('State: @state', ['@state' => $form_state->get('workflow')->getDataElement('state')]),
                        '#prefix' => '<h6>',
                        '#suffix' => '</h6>'
                ];
                
                /**
                 * form step 1
                 */
/* Step 1 */    //if($form_state->get('step') == 1):
                        
                        //chapter-title
                        $form['title'] = [
                                '#type'         => 'textfield',
                                '#title'        => t('Title'),
                                '#required'     => true,
                                '#placeholder'  => (string)$this->t('Enter the title of your chapter here'),
                                '#maxlength'    => 255
                        ];
                        
                        if($form_state->get('form_data')['title']) $form['title']['#default_value'] = $form_state->get('form_data')['title'];
                
                        //abstract (if required)
                        if($book->getControlElement('abstract_required') == 1):
                                
                                $form['abstract'] = [
                                        '#type'         => 'textarea',
                                        '#title'        => (string)$this->t('Abstract'),
                                        '#required'     => true,
                                        '#prefix'       => (string)$this->t('If an abstract is required or optional, please enter it here.'),
                                        '#rows'         => 20,
                                        '#attributes'   => [
                                                'class'         => [
                                                	\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
                                                ]
                                        ]
                                ];
                                
                                if($form_state->get('form_data')['abstract']) $form['abstract']['#default_value'] = $form_state->get('form_data')['abstract'];

                        endif;
                        
                        //keywords
                        $form['keywords'] = [
                                '#type'                 => 'textfield',
                                '#title'                => (string)t('Keywords'),
                                '#default_value'        => implode(';', json_decode($form_state->get('form_data')['keywords'])),
                                '#placeholder'          => (string)$this->t('Enter the keywords separated with a semicolon here'),
                                '#maxlength'            => 400
                        ];
                        
                        if($form_state->get('form_data')['keywords']) $form['keywords']['#default_value'] = $form_state->get('form_data')['keywords'];
                        
                        //chapter text
                        $form['chapter_text'] = [
                                '#type'         => 'textarea',
                                '#title'        => t('Main Text'),
                                '#required'     => true,
                                '#rows'         => 50,
                                '#attributes'   => [
                                        'class'         => [
                                        	\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
                                        ]
                                ]
                        ];
                        
                        if($form_state->get('form_data')['chapter_text']) $form['chapter_text']['#default_value'] = $form_state->get('form_data')['chapter_text'];
                        
                        //authors
                        if(count($form_state->get('form_data')['author_db' ]) && $form_state->getRebuildInfo('step1') != true) $form_state->set('cntAuthorsDB' , count($form_state->get('form_data')['author_db' ]));
                        if(count($form_state->get('form_data')['author_add']) && $form_state->getRebuildInfo('step1') != true) $form_state->set('cntAuthorsAdd', count($form_state->get('form_data')['author_add']));
                        if(!$form_state->has('cntAuthorsDB' ) || $form_state->get('cntAuthorsDB') < 3){ $form_state->set('cntAuthorsDB' , 3); }
                        if(!$form_state->has('cntAuthorsAdd')){ $form_state->set('cntAuthorsAdd', 1); }
                        
                        $form['fs_authors'] = [
                                '#type' => 'fieldset',
                                '#title' => (string)$this->t('Author(s)'),
                                '#prefix' => '<br>',
                                'content' => [
                                        'description' => [
                                                '#markup' => (string)$this->t('Enter one author per box. Authors who are not yet in the database can be added as additional authors'),
                                                '#suffix' => '<br><br>'
                                        ],
                                        'author-db' => [
                                                '#type'   => 'container',
                                                '#prefix' => '<div id="author-db-wrapper">',
                                                '#suffix' => '</div>',
                                                '#tree'   => true,
                                                'content' => [
                                                        '#type' => 'table',
                                                        '#header' => [
                                                                (string)$this->t('Author(s)'),
                                                                (string)$this->t('Corresponding'),
                                                                (string)$this->t('Position')
                                                        ]
                                                ]
                                        ],
                                        'author-db-add' => [
                                                '#type'                    => 'submit',
                                                '#button_type'             => 'warning',
                                                '#value'                   => (string)$this->t('add box'),
                                                '#limit_validation_errors' => [],
                                                '#submit'                  => ['::addAuthorDB'],
                                                '#prefix'                  => '',
                                                '#suffix'                  => '<br><br>',
                                                '#description'             => (string)$this->t('Add further authors'),
                                                '#ajax'                    => [
                                                        'wrapper'       => 'author-db-wrapper',
                                                        'effect'        => 'fade',
                                                        'callback'      => '::addAuthorDBCallback'
                                                ]
                                        ],
                                        'author-add' => [
                                                '#type'    => 'container',
                                                '#prefix'  => '<div id="author-add-wrapper">',
                                                '#suffix'  => '</div>',
                                                '#tree'    => true,
                                                'content' => []
                                        ],
                                        'author-add-add' => [
                                                '#type'                    => 'submit',
                                                '#button_type'             => 'warning',
                                                '#value'                   => (string)$this->t('add box '), //Space is important!
                                                '#limit_validation_errors' => [],
                                                '#submit'                  => ['::addAuthorAdd'],
                                                '#prefix'                  => '<br>',
                                                '#description'             => (string)$this->t('For more additional authors, add more boxes'),
                                                '#ajax'                    => [
                                                        'wrapper'       => 'author-add-wrapper',
                                                        'effect'        => 'fade',
                                                        'callback'      => '::addAuthorAddCallback'
                                                ]
                                        ]
                                ]
                        ];
                        
                        for($i = 0; $i < $form_state->get('cntAuthorsDB'); $i++){
                                
                                $form['fs_authors']['content']['author-db']['content'][$i] = [
                                        'author'           => [
                                                'field' => [
                                                        '#type'                          => 'textfield',
                                                        '#autocomplete_route_name'       => 'autocomplete.user.public',
                                                        '#autocomplete_route_parameters' => array(),
                                                        '#title'                         => (string)$this->t('Author(s)'),
                                                        '#title_display'                 => 'invisible',
                                                        '#placeholder'                   => (string)$this->t('Enter at least 3 characters of the name and choose from the database'),
                                                        '#description'                   => (string)$this->t('Matching entries from the database will be suggested automatically'),
                                                        '#maxlength'                     => 256,
                                                        '#id'                            => 'author_db_'.$i,
                                                        '#ajax'                          => [
                                                                'callback'               => '::AuthorDBSelect',
                                                                'event'                  => 'autocompleteclose',
                                                                'progress'               => false
                                                        ]
                                                ]
                                        ],
                                        'is-corresponding' => [
                                                '#type' => 'checkbox'
                                        ],
                                        'weight'           => [
                                                '#type'          => 'number',
                                                '#size'          => 3,
                                                '#min'           => 1,
                                                '#max'           => 100,
                                                '#description'   => (string)$this->t('The authors’ order can be determined here')
                                        ]
                                ];
                                
                                if(isset($form_state->get('form_data')['author_db'][$i]['author'])){
                                
                                        $form['fs_authors']['content']['author-db']['content'][$i]['author'          ]['field']['#default_value'] = $form_state->get('form_data')['author_db'][$i]['author' ];
                                        $form['fs_authors']['content']['author-db']['content'][$i]['is-corresponding']['#default_value'] = $form_state->get('form_data')['author_db'][$i]['is-corresponding'];
                                        $form['fs_authors']['content']['author-db']['content'][$i]['weight'          ]['#default_value'] = $form_state->get('form_data')['author_db'][$i]['weight'          ];
                                        
                                        if(array_key_exists('raw', $form_state->get('form_data')['author_db'][$i])){
                                                
                                                $form['fs_authors']['content']['author-db']['content'][$i]['author']['field']['#disabled'] = true;
                                                unset($form['fs_authors']['content']['author-db']['content'][$i]['author']['field']['#autocomplete_route_name'          ]);
                                                unset($form['fs_authors']['content']['author-db']['content'][$i]['author']['field']['#autocomplete_route_parameters'    ]);
                                                
                                                $form['fs_authors']['content']['author-db']['content'][$i]['author']['raw'] = [
                                                        '#type'  => 'details',
                                                        '#title' => (string)$this->t('Actions for @author', ['@author' => implode(' ', array_filter([$form_state->get('form_data')['author_db'][$i]['raw']['firstname'], $form_state->get('form_data')['author_db'][$i]['raw']['lastname']]))]),
                                                        'data'   =>[
                                                                '#type'        => 'fieldset',
                                                                '#description' => (string)$this->t('Here you can manually overwrite the author\'s data. If the author is no longer assigned, this should be marked as "delete".'),
                                                                'fields' => [
                                                                        '#prefix'      => '<div id="author-db-'.$i.'">',
                                                                        '#suffix'      => '</div>',
                                                                        'firstname'    => [
                                                                                '#type'         => 'textfield',
                                                                                '#placeholder'  => (string)$this->t('Firstname'),
                                                                                '#disabled'     => true
                                                                        ],
                                                                        'lastname'     => [
                                                                                '#type'         => 'textfield',
                                                                                '#placeholder'  => (string)$this->t('Lastname'),
                                                                                '#disabled'     => true
                                                                        ],
                                                                        'affiliation'  => [
                                                                                '#type'         => 'textfield',
                                                                                '#placeholder'  => (string)$this->t('Affiliation'),
                                                                                '#disabled'     => true
                                                                        ]
                                                                ],
                                                                'delete'       => [
                                                                        '#type'         => 'checkbox',
                                                                        '#title'        => (string)$this->t('Delete @author', ['@author' => implode(' ', array_filter([$form_state->get('form_data')['author_db'][$i]['raw']['firstname'], $form_state->get('form_data')['author_db'][$i]['raw']['lastname']]))]),
                                                                        '#attributes'   => [
                                                                                'style'         => 'margin: 0 0 0 -20px !important'
                                                                        ]
                                                                ],
                                                                'setdata'      => [
                                                                        '#type'         => 'submit',
                                                                        '#value'        => (string)$this->t('Get data from db'),
                                                                        '#ajax'         => [
                                                                                'callback'      => '::setAuthorDBDataCallback',
                                                                                'wrapper'       => 'author-db-'.$i,
                                                                                'effect'        => 'none',
                                                                                'progress'      => [
                                                                                        'type'          => 'throbber'
                                                                                ]
                                                                        ],
                                                                        '#limit_validation_errors' => [],
                                                                        '#submit'       => ['::setAuthorDBData']
                                                                ]
                                                        ]
                                                ];
                                                
                                                $form['fs_authors']['content']['author-db']['content'][$i]['author']['raw']['data']['fields']['firstname'  ]['#default_value'] = $form_state->get('form_data')['author_db'][$i]['raw']['firstname'  ];
                                                $form['fs_authors']['content']['author-db']['content'][$i]['author']['raw']['data']['fields']['lastname'   ]['#default_value'] = $form_state->get('form_data')['author_db'][$i]['raw']['lastname'   ];
                                                $form['fs_authors']['content']['author-db']['content'][$i]['author']['raw']['data']['fields']['affiliation']['#default_value'] = $form_state->get('form_data')['author_db'][$i]['raw']['affiliation'];
                                                
                                                $form['fs_authors']['content']['author-db']['content'][$i]['author']['raw']['data']['setdata']['#name'] = $form_state->get('form_data')['author_db'][$i]['raw'];
                                                $form['fs_authors']['content']['author-db']['content'][$i]['author']['raw']['data']['setdata']['#name']['index'] = $i;
                                        }
                                }
                        }
                        
                        for($i = 0; $i < $form_state->get('cntAuthorsAdd'); $i++){
                                
                                $form['fs_authors']['content']['author-add']['content']['author-add-'.$i] = [
                                        '#type'                          => 'fieldset',
                                        '#tree'                          => true,
                                        '#title'                         => (string)$this->t('Additional author @nr', ['@nr' => ($i + 1)]),
                                        'content' => [
                                                'firstname'   => [
                                                        '#type' => 'textfield',
                                                        '#title' => (string)$this->t('Firstname'),
                                                        '#placeholder' => (string)$this->t('Enter the firstname of additional author #@nr', ['@nr' => ($i + 1)])
                                                ],
                                                'lastname'    => [
                                                        '#type' => 'textfield',
                                                        '#title' => (string)$this->t('Lastname'),
                                                        '#placeholder' => (string)$this->t('Enter the lastname of additional author #@nr', ['@nr' => ($i + 1)])
                                                ],
                                                'affiliation' => [
                                                        '#type' => 'textfield',
                                                        '#title' => (string)$this->t('Affiliation'),
                                                        '#placeholder' => (string)$this->t('Enter the affiliation of additional author #@nr', ['@nr' => ($i + 1)])
                                                ],
                                                'weight'      => [
                                                        '#type'          => 'number',
                                                        '#size'          => 3,
                                                        '#title'         => (string)$this->t('Position')
                                                ]
                                        ]
                                ];
                                
                                if(isset($form_state->get('form_data')['author_add'][$i])){
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['firstname'  ]['#default_value'] = $form_state->get('form_data')['author_add'][$i]['firstname'  ];
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['lastname'   ]['#default_value'] = $form_state->get('form_data')['author_add'][$i]['lastname'   ];
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['affiliation']['#default_value'] = $form_state->get('form_data')['author_add'][$i]['affiliation'];
                                        $form['fs_authors']['content']['author-add']['content']['author-add-'.$i]['content']['weight'     ]['#default_value'] = $form_state->get('form_data')['author_add'][$i]['weight'     ];
                                }
                        }
                        
                        //corporations
                        if(count($form_state->get('form_data')['corporation']) && $form_state->getRebuildInfo('step1') != true) $form_state->set('cntCorporations' , count($form_state->get('form_data')['corporation']));
                        if(!$form_state->has('cntCorporations')){ $form_state->set('cntCorporations', 1); }
                        
                        $form['corporations'] = [
                                '#type'   => 'container',
                                '#prefix' => '<div id="corporations-wrapper">',
                                '#suffix' => '</div>',
                                '#tree'   => true,
                                'content' => []
                        ];
                        
                        $form['corporations-add'] = [
                                '#type'                    => 'submit',
                                '#button_type'             => 'warning',
                                '#value'                   => (string)$this->t('add corporation'), //Space is important!
                                '#limit_validation_errors' => [],
                                '#submit'                  => ['::addCorporation'],
                                '#prefix'                  => '<br>',
                                '#description'             => (string)$this->t('Add more corporations'),
                                '#suffix'                  => '<br><br>',
                                '#ajax'                    => [
                                        'wrapper'       => 'corporations-wrapper',
                                        'effect'        => 'fade',
                                        'callback'      => '::addCorporationCallback'
                                ]
                        ];
                        
                        for($i = 0; $i < $form_state->get('cntCorporations'); $i++){
                                
                                $form['corporations']['content'][$i] = [
                                        '#type'          => 'textfield',
                                        '#title'         => (string)$this->t('Corporation(s)'),
                                        '#title_display' => $i ? 'invisible' : 'before',
                                        '#placeholder'   => (string)$this->t('Enter corporations that should appear as authors here'),
                                        '#description'   => (string)$this->t('One corporation per box')
                                ];
                                
                                if(isset($form_state->get('form_data')['corporation'][$i])) $form['corporations']['content'][$i]['#default_value'] = $form_state->get('form_data')['corporation'][$i];
                        }
                        
                        $form['doi'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t('DOI'),
                                '#default_value' => $form_state->get('form_data')['doi']
                        ];
                        
                        $form['version'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t('Version'),
                                '#default_value' => $form_state->get('form_data')['version']
                        ];
                        
/* Step 2 */    //elseif($form_state->get('step') == 2):
                        
                        //conflict of interest
                        $form['conflict_of_interest'] = [
                                '#type'         => 'checkbox',
                                '#title'        => (string)t('Conflict of interest'),
                                '#prefix'       => (string)$this->t('Here you may add if there’s any conflict of interest to declare, which might be relevant regarding the development of your chapter. If there’s anything to declare you can add a description in the appearing box.')
                        ];
                        
                        if(isset($form_state->get('form_data')['conflict_of_interest'])) $form['conflict_of_interest']['#default_value'] = $form_state->get('form_data')['conflict_of_interest'];
                        
                        $form['conflict_of_interest_text'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t('Description of conflict of interest'),
                                '#maxlength'     => 256,
                                '#default_value' => isset($workflow_data) ? $workflow_data->conflict_of_interest_text : '',
                                '#states'        => array(
                                        'visible'       => array(   // action to take.
                                                ':input[name="conflict_of_interest"]' => array('checked' => true),
                                        ),
                                        'invisible'     => array(   // action to take.
                                                ':input[name="conflict_of_interest"]' => array('checked' => false),
                                        )
                                )
                        ];
                        
                        if(isset($form_state->get('form_data')['conflict_of_interest_text'])) $form['conflict_of_interest_text']['#default_value'] = $form_state->get('form_data')['conflict_of_interest_text'];
                        
                        //funding
                        $form['funding'] = [
                                '#type'         => 'checkbox',
                                '#title'        => t('Funds received'),
                                '#prefix'       => (string)$this->t('If you received any funds please add the information here.')
                        ];
                        
                        if(isset($form_state->get('form_data')['funding'])) $form['funding']['#default_value'] = $form_state->get('form_data')['funding'];
                        
                        $form['funding_name'] = [
                                '#type'      => 'textfield',
                                '#title'     => (string)$this->t('Funding name'),
                                '#maxlength' => 256,
                                '#states'    => array(
                                        'visible'   => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => true),
                                        ),
                                        'invisible' => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => false),
                                        )
                                ),
                        ];
                        
                        if(isset($form_state->get('form_data')['funding_name'])) $form['funding_name']['#default_value'] = $form_state->get('form_data')['funding_name'];
                        
                        $form['funding_id'] = [
                                '#type'   => 'textfield',
                                '#title'  => (string)t('Funding ID'),
                                '#states' => array(
                                        'visible'   => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => true),
                                        ),
                                        'invisible' => array(   // action to take.
                                                ':input[name="funding"]' => array('checked' => false),
                                        )
                                ),
                        ];
                        
                        if(isset($form_state->get('form_data')['funding_id'])) $form['funding_id']['#default_value'] = $form_state->get('form_data')['funding_id'];
                        
                        $form['license'] = [
                                '#type'          => 'select',
                                '#title'         => t('License'),
                                '#required'      => true,
                                '#options'       => \Drupal::service('publisso_gold.tools')->getLicenses(),
                                '#default_value' => $form_state->get('form_data')['license']
                        ];
                        
                        $form['publication_place'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t('Publication place'),
                                '#required'      => true,
                                '#default_value' => isset($form_state->get('form_data')['publication_place']) ? $form_state->get('form_data')['publication_place'] : $book->getElement('publication_place')
                        ];
                        
                        $form['publication_year'] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t('Publication year'),
                                '#required'      => true,
                                '#default_value' => isset($form_state->get('form_data')['publication_year']) ? $form_state->get('form_data')['publication_year'] : $book->getElement('publication_year')
                        ];
                        
                        $form['sub_category'] = [
                                '#type'          => 'textfield',
                                '#title'         => t('Subcategory'),
                                '#required'      => true,
                                '#default_value' => isset($form_state->get('form_data')['sub_category']) ? $form_state->get('form_data')['sub_category'] : $book->getElement('sub_category')
                        ];
                        
                        //accept policy
                        $form['accept_policy'] = [
                                '#type'     => 'checkbox',
                                '#title'    => (string)t('I accept the author\'s contract</a>'),
                                '#required' => true,
                                '#disabled' => true
                        ];
                        
                        if(isset($form_state->get('form_data')['accept_policy'])) $form['accept_policy']['#default_value'] = $form_state->get('form_data')['accept_policy'];
                        true;

                $form['override_dates'] = [
                        '#type' => 'details',
                        '#title' => (string)$this->t('Override dates'),
                        '#tree' => false,
                        'dates' => [
                                'received' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Received'),
                                        '#default_value' => $form_state->get('form_data')['received'] ? date("Y-m-d", strtotime($form_state->get('form_data')['received'])) : ($form_state->get('workflow')->getStateChainElement('submission finished') ? date("Y-m-d",strtotime($form_state->get('workflow')->getStateChainElement('submission finished'))) :false) ?? ""
                                ],
                                'revised' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Revised'),
                                        '#default_value' => $form_state->get('form_data')['revised'] ? date("Y-m-d", strtotime($form_state->get('form_data')['revised'])) : ($form_state->get('workflow')->getStateChainElement('revision finished') ? date("Y-m-d",strtotime($form_state->get('workflow')->getStateChainElement('revision finished'))) :false) ?? ""
                                ],
                                'accepted' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Accepted'),
                                        '#default_value' => $form_state->get('form_data')['accepted'] ? date("Y-m-d", strtotime($form_state->get('form_data')['accepted'])) : ($form_state->get('workflow')->getStateChainElement('accepted') ? date("Y-m-d",strtotime($form_state->get('workflow')->getStateChainElement('accepted'))) :false) ?? ""
                                ],
                                'published' => [
                                        '#type' => 'date',
                                        '#title' => (string)$this->t('Published'),
                                        '#default_value' => $form_state->get('form_data')['published'] ? date("Y-m-d", strtotime($form_state->get('form_data')['published'])) : ($form_state->get('workflow')->getStateChainElement('published') ? date("Y-m-d",strtotime($form_state->get('workflow')->getStateChainElement('published'))) :false) ?? ""
                                ]
                        ]
                ];
                        
/* Step 3 */    //elseif($form_state->get('step') == 3):

                        
                //endif;
                
                /*#############################################################################################*/
                
                $form['erratum'] = [
                        '#type'         => 'textarea',
                        '#title'        => (string)t('Erratum'),
                        '#suffix'       => '<br>',
                        '#prefix'       => '<br>',
                        '#default_value'=> $form_state->get('form_data')['erratum'],
                        '#attributes'   => [
                                'class'         => [
                                	\Drupal::service('publisso_gold.setup')->getValue('submission.text_editor')
                                ]
                        ]
                ];
                
                $form['spacer'] = [
                        '#markup' => '<hr>'
                ];
                
                $form['close'] = [
                        '#type'                         => 'submit',
                        '#default_value'                => (string)$this->t('Back to dashboard'),
                        '#button_type'                  => 'warning',
                        '#suffix'                       => '&nbsp;&nbsp;&nbsp;',
                        '#limit_validation_errors'      => [],
                        '#submit'                       => ['::backToDashboard']
                ];
                
                $form['preview'] = [
                        '#type'                         => 'button',
                        '#value'                        => (string)$this->t('Preview'),
                        '#button_type'                  => 'primary',
                        '#suffix'                       => '&nbsp;&nbsp;&nbsp;',
                        '#limit_validation_errors'      => [],
                        '#description'                  => (string)$this->t('See how your chapter looks like'),
                        '#ajax'                         => [
                                'callback' => '::previewBookchapter',
                                'progress' => [
                                        'type' => 'none'
                                ]
                        ]
                ];
                
                $form['genpdf'] = [
                        '#type'                         => 'button',
                        '#value'                        => (string)$this->t('Create PDF'),
                        '#button_type'                  => 'primary',
                        '#suffix'                       => '&nbsp;&nbsp;&nbsp;',
                        '#limit_validation_errors'      => [],
                        '#description'                  => (string)$this->t('Create PDF'),
                        '#ajax'                         => [
                                'callback' => '::genPDF',
                                'progress' => [
                                        'type' => 'none'
                                ]
                        ]
                ];
                
                $form['finish'] = [
                        '#type'         => 'submit',
                        '#value'        => (string)$this->t('Finish'),
                        '#button_type'  => 'success',
                        '#submit'       => ['::processForm']
                ];
                
                $form['store_data_control_key'] = [
                        '#type'         => 'hidden',
                        '#value'        => 'previewchapter'
                ];
                
                $form['ctrl_action'] = [
                        '#type' => 'hidden',
                        '#value' => 'autosave'
                ];
                
                $form['ctrl_type'] = [
                        '#type' => 'hidden',
                        '#value' => 'bookchapter'
                ];
                
                $form['tmp_id'] = [
                        '#type' => 'hidden',
                        '#id' => 'tmp_id'
                ];
                
                if(!empty($form_state->get('form_data')['tmp_id'])){
                        $form['tmp_id']['#value'] = $form_state->get('form_data')['tmp_id'];
                }
                
                $form['bk_id'] = [
                        '#type' => 'hidden',
                        '#value' => $form_state->get('book')->getElement('id')
                ];
                
                $form['#cache'] = [
                        'max-age' => 0
                ];
                
                $form_state->setCached(false);
                
                return $form;
        }
        
        /**
         * {@inheritdoc}
         */
        public function validateForm(array &$form, FormStateInterface $form_state){
                
                foreach($form_state->getValue('fs_authors')['content']['author-db']['content'] as $_){
                
                        if(!array_key_exists('raw', $_['author'])){

                                list($author, $data) = preg_split('/;\s{1,}/', $_['author']['field'], 2);
                                
                                if(!empty($author)){
                                        $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                        $found = 0;
                                        $last_id = null;
                                        $string = '';

                                        foreach($res as $v){
                                                $string .= ' || '.implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)])).' | '.implode( '; ', [$author, $data]);
                                                if(
                                                        implode( '; ', [$author, $data])
                                                                        == 
                                                        implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)]))
                                                ){ 
                                                        $found++; 
                                                        $last_id = $v->up_uid;
                                                }
                                        }

                                        if($found != 1){
                                                $form_state->setError($form, (string)$this->t('Please use only suggested values in author-field #@i!', ['@i' => $i + 1]));
                                        }
                                }
                        }
                        
                        $i++;
                }
                
                return $form;
        }
        
        /**
         * {@inheritdoc}
         */
        public function submitForm(array &$form, FormStateInterface $form_state){
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return AjaxResponse
         */
        public function genPDF(array &$form, FormStateInterface $form_state){
                
                $this->processForm($form, $form_state, false);
                
                //get stored form-data
                $form_data = $form_state->get('form_data');
                
                //get the workflow
                $workflow = $form_state->get('workflow');
                
                $workflow->reload();
                
                $workflow->historize();
                
                //set the data
                $this->storeFormData2Workflow($workflow, $form_state, true);
                $workflow->save();
                $workflow->publish(false, true);
                
                $url = Url::fromRoute('publisso_gold.export.chapter.volatile', ['cp_id' => $workflow->getDataElement('cp_id')]);
                $url->setAbsolute();
                
                $chapter = new \Drupal\publisso_gold\Controller\Bookchapter($workflow->getDataElement('cp_id'));
                
                
                
                $blobID = $chapter->getElement('pdf_blob_id');
                
                if($blobID){
                        $blob = new \Drupal\publisso_gold\Controller\Blob($blobID);
                        $blob->delete();
                        $chapter->setElement('pdf_blob_id', null);
                }
                
                $response = new AjaxResponse();
                $command = new InvokeCommand(
                                'html',
                                'trigger',
                                [
                                        'openPDF',
                                        [
                                                'url' => $url->toString()
                                        ]
                                ]
                                );
                $response->addCommand($command);
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function processForm(array &$form, FormStateInterface $form_state){
                
                //init temp sorage if not done
                if(!$form_state->has('form_data')) $form_state->set('form_data', []);
                
                /**
                 * store formdata in a temporary storage
                 */
                $form_data = $form_state->get('form_data');
                
                $form_data['title'] = $form_state->getValue('title');
                $form_data['keywords'] = $form_state->getValue('keywords');
                $form_data['chapter_text'] = $form_state->getValue('chapter_text');
                
                if($form_state->hasValue('abstract'))
                        $form_data['abstract'] = $form_state->getValue('abstract');
                
                //authors in db
                $form_data['author_db'] = [];
                
                foreach($form_state->getValue('fs_authors')['content']['author-db']['content'] as $authorDB){
                        if(!empty($authorDB['author'])) $form_data['author_db'][] = $authorDB;
                }
                
                //additional authors
                $form_data['author_add'] = [];
                foreach($form_state->getValue('fs_authors')['content']['author-add']['content'] as $authorAdd){
                        if(!empty($authorAdd['content']['firstname'  ]) &&
                           !empty($authorAdd['content']['lastname'   ]) &&
                           !empty($authorAdd['content']['affiliation'])){
                                $form_data['author_add'][] = $authorAdd['content'];
                        }
                }
                
                //corporations
                $form_data['corporation'] = [];
                foreach($form_state->getValue('corporations')['content'] as $corporation){
                        if(!empty($corporation)) $form_data['corporation'][] = $corporation;
                }
                
                foreach($form_state->get('simpleValues') as $key){
                        $form_data[$key] = $form_state->getValue($key);
                }
                
                //store $form_data
                $form_state->set('form_data', $form_data);
                
                $this->finishAdaption($form, $form_state);
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        protected function processUserInput(array &$form, FormStateInterface $form_state){
                
                //init temp sorage if not done
                if(!$form_state->has('form_data')) $form_state->set('form_data', []);
                
                /**
                 * store formdata in a temporary storage
                 */
                $form_data = $form_state->get('form_data');
                $form_raw  = $form_state->getUserInput();
                
                $form_data['title'] = $form_raw['title'];
                $form_data['keywords'] = $form_raw['keywords'];
                $form_data['chapter_text'] = $form_raw['chapter_text'];
                
                if(array_key_exists('abstract', $form_raw))
                        $form_data['abstract'] = $form_raw['abstract'];
                
                //authors in db
                $form_data['author_db'] = [];
                
                foreach($form_raw['fs_authors']['content']['author-db']['content'] as $authorDB){
                        if(!empty($authorDB['author'])) $form_data['author_db'][] = $authorDB;
                }
                
                //additional authors
                $form_data['author_add'] = [];
                foreach($form_raw['fs_authors']['content']['author-add']['content'] as $authorAdd){
                        if(!empty($authorAdd['content']['firstname'  ]) &&
                           !empty($authorAdd['content']['lastname'   ]) &&
                           !empty($authorAdd['content']['affiliation'])){
                                $form_data['author_add'][] = $authorAdd['content'];
                        }
                }
                
                //corporations
                $form_data['corporation'] = [];
                foreach($form_raw['corporations']['content'] as $corporation){
                        if(!empty($corporation)) $form_data['corporation'][] = $corporation;
                }
                
                foreach($form_state->get('simpleValues') as $key){
                        if(array_key_exists($key, $form_raw)) $form_data[$key] = $form_raw[$key];
                }
                
                //store $form_data
                $form_state->set('form_data', $form_data);
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        protected function finishAdaption(array &$form, FormStateInterface $form_state){
                
                if(!is_object($form_state->get('workflow'))) $form_state->set('workflow', new \Drupal\publisso_gold\Controller\Workflow($form_state->get('build_args')['wf_id']));
                
                if(!is_object($form_state->get('book'))) $form_state->set('book', new \Drupal\publisso_gold\Controller\Book($form_state->get('build_args')['bk_id']));
                if(!is_object($form_state->get('chapter'))) $form_state->set('chapter', new \Drupal\publisso_gold\Controller\Bookchapter($form_state->get('build_args')['cp_id']));
                
                //get the book
                $book = $form_state->get('book');
                
                //get stored form-data
                $form_data = $form_state->get('form_data');
                
                //get the workflow
                $workflow = $form_state->get('workflow');
                $workflow->reload();
                
                //set the data
                $this->storeFormData2Workflow($workflow, $form_state);
                
                $workflow->save();
                $workflow->setState('author clearing request');
                $workflow->unlock();
                
                $form_state->setRedirect('publisso_gold.dashboard');
                return $form;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthorDB(array &$form, FormStateInterface $form_state){
                $cntAuthorsDB = $form_state->get('cntAuthorsDB');
                $cntAuthorsDB++;
                $form_state->set('cntAuthorsDB', $cntAuthorsDB);
                $form_state->addRebuildInfo('step1', true);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorDBCallback(array &$form, FormStateInterface $form_state){
                return $form['fs_authors']['content']['author-db'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthorAdd(array &$form, FormStateInterface $form_state){
                $cntAuthorsAdd = $form_state->get('cntAuthorsAdd');
                $cntAuthorsAdd++;
                $form_state->set('cntAuthorsAdd', $cntAuthorsAdd);
                $form_state->addRebuildInfo('step1', true);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorAddCallback(array &$form, FormStateInterface $form_state){
                return $form['fs_authors']['content']['author-add'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addCorporation(array &$form, FormStateInterface $form_state){
                $cntCorporations = $form_state->get('cntCorporations');
                $cntCorporations++;
                $form_state->set('cntCorporations', $cntCorporations);
                $form_state->addRebuildInfo('step1', true);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addCorporationCallback(array &$form, FormStateInterface $form_state){
                return $form['corporations'];
        }
        
        /**
         * @param $bk_id
         * @return array
         */
        protected function getBook($bk_id){
                
                if(empty($bk_id)){
                        return [
                                'msg' => (string)$this->t('Initiate submission without book! Abort!'),
                                'book' => false
                        ];
                }
                
                //try to load book. if fails, throw an error!
                $book = new \Drupal\publisso_gold\Controller\Book($bk_id);
                
                if(!count($book->getKeys())){
                        return [
                                'msg' => (string)$this->t('Book (@bk_id) not found! Abort!', ['@bk_id' => $bk_id]),
                                'book' => false
                        ];
                }
                
                return [
                        'msg' => 'OK',
                        'book' => $book
                ];
        }
        
        /**
         * @param $cp_id
         * @return array
         */
        protected function getChapter($cp_id){
                
                if(empty($cp_id)){
                        return [
                                'msg' => (string)$this->t('Initiate erratum without chapter! Abort!'),
                                'chapter' => false
                        ];
                }
                
                //try to load book. if fails, throw an error!
                $chapter = new \Drupal\publisso_gold\Controller\Bookchapter($cp_id);
                
                if(!count($chapter->getKeys())){
                        return [
                                'msg' => (string)$this->t('Chapter (@cp_id) not found! Abort!', ['@cp_id' => $cp_id]),
                                'chapter' => false
                        ];
                }
                
                return [
                        'msg' => 'OK',
                        'chapter' => $chapter
                ];
        }
        
        /**
         * @param $wf_id
         * @return array
         */
        protected function getWorkflow($wf_id){
                
                if(empty($wf_id)){
                        return [
                                'msg' => (string)$this->t('Initiate revision without workflow! Abort!'),
                                'workflow' => false
                        ];
                }
                
                //try to load workflow. if fails, throw an error!
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
                
                if(!count($workflow->getKeys())){
                        return [
                                'msg' => (string)$this->t('Workflow (@wf_id) not found! Abort!', ['@wf_id' => $wf_id]),
                                'workflow' => false
                        ];
                }
                
                return [
                        'msg' => 'OK',
                        'workflow' => $workflow
                ];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addOne(array &$form, FormStateInterface $form_state) {
                
                $numFiles = $form_state->get('numFiles');
                $form_state->set('numFiles', ++$numFiles);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addmoreCallback(array &$form, FormStateInterface $form_state) {
                return $form['files_fieldset'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function removeCallback(array &$form, FormStateInterface $form_state) {
                
                $numFiles = $form_state->get('numFiles');
                if($numFiles > 0) $form_state->set('numFiles', --$numFiles);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @return AjaxResponse
         */
        public function delFile(array &$form, FormStateInterface $form_state, $args = []) {
                
                $fid = $form_state->getTriggeringElement()['fid'];
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $response = new AjaxResponse();
                
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                $fids = array_diff($fids, [$fid]);
                $workflow->setDataElement('files', serialize($fids));
                
                //check if file are deleted
                //reload Workflow
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                
                if(array_search($fid, $fids)){
                        
                        $file = new \Drupal\publisso_gold\Controller\Blob($fid);
                        $response->addCommand(new AppendCommand('div#rwBlob-'.$fid, '<div class="rwError">'.((string)$this->t('Error deleting file "'.$file->meta['name'].'"')).'</div>'));
                }
                else{
                        $response->addCommand(new RemoveCommand('div#rwBlob-'.$fid));
                }
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return AjaxResponse
         */
        public function previewBookchapter(array &$form, FormStateInterface $form_state){
                
                /*
                 switch($form_state->get('step')){
                 case 1:
                 $this->processForm($form, $form_state, false);
                 break;
                 case 2:
                 $this->processStep2($form, $form_state);
                 break;
                 case 3:
                 $this->processStep3($form, $form_state);
                 break;
                 }*/
                $this->processUserInput($form, $form_state);
                $data = $form_state->get('form_data');
                
                $data['data_type'] = 'bookchapter';
                $data['type'] = 'bcs';
                $data['state'] = 'in submission';
                $data['bk_id'] = $form_state->get('book')->getElement('id');
                
                
                
                $data = serialize($data);
                
                $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold');
                $tempstore->set('preview_bookchapter', $data);
                
                $response = new AjaxResponse();
                $command = new InvokeCommand(
                                'html',
                                'trigger',
                                [
                                        'previewBookchapter',
                                        [
                                                'form' => $this->getFormId(),
                                                'bk_id'=> $form_state->get('book')->getElement('id'),
                                                'data' => base64_encode(json_encode($form_state->get('form_data')))
                                        ]
                                ]
                                );
                $response->addCommand($command);
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function backToDashboard(array &$form, FormStateInterface $form_state){
                
                /*
                $workflow = $form_state->get('workflow');
                $workflow->reload();
                $workflow->historize();
                
                $call = "processUserInput";
                if(method_exists($this, $call)) $this->$call($form, $form_state);
                
                $this->storeFormData2Workflow($workflow, $form_state);
                
                $workflow->unlock();
                */
                $form_state->setRedirect('publisso_gold.dashboard');
        }
        
        /**
         * @param $workflow
         * @param FormStateInterface $form_state
         * @param false $store_files
         */
        protected function storeFormData2Workflow(&$workflow, FormStateInterface $form_state, $store_files = false){
                
                $form_data = $form_state->get('form_data');
                
                foreach($form_state->get('simpleValues') as $key){
                        if(array_key_exists($key, $form_data)) $workflow->setDataElement($key, $form_data[$key], false);
                }
                
                //abstract
                $element                = 'abstract';
                $$element               = $form_data[$element];
                $$element               = normalizeInlineImageLinks($$element);
                $workflow->setDataElement($element, base64_encode($$element), false);

                //chapter_text
                $element                = 'chapter_text';
                $$element               = $form_data[$element];
                $$element               = normalizeInlineImageLinks($$element);
                $workflow->setDataElement($element, base64_encode($$element), false);
                
                //keywords
                $keywords = array_filter(array_map('trim', explode(';', $form_data['keywords'])));
                $workflow->setDataElement('keywords', json_encode($keywords), false);
                
                //corporations
                $workflow->setDataElement('corporations', json_encode(array_filter(array_map('trim', $form_data['corporation']))), false);
                
                //authors
                $authors = [];
                
                foreach($form_data['author_db'] as $item){
                        
                        if(!array_key_exists('delete', $item['author']['raw']['data']) || $item['author']['raw']['data']['delete'] == 0){
                                
                                $is_corresponding = $item['is-corresponding'];
                                $weight           = $item['weight'];
                                $raw              = array_key_exists('raw', $item['author']) ? $item['author']['raw'] : null;
                                
                                if(!array_key_exists('raw', $item['author'])){
                                        
                                        $author           = $item['author']['field'];
                                        list($author, $data) = preg_split('/;\s{1,}/', $author, 2);
                                }
                                else{
                                        $author = implode(' ', array_filter([$item['author']['raw']['data']['fields']['firstname'], $item['author']['raw']['data']['fields']['lastname']]));
                                        $data   = $item['author']['raw']['data']['fields']['affiliation'];
                                }
                                
                                if(!empty($author)){
                                        
                                        $res = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->where("CONCAT_WS(:sep, up_firstname, up_lastname) = :author", [':sep' => ' ', ':author' => $author])->execute()->fetchAll();
                                        $found = 0;
                                        $last_id = null;
                                        $string = '';

                                        foreach($res as $v){
                                                $_user = new \Drupal\publisso_gold\Controller\User($v->up_uid);
                                                $aff = getUserAffiliation($_user);
                                                $string .= ' || '.implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $v->up_department, $v->up_institute, getCountry($v->up_country)])).' | '.implode( '; ', [$author, $data]);
                                                if(
                                                        implode( '; ', [$author, $data])
                                                                        == 
                                                        implode('; ', array_filter([implode(' ', array_filter([$v->up_firstname, $v->up_lastname])), $aff]))
                                                ){ 
                                                        $found++; 
                                                        $last_id = $v->up_uid;
                                                }
                                        }

                                        if($found == 1){
                                                
                                                $user = new \Drupal\publisso_gold\Controller\User($last_id);
                                                
                                                $authors[] = [
                                                        'weight'             => $weight,
                                                        'is_corresponding'   => $is_corresponding,
                                                        'firstname'          => $user->profile->getElement('firstname'),
                                                        'lastname'           => $user->profile->getElement('lastname'),
                                                        'affiliation'        => getUserAffiliation($user),
                                                        'uid'                => $user->getElement('id'),
                                                        'profile_archive_id' => $user->profile->makeSnapshot($_SESSION['user']['id'])
                                                ];
                                        }
                                }
                        }
                }
                
                foreach($form_data['author_add'] as $item){
                        $authors[] = array_map('trim', $item);
                }
                
                $workflow->setDataElement('authors', json_encode($authors), false);
                $workflow->setElement('modified', date('Y-m-d H:i:s'), false);
                $workflow->save();
        }
        
        /**
         * @param $wf_id
         * @return array
         */
        protected function getCommentsForAuthor($wf_id){
        
                $comments = \Drupal::database()->select('rwPubgoldWorkflowCommentsForAuthors', 'cfa')
                        ->fields('cfa', [])
                        ->condition('cfa_wfid', $wf_id, '=')
                        ->condition('cfa_for_uid', $_SESSION['user']['id'], '=')
                        ->execute()
                        ->fetchAll();

                $renderComments = [];
                $c = 0;
                
                foreach($comments as $_){

                        if($c === 0){
                                $renderComments = [
                                        '#type' => 'details',
                                        '#open' => false,
                                        '#title' => (string)t('Comment(s) for author'),
                                        '#description' => (string)t('These comments are only available for you during current workflow. Nobody else can see these comments!'),
                                        'content' => []
                                ];
                        }

                        $_->cfa_comment = base64_decode($_->cfa_comment);

                        $renderComments['content'][] = [
                                '#type' => 'details',
                                '#open' => false,
                                '#title' => (string)t('Comment').' #' . ($c + 1),
                                'content' => [
                                        '#type' => 'markup',
                                        '#markup' => $_->cfa_comment
                                ]
                        ];

                        $c++;
                }
                
                return $renderComments;
        }
        
        /**
         * @param $wf_id
         * @return array
         */
        protected function getAssignedFiles($wf_id){
                
                $files_tmpl = [];
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($wf_id);
                $fids = $workflow->getDataElement('files');
                if($fids) $fids = unserialize($fids);
                
                if(is_array($fids)){
                        
                        foreach($fids as $fid){
                                
                                $blob = new \Drupal\publisso_gold\Controller\Blob($fid);
                                
                                $files_tmpl[] = [
                                        '#type' => 'fieldset',
                                        '#id' => 'rwBlob-'.$fid,
                                        'content' => [
                                                '#type' => 'link',
                                                '#title' => $blob->meta['name'],
                                                '#url' => Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()]),
                                                '#suffix' => '<br><i>'.$blob->meta['description'].'</i>'
                                        ],
                                        'preview' => preg_match('/^(image|video|audio)\//', $blob->type, $matches) ? [
                                                '#type' => 'inline_template',
                                                '#template' => (
                                                        $matches[1] == 'image' ? '<br><img height="100" src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'">' : (
                                                                $matches[1] == 'video' ? '<br><video height="100" controls><source src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'" type="'.$blob->type.'"></video>' : (
                                                                        $matches[1] == 'audio' ? '<br><audio controls><source src="'.Url::fromRoute('publisso_gold.getFile', ['fid' => $blob->getId()])->toString().'" type="'.$blob->type.'"></audio>' : (
                                                                                ''
                                                                        )
                                                                )
                                                        )
                                                )
                                        ] : '',
                                        'action#'.$fid => [
                                                '#type' => 'button',
                                                '#value' => (string)$this->t('Delete File \''.$blob->meta['name'].'\''),
                                                '#prefix' => '<br><br>',
                                                'fid' => $fid,
                                                '#ajax' => [
                                                        'callback' => '::delWorkflowFile',
                                                        'wrapper' => 'rwBlob-'.$fid,
                                                        'effect' => 'slide'
                                                ],
                                                'limit_validation_errors' => []
                                        ]
                                ];
                        }
                }
                
                if(count($files_tmpl)){
                        return [
                            '#type' => 'details',
                            '#title' => t('Assigned files'),
                            '#open' => false,
                            'content' => $files_tmpl,
                            '#prefix' => '<br>'
                        ];
                }
                else{
                        return [];
                }
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param array $args
         * @return AjaxResponse
         */
        public function delWorkflowFile(array &$form, FormStateInterface $form_state, $args = []) {
                
                $fid = $form_state->getTriggeringElement()['fid'];
                $workflow = $form_state->get('workflow');
                $response = new AjaxResponse();
                
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                $fids = array_diff($fids, [$fid]);
                $workflow->setDataElement('files', serialize($fids));
                
                //check if file are deleted
                //reload Workflow
                $workflow = new \Drupal\publisso_gold\Controller\Workflow($form_state->get('wf_id'));
                $fids = $workflow->getDataElement('files');
                $fids = unserialize($fids);
                
                if(array_search($fid, $fids)){
                        
                        $file = new \Drupal\publisso_gold\Controller\Blob($fid);
                        $response->addCommand(new AppendCommand('div#rwBlob-'.$fid, '<div class="rwError">'.((string)$this->t('Error deleting file "'.$file->meta['name'].'"')).'</div>'));
                }
                else{
                        $response->addCommand(new RemoveCommand('div#rwBlob-'.$fid));
                }
                
                $form_state->setRebuild();
                return $response;
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function setAuthorDBData(array &$form, FormStateInterface $form_state){
                
                $params = $form_state->getTriggeringElement()['#name']['uid'];
                $form_state->set('RUID', $params);
                $form_state->setRebuild();
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function setAuthorDBDataCallback(array &$form, FormStateInterface $form_state){
                
                $params = $form_state->getTriggeringElement()['#name']['index'];
                
                foreach($form['fs_authors']['content']['author-db']['content'][$params]['author']['raw']['data']['fields'] as $k => $field){
                        
                        if(isset($form['fs_authors']['content']['author-db']['content'][$params]['author']['raw']['data']['fields'][$k]['#default_value'])){
                                $form['fs_authors']['content']['author-db']['content'][$params]['author']['raw']['data']['fields'][$k]['#value'] = $form['fs_authors']['content']['author-db']['content'][$params]['author']['raw']['data']['fields'][$k]['#default_value'];
                                //unset($form['fs_authors']['content']['author-db']['content'][$params]['author']['raw']['data']['fields'][$k]['#default_value']);
                        }
                }
                
                return $form['fs_authors']['content']['author-db']['content'][$params]['author']['raw']['data']['fields'];
        }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @param Request $request
         * @return array
         */
        public function AuthorDBSelect(array &$form, FormStateInterface $form_state, Request $request){
                return $form;
        }
}
