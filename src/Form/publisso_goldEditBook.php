<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldEditBook.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        
        use Drupal\publisso_gold\Controller\Manager\BookManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use \Drupal\publisso_gold\Controller\Reviewsheets;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldEditBook extends FormBase {
                
                private $modname = 'publisso_gold';
                private $modpath;
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'publisso_goldeditbook';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state ) {
                        
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        $reviewSheets = new Reviewsheets();
                        if ( $this->modname && !$form_state->get( 'modname' ) ) {
                                $form_state->set( 'modname', $this->modname );
                        }
                        
                        if ( !$this->modname && $form_state->get( 'modname' ) ) {
                                $this->modname = $form_state->get( 'modname' );
                        }
                        
                        $this->modpath = drupal_get_path( 'module', $this->modname );
                        
                        if ( $this->modpath && !$form_state->get( 'modpath' ) ) {
                                $form_state->set( 'modpath', $this->modpath );
                        }
                        
                        if ( !$this->modpath && $form_state->get( 'modpath' ) ) {
                                $this->modpath = $form_state->get( 'modpath' );
                        }
                        
                        require_once( $this->modpath . '/inc/publisso_gold.lib.inc.php' );
                        
                        $rp = [];
                        
                        //get review-pages
                        foreach ( getReviewpages() as $page ) {
                                $rp[ $page->rp_id ] = $page->rp_name;
                        }
                        
                        $availThemes = [];
                        foreach ( \Drupal::service( 'theme_handler' )->listInfo() as $name => $theme ) {
                                $availThemes[ $name ] = $theme->info[ 'name' ];
                        }
                        
                        if ( !$form_state->get( 'bk_id' ) ) {
                                $args = $form_state->getBuildInfo();
                                $bk_id = $args[ 'args' ][ 0 ][ 'bk_id' ];
                                $form_state->set( 'bk_id', $bk_id );
                        }
                        
                        #$book = getBook( \Drupal::database(), $form_state->get( 'bk_id' ) );
                        $book = BookManager::getBook( $form_state->get( 'bk_id' ) );
                        
                        if ( !$book ) {
                                \Drupal::service( 'messenger' )->addMessage( ( $this->t( 'Book not found!' ) ) );
                                return [];
                        }
                        
                        if ( !$form_state->get( 'book' ) ) $form_state->set( 'book', $book );
                        
                        $form[ 'site_headline' ] = [
                                '#type' => 'markup', '#markup' => '<h1>' . t( 'Edit Book' ) . '</h1>',
                        ];
                        
                        $form[ 'rp_id' ] = [
                                '#type' => 'select', '#title' => (string)t( 'select review-page' ), '#options' => $reviewSheets->getPageIDsNames(), '#required' => true, '#default_value' => $book->getElement( 'rpid' ),
                        ];
                        
                        $form[ 'language' ] = [
                                '#type' => 'select', '#title' => t( 'Language' ), '#options' => json_decode( Publisso::setup()->getValue( 'book.available.languages' ), true ), '#default_value' => $book->getElement( 'language' ) ?? Publisso::setup()->getValue( 'book.default.language' ),
                        ];
                        
                        $form[ 'theme' ] = [
                                '#type' => 'select', '#title' => (string)t( 'Theme' ), '#options' => $availThemes, '#required' => true, '#default_value' => $book->getElement( 'theme' ) ?? Publisso::setup()->getValue( 'books.default_theme' ),
                        ];
                        
                        $form[ 'title' ] = [
                                '#type' => 'textfield', '#title' => t( 'Title' ), '#required' => true, '#default_value' => $book->getElement( 'title' ),
                        ];
                        
                        $form[ 'title_abbr' ] = [
                                '#type' => 'textfield', '#title' => t( 'Title Abbreviation' ), '#required' => true, '#default_value' => $book->getElement( 'title_abbr' ),
                        ];
                        
                        $form[ 'logo' ] = [
                                '#type' => 'managed_file', '#title' => t( 'Logo' ), '#required' => false,
                        ];
                        
                        if ( !empty( $book->getElement( 'logo' ) ) ) {
                                $form[ 'view_logo' ] = [
                                        '#type' => 'markup', '#markup' => '<img width="100" src="/system/getPicture/bl/' . $form_state->get( 'bk_id' ) . '">',
                                ];
                        }
                        
                        $form[ 'delete_logo' ] = [
                                '#type' => 'checkbox', '#description' => (string)t( 'If you want to delete logo, mark checkbox. Then no file-upload will be processed!' ), '#title' => (string)t( 'delete logo' ),
                        ];
                        
                        $form[ 'cover' ] = [
                                '#type' => 'managed_file', '#title' => t( 'Cover' ), '#required' => false,
                        ];
                        
                        if ( !empty( $book->getElement( 'cover' ) ) ) {
                                $form[ 'view_cover' ] = [
                                        '#type' => 'markup', '#markup' => '<img width="150" src="/system/getPicture/bc/' . $form_state->get( 'bk_id' ) . '">',
                                ];
                        }
                        
                        $form[ 'delete_cover' ] = [
                                '#type' => 'checkbox', '#description' => (string)t( 'If you want to delete cover, mark checkbox. Then no file-upload will be processed!' ), '#title' => (string)t( 'delete cover' ),
                        ];
                        
                        $description = base64_decode( $book->getElement( 'description' ) );
                        
                        $form[ 'description' ] = [
                                '#type' => 'textarea', '#title' => t( 'Description' ), '#required' => true, '#default_value' => $description, '#attributes' => [
                                        'class' => [
                                                Publisso::setup()->getValue( 'submission.text_editor' ),
                                        ],
                                ],
                        ];
                        
                        $form[ 'citation_note' ] = [
                                '#type' => 'textarea', '#title' => (string)t( 'Citation note' ), '#required' => true, '#default_value' => $book->getElement( 'citation_note' ), '#attributes' => [
                                        'rows' => 2,
                                ],
                        ];
                        
                        $form[ 'edition' ] = [
                                '#type' => 'textfield', '#title' => t( 'Edition' ), '#required' => false, '#default_value' => $book->getElement( 'edition' ),
                        ];
                        
                        $form[ 'publication_place' ] = [
                                '#type' => 'textfield', '#title' => t( 'Publication place' ), '#required' => false, '#default_value' => $book->getElement( 'publication_place' ),
                        ];
                        
                        $form[ 'publisher' ] = [
                                '#type' => 'textfield', '#title' => (string)t( 'Publisher' ), '#required' => false, '#default_value' => $book->getElement( 'publisher' ),
                        ];
                        
                        $form[ 'begin_publication' ] = [
                                '#type' => 'textfield', '#title' => t( 'Begin Publication' ), '#required' => false, '#default_value' => $book->getElement( 'begin_publication' ),
                        ];
                        
                        $form[ 'isbn' ] = [
                                '#type' => 'textfield', '#title' => t( 'ISBN' ), '#default_value' => $book->getElement( 'isbn' ),
                        ];
                        
                        $form[ 'issn' ] = [
                                '#type' => 'textfield', '#title' => t( 'ISSN' ), '#default_value' => $book->getElement( 'issn' ),
                        ];
                        
                        $form[ 'ddc' ] = [
                                '#type' => 'textfield', '#title' => t( 'DDC' ), '#required' => true, '#default_value' => $book->getElement( 'ddc' ),
                        ];
                        
                        $form[ 'funding_id' ] = [
                                '#type' => 'textfield', '#title' => (string)t( 'Funding ID' ), '#default_value' => $book->getElement( 'funding_id' ),
                        ];
                        
                        $form[ 'funding_name' ] = [
                                '#type' => 'textfield', '#title' => (string)t( 'Funding name' ), '#default_value' => $book->getElement( 'funding_name' ),
                        ];
                        
                        $form[ 'license' ] = [
                                '#type' => 'textfield', '#title' => (string)t( 'License' ), '#default_value' => $book->getElement( 'license' ),
                        ];
                        
                        $form[ 'category' ] = [
                                '#type' => 'select', '#title' => t( 'Category' ), '#options' => Publisso::tools()->getBookCategories(), '#required' => true, '#default_value' => $book->getElement( 'bcatid' ),
                        ];
                        
                        $form[ 'public_editors' ] = [
                                '#type' => 'textfield', '#title' => (string)t( 'Editors (appear in book-header)' ), '#default_value' => $form_state->get( 'book' )->getElement( 'public_editors' ), '#maxlength' => 255
                        ];
                        
                        $form[ 'funding_text' ] = [
                                '#type' => 'textfield', '#title' => (string)t( 'Funding freetext' ), '#default_value' => $form_state->get( 'book' )->getElement( 'funding_text' ),
                        ];
                        
                        $_ = base64_decode( $form_state->get( 'book' )->getElement( 'about' ) );
                        
                        $form[ 'about' ] = [
                                '#type' => 'textarea', '#title' => (string)t( 'About this book' ), '#default_value' => $_, '#attributes' => [
                                        'class' => [
                                                \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' ),
                                        ],
                                ],
                        ];
                        
                        $_ = base64_decode( $form_state->get( 'book' )->getElement( 'manuscript_guidelines' ) );
                        
                        $form[ 'manuscript_guidelines' ] = [
                                '#type' => 'textarea', '#title' => (string)t( 'Manuscript guidelines' ), '#default_value' => $_, '#attributes' => [
                                        'class' => [
                                                \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' ),
                                        ],
                                ],
                        ];
                        
                        $_ = base64_decode( $form_state->get( 'book' )->getElement( 'media' ) );
                        $form[ 'media' ] = [
                                '#type' => 'textarea', '#default_value' => $_, '#title' => (string)t( 'Media' ), '#attributes' => [
                                        'class' => [
                                                \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' ),
                                        ],
                                ],
                        ];
                        
                        $_ = base64_decode( $form_state->get( 'book' )->getElement( 'imprint' ) );
                        $form[ 'imprint' ] = [
                                '#type' => 'textarea', '#title' => (string)t( 'Imprint' ), '#default_value' => $_, '#attributes' => [
                                        'class' => [
                                                \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' ),
                                        ],
                                ],
                        ];
                        
                        $form[ 'mail_signature' ] = [
                                '#type' => 'textarea', '#title' => 'EMail Signature', '#description' => t( 'Signature appended to outgoing emails relatet to this book' ), '#default_value' => $form_state->get( 'book' )->getElement( 'mail_signature' ),
                        ];
                        
                        $form[ 'public' ] = [
                                '#type'          => 'checkbox',
                                '#title'         => (string)$this->t( 'Public' ),
                                '#description'   => (string)$this->t( 'Indicates whether the book is publicly listed' ),
                                '#default_value' => $form_state->get( 'book' )->getElement( 'public' ),
                        ];
                        
                        $form[ 'control' ] = [
                                '#type' => 'fieldset', '#title' => t( 'Control Elements' ), '#collapsible' => true, '#collapsed' => true, '#tree' => true, '#prefix' => '<br>', 'elements' => [
                                        
                                        'review_change_origtext' => [
                                                '#type' => 'checkbox', '#title' => t( 'Allow changes in Origtext' ), '#default_value' => $form_state->get( 'book' )->getControlElement( 'review_change_origtext' ),
                                        ],
                                        
                                        'review_type' => [
                                                '#type'        => 'select', '#title' => t( 'Review Type' ), '#options' => [
                                                        'op' => t( 'Open' ), 'sb' => t( 'Single Blind' ), 'db' => t( 'Double Blind' ),
                                                ], '#required' => true, '#default_value' => empty( $form_state->get( 'book' )->getControlElement( 'review_type' ) ) ? 'op' : $form_state->get( 'book' )->getControlElement( 'review_type' ),
                                        ],
                                        
                                        'time_for_review' => [
                                                '#type' => 'number', '#size' => 3, '#title' => t( 'Time for Review (Days)' ), '#default_value' => !empty( $form_state->get( 'book' )->getControlElement( 'time_for_review' ) ) ? $form_state->get( 'book' )->getControlElement( 'time_for_review' ) : 0,
                                        ],
                                        
                                        'time_for_revision' => [
                                                '#type' => 'number', '#size' => 3, '#title' => t( 'Time for Revision (Days)' ), '#default_value' => !empty( $form_state->get( 'book' )->getControlElement( 'time_for_revision' ) ) ? $form_state->get( 'book' )->getControlElement( 'time_for_revision' ) : 0,
                                        ],
                                        
                                        'abstract_required' => [
                                                '#type'     => 'select', '#options' => [
                                                        0 => (string)t( $texts->get( 'bookmanagement.control.abstract_required.option.no', 'fc' ) ), 1 => (string)t( $texts->get( 'bookmanagement.control.abstract_required.option.yes', 'fc' ) ), 2 => (string)t( $texts->get( 'bookmanagement.control.abstract_required.option.optional', 'fc' ) ),
                                                ], '#title' => t( 'Abstract required' ), '#default_value' => $form_state->get( 'book' )->getControlElement( 'abstract_required' ),
                                        ],
                                        
                                        'char_limit_abstract' => [
                                                '#type' => 'number', '#title' => t( 'Character limit abstract' ), '#default_value' => !empty( $form_state->get( 'book' )->getControlElement( 'char_limit_abstract' ) ) ? $form_state->get( 'book' )->getControlElement( 'char_limit_abstract' ) : 0,
                                        ],
                                        
                                        'char_limit_text' => [
                                                '#type' => 'number', '#title' => t( 'Character limit text' ), '#default_value' => !empty( $form_state->get( 'book' )->getControlElement( 'char_limit_text' ) ) ? $form_state->get( 'book' )->getControlElement( 'char_limit_text' ) : 0,
                                        ],
                                        
                                        'reviewer_suggestion' => [
                                                '#type' => 'checkbox', '#title' => t( 'Reviewer Suggestion' ), '#default_value' => $form_state->get( 'book' )->getControlElement( 'reviewer_suggestion' ),
                                        ],
                                        
                                        'allow_invite_users' => [
                                                '#type'          => 'checkbox', '#title' => t( 'Special invitation workflow' ),
                                                '#default_value' => $form_state->get( 'book' )->getControlElement( 'allow_invite_users' ),
                                        ],
                                        
                                        'cn_version_string' => [
                                                '#type'             => 'select', '#options' => [
                                                        '' => (string)t( $texts->get( 'book.control.cn_version_string.option.empty', 'fc' ) ), 'chapter.field.published' => (string)t( $texts->get( 'book.control.cn_version_string.option.published', 'fc' ) ), 'chapter.field.version' => (string)t( $texts->get( 'book.control.cn_version_string.option.version', 'fc' ) ),
                                                ], '#default_value' => $form_state->get( 'book' )->getControlElement( 'cn_version_string' ), '#title' => (string)t( $texts->get( 'book.control.cn_version_string.title', 'fc' ) ),
                                        ],
                                        
                                        'cn_version_string_prefix' => [
                                                '#type' => 'textfield', '#title' => (string)t( $texts->get( 'book.control.cn_version_string_prefix.title', 'fc' ) ), '#default_value' => $form_state->get( 'book' )->getControlElement( 'cn_version_string_prefix' ),
                                        ],
                                        
                                        'show_submission_date' => [
                                                '#type' => 'checkbox', '#title' => t( 'Show submission date' ), '#default_value' => $form_state->get( 'book' )->getControlElement( 'show_submission_date' ),
                                        ],
                                        
                                        'show_revision_date' => [
                                                '#type' => 'checkbox', '#title' => t( 'Show revision date' ), '#default_value' => $form_state->get( 'book' )->getControlElement( 'show_revision_date' ),
                                        ],
                                        
                                        'show_acceptance_date' => [
                                                '#type' => 'checkbox', '#title' => t( 'Show acceptance date' ), '#default_value' => $form_state->get( 'book' )->getControlElement( 'show_acceptance_date' ),
                                        ],
                                        
                                        'show_publication_date' => [
                                                '#type' => 'checkbox', '#title' => t( 'Show publication date' ), '#default_value' => $form_state->get( 'book' )->getControlElement( 'show_publication_date' ),
                                        ],
                                        
                                        'search_livivo_enabled' => [
                                                '#type'          => 'checkbox',
                                                '#title'         => t( 'Enable Livivo-Search' ),
                                                '#default_value' => $form_state->get( 'book' )->getControlElement( 'search_livivo_enabled' ),
                                        ],
                                        
                                        'search_pubmed_enabled' => [
                                                '#type'          => 'checkbox',
                                                '#title'         => t( 'Enable Pubmed-Search' ),
                                                '#default_value' => $form_state->get( 'book' )->getControlElement( 'search_pubmed_enabled' ),
                                        ],
                                ],
                        ];
                        
                        $form[ 'submit' ] = [
                                '#type' => 'submit', '#value' => t( 'Update Book' ),
                        ];
                        
                        $form[ '#attributes' ] = [
                                'enctype' => 'multipart/form-data',
                        ];
                        
                        $form[ '#attached' ] = [
                                'library' => [
                                        'publisso_gold/default',
                                ],
                        ];
                        
                        $form[ '#cache' ][ 'max-age' ] = 0;
                        
                        $form_state->setCached( false );
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $book = BookManager::getBook( $form_state->get( 'bk_id' ), false );
                        
                        //process file-upload
                        $cover = [];
                        $cover[ 'content' ] = null;
                        $cover[ 'type' ] = null;
                        
                        $logo = [];
                        $logo[ 'content' ] = null;
                        $logo[ 'type' ] = null;
                        
                        if ( $form_state->getValue( 'delete_cover' ) == 0 ) {
                                $fid = $form_state->getValue( 'cover' );
                                
                                if ( $fid ) {
                                        $cover = \Drupal::database()->select( 'file_managed', 'f' )->condition( 'f.fid', $fid, '=' )->fields( 'f', [] )->execute()->fetchAssoc();
                                        $cover[ 'path' ] = str_replace( 'temporary:/', '/tmp', $cover[ 'uri' ] );
                                        $cover[ 'content' ] = '0x' . bin2hex( file_get_contents( $cover[ 'path' ] ) );
                                        $cover[ 'type' ] = $cover[ 'filemime' ];
                                }
                        }
                        //echo '<pre>'.print_r($cover, 1).'</pre>'; phpinfo(); exit();
                        if ( $form_state->getValue( 'delete_logo' ) == 0 ) {
                                $fid = $form_state->getValue( 'logo' );
                                if ( $fid ) {
                                        $logo = \Drupal::database()->select( 'file_managed', 'f' )->condition( 'f.fid', $fid, '=' )->fields( 'f', [] )->execute()->fetchAssoc();
                                        $logo[ 'path' ] = str_replace( 'temporary:/', '/tmp', $logo[ 'uri' ] );
                                        $logo[ 'content' ] = '0x' . bin2hex( file_get_contents( $logo[ 'path' ] ) );
                                        $logo[ 'type' ] = $logo[ 'filemime' ];
                                }
                        }
                        $sustaining_members = $form_state->getValue( 'sustaining_members' );
                        $sustaining_members_images = [];
                        
                        if ( is_array( $sustaining_members ) ) {
                                
                                foreach ( $sustaining_members as $_ ) {
                                        
                                        $fid = $_[ 0 ];
                                        
                                        if ( $fid ) {
                                                $result = \Drupal::database()->select( 'file_managed', 'f' )->condition( 'f.fid', $fid, '=' )->fields( 'f', [] )->execute()->fetchAssoc();
                                                $result[ 'path' ] = str_replace( 'temporary:/', '/tmp', $result[ 'uri' ] );
                                                preg_match( '/(.+)\.{1}(.+$)/i', $result[ 'filename' ], $matches );
                                                $sustaining_members_images[] = '0x' . bin2hex( file_get_contents( $result[ 'path' ] ) );
                                        }
                                }
                        }
                        
                        $book->setElement( 'title', $form_state->getValue( 'title' ), false );
                        $book->setElement( 'title_abbr', $form_state->getValue( 'title_abbr' ), false );
                        $book->setElement( 'description', base64_encode( $form_state->getValue( 'description' ) ), false );
                        $book->setElement( 'citation_note', $form_state->getValue( 'citation_note' ), false );
                        $book->setElement( 'edition', $form_state->getValue( 'edition' ), false );
                        $book->setElement( 'publication_place', $form_state->getValue( 'publication_place' ), false );
                        $book->setElement( 'publisher', $form_state->getValue( 'publisher' ), false );
                        $book->setElement( 'begin_publication', $form_state->getValue( 'begin_publication' ), false );
                        $book->setElement( 'issn', $form_state->getValue( 'issn' ), false );
                        $book->setElement( 'isbn', $form_state->getValue( 'isbn' ), false );
                        $book->setElement( 'ddc', $form_state->getValue( 'ddc' ), false );
                        $book->setElement( 'bcatid', $form_state->getValue( 'bcatid' ), false );
                        $book->setElement( 'rpid', $form_state->getValue( 'rp_id' ), false );
                        $book->setElement( 'license', $form_state->getValue( 'license' ), false );
                        $book->setElement( 'theme', $form_state->getValue( 'theme' ), false );
                        $book->setElement( 'bcatid', $form_state->getValue( 'category' ), false );
                        $book->setElement( 'public_editors', $form_state->getValue( 'public_editors' ), false );
                        $book->setElement( 'about', base64_encode( $form_state->getValue( 'about' ) ), false );
                        $book->setElement( 'imprint', base64_encode( $form_state->getValue( 'imprint' ) ), false );
                        $book->setElement( 'manuscript_guidelines', base64_encode( $form_state->getValue( 'manuscript_guidelines' ) ), false );
                        $book->setElement( 'media', base64_encode( $form_state->getValue( 'media' ) ), false );
                        $book->setElement( 'funding_text', $form_state->getValue( 'funding_text' ), false );
                        $book->setElement( 'mail_signature', $form_state->getValue( 'mail_signature' ), false );
                        $book->setElement( 'language', $form_state->getValue( 'language' ), false );
                        $book->setElement( 'public', $form_state->getValue( 'public' ) ?? 0, false );
                        
                        $control = $form_state->getValue( 'control' );
                        
                        $book->setElement( 'control', json_encode( $control[ 'elements' ] ), false );
                        
                        if ( ( $form_state->getValue( 'delete_cover' ) == 1 ) || ( $cover[ 'content' ] != null ) ) {
                                
                                $book->setElement('cover', $cover['content'], false);
                                $book->setElement('cover_type', $cover['type'], false);
                        }
                        
                        if ( ( $form_state->getValue( 'delete_logo' ) == 1 ) || ( $logo[ 'content' ] != null ) ) {
        
                                $book->setElement('logo', $logo['content'], false);
                                $book->setElement('logo_type', $logo['type'], false);
                        }
                        
                        $book->save();
                        
                        $form_state->setRedirect( 'publisso_gold.books_book', [ 'bk_id' => $form_state->get( 'bk_id' ) ] );
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function add_corporation (array &$form, FormStateInterface $form_state ) {
                        
                        $form[ 'corporation' ][] = [
                                '#type' => 'textfield', '#title' => t( 'Corporation' ),
                        ];
                        
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addMoreSubmit (array &$form, FormStateInterface $form_state ) {
                        
                        $form_state->set( 'num_corporation', $form_state->get( 'num_corporation' ) + 1 );
                        $form[ 'corporation' ][] = [
                                '#type' => 'textfield', '#title' => t( 'Corporation' ),
                        ];
                        
                        $form_state->setRebuild();
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addMoreCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'corporation' ];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function add_sustaining_members (array &$form, FormStateInterface $form_state ) {
                        
                        $form[ 'sustaining_members' ][] = [
                                '#type' => 'textfield', '#title' => t( 'Sustaining members' ),
                        ];
                        
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addMoreSMSubmit (array &$form, FormStateInterface $form_state ) {
                        
                        $form_state->set( 'num_sustaining_members', $form_state->get( 'num_sustaining_members' ) + 1 );
                        $form[ 'sustaining_members' ][] = [
                                '#type' => 'textfield', '#title' => t( 'Sustaining members' ),
                        ];
                        
                        $form_state->setRebuild();
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addMoreSMCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'sustaining_members' ];
                }
        }
