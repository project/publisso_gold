<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldTableOfContentAdd.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldTableOfContentAdd extends FormBase {
    
    private $modname = 'publisso_gold';
    private $modpath;
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldtableofcontentadd';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        if($this->modname && !$form_state->get('modname')){
            $form_state->set('modname', $this->modname);
        }
        
        if(!$this->modname && $form_state->get('modname')){
            $this->modname = $form_state->get('modname');
        }
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        $form['title'] = [
            '#type' => 'markup',
            '#markup' => (string)t('Add new table of content')
        ];
        
        $form['name'] = [
            '#type'         => 'textfield',
            '#maxlength'    => 255,
            '#title'        => (string)t('Name'),
            '#placeholder'  => (string)t('name'),
            '#required'     => true,
            '#description'  => (string)t('The name is used for internal access to assign table of content to a medium (i.e. book, journal, ...)')
        ];
        
        $form['title'] = [
            '#type'         => 'textfield',
            '#maxlength'    => 255,
            '#title'        => (string)t('Title'),
            '#placeholder'  => (string)t('title'),
            '#required'     => true,
            '#description'  => (string)t('The title is used as headline of table of content')
        ];
        
        $form['description'] = [
            '#type'         => 'textfield',
            '#maxlength'    => 255,
            '#title'        => (string)t('Description'),
            '#placeholder'  => (string)t('description'),
            '#required'     => false,
            '#description'  => (string)t('The description is for internal use only')
        ];
        
        $form['submit'] = [
            '#type'  => 'submit',
            '#value' => (string)t('Submit')
        ];
        
        $form['#cache']['max-age'] = 0;
        $form_state->setCached(false);
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $data = [
            'toc_name'          => $form_state->getValue('name'),
            'toc_title'         => $form_state->getValue('title'),
            'toc_description'   => $form_state->getValue('description')
        ];
        
        \Drupal::database()
            ->insert('rwPubgoldTableOfContent')
            ->fields($data)
            ->execute();
        
        $form_state->setRedirect('publisso_gold.table_of_content');
        
        return $form;
    }
}
