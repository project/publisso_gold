<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\EditJournal.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Database\Connection;
        use Drupal\publisso_gold\Controller\License;
        use Drupal\publisso_gold\Controller\Manager\JournalManager;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        
        /**
         * Provides a simple example form.
         */
        class EditJournal extends FormBase {
                
                private $modname = 'publisso_gold';
                private $database;
                private $modpath;
                private $num_corporations;
                
                public function __construct ( Connection $database ) {
                        $this->database = $database;
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return EditJournal|static
                 */
                public static function create (ContainerInterface $container ) {
                        return new static( $container->get( 'database' ) );
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'EditJournal';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state, $args = [] ) {
                        
                        $form_state->set( 'simpleValues', [
                                'rp_id'                 => 'rpid',
                                'title_abbr'            => 'title_abbr',
                                'title_nlm'             => 'title_nlm',
                                'corporation'           => 'corporation',
                                'editors'               => 'editors',
                                'issn'                  => 'issn',
                                'funding_text'          => 'funding',
                                'language'              => 'language',
                                'about'                 => 'about',
                                'imprint'               => 'imprint',
                                'manuscript_guidelines' => 'manuscript_guidelines',
                                'mail_signature'        => 'mail_signature',
                                'title'                 => 'title',
                                'publisher'             => 'publisher',
                                'publication_place'     => 'publication_place',
                                'contact'               => 'contact',
                                'welcome'               => 'welcome',
                                'public'                => 'public'
                        ]);
                        
                        if ( $this->modname && !$form_state->get( 'modname' ) ) {
                                $form_state->set( 'modname', $this->modname );
                        }
                        
                        if ( !$this->modname && $form_state->get( 'modname' ) ) {
                                $this->modname = $form_state->get( 'modname' );
                        }
                        
                        $this->modpath = drupal_get_path( 'module', $this->modname );
                        
                        if ( $this->modpath && !$form_state->get( 'modpath' ) ) {
                                $form_state->set( 'modpath', $this->modpath );
                        }
                        
                        if ( !$this->modpath && $form_state->get( 'modpath' ) ) {
                                $this->modpath = $form_state->get( 'modpath' );
                        }
                        
                        require_once( $this->modpath . '/inc/publisso_gold.lib.inc.php' );
                        
                        if ( !$this->database )
                                $this->database = \Drupal::database();
                        
                        if ( !$form_state->get( 'jrn_id' ) ) $form_state->set( 'jrn_id', $args[ 'jrn_id' ] );
                        $journal = new \Drupal\publisso_gold\Controller\Journal( $form_state->get( 'jrn_id' ) );
                        $journal = JournalManager::getJournal($form_state->get('jrn_id'));
                        
                        $rp = [];
                        
                        //get review-pages
                        foreach ( \Drupal::service('publisso_gold.tools')->getReviewpages() as $page ) {
                                $rp[ $page->rp_id ] = $page->rp_name;
                        }
                        
                        $form[ '#tree' ] = true;
                        
                        $form[ 'site_headline' ] = [
                                '#type'   => 'markup',
                                '#markup' => '<h1>' . t( 'Edit Journal' ) . '</h1>',
                        ];
                        
                        $form[ 'site_subheadline' ] = [
                                '#type'   => 'markup',
                                '#markup' => '<h2>' . $journal->getElement( 'title' ) . '</h2>',
                        ];
                        
                        $form[ 'rp_id' ] = [
                                '#type'          => 'select',
                                '#title'         => (string)t( 'select review-page' ),
                                '#options'       => $rp,
                                '#required'      => true,
                                '#default_value' => $journal->getElement( 'rpid' ),
                        ];
                        
                        $form[ 'title' ] = [
                                '#type'          => 'textfield',
                                '#title'         => t( 'Title' ),
                                '#required'      => true,
                                '#default_value' => $journal->getElement( 'title' ),
                        ];
                        
                        $form[ 'title_abbr' ] = [
                                '#type'          => 'textfield',
                                '#title'         => t( 'TitleAbbrev' ),
                                '#required'      => true,
                                '#default_value' => $journal->getElement( 'title_abbr' ),
                        ];
                        
                        $form[ 'title_nlm' ] = [
                                '#type'          => 'textfield',
                                '#title'         => t( 'NLM TitleAbbrev' ),
                                '#required'      => false,
                                '#default_value' => $journal->getElement( 'title_nlm' ),
                        ];
                        
                        $form[ 'logo' ] = [
                                '#type'              => 'managed_file',
                                '#title'             => t( 'Logo' ),
                                '#required'          => true,
                                '#multiple'          => true,
                                '#upload_location'   => 'public://publisso/journals/logos/',
                                '#upload_validators' => [
                                        'file_validate_extensions' => [ 'gif png jpg jpeg bmp' ],
                                        // Pass the maximum file size in bytes
                                        'file_validate_size'       => [ 30 * 1024 * 1024 ],
                                ],
                        ];
        
                        if($journal->getElement( 'logo' )) $form['logo']['#default_value'] = $journal->getElement( 'logo' );
                        
                        $form[ 'cover' ] = [
                                '#type'              => 'managed_file',
                                '#title'             => t( 'Cover' ),
                                '#required'          => false,
                                '#upload_location'   => 'public://publisso/journals/covers/',
                                '#upload_validators' => [
                                        'file_validate_extensions' => [ 'gif png jpg jpeg bmp' ],
                                        // Pass the maximum file size in bytes
                                        'file_validate_size'       => [ 30 * 1024 * 1024 ],
                                ]
                        ];
                        
                        if($journal->getElement( 'cover' )) $form['cover']['#default_value'] = $journal->getElement( 'cover' );
                        
                        $form[ 'corporation' ] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t( 'Corporation' ),
                                '#required'      => true,
                                '#default_value' => $journal->getElement( 'corporation' ),
                        ];
                        
                        $form[ 'editors' ] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)$this->t( 'Editors' ),
                                '#required'      => true,
                                '#default_value' => $journal->getElement( 'editors' ),
                        ];
                        
                        $form[ 'publication_place' ] = [
                                '#type'          => 'select',
                                '#title'         => t( 'Publication place' ),
                                '#required'      => true,
                                '#options'       => \Drupal::service('publisso_gold.tools')->getPublicationPlaces(),
                                '#default_value' => \Drupal::service('publisso_gold.tools')->getPublicationPlaceIDByName($journal->getElement( 'publication_place' )),
                        ];
                        
                        $form[ 'publication_place' ][ '#options' ][ 'other' ] = (string)t( 'Other/Not listed' );
                        
                        $form[ 'publication_place_other' ] = [
                                '#type'        => 'textfield',
                                '#placeholder' => (string)t( 'Please specify publication place here....' ),
                                '#states'      => [
                                        'invisible' => [
                                                ':input[name="publication_place"]' => [ '!value' => 'other' ],
                                        ],
                                        'optional'  => [
                                                ':input[name="publication_place"]' => [ '!value' => 'other' ],
                                        ],
                                        'visible'   => [
                                                ':input[name="publication_place"]' => [ 'value' => 'other' ],
                                        ],
                                        'required'  => [
                                                ':input[name="publication_place"]' => [ 'value' => 'other' ],
                                        ],
                                ],
                        ];
                        
                        $form[ 'publisher' ] = [
                                '#type'          => 'select',
                                '#title'         => (string)t( 'Publisher' ),
                                '#required'      => true,
                                '#options'       => \Drupal::service('publisso_gold.tools')->getPublishers(),
                                '#default_value' => \Drupal::service('publisso_gold.tools')->getPublisherIDByName($journal->getElement( 'publisher' )),
                        ];
                        
                        $form[ 'publisher' ][ '#options' ][ 'other' ] = (string)t( 'Other/Not listed' );
                        
                        $form[ 'publisher_other' ] = [
                                '#type'        => 'textfield',
                                '#placeholder' => (string)t( 'Please specify publisher place here....' ),
                                '#states'      => [
                                        'invisible' => [
                                                ':input[name="publisher"]' => [ '!value' => 'other' ],
                                        ],
                                        'optional'  => [
                                                ':input[name="publisher"]' => [ '!value' => 'other' ],
                                        ],
                                        'visible'   => [
                                                ':input[name="publisher"]' => [ 'value' => 'other' ],
                                        ],
                                        'required'  => [
                                                ':input[name="publisher"]' => [ 'value' => 'other' ],
                                        ],
                                ],
                        ];
                        
                        $form[ 'issn' ] = [
                                '#type'          => 'textfield',
                                '#title'         => t( 'ISSN' ),
                                '#required'      => true,
                                '#default_value' => $journal->getElement( 'issn' ),
                        ];
                        
                        $form[ 'ddc' ] = [
                                '#type'          => 'select',
                                '#title'         => t( 'DDC' ),
                                '#required'      => true,
                                '#options'       => getDDCs(),
                                '#default_value' => \Drupal::service('publisso_gold.tools')->getDDCIDByName($journal->getElement( 'ddc' )),
                        ];
                        
                        $form[ 'ddc' ][ '#options' ][ 'other' ] = (string)t( 'Other/Not listed' );
                        
                        $form[ 'ddc_other_index' ] = [
                                '#type'        => 'textfield',
                                '#placeholder' => (string)t( 'Please specify DDC index here....' ),
                                '#states'      => [
                                        'invisible' => [
                                                ':input[name="ddc"]' => [ '!value' => 'other' ],
                                        ],
                                        'optional'  => [
                                                ':input[name="ddc"]' => [ '!value' => 'other' ],
                                        ],
                                        'visible'   => [
                                                ':input[name="ddc"]' => [ 'value' => 'other' ],
                                        ],
                                        'required'  => [
                                                ':input[name="ddc"]' => [ 'value' => 'other' ],
                                        ],
                                ],
                        ];
                        
                        $form[ 'ddc_other_name' ] = [
                                '#type'        => 'textfield',
                                '#placeholder' => (string)t( 'Please specify DDC description here....' ),
                                '#states'      => [
                                        'invisible' => [
                                                ':input[name="ddc"]' => [ '!value' => 'other' ],
                                        ],
                                        'optional'  => [
                                                ':input[name="ddc"]' => [ '!value' => 'other' ],
                                        ],
                                        'visible'   => [
                                                ':input[name="ddc"]' => [ 'value' => 'other' ],
                                        ],
                                        'required'  => [
                                                ':input[name="ddc"]' => [ 'value' => 'other' ],
                                        ],
                                ],
                        ];
                        
                        $license = new License($journal->getElement( 'license' ));
                        
                        $form[ 'license' ] = [
                                '#type'          => 'select',
                                '#title'         => t( 'License' ),
                                '#required'      => true,
                                '#options'       => \Drupal::service('publisso_gold.tools')->getLicenses(),
                                '#default_value' => $license->getId(),
                        ];
                        
                        $form[ 'license' ][ '#options' ][ 'other' ] = (string)t( 'Other/Not listed' );
                        
                        $form[ 'license_other' ] = [
                                '#type'        => 'textfield',
                                '#placeholder' => (string)t( 'Please specify license here....' ),
                                '#states'      => [
                                        'invisible' => [
                                                ':input[name="license"]' => [ '!value' => 'other' ],
                                        ],
                                        'optional'  => [
                                                ':input[name="license"]' => [ '!value' => 'other' ],
                                        ],
                                        'visible'   => [
                                                ':input[name="license"]' => [ 'value' => 'other' ],
                                        ],
                                        'required'  => [
                                                ':input[name="license"]' => [ 'value' => 'other' ],
                                        ],
                                ],
                        ];
                        
                        $form[ 'category' ] = [
                                '#type'          => 'select',
                                '#title'         => t( 'Category' ),
                                '#required'      => true,
                                '#options'       => \Drupal::service('publisso_gold.tools')->getCategories(),
                                '#default_value' => $journal->getElement( 'category' ),
                        ];
                        
                        $form[ 'category' ][ '#options' ][ 'other' ] = (string)t( 'Other/Not listed' );
                        
                        $form[ 'category_other' ] = [
                                '#type'        => 'textfield',
                                '#placeholder' => (string)t( 'Please specify category here....' ),
                                '#states'      => [
                                        'invisible' => [
                                                ':input[name="category"]' => [ '!value' => 'other' ],
                                        ],
                                        'optional'  => [
                                                ':input[name="category"]' => [ '!value' => 'other' ],
                                        ],
                                        'visible'   => [
                                                ':input[name="category"]' => [ 'value' => 'other' ],
                                        ],
                                        'required'  => [
                                                ':input[name="category"]' => [ 'value' => 'other' ],
                                        ],
                                ],
                        ];
                        
                        $form[ 'funding_text' ] = [
                                '#type'          => 'textfield',
                                '#title'         => (string)t( 'Funding freetext' ),
                                '#required'      => false,
                                '#default_value' => $journal->getElement( 'funding' ),
                        ];
                        
                        $form[ 'language' ] = [
                                '#type'          => 'select',
                                '#title'         => t( 'Language' ),
                                '#options'       => json_decode( \Drupal::service('publisso_gold.setup')->getValue( 'journal.available.languages' ), true ),
                                '#default_value' => $journal->getElement( 'language' ),
                                '#required'      => true,
                        ];
                        
                        $form[ 'about' ] = [
                                '#type'          => 'textarea',
                                '#default_value' => $journal->getElement( 'about' ),
                                '#title'         => (string)t( 'About this journal' ),
                                '#attributes'    => [
                                        'class' => [
                                                \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' ),
                                        ],
                                ],
                        ];
                        
                        $form[ 'manuscript_guidelines' ] = [
                                '#type'          => 'textarea',
                                '#title'         => (string)t( 'Manuscript Guidelines' ),
                                '#attributes'    => [
                                        'class' => [
                                                \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' ),
                                        ],
                                ],
                                '#prefix'        => '<br>',
                                '#default_value' => $journal->getElement( 'manuscript_guidelines' ),
                        ];
                        
                        $form[ 'imprint' ] = [
                                '#type'          => 'textarea',
                                '#title'         => (string)t( 'Imprint' ),
                                '#attributes'    => [
                                        'class' => [
                                                \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' ),
                                        ],
                                ],
                                '#prefix'        => '<br>',
                                '#default_value' => $journal->getElement( 'imprint' ),
                        ];
        
                        $form[ 'contact' ] = [
                                '#type'       => 'textarea',
                                '#title'      => (string)t( 'Contact' ),
                                '#attributes' => [
                                        'class' => [
                                                \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' ),
                                        ],
                                ],
                                '#prefix'     => '<br>',
                                '#default_value' => $journal->getElement( 'contact' ),
                        ];
        
                        $form[ 'welcome' ] = [
                                '#type'       => 'textarea',
                                '#title'      => (string)t( 'Welcome (Current Volume)' ),
                                '#attributes' => [
                                        'class' => [
                                                \Drupal::service( 'publisso_gold.setup' )->getValue( 'submission.text_editor' ),
                                        ],
                                ],
                                '#prefix'     => '<br>',
                                '#default_value' => $journal->getElement( 'welcome' ),
                        ];
                        
                        $form[ 'mail_signature' ] = [
                                '#type'          => 'textarea',
                                '#title'         => 'EMail Signature',
                                '#description'   => t( 'Signature appended to outgoing emails relatet to this book' ),
                                '#prefix'        => '<br>',
                                '#default_value' => $journal->getElement( 'mail_signature' ),
                        ];
        
                        $form['public'] = [
                                '#type' => 'checkbox',
                                '#title' => (string)$this->t('Public'),
                                '#description' => (string)$this->t('Indicates whether the journal is publicly listed'),
                                '#default_value' => $journal->getElement('public')
                        ];
                        
                        $articletypes = [];
                        foreach ( getArticleTypes() as $k => $v ) {
                                $articletypes[ $k ] = [ $v ];
                        }
                        
                        $form[ 'control' ] = [
                                '#type'        => 'fieldset',
                                '#title'       => t( 'Control Elements' ),
                                '#collapsible' => true,
                                '#collapsed'   => true,
                                '#tree'        => true,
                                '#prefix'      => '<br>',
                                'elements'     => [
        
                                        'show_empty_tab_archive' => [
                                                '#type' => 'checkbox',
                                                '#title' => (string)$this->t('Show empty tab "Archive"'),
                                                '#default_value' => $journal->getControlElement( 'show_empty_tab_archive' ),
                                        ],
        
                                        'empty_tab_archive_message' => [
                                                '#type' => 'textfield',
                                                '#title' => (string)$this->t('Placeholder for empty tab "Archive"'),
                                                '#states'    => array(
                                                        'visible'   => array(   // action to take.
                                                                                ':input[name="control[elements][show_empty_tab_archive]"]' => array('checked' => true),
                                                        ),
                                                        'invisible' => array(   // action to take.
                                                                                ':input[name="control[elements][show_empty_tab_archive]"]' => array('checked' => false),
                                                        )
                                                ),
                                                '#default_value' => $journal->getControlElement( 'empty_tab_archive_message' ),
                                        ],
                                        
                                        'review_change_origtext' => [
                                                '#type'          => 'checkbox',
                                                '#title'         => t( 'Allow changes in Origtext' ),
                                                '#default_value' => $journal->getControlElement( 'review_change_origtext' ),
                                        ],
                                        
                                        'review_type' => [
                                                '#type'          => 'select',
                                                '#title'         => t( 'Review Type' ),
                                                '#options'       => [
                                                        'op' => t( 'Open' ),
                                                        'sb' => t( 'Single Blind' ),
                                                        'db' => t( 'Double Blind' ),
                                                ],
                                                '#required'      => true,
                                                '#default_value' => $journal->getControlElement( 'review_type' ),
                                        ],
                                        
                                        'time_for_review' => [
                                                '#type'          => 'number',
                                                '#size'          => 3,
                                                '#title'         => (string)t( 'Time for Review (Days)' ),
                                                '#default_value' => $journal->getControlElement( 'time_for_review' ),
                                        ],
                                        
                                        'time_for_revision'   => [
                                                '#type'          => 'number',
                                                '#size'          => 3,
                                                '#title'         => t( 'Time for Revision (Days)' ),
                                                '#default_value' => $journal->getControlElement( 'time_for_revision' ),
                                        ],
                                        /*
                                        'abstract_required' => [
                                                '#type' => 'checkbox',
                                                '#title' => t('Abstract required')
                                        ],
                                        */
                                        'char_limit_abstract' => [
                                                '#type'          => 'number',
                                                '#title'         => t( 'Character limit abstract' ),
                                                '#default_value' => $journal->getControlElement( 'char_limit_abstract' ),
                                        ],
                                        
                                        'char_limit_text' => [
                                                '#type'          => 'number',
                                                '#title'         => t( 'Character limit text' ),
                                                '#default_value' => $journal->getControlElement( 'char_limit_text' ),
                                        ],
                                        
                                        'reviewer_suggestion' => [
                                                '#type'          => 'checkbox',
                                                '#title'         => t( 'Reviewer Suggestion' ),
                                                '#default_value' => $journal->getControlElement( 'reviewer_suggestion' ),
                                        ],
                                        
                                        'allow_invite_users' => [
                                                '#type'          => 'checkbox',
                                                '#title'         => t( 'Allow user-invitations' ),
                                                '#default_value' => $journal->getControlElement( 'allow_invite_users' ),
                                        ],
                                        
                                        'article_types' => [
                                                '#type'          => 'tableselect',
                                                '#title'         => t( 'Allowed articletypes' ),
                                                '#header'        => [ 'Allowed articletype' ],
                                                '#options'       => $articletypes,
                                                '#default_value' => $journal->getControlElement( 'article_types' ),
                                        ],
                                        
                                        'with_translated_title' => [
                                                '#type'          => 'checkbox',
                                                '#title'         => t( 'With translated title' ),
                                                '#default_value' => $journal->getControlElement( 'with_translated_title' ),
                                        ],
                                        
                                        'with_translated_abstract' => [
                                                '#type'          => 'checkbox',
                                                '#title'         => t( 'With translated abstract' ),
                                                '#default_value' => $journal->getControlElement( 'with_translated_abstract' ),
                                        ],
                                        
                                        'show_submission_date' => [
                                                '#type'          => 'checkbox',
                                                '#title'         => t( 'Show submission date' ),
                                                '#default_value' => $journal->getControlElement( 'show_submission_date' ),
                                        ],
                                        
                                        'show_revision_date' => [
                                                '#type'          => 'checkbox',
                                                '#title'         => t( 'Show revision date' ),
                                                '#default_value' => $journal->getControlElement( 'show_revision_date' ),
                                        ],
                                        
                                        'show_acceptance_date' => [
                                                '#type'          => 'checkbox',
                                                '#title'         => t( 'Show acceptance date' ),
                                                '#default_value' => $journal->getControlElement( 'show_acceptance_date' ),
                                        ],
                                        
                                        'show_publication_date' => [
                                                '#type'          => 'checkbox',
                                                '#title'         => t( 'Show publication date' ),
                                                '#default_value' => $journal->getControlElement( 'show_publication_date' ),
                                        ],

                                        'search_livivo_enabled' => [
                                                '#type'  => 'checkbox',
                                                '#title' => t( 'Enable Livivo-Search' ),
                                                '#default_value' => $journal->getControlElement( 'search_livivo_enabled' ),
                                        ],

                                        'search_pubmed_enabled' => [
                                                '#type'  => 'checkbox',
                                                '#title' => t( 'Enable Pubmed-Search' ),
                                                '#default_value' => $journal->getControlElement( 'search_pubmed_enabled' ),
                                        ]
                                ],
                        ];
                        
                        $form[ 'submit' ] = [
                                '#type'   => 'submit',
                                '#value'  => t( 'Save journal' ),
                                '#prefix' => '<br>',
                                '#suffix' => '<br><br><br>',
                        ];
                        
                        $form[ '#attributes' ] = [
                                'enctype' => 'multipart/form-data',
                        ];
                        
                        $form[ '#attached' ] = [
                                'library' => [
                                        'publisso_gold/default',
                                ],
                        ];
                        
                        $form[ '#cache' ][ 'max-age' ] = 0;
                        
                        $form_state->setCached( false );
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $journal = JournalManager::getJournal($form_state->get('jrn_id'));
                        
                        foreach(['logo', 'cover'] as $picType) {
                                
                                if ( !$journal->getElement( $picType ) ) $journal->setElement( $picType, [] );
        
                                $formPic = $form_state->getValue( $picType );
                                $journalPic = $journal->getElement( $picType );
        
                                $deletePic = array_diff( $journalPic, $formPic );
                                $newPic = array_diff( $formPic, $journalPic );
        
                                foreach ( $deletePic as $_ ) {
                                        $file = \Drupal\file\Entity\File::load( $_ );
                                        $file->delete();
                                }
        
                                foreach ( $newPic as $_ ) {
                                        $file = \Drupal\file\Entity\File::load( $_ );
                                        $file->setPermanent();
                                        $file->save();
                                        $journalPic[] = $_;
                                }
        
                                $journal->setElement( $picType, array_diff( $journalPic, $deletePic ) );
                        }
                        
                        foreach ( $form_state->get( 'simpleValues' ) as $formKey => $dbKey ) {
                                $journal->setElement( $dbKey, $form_state->getValue( $formKey ), false );
                        }
                        
                        //control elements
                        foreach($form_state->getValue( 'control' )[ 'elements' ] as $k => $v)
                                $journal->setControlElement($k, $v);
                        #$journal->setElement( 'control', ( serialize($form_state->getValue( 'control' )[ 'elements' ]) ), false );
                        
                        //logo
                        
                        $journal->setElement( 'logo', ( serialize($form_state->getValue( 'logo' )) ), false );
                        echo $journal->getElement('logo');
                        //cover
                        $journal->setElement( 'cover', ( serialize($form_state->getValue( 'cover' )) ), false );
                        
                        //publication place
                        $key = 'publication_place';
                        
                        if ( $form_state->getValue( $key ) == 'other' ) {
                                $journal->setElement( $key, $form_state->getValue( $key . '_other' ), false );
                                createPublicationPlace( $form_state->getValue( $key . '_other' ) );
                        }
                        else
                                $journal->setElement( $key, \Drupal::service('publisso_gold.tools')->getPublicationPlace( $form_state->getValue( $key ) ), false );
                        
                        //publisher
                        $key = 'publisher';
                        if ( $form_state->getValue( $key ) == 'other' ) {
                                $journal->setElement( $key, $form_state->getValue( $key . '_other' ), false );
                                createPublisher( $form_state->getValue( $key . '_other' ) );
                        }
                        else {
                                $journal->setElement( $key, getPublisher( $form_state->getValue( $key ) ), false );
                        }
                        //ddc
                        $key = 'ddc';
                        if ( $form_state->getValue( $key ) == 'other' ) {
                                $journal->setElement( $key, $form_state->getValue( $key . '_other' ), false );
                                createDDC( $form_state->getValue( $key . '_other_index' ), $form_state->getValue( $key . '_other_name' ) );
                        }
                        else
                                $journal->setElement( $key, getDDC( $form_state->getValue( $key ) ), false );
                        
                        //license
                        $key = 'license';
                        if ( $form_state->getValue( $key ) == 'other' ) {
                                $license = new \Drupal\publisso_gold\Controller\License();
                                $license->create( $form_state->getValue( $key . '_other' ) );
                        }
                        else {
                                $license = new \Drupal\publisso_gold\Controller\License( $form_state->getValue( $key ) );
                        }
                        
                        $journal->setElement( $key, $license->getId(), false );
                        
                        //category
                        $key = 'category';
                        if ( $form_state->getValue( $key ) == 'other' ) {
                                $journal->setElement( $key, $form_state->getValue( $key . '_other' ), false );
                                createCategory( $form_state->getValue( $key . '_other' ) );
                        }
                        else
                                $journal->setElement( $key, getCategory( $form_state->getValue( $key ) ), false );
                        
                        //echo '<pre>'.print_r($journal, 1).'</pre>'; exit();
                        $journal->save();
                        
                        //if all done, sets uploaded files permanent
                        if ( is_array( unserialize($journal->getElement('cover')) ) ) {
                                foreach ( unserialize($journal->getElement('cover')) as $_ ) {
                                        $file = \Drupal\file\Entity\File::load( $_ );
                                        $file->setPermanent();
                                        \Drupal::service( 'file.usage' )->add( $file, 'publisso_gold', 'user', 1 );
                                        $file->save();
                                }
                        }
                        
                        if ( is_array( unserialize($journal->getElement('logo')) ) ) {
                                foreach ( unserialize($journal->getElement('logo')) as $_ ) {
                                        $file = \Drupal\file\Entity\File::load( $_ );
                                        $file->setPermanent();
                                        \Drupal::service( 'file.usage' )->add( $file, 'publisso_gold', 'user', 1 );
                                        $file->save();
                                }
                        }
                        
                        $form_state->setRedirect( 'publisso_gold.journals_journal', [ 'jrn_id' => $journal->getElement( 'id' ) ] );
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function add_corporation (array &$form, FormStateInterface $form_state ) {
                        
                        $form[ 'corporation' ][] = [
                                '#type'  => 'textfield',
                                '#title' => t( 'Corporation' ),
                        ];
                        
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addMoreSubmit (array &$form, FormStateInterface $form_state ) {
                        
                        $form_state->set( 'num_corporation', $form_state->get( 'num_corporation' ) + 1 );
                        $form[ 'corporation' ][] = [
                                '#type'  => 'textfield',
                                '#title' => t( 'Corporation' ),
                        ];
                        
                        $form_state->setRebuild();
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addMoreCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'corporation' ];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function add_sustaining_members (array &$form, FormStateInterface $form_state ) {
                        
                        $form[ 'sustaining_members' ][] = [
                                '#type'  => 'textfield',
                                '#title' => t( 'Sustaining members' ),
                        ];
                        
                        $form_state->setRebuild();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function addMoreSMSubmit (array &$form, FormStateInterface $form_state ) {
                        
                        $form_state->set( 'num_sustaining_members', $form_state->get( 'num_sustaining_members' ) + 1 );
                        $form[ 'sustaining_members' ][] = [
                                '#type'  => 'textfield',
                                '#title' => t( 'Sustaining members' ),
                        ];
                        
                        $form_state->setRebuild();
                        $form_state->setRebuild();
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return mixed
                 */
                public function addMoreSMCallback (array &$form, FormStateInterface $form_state ) {
                        return $form[ 'sustaining_members' ];
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function fileSelect (array &$form, FormStateInterface $form_state ) {
                        error_log( print_r( $form_state->getTriggeringElement(), 1 ) );
                        return $form;
                }
        }
