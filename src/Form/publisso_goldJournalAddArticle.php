<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldJournalAddArticle.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldjournalAddArticle extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldjournalAddArticle|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldjournaladdarticle';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $this->modpath = drupal_get_path('module', $this->modname);
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
        //expect build-info-args as array, index 0 is bk_id
        $build_info = $form_state->getBuildInfo();
        $args = $build_info['args'];
        
        if(array_key_exists(0, $args) && array_key_exists('jrn_id', $args[0]))
          $form_state->set('jrn_id', $args[0]['jrn_id']);
        
        if(array_key_exists(0, $args) && array_key_exists('wf_id', $args[0]))
          $form_state->set('wf_id', $args[0]['wf_id']);
        
        if(!$form_state->get('jrn_id')){
            drupal_set_message(t('No journal given!'), 'error');
            return array();
        }
        
        $journal = getJournal(\Drupal::database(), $form_state->get('jrn_id'));
        
        if($form_state->get('wf_id')){
            $workflow = getDashboard(\Drupal::Database(), $form_state->get('wf_id'));
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            $article_text = json_decode(base64_decode($workflow_data->article_text));
            $abstract = json_decode(base64_decode($workflow_data->abstract));
            
            $delta = ['workflow', 'workflow_data'];
            foreach($delta as $_) $form_state->set($_, $$_);
        }
        
        $form ['site_headline'] = [
            '#type' => 'markup',
            '#markup' => '<h1>'.t(($form_state->get('wf_id') ? 'Edit' : 'Add').' Article').'</h1>'
        ];
            
        $form['journal_title'] = [
            '#type' => 'markup',
            '#markup' => '<h2>'.$journal['jrn_title'].'</h2>'
        ];
            
        $form['title'] = [
            '#type' => 'textfield',
            '#title' => t('Title'),
            '#required' => true,
            '#default_value' => isset($workflow_data->title) ? $workflow_data->title : null
        ];
        
        $form['title_translated'] = [
            '#type' => 'textfield',
            '#title' => t('Title translated'),
            '#required' => true,
            '#default_value' => isset($workflow_data->title_translated) ? $workflow_data->title_translated : ''
        ];
            
        $form['article_type'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Article type'),
            '#required' => true,
            '#default_value' => isset($workflow_data->article_type) ? $workflow_data->article_type : ''
        ];
        
        $form['abstract'] = [
            '#type' => 'text_format',
            '#title' => t('Abstract'),
            '#required' => true
        ];
        
         if(isset($abstract)){
          $form['abstract']['#format'] = $abstract->format;
          $form['abstract']['#default_value'] = $abstract->value;
        }
        else{
          $form['abstract']['#format'] = 'full_html';
        }
        
        $form['article_text'] = [
            '#type' => 'text_format',
            '#title' => t('Article Text'),
            '#required' => true,
            '#rows' => 50,
            '#prefix' => '<br>'
        ];
        
        if(isset($article_text)){
          $form['article_text']['#format'] = $article_text->format;
          $form['article_text']['#default_value'] = $article_text->value;
        }
        else{
          $form['article_text']['#format'] = 'full_html';
        }
        
        $form['fs_authors'] = [
            '#type' => 'fieldset',
            '#title' => (string)t('Author(s)'),
            '#prefix' => '<br>',
            'content' => []
        ];
                    
        $form['fs_authors']['content']['authors'] = [
            '#title' => (string)t('Author(s)'),
            '#type' => 'select',
            '#multiple' => true,
            '#options' => getUsersByRole(\Drupal::database(), [2,3,4,5,6,7]),
            '#default_value' => isset($workflow_data) ? explode(',', $workflow_data->authors) : ''
        ];
                    
        $form['fs_authors']['content']['more_authors'] = [
          '#tree' => TRUE,
          '#prefix' => '<div id="authors-replace">',
          '#suffix' => '</div>'
        ];
        
        //init counter
        if(!$form_state->get('num_authors'))
           $form_state->set('num_authors', 0);
        
        $max = $form_state->get('num_authors');
        
        for ($delta = 0; $delta < $max; $delta++) {
            
            if (!isset($form['fs_authors']['content']['more_authors'][$delta])) {
                
                $element = array(
                    '#type' => 'fieldset',
                    '#title' => (string)t('Author').' #'.(string)($delta + 1),
                    'content' => [
                        'more_author_name' => [
                            '#type' => 'textfield',
                            '#title' => (string)t('Name')
                        ],
                        
                        'more_author_lastname' => [
                            '#type' => 'textfield',
                            '#title' => (string)t('Lastname')
                        ],
                        
                        'more_author_affiliation' => [
                            '#type' => 'textfield',
                            '#title' => (string)t('Affiliation')
                        ]
                    ]
                );
                
                $form['fs_authors']['content']['more_authors'][$delta] = $element;
            }
        }
        
        if(!$form_state->get('wf_id')){  
          $form['fs_authors']['content']['addAuthor'] = [
              '#type' => 'submit',
              '#name' => 'addAuthor',
              '#value' => t('Add Author'),
              '#submit' => array('::addAuthor'),
              '#suffix' => '<br><br>',
              '#prefix' => '<br><br>',
              
              '#ajax' => array(
                'callback' => '::addAuthorCallback',
                'wrapper' => 'authors-replace',
                'effect' => 'fade'
              )
              
          ];
        }
        $form['fs_corresponding_authors'] = [
            '#type' => 'fieldset',
            '#title' => (string)t('Corresponding Author(s)'),
            '#prefix' => '<br>',
            'content' => []
        ];
                
        $form['fs_corresponding_authors']['content']['corresponding_authors'] = [
            '#title' => (string)t('Corresponding Author(s)'),
            '#type' => 'select',
            '#multiple' => true,
            '#options' => getUsersByRole(\Drupal::database(), [2,3,4,5,6,7]),
            '#default_value' => isset($workflow_data) ? explode(',', $workflow_data->corresponding_authors) : ''
        ];
                /*
                'more_authors' => [
                    '#tree' => TRUE,
                    '#prefix' => '<div id="authors-replace">',
                    '#suffix' => '</div>'
                ],
                
                'addAuthor' => [
                    '#type' => 'submit',
                    '#name' => 'addAuthor',
                    '#value' => t('Add Author'),
                    '#submit' => array('::addAuthor'),
                    '#suffix' => '<br><br>',
                    '#prefix' => '<br><br>',
                    
                    '#ajax' => array(
                      'callback' => '::addAuthorCallback',
                      'wrapper' => 'authors-replace',
                      'effect' => 'fade'
                    )
                    
                ]
                */
            
        $form['corporation'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Corporation'),
            '#default_value' => isset($workflow_data) ? $workflow_data->corporation : null
        ];
            
        $form['conflict_of_interest'] = [
            '#type' => 'checkbox',
            '#title' => (string)t('Conflict of interest'),
            '#default_value' => isset($workflow_data) ? $workflow_data->conflict_of_interest : ''
        ];
            
        $form['conflict_of_interest_text'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Description of conflict of interest'),
            '#default_value' => isset($workflow_data) ? $workflow_data->conflict_of_interest_text : ''
        ];
            
        $form['author_contract'] = [
            '#type' => 'checkbox',
            '#title' => (string)t('Author contract'),
            '#default_value' => isset($workflow_data) ? $workflow_data->author_contract : ''
        ];
            
        $form['patients_rights'] = [
            '#type' => 'checkbox',
            '#title' => (string)t('Patients rights'),
            '#default_value' => isset($workflow_data) ? $workflow_data->patients_rights : ''
        ];
        
        $form['funding_name'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Funding name'),
            '#default_value' => isset($workflow_data) ? $workflow_data->funding_name : ''
        ];
            
        $form['funding_id'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Funding ID'),
            '#default_value' => isset($workflow_data) ? $workflow_data->funding_id : ''
        ];
            
        $form['keywords'] = [
            '#type' => 'textfield',
            '#title' => (string)t('Keywords'),
            '#default_value' => isset($workflow_data->keywords) ? implode(', ', json_decode($workflow_data->keywords, true)) : ''
        ];
            
        $form['publication_place'] = [
            '#type' => 'textfield',
            '#title' => t('Publication Place'),
            '#required' => true,
            '#default_value' => isset($workflow_data->publication_place) ? $workflow_data->publication_place : 'Cologne' 
        ];
            
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => t(($form_state->get('wf_id') ? 'Finish revision' : 'Save article'))
        ];
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $journal = getJournal(\Drupal::database(), $form_state->get('jrn_id'));
        
        //get journal eo
        $eo = getJournalEditorialOffice(\Drupal::database(), $form_state->get('jrn_id'));
        $submission_assign = $eo_str = '';
        
        foreach($eo as $_){
            $submission_assign = (!empty($submission_assign) ? ',' : '').'u:'.$_['up_uid'];
            $eo_str = (!empty($eo_str) ? ',' : '').$_['up_uid'];
        }
        
        if(empty($submission_assign)) //if no eo assigned -> publisso
            $submission_assign = 'r:2';
        
        $abstract               = $form_state->getValue('abstract');
        $abstract['value']      = normalizeInlineImageLinks($abstract['value']);
        
        $article_text           = $form_state->getValue('article_text');
        $article_text['value']  = normalizeInlineImageLinks($article_text['value']);
        
        $more_authors = [];
        $_ = $form_state->getValue('more_authors');
        
        foreach($_ as $__){
          $more_authors[] = $__['content'];
        }
        
        foreach($form_state->getValues() as $k => $v)
          $data[$k] = $v;
        
        $data = [
            
            'abstract'                  => base64_encode(json_encode($abstract)),
            'article_text'              => base64_encode(json_encode($article_text)),
            'article_type'              => $form_state->getValue('article_type'),
            'title'                     => $form_state->getValue('title'),
            'title_translated'          => $form_state->getValue('title_translated'),
            'author_uid'                => $_SESSION['user']['id'],
            'authors'                   => implode(',', $form_state->getValue('authors')),
            'publication_place'         => $form_state->getValue('publication_place'),
            'corresponding_authors'     => implode(',', $form_state->getValue('corresponding_authors')),
            'corporation'               => $form_state->getValue('corporation'),
            'conflict_of_interest'      => $form_state->getValue('conflict_of_interest'),
            'conflict_of_interest_text' => $form_state->getValue('conflict_of_interest_text'),
            'author_contract'           => $form_state->getValue('author_contract'),
            'patients_rights'           => $form_state->getValue('patients_rights'),
            'keywords'                  => json_encode(preg_split('/[^a-zA-Z0-9 ]+/', $form_state->getValue('keywords'))),
            'funding_name'              => $form_state->getValue('funding_name'),
            'funding_id'                => $form_state->getValue('funding_id'),
            'jrn_id'                    => $form_state->get('jrn_id'),
            'state'                     => 'submission finished',
            'type'                      => 'journalarticle',
            'more_authors'              => json_encode($more_authors)
        ];
        
        if(!$form_state->get('wf_id')){
            
            $fields = [
                    'wf_assigned_to' => $submission_assign,
                    'wf_data' => base64_encode(json_encode($data)),
                    'wf_assigned_to_eo' => $eo_str,
                    'wf_created_by_uid' => $_SESSION['user']['id']
            ];
            
            $wf_id = \Drupal::database()
                ->insert('rwPubgoldWorkflow')
                ->fields($fields)
                ->execute();
        }
        else{
            
            $delta = ['workflow', 'workflow_data'];
            foreach($delta as $_) $$_ = $form_state->get($_);
            
            foreach($data as $k => $v){
                $workflow_data->$k = $v;
            }
            
            $workflow_data->state = 'revision finished';
            $submission_assign = 'u:'.$workflow->wf_editor_last_edited;
            
            $fields = [
                    'wf_assigned_to' => $submission_assign,
                    'wf_data' => base64_encode(json_encode($workflow_data)),
                    'wf_locked' => 0,
                    'wf_locked_by_uid' => null
            ];
            
            \Drupal::database()
                ->update('rwPubgoldWorkflow')
                ->fields($fields)
                ->condition('wf_id', $form_state->get('wf_id'))
                ->execute();
        }
        
        drupal_set_message(t('Article successfully saved!'));
        $form_state->setRedirect('publisso_gold.journals_journal', ['jrn_id' => $form_state->get('jrn_id')]);
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         */
        public function addAuthor(array &$form, FormStateInterface $form_state) {
        
        $form_state->set('num_authors', $form_state->get('num_authors') + 1);
        
        $form['fs_authors']['content']['more_authors'][] = [
            '#type' => 'fieldset',
            '#title' => (string)t('Author').' #'.$form_state->get('num_authors'),
            'content' => [
                'more_author_name' => [
                    '#type' => 'textfield',
                    '#title' => (string)t('Name')
                ],
                
                'more_author_lastname' => [
                    '#type' => 'textfield',
                    '#title' => (string)t('Lastname')
                ],
                
                'more_author_affiliation' => [
                    '#type' => 'textfield',
                    '#title' => (string)t('Affiliation')
                ]
            ]
        ];
        
        $form_state->setRebuild();
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addAuthorCallback(array &$form, FormStateInterface $form_state) {
        return $form['fs_authors']['content']['more_authors'];
    }
}