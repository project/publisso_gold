<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldChangePassword.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldChangePassword extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $currentUser;
    
    public function __construct(){
        $this->currentUser = new \Drupal\publisso_gold\Controller\User($_SESSION['user']['id']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'publisso_goldchangepassword';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $this->modpath = drupal_get_path('module', $this->modname);
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
        $form['info'] = array(
            '#type' => 'markup',
            '#markup' => '<span class="rwAlert">'.t('You are prompted to change your password').'</span><br>'
        );
        
        $form['password_old'] = array(
            '#type' => 'password',
            '#title' => t('Enter first your old password').':',
            '#required' => true
        );
        
        $form['password_new'] = array(
            '#type' => 'password_confirm',
            '#title' => t('Please enter a new password').':',
            '#required' => true
        );
        
        $form['save'] = array(
            '#type' => 'submit',
            '#value' => t('Save')
        );
        
        $form['#cache']['max-age'] = 0;
        $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
          
        $user = $this
                    ->database
                    ->select('rwPubgoldUsers', 'u')
                    ->fields('u', [])
                    ->condition('id', $_SESSION['user']['id'], '=')
                    ->execute()
                    ->fetchAssoc();
        
        //verify old password
        if('*'.strtoupper(sha1(sha1($form_state->getValue('password_old'), true))) != $user['password']){
            
            $form_state->setErrorByName('password_old', t('Please enter your OLD password!'));
        }
        
        //make sure, passwords are not identical
        if($form_state->getValue('password_old') == $form_state->getValue('password_new')){
            $form_state->setErrorByName('password_new', t('The new password must not be the same as the old!'));
        }
        
        return $form;
    }

    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $password = $form_state->getValue('password_new');
        $messenger = \Drupal::service('messenger');
        $tools = \Drupal::service('publisso_gold.tools');
        $session = \Drupal::service('session');
        $user = new \Drupal\publisso_gold\Controller\User($session->get('user')['id']);
        
        $sso = \Drupal::service('publisso_gold.sso_provider');
        
        if($sso->SSOActive() === true){
                
                $sso->setUserPassword($password);
                $messenger->addError('Password change failed!');
                $tools->logMsg($sso->getLastResponseMessage());
                return $form;
        }
        
        $this
            ->database
            ->update('rwPubgoldUsers')
            ->fields(
                array(
                    'password' => '*'.strtoupper(sha1(sha1($form_state->getValue('password_new'), true))),
                    'pw_change_required' => 0
                )
            )
            ->condition('id', $user->getElement('id'), '=')
            ->execute();
        
        
        $sUser = $session->get('user');
        $sUser['pw_change'] = 0;
        $sUser['password'] = $password;
        
        $messenger->addMessage((string)$this->t('Password successfully updated!'));
        return $form;
    }
}
