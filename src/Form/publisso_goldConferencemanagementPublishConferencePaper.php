<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldConferencemanagementPublishConferencePaper.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldConferencemanagementPublishConferencePaper extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $num_corporations;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldConferencemanagementPublishConferencePaper|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldconferencemanagementpublishconferencepaper';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        $args = $form_state->getBuildInfo();
        
        if(count($args)){
            $wf_id = $args['args'][0]['wf_id'];
            $cf_id = $args['args'][0]['cf_id'];
        }
        
        if(!$wf_id)
            $wf_id = $form_state->get('wf_id');
        
        if(!$cf_id)
            $cf_id = $form_state->get('cf_id');
        
        if($wf_id && $cf_id){
            
            $form_state->set('wf_id', $wf_id);
            $form_state->set('cf_id', $cf_id);
            
            $workflow = getDashboard(\Drupal::database(), $wf_id);
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            
            $delta = ['workflow', 'workflow_data'];
            foreach($delta as $_) $form_state->set($_, $$_);
            
            $form['debug'] = [
                '#type' => 'markup',
                '#markup' => '<pre>'.print_r($workflow_data, 1).'</pre>'
            ];
            
            $form['submit'] = [
                    '#type' => 'submit',
                    '#value' => (string)t('publish paper')
            ];
        }
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $delta = ['workflow', 'workflow_data', 'wf_id', 'cf_id'];
        foreach($delta as $_) $$_ = $form_state->get($_);
        
        //save paper
        $data = [
            'cfp_abstract'               => $workflow_data->abstract,
            'cfp_author'                 => $workflow_data->author_uid,
            'cfp_cfid'                   => $cf_id,
            'cfp_paper_text'             => $workflow_data->paper_text,
            'cfp_correction'             => $workflow_data->correction,
            'cfp_corresponding_author'   => $workflow_data->corresponding_author,
            'cfp_ddc'                    => $workflow_data->ddc,
            'cfp_doi'                    => $workflow_data->doi,
            'cfp_erratum'                => $workflow_data->erratum,
            'cfp_funding_id'             => $workflow_data->funding_id,
            'cfp_funding_name'           => $workflow_data->funding_name,
            'cfp_keywords'               => $workflow_data->keywords,
            'cfp_license'                => $workflow_data->license,
            'cfp_publication_place'      => $workflow_data->publication_place,
            'cfp_publication_year'       => $workflow_data->publication_year,
            'cfp_publisher'              => $workflow_data->publisher,
            'cfp_title'                  => $workflow_data->title,
            'cfp_presenting_author'      => $workflow_data->presenting_author,
            'cfp_urn'                    => $workflow_data->urn,
            'cfp_version'                => $workflow_data->version,
            'cfp_article_type'           => $workflow_data->article_type,
            'cfp_session'                => $workflow_data->session,
            'cfp_doc_number'             => $workflow_data->doc_number,
            'cfp_comment'                => $workflow_data->comment
        ];
        
        \Drupal::database()
            ->insert('rwPubgoldConferencePapers')
            ->fields($data)
            ->execute();
        
        //delete complete workflow
        $delete_from = ['rwPubgoldWorkflow', 'rwPubgoldWorkflowComments', 'rwPubgoldWorkflowHistory'];
        
        foreach($delete_from as $table){
            
            $qry = \Drupal::database()->delete($table);
            
            switch($table){
                
                case 'rwPubgoldWorkflow':
                    $qry->condition('wf_id', $wf_id, '=');
                    break;
                
                case 'rwPubgoldWorkflowComments':
                    $qry->condition('wfc_wfid', $wf_id, '=');
                    break;
                
                case 'rwPubgoldWorkflowHistory':
                    $qry->condition('wfh_wfid', $wf_id, '=');
                    break;
            }
            
            $qry->execute();
        }
        
        $form_state->setRedirect('publisso_gold.dashboard');
        
        return $form;
    }
        
        /**
         * @param null $tmpl
         * @param null $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl = NULL, $vars = NULL){
        
        if($tmpl == NULL || $vars == NULL){
            //set Site-Vars
            $this->tmpl = str_replace(array_keys($this->tmpl_vars), array_values($this->tmpl_vars), $this->tmpl);
            
            //remove unused vars
            $this->tmpl = preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl);
        }
        else{
            //set Site-Vars
            $tmpl = str_replace(array_keys($vars), array_values($vars), $tmpl);
            
            //remove unused vars
            return preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $tmpl);
        }
    }
}