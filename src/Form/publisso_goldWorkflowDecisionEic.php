<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowDecisionEic.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Database\Connection;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldWorkflowDecisionEic extends FormBase {
                
                private $modname = 'publisso_gold';
                private $database;
                private $modpath;
                private $num_corporations;
                
                public function __construct ( Connection $database ) {
                        $this->database = $database;
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return publisso_goldWorkflowDecisionEic|static
                 */
                public static function create (ContainerInterface $container ) {
                        return new static( $container->get( 'database' ) );
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'publisso_goldworkflowdecisioneic';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state ) {
                        
                        $texts = \Drupal::service( 'publisso_gold.texts' );
                        $this->modpath = drupal_get_path( 'module', $this->modname );
                        
                        if ( $this->modpath && !$form_state->get( 'modpath' ) ) {
                                $form_state->set( 'modpath', $this->modpath );
                        }
                        
                        if ( !$this->modpath && $form_state->get( 'modpath' ) ) {
                                $this->modpath = $form_state->get( 'modpath' );
                        }
                        
                        $args = $form_state->getBuildInfo();
                        
                        if ( count( $args ) )
                                $wf_id = $args[ 'args' ][ 0 ][ 'wf_id' ];
                        
                        if ( !$wf_id )
                                $wf_id = $form_state->get( 'wf_id' );
                        
                        if ( $wf_id ) {
                                
                                $form_state->set( 'wf_id', $wf_id );
                                
                                $workflow = getDashboard( \Drupal::database(), $wf_id );
                                $workflow = $workflow[ 0 ];
                                $workflow_data = json_decode( base64_decode( $workflow->wf_data ) );
                                
                                $delta = [ 'workflow', 'workflow_data' ];
                                foreach ( $delta as $_ ) $form_state->set( $_, $$_ );
                                
                                $form [ 'commentforauthor' ] = [
                                        '#title'    => ( string )t( 'Comment for author' ),
                                        '#type'     => 'textarea',
                                        '#required' => true,
                                ];
                                
                                $form[ 'comment' ] = [
                                        
                                        '#title' => (string)t( 'Comment' ),
                                        '#type'  => 'textarea',
                                ];
                                
                                switch ( $workflow_data->state ) {
                                        
                                        case 'decision eic':
                                                
                                                $form[ 'eic_action' ] = [
                                                        '#title'    => (string)t( 'choose action' ),
                                                        '#type'     => 'select',
                                                        '#required' => true,
                                                        '#options'  => [
                                                                'accept'   => (string)t( $texts->get( 'workflow.accept_submission', 'fc' ) ),
                                                                'revision' => (string)t( $texts->get( 'workflow.author_revision', 'fc' ) ),
                                                                'reject'   => (string)t( $texts->get( 'workflow.reject_submission', 'fc' ) ),
                                                        ],
                                                ];
                                                
                                                break;
                                }
                                
                                $form[ 'submit' ] = [
                                        '#type'  => 'submit',
                                        '#value' => (string)t( 'Submit' ),
                                ];
                        }
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        $session = \Drupal::service( 'session' );
                        $objWorkflow = new \Drupal\publisso_gold\Controller\Workflow( $form_state->get( 'wf_id' ) );
                        
                        $eic_action = $form_state->getValue( 'eic_action' );
                        $delta = [ 'workflow', 'workflow_data' ];
                        foreach ( $delta as $_ ) $$_ = $form_state->get( $_ );
                        
                        switch ( $eic_action ) {
                                
                                case 'reject':
                                        //back to author
                                        $objWorkflow->setState( 'rejected' );
                                        //$workflow_data->state = 'rejected';
                                        //$workflow->wf_assigned_to = 'u:'.$workflow_data->author_uid;
                                        break;
                                
                                case 'accept':
                                        //up to editorial office
                                        $objWorkflow->setState( 'accepted' );
                                        break;
                                
                                
                                case 'revision':
                                        //back to author
                                        $objWorkflow->setState( 'in revision' );
                                        break;
                        }
                        
                        //save workflow
                        $objWorkflow->setElement( 'eic_last_edited', $session->get( 'user' )[ 'id' ] );
                        
                        //save comment if submitted
                        
                        
                        $comment = $form_state->getValue( 'comment' );
                        
                        if ( $comment != '' ) {
                                
                                $data = [
                                        'wfc_comment'        => base64_encode( $comment ),
                                        'wfc_created_by_uid' => $session->get( 'user' )[ 'id' ],
                                        'wfc_wfid'           => $objWorkflow->getElement( 'id' ),
                                ];
                                
                                \Drupal::database()->insert( 'rwPubgoldWorkflowComments' )->fields( $data )->execute();
                        }
                        
                        $commentforauthor = $form_state->getValue( 'commentforauthor' );
                        if ( $commentforauthor != '' ) {
                                
                                $data = [
                                        'cfa_comment'        => base64_encode( $commentforauthor ),
                                        'cfa_created_by_uid' => $session->get( 'user' )[ 'id' ],
                                        'cfa_wfid'           => $objWorkflow->getElement( 'id' ),
                                        'cfa_for_uid'        => $objWorkflow->getElement( 'created_by_uid' ),
                                ];
                                
                                \Drupal::database()->insert( 'rwPubgoldWorkflowCommentsForAuthors' )->fields( $data )->execute();
                        }
                        
                        //unlock workflow
                        $objWorkflow->unlock();
                        $form_state->setRedirect( 'publisso_gold.dashboard' );
                        
                        return $form;
                }
        
                /**
                 * @param null $tmpl
                 * @param null $vars
                 * @return string|string[]|null
                 */
                private function renderVars ($tmpl = null, $vars = null ) {
                        
                        if ( $tmpl == null || $vars == null ) {
                                //set Site-Vars
                                $this->tmpl = str_replace( array_keys( $this->tmpl_vars ), array_values( $this->tmpl_vars ), $this->tmpl );
                                
                                //remove unused vars
                                $this->tmpl = preg_replace( '(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl );
                        }
                        else {
                                //set Site-Vars
                                $tmpl = str_replace( array_keys( $vars ), array_values( $vars ), $tmpl );
                                
                                //remove unused vars
                                return preg_replace( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
                        }
                }
        }
