<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\siteAdministration.
 */
namespace Drupal\publisso_gold\Form;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a simple example form.
 */
class siteAdministration extends FormBase {
        public function __construct() {
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function getFormId() {
                return 'siteAdministration';
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
                
                $rq = \Drupal::service ( 'request_stack' )->getCurrentRequest ();
                
                if ($rq->query->has ( 'action' )) {
                        
                        $messenger = \Drupal::service('messenger');
                        
                        list ( $topic, $action, $param ) = explode ( "|", $rq->query->get ( 'action' ), 3 );
                        $param = explode ( "|", $param );
                        
                        switch ($topic) {
                                
                                case 'pr' : // pending registrations
                                        
                                        switch ($action) {
                                                
                                                case 'rs' : // resend reginfo
                                                        
                                                        $id = $param [0];
                                                        $ds = \Drupal::database()->select('rwPubgoldRegisterPending', 't')->fields('t', ['rp_uuid', 'rp_email'])->condition('rp_id', $id, '=')->execute()->fetchAssoc();
                                                        
                                                        $url = Url::fromRoute ( 'publisso_gold.confirm_register', [ 
                                                                'token' => $ds['rp_uuid'] 
                                                        ] );
                                                        $url->setAbsolute ();
                                                        $link = $url->toString ();
                                                        
                                                        $mail = ( string ) $this->t ( "Thank you for registering at \"PUBLISSO\"!" ) . "\n" . "\n" . t ( "To confirm your registration click the link below or copy it to the address-bar in your browser:" ) . "\n" . $link . "\n" . "\n" . t ( "Kindly regards" ) . "\n" . t ( "Your Publisso - Team" );
                                                        
                                                        $params = [ 
                                                                'headers' => [ 
                                                                        'MIME-Version' => '1.0',
                                                                        'Content-Type' => 'text/plain; charset=UTF-8;',
                                                                        'Content-Transfer-Encoding' => 'Base64',
                                                                        'X-Mailer' => 'Publisso Mailer',
                                                                        'From' => 'PUBLISSO <livingbooks@zbmed.de>' 
                                                                ],
                                                                'to' => $ds['rp_email'],
                                                                'body' => base64_encode ( $mail ),
                                                                'subject' => ( string ) $this->t ( 'Registration for PUBLISSO Books' ) 
                                                        ];
                                                        $mail_system    = \Drupal::service('plugin.manager.mail')->getInstance(array('module' => $this->modname, 'key' => $ds['rp_uuid']));
                                                        $mail_system->mail ( $params );
                                                        $messenger->addMessage((string)$this->t('Registration-infomail successfully sent'));
                                                        break;
                                                
                                                case 'dl' : // delete
                                                        
                                                        $id = $param [0];
                                                        \Drupal::database ()->delete ( 'rwPubgoldRegisterPending' )->condition ( 'rp_id', $id, '=' )->execute ();
                                                        $messenger->addMessage((string)$this->t('Pending registration deleted'));
                                                        break;
                                                
                                                case 'ex' : // extend time
                                                        
                                                        $id = $param [0];
                                                        $qry = \Drupal::database ()->update ( 'rwPubgoldRegisterPending' );
                                                        $qry->expression ( 'rp_timestamp', 'NOW()' );
                                                        $qry->condition ( 'rp_id', $id, '=' )->execute ();
                                                        $messenger->addMessage((string)$this->t('Pending registration time extended'));
                                                        break;
                                        }
                                        break;
                        }
                        
                        return new RedirectResponse ( Url::createFromRequest ( $rq )->toString () );
                }
                
                $setup = \Drupal::service ( 'publisso_gold.setup' );
                $texts = \Drupal::service ( 'publisso_gold.texts' );
                
                /*
                 * $form['debug'] = [
                 * '#markup' => print_r($setup, 1),
                 * '#prefix' => '<pre>',
                 * '#suffix' => '</pre>'
                 * ];
                 */
                $form ['admin'] = array (
                        '#type' => 'vertical_tabs',
                        '#default_tab' => 'edit-setup' 
                );
                
                $form ['setup'] = array (
                        '#type' => 'details',
                        '#title' => $this->t ( 'Setup' ),
                        '#group' => 'admin',
                        '#tree' => true 
                );
                
                $form ['setup'] ['form'] = array (
                        '#type' => 'container',
                        'content' => [ ] 
                );
                
                foreach ( $setup->getKeys () as $key ) {
                        $form ['setup'] ['form'] ['content'] [$key] = [ 
                                
                                '#type' => 'details',
                                '#title' => $setup->getDescription ( $key ) . " ($key)",
                                'content' => [ 
                                        'description' => [ 
                                                '#type' => 'textfield',
                                                '#default_value' => $setup->getDescription ( $key ) 
                                        ],
                                        'value' => [ 
                                                '#type' => 'textarea',
                                                '#rows' => 3,
                                                '#default_value' => $setup->getValue ( $key ) 
                                        ] 
                                ] 
                        ];
                }
                
                $form ['setup'] ['form'] ['submit'] = [ 
                        '#type' => 'submit',
                        '#value' => \Drupal::service ( 'publisso_gold.texts' )->get ( 'global.save', 'fc' ),
                        '#submit' => [ 
                                '::saveSetup' 
                        ] 
                ];
                
                $form ['texts'] = array (
                        '#type' => 'details',
                        '#title' => $this->t ( 'Texts' ),
                        '#group' => 'admin',
                        '#tree' => true 
                );
                
                $form ['texts'] ['form'] = array (
                        '#type' => 'container',
                        'content' => [ ] 
                );
                
                foreach ( $texts->getKeys () as $key ) {
                        $form ['texts'] ['form'] ['content'] [$key] = [ 
                                
                                '#type' => 'fieldset',
                                'content' => [ 
                                        'key' => [ 
                                                '#markup' => $key 
                                        ],
                                        'value' => [ 
                                                '#type' => 'textfield',
                                                '#maxlength' => 500,
                                                '#default_value' => $texts->get ( $key ) 
                                        ] 
                                ] 
                        ];
                }
                
                $form ['texts'] ['form'] ['submit'] = [ 
                        '#type' => 'submit',
                        '#value' => \Drupal::service ( 'publisso_gold.texts' )->get ( 'global.save', 'fc' ),
                        '#submit' => [ 
                                '::saveTexts' 
                        ] 
                ];
                
                $form ['pending_register'] = [ 
                        '#type' => 'details',
                        '#title' => ( string ) $this->t ( 'Pending registrations' ),
                        '#group' => 'admin',
                        '#tree' => true 
                ];
                
                $form ['pending_register'] ['table'] = [ 
                        '#type' => 'table',
                        '#header' => [ 
                                'rp_id' => ( string ) $this->t ( 'ID' ),
                                'rp_email' => ( string ) $this->t ( 'Email' ),
                                'rp_firstname' => ( string ) $this->t ( 'Firstname' ),
                                'rp_lastname' => ( string ) $this->t ( 'Lastname' ),
                                'rp_username' => ( string ) $this->t ( 'User' ),
                                'rp_timestamp' => ( string ) $this->t ( 'Timestamp' ),
                                'end' => ( string ) $this->t ( 'Time left' ),
                                'actions' => ( string ) $this->t ( 'Actions' ) 
                        ],
                        '#empty' => ( string ) $this->t ( 'No pending registrations found' ) 
                ];
                
                $qry = \Drupal::database ()->select ( 'rwPubgoldRegisterPending', 't' )->fields ( 't', [ 
                        'rp_id',
                        'rp_email',
                        'rp_firstname',
                        'rp_lastname',
                        'rp_username',
                        'rp_timestamp' 
                ] );
                
                $rows = [ ];
                
                if ($qry->countQuery ()->execute ()->fetchField ()) {
                        
                        foreach ( $qry->orderBy ( 'rp_timestamp', 'DESC' )->execute ()->fetchAll () as $row ) {
                                
                                $nrow = [];
                                $start = strtotime ( $row->rp_timestamp );
                                $end = $start + $setup->getValue ( 'system.keep.register_pending' ) * 3600;
                                
                                $row->rp_timestamp = date ( "d.m.Y H:i:s", $start );
                                $secs = $end - time ();
                                
                                // $row->end = ($end - $start) / 3600;
                                $row->end = gmdate ( "d H:i:s", $secs );
                                
                                foreach ( $row as $k => $v )
                                        $nrow [] = [ 
                                                'data' => $v,
                                                'field' => $k 
                                        ];
                                
                                $nrow [] = [
                                        'data' => [ 
                                                '#type' => 'dropbutton',
                                                '#links' => [ 
                                                        'resend' => [ 
                                                                '#type' => 'link',
                                                                '#title' => ( string ) $this->t ( 'Resend email' ),
                                                                '#url' => Url::fromRoute ( \Drupal::routeMatch ()->getRouteName (), [ 
                                                                        'action' => 'pr|rs|' . $row->rp_id 
                                                                ] ) 
                                                        ],
                                                        'delete' => [ 
                                                                '#type' => 'link',
                                                                '#title' => ( string ) $this->t ( 'Delete' ),
                                                                '#url' => Url::fromRoute ( \Drupal::routeMatch ()->getRouteName (), [ 
                                                                        'action' => 'pr|dl|' . $row->rp_id 
                                                                ] ) 
                                                        ],
                                                        'keep' => [ 
                                                                '#type' => 'link',
                                                                '#title' => ( string ) $this->t ( 'Extend' ),
                                                                '#url' => Url::fromRoute ( \Drupal::routeMatch ()->getRouteName (), [ 
                                                                        'action' => 'pr|ex|' . $row->rp_id 
                                                                ] ) 
                                                        ] 
                                                ] 
                                        ],
                                        'field' => 'actions' ,
                                        'width' => 150
                                ];
                                $rows [] = ( array ) $nrow;
                        }
                }
                
                $form ['pending_register'] ['table'] ['#rows'] = $rows;
                $form ['#cache'] ['max-age'] = 0;
                $form ['#attached'] ['library'] [] = 'field_group/formatter.horizontal_tabs';
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function validateForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function saveTexts(array &$form, FormStateInterface $form_state) {
                $texts = \Drupal::service ( 'publisso_gold.texts' );
                
                foreach ( $form_state->getValue ( 'texts' ) ['form'] ['content'] as $key => $_ ) {
                        
                        if ($texts->get ( $key ) != $_ ['content'] ['value']) {
                                $texts->update ( $key, $_ ['content'] ['value'] );
                        }
                }
                
                \Drupal::service ( 'messenger' )->addMessage ( 'Data successfully saved' );
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function saveSetup(array &$form, FormStateInterface $form_state) {
                $setup = \Drupal::service ( 'publisso_gold.setup' );
                
                foreach ( $form_state->getValue ( 'setup' ) ['form'] ['content'] as $key => $_ ) {
                        
                        $_ = $_ ['content'];
                        
                        if ($setup->getValue ( $key ) != $_ ['value']) {
                                // create new entry
                                $setup->createEntryForKey ( $key, $_ ['value'], $_ ['description'] );
                        } elseif (($setup->getDescription ( $key ) != $_ ['description']) && ! empty ( $_ ['description'] )) {
                                // update description
                                error_log ( 'Update DESC "' . $_ ['description'] . '"' );
                                $setup->updateDescriptionForKey ( $key, $_ ['description'] );
                        }
                }
                
                \Drupal::service ( 'messenger' )->addMessage ( 'Data successfully saved' );
                return $form;
        }
}
