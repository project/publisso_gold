<?php
        
        
        namespace Drupal\publisso_gold\Form;
        
        
        use Drupal\Core\Ajax\AjaxResponse;
        use Drupal\Core\Ajax\CloseModalDialogCommand;
        use Drupal\Core\Ajax\HtmlCommand;
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Classes\Ajax\TimeoutHtmlCommand;
        use Drupal\publisso_gold\Controller\Manager\LicenseManager;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;
        use Drupal\publisso_gold\Controller\Workflow\Workflow;

        /**
         * Class AdaptItemAbstract
         * @package Drupal\publisso_gold\Form
         */
        class AdaptItemAbstract extends FormBase {
                
                /**
                 * @inheritDoc
                 */
                public function getFormId () {
                        return "formal-adapt-item";
                }
        
                /**
                 * @inheritDoc
                 */
                public function buildForm ( array $form, FormStateInterface $form_state, $wf_id = null ) {
                        
                        if(null === ($workflow = $this->loadWorkflow($form_state, $wf_id))) return $form;
                        
                        $form['container'] = [
                                '#tree' => true,
                                '#type' => 'container',
                                'grid' => [
                                        '#type' => 'container',
                                
                                        'left' => [
                                                '#type' => 'container',
                                                '#prefix' => '<div class="col-sm-4">',
                                                '#suffix' => '</div>'
                                        ],
                                        'center' => [
                                                '#type' => 'container',
                                                '#prefix' => '<div class="col-sm-4">',
                                                '#suffix' => '</div>'
                                        ],
                                        'right' => [
                                                '#type' => 'container',
                                                '#prefix' => '<div class="col-sm-4">',
                                                '#suffix' => '</div>'
                                        ]
                                ],
                                'bottom' => [
                                        '#type' => 'container',
                                        '#prefix' => '<div class="col-sm-12">',
                                        '#suffix' => '</div><br clear=""all">'
                                ]
                        ];
                        
                        $left   = &$form['container']['grid']['left'];
                        $center = &$form['container']['grid']['center'];
                        $right  = &$form['container']['grid']['right'];
                        $bottom = &$form['container']['bottom'];
                        
                        //first row
                        $left[]['doi'] = [
                                '#type' => 'textfield',
                                '#title' => (string)$this->t('DOI'),
                                '#required' => true,
                                '#default_value' => $workflow->getDataElement('doi')
                        ];
                        
                        $center[]['version'] = [
                                '#type' => 'textfield',
                                '#title' => (string)$this->t('Version'),
                                '#default_value' => $workflow->getDataElement('version')
                        ];
                        
                        $right[]['doc_no'] = [
                                '#type' => 'textfield',
                                '#title' => (string)$this->t('Doc-no'),
                                '#required' => true,
                                '#default_value' => $workflow->getDataElement('doc_no')
                        ];
                        
                        //second row
                        
                        $left[]['publisher_abbr'] = [
                                '#type' => 'textfield',
                                '#title' => (string)$this->t('Publisher (Abbr.)'),
                                '#required' => true,
                                '#default_value' => $workflow->getDataElement('publisher_abbr') ?? $workflow->getParentMedium()->getElement('publisher_abbr')
                        ];
        
                        $center[]['publisher_name'] = [
                                '#type' => 'textfield',
                                '#title' => (string)$this->t('Publisher name'),
                                '#required' => true,
                                '#default_value' => $workflow->getDataElement('publisher_name') ?? $workflow->getParentMedium()->getElement('publisher_name')
                        ];
                        
                        $right[]['publisher_city'] = [
                                '#type' => 'textfield',
                                '#title' => (string)$this->t('Publisher city'),
                                '#default_value' => $workflow->getDataElement('publisher_city') ?? $workflow->getParentMedium()->getElement('publisher_city'),
                                '#required' => true
                        ];
                        
                        //third row
                        
                        $left[]['publication_year'] = [
                                '#type' => 'number',
                                '#required' => true,
                                '#min' => date('Y') - 20,
                                '#max' => date('Y') + 20,
                                '#title' => (string)$this->t('Publication year'),
                                '#default_value' => $workflow->getDataElement('publication_year') ?? date('Y'),
                                '#label_attributes' => [
                                        'style' => 'display: block;'
                                ],
                                '#attributes' => [
                                        'style' => 'width: 100%;'
                                ]
                        ];
                        
                        $center[]['license'] = [
                                '#type' => 'select',
                                '#title' => (string)$this->t('License'),
                                '#options' => LicenseManager::getFormSelectOptions(),
                                '#default_value' => $workflow->getDataElement('license'),
                                '#required' => true
                        ];
                        
                        //bottom
                        $bottom[] = [
                                '#type' => 'fieldset',
                                '#title' => (string)$this->t('Override dates'),
                                '#prefix' => '<br>',
                                '#collapsible' => true,
                                '#collapsed' => true,
                                'container' => [
                                        '#type' => 'container',
                                        'received' => [
                                                '#prefix' => '<div class="col-sm-3">',
                                                '#suffix' => '</div>',
                                                '#type' => 'date',
                                                '#title' => (string)$this->t('Received'),
                                                '#default_value' => $workflow->getDataElement('received')
                                        ],
                                        'revised' => [
                                                '#prefix' => '<div class="col-sm-3">',
                                                '#suffix' => '</div>',
                                                '#type' => 'date',
                                                '#title' => (string)$this->t('Revised'),
                                                '#default_value' => $workflow->getDataElement('revised')
                                        ],
                                        'accepted' => [
                                                '#prefix' => '<div class="col-sm-3">',
                                                '#suffix' => '</div>',
                                                '#type' => 'date',
                                                '#title' => (string)$this->t('Accepted'),
                                                '#default_value' => $workflow->getDataElement('accepted')
                                        ],
                                        'published' => [
                                                '#prefix' => '<div class="col-sm-3">',
                                                '#suffix' => '</div>',
                                                '#type' => 'date',
                                                '#title' => (string)$this->t('Published'),
                                                '#default_value' => $workflow->getDataElement('published')
                                        ]
                                ]
                        ];
                        
                        $bottom[]['actions'] = $this->getActions();
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function validateForm (array &$form, FormStateInterface $form_state ) {
                        parent::validateForm( $form, $form_state ); // TODO: Change the autogenerated stub
                }
        
                /**
                 * @inheritDoc
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
        
                        $response = new AjaxResponse();
                        $workflow = WorkflowManager::getItem($form_state->get('wf_id'));
                        
                        if($workflow->getLock()) {
                                foreach ( $this->getContainerValues( $form_state->getValues()[ 'container' ], [ 'container', 'left', 'center', 'right', 'bottom', 'grid' ] ) as $key => $value ) {
                                        $workflow->setDataElement( $key, $value, false );
                                }
                                $workflow->save();
                                $workflow->transferData2Medium();
                                $workflow->unlock();
                                $this->messenger()->addMessage((string)$this->t('Successfully saved!'));
                                $response->addCommand(new HtmlCommand('#messages', '<span style="color: darkgreen;">'.((string)$this->t('Successfully saved!')).'</span>'));
                                $response->addCommand(new TimeoutHtmlCommand('#messages', 1000, '&nbsp;'));
                        }
                        else{
                                $response->addCommand(new HtmlCommand('#messages', '<span style="color: red;">'.((string)$this->t('Error: Cant get lock for this item!')).'</span>'));
                                $this->messenger()->addError((string)$this->t('Cant store form-data. Unable to get lock!'));
                        }
                        
                        return $response;
                }
        
                /**
                 * @param FormStateInterface $form_state
                 * @param null $wf_id
                 * @return Workflow|null
                 */
                private function loadWorkflow(FormStateInterface $form_state, $wf_id = null) :? Workflow{
                        
                        if(!$form_state->has('wf_id')) $form_state->set('wf_id', $wf_id);
                        if(!$form_state->get('wf_id')) return null;
                        return WorkflowManager::getItem($form_state->get('wf_id'));
                }
        
                /**
                 * @return array
                 */
                private function getActions(){
                        
                        return [
                                '#type' => 'actions',
                                [
                                        'cancel' => [
                                                '#type' => 'button',
                                                '#value' => (string)$this->t('Cancel'),
                                                '#button_type' => 'danger',
                                                '#attributes' => [
                                                        'class' => ['btn', 'btn-danger']
                                                ],
                                                '#ajax' => [
                                                        'callback' => '::cancel',
                                                        'progress' => [
                                                                'type' => 'none'
                                                        ]
                                                ],
                                                '#limit_validation_errors' => []
                                        ],
                                        'submit' => [
                                                '#type' => 'button',
                                                '#value' => (string)$this->t('Submit'),
                                                '#button_type' => 'success',
                                                '#attributes' => [
                                                        'class' => ['btn', 'btn-success']
                                                ],
                                                '#ajax' => [
                                                        'callback' => '::submitForm',
                                                        'progress' => [
                                                                'type' => 'none'
                                                        ]
                                                ]
                                        ],
                                        'messages' => [
                                                '#type' => 'markup',
                                                '#markup' => '&nbsp;',
                                                '#prefix' => '<div id="messages">',
                                                '#suffix' => '</div>'
                                        ]
                                ]
                        ];
                }
                
                private function getContainerValues(array $haystack, array $excludeKeys = []) : array {
                        
                        $ret = [];
                        
                        foreach($haystack as $key => $value){
                                
                                if(!is_array($value) || (is_array($value && !in_array($key, $excludeKeys))))
                                        $ret[$key] = $value ?? null;
                                else
                                        $ret = array_merge($ret, $this->getContainerValues($value, $excludeKeys));
                                
                        }
                        
                        return $ret;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return AjaxResponse
                 */
                public function cancel(array &$form, FormStateInterface $form_state){
                        $response = new AjaxResponse();
                        $response->addCommand(new CloseModalDialogCommand());
                        return $response;
                }
        }