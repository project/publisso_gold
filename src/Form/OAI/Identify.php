<?php
        /**
         * Created by PhpStorm.
         * User: sseliger
         * Date: 02.04.19
         * Time: 14:49
         */
        
        namespace Drupal\publisso_gold\Form\OAI;
        
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Symfony\Component\HttpFoundation\Response;

        /**
         * Class Identify
         * @package Drupal\publisso_gold\Form\OAI
         */
        class Identify extends FormBase {
        
                /**
                 * @return string
                 */
                public function getFormId () {
                        return 'oai.identify';
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @param array $args
                 * @return array
                 */
                public function buildForm (array $form, FormStateInterface $form_state, $args = [] ) {
        
                        $form_state->set('formSubmitted', false);
                        $class = explode('\\', self::class);
                        $form['headline'] = [
                                '#markup' => 'OAI '.end($class).' Request Form',
                                '#prefix' => '<h2>',
                                '#suffix' => '</h2>'
                        ];
                        
                        $form['form'] = [
                                '#type' => 'container',
                                '#prefix' => '<div class="rwTable">',
                                '#suffix' => '</div>'
                        ];
                        
                        if(false === $form_state->get('formSubmitted')) {
                                
                                $form[ 'form' ][ 'submit' ] = [
                                        '#type'   => 'submit',
                                        '#value'  => (string)$this->t( 'Submit' ),
                                        '#prefix' => '<div class="rwTablerow"><div class="rwTablecell">',
                                        '#suffix' => '</div></div>'
                                ];
                        }
                        
                        $form['verb'] = [
                                '#type' => 'hidden',
                                '#value' => end($class)
                        ];
                        
                        $form['#method'] = 'post';
                        $form['#action'] = './'.end($class);
                        $form['#token'] = false;
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function submitForm (array &$form, FormStateInterface $form_state ) {
        
                        $class = explode('\\', self::class);
                        $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold.oai.response');
                        $data = [];
                        
                        $data['action'] = end($class);
                        $data['request'] = \Drupal::service('request_stack')->getCurrentRequest()->getUri();
                        
                        $tempstore->set('data', $data);
                        
                        $form_state->setRedirect('publisso_gold.oai', ['paramType' => 'response', 'paramID' => end($class)]);
                        return $form;
                }
        }
