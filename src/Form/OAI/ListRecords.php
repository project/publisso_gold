<?php
        /**
         * Created by PhpStorm.
         * User: sseliger
         * Date: 02.04.19
         * Time: 14:49
         */
        
        namespace Drupal\publisso_gold\Form\OAI;
        
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;

        /**
         * Class ListRecords
         * @package Drupal\publisso_gold\Form\OAI
         */
        class ListRecords extends FormBase {
        
                /**
                 * @return string
                 */
                public function getFormId () {
                        return 'oai.listrecords';
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @param array $args
                 * @return array
                 */
                public function buildForm (array $form, FormStateInterface $form_state, $args = [] ) {
        
                        $form_state->set('formSubmitted', false);
                        $class = explode('\\', self::class);
                        
                        $form['headline'] = [
                                '#markup' => 'OAI '.end(explode('\\', self::class)).' Request Form',
                                '#prefix' => '<h2>',
                                '#suffix' => '</h2>'
                        ];
                        
                        $form['form'] = [
                                '#type' => 'container',
                                '#prefix' => '<div class="rwTable">',
                                '#suffix' => '</div>'
                        ];
                        
                        $form['form']['from'] = [
                                '#type' => 'date',
                                '#title' => 'from:',
                                '#prefix' => '<div class="rwTablerow"><div class="rwTablecell"><div class="rwTable"><div class="rwTablerow"><div class="rwTablecell">',
                                '#suffix' => '</div>'
                        ];
        
                        $form['form']['until'] = [
                                '#type' => 'date',
                                '#title' => 'until:',
                                '#prefix' => '<div class="rwTablecell">',
                                '#suffix' => '</div></div></div></div></div>'
                        ];
        
                        $form['form']['set'] = [
                                '#type' => 'textfield',
                                '#title' => 'set:',
                                '#prefix' => '<div class="rwTablerow"><div class="rwTablecell"><div class="rwTable"><div class="rwTablerow"><div class="rwTablecell">',
                                '#suffix' => '</div></div>'
                        ];
        
                        $form['form']['metadataPrefix'] = [
                                '#type' => 'textfield',
                                '#title' => 'metadataPrefix:',
                                '#prefix' => '<div class="rwTablerow"><div class="rwTablecell">',
                                '#suffix' => '</div></div>'
                        ];
                        
                        $form['form']['submit'] = [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Submit'),
                                '#prefix' => '<div class="rwTablerow"><div class="rwTablecell">',
                                '#suffix' => '</div></div></div></div></div>'
                        ];
                        
                        $form['verb'] = [
                                '#type' => 'hidden',
                                '#value' => end(explode('\\', self::class))
                        ];
        
                        $form['#method'] = 'post';
                        $form['#action'] = './'.end($class);
                        $form['#token'] = false;
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function submitForm (array &$form, FormStateInterface $form_state ) {
                        
                        $class = explode('\\', self::class);
                        $tempstore = \Drupal::service('user.private_tempstore')->get('publisso_gold.oai.response');
                        $data = [];
        
                        $data['verb'] = end($class);
                        $data['request'] = \Drupal::service('request_stack')->getCurrentRequest()->getUri();
                        $data['set'] = $form_state->getValue('set');
                        $data['from'] = $form_state->getValue('from');
                        $data['until'] = $form_state->getValue('until');
                        $data['metadataPrefix'] = $form_state->getValue('metadataPrefix');
        
                        $tempstore->set('data', $data);
        
                        $form_state->setRedirect('publisso_gold.oai', ['paramType' => 'response', 'paramID' => end($class)]);
                        return $form;
                }
        }
