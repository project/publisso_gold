<?php
        /**
         * Created by PhpStorm.
         * User: sseliger
         * Date: 02.04.19
         * Time: 14:49
         */
        
        namespace Drupal\publisso_gold\Form\OAI;
        
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;

        /**
         * Class ListRecordsResumption
         * @package Drupal\publisso_gold\Form\OAI
         */
        class ListRecordsResumption extends FormBase {
        
                /**
                 * @return string
                 */
                public function getFormId () {
                        return 'oai.listrecordsresumption';
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @param array $args
                 * @return array
                 */
                public function buildForm (array $form, FormStateInterface $form_state, $args = [] ) {
        
                        $form_state->set('formSubmitted', false);
                        $class = explode('\\', self::class);
                        
                        $form['headline'] = [
                                '#markup' => 'OAI '.end(explode('\\', self::class)).' Request Form',
                                '#prefix' => '<h2>',
                                '#suffix' => '</h2>'
                        ];
                        
                        $form['form'] = [
                                '#type' => 'container',
                                '#prefix' => '<div class="rwTable">',
                                '#suffix' => '</div>'
                        ];
                        
                        $form['form']['resumptionToken'] = [
                                '#type' => 'textfield',
                                '#title' => (string)$this->t('resumptionToken').':',
                                '#prefix' => '<div class="rwTablerow"><div class="rwTablecell">',
                                '#suffix' => '</div></div>'
                        ];
                        
                        $form['form']['submit'] = [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Submit'),
                                '#prefix' => '<div class="rwTablerow"><div class="rwTablecell">',
                                '#suffix' => '</div></div>'
                        ];
                        
                        $form['verb'] = [
                                '#type' => 'hidden',
                                '#value' => end(explode('\\', self::class))
                        ];
        
                        $form['#method'] = 'get';
                        $form['#action'] = '../response/ListRecords';
                        $form['#token'] = false;
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function submitForm (array &$form, FormStateInterface $form_state ) {
                        #$form_state->setRedirect('publisso_gold.oai', ['paramType' => 'response', 'paramID' => 'ListRecords', 'resumptionToken' => $form_state->getValue('resumptionToken')]);
                        return $form;
                }
        }
