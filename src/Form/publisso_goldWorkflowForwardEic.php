<?php
        /**
         * @file
         * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowForwardEic.
         */
        
        namespace Drupal\publisso_gold\Form;
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\Core\Database\Connection;
        use Symfony\Component\DependencyInjection\ContainerInterface;
        
        /**
         * Provides a simple example form.
         */
        class publisso_goldWorkflowForwardEic extends FormBase {
                
                private $modname = 'publisso_gold';
                private $database;
                private $modpath;
                private $num_corporations;
                
                public function __construct ( Connection $database ) {
                        $this->database = $database;
                }
        
                /**
                 * @param ContainerInterface $container
                 * @return publisso_goldWorkflowForwardEic|static
                 */
                public static function create (ContainerInterface $container ) {
                        return new static( $container->get( 'database' ) );
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function getFormId () {
                        return 'publisso_goldworkflowforwardeic';
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function buildForm ( array $form, FormStateInterface $form_state ) {
                        
                        $this->modpath = drupal_get_path( 'module', $this->modname );
                        
                        if ( $this->modpath && !$form_state->get( 'modpath' ) ) {
                                $form_state->set( 'modpath', $this->modpath );
                        }
                        
                        if ( !$this->modpath && $form_state->get( 'modpath' ) ) {
                                $this->modpath = $form_state->get( 'modpath' );
                        }
                        
                        $args = $form_state->getBuildInfo();
                        
                        if ( count( $args ) )
                                $wf_id = $args[ 'args' ][ 0 ][ 'wf_id' ];
                        
                        if ( !$wf_id )
                                $wf_id = $form_state->get( 'wf_id' );
                        
                        if ( $wf_id ) {
                                
                                $form_state->set( 'wf_id', $wf_id );
                                $form_state->set( 'workflow', new \Drupal\publisso_gold\Controller\Workflow( $form_state->get( 'wf_id' ) ) );
                                
                                //for states "review finished" or "revision finished"
                                if ( $form_state->get( 'workflow' )->getDataElement( 'state' ) != 'assigned to editor' ) {
                                        
                                        $form[ 'comment_for_author' ] = [
                                                '#type'  => 'textarea',
                                                '#title' => (string)$this->t( 'Comment for author' ),
                                        ];
                                        
                                        $form[ 'recommendation' ] = [
                                                '#type'     => 'select',
                                                '#title'    => (string)$this->t( 'Recommendation' ),
                                                '#required' => true,
                                                '#suffix'   => (string)$this->t( 'Recommendation is necessary for "Decision EiC"' ) . '<br><br>',
                                                '#options'  => [
                                                        'accept'   => (string)$this->t( 'Accept submission' ),
                                                        'reject'   => (string)$this->t( 'Reject submission' ),
                                                        'revision' => (string)$this->t( 'To author for revision' ),
                                                ],
                                        ];
                                }
                                
                                $form[ 'revision' ] = [
                                        'submit' => [
                                                '#type'                    => 'submit',
                                                '#value'                   => t( 'Author-revision' ),
                                                '#submit'                  => [ '::authorRevision' ],
                                                '#prefix'                  => '<div style="float: left;">',
                                                '#suffix'                  => '&nbsp;&nbsp;&nbsp;&nbsp;',
                                                '#button_type'             => 'primary',
                                                '#limit_validation_errors' => [],
                                        ],
                                ];
                                
                                $form[ 'decision_eic' ] = [
                                        'submit' => [
                                                '#type'        => 'submit',
                                                '#value'       => t( 'Decision Editor-in-Chief' ),
                                                '#suffix'      => '</div><br clear="all">',
                                                '#button_type' => 'success',
                                        ],
                                ];
                        }
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function validateForm ( array &$form, FormStateInterface $form_state ) {
                        
                        return $form;
                }
                
                /**
                 * {@inheritdoc}
                 */
                public function submitForm ( array &$form, FormStateInterface $form_state ) {
                        
                        
                        $form_state->get( 'workflow' )->historize();
                        $form_state->get( 'workflow' )->setState( 'decision eic' );
                        /*
                        $workflow = getDashboard($this->database, $form_state->get('wf_id'));
                        $workflow = $workflow[0];
                        
                        //save current workflow in history
                        $workflow_history = array();
                        $workflow_history['wfh_created_by_uid'] = $_SESSION['user']['id'];
                        
                        foreach($workflow as $k => $v){
                            
                            if($k == 'wf_id'){
                                $key = 'wfh_wfid';
                            }
                            else{
                                $key = explode('_', $k, 2);
                                $key = $key[0].'h_'.$key[1];
                            }
                            
                            $workflow_history[$key] = $v;
                        }
                        
                        \Drupal::database()->insert('rwPubgoldWorkflowHistory')->fields($workflow_history)->execute();
                        
                        //set new status
                        $workflow_data = json_decode(base64_decode($workflow->wf_data));
                        $workflow_data->state = 'decision eic';
                        $workflow->wf_data = base64_encode(json_encode($workflow_data));
                        */
                        //set recommendation
                        
                        if ( !empty( $form_state->getValue( 'comment_for_author' ) ) ) {
                                
                                $comment = [
                                        'cfa_created_by_uid' => \Drupal::service( 'session' )->get( 'user' )[ 'id' ],
                                        'cfa_for_uid'        => $form_state->get( 'workflow' )->getElement( 'created_by_uid' ),
                                        'cfa_comment'        => base64_encode( $form_state->getValue( 'comment_for_author' ) ),
                                        'cfa_wfid'           => $form_state->get( 'workflow' )->getElement( 'id' ),
                                ];
                                
                                try {
                                        \Drupal::database()
                                               ->insert( 'rwPubgoldWorkflowCommentsForAuthors' )
                                               ->fields( $comment )
                                               ->execute()
                                        ;
                                }
                                catch ( \Exception $e ) {
                                        \Drupal::service( 'messenger' )->addError( $e->getMessage() );
                                        return $form;
                                }
                        }
                        
                        $form_state->get( 'workflow' )->setRecommendation( $form_state->getValue( 'recommendation' ) );
                        $form_state->get( 'workflow' )->unlock();
                        /*
                        //unlock data
                        $sql = "
                            update
                                rwpg_rwPubgoldWorkflow
                            set
                                wf_locked = 0,
                                wf_locked_by_uid = NULL,
                                wf_assigned_to = :eic,
                                wf_data = :wf_data
                            where
                                wf_id = :wf_id
                        ";
                        
                        $data = [
                            ':wf_id' => $form_state->get('wf_id'),
                            ':eic' => 'u:'.implode(',u:', explode(',', $workflow->wf_assigned_to_eic)),
                            ':wf_data' => $workflow->wf_data
                        ];
                        
                        \Drupal::database()->query($sql, $data);
                        */
                        $form_state->setRedirect( 'publisso_gold.dashboard' );
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array
                 */
                public function authorRevision (array &$form, FormStateInterface $form_state ) {
        
                        if ( !empty( $form_state->getUserInput()['comment_for_author' ] ) ) {
                
                                $comment = [
                                        'cfa_created_by_uid' => \Drupal::service( 'session' )->get( 'user' )[ 'id' ],
                                        'cfa_for_uid'        => $form_state->get( 'workflow' )->getElement( 'created_by_uid' ),
                                        'cfa_comment'        => base64_encode( $form_state->getUserInput()['comment_for_author' ] ),
                                        'cfa_wfid'           => $form_state->get( 'workflow' )->getElement( 'id' ),
                                ];
                
                                try {
                                        \Drupal::database()
                                               ->insert( 'rwPubgoldWorkflowCommentsForAuthors' )
                                               ->fields( $comment )
                                               ->execute()
                                        ;
                                }
                                catch ( \Exception $e ) {
                                        \Drupal::service( 'messenger' )->addError( $e->getMessage() );
                                        return $form;
                                }
                        }
                        
                        $objWorkflow = new \Drupal\publisso_gold\Controller\Workflow( $form_state->get( 'wf_id' ) );
                        $objWorkflow->historize();
                        $objWorkflow->setState( 'in revision' );
                        $objWorkflow->setElement( 'editor_last_edited', $_SESSION[ 'user' ][ 'id' ] );
                        $objWorkflow->unlock();
                        
                        $form_state->setRedirect( 'publisso_gold.dashboard' );
                        
                        return $form;
                }
        
                /**
                 * @param null $tmpl
                 * @param null $vars
                 * @return string|string[]|null
                 */
                private function renderVars ($tmpl = null, $vars = null ) {
                        
                        if ( $tmpl == null || $vars == null ) {
                                //set Site-Vars
                                $this->tmpl = str_replace( array_keys( $this->tmpl_vars ), array_values( $this->tmpl_vars ), $this->tmpl );
                                
                                //remove unused vars
                                $this->tmpl = preg_replace( '(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl );
                        }
                        else {
                                //set Site-Vars
                                $tmpl = str_replace( array_keys( $vars ), array_values( $vars ), $tmpl );
                                
                                //remove unused vars
                                return preg_replace( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
                        }
                }
        }
