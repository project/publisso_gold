<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldBookmanagementAddChapter.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Mail;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;

/**
 * Provides a simple example form.
 */
class publisso_goldBookmanagementAddChapter extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldBookmanagementAddChapter|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldbookmanagementaddchapter';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $this->modpath = drupal_get_path('module', $this->modname);
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        if(!$this->database)
            $this->database = \Drupal::database();
        
        //expect build-info-args as array, index 0 is bk_id
        $build_info = $form_state->getBuildInfo();
        $args = $build_info['args'];

        $form_state->set('bk_id', $args[0]['bk_id']);
        
        if(array_key_exists('wf_id', $args[0])){
            
            $workflow = getDashboard(\Drupal::database(), $args[0]['wf_id']);
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            $abstract = json_decode(base64_decode($workflow_data->abstract));
            $chapter_text = json_decode(base64_decode($workflow_data->chapter_text));
        }
        
        if(!$form_state->get('bk_id')){
            drupal_set_message(t('No book given!'), 'error');
            return array();
        }
        
        $book = $this->database->select('rwPubgoldBooks', 'bk')->fields('bk', [])->condition('bk_id', $form_state->get('bk_id'), '=')->execute()->fetchAssoc();
        
        $form = [
            
            'site_headline' => [
            '#type' => 'markup',
            '#markup' => '<h1>'.t('Add Chapter').'</h1>'
            ],
            
            'book_title' => [
                '#type' => 'markup',
                '#markup' => '<h2>'.$book['bk_title'].'</h2>'
            ],
            
            'author' => [
                '#type' => 'select',
                '#title' => t('Author'),
                '#options' => getUsersByRole($this->database, [7]),
                '#required' => true
            ],
            
            'title' => [
                '#type' => 'textfield',
                '#title' => t('Title'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->title : ''
            ],
            
            'license' => [
                '#type' => 'textfield',
                '#title' => t('License'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->license : ''
            ],
            
            'abstract' => [
                '#type' => 'text_format',
                '#title' => t('Abstract'),
                '#required' => true,
                '#format' => isset($abstract) ? $abstract->format : 'basic_html',
                '#default_value' => isset($abstract) ? $abstract->value : ''
            ],
            
            'publication_year' => [
                '#type' => 'textfield',
                '#title' => t('Publication Year'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->publication_year : ''
            ],
            
            'publication_place' => [
                '#type' => 'textfield',
                '#title' => t('Publication Place'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->publication_place : ''
            ],
            
            'publisher' => [
                '#type' => 'textfield',
                '#title' => t('Publisher'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->publisher : ''
            ],
            
            'doi' => [
                '#type' => 'textfield',
                '#title' => t('DOI'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->doi : ''
            ],
            
            'ddc' => [
                '#type' => 'textfield',
                '#title' => t('DDC'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->ddc : ''
            ],
            
            'corresponding_author' => [
                '#type' => 'textfield',
                '#title' => t('Corresponding Author'),
                '#required' => true,
                '#default_value' => isset($workflow_data) ? $workflow_data->corresponding_author : ''
            ],
            
            'chapter_text' => [
                '#type' => 'text_format',
                '#title' => t('Chapter Text'),
                '#required' => true,
                '#rows' => 50,
                '#format' => isset($chapter_text) ? $chapter_text->format : 'full_html',
                '#default_value' => isset($chapter_text) ? $chapter_text->value : ''
            ],
            
            'urn' => [
                '#type' => 'textfield',
                '#title' => t('URN'),
                '#default_value' => isset($workflow_data) ? $workflow_data->urn : ''
            ],
            
            'keywords' => [
                '#type' => 'textfield',
                '#title' => t('Keywords'),
                '#default_value' => isset($workflow_data) ? implode(', ', json_decode($workflow_data->keywords)) : ''
            ],
            
            'funding_number' => [
                '#type' => 'textfield',
                '#title' => t('Funding Number'),
                '#default_value' => isset($workflow_data) ? $workflow_data->funding_number : ''
            ],
            
            'funding_id' => [
                '#type' => 'textfield',
                '#title' => t('Funding ID'),
                '#default_value' => isset($workflow_data) ? $workflow_data->funding_id : ''
            ],
            
            'description_errata' => [
                '#type' => 'markup',
                '#markup' => '<span class="rwAlert">'.t('You can set Version OR Errata OR Correction!').'</span>'
            ],
            
            'version' => [
                '#type' => 'textfield',
                '#title' => t('Version'),
                '#default_value' => isset($workflow_data) ? $workflow_data->version : ''
            ],
            
            'errata' => [
                '#type' => 'text_format',
                '#title' => t('Errata'),
                '#format' => 'full_html'
            ],
            
            'correction' => [
                '#type' => 'text_format',
                '#title' => t('Correction'),
                '#format' => 'full_html'
            ],
            
            'submit' => [
                '#type' => 'submit',
                '#value' => t('Save Chapter')
            ]
            
        ];
        
        $form['#cache']['max-age'] = 0;
      
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        $alpha = 0;
        
        if($form_state->getValue('version') != '') $alpha++;
        
        $errata = $form_state->getValue('errata');
        if($errata['value'] != '') $alpha++;
        
        $correction = $form_state->getValue('correction');
        if($correction['value'] != '') $alpha++;
        
        if($alpha > 1){
            $form_state->setErrorByName('version', t('You can set Version OR Errata OR Correction!'));
        }
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $chapter_authors        = $form_state->getValue('author');
        
        $abstract               = $form_state->getValue('abstract');
        $abstract['value']      = normalizeInlineImageLinks($abstract['value']);
        
        $chapter_text           = $form_state->getValue('chapter_text');
        $chapter_text['value']  = normalizeInlineImageLinks($chapter_text['value']);
        
        $errata                 = $form_state->getValue('errata');
        $errata['value']        = normalizeInlineImageLinks($errata['value']);
        
        $correction             = $form_state->getValue('correction');
        $correction['value']    = normalizeInlineImageLinks($correction['value']);
        
        $data = [
            
            'cp_abstract'               => base64_encode(json_encode($abstract)),
            'cp_chapter_text'           => base64_encode(json_encode($chapter_text)),
            'cp_errata'                 => base64_encode(json_encode($errata)),
            'cp_correction'             => base64_encode(json_encode($correction)),
            'cp_author_uid'             => $form_state->getValue('author'),
            'cp_license'                => $form_state->getValue('license'),
            'cp_publication_year'       => $form_state->getValue('publication_year'),
            'cp_publication_place'      => $form_state->getValue('publication_place'),
            'cp_publisher'              => $form_state->getValue('publisher'),
            'cp_doi'                    => $form_state->getValue('doi'),
            'cp_ddc'                    => $form_state->getValue('ddc'),
            'cp_corresponding_author'   => $form_state->getValue('corresponding_author'),
            'cp_urn'                    => $form_state->getValue('urn'),
            'cp_keywords'               => json_encode(preg_split('/[^a-zA-Z0-9]+/', $form_state->getValue('keywords'))),
            'cp_funding_number'         => $form_state->getValue('funding_number'),
            'cp_funding_id'             => $form_state->getValue('funding_id'),
            'cp_version'                => $form_state->getValue('version'),
            'cp_bkid'                   => $form_state->get('bk_id'),
            'cp_title'                  => $form_state->getValue('title')
        ];
        
        $cp_id = \Drupal::database()
            ->insert('rwPubgoldBookChapters')
            ->fields($data)
            ->execute();
        
        drupal_set_message(t('Chapter successfully saved!'));
        $form_state->setRedirect('publisso_gold.bookmanagement_book', ['bk_id' => $form_state->get('bk_id')]);
        
        return $form;
    }    
}
