<?php

/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldWorkflowEditorClearingRequest.
 */
namespace Drupal\publisso_gold\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldWorkflowEditorClearingRequest extends FormBase {
        private $modname = 'publisso_gold';
        private $database;
        private $modpath;
        private $num_corporations;
        public function __construct(Connection $database) {
                $this->database = $database;
        }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldWorkflowEditorClearingRequest|static
         */
        public static function create(ContainerInterface $container) {
                return new static ( $container->get ( 'database' ) );
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function getFormId() {
                return 'publisso_goldworkfloweditorclearingrequest';
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function buildForm(array $form, FormStateInterface $form_state) {
                $this->modpath = drupal_get_path ( 'module', $this->modname );
                
                if ($this->modpath && ! $form_state->get ( 'modpath' )) {
                        $form_state->set ( 'modpath', $this->modpath );
                }
                
                if (! $this->modpath && $form_state->get ( 'modpath' )) {
                        $this->modpath = $form_state->get ( 'modpath' );
                }
                
                $args = $form_state->getBuildInfo ();
                
                if (count ( $args ))
                        $wf_id = $args ['args'] [0] ['wf_id'];
                
                if (! $wf_id)
                        $wf_id = $form_state->get ( 'wf_id' );
                
                if ($wf_id) {
                        
                        $form_state->set ( 'wf_id', $wf_id );
                        
                        $workflow = getDashboard ( \Drupal::database (), $wf_id );
                        $workflow = $workflow [0];
                        $workflow_data = json_decode ( base64_decode ( $workflow->wf_data ) );
                        
                        $delta = [ 
                                'workflow',
                                'workflow_data' 
                        ];
                        foreach ( $delta as $_ )
                                $form_state->set ( $_, $$_ );
                        
                        $objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
                        
                        if ($objWorkflow->getDataElement ( 'cp_id' )) {
                                
                                $url = Url::fromRoute ( 'publisso_gold.export.chapter', [ 
                                        'cp_id' => $objWorkflow->getDataElement ( 'cp_id' ) 
                                ] );
                                
                                $form ['pdf'] = [ 
                                        '#type' => 'link',
                                        '#title' => ( string ) $this->t ( 'Download as PDF' ),
                                        '#url' => $url,
                                        '#prefix' => '<span class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;',
                                        '#suffix' => '<br><br>',
                                        '#attributes' => [ 
                                                'class' => [ 
                                                        'download-pdf' 
                                                ] 
                                        ] 
                                ];
                        }
                        
                        $form ['comment'] = [ 
                                
                                '#title' => ( string ) t ( 'Comment' ),
                                '#type' => 'text_format',
                                '#format' => 'full_html' 
                        ];
                        
                        switch ($workflow_data->state) {
                                
                                case 'editor clearing request' :
                                        
                                        $form ['comment'] = [ 
                                                '#title' => ( string ) t ( 'Comment' ),
                                                '#type' => 'textarea' 
                                        ];
                                        
                                        $form ['eic_action'] = [ 
                                                '#title' => ( string ) t ( 'choose action' ),
                                                '#type' => 'select',
                                                '#required' => true,
                                                '#options' => [ 
                                                        'editor_change_request' => ( string ) t ( 'Editor change request' ),
                                                        'cleared_for_publication' => ( string ) t ( 'Cleared for publication' ) 
                                                ] 
                                        ];
                                        
                                        break;
                        }
                        
                        $form ['preview'] = [ 
                                '#type' => 'link',
                                '#url' => \Drupal\Core\Url::fromRoute ( 'publisso_gold.workflow.preview', [ 
                                        'wf_id' => $form_state->get ( 'wf_id' ) 
                                ] ),
                                '#title' => ( string ) t ( 'Preview' ),
                                '#attributes' => [ 
                                        'class' => [ 
                                                'btn',
                                                'btn-warning' 
                                        ],
                                        'target' => [ 
                                                '_blank' 
                                        ] 
                                ],
                                '#suffix' => '&nbsp;&nbsp;&nbsp;' 
                        ];
                        
                        $form ['submit'] = [ 
                                '#type' => 'submit',
                                '#value' => ( string ) t ( 'Submit' ) 
                        ];
                }
                
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function validateForm(array &$form, FormStateInterface $form_state) {
                return $form;
        }
        
        /**
         *
         * {@inheritdoc}
         *
         */
        public function submitForm(array &$form, FormStateInterface $form_state) {
                $session = \Drupal::service ( 'session' );
                $objWorkflow = new \Drupal\publisso_gold\Controller\Workflow ( $form_state->get ( 'wf_id' ) );
                $objWorkflow->historize ();
                
                $eic_action = $form_state->getValue ( 'eic_action' );
                $delta = [ 
                        'workflow',
                        'workflow_data' 
                ];
                foreach ( $delta as $_ )
                        $$_ = $form_state->get ( $_ );
                
                switch ($eic_action) {
                        
                        case 'editor_change_request' :
                                $objWorkflow->setState ( 'editor change request' );
                                break;
                        
                        case 'cleared_for_publication' :
                                $objWorkflow->setState ( 'cleared for publication' );
                                break;
                }
                
                // save comment if submitted
                $comment = $form_state->getValue ( 'comment' );
                
                if ($comment) {
                        
                        $comment = base64_encode ( $comment );
                        
                        $data = [ 
                                'wfc_comment' => $comment,
                                'wfc_created_by_uid' => $session->get ( 'user' ) ['id'],
                                'wfc_wfid' => $objWorkflow->getElement ( 'id' ) 
                        ];
                        
                        \Drupal::database ()->insert ( 'rwPubgoldWorkflowComments' )->fields ( $data )->execute ();
                }
                
                // unlock data
                $objWorkflow->unlock ();
                
                $form_state->setRedirect ( 'publisso_gold.dashboard' );
                
                return $form;
        }
        
        /**
         * @param null $tmpl
         * @param null $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl = NULL, $vars = NULL) {
                if ($tmpl == NULL || $vars == NULL) {
                        // set Site-Vars
                        $this->tmpl = str_replace ( array_keys ( $this->tmpl_vars ), array_values ( $this->tmpl_vars ), $this->tmpl );
                        
                        // remove unused vars
                        $this->tmpl = preg_replace ( '(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl );
                } else {
                        // set Site-Vars
                        $tmpl = str_replace ( array_keys ( $vars ), array_values ( $vars ), $tmpl );
                        
                        // remove unused vars
                        return preg_replace ( '(::([a-zA-Z-_1-9]+)?::)', '', $tmpl );
                }
        }
}
