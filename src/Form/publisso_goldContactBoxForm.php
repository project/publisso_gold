<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldContactBoxForm.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;

/**
 * Provides a simple example form.
 */
class publisso_goldContactBoxForm extends FormBase {
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'publisso_goldcontactboxform';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $language = \Drupal::languageManager()->getCurrentLanguage();
    
    switch($language->getId()){
      
      case 'de':
        
        $form['topic'] = [
          '#type' => 'select',
          '#title' => '',
          '#options' => [
            '0'   => 'Bitte wählen',
            '205' => 'Allgemein',
            '196' => 'DOI-Service',
            '202' => 'Fachrepositorium Lebenswissenschaften',
            '195' => 'Forschungsdatenmanagement',
            '204' => 'GMS',
            '203' => 'Hochschulschriftenserver Medizin',
            '198' => 'Kongresse',
            '199' => 'LeibnizOpen',
            '200' => 'Living Handbooks',
            '194' => 'Open-Access-Publizieren und -Beraten / Leitung',
            '201' => 'Publikatiosberatung',
            '216' => 'PUBLISSO Plattform Gold',
            '197' => 'Zeitschriften / Artikel'
          ],
          '#attributes' => [
            'class' => 'select-positions',
            'id'    => 'select-positions-id',
            'name'  => 'select-abteilung-id'
          ]
        ];
        break;
      
      case 'en':
        
        $form['topic'] = [
          '#type' => 'select',
          '#title' => '',
          '#options' => [
            '0'   => 'Please choose',
            '198' => 'Conference Papers',
            '196' => 'DOI-Service',
            '194' => 'Head of Open Access Publishing and Advisory Services',
            '197' => 'Journals / Articles',
            '199' => 'LeibnitzOpen',
            '200' => 'Living Handbooks',
            '201' => 'Open Access Advisory Service',
            '195' => 'Research Data Management',
            '202' => 'Specialist Repository for Life Sciences',
            '203' => 'Theses and Dissertations'
          ],
          '#attributes' => [
            'class' => 'select-positions',
            'id'    => 'select-positions-id',
            'name'  => 'select-abteilung-id'
          ]
        ];
        break;
    }
    
    $form['topic']['#prefix'] = '
        <div id="contact_box" class="contact_box tx_dreipc_contact_contact_box_top">
        <div>
            <div>
                <form>
                    <h2>
                        Nehmen Sie Kontakt auf.
                    </h2>
                    <label class="hidden" for="select-abteilung-id">
                        Arbeitsbereich
                    </label>
                    <div id="topbox-positions-ajax-container" class="choose-position-container">
    ';
    
    $form['topic']['#suffix'] = '
    </div>
                    <br>
                </form>
                <span class="icon-cancel"></span>
            </div>
            <div id="topbox-contacts-ajax-container" class="show-contact-container"></div>
        </div>
        <div class="contact_tab">
            <a href="#" tabindex="1">
                <span class="icon-user"></span>
                PUBLISSO-Kontakt
                <span class="icon-arrow-down"></span>
            </a>
        </div>
    </div>
    ';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }
}





