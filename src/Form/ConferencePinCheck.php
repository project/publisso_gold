<?php
        
        
        namespace Drupal\publisso_gold\Form;
        
        
        use Drupal\Core\Form\FormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Drupal\publisso_gold\Controller\Conference;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class ConferencePinCheck
         * @package Drupal\publisso_gold\Form
         */
        class ConferencePinCheck extends FormBase {
        
                /**
                 * @return string
                 */
                public function getFormId () {
                        return 'conference-pin-check';
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @param array $args
                 * @return array
                 */
                public function buildForm (array $form, FormStateInterface $form_state, $args = [] ) {
                        
                        if(!$form_state->has('cf_id')) $form_state->set('cf_id', $args['cf_id']);
                        
                        $form['pin'] = [
                                '#type' => 'textfield',
                                '#title' => (string)$this->t('Please enter the PIN or pass-phrase to make a submission:')
                        ];
                        
                        $form['submit'] = [
                                '#type' => 'submit',
                                '#value' => (string)$this->t('Submit'),
                                '#button_type' => 'success'
                        ];
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 * @return array|void
                 */
                public function validateForm (array &$form, FormStateInterface $form_state ) {
        
                        $conference = ConferenceManager::getConference($form_state->get('cf_id'));
                        
                        if($conference->getElement('submission_pin') != $form_state->getValue('pin')){
                                $form_state->setError($form['pin'], (string)$this->t('Your entry is incorrect!'));
                        }
                        
                        return $form;
                }
        
                /**
                 * @param array $form
                 * @param FormStateInterface $form_state
                 */
                public function submitForm (array &$form, FormStateInterface $form_state ) {
                        
                        $cntr = new Conference();
                        $wf_id = $cntr->initSubmission($form_state->get('cf_id'), true);
                        $form_state->setRedirect( 'publisso_gold.workflow.item', [ 'wf_id' => $wf_id] );
                }
        }