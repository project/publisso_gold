<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldReviewBookchapter.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldReviewBookchapter extends FormBase {
    
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    private $num_corporations;
    
    public function __construct(Connection $database){
        $this->database = $database;
    }
        
        /**
         * @param ContainerInterface $container
         * @return publisso_goldReviewBookchapter|static
         */
        public static function create(ContainerInterface $container){
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldreviewbookchapter';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        
        $args = $form_state->getBuildInfo();
        
        if(!count($args)){
            
            return [
                '#cache' => [
                    'max-age' => 0
                ],
                
                'error' => [
                    '#type' => 'markup',
                    '#markup' => '<span class="rwAlert">'.t('No review-object given!').'</span>'
                ]
            ];
        }
        
        //prepare data
        foreach($args['args'][0] as $k => $v)
            $$k = $v;
        
        $form_state->set('wf_id', $workflow->wf_id);
        $workflow_item = json_decode(base64_decode($workflow->wf_data));
        
        foreach(['abstract', 'chapter_text'] as $k)
            $workflow_item->$k = json_decode(base64_decode($workflow_item->$k));
        
        //get workflow cache
        
        $wf_cache = \Drupal::database()
            ->select('rwPubgoldWorkflowCache', 'wc')
            ->fields('wc', [])
            ->condition('wc_uid', $_SESSION['user']['id'])
            ->condition('wc_wfid', $workflow->wf_id)
            ->execute()
            ->fetchAssoc();
        
        if(count($wf_cache)){
            $wf_cache['wc_data'] = json_decode(base64_decode($wf_cache['wc_data']));
        }
        
        //generate output
        
        /**
         * if changes allowed in origtext, generate textfield,
         * else generate markup-html and append emty textfield
         */
        
        if($book['bk_allow_review_orig_change'] == 1){
            
            $form['orig_text'] = [
                '#type' => 'text_format',
                '#format' => isset($wf_cache['wc_data']->orig_text->format) ? $wf_cache['wc_data']->orig_text->format : $workflow_item->chapter_text->format,
                '#default_value' => isset($wf_cache['wc_data']->orig_text->value) ? $wf_cache['wc_data']->orig_text->value : $workflow_item->chapter_text->value,
                '#title' => t('original text from author')
            ];
        }
        else{
            
            $form['orig_text'] = [
                '#type' => 'details',
                '#open' => false,
                'content' => [
                    '#type' => 'markup',
                    '#markup' => $workflow_item->chapter_text->value
                ],
                '#title' => t('original text from author'),
                '#description' => t('
                    This is the original Version of the author. The direct editing is prohibited editorial.
                '),
            ];
        }
        
        $form['back'] = [
            '#type' => 'submit',
            '#value' => t('back'),
            '#submit' => ['::back']
        ];
        
        $form['save'] = [
            '#type' => 'submit',
            '#value' => t('save')
        ];
        
        $form['close'] = [
            '#type' => 'submit',
            '#value' => t('save and finish revision'),
            '#submit' => ['::finish_revision'],
            '#validate' => ['::validateFinish'],
            '#attributes' => array('onclick' => 'if(!confirm("'.t('This complete your revision. This action can not be undone!\n\nOK: Finish revision\nCancel: Continue revision').'")){return false;}')
        ];
        
        $form['#cache'] = [
            'max-age' => 0
        ];
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function validateFinish(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $data = [
            ':wc_data' => base64_encode(json_encode($form_state->getValues())),
            ':wc_uid' => $_SESSION['user']['id'],
            ':wc_wfid' => $form_state->get('wf_id')
        ];
        
        if($form_state->hasValue('orig_text')){
            
            $workflow = getDashboard(\Drupal::database(), $form_state->get('wf_id'));
            $workflow = $workflow[0];
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            $chapter_text = json_decode(base64_decode($workflow_data->chapter_text));
        }
        
        $sql = "REPLACE INTO {rwPubgoldWorkflowCache} (`wc_data`, `wc_uid`, `wc_wfid`) VALUES (:wc_data, :wc_uid, :wc_wfid)";
        
        \Drupal::database()
            ->query($sql, $data);
        
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function finish_revision(array &$form, FormStateInterface $form_state) {
        
        //create workflow-history
        $workflow = getDashboard(\Drupal::database(), $form_state->get('wf_id'));
        $workflow = $workflow[0];
        $workflow_data = json_decode(base64_decode($workflow->wf_data));
        
        if($form_state->hasValue('comments')){ //if only comments are allowed
            $comments = $form_state->getValue('comments');
        
            $comment = array(
                'wfc_created_by_uid' => $_SESSION['user']['id'],
                'wfc_comment' => base64_encode(json_encode($comments)),
                'wfc_wfid' => $form_state->get('wf_id')
            );
            
            \Drupal::database()
                ->insert('rwPubgoldWorkflowComments')
                ->fields($comment)
                ->execute();
        }
        elseif($form_state->hasValue('orig_text')){ //if changes in orig text
            
            if(!isset($workflow_data->history)){
                $workflow_data->history = [];
            }
            
            $orig_text = $form_state->getValue('orig_text');
            
            //save current workflow in history
            $workflow_history = array();
            $workflow_history['wfh_created_by_uid'] = $_SESSION['user']['id'];
            
            foreach($workflow as $k => $v){
                
                if($k == 'wf_id'){
                    $key = 'wfh_wfid';
                }
                else{
                    $key = explode('_', $k, 2);
                    $key = $key[0].'h_'.$key[1];
                }
                
                $workflow_history[$key] = $v;
            }
            
            \Drupal::database()->insert('rwPubgoldWorkflowHistory')->fields($workflow_history)->execute();
            
            //change orig text
            $workflow_data->chapter_text = base64_encode(json_encode($orig_text));
        }
        
        //remove revisor from assignment
        $assigned_to = explode(',', $workflow->wf_assigned_to);
        unset($assigned_to[array_search('u:'.$_SESSION['user']['id'], $assigned_to)]);
        //set new assign-list
        $workflow->wf_assigned_to = implode(',', $assigned_to);
        //set new workflow data
        $workflow->wf_data = base64_encode(json_encode($workflow_data));
        
        //set reviewer edited (append to list)
        $reviewers = [$_SESSION['user']['id']];
        
        if($workflow->wf_reviewer_edited != ''){
            $reviewers = array_merge($reviewers, explode(',', $workflow->wf_reviewer_edited));
        }
        
        $workflow->wf_reviewer_edited = implode(',', $reviewers);
        
        if($workflow->wf_assigned_to == ''){ //all reviewers finished -> set new status and give back to editor
            
            //update workflow data
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            $workflow_data->state = 'review finished';
            $workflow->wf_data = base64_encode(json_encode($workflow_data));
            $workflow->wf_assigned_to = 'u:'.$workflow->wf_editor_last_edited;
        }
        
        //save workflow
        unset($workflow->wf_id);
        \Drupal::database()->update('rwPubgoldWorkflow')->fields((array)$workflow)->condition('wf_id', $form_state->get('wf_id'), '=')->execute();
        
        //delete my workflow-cache
        \Drupal::database()->delete('rwPubgoldWorkflowCache')
                           ->condition('wc_uid', $_SESSION['user']['id'], '=')
                           ->condition('wc_wfid', $form_state->get('wf_id'), '=')
                           ->execute();
        
        //unlock workflow
        $sql = "
            update 
                rwpg_rwPubgoldWorkflow 
            set
                wf_locked = 0,
                wf_locked_by_uid = NULL
            where 
                wf_id = :wf_id
        ";
        
        $data = [
            ':wf_id' => $form_state->get('wf_id')
        ];
        
        \Drupal::database()->query($sql, $data);
        
        $form_state->setRedirect('publisso_gold.dashboard');
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         * @throws \Exception
         */
        public function back(array &$form, FormStateInterface $form_state) {
        
        //save orig-text 
        if($form_state->hasValue('orig_text')){
            
            //get workflow for history
            $workflow = getDashboard(\Drupal::database(), $form_state->get('wf_id'));
            $workflow = $workflow[0];
            
            //prepare workflow for history
            $workflow_history = array();
            $workflow_history['wfh_created_by_uid'] = $_SESSION['user']['id'];
            
            foreach($workflow as $k => $v){
                
                if($k == 'wf_id'){
                    $key = 'wfh_wfid';
                }
                else{
                    
                    $key = explode("_", $k, 2);
                    $key = $key[0].'h_'.$key[1];
                    
                }
                
                $workflow_history[$key] = $v;
            }
            
            \Drupal::database()->insert('rwPubgoldWorkflowHistory')->fields($workflow_history)->execute();
            
            //update orig-text
            $workflow_data = json_decode(base64_decode($workflow->wf_data));
            $workflow_data->chapter_text = base64_encode(json_encode($form_state->getValue('orig_text')));
            
            \Drupal::database()
                ->update('rwPubgoldWorkflow')
                ->fields(['wf_data' => base64_encode(json_encode($workflow_data))])
                ->condition('wf_id', $form_state->get('wf_id'), '=')
                ->execute();
        }
        
        //unlock workflow
        $sql = "
            update 
                rwpg_rwPubgoldWorkflow 
            set
                wf_locked = 0,
                wf_locked_by_uid = NULL
            where 
                wf_id = :wf_id
        ";
        
        $data = [
            ':wf_id' => $form_state->get('wf_id')
        ];
        
        \Drupal::database()->query($sql, $data);
        
        $form_state->setRedirect('publisso_gold.dashboard');
        return $form;
    }
        
        /**
         * @param null $tmpl
         * @param null $vars
         * @return string|string[]|null
         */
        private function renderVars($tmpl = NULL, $vars = NULL){
        
        if($tmpl == NULL || $vars == NULL){
            //set Site-Vars
            $this->tmpl = str_replace(array_keys($this->tmpl_vars), array_values($this->tmpl_vars), $this->tmpl);
            
            //remove unused vars
            $this->tmpl = preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $this->tmpl);
        }
        else{
            //set Site-Vars
            $tmpl = str_replace(array_keys($vars), array_values($vars), $tmpl);
            
            //remove unused vars
            return preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $tmpl);
        }
    }
}
