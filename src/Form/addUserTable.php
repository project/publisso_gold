<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\addUserTable.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;
use Drupal\file\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File;
use Drupal\publisso_gold\Controller\UserTable;

/**
 * Class addUserTable
 * @package Drupal\publisso_gold\Form
 */
class addUserTable extends FormBase {
        
    private $modname = 'publisso_gold';
    private $database;
    private $modpath;
    
    public function __construct(){
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'addUserTable';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
        
        $session = \Drupal::service('session');
        
        if(!$form_state->has('colNr')) $form_state->set('colNr', 1);
        if(!$form_state->has('rowNr')) $form_state->set('rowNr', 2);
        
        $form['mu_headline'] = [
                '#markup' => (string)t('Create new table'),
                '#prefix' => '<h3>',
                '#suffix' => '</h3>'
        ];
        
        $form['name'] = [
                '#title' => (string)t('Tablename'),
                '#type'  => 'textfield',
                '#required' => 'true'
        ];
        
        $form['mu_hint'] = [
                '#markup' => 'The first row will be used as column-header(s). Empty rows will be ignored.',
                '#prefix' => '<hr>',
                '#suffix' => '<hr>'
        ];
        
        $form['removeCol'] = [
                '#type' => 'submit',
                '#submit' => ['::removeCol'],
                '#value' => (string)t('Remove last column'),
                '#ajax' => [
                        'wrapper' => 'table-wrapper',
                        'callback' => '::removeColCallback',
                        'progress' => [
                                'type' => NULL,
                                'message' => NULL,
                        ]
                ],
                '#suffix' => '&nbsp;&nbsp;&nbsp;',
                 '#limit_validation_errors' => []
        ];
        
        $form['addCol'] = [
                '#type' => 'submit',
                '#submit' => ['::addCol'],
                '#value' => (string)t('Add column'),
                '#ajax' => [
                        'wrapper' => 'table-wrapper',
                        'callback' => '::addColCallback',
                        'progress' => [
                                'type' => NULL,
                                'message' => NULL,
                        ]
                ],
                '#suffix' => '<br><br>',
                 '#limit_validation_errors' => []
        ];
        
        $form['table-wrapper'] = [
                '#type' => 'container',
                '#prefix' => '<div id="table-wrapper">',
                '#suffix' => '</div>'
        ];
        
        $form['table-wrapper']['table'] = [
                '#type' => 'table',
        ];
        
        for($i = 0; $i < $form_state->get('rowNr'); $i++){
                
                for($ii = 0; $ii < $form_state->get('colNr'); $ii++){
                        
                        $form['table-wrapper']['table'][$i][$ii] = [
                                '#type' => $i == 0 ? 'textfield' : 'textarea',
                                '#placeholder' => "",
                                '#required' => $i == 0,
                                '#rows' => 1
                        ];
                }
        }
        
        $form['removeRow'] = [
                '#type' => 'submit',
                '#submit' => ['::removeRow'],
                '#value' => (string)t('Remove last row'),
                '#ajax' => [
                        'wrapper' => 'table-wrapper',
                        'callback' => '::removeRowCallback',
                        'progress' => [
                                'type' => NULL,
                                'message' => NULL,
                        ]
                ],
                '#suffix' => '&nbsp;&nbsp;&nbsp;',
                '#limit_validation_errors' => []
        ];
        
        $form['addRow'] = [
                '#type' => 'submit',
                '#submit' => ['::addRow'],
                '#value' => (string)t('Add row'),
                '#ajax' => [
                        'wrapper' => 'table-wrapper',
                        'callback' => '::addRowCallback',
                        'progress' => [
                                'type' => NULL,
                                'message' => NULL,
                        ]
                ],
                '#suffix' => '<br><br>',
                '#limit_validation_errors' => []
        ];
        
        $form['submit'] = [
                '#type' => 'submit',
                '#value' => (string)t('Save'),
                '#prefix' => '<hr>',
                '#suffix' => '<br><br>'
        ];
        
        $form['#cache']['max-age'] = 0;
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        $userTable = new UserTable();
        //$userTable->loadTables();
        
        if($userTable->tableExists($form_state->getValue('name'))){
                $form_state->setErrorByName('name', (string)t('A table with name "@name" already exists!', ['@name' => $form_state->getValue('name')]));
        }
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
        $node = '
        <object name="::name::">
                <meta>
                        <maxversion>1</maxversion>
                        <author>
                                <firstname>::author.firstname::</firstname>
                                <lastname>::author.lastname::</lastname>
                        </author>
                        <created>
                                ::created::
                        </created>
                        <modified>
                                ::modified::
                        </modified>
                </meta>
                <headers>
                        ::headers::
                </headers>
                <datas>
                        <data version="1" timestamp="::timestamp::">
                                ::rows::
                        </data>
                </datas>
        </object>
        ';

        $tableData      = $form_state->getValue('table');
        $tableName      = $form_state->getValue('name');
        $tableHeaders   = array_shift($tableData);
        $session        = \Drupal::service('session');
        $author         = new \Drupal\publisso_gold\Controller\User($session->get('user')['id']);
        
        $headerMarkup = '';
        
        foreach($tableHeaders as $weight => $column){
                $headerMarkup .= '
                        <header weight="'.$weight.'" id="'.md5($column).'">
                                <title>'.$column.'</title>
                        </header>
                ';
        }
        
        $dataMarkup = '';
        
        foreach($tableData as $weight => $row){
                
                $tmpl = '
                        <row weight="'.$weight.'">
                ';
                
                $allEmpty = true;
                
                foreach($row as $id => $cell){
                        
                        if(!empty($cell)) $allEmpty = false;
                        $tmpl .= '
                              <cell for="'.md5($tableHeaders[$id]).'">'.trim($cell).'</cell>  
                        ';
                }
                
                $tmpl .= '
                        </row>
                ';
                
                if(!$allEmpty) $dataMarkup .= $tmpl;
        }
        
        $node = str_replace('::headers::', $headerMarkup, $node);
        $node = str_replace('::rows::', $dataMarkup, $node);
        $node = str_replace('::name::', $tableName, $node);
        
        if($author){
                $node = str_replace('::author.firstname::', $author->profile->getElement('firstname'), $node);
                $node = str_replace('::author.lastname::', $author->profile->getElement('lastname'), $node);
        }
        
        $node = str_replace('::created::', date('Y-m-d H:i:s'), $node);
        $node = str_replace('::modified::', date('Y-m-d H:i:s'), $node);
        $node = str_replace('::timestamp::', time(), $node);
        
        $userTable = new UserTable();
        $userTable->addTable($node);
        
        $form_state->setRedirect('publisso_gold.user.table');
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function addCol(array &$form, FormStateInterface $form_state){
        $colNr = $form_state->get('colNr');
        $colNr += 1;
        $form_state->set('colNr', $colNr);
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addColCallback(array &$form, FormStateInterface $form_state){
        return $form['table-wrapper'];
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function removeCol(array &$form, FormStateInterface $form_state){
        $colNr = $form_state->get('colNr');
        $colNr -= 1;
        $form_state->set('colNr', $colNr < 1 ? 1 : $colNr);
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function removeColCallback(array &$form, FormStateInterface $form_state){
        return $form['table-wrapper'];
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function addRow(array &$form, FormStateInterface $form_state){
        $rowNr = $form_state->get('rowNr');
        $rowNr += 1;
        $form_state->set('rowNr', $rowNr);
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function addRowCallback(array &$form, FormStateInterface $form_state){
        return $form['table-wrapper'];
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function removeRow(array &$form, FormStateInterface $form_state){
        $rowNr = $form_state->get('rowNr');
        $rowNr -= 1;
        $form_state->set('rowNr', $rowNr < 2 ? 2 : $rowNr);
        $form_state->setRebuild();
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return mixed
         */
        public function removeRowCallback(array &$form, FormStateInterface $form_state){
        return $form['table-wrapper'];
    }
}
