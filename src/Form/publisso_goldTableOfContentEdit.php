<?php
/**
 * @file
 * Contains \Drupal\publisso_gold\Form\publisso_goldTableOfContentEdit.
 */
namespace Drupal\publisso_gold\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple example form.
 */
class publisso_goldTableOfContentEdit extends FormBase {
    
    private $modname = 'publisso_gold';
    private $modpath;
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'publisso_goldtableofcontentedit';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        if($this->modname && !$form_state->get('modname')){
            $form_state->set('modname', $this->modname);
        }
        
        if(!$this->modname && $form_state->get('modname')){
            $this->modname = $form_state->get('modname');
        }
        
        $this->modpath = drupal_get_path('module', $this->modname);
        
        if($this->modpath && !$form_state->get('modpath')){
            $form_state->set('modpath', $this->modpath);
        }
        
        if(!$this->modpath && $form_state->get('modpath')){
            $this->modpath = $form_state->get('modpath');
        }
        
        require_once($this->modpath.'/inc/publisso_gold.lib.inc.php');
        $args = $form_state->getBuildInfo();
        
        $toc = new \Drupal\publisso_gold\Controller\TableOfContent($args['args'][0]['toc_id']);
        
        if(!$form_state->get('toc'))
            $form_state->set('toc', $toc);
        
        $form['title'] = [
            '#type' => 'markup',
            '#markup' => '<h2>'.((string)t('Edit table of content')).'</h2>'
        ];
        
        /**
         * Display choosen table of content
         */
        
        $form['toc']['info'] = [
            '#type' => 'markup',
            '#markup' => ''
        ];
        
        if($toc->getElement('medium_id')){
            
            $medium = $this->getAssignedMedium($toc->getElement('medium'), $toc->getElement('medium_id'));
            
            $markup = '
                <cite>'.((string)t('Table of content is assigned to:')).'</cite>
                <p><h4>'.(ucfirst($toc->getElement('medium'))).': '.($medium->getElement('title')).'</h4></p>
            ';
            
            $form['toc']['unassign'] = [
                '#suffix' => '<hr>',
                '#type' => 'submit',
                '#value' => (string)t('unassign'),
                '#submit' => ['::unassign']
            ];
            
            $form['toc']['info']['#markup'] = $markup;
        }
        else{
            
            $form['toc']['info']['#markup'] = (string)t('This table of content is currently not assigned');
            $form['toc']['assign'] = [
                '#type' => 'select',
                '#title' => (string)t('Assign table of content to:'),
                '#options' => $this->getAvailableMedia(),
                '#size' => 10
            ];
            
            $form['toc']['submit'] = [
                '#suffix' => '<hr>',
                '#type' => 'submit',
                '#value' => (string)t('assign'),
                '#submit' => ['::assign']
            ];
        }
        
        $form['toc']['title'] = [
            '#type' => 'markup',
            '#markup' => '<h3>'.$toc->getElement('title').'</h3>'
        ];
        
        $form['toc']['description'] = [
            '#type' => 'markup',
            '#markup' => $toc->getElement('description'),
            '#suffix' => '<br><br>'
        ];
        
        $form['toc']['add'] = [
            '#type' => 'markup',
            '#markup' => '<a href="/publisso_gold/toc/'.$toc->getElement('id').'/item/0/add/">'.((string)t('add entry')).'</a>',
            '#suffix' => '<br><br>'
        ];
        
        if(count($toc->readItems())){
            
            $markup = $this->displayStructure($toc->readStructure());
            
            $form['structure'] = [
                '#type' => 'markup',
                '#markup' => $markup
            ];
        }
        
        $form['#cache']['max-age'] = 0;
        $form_state->setCached(false);
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        
    
        return $form;
    }
        
        /**
         * @return array
         */
        private function getAvailableMedia(){
        
        $available_media = [];
            
        //getting books
        $assigned_media = [];
        
        $result = \Drupal::database()
            ->select('rwPubgoldTableOfContent', 't')
            ->fields('t', ['toc_medium_id'])
            ->condition('toc_medium', 'book', '=')
            ->condition('toc_medium_id', '0', '>')
            ->execute()
            ->fetchAll();
        
        foreach($result as $_){
            $assigned_media[] = $_->toc_medium_id;
        }
            
        $result = \Drupal::database()->select('rwPubgoldBooks', 'b')->fields('b', []);
        
        if(count($assigned_media))
            $result->condition('bk_id', $assigned_media, 'NOT IN');
        
        $result = $result->execute()->fetchAll();
        
        foreach($result as $_){
            $available_media['books']['b:'.$_->bk_id] = $_->bk_title;
        }
        
        //getting journals
        $assigned_media = [];
        
        $result = \Drupal::database()
            ->select('rwPubgoldTableOfContent', 't')
            ->fields('t', ['toc_medium_id'])
            ->condition('toc_medium', 'journal', '=')
            ->condition('toc_medium_id', '0', '>')
            ->execute()
            ->fetchAll();
        
        foreach($result as $_){
            $assigned_media[] = $_->toc_medium_id;
        }
            
        $result = \Drupal::database()->select('rwPubgoldJournals', 'b')->fields('b', []);
        
        if(count($assigned_media))
            $result->condition('jrn_id', $assigned_media, 'NOT IN');
        
        $result = $result->execute()->fetchAll();
        
        foreach($result as $_){
            $available_media['journal']['j:'.$_->jrn_id] = $_->jrn_title;
        }
        
        //getting conferences
        $assigned_media = [];
        
        $result = \Drupal::database()
            ->select('rwPubgoldTableOfContent', 't')
            ->fields('t', ['toc_medium_id'])
            ->condition('toc_medium', 'conference', '=')
            ->condition('toc_medium_id', '0', '>')
            ->execute()
            ->fetchAll();
        
        foreach($result as $_){
            $assigned_media[] = $_->toc_medium_id;
        }
            
        $result = \Drupal::database()->select('rwPubgoldConferences', 'b')->fields('b', []);
        
        if(count($assigned_media))
            $result->condition('cf_id', $assigned_media, 'NOT IN');
        
        $result = $result->execute()->fetchAll();
        
        foreach($result as $_){
            $available_media['conference']['c:'.$_->cf_id] = $_->cf_title;
        }
        
        return $available_media;
    }
        
        /**
         * @param $type
         * @param $id
         * @return \Drupal\publisso_gold\Controller\Book|\Drupal\publisso_gold\Controller\Conference|\Drupal\publisso_gold\Controller\Journal
         */
        private function getAssignedMedium($type, $id){
        
        switch($type){
            
            case 'book':
                return new \Drupal\publisso_gold\Controller\Book($id);
                break;
            
            case 'journal':
                return new \Drupal\publisso_gold\Controller\Journal($id);
                break;
            
            case 'conference':
                return new \Drupal\publisso_gold\Controller\Conference($id);
                break;
        }
    }
        
        /**
         * @param $structure
         * @return string
         */
        private function displayStructure($structure){
        
        $markup = '';
        
        if(count($structure) > 0){
            
            $markup = '<ul class="rwToc">';
            
            foreach($structure as $item){
                
                
                
                $markup .= '<li>';
                $markup  .= $item['item']->getElement('title').' ('.$item['item']->getElement('weight').')'
                         .' [
                            <a href="/publisso_gold/toc/item/edit/'.$item['item']->getElement('id').'">edit</a> | 
                            <a href="/publisso_gold/toc/'.$item['item']->getElement('tocid').'/item/'.$item['item']->getElement('id').'/add">'.((string)t('add sub-entry')).'</a> | 
                            '.(!count($item['childs']) ? '<a href="/publisso_gold/toc/item/'.$item['item']->getElement('id').'/delete">delete</a>' : (string)t('delete')).']<br>'
                         .'<cite>'.$item['item']->getElement('description').'</cite><br>'
                         .$item['item']->getElement('authors').'<br><br>';
                $markup .= $this->displayStructure($item['childs']);
                $markup .= '</li>';
            }
            
            $markup .= '</ul>';
        }
        
        return $markup;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function unassign(array &$form, FormStateInterface $form_state){
        
        $form_state->get('toc')->setElement('medium', null);
        $form_state->get('toc')->setElement('medium_id', null);
        return $form;
    }
        
        /**
         * @param array $form
         * @param FormStateInterface $form_state
         * @return array
         */
        public function assign(array &$form, FormStateInterface $form_state){
        
        $assign = explode(':', $form_state->getValue('assign'));
        
        switch($assign[0]){
            
            case 'b':
                $form_state->get('toc')->setElement('medium', 'book');
                $form_state->get('toc')->setElement('medium_id', $assign[1]);
                break;
            
            case 'j':
                $form_state->get('toc')->setElement('medium', 'journal');
                $form_state->get('toc')->setElement('medium_id', $assign[1]);
                break;
            case 'c':
                break;
        }
        
        return $form;
    }
}
