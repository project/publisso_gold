<?php
        
        
        namespace Drupal\publisso_gold\Classes;
        
        use Drupal as D;
        use Drupal\publisso_gold\Controller\Publisso as P;

        /**
         * Class SubmissionFunding
         * @package Drupal\publisso_gold\Classes
         */
        class SubmissionFunding {
                
                private $submission_type;
                private $submission_id;
                private $number;
                private $elements;
                private $db;
                private $messenger;
                private $tm;
                private $funder;
                public  $loaded;
        
                /**
                 * SubmissionFunding constructor.
                 * @param int $mediumID
                 * @param string $mediumType
                 * @param int $fundingNumber
                 */
                public function __construct(int $mediumID, string $mediumType, int $fundingNumber) {
                        
                        $this->submission_type = $mediumType;
                        $this->submission_id   = $mediumID;
                        $this->number          = $fundingNumber;
                        $this->db              = D::database();
                        $this->messenger       = D::messenger();
                        $this->tm              = D::translation();
                        $this->elements        = [];
                        $this->funder          = null;
                        
                        $this->loaded = $this->load();
                }
        
                /**
                 * @return bool
                 */
                private function load() {
                        
                        $error = null;
                        
                        try {
                                $this->elements = $this->db->select('rwPubgoldSubmissionFunding', 't')->fields('t', [])->condition('submission_id', $this->submission_id, '=')->condition('submission_type', $this->submission_type, '=')->condition('number', $this->number, '=')->execute()->fetchAssoc();
                                
                                if ($this->elements['funder']) $this->funder = D\publisso_gold\Controller\Manager\FunderManager::getFunder($this->elements['funder']);
                        }
                        catch (\Exception $e) {
                                $error = $e;
                        }
                        catch (\Error $e) {
                                $error = $e;
                        }
                        finally {
                                
                                if ($error) {
                                        D\publisso_gold\Controller\Publisso::log('Error in file ' . $error->getFile() . ' line ' . $error->getLine() . ': ' . $error->getMessage());
                                        $this->messenger->addError((string)$this->tm->translate('Cant load submission-funding #@id. See error-log for details.', ['@id' => $this->number]));
                                        return false;
                                }
                                
                                return true;
                        }
                }
        
                /**
                 * @return mixed|null
                 */
                public function getId() {
                        return $this->elements['id'] ?? null;
                }
                
                public function getElement(string $element): ?string {
                        return $this->elements[$element] ?? null;
                }
        
                /**
                 * @param string $element
                 * @param $value
                 */
                public function setElement(string $element, $value) {
                        if (!is_scalar($value)) $value = serialize($value);
                        $this->elements[$element] = $value;
                }
        
                /**
                 * @return Funder
                 */
                public function funder(): Funder {
                        return $this->funder;
                }
        
                /**
                 * @return bool
                 */
                public function save() {
                        
                        $error = null;
                        
                        try {
                                $this->db->update('rwPubgoldSubmissionFunding')->fields($this->elements)->condition('submission_id', $this->submission_id, '=')->condition('submission_type', $this->submission_type, '=')->condition('number', $this->number, '=')->execute();
                        }
                        catch (\Exception $e) {
                                $error = $e;
                        }
                        catch (\Error $e) {
                                $error = $e;
                        }
                        finally {
                                
                                if ($error) {
                                        P::log('Error in file ' . $error->getFile() . ' line ' . $error->getLine() . ': ' . $error->getMessage());
                                        $this->messenger->addError((string)$this->tm->translate('Cant store submission-funding #@id. See error-log for details.', ['@id' => $this->number]));
                                        return false;
                                }
                                
                                return true;
                        }
                }
        }