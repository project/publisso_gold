<?php
        
        namespace Drupal\publisso_gold\Classes\Ajax;
        
        use Drupal\Core\Ajax\CommandInterface;

        /**
         * Class OpenUrlCommand
         * @package Drupal\publisso_gold\Classes\Ajax
         */
        class OpenUrlCommand implements CommandInterface {
                
                private $selector;
                private $url;
                private $target;
                
                /**
                 * OpenUrlCommand constructor.
                 * @param $selector
                 * @param $url
                 * @param string $target
                 */
                public function __construct($selector, $url, string $target = '_self') {
                        
                        $this->selector = $selector;
                        $this->url      = $url;
                        $this->target   = $target;
                }
                
                /**
                 * @inheritDoc
                 */
                public function render() {
                        return ['command' => 'OpenUrlCommand', 'selector' => $this->selector, 'args' => ['url' => $this->url, 'target' => $this->target]];
                }
        }