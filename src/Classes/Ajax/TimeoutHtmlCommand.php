<?php

        namespace Drupal\publisso_gold\Classes\Ajax;

        use \Drupal\Core\Ajax\CommandInterface;

        /**
         * Class TimeoutHtmlCommand
         * @package Drupal\publisso_gold\Classes\Ajax
         */
        class TimeoutHtmlCommand implements CommandInterface {

                private $selector;
                private $timeout;
                private $html;

                /**
                 * TimeoutHtmlCommand constructor.
                 * @param $selector
                 * @param $timeout
                 * @param string $html
                 */
                public function __construct($selector, $timeout, string $html = '') {

                        $this->selector = $selector;
                        $this->timeout  = $timeout;
                        $this->html     = $html;
                }

                /**
                 * @inheritDoc
                 */
                public function render() {
                        return ['command' => 'TimeoutHtmlCommand', 'selector' => $this->selector, 'args' => ['timeout' => $this->timeout, 'html' => $this->html]];
                }
        }
