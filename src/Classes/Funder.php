<?php
        
        
        namespace Drupal\publisso_gold\Classes;
        use Drupal as D;

        /**
         * Class Funder
         * @package Drupal\publisso_gold\Classes
         */
        class Funder {
                
                private $elements;
                private $id;
                private $db;
        
                /**
                 * Funder constructor.
                 * @param int $id
                 */
                public function __construct(int $id){
                        
                        $this->id = $id;
                        $this->db = D::database();
                        $this->elements = [];
                        $this->load();
                }
                
                private function load(){
                        $this->elements = $this->db->select('rwPubgoldFunders', 't')->fields('t', [])->condition('id', $this->id, '=')->execute()->fetchAssoc();
                }
        
                /**
                 * @return mixed|null
                 */
                public function getId(){
                        return $this->elements['id'] ?? null;
                }
        
                /**
                 * @return mixed|null
                 */
                public function getAbbr(){
                        return $this->elements['abbr'] ?? null;
                }
        
                /**
                 * @return mixed|null
                 */
                public function getName(){
                        return $this->elements['name'] ?? null;
                }
        
                /**
                 * @return mixed|null
                 */
                public function getCountry(){
                        return $this->elements['country'] ?? null;
                }
        }