<?php
        
        
        namespace Drupal\publisso_gold\Classes;
        
        
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Blob;
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class AuthorsContract
         * @package Drupal\publisso_gold\Classes
         */
        class AuthorsContract {
                
                private $name;
                private $blob_id;
                private $blob;
                private $description;
                private $upload_date;
        
                /**
                 * AuthorsContract constructor.
                 * @param string $name
                 */
                public function __construct(string $name) {
                        $this->init($name);
                }
        
                /**
                 * @param $name
                 * @return bool
                 */
                public function init($name) {
                        
                        $qry = \Drupal::database()->select('rwPubgoldAuthorsContracts', 't');
                        $qry->fields('t', ['name', 'blob_id', 'description', 'upload_date']);
                        $qry->condition('name', $name, '=');
                        
                        if (!$qry->countQuery()->execute()->fetchField()) {
                                return false;
                        };
                        
                        $res = $qry->execute()->fetchAssoc();
                        
                        $this->name        = $res['name'];
                        $this->blob_id     = $res['blob_id'];
                        $this->description = $res['description'];
                        $this->upload_date = $res['upload_date'];
                        
                        $this->blob = new Blob($this->blob_id);
                        return true;
                }
        
                /**
                 * @param int $id
                 */
                public function setBlobID(int $id) {
                        \Drupal::database()->update('rwPubgoldAuthorsContracts')->fields(['blob_id' => $id])->condition('name', $this->name, '=')->execute();
                }
        
                /**
                 * @param string $name
                 * @param int $blobID
                 * @param string $description
                 * @return AuthorsContract|false
                 */
                public static function create(string $name, int $blobID, string $description = '') {
                        
                        try {
                                \Drupal::database()->insert('rwPubgoldAuthorsContracts')->fields(['name' => $name, 'description' => $description ?? null, 'upload_date' => date('Y-m-d'), 'blob_id' => $blobID])->execute();
                                
                                return new self($name);
                        }
                        catch (\Exception $e) {
                                Publisso::tools()->logMsg('Error in ' . __FILE__ . ' in line ' . __LINE__ . ': ' . $e->getMessage());
                                return false;
                        }
                }
        
                /**
                 * @return bool
                 */
                public function exists() {
                        return !!self::getName();
                }
        
                /**
                 * @param string $name
                 * @return AuthorsContract
                 */
                public static function load(string $name) {
                        return new self($name);
                }
        
                /**
                 * @return mixed
                 */
                public function getName() {
                        return $this->name;
                }
        
                /**
                 * @return Blob
                 */
                public function getBlob(): Blob {
                        return $this->blob;
                }
        
                /**
                 * @return mixed
                 */
                public function getDeacription() {
                        return $this->description;
                }
        
                /**
                 * @return mixed
                 */
                public function getUploadDate() {
                        return $this->upload_date;
                }
        
                public function delete() {
                        $this->blob->delete($this->blob->getId());
                        \Drupal::database()->delete('rwPubgoldAuthorsContracts')->condition('name', $this->name, '=')->execute();
                }
        
                /**
                 * @param array $options
                 * @return Url
                 */
                public function getUrl(array $options = []): Url {
                        
                        return $this->getBlob()->getUrl($options);
                }
        }
