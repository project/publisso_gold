<?php
        
        /**
         * @file
         * Contains \Drupal\publisso_gold\Classes\Cron.
         */
        
        namespace Drupal\publisso_gold\Classes\Cron;
        
        use Drupal\publisso_gold\Controller\Publisso;

        /**
         * Class Cron
         * @package Drupal\publisso_gold\Classes\Cron
         */
        class Cron {
                
                private $setup;
                private $tools;
                private $texts;
                
                public function __construct() {
                        
                        $this->setup = Publisso::setup();
                        $this->tools = Publisso::tools();
                        $this->texts = Publisso::text();
                }
                
                public function cleanPendingActions() {
                        
                        $keys      = $this->setup->getKeys();
                        $processes = [];
                        
                        foreach ($keys as $key) {
                                
                                if (preg_match('/^(system\.keep\.pending\.)(.+)$/', $key, $matches)) {
                                        $processes[] = $matches[2];
                                }
                        }
                        
                        foreach ($processes as $process) {
                                
                                $value = (int)$this->setup->getValue($matches[1] . $process);
                                
                                if ($value) {
                                        $qry = \Drupal::database()->select('rwPubgoldPendingActions', 't')->fields('t', ['id'])->condition('process', $process, '=')->where('created < NOW() - INTERVAL :value HOUR', [':value' => $value]);
                                        
                                        $ids = $qry->execute()->fetchCol();
                                        
                                        if (count($ids)) {
                                                \Drupal::database()->delete('rwPubgoldPendingActions')->condition('id', $ids, 'IN')->execute();
                                        }
                                }
                        }
                        
                }
                
                public function cleanMaillog() {
                        
                        $value = (int)$this->setup->getValue('system.keep.maillog');
                        
                        if ($value) {
                                \Drupal::database()->delete('rwPubgoldMaillog')->where('`timestamp` < NOW() - INTERVAL :value HOUR', [':value' => $value])->execute();
                        }
                }
                
                public function cleanRegisterPending() {
                        
                        $value = (int)$this->setup->getValue('system.keep.register_pending');
                        
                        if ($value) {
                                \Drupal::database()->delete('rwPubgoldRegisterPending')->where('`rp_timestamp` < NOW() - INTERVAL :value HOUR', [':value' => $value])->execute();
                        }
                }
                
                public function cleanUserInvitations() {
                        
                        $value = (int)$this->setup->getValue('system.keep.user_invitations');
                        
                        if ($value) {
                                \Drupal::database()->delete('rwPubgoldUserInvitations')->where('`created` < NOW() - INTERVAL :value HOUR', [':value' => $value])->execute();
                        }
                }
        }
