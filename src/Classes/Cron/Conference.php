<?php
        
        
        namespace Drupal\publisso_gold\Classes\Cron;
        
        use Drupal\Core\Database\Connection;
        use Drupal\Core\StringTranslation\TranslatableMarkup;
        use Drupal\Core\StringTranslation\TranslationManager;
        use Drupal\publisso_gold\Controller\Manager\AbstractManager;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\WorkflowManager;

        /**
         * Class Conference
         * @package Drupal\publisso_gold\Classes\Cron
         */
        class Conference {
                
                private $tm;
                private $dbh;
                
                /**
                 * Conference constructor.
                 * @param TranslationManager $manager
                 * @param Connection $dbh
                 */
                public function __construct(TranslationManager $manager, Connection $dbh) {
                        
                        $this->tm  = $manager;
                        $this->dbh = $dbh;
                        
                        $this->closeSubmissions();
                }
                
                /**
                 * @param string $string
                 * @param array $args
                 * @return TranslatableMarkup
                 */
                private function t(string $string, array $args = []) {
                        return $this->tm->translate($string, $args);
                }
                
                private function closeSubmissions() {
                        
                        $filter = ['status' => 'submission_closed', 'closed_flag' => 0];
                        
                        foreach (ConferenceManager::getConferences($filter) as $cf_id) {
                                
                                $conference = ConferenceManager::getConference($cf_id);
                                $cms        = $conference->readConferenceManager();
                                $cmIDs      = [];
                                foreach ($cms as $cm) $cmIDs[] = $cm->getId();
                                
                                foreach (AbstractManager::getAbstractList($cf_id, ['ca_id', 'ca_wfid']) as $result) {
                                        
                                        $workflow = WorkflowManager::getItem($result->ca_wfid);
                                        
                                        if ($workflow->getElement('state') == 'running submission') {
                                                
                                                //assign workflow-cm's
                                                $workflow->setConferenceManagerIDs($cmIDs);
                                                $workflow->assignToUser($cmIDs);
                                                $workflow->newSchemaState($workflow->getSchemaDefaultFollower('schema_follower'));
                                                $workflow->setDataElement('received', date('Y-m-d'));
                                                $workflow->unlock();
                                        }
                                }
                                
                                $conference->setElement('submissions_closed', 1);
                        }
                }
        }