<?php
        
        
        namespace Drupal\publisso_gold\Classes;
        
        
        use Drupal\Core\Url;
        use Drupal\publisso_gold\Controller\Bookchapter;
        use Drupal\publisso_gold\Controller\Journalarticle;
        use Drupal\publisso_gold\Controller\License;
        use Drupal\publisso_gold\Controller\Manager\ConferenceManager;
        use Drupal\publisso_gold\Controller\Manager\OAIManager;
        use Drupal\publisso_gold\Controller\Publisso;
        use Drupal\publisso_gold\Controller\Reference;

        /**
         * Class OAIFactory
         * @package Drupal\publisso_gold\Classes
         */
        class OAIFactory {
        
                /**
                 * @return string
                 */
                public static function getDefaultResponse(): string {
                        return \file_get_contents(OAIManager::getAssetsDir() . '/default.html');
                }
        
                /**
                 * @return string
                 */
                public static function getNavigation(): string {
                        return \file_get_contents(OAIManager::getAssetsDir() . '/navigation.html');
                }
        
                /**
                 * @return string
                 */
                public static function getCSS(): string {
                        return \file_get_contents(OAIManager::getAssetsDir() . '/oai.css');
                }
        
                /**
                 * @param string $type
                 * @param string $metadataSet
                 * @return string
                 */
                public static function getResponseSchema(string $type, string $metadataSet = 'oai_dc'): string {
                        $method = "getResponseSchema_" . ucfirst($type);
                        return self::$method($metadataSet);
                }
        
                /**
                 * @param string $metadataSet
                 * @return false|string
                 */
                private static function getResponseSchema_ListSets(string $metadataSet) {
                        return \file_get_contents(OAIManager::getAssetsDir() . '/oai_listsets.xml');
                }
        
                /**
                 * @param string $metadataSet
                 * @return false|string
                 */
                private static function getResponseSchema_ListMetadataFormats(string $metadataSet) {
                        return \file_get_contents(OAIManager::getAssetsDir() . '/oai_listmetadataformats.xml');
                }
        
                /**
                 * @param string $metadataSet
                 * @return false|string
                 */
                private static function getResponseSchema_getRecord(string $metadataSet) {
                        return \file_get_contents(OAIManager::getAssetsDir() . '/oai_record.xml');
                }
        
                /**
                 * @param string $metadataSet
                 * @return false|string
                 */
                private static function getResponseSchema_ListRecords(string $metadataSet) {
                        return \file_get_contents(OAIManager::getAssetsDir() . '/oai_listrecords.xml');
                }
        
                /**
                 * @param string $metadataSet
                 * @return false|string
                 */
                private static function getResponseSchema_ListIdentifiers(string $metadataSet) {
                        return \file_get_contents(OAIManager::getAssetsDir() . '/oai_listidentifiers.xml');
                }
        
                /**
                 * @param \DOMDocument $xml
                 * @param \DOMXPath $xpath
                 * @return bool
                 */
                private static function setSchemaValues_ListIdentifiers(\DOMDocument &$xml, \DOMXPath &$xpath): bool {
                        
                        $tempstore       = \Drupal::service('user.private_tempstore')->get('publisso_gold.oai.response')->get('data');
                        $request         = \Drupal::requestStack()->getCurrentRequest();
                        $params          = array_merge($request->request->all(), $request->query->all());
                        $tempstore       = array_merge($tempstore, $params);
                        $identifiersNode = $xpath->evaluate('//identifiers')->item(0);
                        $errorNode       = $xml->createElement('error');
                        $requestNode     = $xpath->evaluate('//request')->item(0);
                        
                        if (!$tempstore['metadataPrefix']) $tempstore['metadataPrefix'] = 'oai_dc';
                        
                        //prüfen auf Fehler
                        $mandatoryParameters = ['metadataPrefix'];
                        foreach ($mandatoryParameters as $param) {
                                if (!array_key_exists($param, $tempstore) || empty($tempstore[$param])) {
                                        $errorNode->setAttribute('code', 'Bad Argument');
                                }
                        }
                        
                        //metadataPrefix muss vorhanden sein - definiert in oai_listmetadataprefixes.xml
                        if (!in_array($tempstore['metadataPrefix'], OAIManager::getMetadataPrefixes())) {
                                $errorNode->setAttribute('code', 'cannotDisseminateFormat');
                        }
                        
                        $resumptionToken = null;
                        if (array_key_exists('resumptionToken', $params)) {
                                
                                $resumptionToken = $params['resumptionToken'];
                                $resumptionToken = explode('|', $resumptionToken);
                        }
                        
                        foreach ($tempstore as $k => $v) {
                                if (!empty($v)) $resumptionToken[] = "$k=$v";
                        }
                        
                        foreach ($resumptionToken as $k => $v) {
                                $_                      = explode('=', $v);
                                $resumptionToken[$_[0]] = $_[1];
                                unset($resumptionToken[$k]);
                        }
                        
                        if (!(array_key_exists('next', $resumptionToken))) $resumptionToken['next'] = 1;
                        
                        if (array_key_exists('from', $resumptionToken) && array_key_exists('until', $resumptionToken)) {
                                
                                $from  = strtotime($resumptionToken['from']);
                                $until = strtotime($resumptionToken['until']);
                                
                                if ($from > $until) {
                                        $errorNode->setAttribute('code', 'cannotDisseminateFormat,badArgument');
                                }
                        }
                        
                        //wenn ein Fehler auftrat, dann die Platzhalter-Node durch die Fehler-Node ersetzen und abbrechen
                        if ($errorNode->hasAttribute('code')) {
                                $_node = $xpath->evaluate('//ListIdentifiers')->item(0);
                                $_node->parentNode->replaceChild($errorNode, $_node);
                                return false;
                        }
                        
                        $qry = \Drupal::database()->select('rwpgvwEntities', 't')->fields('t', []);
                        
                        if (array_key_exists('from', $resumptionToken)) {
                                $qry->condition('published', $resumptionToken['from'], '>=');
                        }
                        
                        if (array_key_exists('until', $resumptionToken)) {
                                $qry->condition('published', $resumptionToken['until'], '<=');
                        }
                        
                        if (array_key_exists('set', $resumptionToken)) {
                                
                                switch ($resumptionToken['set']) {
                                        
                                        case 'ddc':
                                                $qry->isNotNull('ddc');
                                                break;
                                        
                                        case 'doc-type:article':
                                                $qry->condition('type', 'JA', '=');
                                                break;
                                        
                                        case 'doc-type:chapter':
                                                $qry->condition('type', 'BC', '=');
                                                break;
                                        
                                        case 'doc-type:conferenceObject':
                                                $qry->condition('type', 'CP', '=');
                                                break;
                                        
                                        case 'fundedressources':
                                                $qry->isNotNull('funding_id');
                                                break;
                                        
                                        case 'status-type:publishedVersion':
                                                $qry->condition('status', 'published', '=');
                                                break;
                                        
                                        case 'status-type:updatedVersion':
                                                $qry->condition('status', 'updated', '=');
                                                break;
                                        
                                        default:
                                                if (preg_match('/^ddc:(.+)/', $resumptionToken['set'], $matches)) {
                                                        $qry->condition('ddc', $matches[1], '=');
                                                }
                                }
                        }
                        $qry->orderBy('published', 'ASC');
                        $numDataset = $qry->countQuery()->execute()->fetchField();
                        
                        $resultLength = 100;
                        $qry->range($resumptionToken['next'] - 1, $resultLength);
                        //einpflegen der Werte
                        foreach ($qry->execute()->fetchAll() as $res) {
                                
                                $result = (array)$res;
                                
                                $requestNode->setAttribute('metadataPrefix', $tempstore['metadataPrefix']);
                                
                                $record = ['identifier' => $tempstore['identifier'], 'status-type' => 'status-type:' . $result['status'] . 'Version', 'access-type' => 'open_access', 'datestamp' => $result['published'],];
                                
                                switch ($result['type']) {
                                        
                                        case 'JA':
                                                $record['doc-type'] = 'doc-type:article';
                                                break;
                                        
                                        case 'BC':
                                                $record['doc-type'] = 'doc-type:chapter';
                                                break;
                                        
                                        case 'CP':
                                                $record['doc-type'] = 'doc-type:conferenceObject';
                                                break;
                                }
                                
                                if (!empty($result['ddc'])) $record['ddc'] = 'ddc:' . $result['ddc'];
                                
                                if (!empty($result['doi'])) {
                                        $record['identifier'] = 'oai:oai.publisso.de:' . strtoupper($result['type']) . '!DOI!' . $result['doi'];
                                } else {
                                        $record['identifier'] = 'oai:oai.publisso.de:' . strtoupper($result['type']) . '!IID!' . $result['id'];
                                }
                                
                                $recordXML = \file_get_contents(OAIManager::getAssetsDir() . '/oai_identifier_' . $tempstore['metadataPrefix'] . '.xml');
                                
                                $frag = $xml->createDocumentFragment();
                                $frag->appendXML($recordXML);
                                
                                $recordNode = $identifiersNode->parentNode->appendChild($frag);
                                
                                foreach ($xpath->query('//*[@systemValue]', $recordNode) as $_node) {
                                        
                                        list($type, $name) = explode('::', $_node->getAttribute('systemValue'));
                                        $_node->removeAttribute('systemValue');
                                        
                                        switch (strtolower($type)) {
                                                
                                                case 'record':
                                                        
                                                        if ($_node->hasAttribute('lang')) $value = $record[$_node->getAttribute('lang')][$name] ?? null; else $value = $record[$name];
                                                        
                                                        if (empty($value) && $_node->hasAttribute('removeIfEmpty') && $_node->getAttribute('removeIfEmpty')) {
                                                                $_node->parentNode->removeChild($_node);
                                                        }
                                                        
                                                        $_node->removeAttribute('removeIfEmpty');
                                                        
                                                        if (is_scalar($value)) {
                                                                $_node->nodeValue = $value;
                                                        }
                                                        
                                                        if (is_array($value) || is_object($value)) {
                                                                $value            = (array)$value;
                                                                $_node->nodeValue = array_shift($value);
                                                                
                                                                if (count($value)) {
                                                                        $value = array_reverse($value);
                                                                        
                                                                        foreach ($value as $_) {
                                                                                $__node            = $_node->cloneNode();
                                                                                $__node->nodeValue = $_;
                                                                                
                                                                                $_node->parentNode->insertBefore($__node, $_node->nextSibling);
                                                                        }
                                                                }
                                                        }
                                                        
                                                        break;
                                        }
                                        
                                        $_node->removeAttribute('removeIfEmpty');
                                }
                        }
                        
                        if ($resumptionToken['next'] + $resultLength < $numDataset) {
                                $next = $resumptionToken['next'] + $resultLength;
                                
                                $params = ['metadataPrefix' => $tempstore['metadataPrefix'], 'next' => $next];
                                
                                foreach ($resumptionToken as $k => $v) {
                                        if (!array_key_exists($k, $params)) $params[$k] = $v;
                                }
                                
                                $paramStr  = '';
                                $availKeys = ['set', 'from', 'until', 'next', 'metadataPrefix'];
                                
                                foreach ($params as $k => $v) {
                                        if (!(empty($k) || empty($v)) && in_array($k, $availKeys)) $paramStr .= (!empty($paramStr) ? '|' : '') . "$k=$v";
                                }
                                date_default_timezone_set("UTC");
                                
                                $tokenNode = $xml->createElement('resumptionToken', $paramStr);
                                $tokenNode->setAttribute('completeListSize', $numDataset);
                                $tokenNode->setAttribute('expirationDate', date('c', time() + 48 * 3600));
                                $identifiersNode->parentNode->appendChild($tokenNode);
                        }
                        
                        $identifiersNode->parentNode->removeChild($identifiersNode);
                        
                        return true;
                }
        
                /**
                 * @param \DOMDocument $xml
                 * @param \DOMXPath $xpath
                 * @return bool
                 */
                private static function setSchemaValues_ListRecords(\DOMDocument &$xml, \DOMXPath &$xpath): bool {
                        
                        $tempstore   = \Drupal::service('user.private_tempstore')->get('publisso_gold.oai.response')->get('data');
                        $request     = \Drupal::requestStack()->getCurrentRequest();
                        $params      = array_merge($request->query->all(), $request->request->all());
                        $tempstore   = array_merge($tempstore, $params);
                        $recordsNode = $xpath->evaluate('//records')->item(0);
                        $errorNode   = $xml->createElement('error');
                        $requestNode = $xpath->evaluate('//request')->item(0);
                        
                        if (!$tempstore['metadataPrefix']) $tempstore['metadataPrefix'] = 'oai_dc';
                        
                        //prüfen auf Fehler
                        $mandatoryParameters = ['metadataPrefix'];
                        foreach ($mandatoryParameters as $param) {
                                if (!array_key_exists($param, $tempstore) || empty($tempstore[$param])) {
                                        $errorNode->setAttribute('code', 'Bad Argument');
                                }
                        }
                        
                        //metadataPrefix muss vorhanden sein - definiert in oai_listmetadataprefixes.xml
                        if (!in_array($tempstore['metadataPrefix'], OAIManager::getMetadataPrefixes())) {
                                $errorNode->setAttribute('code', 'cannotDisseminateFormat');
                        }
                        
                        $resumptionToken = null;
                        if (array_key_exists('resumptionToken', $params)) {
                                
                                $resumptionToken = $params['resumptionToken'];
                                $resumptionToken = explode('|', $resumptionToken);
                        }
                        
                        foreach ($tempstore as $k => $v) {
                                if (!empty($v)) $resumptionToken[] = "$k=$v";
                        }
                        
                        foreach ($resumptionToken as $k => $v) {
                                $_                      = explode('=', $v);
                                $resumptionToken[$_[0]] = $_[1];
                                unset($resumptionToken[$k]);
                        }
                        
                        if (!(array_key_exists('next', $resumptionToken))) $resumptionToken['next'] = 1;
                        
                        if (array_key_exists('from', $resumptionToken) && array_key_exists('until', $resumptionToken)) {
                                
                                $from  = strtotime($resumptionToken['from']);
                                $until = strtotime($resumptionToken['until']);
                                
                                if ($from > $until) {
                                        $errorNode->setAttribute('code', 'cannotDisseminateFormat,badArgument');
                                }
                        }
                        
                        //wenn ein Fehler auftrat, dann die Platzhalter-Node durch die Fehler-Node ersetzen und abbrechen
                        if ($errorNode->hasAttribute('code')) {
                                $_node = $xpath->evaluate('//ListRecords')->item(0);
                                $_node->parentNode->replaceChild($errorNode, $_node);
                                return false;
                        }
                        
                        $qry = \Drupal::database()->select('rwpgvwEntities', 't')->fields('t', []);
                        
                        if (array_key_exists('from', $resumptionToken)) {
                                $qry->condition('published', $resumptionToken['from'], '>=');
                                
                                if (!array_key_exists('until', $resumptionToken)) {
                                        $qry->condition('published', $resumptionToken['from'], '<=');
                                }
                        }
                        
                        if (array_key_exists('until', $resumptionToken)) {
                                $qry->condition('published', $resumptionToken['until'], '<=');
                        }
                        
                        if (array_key_exists('set', $resumptionToken)) {
                                
                                switch ($resumptionToken['set']) {
                                        
                                        case 'ddc':
                                                $qry->isNotNull('ddc');
                                                break;
                                        
                                        case 'doc-type:article':
                                                $qry->condition('type', 'JA', '=');
                                                break;
                                        
                                        case 'doc-type:chapter':
                                                $qry->condition('type', 'BC', '=');
                                                break;
                                        
                                        case 'doc-type:conferenceObject':
                                                $qry->condition('type', 'CP', '=');
                                                break;
                                        
                                        case 'fundedressources':
                                                $qry->isNotNull('funding_id');
                                                break;
                                        
                                        case 'status-type:publishedVersion':
                                                $qry->condition('status', 'published', '=');
                                                break;
                                        
                                        case 'status-type:updatedVersion':
                                                $qry->condition('status', 'updated', '=');
                                                break;
                                        
                                        default:
                                                if (preg_match('/^ddc:(.+)/', $resumptionToken['set'], $matches)) {
                                                        $qry->condition('ddc', $matches[1], '=');
                                                }
                                }
                        }
                        $qry->orderBy('published', 'ASC');
                        $numDataset = $qry->countQuery()->execute()->fetchField();
                        
                        $resultLength = 100;
                        $qry->range($resumptionToken['next'] - 1, $resultLength);
                        //einpflegen der Werte
                        foreach ($qry->execute()->fetchAll() as $res) {
                                
                                $result = (array)$res;
                                $requestNode->setAttribute('metadataPrefix', $tempstore['metadataPrefix']);
                                
                                $record = ['identifier' => $tempstore['identifier'] ?? null, 'status-type' => 'status-type:' . $result['status'] . 'Version', 'access-type' => 'open_access'];
                                
                                $recordDir  = OAIManager::getVarDir() . '/' . date("Y", strtotime($result['published'])) . '/' . date("m", strtotime($result['published'])) . '/' . $tempstore['metadataPrefix'] . '/';
                                $recordFile = $recordDir . $result['type'] . '-IID-' . $result['id'] . '.xml';
                                
                                if (!is_file($recordFile)) {
                                        
                                        $recordXML = \file_get_contents(OAIManager::getAssetsDir() . '/oai_record_' . $tempstore['metadataPrefix'] . '.xml');
                                        
                                        $frag = $xml->createDocumentFragment();
                                        $frag->appendXML($recordXML);
                                        
                                        $recordNode = $recordsNode->parentNode->appendChild($frag);
                                        
                                        self::setEntityDataToRecord($record, $result['type'], 'IID', $result['id']);
                                        
                                        foreach ($xpath->query('//*[@systemValue]', $recordNode) as $_node) {
                                                
                                                list($type, $name) = explode('::', $_node->getAttribute('systemValue'));
                                                $_node->removeAttribute('systemValue');
                                                
                                                switch (strtolower($type)) {
                                                        
                                                        case 'record':
                                                                
                                                                if ($_node->hasAttribute('lang')) $value = $record[$_node->getAttribute('lang')][$name] ?? null; else $value = $record[$name];
                                                                
                                                                if (empty($value) && $_node->hasAttribute('removeIfEmpty') && $_node->getAttribute('removeIfEmpty')) {
                                                                        $_node->parentNode->removeChild($_node);
                                                                }
                                                                
                                                                $_node->removeAttribute('removeIfEmpty');
                                                                
                                                                if (is_scalar($value)) {
                                                                        #$_node->nodeValue = $value;
                                                                        $_node->appendChild($xml->createCDATASection($value));
                                                                }
                                                                
                                                                if (is_array($value) || is_object($value)) {
                                                                        $value            = (array)$value;
                                                                        $_node->nodeValue = array_shift($value);
                                                                        
                                                                        if (count($value)) {
                                                                                $value = array_reverse($value);
                                                                                
                                                                                foreach ($value as $_) {
                                                                                        $__node            = $_node->cloneNode();
                                                                                        $__node->nodeValue = $_;
                                                                                        
                                                                                        $_node->parentNode->insertBefore($__node, $_node->nextSibling);
                                                                                }
                                                                        }
                                                                }
                                                                
                                                                break;
                                                }
                                                
                                                $_node->removeAttribute('removeIfEmpty');
                                        }
                                        
                                        if (!is_dir($recordDir)) mkdir($recordDir, 0750, true);
                                        \file_put_contents($recordFile, $recordNode->C14N());
                                } else {
                                        $recordXML = \file_get_contents($recordFile);
                                        
                                        $frag = $xml->createDocumentFragment();
                                        $frag->appendXML($recordXML);
                                        
                                        $recordNode = $recordsNode->parentNode->appendChild($frag);
                                }
                        }
                        
                        if ($resumptionToken['next'] + $resultLength < $numDataset) {
                                $next = $resumptionToken['next'] + $resultLength;
                                
                                $params = ['metadataPrefix' => $tempstore['metadataPrefix'], 'next' => $next];
                                
                                foreach ($resumptionToken as $k => $v) {
                                        if (!array_key_exists($k, $params)) $params[$k] = $v;
                                }
                                
                                $paramStr  = '';
                                $availKeys = ['set', 'from', 'until', 'next', 'metadataPrefix'];
                                
                                foreach ($params as $k => $v) {
                                        if (!(empty($k) || empty($v)) && in_array($k, $availKeys)) $paramStr .= (!empty($paramStr) ? '|' : '') . "$k=$v";
                                }
                                date_default_timezone_set("UTC");
                                
                                $tokenNode = $xml->createElement('resumptionToken', $paramStr);
                                $tokenNode->setAttribute('completeListSize', $numDataset);
                                $tokenNode->setAttribute('expirationDate', date('c', time() + 48 * 3600));
                                $recordsNode->parentNode->appendChild($tokenNode);
                        }
                        
                        $recordsNode->parentNode->removeChild($recordsNode);
                        return true;
                }
        
                /**
                 * @param \DOMDocument $xml
                 * @param \DOMXPath $xpath
                 * @return bool
                 */
                private static function setSchemaValues_GetRecord(\DOMDocument &$xml, \DOMXPath &$xpath): bool {
                        
                        $tempstore   = \Drupal::service('user.private_tempstore')->get('publisso_gold.oai.response')->get('data');
                        $request     = \Drupal::requestStack()->getCurrentRequest();
                        $params      = array_merge($request->request->all(), $request->query->all());
                        $tempstore   = array_merge($tempstore, $params);
                        $recordNode  = $xpath->evaluate('//record')->item(0);
                        $errorNode   = $xml->createElement('error');
                        $requestNode = $xpath->evaluate('//request')->item(0);
                        
                        if (!$tempstore['metadataPrefix']) $tempstore['metadataPrefix'] = 'oai_dc';
                        
                        //prüfen auf Fehler
                        $mandatoryParameters = ['identifier', 'metadataPrefix'];
                        foreach ($mandatoryParameters as $param) {
                                if (!array_key_exists($param, $tempstore) || empty($tempstore[$param])) {
                                        $errorNode->setAttribute('code', 'Bad Argument');
                                }
                        }
                        
                        //metadataPrefix muss vorhanden sein - definiert in oai_listmetadataprefixes.xml
                        if (!in_array($tempstore['metadataPrefix'], OAIManager::getMetadataPrefixes())) {
                                $errorNode->setAttribute('code', 'cannotDisseminateFormat');
                        }
                        
                        //prüfen des Identifiers auf korrekte Syntax
                        $pattern = '/^' . Publisso::setup()->getValue('oai.Identify.description.scheme') . ':' . str_replace('.', '\.', Publisso::setup()->getValue('oai.Identify.description.repositoryIdentifier')) . ':(BC|JA|CP)!(DOI|IID)!([^!:]+)$/';
                        if (!preg_match($pattern, $tempstore['identifier'], $matches)) {
                                $errorNode->setAttribute('code', 'Malformed Identifier');
                        }
                        
                        $type   = $matches[1];
                        $IDType = $matches[2];
                        $ID     = $matches[3];
                        
                        //ist diese Entity vorhanden?
                        $qry = \Drupal::database()->select('rwpgvwEntities', 't');
                        $qry->fields('t', ['id']);
                        $qry->condition('type', $type, '=');
                        
                        if ($IDType == 'DOI') $qry->condition('doi', '%' . $ID, 'LIKE'); elseif ($IDType == 'IID') $qry->condition('id', $ID, '=');
                        
                        if (!($id = $qry->execute()->fetchField())) {
                                $errorNode->setAttribute('code', 'Entity Not Found');
                        }
                        
                        //wenn ein Fehler auftrat, dann die Platzhalter-Node durch die Fehler-Node ersetzen und abbrechen
                        if ($errorNode->hasAttribute('code')) {
                                $_node = $xpath->evaluate('//GetRecord')->item(0);
                                $_node->parentNode->replaceChild($errorNode, $_node);
                                return false;
                        }
                        
                        //einpflegen der Werte
                        $result = $qry->addField('t', 'status');
                        $result = $qry->execute()->fetchAssoc();
                        $requestNode->setAttribute('identifier', $tempstore['identifier']);
                        $requestNode->setAttribute('metadataPrefix', $tempstore['metadataPrefix']);
                        
                        $record = ['identifier' => $tempstore['identifier'], 'status-type' => 'status-type:' . $result['status'] . 'Version', 'access-type' => 'open_access'];
                        
                        $recordXML = \file_get_contents(OAIManager::getAssetsDir() . '/oai_record_' . $tempstore['metadataPrefix'] . '.xml');
                        $frag      = $xml->createDocumentFragment();
                        $frag->appendXML($recordXML);
                        $recordNode->parentNode->replaceChild($frag, $recordNode);
                        
                        self::setEntityDataToRecord($record, $type, $IDType, $ID);
                        
                        foreach ($xpath->query('//*[@systemValue]') as $_node) {
                                
                                list($type, $name) = explode('::', $_node->getAttribute('systemValue'));
                                
                                switch (strtolower($type)) {
                                        
                                        case 'record':
                                                
                                                if ($_node->hasAttribute('lang')) $value = $record[$_node->getAttribute('lang')][$name] ?? null; else $value = $record[$name];
                                                
                                                if (empty($value) && $_node->hasAttribute('removeIfEmpty') && $_node->getAttribute('removeIfEmpty')) {
                                                        $_node->parentNode->removeChild($_node);
                                                }
                                                
                                                if (is_scalar($value)) {
                                                        $_node->nodeValue = $value;
                                                }
                                                
                                                if (is_array($value) || is_object($value)) {
                                                        $value            = (array)$value;
                                                        $_node->nodeValue = array_shift($value);
                                                        
                                                        if (count($value)) {
                                                                $value = array_reverse($value);
                                                                
                                                                foreach ($value as $_) {
                                                                        $__node            = $_node->cloneNode();
                                                                        $__node->nodeValue = $_;
                                                                        
                                                                        $_node->parentNode->insertBefore($__node, $_node->nextSibling);
                                                                }
                                                        }
                                                }
                                                
                                                break;
                                }
                        }
                        
                        return true;
                }
        
                /**
                 * @param array $record
                 * @param string $mediumType
                 * @param string $IDType
                 * @param $mediumID
                 * @return bool
                 */
                private static function setEntityDataToRecord(array &$record, string $mediumType, string $IDType, $mediumID): bool {
                        
                        $medium = null;
                        $lang   = \Drupal::service('language_manager')->getDefaultLanguage()->getId();
                        
                        if (strtolower($IDType) == 'doi') {
                                $mediumID = \Drupal::database()->select('rwpgvwEntities', 't')->fields('t', ['id'])->condition('doi', $mediumID, '=')->execute()->fetchField();
                        }
                        
                        switch (strtolower($mediumType)) {
                                
                                case 'bc': //bookchapter
                                        $medium                   = new Bookchapter($mediumID, true);
                                        $record['record-type']    = 'text';
                                        $record['record-subtype'] = 'bookpart';
                                        $record['doc-type']       = 'doc-type:bookpart';
                                        $origText                 = base64_decode($medium->getElement('chapter_text')) ?? '';
                                        break;
                                
                                case 'ja': //journalarticle
                                        $medium                   = new Journalarticle($mediumID, true);
                                        $record['record-type']    = 'text';
                                        $record['record-subtype'] = 'article';
                                        $record['doc-type']       = 'doc-type:article';
                                        $origText                 = base64_decode($medium->getElement('article_text')) ?? '';
                                        break;
                                
                                case 'cp': //conferencepaper
                                        $medium                   = ConferenceManager::getConferencePaper($mediumID, true);
                                        $record['record-type']    = 'text';
                                        $record['record-subtype'] = 'paper';
                                        $record['doc-type']       = 'doc-type:conferencepaper';
                                        $origText                 = base64_decode($medium->getElement('paper_text')) ?? '';
                                        break;
                        }
                        
                        if (!$medium) return false;
                        if (!$record['identifier']) {
                                if (!empty($medium->getElement('doi'))) {
                                        $record['identifier'] = 'oai:oai.publisso.de:' . strtoupper($mediumType) . '!DOI!' . $medium->getElement('doi');
                                } else {
                                        $record['identifier'] = 'oai:oai.publisso.de:' . strtoupper($mediumType) . '!IID!' . $medium->getElement('id');
                                }
                        }
                        
                        $translations = [];
                        
                        try {
                                $translations = $medium->getTranslations();
                        }
                        catch (\Error $e) {
                        }
                        
                        $translations[] = $medium->getElement('language') ?? $medium->getParentElement('language') ?? $lang;
                        $translations   = array_unique($translations);
                        
                        
                        $record['title'] = str_replace('&', '&amp;', html_entity_decode($medium->getElement('title')));
                        foreach ($translations as $lang) $record[$lang]['title'] = str_replace('&', '&amp;', html_entity_decode($medium->getElement('title', $lang))) ?? null;
                        
                        $record['abstract'] = str_replace('&', '&amp;', strip_tags(html_entity_decode(base64_decode($medium->getElement('abstract')))));
                        foreach ($translations as $lang) $record[$lang]['abstract'] = str_replace('&', '&amp;', strip_tags(html_entity_decode(base64_decode($medium->getElement('abstract', $lang))))) ?? null;
                        
                        $record['publisher'] = str_replace('&', '&amp;', html_entity_decode(Publisso::tools()->getPublisher($medium->getElement('publisher'))));
                        foreach ($translations as $lang) $record[$lang]['publisher'] = html_entity_decode(Publisso::tools()->getPublisher($medium->getElement('publisher', $lang))) ?? null;
                        
                        $record['language'] = !empty($medium->getElement('language')) ? $medium->getElement('language') : $medium->getParentElement('language');
                        foreach ($translations as $lang) $record[$lang]['language'] = $medium->getElement('language', $lang) ?? null;
                        
                        $record['datestamp'] = $record['date-published'] = html_entity_decode($medium->getElement('published'));
                        foreach ($translations as $lang) $record[$lang]['date-published'] = $medium->getElement('published', $lang) ?? null;
                        
                        $url = $medium->getLink('url');
                        if ($url) $record['url'] = $url->toString();
                        
                        $record['parent-title'] = str_replace('&', '&amp;', html_entity_decode($medium->getParentElement('title')));
                        foreach ($translations as $lang) $record[$lang]['parent-title'] = str_replace('&', '&amp;', $medium->getParentElement('title', $lang)) ?? null;
                        
                        if ($medium->getElement('keywords')) {
                                
                                if (is_scalar($medium->getElement('keywords'))) {
                                        $keywords = json_decode($medium->getElement('keywords'));
                                        if (json_last_error()) {
                                                if (false === ($keywords = unserialize($medium->getElement('keywords')))) $keywords = explode($medium->getElement('keywords'));
                                        }
                                }
                                
                                $keywords = (array)$keywords;
                                $keywords = array_map('trim', $keywords);
                                $keywords = array_map('html_entity_decode', $keywords);
                                foreach ($keywords as $k => $v) $keywords[$k] = str_replace('&', '&amp;', $v);
                                $record['keywords'] = $keywords;
                                
                                foreach ($translations as $lang) {
                                        
                                        if (is_scalar($medium->getElement('keywords', $lang))) {
                                                $keywords = json_decode($medium->getElement('keywords', $lang));
                                                if (json_last_error()) {
                                                        if (false === ($keywords = unserialize($medium->getElement('keywords', $lang)))) $keywords = explode($medium->getElement('keywords', $lang));
                                                }
                                        }
                                        
                                        $keywords                  = (array)$keywords;
                                        $keywords                  = array_map('trim', $keywords);
                                        $keywords                  = array_map('html_entity_decode', $keywords);
                                        $record[$lang]['keywords'] = $keywords;
                                }
                        }
                        
                        if ($medium->getElement('license')) {
                                $license           = new License($medium->getElement('license'));
                                $record['license'] = $license->getElement('url');
                        }
                        
                        if ($medium->getElement('ddc')) {
                                $record['ddc']       = !empty($medium->getElement('ddc')) ? 'ddc:' . Publisso::tools()->getDDC($medium->getElement('ddc'), 'number') : null;
                                $record['ddc-title'] = 'ddc:' . str_replace('&', '&amp;', html_entity_decode(Publisso::tools()->getDDC($medium->getElement('ddc'), 'name')));
                        }
                        
                        foreach ($medium->readAuthors() as $author) {
                                $record['author'][] = $author->profile->getElement('lastname') . ', ' . $author->profile->getFirstnameInitials();
                        }
                        
                        $record['references'] = [];
                        //References
                        $refList = [];
                        
                        
                        $origTextXML = new \DOMDocument();
                        $origTextXML->loadHTML($origText);
                        $origTextXPATH = new \DOMXpath($origTextXML);
                        
                        foreach ($origTextXPATH->evaluate('//a') as $nodeA) {
                                
                                if ($nodeA->hasAttribute('href') && substr($nodeA->getAttribute('href'), 0, 10) == '#rwPubRef~') {
                                        $refList[$nodeA->nodeValue] = new Reference(explode('~', $nodeA->getAttribute('href'))[1]);
                                }
                        }
                        
                        foreach ($refList as $reference) {
                                $record['references'][] = str_replace('&', '&amp;', html_entity_decode($reference->getElement('reference')));
                        }
                        
                        //generate DOI-Link
                        if (!empty($medium->getElement('doi'))) {
                                $record['url-doi'] = Url::fromUri('https://dx.doi.org/' . $medium->getElement('doi'))->toString();
                        }
                        
                        //generate URN-Link
                        if (!empty($medium->getElement('urn'))) {
                                $record['url-urn'] = Url::fromUri('https://nbn-resolving.de/' . $medium->getElement('urn'))->toString();
                        }
                        
                        return true;
                }
        
                /**
                 * @param string $metadataSet
                 * @return false|string
                 */
                private static function getResponseSchema_Identify(string $metadataSet) {
                        return \file_get_contents(OAIManager::getAssetsDir() . '/oai_identify.xml');
                }
        
                /**
                 * @param \DOMDocument $xml
                 * @param string $requestType
                 * @return bool
                 */
                public static function setSchemaValues(\DOMDocument &$xml, string $requestType): bool {
                        
                        $xpath  = new \DOMXPath($xml);
                        $method = explode('::', __METHOD__)[1] . '_' . ucfirst($requestType);
                        self::$method($xml, $xpath);
                        return true;
                }
        
                /**
                 * @param \DOMDocument $xml
                 * @param \DOMXPath $xpath
                 * @return bool
                 */
                private static function setSchemaValues_ListSets(\DOMDocument &$xml, \DOMXPath &$xpath): bool {
                        return true;
                }
        
                /**
                 * @param \DOMDocument $xml
                 * @param \DOMXPath $xpath
                 * @return bool
                 */
                private static function setSchemaValues_ListMetadataFormats(\DOMDocument &$xml, \DOMXPath &$xpath): bool {
                        return true;
                }
        
                /**
                 * @param \DOMDocument $xml
                 * @param \DOMXPath $xpath
                 * @return bool
                 */
                private static function setSchemaValues_Identify(\DOMDocument &$xml, \DOMXPath &$xpath): bool {
                        
                        foreach ($xpath->query('//*[@systemValue]') as $node) {
                                
                                list($method, $key) = explode('::', $node->getAttribute('systemValue'));
                                
                                switch ($method) {
                                        
                                        case 'setup':
                                                $value = Publisso::setup()->getValue($key);
                                                
                                                if ($node->hasAttribute('multiValueSeparator')) {
                                                        $value = explode($node->getAttribute('multiValueSeparator'), $value);
                                                        $node->removeAttribute('multiValueSeparator');
                                                }
                                                
                                                $value           = (array)$value;
                                                $node->nodeValue = array_shift($value); //das erste Element
                                                
                                                if (count($value)) { //falls es noch mehr gibt
                                                        
                                                        $value = array_reverse($value); //das Einsetzen der neuen Nodes ist von unten nach oben, also Array umdrehen, damit die Reihenfolge des DB-Eintrags erhalten bleibt
                                                        
                                                        foreach ($value as $_) {
                                                                $_node            = $node->cloneNode();
                                                                $_node->nodeValue = $_;
                                                                
                                                                $node->parentNode->insertBefore($_node, $node->nextSibling);
                                                        }
                                                }
                                                break;
                                        
                                        case 'db':
                                                $qry = [];
                                                foreach (explode('|', $key) as $_) {
                                                        $qry[explode('=', $_)[0]] = explode('=', $_)[1];
                                                }
                                                
                                                $db = \Drupal::database()->select($qry['dbname'], 't');
                                                
                                                foreach ($qry as $k => $v) {
                                                        
                                                        switch ($k) {
                                                                
                                                                case 'expression':
                                                                        $db->addExpression($v);
                                                                        break;
                                                                
                                                                case 'field':
                                                                        $db->addField($v);
                                                                        break;
                                                                
                                                                case 'condition':
                                                                        $condition = explode('#', $v);
                                                                        $db->condition($condition[0], $condition[1], $condition[2]);
                                                                        break;
                                                        }
                                                }
                                                
                                                try {
                                                        $node->nodeValue = $db->execute()->fetchField();
                                                }
                                                catch (\Exception $e) {
                                                        Publisso::tools()->logMsg('ERROR in ' . __FILE__ . ' line ' . __LINE__ . ': ' . $e->getMessage());
                                                }
                                                
                                                break;
                                        
                                        default:
                                                Publisso::tools()->logMsg('ERROR in ' . __FILE__ . ' line ' . __LINE__ . ': OAI-method "' . $method . '" ist not implemented for verb "Identify"');
                                }
                                
                        }
                        
                        foreach ($xpath->query('//*[@systemValue]') as $node) {
                                $node->removeAttribute('systemValue');
                        }
                        
                        return true;
                }
        }
