<?php

	/**
	 * @file
	 * Contains \Drupal\publisso_gold\Controller\Template.
	 */

	namespace Drupal\publisso_gold\Controller;
        
        /**
         * Class Template
         * @package Drupal\publisso_gold\Controller
         */
        class Template{
		
		private $dbh;
		private $var;
		private $tmp;
                
                /**
                 * @return string
                 */
                public function __toString(){
			return 'Template';
		}
                
                /**
                 * @param $name
                 * @return $this
                 */
                public function get($name){
			
			$path = __DIR__."/../../inc/$name.tmpl.inc.php";
			if(!(file_exists($path) && is_readable($path))){
                                $this->tmp = (string)t("Can't read template @name", ['@name' => $name]);
                        }
                        else{
                                $this->tmp = file_get_contents($path);
                        }
                        
                        return $this;
		}
                
                /**
                 * @param $name
                 * @param $value
                 */
                public function setVar($name, $value){
                        $this->var[$name] = $value;
		}
                
                /**
                 * @param $name
                 * @param $value
                 */
                public function appendToVar($name, $value){
                        $this->var[$name] .= $value;
		}
                
                /**
                 * @param $name
                 * @param $value
                 */
                public function prependToVar($name, $value){
                        $this->var[$name] = $value.$this->var[$name];
		}
                
                /**
                 * @param $name
                 * @return mixed
                 */
                public function getVar($name){
                        return $this->var[$name];
		}
                
                /**
                 * @param string $delimiter
                 * @param bool $unknownVars
                 * @return string|string[]|null
                 */
                public function parse($delimiter = '::', $unknownVars = true){
                        
                        $tmpl = $this->tmp;
                        $vars =[];
                        
                        foreach($this->var as $key => $val){
                                $vars[$delimiter.$key.$delimiter] = $val;
                        }
                        
                        $tmpl = str_replace(array_keys($vars), array_values($vars), $tmpl);
                        
                        if($unknownVars)
                                $tmpl = preg_replace("/$delimiter([a-zA-Z-_1-9]+)$delimiter?/", '', $tmpl);
                        
                        return $tmpl;
		}
                
                /**
                 * @param $HTMLString
                 */
                public function prependHTML($HTMLString){
                        $this->tmp = $HTMLString.$this->tmp;
		}
                
                /**
                 * @param $HTMLString
                 */
                public function appendHTML($HTMLString){
                        $this->tmp = $this->tmp.$HTMLString;
		}
		
		public function print(){
                        return $this->tmp;
		}
	}
