jQuery(document).ready(function() {

    (function(jQuery) {
        var pc = 100;

        function rwResetFontSize() {
            rwSetFontSize(pc, 100);
            pc = 100;
        }

        function rwSetFontSize(e, t) {

            var n = [];

            jQuery("body *").each(function () {
                var e = jQuery(this).css("font-size");
                e = e.replace("px", "");
                n.push({elem: jQuery(this), fs: e});
            });

            for (var i in n) {
                var r = n[i];
                jQuery(r.elem).css("font-size", r.fs * t / e + "px");
            }
        }

        function getFontSize() {

            jQuery.ajax({
                url: "/system/control/getFontSize", success: function (e) {
                    e = e || 100;
                    rwSetFontSize(pc, e);
                    pc = e;
                }
            });
        }

        jQuery('span#rwCtrlIncFont').on('click', function () {
            var t = 1 * pc + 5 * 1;
            rwSetFontSize(pc, t);
            jQuery.ajax({url: "/system/control/setFontSize/" + t});
            pc = t;
        });

        jQuery("span#rwCtrlDecFont").on("click", function () {
            var t = 1 * pc - 5 * 1;
            rwSetFontSize(pc, t);
            jQuery.ajax({url: "/system/control/setFontSize/" + t});
            pc = t;
        });

        jQuery("span#rwCtrlResFont").on("click", function () {
            rwResetFontSize();
            jQuery.ajax({url: "/system/control/setFontSize/" + pc});
        });

        getFontSize();
    })(jQuery);
});
