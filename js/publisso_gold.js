window.onerror = function () {
        return true;
};
(function (jQuery) {

        jQuery(document).ready(function () {
                
                //prevent form-submit during ajax
                jQuery(document).ajaxStart(function () {
                        jQuery('button[type="submit"]').attr('disabled', 'disabled');
                });
                
                jQuery(document).ajaxComplete(function () {
                        jQuery('button[type="submit"]').removeAttr('disabled');
                });
                

                
                jQuery('ul#conference-session-list li span').on('click', function () {
                        
                        var parent = this.parentElement;
                        
                        if (jQuery(parent).find('> ul').length) {
                                jQuery(parent).css('list-style', 'disclosure-' + (jQuery(parent).find('> .collapse.in').length ? 'closed' : 'open'));
                        }
                });

                function autosaveBookchapter(e, r, a) {
                        var o = jQuery("form#" + e + " :input").serializeArray(), n = {};
                        for (var t in o) {
                                var i = o[t];
                                n[i.name] = i.value;
                        }
                        if ("" != n.title) {
                                var u = jQuery('input[name="tmp_id"]');
                                jQuery.post("/autosave", {p1: n, p2: r}, function (e) {
                                        jQuery(u).val(e.data), a && (window.location.href = a);
                                }, "json");
                        } else a && (window.location.href = a);
                        return jQuery("form#" + e).length, !0;
                }
                
                function htmlEntities(e) {
                        return String(e).replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, "'");
                }
                
                function encode_utf8(e) {
                        return unescape(encodeURIComponent(e));
                }
                
                function Round(e, t) {
                        var n = Math.pow(10, t);
                        return Math.round(e * n) / n;
                }
                
                
                
                jQuery(document).on("openPDF", function (f, data) {
                        window.open(data.url, '_top');
                        return true;
                });
                
                jQuery(document).on("previewBookchapter", function (e, t) {
                        var form = btoa(encode_utf8(JSON.stringify(jQuery("#" + t.form).serializeArray()))), bk_id = t.bk_id, url = "/publisso_gold/submission/preview";
                        var o = document.createElement("form");
                        o.target = "_blank", o.method = "POST", o.action = url;
                        var r = document.createElement("input");
                        return r.type = "text", r.name = "data", r.value = form + "|" + t.data, o.appendChild(r), document.body.appendChild(o), o.submit(), document.body.removeChild(o), !0;
                });
                jQuery(document).on("previewBookchapterPDF", function (o, a) {
                        open("publisso_gold/export/raw/pdf");
                }), jQuery(document).on("saveBookchapter", function (o, a) {
                        autosaveBookchapter(a.form, a.data, "/publisso_gold/book/" + a.bk_id);
                }), jQuery(document).on("saveBookchapter1", function (o, a) {
                        autosaveBookchapter(a.form, a.data, "/publisso_gold/dashboard");
                }), jQuery(document).on("saveJournalarticle", function (o, a) {
                        console.log(a), autosaveBookchapter(a.form, a.data, "/publisso_gold/journal/" + a.jrn_id);
                }), jQuery(document).on("saveJournalarticle1", function (o, a) {
                        console.log(a), autosaveBookchapter(a.form, a.data, "/publisso_gold/dashboard");
                });

                jQuery(document).on("click", "span.toggle", function () {
                        jQuery(this).toggleClass("icon-arrow-up icon-arrow-down");
                });

                jQuery("div#nav li").each(function (i) {
                        if (jQuery(this).find("ul").length) {
                                var n = jQuery(this), r = jQuery(n).find("span#icon").first()[0];
                                jQuery(r).addClass("icon-arrow-down"), jQuery(r).addClass("toggle")
                        }
                });
                jQuery.fn.nthParent = function (n) {
                        for (var r = this, t = 0; t < n; t++) r = r.parent();
                        return r
                };
                jQuery.ajax({
                        url: "/system/ppAvailable", type: "GET", success: function (e) {
                                1 == e ? -1 != document.cookie.indexOf("cookie_agreed=1") ? jQuery("#cookie_agree").hide() : (jQuery("#cookie_agree").prependTo("body"), jQuery("#cookie_agree").show()) : jQuery("#cookie_agree").hide()
                        }
                });

                jQuery('input#dashboardGotoItem').on(
                        'keyup',
                        function (event) {
                                if (event.which == 13) { //Enter
                                        var item = jQuery('a[name="rwDashboardItem-' + this.value + '"]');
                                        var pos = item.offset().top;
                                        jQuery('html,body').animate({
                                                'scrollTop': pos
                                        }, 1000);
                                        
                                }
                        }
                );
                
                function printChapter() {
                        var e = document.getElementById("chapter-text").innerHTML,
                                t = document.getElementById("chapter-citationnote").innerHTML,
                                n = document.getElementById("chapter-license").innerHTML,
                                i = window.open("/system/siteSkel", "_blank");
                        i.onload = function () {
                                i.document.title = window.document.title, i.document.body.innerHTML = '<div class=""col-sm-9"><div id="chapter-content">' + e + "</div><hr>" + t + "<hr>" + n + "</div>", i.print(), i.close()
                        };
                }
                
                function UnCryptMailto(t) {
                        for (var n = 0, o = "", r = 0; r < t.length; r++) (n = t.charCodeAt(r)) >= 8364 && (n = 128), o += String.fromCharCode(n - 1);
                        return o
                }
                
                function linkTo_UnCryptMailto(t) {
                        location.href = UnCryptMailto(t);
                }
                
                function filterManagementMediumList(e, i) {
                        if (13 == e.keyCode) {
                                var a = jQuery(i).val();
                                window.location.search = "" != a ? "?filter=" + a : "";
                        }
                }
                
        });
})(jQuery);

jQuery(document).ready(function () {

        jQuery('.encmailadd').each(function () {
                var amail = jQuery(this);
                jQuery.post('/system/control/getEncData/' + jQuery(this).attr('data'), '', function (e) {
                        jQuery(amail).attr('href', 'mailto:' + e + jQuery(amail).attr('href'));
                });
        });

        (function (jQuery, Drupal) {
                Drupal.AjaxCommands.prototype.TimeoutHtmlCommand = function (ajax, response) {
                        
                        var timeout = response.args.timeout;
                        var html = response.args.html || "";
                        var target = jQuery(response.selector);
                        
                        setTimeout(function () {
                                jQuery(target).html(html);
                        }, timeout, target, html);
                };
                
                Drupal.AjaxCommands.prototype.OpenUrlCommand = function (ajax, response) {
                        
                        var url = response.args.url;
                        var target = response.args.target;
                        window.open(url, target);
                };
        })(jQuery, Drupal);
        
        jQuery.fn.modal.Constructor.prototype._enforceFocus = function () {
                var $modalElement = this.$element;
                jQuery(document).on('focusin.modal', function (e) {

                        if ($modalElement[0] !== e.target && !$modalElement.has(e.target).length && jQuery(e.target).parentsUntil('*[role="dialog"]').length === 0) {
                                $modalElement.focus();
                        }
                });
        };

        jQuery('textarea#ta-rwReferences').on('keyup change', function (event) {
                
                if ((event.type == 'keyup' && event.originalEvent.keyCode == 13) || event.type == 'change') {
                        r = this.value.split(/\r?\n/);
                        var e = 1;
                        t = "";
                        for (var i in r) {
                                var a = r[i].toString().replace(/^\[\d+\]/, "").replace(/\r?\n/, "").trim();
                                "" != a && (t += "[" + e++ + "] " + a + "\n");
                        }
                        
                        if (event.type == 'change') {
                                t = t.trim();
                        }
                        this.value = t;
                }
        });

        jQuery(document).on('wopen', function (e, data) {
                console.log('Open preview...');
                var winx = window.open(data.url, 'Preview', 'status=no,location=no,menubar=no,toolbar=no,search=no');
                return true;
        });

        function previewBookchapter(e, r) {
                console.log('Preview bookchapter');
                if (jQuery("form#" + r).length) {
                        var a = jQuery("form#" + r).find(":input:disabled").removeAttr("disabled");
                        var data = jQuery("form#" + r + " :input").serializeArray();
                        a.attr("disabled", "disabled");
                        var response = jQuery.post("/publisso_gold/control/store_data", data);
                        response.always(function (r) {
                                var preview = window.open(e, "_blank");
                        });
                }
                return !1;
        }
});
