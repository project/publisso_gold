<?php
        
        /**
         * Class haxe_Int32
         */
        class haxe_Int32 {
	public function __construct(){}
        
        /**
         * @param $a
         * @param $b
         * @return int
         */
        static function make($a, $b) {
		return $a << 16 | $b;
	}
        
        /**
         * @param $x
         * @return int
         */
        static function ofInt($x) {
		return $x | 0;
	}
        
        /**
         * @param $x
         * @return int
         */
        static function clamp($x) {
		return $x | 0;
	}
        
        /**
         * @param $x
         * @return int
         * @throws HException
         */
        static function toInt($x) {
		if(($x >> 30 & 1) !== _hx_shift_right($x, 31)) {
			throw new HException("Overflow " . Std::string($x));
		}
		return $x & -1;
	}
        
        /**
         * @param $x
         * @return mixed
         */
        static function toNativeInt($x) {
		return $x;
	}
        
        /**
         * @param $a
         * @param $b
         * @return int
         */
        static function add($a, $b) {
		return $a + $b | 0;
	}
        
        /**
         * @param $a
         * @param $b
         * @return int
         */
        static function sub($a, $b) {
		return $a - $b | 0;
	}
        
        /**
         * @param $a
         * @param $b
         * @return int
         */
        static function mul($a, $b) {
		return $a * ($b & 65535) + ($a * (_hx_shift_right($b, 16)) << 16 | 0) | 0;
	}
        
        /**
         * @param $a
         * @param $b
         * @return int
         */
        static function div($a, $b) {
		return intval($a / $b);
	}
        
        /**
         * @param $a
         * @param $b
         * @return float|int
         */
        static function mod($a, $b) {
		return _hx_mod($a, $b);
	}
        
        /**
         * @param $a
         * @param $b
         * @return int
         */
        static function shl($a, $b) {
		return $a << $b;
	}
        
        /**
         * @param $a
         * @param $b
         * @return int
         */
        static function shr($a, $b) {
		return $a >> $b;
	}
        
        /**
         * @param $a
         * @param $b
         * @return int
         */
        static function ushr($a, $b) {
		return _hx_shift_right($a, $b);
	}
        
        /**
         * @param $a
         * @param $b
         * @return int
         */
        static function hand($a, $b) {
		return $a & $b;
	}
        
        /**
         * @param $a
         * @param $b
         * @return int
         */
        static function hor($a, $b) {
		return $a | $b;
	}
        
        /**
         * @param $a
         * @param $b
         * @return int
         */
        static function hxor($a, $b) {
		return $a ^ $b;
	}
        
        /**
         * @param $a
         * @return int
         */
        static function neg($a) {
		return -$a;
	}
        
        /**
         * @param $a
         * @return bool
         */
        static function isNeg($a) {
		return $a < 0;
	}
        
        /**
         * @param $a
         * @return bool
         */
        static function isZero($a) {
		return $a === 0;
	}
        
        /**
         * @param $a
         * @return bool
         */
        static function complement($a) {
		return ~$a;
	}
        
        /**
         * @param $a
         * @param $b
         * @return mixed
         */
        static function compare($a, $b) {
		return $a - $b;
	}
        
        /**
         * @param $a
         * @param $b
         * @return bool|int
         */
        static function ucompare($a, $b) {
		if($a < 0) {
			return haxe_Int32_0($a, $b);
		}
		return haxe_Int32_1($a, $b);
	}
        
        /**
         * @return string
         */
        function __toString() { return 'haxe.Int32'; }
}
        
        /**
         * @param $a
         * @param $b
         * @return bool|int
         */
        function haxe_Int32_0(&$a, &$b) {
	if($b < 0) {
		return ~$b - ~$a;
	} else {
		return 1;
	}
}
        
        /**
         * @param $a
         * @param $b
         * @return int
         */
        function haxe_Int32_1(&$a, &$b) {
	if($b < 0) {
		return -1;
	} else {
		return $a - $b;
	}
}
