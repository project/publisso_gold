<?php
        
        /**
         * Class haxe_io_Eof
         */
        class haxe_io_Eof {
	public function __construct() { 
	}
        
        /**
         * @return string
         */
        public function toString() {
		return "Eof";
	}
        
        /**
         * @return string
         */
        function __toString() { return $this->toString(); }
}
