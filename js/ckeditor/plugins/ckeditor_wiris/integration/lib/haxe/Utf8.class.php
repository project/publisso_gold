<?php
        
        /**
         * Class haxe_Utf8
         */
        class haxe_Utf8 {
        /**
         * haxe_Utf8 constructor.
         * @param null $size
         */
        public function __construct($size = null) {
		if(!php_Boot::$skip_constructor) {
		$this->__b = "";
	}}
        
        /**
         * @return string
         */
        public function toString() {
		return $this->__b;
	}
        
        /**
         * @param $c
         */
        public function addChar($c) {
		$this->__b .= haxe_Utf8::uchr($c);
	}
	public $__b;
        
        /**
         * @param $m
         * @param $a
         * @return mixed|string
         * @throws HException
         */
        public function __call($m, $a) {
		if(isset($this->$m) && is_callable($this->$m))
			return call_user_func_array($this->$m, $a);
		else if(isset($this->�dynamics[$m]) && is_callable($this->�dynamics[$m]))
			return call_user_func_array($this->�dynamics[$m], $a);
		else if('toString' == $m)
			return $this->__toString();
		else
			throw new HException('Unable to call �'.$m.'�');
	}
        
        /**
         * @param $s
         * @return false|string
         */
        static function encode($s) {
		return utf8_encode($s);
	}
        
        /**
         * @param $s
         * @return false|string
         */
        static function decode($s) {
		return utf8_decode($s);
	}
        
        /**
         * @param $s
         * @param $chars
         */
        static function iter($s, $chars) {
		$len = haxe_Utf8::length($s);
		{
			$_g = 0;
			while($_g < $len) {
				$i = $_g++;
				call_user_func_array($chars, array(haxe_Utf8::charCodeAt($s, $i)));
				unset($i);
			}
		}
	}
        
        /**
         * @param $s
         * @param $index
         * @return mixed
         */
        static function charCodeAt($s, $index) {
		return haxe_Utf8::uord(haxe_Utf8::sub($s, $index, 1));
	}
        
        /**
         * @param $i
         * @return false|string|string[]|null
         */
        static function uchr($i) {
		return mb_convert_encoding(pack('N',$i), 'UTF-8', 'UCS-4BE');
	}
        
        /**
         * @param $s
         * @return mixed
         */
        static function uord($s) {
		$c = unpack('N', mb_convert_encoding($s, 'UCS-4BE', 'UTF-8'));
		return $c[1];
	}
        
        /**
         * @param $s
         * @return bool
         */
        static function validate($s) {
		return mb_check_encoding($s, "UTF-8");
	}
        
        /**
         * @param $s
         * @return false|int
         */
        static function length($s) {
		return mb_strlen($s, "UTF-8");
	}
        
        /**
         * @param $a
         * @param $b
         * @return int|lt
         */
        static function compare($a, $b) {
		return strcmp($a, $b);
	}
        
        /**
         * @param $s
         * @param $pos
         * @param $len
         * @return string
         */
        static function sub($s, $pos, $len) {
		return mb_substr($s, $pos, $len, "UTF-8");
	}
	static $enc = "UTF-8";
        
        /**
         * @return string
         */
        function __toString() { return $this->toString(); }
}
