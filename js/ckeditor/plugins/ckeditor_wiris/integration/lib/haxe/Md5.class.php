<?php
        
        /**
         * Class haxe_Md5
         */
        class haxe_Md5 {
	public function __construct(){}
        
        /**
         * @param $s
         * @return string
         */
        static function encode($s) {
		return md5($s);
	}
        
        /**
         * @return string
         */
        function __toString() { return 'haxe.Md5'; }
}
