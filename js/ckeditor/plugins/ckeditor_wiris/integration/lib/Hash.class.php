<?php
        
        /**
         * Class Hash
         */
        class Hash implements IteratorAggregate{
	public function __construct() {
		if(!php_Boot::$skip_constructor) {
		$this->h = array();
	}}
        
        /**
         * @return _hx_array_iterator|Traversable
         */
        public function getIterator() {
		return $this->iterator();
	}
        
        /**
         * @return string
         */
        public function toString() {
		$s = "{";
		$it = $this->keys();
		$�it = $it;
		while($�it->hasNext()) {
			$i = $�it->next();
			$s .= $i;
			$s .= " => ";
			$s .= Std::string($this->get($i));
			if($it->hasNext()) {
				$s .= ", ";
			}
		}
		return $s . "}";
	}
        
        /**
         * @return _hx_array_iterator
         */
        public function iterator() {
		return new _hx_array_iterator(array_values($this->h));
	}
        
        /**
         * @return _hx_array_iterator
         */
        public function keys() {
		return new _hx_array_iterator(array_keys($this->h));
	}
        
        /**
         * @param $key
         * @return bool
         */
        public function remove($key) {
		if(array_key_exists($key, $this->h)) {
			unset($this->h[$key]);
			return true;
		} else {
			return false;
		}
	}
        
        /**
         * @param $key
         * @return bool
         */
        public function exists($key) {
		return array_key_exists($key, $this->h);
	}
        
        /**
         * @param $key
         * @return mixed|null
         */
        public function get($key) {
		if(array_key_exists($key, $this->h)) {
			return $this->h[$key];
		} else {
			return null;
		}
	}
        
        /**
         * @param $key
         * @param $value
         */
        public function set($key, $value) {
		$this->h[$key] = $value;
	}
	public $h;
        
        /**
         * @param $m
         * @param $a
         * @return mixed|string
         * @throws HException
         */
        public function __call($m, $a) {
		if(isset($this->$m) && is_callable($this->$m))
			return call_user_func_array($this->$m, $a);
		else if(isset($this->�dynamics[$m]) && is_callable($this->�dynamics[$m]))
			return call_user_func_array($this->�dynamics[$m], $a);
		else if('toString' == $m)
			return $this->__toString();
		else
			throw new HException('Unable to call �'.$m.'�');
	}
        
        /**
         * @return string
         */
        function __toString() { return $this->toString(); }
}
