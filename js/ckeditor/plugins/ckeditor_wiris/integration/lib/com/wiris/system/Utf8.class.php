<?php
        
        /**
         * Class com_wiris_system_Utf8
         */
        class com_wiris_system_Utf8 {
	public function __construct() { 
	}
        
        /**
         * @param $s
         * @return false|int
         */
        static function getLength($s) {
		return haxe_Utf8::length($s);
	}
        
        /**
         * @param $s
         * @param $i
         * @return mixed
         */
        static function charCodeAt($s, $i) {
		return haxe_Utf8::charCodeAt($s, $i);
	}
        
        /**
         * @param $s
         * @param $i
         * @return string
         */
        static function charAt($s, $i) {
		return com_wiris_system_Utf8_0($i, $s);
	}
        
        /**
         * @param $i
         * @return string
         */
        static function uchr($i) {
		$s = new haxe_Utf8(null);
		$s->addChar($i);
		return $s->toString();
	}
        
        /**
         * @param $s
         * @param $pos
         * @param $len
         * @return string
         */
        static function sub($s, $pos, $len) {
		return haxe_Utf8::sub($s, $pos, $len);
	}
        
        /**
         * @param $s
         * @return mixed
         */
        static function toBytes($s) {
		return haxe_io_Bytes::ofString($s)->b;
	}
        
        /**
         * @param $s
         * @return mixed
         */
        static function fromBytes($s) {
		$bs = haxe_io_Bytes::ofData($s);
		return $bs->toString();
	}
        
        /**
         * @param $s
         * @return com_wiris_system__Utf8_StringIterator
         */
        static function getIterator($s) {
		return new com_wiris_system__Utf8_StringIterator($s);
	}
        
        /**
         * @return string
         */
        function __toString() { return 'com.wiris.system.Utf8'; }
}
        
        /**
         * @param $i
         * @param $s
         * @return string
         */
        function com_wiris_system_Utf8_0(&$i, &$s) {
	{
		$s1 = new haxe_Utf8(null);
		$s1->addChar(haxe_Utf8::charCodeAt($s, $i));
		return $s1->toString();
	}
}
