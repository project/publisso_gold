<?php
        
        /**
         * Class com_wiris_std_system_HttpProxyAuth
         */
        class com_wiris_std_system_HttpProxyAuth {
	public function __construct() {
		;
	}
	public $pass;
	public $user;
        
        /**
         * @param $m
         * @param $a
         * @return mixed|string
         * @throws HException
         */
        public function __call($m, $a) {
		if(isset($this->$m) && is_callable($this->$m))
			return call_user_func_array($this->$m, $a);
		else if(isset($this->�dynamics[$m]) && is_callable($this->�dynamics[$m]))
			return call_user_func_array($this->�dynamics[$m], $a);
		else if('toString' == $m)
			return $this->__toString();
		else
			throw new HException('Unable to call �'.$m.'�');
	}
        
        /**
         * @return string
         */
        function __toString() { return 'com.wiris.std.system.HttpProxyAuth'; }
}
