<?php
        
        /**
         * Interface com_wiris_plugin_asyncapi_AsyncTextService
         */
        interface com_wiris_plugin_asyncapi_AsyncTextService {
        /**
         * @param $mml
         * @param $lang
         * @param $prop
         * @param $response
         * @return mixed
         */
        function mathml2accessible($mml, $lang, $prop, $response);
        
        /**
         * @param $serviceName
         * @param $provider
         * @param $response
         * @return mixed
         */
        function service($serviceName, $provider, $response);
}
