<?php
        
        /**
         * Class com_wiris_system_StringEx
         */
        class com_wiris_system_StringEx {
	public function __construct(){}
        
        /**
         * @param $s
         * @param $start
         * @param null $end
         * @return string
         */
        static function substring($s, $start, $end = null) {
		if($end === null) {
			return _hx_substr($s, $start, null);
		}
		return _hx_substr($s, $start, $end - $start);
	}
        
        /**
         * @param $s1
         * @param $s2
         * @return int
         */
        static function compareTo($s1, $s2) {
		if($s1 > $s2) {
			return 1;
		}
		if($s1 < $s2) {
			return -1;
		}
		return 0;
	}
        
        /**
         * @return string
         */
        function __toString() { return 'com.wiris.system.StringEx'; }
}
