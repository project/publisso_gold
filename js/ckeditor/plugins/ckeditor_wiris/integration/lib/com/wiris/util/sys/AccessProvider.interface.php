<?php
        
        /**
         * Interface com_wiris_util_sys_AccessProvider
         */
        interface com_wiris_util_sys_AccessProvider {
	function isEnabled();
	function requireAccess();
}
