<?php
        
        /**
         * Interface com_wiris_util_type_Comparator
         */
        interface com_wiris_util_type_Comparator {
        /**
         * @param $a
         * @param $b
         * @return mixed
         */
        function compare($a, $b);
}
