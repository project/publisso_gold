<?php
        
        /**
         * Interface com_wiris_plugin_configuration_ConfigurationUpdater
         */
        interface com_wiris_plugin_configuration_ConfigurationUpdater {
        /**
         * @param $configuration
         * @return mixed
         */
        function updateConfiguration(&$configuration);
        
        /**
         * @param $obj
         * @return mixed
         */
        function init($obj);
}
