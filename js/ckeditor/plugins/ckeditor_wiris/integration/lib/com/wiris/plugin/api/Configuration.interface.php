<?php
        
        /**
         * Interface com_wiris_plugin_api_Configuration
         */
        interface com_wiris_plugin_api_Configuration {
        /**
         * @param $configurationKeys
         * @param $configurationValues
         * @return mixed
         */
        function setConfigurations($configurationKeys, $configurationValues);
        
        /**
         * @param $configurationKeys
         * @return mixed
         */
        function getJsonConfiguration($configurationKeys);
        
        /**
         * @param $context
         * @return mixed
         */
        function setInitObject($context);
        
        /**
         * @param $name
         * @param $value
         * @return mixed
         */
        function setProperty($name, $value);
        
        /**
         * @param $name
         * @param $dflt
         * @return mixed
         */
        function getProperty($name, $dflt);
	function getJavaScriptConfigurationJson();
	function getFullConfiguration();
}
