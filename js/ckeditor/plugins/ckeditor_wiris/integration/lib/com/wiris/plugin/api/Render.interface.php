<?php
        
        /**
         * Interface com_wiris_plugin_api_Render
         */
        interface com_wiris_plugin_api_Render {
        /**
         * @param $mml
         * @param $param
         * @return mixed
         */
        function computeDigest($mml, $param);
        
        /**
         * @param $digest
         * @return mixed
         */
        function getMathml($digest);
        
        /**
         * @param $digest
         * @param $lang
         * @return mixed
         */
        function showImageHash($digest, $lang);
        
        /**
         * @param $digest
         * @param $lang
         * @return mixed
         */
        function showImageJson($digest, $lang);
        
        /**
         * @param $digest
         * @param $mml
         * @param $provider
         * @return mixed
         */
        function showImage($digest, $mml, $provider);
        
        /**
         * @param $mml
         * @param $param
         * @param $output
         * @return mixed
         */
        function createImage($mml, $param, &$output);
}
