<?php
        
        /**
         * Interface com_wiris_plugin_impl_HttpListener
         */
        interface com_wiris_plugin_impl_HttpListener {
        /**
         * @param $msg
         * @return mixed
         */
        function onError($msg);
        
        /**
         * @param $data
         * @return mixed
         */
        function onData($data);
}
