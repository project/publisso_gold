<?php
        
        /**
         * Class com_wiris_plugin_asyncimpl_HttpPostAndContinue
         */
        class com_wiris_plugin_asyncimpl_HttpPostAndContinue implements com_wiris_plugin_impl_HttpListener{
        /**
         * com_wiris_plugin_asyncimpl_HttpPostAndContinue constructor.
         * @param $h
         * @param $obj
         * @param $methodName
         * @throws HException
         */
        public function __construct($h, $obj, $methodName) {
		if(!php_Boot::$skip_constructor) {
		$this->h = $h;
		$this->obj = $obj;
		$this->method = Reflect::field($obj, $methodName);
		if(_hx_field($this, "method") === null) {
			throw new HException("Method not found: " . $methodName);
		}
		$h->setListener($this);
	}}
        
        /**
         * @param $msg
         * @return mixed|void
         * @throws HException
         */
        public function onError($msg) {
		throw new HException($msg);
	}
        
        /**
         * @param $data
         * @return mixed|void
         */
        public function onData($data) {
		$args = new _hx_array(array());
		$args->push($data);
		Reflect::callMethod($this->obj, $this->method, $args);
	}
	public function post() {
		$this->h->request(true);
	}
	public $h;
	public $method;
	public $obj;
        
        /**
         * @param $m
         * @param $a
         * @return mixed|string
         * @throws HException
         */
        public function __call($m, $a) {
		if(isset($this->$m) && is_callable($this->$m))
			return call_user_func_array($this->$m, $a);
		else if(isset($this->�dynamics[$m]) && is_callable($this->�dynamics[$m]))
			return call_user_func_array($this->�dynamics[$m], $a);
		else if('toString' == $m)
			return $this->__toString();
		else
			throw new HException('Unable to call �'.$m.'�');
	}
        
        /**
         * @param $h
         * @param $obj
         * @param $methodName
         * @throws HException
         */
        static function doPost($h, $obj, $methodName) {
		_hx_deref(new com_wiris_plugin_asyncimpl_HttpPostAndContinue($h, $obj, $methodName))->post();
	}
        
        /**
         * @return string
         */
        function __toString() { return 'com.wiris.plugin.asyncimpl.HttpPostAndContinue'; }
}
