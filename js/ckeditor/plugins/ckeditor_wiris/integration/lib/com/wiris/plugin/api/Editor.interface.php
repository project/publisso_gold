<?php
        
        /**
         * Interface com_wiris_plugin_api_Editor
         */
        interface com_wiris_plugin_api_Editor {
        /**
         * @param $language
         * @param $provider
         * @return mixed
         */
        function editor($language, $provider);
}
