<?php
        
        /**
         * Interface com_wiris_plugin_asyncapi_BytesCallback
         */
        interface com_wiris_plugin_asyncapi_BytesCallback {
        /**
         * @param $msg
         * @return mixed
         */
        function error($msg);
        
        /**
         * @param $bs
         * @return mixed
         */
        function returnBytes($bs);
}
