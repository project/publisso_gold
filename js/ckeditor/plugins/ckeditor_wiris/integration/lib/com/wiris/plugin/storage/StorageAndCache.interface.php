<?php
        
        /**
         * Interface com_wiris_plugin_storage_StorageAndCache
         */
        interface com_wiris_plugin_storage_StorageAndCache {
	function deleteCache();
        
        /**
         * @param $digest
         * @param $service
         * @param $stream
         * @return mixed
         */
        function storeData($digest, $service, $stream);
        
        /**
         * @param $digest
         * @param $service
         * @return mixed
         */
        function retreiveData($digest, $service);
        
        /**
         * @param $digest
         * @return mixed
         */
        function decodeDigest($digest);
        
        /**
         * @param $content
         * @return mixed
         */
        function codeDigest($content);
        
        /**
         * @param $obj
         * @param $config
         * @param $cache
         * @param $cacheFormula
         * @return mixed
         */
        function init($obj, $config, $cache, $cacheFormula);
}
