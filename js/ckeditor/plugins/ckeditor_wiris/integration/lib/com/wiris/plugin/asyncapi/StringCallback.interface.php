<?php
        
        /**
         * Interface com_wiris_plugin_asyncapi_StringCallback
         */
        interface com_wiris_plugin_asyncapi_StringCallback {
        /**
         * @param $msg
         * @return mixed
         */
        function error($msg);
        
        /**
         * @param $str
         * @return mixed
         */
        function returnString($str);
}
