<?php
        
        /**
         * Class com_wiris_system_PropertiesTools
         */
        class com_wiris_system_PropertiesTools {
	public function __construct(){}
        
        /**
         * @param $s
         * @return null
         */
        static function getSystemProperty($s) {
		return null;
	}
        
        /**
         * @param $prop
         * @param $key
         * @param null $dflt
         * @return mixed|null
         */
        static function getProperty($prop, $key, $dflt = null) {
		if(isset($prop[$key])) {
			return $prop[$key];
		}
		return $dflt;
	}
        
        /**
         * @return array
         */
        static function newProperties() {
		return array();;
	}
        
        /**
         * @param $prop
         * @param $key
         * @param $value
         */
        static function setProperty($prop, $key, $value) {
		$prop[$key] = $value;
	}
        
        /**
         * @param $prop
         * @return Hash
         */
        static function fromProperties($prop) {
		$ht = new Hash();
		$key = "";
		$value = "";
		foreach ($prop as $key => $value) {
		$ht->set($key, $value);
		}
		return $ht;
	}
        
        /**
         * @param $h
         * @return array
         */
        static function toProperties($h) {
		$np = array();;
		$ks = $h->keys();
		while($ks->hasNext()) {
			$k = $ks->next();
			$np[$k] = $h->get($k);
			unset($k);
		}
		return $np;
	}
        
        /**
         * @return string
         */
        function __toString() { return 'com.wiris.system.PropertiesTools'; }
}
