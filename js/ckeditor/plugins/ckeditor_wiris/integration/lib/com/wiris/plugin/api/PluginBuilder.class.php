<?php
        
        /**
         * Class com_wiris_plugin_api_PluginBuilder
         */
        class com_wiris_plugin_api_PluginBuilder {
	public function __construct() { 
	}
        
        /**
         * @return null
         */
        public function getAccessProvider() {
		return null;
	}
        
        /**
         * @param $provider
         */
        public function setAccessProvider($provider) {
	}
        
        /**
         * @return null
         */
        public function getCustomParamsProvider() {
		return null;
	}
        
        /**
         * @param $provider
         */
        public function setCustomParamsProvider($provider) {
	}
        
        /**
         * @param $properties
         * @return null
         */
        public function newGenericParamsProvider($properties) {
		return null;
	}
        
        /**
         * @return null
         */
        public function getImageFormatController() {
		return null;
	}
        
        /**
         * @return false
         */
        public function isEditorLicensed() {
		return false;
	}
        
        /**
         * @return null
         */
        public function newResourceLoader() {
		return null;
	}
        
        /**
         * @return null
         */
        public function newCleanCache() {
		return null;
	}
        
        /**
         * @param $response
         * @param $origin
         */
        public function addCorsHeaders($response, $origin) {
	}
        
        /**
         * @return null
         */
        public function newEditor() {
		return null;
	}
        
        /**
         * @return null
         */
        public function newCas() {
		return null;
	}
        
        /**
         * @return null
         */
        public function newTest() {
		return null;
	}
        
        /**
         * @param $cache
         */
        public function setStorageAndCacheCacheFormulaObject($cache) {
	}
        
        /**
         * @param $cache
         */
        public function setStorageAndCacheCacheObject($cache) {
	}
        
        /**
         * @param $obj
         */
        public function setStorageAndCacheInitObject($obj) {
	}
        
        /**
         * @return null
         */
        public function getStorageAndCache() {
		return null;
	}
        
        /**
         * @return null
         */
        public function getConfiguration() {
		return null;
	}
        
        /**
         * @return null
         */
        public function newAsyncTextService() {
		return null;
	}
        
        /**
         * @return null
         */
        public function newTextService() {
		return null;
	}
        
        /**
         * @return null
         */
        public function newAsyncRender() {
		return null;
	}
        
        /**
         * @return null
         */
        public function newRender() {
		return null;
	}
        
        /**
         * @param $store
         */
        public function setStorageAndCache($store) {
	}
        
        /**
         * @param $conf
         */
        public function addConfigurationUpdater($conf) {
	}
	static $pb = null;
        
        /**
         * @return null
         */
        static function getInstance() {
		if(com_wiris_plugin_api_PluginBuilder::$pb === null) {
			com_wiris_plugin_api_PluginBuilder::$pb = new com_wiris_plugin_impl_PluginBuilderImpl();
		}
		return com_wiris_plugin_api_PluginBuilder::$pb;
	}
        
        /**
         * @return com_wiris_plugin_impl_PluginBuilderImpl
         */
        static function newInstance() {
		return new com_wiris_plugin_impl_PluginBuilderImpl();
	}
        
        /**
         * @return string
         */
        function __toString() { return 'com.wiris.plugin.api.PluginBuilder'; }
}
