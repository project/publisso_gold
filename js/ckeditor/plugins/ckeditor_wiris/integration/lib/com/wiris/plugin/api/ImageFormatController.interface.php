<?php
        
        /**
         * Interface com_wiris_plugin_api_ImageFormatController
         */
        interface com_wiris_plugin_api_ImageFormatController {
        /**
         * @param $dpi
         * @param $metrics
         * @return mixed
         */
        function scalateMetrics($dpi, $metrics);
        
        /**
         * @param $bytes
         * @param $output
         * @return mixed
         */
        function getMetrics($bytes, &$output);
	function getContentType();
}
