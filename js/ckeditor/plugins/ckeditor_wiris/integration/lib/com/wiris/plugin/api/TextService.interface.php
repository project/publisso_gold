<?php
        
        /**
         * Interface com_wiris_plugin_api_TextService
         */
        interface com_wiris_plugin_api_TextService {
        /**
         * @param $str
         * @param $prop
         * @return mixed
         */
        function filter($str, $prop);
        
        /**
         * @param $digest
         * @param $latex
         * @return mixed
         */
        function getMathML($digest, $latex);
        
        /**
         * @param $mml
         * @return mixed
         */
        function latex2mathml($mml);
        
        /**
         * @param $mml
         * @return mixed
         */
        function mathml2latex($mml);
        
        /**
         * @param $mml
         * @param $lang
         * @param $prop
         * @return mixed
         */
        function mathml2accessible($mml, $lang, $prop);
        
        /**
         * @param $serviceName
         * @param $provider
         * @return mixed
         */
        function service($serviceName, $provider);
}
