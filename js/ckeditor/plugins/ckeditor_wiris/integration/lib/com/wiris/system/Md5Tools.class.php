<?php
        
        /**
         * Class com_wiris_system_Md5Tools
         */
        class com_wiris_system_Md5Tools {
	public function __construct(){}
        
        /**
         * @param $content
         * @return string
         */
        static function encodeString($content) {
		return haxe_Md5::encode($content);
	}
        
        /**
         * @param $content
         * @return string
         */
        static function encodeBytes($content) {
		return haxe_Md5::encode($content->toString());
	}
        
        /**
         * @return string
         */
        function __toString() { return 'com.wiris.system.Md5Tools'; }
}
