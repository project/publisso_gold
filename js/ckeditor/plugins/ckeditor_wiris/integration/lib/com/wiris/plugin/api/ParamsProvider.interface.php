<?php
        
        /**
         * Interface com_wiris_plugin_api_ParamsProvider
         */
        interface com_wiris_plugin_api_ParamsProvider {
	function getServiceParameters();
        
        /**
         * @param $configuration
         * @return mixed
         */
        function getRenderParameters($configuration);
	function getParameters();
        
        /**
         * @param $param
         * @return mixed
         */
        function getRequiredParameter($param);
        
        /**
         * @param $param
         * @param $dflt
         * @return mixed
         */
        function getParameter($param, $dflt);
}
