<?php
        
        /**
         * Interface com_wiris_util_sys_Cache
         */
        interface com_wiris_util_sys_Cache {
        /**
         * @param $key
         * @return mixed
         */
        function delete($key);
	function deleteAll();
        
        /**
         * @param $key
         * @return mixed
         */
        function get($key);
        
        /**
         * @param $key
         * @param $value
         * @return mixed
         */
        function set($key, $value);
}
