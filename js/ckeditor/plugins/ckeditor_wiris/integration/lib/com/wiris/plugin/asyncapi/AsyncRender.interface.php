<?php
        
        /**
         * Interface com_wiris_plugin_asyncapi_AsyncRender
         */
        interface com_wiris_plugin_asyncapi_AsyncRender {
        /**
         * @param $digest
         * @param $call
         * @return mixed
         */
        function getMathml($digest, $call);
        
        /**
         * @param $digest
         * @param $mml
         * @param $param
         * @param $call
         * @return mixed
         */
        function showImage($digest, $mml, $param, $call);
        
        /**
         * @param $mml
         * @param $param
         * @param $output
         * @param $call
         * @return mixed
         */
        function createImage($mml, $param, &$output, $call);
}
