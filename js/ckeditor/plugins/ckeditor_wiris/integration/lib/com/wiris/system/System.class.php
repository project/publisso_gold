<?php
        
        /**
         * Class com_wiris_system_System
         */
        class com_wiris_system_System {
	public function __construct(){}
        
        /**
         * @param $src
         * @param $srcPos
         * @param $dest
         * @param $destPos
         * @param $n
         */
        static function arraycopy($src, $srcPos, $dest, $destPos, $n) {
		$_g = 0;
		while($_g < $n) {
			$i = $_g++;
			$dest[$destPos + $i] = $src[$srcPos + $i];
			unset($i);
		}
	}
        
        /**
         * @return string
         */
        function __toString() { return 'com.wiris.system.System'; }
}
