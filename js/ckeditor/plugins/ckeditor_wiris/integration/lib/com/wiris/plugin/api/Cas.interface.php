<?php
        
        /**
         * Interface com_wiris_plugin_api_Cas
         */
        interface com_wiris_plugin_api_Cas {
        /**
         * @param $mode
         * @param $language
         * @return mixed
         */
        function cas($mode, $language);
        
        /**
         * @param $imageParameter
         * @return mixed
         */
        function createCasImage($imageParameter);
        
        /**
         * @param $formula
         * @param $provider
         * @return mixed
         */
        function showCasImage($formula, $provider);
}
