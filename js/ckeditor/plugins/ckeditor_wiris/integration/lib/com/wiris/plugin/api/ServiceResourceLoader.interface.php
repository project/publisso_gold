<?php
        
        /**
         * Interface com_wiris_plugin_api_ServiceResourceLoader
         */
        interface com_wiris_plugin_api_ServiceResourceLoader {
        /**
         * @param $name
         * @return mixed
         */
        function getContentType($name);
        
        /**
         * @param $resource
         * @return mixed
         */
        function getContent($resource);
}
