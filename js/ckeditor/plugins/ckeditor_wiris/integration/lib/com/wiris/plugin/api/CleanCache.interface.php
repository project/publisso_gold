<?php
        
        /**
         * Interface com_wiris_plugin_api_CleanCache
         */
        interface com_wiris_plugin_api_CleanCache {
	function getContentType();
	function getCacheOutput();
        
        /**
         * @param $provider
         * @return mixed
         */
        function init($provider);
}
