<?php
        
        /**
         * Class Std
         */
        class Std {
	public function __construct(){}
        
        /**
         * @param $v
         * @param $t
         * @return bool
         */
        static function is($v, $t) {
		return _hx_instanceof($v, $t);
	}
        
        /**
         * @param $s
         * @return mixed|string
         */
        static function string($s) {
		return _hx_string_rec($s, "");
	}
        
        /**
         * @param $x
         * @return int
         */
        static function int($x) {
		return intval($x);
	}
        
        /**
         * @param $x
         * @return int|null
         */
        static function parseInt($x) {
		$x = ltrim($x);
		$firstCharIndex = ((_hx_char_at($x, 0) === "-") ? 1 : 0);
		$firstCharCode = _hx_char_code_at($x, $firstCharIndex);
		if(!($firstCharCode !== null && $firstCharCode >= 48 && $firstCharCode <= 57)) {
			return null;
		}
		$secondChar = _hx_char_at($x, $firstCharIndex + 1);
		if($secondChar === "x" || $secondChar === "X") {
			return intval($x, 0);
		} else {
			return intval($x, 10);
		}
	}
        
        /**
         * @param $x
         * @return float
         */
        static function parseFloat($x) {
		$result = floatval($x);
		if($result != 0) {
			return $result;
		}
		$x = ltrim($x);
		$firstCharIndex = ((_hx_char_at($x, 0) === "-") ? 1 : 0);
		$charCode = _hx_char_code_at($x, $firstCharIndex);
		if($charCode === 46) {
			$charCode = _hx_char_code_at($x, $firstCharIndex + 1);
		}
		if($charCode !== null && $charCode >= 48 && $charCode <= 57) {
			return 0.0;
		} else {
			return Math::$NaN;
		}
	}
        
        /**
         * @param $x
         * @return int
         */
        static function random($x) {
		return mt_rand(0, $x - 1);
	}
        
        /**
         * @param $charCode
         * @return bool
         */
        static function isDigitCode($charCode) {
		return $charCode !== null && $charCode >= 48 && $charCode <= 57;
	}
        
        /**
         * @return string
         */
        function __toString() { return 'Std'; }
}
