<?php
        
        /**
         * Class Sys
         */
        class Sys {
	public function __construct(){}
        
        /**
         * @param $v
         */
        static function hprint($v) {
		echo(Std::string($v));
	}
        
        /**
         * @param $v
         */
        static function println($v) {
		Sys::hprint($v);
		Sys::hprint("\x0A");
	}
        
        /**
         * @return _hx_array
         */
        static function args() {
		return ((array_key_exists("argv", $_SERVER)) ? new _hx_array(array_slice($_SERVER["argv"], 1)) : new _hx_array(array()));
	}
        
        /**
         * @param $s
         * @return array|false|string
         */
        static function getEnv($s) {
		return getenv($s);
	}
        
        /**
         * @param $s
         * @param $v
         */
        static function putEnv($s, $v) {
		putenv($s . "=" . $v);
		return;
	}
        
        /**
         * @param $seconds
         */
        static function sleep($seconds) {
		usleep($seconds * 1000000);
		return;
	}
        
        /**
         * @param $loc
         * @return bool
         */
        static function setTimeLocale($loc) {
		return setlocale(LC_TIME, $loc) !== false;
	}
        
        /**
         * @return string
         */
        static function getCwd() {
		$cwd = getcwd();
		$l = _hx_substr($cwd, -1, null);
		return $cwd . ((($l === "/" || $l === "\\") ? "" : "/"));
	}
        
        /**
         * @param $s
         */
        static function setCwd($s) {
		chdir($s);
	}
        
        /**
         * @return string
         */
        static function systemName() {
		$s = php_uname("s");
		$p = null;
		if(($p = _hx_index_of($s, " ", null)) >= 0) {
			return _hx_substr($s, 0, $p);
		} else {
			return $s;
		}
	}
        
        /**
         * @param $arg
         * @return string
         */
        static function escapeArgument($arg) {
		$ok = true;
		{
			$_g1 = 0; $_g = strlen($arg);
			while($_g1 < $_g) {
				$i = $_g1++;
				switch(_hx_char_code_at($arg, $i)) {
				case 32:case 34:{
					$ok = false;
				}break;
				case 0:case 13:case 10:{
					$arg = _hx_substr($arg, 0, $i);
				}break;
				}
				unset($i);
			}
		}
		if($ok) {
			return $arg;
		}
		return "\"" . _hx_explode("\"", $arg)->join("\\\"") . "\"";
	}
        
        /**
         * @param $cmd
         * @param null $args
         * @return int
         */
        static function command($cmd, $args = null) {
		if($args !== null) {
			$cmd = Sys::escapeArgument($cmd);
			{
				$_g = 0;
				while($_g < $args->length) {
					$a = $args[$_g];
					++$_g;
					$cmd .= " " . Sys::escapeArgument($a);
					unset($a);
				}
			}
		}
		$result = 0;
		system($cmd, $result);
		return $result;
	}
        
        /**
         * @param $code
         */
        static function hexit($code) {
		exit($code);
	}
        
        /**
         * @return float|string
         */
        static function time() {
		return microtime(true);
	}
        
        /**
         * @return float|mixed|string
         */
        static function cpuTime() {
		return microtime(true) - $_SERVER['REQUEST_TIME'];
	}
        
        /**
         * @return mixed
         */
        static function executablePath() {
		return $_SERVER['SCRIPT_FILENAME'];
	}
        
        /**
         * @return Hash
         */
        static function environment() {
		return php_Lib::hashOfAssociativeArray($_SERVER);
	}
        
        /**
         * @return sys_io_FileInput
         */
        static function stdin() {
		return new sys_io_FileInput(fopen("php://stdin", "r"));
	}
        
        /**
         * @return sys_io_FileOutput
         */
        static function stdout() {
		return new sys_io_FileOutput(fopen("php://stdout", "w"));
	}
        
        /**
         * @return sys_io_FileOutput
         */
        static function stderr() {
		return new sys_io_FileOutput(fopen("php://stderr", "w"));
	}
        
        /**
         * @param $echo
         * @return false|string
         */
        static function getChar($echo) {
		$v = fgetc(STDIN);
		if($echo) {
			echo($v);
		}
		return $v;
	}
        
        /**
         * @return string
         */
        function __toString() { return 'Sys'; }
}
