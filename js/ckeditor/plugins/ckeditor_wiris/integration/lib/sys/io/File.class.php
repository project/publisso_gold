<?php
        
        /**
         * Class sys_io_File
         */
        class sys_io_File {
	public function __construct(){}
        
        /**
         * @param $path
         * @return false|string
         */
        static function getContent($path) {
		return file_get_contents($path);
	}
        
        /**
         * @param $path
         * @return haxe_io_Bytes
         */
        static function getBytes($path) {
		return haxe_io_Bytes::ofString(sys_io_File::getContent($path));
	}
        
        /**
         * @param $path
         * @param $content
         */
        static function saveContent($path, $content) {
		file_put_contents($path, $content);
	}
        
        /**
         * @param $path
         * @param $bytes
         * @throws HException
         */
        static function saveBytes($path, $bytes) {
		$f = sys_io_File::write($path, null);
		$f->write($bytes);
		$f->close();
	}
        
        /**
         * @param $path
         * @param null $binary
         * @return sys_io_FileInput
         */
        static function read($path, $binary = null) {
		if($binary === null) {
			$binary = true;
		}
		return new sys_io_FileInput(fopen($path, (($binary) ? "rb" : "r")));
	}
        
        /**
         * @param $path
         * @param null $binary
         * @return sys_io_FileOutput
         */
        static function write($path, $binary = null) {
		if($binary === null) {
			$binary = true;
		}
		return new sys_io_FileOutput(fopen($path, (($binary) ? "wb" : "w")));
	}
        
        /**
         * @param $path
         * @param null $binary
         * @return sys_io_FileOutput
         */
        static function append($path, $binary = null) {
		if($binary === null) {
			$binary = true;
		}
		return new sys_io_FileOutput(fopen($path, (($binary) ? "ab" : "a")));
	}
        
        /**
         * @param $src
         * @param $dst
         */
        static function copy($src, $dst) {
		copy($src, $dst);
	}
        
        /**
         * @return string
         */
        function __toString() { return 'sys.io.File'; }
}
