<?php
        
        /**
         * Class sys_net_Socket
         */
        class sys_net_Socket {
	public function __construct() {
		if(!php_Boot::$skip_constructor) {
		$this->input = new sys_io_FileInput(null);
		$this->output = new sys_io_FileOutput(null);
		$this->protocol = "tcp";
	}}
	public function waitForRead() {
		sys_net_Socket::select(new _hx_array(array($this)), null, null, null);
	}
        
        /**
         * @param $b
         * @throws HException
         */
        public function setFastSend($b) {
		throw new HException("Not implemented");
	}
        
        /**
         * @param $b
         * @throws HException
         */
        public function setBlocking($b) {
		$r = stream_set_blocking($this->__s, $b);
		sys_net_Socket::checkError($r, 0, "Unable to block");
	}
        
        /**
         * @param $timeout
         * @throws HException
         */
        public function setTimeout($timeout) {
		$s = intval($timeout);
		$ms = intval(($timeout - $s) * 1000000);
		$r = stream_set_timeout($this->__s, $s, $ms);
		sys_net_Socket::checkError($r, 0, "Unable to set timeout");
	}
        
        /**
         * @return _hx_anonymous
         * @throws HException
         */
        public function host() {
		$r = stream_socket_get_name($this->__s, false);
		sys_net_Socket::checkError($r, 0, "Unable to retrieve the host name");
		return $this->hpOfString($r);
	}
        
        /**
         * @return _hx_anonymous
         * @throws HException
         */
        public function peer() {
		$r = stream_socket_get_name($this->__s, true);
		sys_net_Socket::checkError($r, 0, "Unable to retrieve the peer name");
		return $this->hpOfString($r);
	}
        
        /**
         * @param $s
         * @return _hx_anonymous
         */
        public function hpOfString($s) {
		$parts = _hx_explode(":", $s);
		if($parts->length === 2) {
			return _hx_anonymous(array("host" => new sys_net_Host($parts[0]), "port" => Std::parseInt($parts[1])));
		} else {
			return _hx_anonymous(array("host" => new sys_net_Host(_hx_substr($parts[1], 2, null)), "port" => Std::parseInt($parts[2])));
		}
	}
        
        /**
         * @return sys_net_Socket
         * @throws HException
         */
        public function accept() {
		$r = stream_socket_accept($this->__s);
		sys_net_Socket::checkError($r, 0, "Unable to accept connections on socket");
		$s = new sys_net_Socket();
		$s->__s = $r;
		$s->assignHandler();
		return $s;
	}
        
        /**
         * @param $host
         * @param $port
         * @throws HException
         */
        public function bind($host, $port) {
		$errs = null;
		$errn = null;
		$r = stream_socket_server($this->protocol . "://" . $host->_ip . ":" . _hx_string_rec($port, ""), $errn, $errs, (($this->protocol === "udp") ? STREAM_SERVER_BIND : STREAM_SERVER_BIND | STREAM_SERVER_LISTEN));
		sys_net_Socket::checkError($r, $errn, $errs);
		$this->__s = $r;
		$this->assignHandler();
	}
        
        /**
         * @param $read
         * @param $write
         * @throws HException
         */
        public function shutdown($read, $write) {
		$r = null;
		if(function_exists("stream_socket_shutdown")) {
			$rw = (($read && $write) ? 2 : (($write) ? 1 : (($read) ? 0 : 2)));
			$r = stream_socket_shutdown($this->__s, $rw);
		} else {
			$r = fclose($this->__s);
		}
		sys_net_Socket::checkError($r, 0, "Unable to Shutdown");
	}
        
        /**
         * @param $connections
         * @throws HException
         */
        public function listen($connections) {
		throw new HException("Not implemented");
	}
        
        /**
         * @param $host
         * @param $port
         * @throws HException
         */
        public function connect($host, $port) {
		$errs = null;
		$errn = null;
		$r = stream_socket_client($this->protocol . "://" . $host->_ip . ":" . _hx_string_rec($port, ""), $errn, $errs);
		sys_net_Socket::checkError($r, $errn, $errs);
		$this->__s = $r;
		$this->assignHandler();
	}
        
        /**
         * @param $content
         */
        public function write($content) {
		fwrite($this->__s, $content);
		return;
	}
        
        /**
         * @return string
         */
        public function read() {
		$b = "";
		while (!feof($this->__s)) $b .= fgets($this->__s);
		return $b;
	}
	public function close() {
		fclose($this->__s);
		{
			$this->input->__f = null;
			$this->output->__f = null;
		}
		$this->input->close();
		$this->output->close();
	}
	public function assignHandler() {
		$this->input->__f = $this->__s;
		$this->output->__f = $this->__s;
	}
	public $custom;
	public $output;
	public $input;
	public $protocol;
	public $__s;
        
        /**
         * @param $m
         * @param $a
         * @return mixed|string
         * @throws HException
         */
        public function __call($m, $a) {
		if(isset($this->$m) && is_callable($this->$m))
			return call_user_func_array($this->$m, $a);
		else if(isset($this->�dynamics[$m]) && is_callable($this->�dynamics[$m]))
			return call_user_func_array($this->�dynamics[$m], $a);
		else if('toString' == $m)
			return $this->__toString();
		else
			throw new HException('Unable to call �'.$m.'�');
	}
        
        /**
         * @param $r
         * @param $code
         * @param $msg
         * @throws HException
         */
        static function checkError($r, $code, $msg) {
		if(!($r === false)) {
			return;
		}
		throw new HException(haxe_io_Error::Custom("Error [" . _hx_string_rec($code, "") . "]: " . $msg));
	}
        
        /**
         * @param $isUdp
         * @return int
         */
        static function getType($isUdp) {
		return (($isUdp) ? SOCK_DGRAM : SOCK_STREAM);
	}
        
        /**
         * @param $protocol
         * @return int
         */
        static function getProtocol($protocol) {
		return getprotobyname($protocol);
	}
        
        /**
         * @param $read
         * @param $write
         * @param $others
         * @param null $timeout
         * @return null
         * @throws HException
         */
        static function select($read, $write, $others, $timeout = null) {
		throw new HException("Not implemented");
		return null;
	}
        
        /**
         * @return string
         */
        function __toString() { return 'sys.net.Socket'; }
}
