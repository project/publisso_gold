<?php
        
        /**
         * Class Math
         */
        class Math {
	public function __construct(){}
	static $PI;
	static $NaN;
	static $POSITIVE_INFINITY;
	static $NEGATIVE_INFINITY;
        
        /**
         * @param $v
         * @return float|int
         */
        static function abs($v) {
		return abs($v);
	}
        
        /**
         * @param $a
         * @param $b
         * @return mixed
         */
        static function min($a, $b) {
		return min($a, $b);
	}
        
        /**
         * @param $a
         * @param $b
         * @return mixed
         */
        static function max($a, $b) {
		return max($a, $b);
	}
        
        /**
         * @param $v
         * @return float
         */
        static function sin($v) {
		return sin($v);
	}
        
        /**
         * @param $v
         * @return float
         */
        static function cos($v) {
		return cos($v);
	}
        
        /**
         * @param $y
         * @param $x
         * @return float
         */
        static function atan2($y, $x) {
		return atan2($y, $x);
	}
        
        /**
         * @param $v
         * @return float
         */
        static function tan($v) {
		return tan($v);
	}
        
        /**
         * @param $v
         * @return float
         */
        static function exp($v) {
		return exp($v);
	}
        
        /**
         * @param $v
         * @return float
         */
        static function log($v) {
		return log($v);
	}
        
        /**
         * @param $v
         * @return float
         */
        static function sqrt($v) {
		return sqrt($v);
	}
        
        /**
         * @param $v
         * @return int
         */
        static function round($v) {
		return (int) floor($v + 0.5);
	}
        
        /**
         * @param $v
         * @return int
         */
        static function floor($v) {
		return (int) floor($v);
	}
        
        /**
         * @param $v
         * @return int
         */
        static function ceil($v) {
		return (int) ceil($v);
	}
        
        /**
         * @param $v
         * @return float
         */
        static function atan($v) {
		return atan($v);
	}
        
        /**
         * @param $v
         * @return float
         */
        static function asin($v) {
		return asin($v);
	}
        
        /**
         * @param $v
         * @return float
         */
        static function acos($v) {
		return acos($v);
	}
        
        /**
         * @param $v
         * @param $exp
         * @return float|int
         */
        static function pow($v, $exp) {
		return pow($v, $exp);
	}
        
        /**
         * @return float|int
         */
        static function random() {
		return mt_rand() / mt_getrandmax();
	}
        
        /**
         * @param $f
         * @return bool
         */
        static function isNaN($f) {
		return is_nan($f);
	}
        
        /**
         * @param $f
         * @return bool
         */
        static function isFinite($f) {
		return is_finite($f);
	}
        
        /**
         * @return string
         */
        function __toString() { return 'Math'; }
}
{
	Math::$PI = M_PI;
	Math::$NaN = acos(1.01);
	Math::$NEGATIVE_INFINITY = log(0);
	Math::$POSITIVE_INFINITY = -Math::$NEGATIVE_INFINITY;
}
