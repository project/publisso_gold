<?php
        
        /**
         * Class StringTools
         */
        class StringTools {
	public function __construct(){}
        
        /**
         * @param $s
         * @return string
         */
        static function urlEncode($s) {
		return rawurlencode($s);
	}
        
        /**
         * @param $s
         * @return string
         */
        static function urlDecode($s) {
		return urldecode($s);
	}
        
        /**
         * @param $s
         * @return string
         */
        static function htmlEscape($s) {
		return _hx_explode(">", _hx_explode("<", _hx_explode("&", $s)->join("&amp;"))->join("&lt;"))->join("&gt;");
	}
        
        /**
         * @param $s
         * @return string
         */
        static function htmlUnescape($s) {
		return htmlspecialchars_decode($s);
	}
        
        /**
         * @param $s
         * @param $start
         * @return bool
         */
        static function startsWith($s, $start) {
		return strlen($s) >= strlen($start) && _hx_substr($s, 0, strlen($start)) === $start;
	}
        
        /**
         * @param $s
         * @param $end
         * @return bool
         */
        static function endsWith($s, $end) {
		$elen = strlen($end);
		$slen = strlen($s);
		return $slen >= $elen && _hx_substr($s, $slen - $elen, $elen) === $end;
	}
        
        /**
         * @param $s
         * @param $pos
         * @return bool
         */
        static function isSpace($s, $pos) {
		$c = _hx_char_code_at($s, $pos);
		return $c >= 9 && $c <= 13 || $c === 32;
	}
        
        /**
         * @param $s
         * @return string
         */
        static function ltrim($s) {
		return ltrim($s);
	}
        
        /**
         * @param $s
         * @return string
         */
        static function rtrim($s) {
		return rtrim($s);
	}
        
        /**
         * @param $s
         * @return string
         */
        static function trim($s) {
		return trim($s);
	}
        
        /**
         * @param $s
         * @param $c
         * @param $l
         * @return string
         */
        static function rpad($s, $c, $l) {
		return str_pad($s, $l, $c, STR_PAD_RIGHT);
	}
        
        /**
         * @param $s
         * @param $c
         * @param $l
         * @return string
         */
        static function lpad($s, $c, $l) {
		return str_pad($s, $l, $c, STR_PAD_LEFT);
	}
        
        /**
         * @param $s
         * @param $sub
         * @param $by
         * @return string|string[]
         */
        static function replace($s, $sub, $by) {
		return str_replace($sub, $by, $s);
	}
        
        /**
         * @param $n
         * @param null $digits
         * @return string
         */
        static function hex($n, $digits = null) {
		$s = dechex($n);
		if($digits !== null) {
			$s = str_pad($s, $digits, "0", STR_PAD_LEFT);
		}
		return strtoupper($s);
	}
        
        /**
         * @param $s
         * @param $index
         * @return int
         */
        static function fastCodeAt($s, $index) {
		return ord(substr($s,$index,1));
	}
        
        /**
         * @param $c
         * @return bool
         */
        static function isEOF($c) {
		return ($c === 0);
	}
        
        /**
         * @return string
         */
        function __toString() { return 'StringTools'; }
}
