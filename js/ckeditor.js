jQuery(document).ready(function() {
    function makeCKEditor(obj) {

        var styles = Array();
        var c = 0;
        for (c = 0; c < document.styleSheets.length; c++) {
            var s = document.styleSheets[c].href;
            if (s) styles[c] = s;
        }

        jQuery.ajax({
            url: "/system/control/getUserLang",
            success: function (e) {

                CKEDITOR.replace(obj.name, {
                    customConfig: './config.js',
                    contentsCss: styles,
                    bodyClass: "chapter-content",
                    extraPlugins: "brclear,filebrowser,filetools,eqneditor,image2,tableresize,quicktable,html5video,youtube,tablesorter,htmlbuttons,symbol,mathjax",
                    htmlbuttons: [
                        {
                            name: 'infobox1',
                            icon: 'ib.svg',
                            html: '<div class="alert alert-info"></div>',
                            title: 'Insert infobox'
                        },
                        {
                            name: 'pagebreak1',
                            icon: 'pb.svg',
                            html: '<div id="rwPageBreak" style="page-break-after: always !important" aria-label="Page Break" class="cke_pagebreak" data-cke-display-name="pagebreak" data-cke-pagebreak="1" title="Page Break" contenteditable="false"></div>',
                            title: 'Insert Page-Break'
                        }
                    ],
                    allowedContent: !0,
                    removePlugins: "iframe,forms,video,newpage,preview,templates,font,specialchar",
                    removeButtons: 'PageBreak',
                    pasteFromWordRemoveFontStyles: !0,
                    pasteFromWordRemoveStyles: !0,
                    pasteFromWordPromptCleanup: !0,
                    language_list: ["de:German", "en:English"],
                    langCode: "en",
                    language: e,
                    defaultLanguage: "en",
                    skin: "kama",
                    uploadUrl: "/system/uploadFile/ckeditor",
                    filebrowserUploadUrl: "/system/uploadFile/ckeditor",
                    format_tags: "p;h2;h3;h4;h5;h6",
                    cloudServices_tokenUrl: "/system/cs-token-endpoint",
                    cloudServices_uploadUrl: "/system/uploadFile/ckeditor",
                    height: "30em",
                    wordcount: {
                        showCharCount: !1
                    },
                    on: {
                        instanceReady: function () {
                            this.setReadOnly(!1);
                        },
                        change: function () {
                            this.updateElement(), jQuery("#" + obj.id).trigger("change");
                        },
                        pluginsLoaded: function () {
                            this.ui.items.brclear.toolbar = "insert,0", this.ui.items.Youtube.toolbar = "insert,0", this.ui.items.Html5video.toolbar = "insert,0";
                        }
                    }
                });
            }
        });
    }

    jQuery('textarea.ck-editor').each(function (i, e) {
        makeCKEditor(e);
    });

});
