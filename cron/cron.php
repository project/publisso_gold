#!/usr/bin/php
<?php
	error_reporting(E_ALL);
	
	
	define('_PATH_DOCUMENTROOT_', '../../../..'						);
	define('_SERVER_PORT_'      , 80								);

	chdir(dirname(__FILE__));
	
	$drupal_caching_tables = [
		'cachetags'					,
		'cache_bootstrap'			,
		'cache_config'				,
		'cache_container'			,
		'cache_data'				,
		'cache_discovery'			,
		'cache_dynamic_page_cache'	,
		'cache_entity'				,
		'cache_menu'				,
		'cache_render'				,
		'cache_toolbar'				,
	];
	
	
	if(!@include(_PATH_DOCUMENTROOT_."/sites/default/settings.php")) trigger_error("Can't load drupal-settings! Please check settings", E_USER_ERROR);
	$db_settings = $databases['default']['default'];
	
	try{
		$dbh = new PDO($db_settings['driver'].':host='.$db_settings['host'].';dbname='.$db_settings['database'], $db_settings['username'], $db_settings['password']);
	}
	catch(PDOException $e){
		echo $e->getMessage();
	}
	
	//get FQDN
	$sql = "SELECT `stpValue`  FROM `rwpg_rwPubgoldSetup` WHERE `stpKey` LIKE 'system.fqdn'";
	
	$sth = $dbh->prepare($sql);
			
        if(!$sth->execute()){
                trigger_error("Database-statement error ".$sth->errorInfo()[1]." \"".$sth->errorInfo()[2]."\"", E_USER_ERROR);
        }
	
	$row = $sth->fetch(PDO::FETCH_ASSOC);
	define('_SERVER_FQDN_'      , $row['stpValue']);
	
	$section = $argv[1];
	$action  = $argv[2];
	
	switch(strtolower($section)){
		
		case 'control':
			control($action);
			break;
		
		default:
			trigger_error('Unimplemented first argument "'.$section.'" in execution!', E_USER_ERROR);
	}
	
	if($dbh) $dbh = null;
        
        /**
         * @param $action
         */
        function control($action){
		
		switch($action){
			
			case 'clear-cache':
				clearCache();
				break;
				
			default:
				trigger_error('Unimplemented second argument "'.$action.'" in execution!', E_USER_ERROR);
		}
	};
	
	function clearCache(){
		
		global $dbh, $db_settings, $drupal_caching_tables;
		
		foreach($drupal_caching_tables as $table){
			$table = $db_settings['prefix'].$table;
			$sth = $dbh->prepare("TRUNCATE TABLE `$table`");
			
			if(!$sth->execute()){
				trigger_error("Database-statement error ".$sth->errorInfo()[1]." \"".$sth->errorInfo()[2]."\"", E_USER_ERROR);
			}
		}
		
		httpSelfRequest();
	}
	
	function httpSelfRequest(){
		$port = 80;
		$url = "/index.php";
		$timeout = 30;
		 
		$fp = fsockopen(_SERVER_FQDN_, _SERVER_PORT_, $errno, $errstr, $timeout);
		
		if($fp){
			$request = "GET ".$url." HTTP/1.1\r\n";
			$request.= "Host: "._SERVER_FQDN_."\r\n";
			$request.= "User-Agent: pubgold/1.0 (PUBGOLD-BOT) Version: 1.0\r\n";
			$request.= "Connection: Close\r\n\r\n";
		 
			fwrite($fp, $request);
			while (!feof($fp)){	fgets($fp, 128); }
			fclose($fp);
		}
		else{
			echo "ERROR: ".$errstr;
		}
	}
?>
