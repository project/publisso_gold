<!Doctype html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <meta charset="UTF-8" />
                <script>

                        function utf8_to_str(a) {
                            
                                    for(var i=0, s=''; i<a.length; i++) {
                                        
                                                var h = a[i].toString(16);
                                                
                                                if(h.length < 2) h = '0' + h;
                                                s += '%' + h;
                                    }
                                    
                                    return decodeURIComponent(s)
                        }
                
                        function subst() {
                                
                                var matches = document.location.href.match(/^http(s{0,1}):\/\/([^\/]+)/);
                                var host = matches[0];
                                var vars={};
                                var x=document.location.search.substring(1).split('&');
                                
                                for (var i in x) {
                                        var z=x[i].split('=',2);
                                        vars[z[0]] = decodeURIComponent(unescape(z[1]).replace(/\+/g, ' '));
                                        //vars[z[0]] = decodeURIComponent(z[1].replace(/\+/g, ' '));
                                }
                                var x=['frompage','topage','page','webpage','section','subsection','subsubsection', 'bookTitle', 'bookID', 'chapterID', 'chapterDOI', 'license', 'language'];

                                if(vars['page'] == 1) {
                                        
                                        if(vars['theme'] == 'gms') document.getElementById('gmsLogo').style.display = 'inline';
                                        
                                        document.getElementById('headP1').style.display = 'block';
                                        document.getElementById('title').innerHTML = vars['bookTitle'];
                                        //document.getElementById('bookLogo').setAttribute('src', host + '/system/getPicture/bl/' + vars['bookID']);
                                        document.getElementById('siteLogo').setAttribute('src', host + '/modules/custom/publisso_gold/images/LOGO_PUBLISSO_' + vars['language'] + '.png');
                                }
                                
                                if(vars['page'] > 1){
                                        document.getElementById('headFF').style.display = 'block';
                                        document.getElementById('author').innerHTML = vars['author'] + '. ' + vars['chapterTitleShort'];
                                }
                        }
                        
                </script>
                <style type="text/css">
                        html,body{
                                margin: 0;
                                padding: 0;
                        }
                </style>
        </head>
        <body style="border:0;margin:0; font-size: 8pt; font-family: arial;" onload="subst()">
                <br /><br />
                <div id="headP1" style="display: none;">
                        <table style="width: 100%;">
                                <tr>
                                        <td style="width: 20%; text-align: left;">
                                                <img id="gmsLogo" style="height: 50px; display: none;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHEAAABCCAIAAAGY9Z4SAAAACXBIWXMAAA7FAAAOxAFed92+AAAf2ElEQVR4nO1cCVxVVf4/59yHEKjhBmruYqUQUomajoBppWiZYmq5IGiNMpPaMjOVjTlNLtXMtJhbo6CgqSlMNm5TKYtmgZYKWpmiue8LCoJy7zn/3+937n3wENDMz2ec/6cj6uO++875nd/5Ld/fcp+r28gkLti1hzKUslyc88z5sddxO4uIW+CC/ySzuGLdRqVsmj9cMbFizY4nosOWrtkxpE97ZbHI0QuykkYwhgTg3YIZkfFJG5Pi4NOZibEzVmx/ok/YB6nfDundPurplKykke8v3Dwutot9Nw4uOGMJT3SEl/A2U2xjYhy8ru3niohLzsS5mXO3gj+SKTlrRfbg3m2BsFNnLwfUvY1xXlB0KSNxVNSoxMz58c7dnGUlxkXFJ3JmwKY54++kbJo+oSe88ffnH+k+amFmYny5uWlkOJdgI9MnPKxfhYc0gZ2473FZjL/0zn+uh4N4N/CBAf+uPaQrY/4IpLPqOzSbyyj+Lv/ElDmbFr/df+nqPDgOuLJ09Y7BfdpHxiWnzx8phORM4K2wfIM6NVPeHqAUn526HW49cOTcnNTts1K/zUqKixqVDCvbs36373hwy4ZLV28bEn0v/BoJ1AtmMbYpMS5idLJQUpOBtwa3agivB/UJY3p7QLpiBnwmfmHW/NioUQvLpALGuGmrYx6+KyI86IHQRtMmPAwsefm9z+AFru5w1771/Yl99Av7CDibRi80lfatL7/72XUwlW7tGpe0cf5whrTdhMG5NBl/MG6hywDKmCh/Ygo5oao9RPedV98m9P7LxBGkvd+4jwsKL8dGhy5cu40zV8a8EbAonAoI9Yakp0DbQKcj41NgLuBO0eXS6LFLHwm/I/u7kwWXzMxEUFiUAeYxL9F4/uKVzEQQNxUfEyqJM0AR7K1xgO/Qlz5ZMr1/96eTXx8bMWn2Rrhn3vItpjKDWjR4eWwPyS1kAiuzOG5FhpkFkAMvTSZ6xqdIbmbB0Src5uJpMVGjF8AaSroiw1vzOZuAAeOHdZ4wrLNUKDk94heBZLiVzoMPOAweFb/gL2N/A5PCjArXV3QabGh0MHBg0ZTHnCv8vZTNqZkHYiJa7TlwBvY1pdykHvMKxrPmjwSrgi86tpFEKjCM1F0+MyB8dMx9XIF9sk3m+OHd4Af4BAQIVvGQPejl+MPdy7j/1f/DuV31cdwKv/oqzNtt5EIhyPTdlEHn4QLhcAi9KZOyqFHkLq6S6jKrde1JtFKACHGSSIYS6arszp85Iw4P7S+vC0wqOeyV1IMni9LeHhhQryZcW7Zmp7Lkk31DFQehEKfOF23YvBfuBC8GBz9ldsZnWw4MjLrz98M7CzxPqdnoQWn30SlAZtfQwEF/TJv9Up+7g/znLN/GuLVq84+Lpw2ErQ16MVVKPFI02KMXcWUmDLwXjHJqZj4pbgVKOdpVhkbwKcVF1OjFY6atAqkEAjuFNMzedVSiSnC3kHCQY2llgIoz0b9n20fGLkFmgn7Slp1JQfXAneIVAzaQ/PqjLRrdrsrOT9gsgkU4B9svUcfNqPjFQKCXt1fGPLLbXNoiZc8p8Ncu9wQC31es2WVKuYWrgdGhNl2ou/LMuWKgT3AJ5yMUz0waFRW/MHIU2oe3J/ToFNqMOfpdjlLOv8o7CS9nrPhWXxoYfY9+s3Noo9iX/nX4dNHfnot48f0MkB8FoAUs5LzhYL26jVr8wrsbsuaN4MLTEtKeUERgZM4b9vo/09fnHHRrxLTxD3WPXwTk3t++OW0Q9N7oFp+cDntnIiNxaFR8irb+ymNSmCsxNiI+qd+41H5RLXDGcpYejgUMGJxDmXFQyuCy58iUhEGhs1dsN6RAtODc72wfMRKYpbjI0QuTV+/KTBwZMRqEQXYJDWzTrB5oS9fQRhzPl3UJDbBAeQRYrJGPj18yZ8XOHh1bvDomqrxSehoqybLwHOFwLXghLdh4L0mIBV4QIXIqvrBn+OS9Jy1lgqiVJ7PipI5+2gwXhr7GNQvsHXnaCaMyRa9U93/pAOiwjnNDqetBZdcewAeYCo00aRscNhqdmzL1zRogf2jGlIyIX4xAE8wQnSsIdMVj+K8PpW0ER+sABLq4tgTiFiOTBum+IquKL69XWh3F+Rme8WfOfO1RkVYEimiK0cw9Pm75+aJiAo4EdfAtFto6cMYrvdFc4WUVBfZHcfqQ9c6Eh+4LbeJeGE5tyodZX2Tvd2tYJsV1YGoycg5MnrVRGvjHgvAEdUXCqsl/7df8jtoko2QqPDfhQSuYKUE46bv8U2OnrCIiVcb8YYy7DMVOnLsw+A+f5O471e3phVnzRnKNxhXIO6xmGsp47t3PM+bHcQCRzAXuF94EQvULptErwbuEN1Z9v/+0wcXsl3q3bRMI65qKjXzl46PHioa/+ilgzcaNb+eMXa08HrQKG3TIhClrtBdIT4y1nQFngXVr0guw/+SVAVjD/pEK+cA9jTbvPA5mPDPnx8jwINiBEOKvczKAo/61fC4UllicuIAujO3adx7MGZwEoVec2YvLRVOfyPxmf2SHFohUr+JoZTJAsiMB7YKxQBCPH3MAGZwzT6foy5nHYOXM5+vPPPCXD7+ePPer9E5tOHpA9kXOAfjgp+8PAmSuWaF3DcABIm+Y9ndT1xHshlhKTYzt0ivyTptKkhObe1XRqsi///DTCUb01fb1VjqNIFXU0ykSj1N5SVYqMGxV0mP33Tvd9dqHWwUzM77e271zm/dTNgM17VrXZ2Qm4SjQ6qAIINjJmD9SIqcNiKshcBZKTFuwGX4sYX0xM9bH26A4oCIu9/SvwE7B2rYK1KFhwaUrHONpA7Vh3giTq917TiVMWQf8RDAoVPlcAXx08Rt9h01aOenDLzd1bvOvjD1wcdYrvYBO5DKtS0ZEfL/3eN6eE4XFZvyAeyG8B0gJWy4pltEJKXCkvRIWY+zMK7EPFe0AnToLaV0vb+9p4OLvpq354KXeQrjQVzA+ZvpaaSgfODlxdTQimoFOSGAhe+zZxYDsenZqCehdkT6hEKGmK5PzMVM/Bz0DgRjSK9jXx5sjJlX7j5wGHQV1GdD9TkfwKhrHCrRKEhc+c2LfDV/tf21eVu7eM93jP4KlLKMU1gprVTcv/5TJazCyUg5Msz8L86+b9WSvhCXnC0thoUljulWAA6BMYE82JA6PeXbpucLiXgkfQfhN5yMkHqB4Y8xvIjq1JnMnrmZsBVqF+58HO7eMeKCFC10ccpCcB9LGlZNF4TgjhgCKOyhReXtDwDaUMR0GErzgPHPeCMTlZSrJPpkxxKZekSDRe5K5hYp+r95meTIBbBPQoHMI0t4lzaGQSrhu2PPqhcgg0WK20UGPYoe9oEauSvwTuk4dTePHRSVx9XXSalsYG3GWhdL44mr04MhW+TfKXrtYpRazDL9WaVOvj9ZbbwB+TUIOAi5m5n+bGI/BtTG2UPkQaHcJaTj9eQxMbhaAv4kDjQ/E01w8qHPnyv57a4FXW43BTtoJWqEc/b3JePQXDrIkYLxdOp5yEUKrxDG4B6Uwb/Y2lE5tSl7NtDrBx+y479qKT/5Qe8XrShdf1+BlbtMh9xq8uC4Lpem7aVSWm5OOyzHY1Y7rIJQiMV55Zv3Gh55NEoQQ1yFa5Qgl6+TQBNgeMdv7Kdmp6T9Q0k3E9AgaN7yLkHbmF0zc7r2nd+4+ZqEguW7zMx7r1lY5vksfKMBQ994GY0pN6pjJYjJ9y753Fm25ePEy3FfHz3vKhAfvaRXA3VmaagAKkWhLN6C8TVv2/PnDjeCuwZMDzgH4BLhzZcbef8980s/biyNqN3bsOTYnLVdJC44PzF2/bm2Zmzc03awV29zzE6GCYA57cHQyoHPBvQSGLOpC4eVnp6693c8HIgUKPyXjFX11GaG0F5v/584W/Xnu10C0NNgdgX79ut49e8U2YVlSiL4JSzYkDoOwCjkrCYAyOkEIrGBbyk4tA28OHj3LkDShE7HuI+83frmk1AiQlBBz34mTF5dn5cM8BUUly9bkDo4OQ0gvKgpDuaN3b0Gx/i8uB2ALVxa9/njzO24XpjW4V8ijExZfANypWHGJ8vW2EP3RR0ws0nLMzmfvj+gUZBB6AfKTP90JixGV7iVBgsTFwkuEPVnmP0di8Cb56EHhvccugaD8g9Qdg3uHOYapKhnVrGCq5LIlEd/CB1XLO/zxugC/wN6e0HPMlLWAMtes3zmwTyg5CmQVyLK/320Fl0r+mZYLASoF4UjH5zn7gdePhDdfnXOoBkZSALRF8WVJJSarc/tAkg6DGfI2Hy/usiBO05AUMe81QDSFL5+m7+aYsRbPDApjWsdQXdRdrQIwglbGnkMFeGzl0P7TMcF/T/nm0OmLdjaSAAXN53r8oeD/5ByUwtCxvK+3wFylpTbnnaRkukk0qNeeiQqo69suKBBOiCBvtaUfSYHtyqw9qLtC9ujUWidXtNMFwtbOesrHxyhLrzujT2S7v6dsNUCilQ1bT5wtIL5SaE8KolDuUPosCG8pTfbYuKXL3hroA1KkePfwZtKOoSnDfJXB8gxN6egPHz8P1BnKK6Be7fJYBZb08fHiniQ67wk0Z4aZvmV/944tQS4Xr9kF+tS0gR8nWaYcN6MQmsVEBaVt2MMsef5iycO/W+xivG3LOn974WEI/RyPddX8V8XQlIFAXwH3W9omK0suW7cDLCVtEBMXoOFDerdj5ZOvhmoXdHvevoJ/pm6LCm8JXAFbBnO9MDQcpgI7pGN3OCb40IThXZvU88fqAseOB1OZuftO90pY5udnrJ0xVOHirPqglIRYKNhiKdxui5sEAzMzbYeBzPaS8gqJPXuyV0h5GYX1nh3cKWHauoOnLmCgLAQVCHlYaLPv808Iu7BcttKA3sHwc+jombeStuzcf5wKM7Ko0Oz/+5S0mcNB1RQlcqoiVOhjvYJsFNwNCDnzUtIEZbCuwO+lwkJZ5B4Lw013BzVwtFWcOlsk4UCINxQHY1qCIlsyj3BG3ICFmzWuO+PVR+DcVqzNnbUiDz56ukSi0mPCpdqjR55y3iWk8Ve5Ryyptu48En7PHXB5Q2K8Zkm3UQsMpcNRj6GcHBbQvzbrhx8PnQN96hpyB6EOpE0Yts2FuPS7feezdxyIHwD2UqB+cf5EdLvFa3cVXOQuVUKwsxJg6VGoJ8lQkfcjoeDb/rEke0nwADtep9jURhKKV9AnTrXOAVFtlmfsnvPx9guXimGqMYPuZfZqEvM8aNF4v/FLCy5cAVl9sld7n9sUWgHkd422rep/lXvMYt5OnrxqGcXsBWYaVHS3u95Myja5OnGksLj4srefS8B83JoyZyMIsMktFyd04emNwbQN6RuSlv7j2UvFBsoCb97Yv/x2sOrG1DMD2r+16BvDsuL+mrZk2hNagkAwvsw7Zm+JqcpL3eXYoutFaEiCW9fL238ehLHXs0uH9w0Jax34j6U5h04U61ojI8UXnlyF2QPr1Ia3DbvMyKk1peweA4lgfSPbTl+UDSdz+ETRY88uGdGrLfjqOam5LoXp6bCW/mR4LX1EVRBqq7FycTVzYnTU6BRF5n7RqrwkBRN5cW4+P7LzOylfW4pMQoXTJ0PrklYpB1vLBzx451X2VlDRUH00+fHhk1ZCyAYoZEbqNoyFDXBcvLavz/uv9EUVUJU0FXhw1MlfoKZnzh+RkbN/8pxNsJqXwYNb+n8wsW9JifleSo6Ldg27CKhfq0tooLLlCX3YpISo/3yZDzx9KjqEEbr09a3VsX0jLxtA4QabNfHPTIx9Y24mggHC4v6+XpOeibo/pGFVGShWLcIXkR1bZ3RohYlxdAX4aR8f14b5w6gCAcbH6N6xeVTHllT9RoAHl7uHt+4e3pxJg1ZEFWzeyPft8T2Vp3LAJ179beTEZyJof9yQrLJk3vURqrRHRXGzDGXYCxFCUdwpFun8HsYSBhXO8SbJXLb80q9oyrioaG5sTIPsRCESkgLI6lqaqiSU64o+QB0Epm4Pj2KuF1V2J4r2ICZW4Zysl9uc0UT6s542gjtg1qNYUB1LqwvunKBblM3IDfcFXu4+hrXCqz/ose2Kv3pcukbm8RqE3lLjV0Jv9nB9v+/cy//4nEL1m9SHdfNGee3CbkdEyhLU1ovqicxQFV3Kr6Pi4BhWuOAPswC6WrKGD5MlEOYT41BKu4Y0nPrcw+gE4SoX7NZLPt9qg6r7SkdvBE9AcczIpxdpYcTuWSrLSUpZaoR9qzWj3HJD2WwFLGmgC0ev5XLq5pQgt0to3A5ZKmtU/XV4DDthogiQOYEE52U8ldQhyJ1Szv9vhlaV4nZft4H+NdPsWjg13+wSglQerZK/cCj3Mh5UslutilU+Z8vsPnvDfd1dTv6FdYubw1M7G8M1odog32rc1MMtj5qJln326FEsZjdzqUoatH7OuDk8BRt85Oi5oqJSv5o+TRrdDsa7+rjtvzWcQI4Sd4xduqwOHT2FKTqmmtxR39fHIGW2GXrD0nqN8JmiX90CrWWQOkOUPHr0wgcfZ2/eeRzTHspudRO8RikvNpQLAtJSxl2SjXi03eC+YTW9XXbRR0+lk2hcfLx259zULVICgjOkLMXspg2Q1b/nxtb0ZmUtN3a0rjSI0Vtdnfnd9KRsp3RJgT2VjuBKVuJIy26GMZlTr+bM3Lr92N+X5Rw7Vgynjg2PQnpZ2OJtMSICH61yuWTp/e0b/ml45zr1/V36AChLR55I21l1g2VcJ8YVtvllgrqmVM7Ow396d4MJO5fg8ExleClLAva9gtw2BXISGW9Q68XC1XlJa3fGRd8d178ztw9JT48VUdiLPiShpGEYpfaDFsI01LJV346K6eCkPpXTlWOjQj3F3NTtbjnCsIUYCv+S8kpDEYwRQhdRs7bmT57zpZ3p5SiZEOGYHBEPpSTwAyAeBiuFk92ad2LgH/8d3LrWzImP65QFlnToKGnFawhv5TwlshR3kkQkqojF/vDuZ9m5R12IypgJTJWi1m3efxnd5f7QpmCbTOorPHO+ePLMDTt/OodlVTABpkhZvffIqUuTnu6ODapIkNCNBtppWtwC0ZYW5k718xdC8ZRVux+NDGlQzxvvd8RM844kka3J+rGgsNRNLbDSsiwHijMny0HJJsWWr8udsWK7gQlhtEqKixYNak59NrJ543r0YXzjcon6W3Lm518dgtNWBNh35RcMezlt8dQBDOthGDNdpy2onKduqOXoPj7+8/6iTdk7jjHmZeISwAbxVJ92o/p14pS/VlRVAANQ/3bvDyY+uiBt64JVOynEEBY3N3x9JKbHyWDs6RTKqffpBYChmhGGzp4LevxFlH60+psJsQ8oz6caNOqDtz9cvtWhVdWqVbOwsNB9A2X9ma540MfVrOXbFGqOtAykpnGA7+Kp/U3B7SZjLDopH2/+6qgeXcP2Tp69Ca6ZmJxWh09cXLY2d2B0qEtn6GxKbryFQ6MK3cArvs07mLYhH1jrYqUow4x3at9o9IBOXOuXs2u8l7tAmOIHdEhevYsQG5UbhZW7+3i71lgChUVVmZG0jxB40bl9oN9t3uu3/IQPbSm5Mmvv0EfD6tXxc7og7JuBLavTvz9XZFEPK2Y6fxcT+taCzVy4TFVq8LIELFWT+NHj58AcGdhsQO2FDCyVoEqcRVV4Il9RMdPgD4YHJaXlHj51UaA5Fo0CfYtKLsNhkFVx2an6ayl/dX5fCxSmAZj8+IvdwC8D9w5kY5/liN7tqevGpL5OYWMopcthyIbgoLq5+WfBDBsWbxtU/1KJSfJTHR4Y+8QD63MOIKwBU8344nU7xg/pyLjzRDRW8JCClHW78AI2LqgHO7Ro2eR2i4ODMSkRTXE2lX9gHiGMpg3r+NfyOnux1MXB9JeCJhw+VfTouCWz//RwwyZ1XfjMN6XaJbYqQ5yZPG0AFiE4VfXoH2nLjdQtGdU1R12Dp7rxnDLLXBg5O4+iGUQMB5YVztAV3CYA273p6VjuoAS0E9qWKzbzld702qBOV4h5dce31I6n0jXBgD4e1ebTzL1wp5dUn6zf+9QjYfXqCgIEOAmQk/HN3sOnCrFrB5WXjxl836mCYkN7RjxTbOmhZbBVmB5LMKb//qGEaesshoXTUgMi9MsFhWroaysl8+LK7NmpebewZh3DWvphsZ5y6tQZolvMKbSXWlWqatq5bp4ydxOAOnzkhEQsAVsA1AMqIhs38iab6AgdIQQNvahYBrwtVYrSCtSnjXgVjbLQt1XWEEBXlXpmcIeV6fkwcakLLLdcsnrbuOFdqbJiN00npm3DCjlsWIn+Ua3r16t1+txlAEaG0i01Nlqje/QDKapdm8D0+SOmzN24LucnQ5Za+I6g2v0Vzlyf5xxcn3OQsxymrmhJ7N+99W+f6ORTw+X0LQs7oNeW9MZz/ARBsC4jVcFl3bKipQUhVdPAulSDRmNAa6lSYT0Ut0wSlaR99PgkJhjIMWDDAtswfyQgV2xSqoIsEDTfGt4jHrs7ZfUP3AITKNLSfxjS5/6Aut7kccXGnL0/nSwGyGOBaeDm6EGdcYvKNHQDCbo33WBhP4Rsl5XIVb3624hXx0QUl5irN+xKy8w/fOYicJ6MCfZcmvwK4j/YkrI+Xr93Rfo+bpn/eDG6Q3Bd0BndO6KLivwXtRnSwYApaVCnJgYbArWe9Evl7DioWyTJCSKq8kJoLbF2BvIIvAcIjcAFz1mCQonLoJLUpedFBbIqzxpmHdW/46LVuwxuWOiYjSWrvhk/oqvW4rmf5JHMoIsZHn2vn4+uNdnIAVtwEJ9Kqo9z3e1uF7Tx+QbLZRk+3q4BvdsP7B2C+obCYJ47U5K968iSNbkHTpcAQ0vRO6EPABv94juf/3FEWO+IYG63CQh3TuDGeEqKQ7gkoI6f4KbE1JZFsRCC5oNHLzRr7E843a5kbEgcbmidxvql+X3+6TFvfAbvWqJE2B0gJDIVHk+pwFNa+A+xXd9c8BWjokNaxp7+Pe8GLJmVs+fwiSJ02OAoLRUfc5+NQCkiUjrLrp/DdbosMZrSqAqgHvBa6IcNEAgIG4O76tatGR1xV3REELyzfM3OD9JyqO3GNiJvJW/vHRGi6AkDoUutv6C+Rw+paEfFjAFRbVIz83UtAIQEHEH65h9iB3akQ6OIAEXYIoYKk0BK3u7jxG6kW3fcMHSuUlQXENsq3LvbnbNSvy0oLNENVSkrc19JiJj9r1yQI+yvYWzMoPvtk2dkRzGoQ49EbDIIXgsE6kx1G51SA/t+0LIC65sE1F40NcbdnCYohkJtlBCdsJje7bb+eARjbh0Lo7srpQCOHLP+3oQb9VG6jIy0AVaCEAKMWlrmfqUjdbCInCet/bFDWMvgoEBqQzFQWEgmqF+CnbhwafaKHaD+OorFr4VAYAK2lZd74OWqY9S4kmDQ80PvmzRnM+mZ/Cxnf0BgraMnC3Vndu1aNQb3aa+fHLNzEboHjR7T0d9xQekFuGZ0uSdg046TFGFbphSHj19Mz97bvWOQtgmKga57UXaFvkmFG0ePFynYtKxB1F5p1KA2wX2jrLJ+o3IqmLOmQT0HAfV8J//2gUlzNgk8OrA1ErBOwtR1PTo2fy72gVreNXTcCB+8dKkkacW25Rl7MS9BkFUR7MfDQOptPF75sjoUpgaU7h3ubBaQd+TURd3unLz6O4OMDkjfmIH3UXtElcVIjU9dpCMvxP4m58V/cVOZXi5sbTPZX+Z+uXnH4eeGd/H1BuOOMSc1Qlo/5J9+6b30s4XFgBRN1DnsT35uWLg2MFgGUU6b3434KPLv+ik/tE201agOQcmv13/2zTWFFyFCB/OFjQfrs/O/yP6JwkH0R2QYqF+XUg3gn69IH5dRqr9DwQ5JpVllJlBHfjbcYM8PDX/hnUxwehS/KcpCsBYB/tERdyt30raqaegLNcCMB/j7fPx2zOjJK88WlaIdBc+nvNZlH/r86yVXGMRd3obSmSeT4nnYChgoG7Gk/HVA04Z+NjYla0FsrYaf1fBUp4DIKrlb6mG2po3rrJwx5PDhiy99sP4IaKKiZw4ppAFt8VL6ETR0p51Cmr6WELX/4Jmxb65DZ4+WVLfT8GpSq1q+dO89zNIhpFF4+4aAMYQkUIGAXMX3v0cDYVX2xQ6VTISMQMgO/PFq4O/65L2nDhw//+a8TXn7z4IaU9szdfhgcdOehEwuamBoUJ0/xf2mSaM6hnBahpiO0Zl+LPmGeIorGO50n8NncvmMN2vi/9H0GFoBOCV2558gowbnbTVvWs/Px+VEGjK4Tf0PJ/bCLnQM82k+9ykra3B0KDbz26kvTbp9oI7NMv427iHbWFDSj5LKiJSV/VUZeH5tW9XHL9Rx+p/cGVf6q9OHeHOLRv6z/9zXrpowcepc0RmAqNSgCVLq5+vVtHFdTtW58qfOyxHEq+fYtXlaOaMJ42ucT7kVWLxdywAKSVVZqs1JkoKruRsf/7V0g56Upi4ykl2xW6i0iaFGP0CgXmUZNWU5T4dQMKQfZqQgXGfjddRIy+p0j/tRzKqNnWI6xIX/Gvj7wg/Tx0yRH6JhYfcHX3dir5Lx83iqJY24QFZPXy17IoDyn+47CXLTyVOYg4DFnRyw0/Dlv8VG6IncrtX+ciFbSoTi7hSE4GU1I+4cTbnyXJVsJSDoToJI7Xt0/l7ntR3SyzTlBsaN1KM0sitTSb0ZpyjgLvgQaRroa/aioTA00U52qiz5wtzbKRuK2nrtrzzgdnJQ84DbNwgnxyjsd20AWfnQqFQnBLgjIspJNZWZJff9N8CdX17jcyuIZ3hhHzh33tEvPK0Uc+6pUlUreXLZ8/6r32XV1zbKKgZVT3KjrHSP/5muvv+h8StPb/74lac3f7gspjbnHqPvfbnlGmVv3cF1EUi7Yulcse3w/wH1pYbreqBNGgAAAABJRU5ErkJggg==" />
                                        </td>
                                        <td style="width: 60%; text-align: center;" id="title">
                                                
                                        </td>
                                        <td style="width: 20%; text-align: right;">
                                                <img id="siteLogo" style="height: 90px;" />
                                        </td>
                                </tr>
                        </table>
                        <hr style="color: rgb(153,191,217); background-color: rgb(153,191,217); height: 2px; border: none; margin: 0;" />
                </div>
                <div id="headFF" style="display: none;">
                        <table style="width: 100%; height: 100%;">
                                <tr>
                                        <td style="width: 60%; text-align: center; vertical-align: top;" id="author">
                                                
                                        </td>
                                </tr>
                        </table>
                        <br /><br />
                </div>
        </body>
</html>
