<!Doctype html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <script>
                        
                        function subst() {
                                
                                var matches = document.location.href.match(/^http(s{0,1}):\/\/([^\/]+)/);
                                var host = matches[0];
                                
                                var vars={};
                                var x=document.location.search.substring(1).split('&');
                                
                                for (var i in x) {
                                        var z=x[i].split('=',2);
                                        vars[z[0]] = decodeURIComponent(unescape(z[1]).replace(/\+/g, ' '));
                                }
                                var x=['frompage','topage','page','webpage','section','subsection','subsubsection', 'journalTitle', 'journalID', 'articleID', 'articleDOI', 'license', 'language'];

                                if(vars['page'] == 1) {
                                        
                                        document.getElementById('footP1').style.display = 'block';
                                        
                                        document.getElementById('title').innerHTML = vars['articleDOI'];
                                        document.getElementById('license').setAttribute('src', host + '/system/getPicture/lic/' + vars['license']);
                                        document.getElementById('journalLogo').setAttribute('src', host + '/system/getPicture/bl/' + vars['journalID']);
                                }
                                
                                if(vars['page'] > 1){
                                        
                                        document.getElementById('footFF').style.display = 'block';
                                        document.getElementById('journalTitle').innerHTML = vars['journalTitle'];
                                        document.getElementById('secRight').innerHTML = vars['page'] + ' / ' + vars['topage'];
                                }
                        }
                        
                </script>
        </head>
        <body style="border:0;margin:0; font-size: 8pt; font-family: arial;" onload="subst()">
                <div id="footP1" style="display: none;">
                        <table style="width: 100%;">
                                <tr>
                                        <td style="width: 20%; text-align: left;">
                                                <img id="journalLogo" style="height: 50px;" />
                                        </td>
                                        <td style="width: 60%; text-align: center; vertical-align: middle;">
                                                <span id="title"></span>
                                        </td>
                                        <td style="width: 20%; text-align: right;">
                                                <img id="license" style="height: 35px;" valign="middle" />
                                        </td>
                                </tr>
                        </table>
                </div>
                <div id="footFF" style="display: none;">
                        <table style="width: 100%;">
                                <tr>
                                        <td style="width: 20%; text-align: left;">
                                                
                                        </td>
                                        <td style="width: 60%; text-align: center; vertical-align: middle;" id="journalTitle">
                                                
                                        </td>
                                        <td style="width: 20%; text-align: right;" id="secRight">
                                                
                                        </td>
                                </tr>
                        </table>
                </div>
        </body>
</html>
