<?php
        /**
         * @return string[]
         */
        function getCountrylist(){
    return \Drupal::service('country_manager')->getStandardList();
    return array("Afghanistan","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antarctica","Antigua and Barbuda","Argentina","Armenia","Aruba","Ascension","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia and Herzegovina","Botswana","Bouvet Island","Brazil","British Indian Ocean Territory","Brunei Darussalam","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central African Republic","Chad","Chile","China","Christmas Island","Cocos (Keeling) Islands","Colombia","Comoros","Democratic Republic of the Congo (Kinshasa)","Congo, Republic of (Brazzaville)","Cook Islands","Costa Rica","Ivory Coast","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","East Timor Timor-Leste","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Guiana","French Metropolitan Areas","French Polynesia","French Southern Territories","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Great Britain","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guernsey","Guinea","Guinea-Bissau","Guyana","Haiti","Heard and Mc Donald Islands","Holy See","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran (Islamic Republic of)","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Korea, Democratic People's Rep. (North Korea)","Korea, Republic of (South Korea)","Kuwait","Kyrgyzstan","Lao, People's Democratic Republic","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macao","Macedonia, Rep. of","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia, Federal States of","Moldova, Republic of","Monaco","Montenegro","Mongolia","Montserrat","Morocco","Mozambique","Myanmar, Burma","Namibia","Nauru","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Niue","Norfolk Island","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Palestinian National Authority","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Pitcairn Island","Poland","Portugal","Puerto Rico","Qatar","Reunion Island","Romania","Russian Federation","Rwanda","Saint Kitts and Nevis","Saint Lucia","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Príncipe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia (Slovak Republic)","Slovenia","Solomon Islands","Somalia","South Africa","South Georgia and South Sandwich Islands","Spain","Sri Lanka","Saint Helena","St. Pierre and Miquelon","Sudan","Suriname","Svalbard and Jan Mayen Islands","Swaziland","Sweden","Switzerland","Syria, Syrian Arab Republic","Taiwan, Republic of China","Tajikistan","Tanzania","Thailand","Tibet","Timor-Leste (East Timor)","Togo","Tokelau","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Turks and Caicos Islands","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","U.S. Minor Outlying Islands","Uruguay","Uzbekistan","Vanuatu","Vatican City State (Holy See)","Venezuela","Vietnam","Virgin Islands (British)","Virgin Islands (U.S.)","Wallis and Futuna Islands","Western Sahara","Yemen","Zambia","Zimbabwe");
}
        
        /**
         * @param $id
         * @return string
         */
        function getCountry($id){
    
    if(preg_match('/^\d+$/', $id)){
      $countries = array("Afghanistan","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antarctica","Antigua and Barbuda","Argentina","Armenia","Aruba","Ascension","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia and Herzegovina","Botswana","Bouvet Island","Brazil","British Indian Ocean Territory","Brunei Darussalam","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central African Republic","Chad","Chile","China","Christmas Island","Cocos (Keeling) Islands","Colombia","Comoros","Democratic Republic of the Congo (Kinshasa)","Congo, Republic of (Brazzaville)","Cook Islands","Costa Rica","Ivory Coast","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","East Timor Timor-Leste","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Guiana","French Metropolitan Areas","French Polynesia","French Southern Territories","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Great Britain","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guernsey","Guinea","Guinea-Bissau","Guyana","Haiti","Heard and Mc Donald Islands","Holy See","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran (Islamic Republic of)","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Korea, Democratic People's Rep. (North Korea)","Korea, Republic of (South Korea)","Kuwait","Kyrgyzstan","Lao, People's Democratic Republic","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macao","Macedonia, Rep. of","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia, Federal States of","Moldova, Republic of","Monaco","Montenegro","Mongolia","Montserrat","Morocco","Mozambique","Myanmar, Burma","Namibia","Nauru","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Niue","Norfolk Island","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Palestinian National Authority","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Pitcairn Island","Poland","Portugal","Puerto Rico","Qatar","Reunion Island","Romania","Russian Federation","Rwanda","Saint Kitts and Nevis","Saint Lucia","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Príncipe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia (Slovak Republic)","Slovenia","Solomon Islands","Somalia","South Africa","South Georgia and South Sandwich Islands","Spain","Sri Lanka","Saint Helena","St. Pierre and Miquelon","Sudan","Suriname","Svalbard and Jan Mayen Islands","Swaziland","Sweden","Switzerland","Syria, Syrian Arab Republic","Taiwan, Republic of China","Tajikistan","Tanzania","Thailand","Tibet","Timor-Leste (East Timor)","Togo","Tokelau","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Turks and Caicos Islands","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","U.S. Minor Outlying Islands","Uruguay","Uzbekistan","Vanuatu","Vatican City State (Holy See)","Venezuela","Vietnam","Virgin Islands (British)","Virgin Islands (U.S.)","Wallis and Futuna Islands","Western Sahara","Yemen","Zambia","Zimbabwe");
    }
    else{
      $countries = \Drupal::service('country_manager')->getStandardList();
    }
    
    return (string)$countries[$id];
}
        
        /**
         * @param int $errno
         * @return string
         */
        function getFileUploadError($errno = 9){
    $phpFileUploadErrors = array(
        0 => 'There is no error, the file uploaded with success',
        1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
        2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
        3 => 'The uploaded file was only partially uploaded',
        4 => 'No file was uploaded',
        6 => 'Missing a temporary folder',
        7 => 'Failed to write file to disk',
        8 => 'A PHP extension stopped the file upload',
        9 => 'Unknown file upload error'
    );
    return $phpFileUploadErrors[$errno];
}
        
        /**
         * @return string[]
         */
        function getDefaultPic(){
  return array(
    'content' =>
'89504e470d0a1a0a0000000d49484452000000010000000108060000001f15c48900000006624b474400ff00ff00ffa0bda793000000097048597300000b1300000b1301009a9c180000000774494d4507e0030b1339045db818790000001d69545874436f6d6d656e7400000000004372656174656420776974682047494d50642e65070000000b4944415408d7636000020000050001e226059b0000000049454e44ae426082',
    'type' => 'image/png'
  );
}
        
        /**
         * @return string[]
         */
        function getDefaultUserpic(){
    return array(
        'content' => '89504e470d0a1a0a0000000d49484452000000eb000000eb08020000000768f682000000017352474200aece1ce90000000467414d410000b18f0bfc6105000000097048597300000ec300000ec301c76fa8640000001a74455874536f667477617265005061696e742e4e45542076332e352e313147f342370000170649444154785eed9df757e2dad787bf7ff5eb58b02252c40616047bc1de7bef8ebd8d58b18020a088208ecc6fef2739192fa38ee31d993b1eb29fb597cb12c3c9394f76f64942f8dfd7af5fef09825bc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc860826fc8e058830e7d156931224690c13180a9c97878788844f3e427116951eaf9584006bf0bf49ea0ecb76fcccef05d387013b8f2fa2e5d2ee7f9f999dd7e72747c7a6c779c9db92f2e7c5e6fc07f73170ae1bfb03cfe87d92cad8bf82dc8e0df411297591bbabbf2f9ceec27bb5bdbcbf30b5323a343ddbdbdad6d9dd6a6d6bafae69ada96dabaf6066b774beb6057f7c4d0f0e799d9edf50dfbe1a1c77d19bcbd85c26c3d3410bf0719fcef40774138100e87916b4f8e8ed69796c60706dbea1baa4a4a8b75fabcac6c6d6aba3a255595ac5025a56427a5e0ab2a39252759a151a4e93395068dd652646caaae19eaee599a9d3bb0d93c6e772818147609f2f8df4306ff0b2019401900e776b7b79150ad553525b979b9199910343b3159f9298905be7ff2e33fdf8b3f426b886e506b6b4de5c8cd1bcb2bceb3f3db4000c38197a041793b64f09b6062dd87c33e8f77676363a0b3b3c2588c749b939c1a6da720a8e8ee6bf1e3c248d5baf4cc527d7e4763d3cac2e285c3110a86c43d853c7e1364f0af6136611276b06b1beee9ad3014eb33b35018448bf87b11bd066d5a063ceeb4366facac62cec7f6191a9d5f4206bf06d308dfb81ccef9c9a97a734581320705803221f13de23e0fac2d2b415821f231f690d1befee38343561cd300bd0e19fc5398bec1dbe0816d6fa0a3ab4c9faf4e5664c5dadde890f271524aa14add5ad780e2f8dae74333688c5e810c7e1974cbc3d707fff5f5e6ea5a4b6d5d7e5636ab5f9f38f72782798c795e75a90989df7de1121a231e0a88e790c12f803e01575eefcafc42adc90c990477ff137d1f03af88495e595ec1c4e030a677681549fc2264f05398be3e8f77717aa6d258ac4e49fdeff56521bc6e6272b1361765f1c5b903e344123f870cfe01f4062c41f5f97976d65c64c84956308dfe56b057376a7482c40e87b873d178fd0019fc0fe88a482412b8b959fbbc54555c2265df6756fdc7f198892786862f5d2ee1dc040d591464b084a0af78bdedcbe66683a542a348fb08fab2404b544929a6fc8285a9691c1fa89688860c9610b408870f6db6b6ba7a5d7ac6c7d19705da8392a6a6ccb4b9ba7a275eb493da2d7bc8600174029c70399d039d5d7999ca8fa62f0bb44a9b9adeded0683f3c448369e01864b0a46ff0f676696ebebca010a27c4c83d909e9c21cf568ff80cfeb459b69ec00192cd40fe88493a3a396daba0f55fe3e0fd6b64a63c9e6ca2a4a76aa2580dc0d6609f8c6ef9f1c1e3168b41f595f1668a12e3db3aba905350f1a2ff3e103723798a5b1a3bdfd3ab3f9af9ffd7d4ba08588b2bcfc958545e12d1eb24fc37237381289f8afaf2787860b556a6542e2135d3e666425246ad3323a1aad170e072b81a48d9125b236980dbffdf0a8de5cf141ae5fbc25583bcbf20a500db33b30a5ed9125b236985d815b9c992d5273500147070e17b91959835ddd1eb71b5b216d8f2c91afc1d8708c3d0ec49d4dcd9ad4741c9a9f58f29103fb9b7081a3d47460b36143643b8840d606e3ab6d7bc762300a6fbbe02a070bf129a948a5fe3c337b1b08c8390dcbd7605642cc4f4e1564abfecacd93ef0cec729ad4b4fef64ef7c505192c4730ea2ea7b3afadfd835fc5f859a0cd387434982b8ef6f6e53c9993b1c10f0f47fbfb0d964a2e4b88c73312fafc8de595fb7058b6e3285383b1d55fefbf6eafad9717143ddac05da0d9f94ad5ecf8044a61210dcb7228656a30c63b140c09e7d172344fb4e02860b0362d63a8a7d7ebf1a02892e750cad1606c32c6db7fed9f1c1ad167663dd182a380c1aa64456753b3f3fc9c0c9611cc60efa567b0bb47c3de87fc4c0e2e82b5bcb9baf6e4e8880c961182c1dfbeb99cceee96562eeee6f959a0e5cac4e47ab3e5607757b6cfbd94a9c1df2211c7d95947a35595c8e589081682c19f92aa4b4dbb5bdbec1e0f690be5848c0d3e3d6baf6f641e446bc15130832b8cc5dbeb1b82bf64b04cf8c7e0867830d85264dc5a5d238365449c195c595cb2b34139584e30839d67e71d8d4dd99cd7c1d99f926aca4cb6ed1daa83650436391289b89d173dad6d39fcdcd8fe3c98c10de68a439b8dce45c80866b0cfe319eaee959e4bf94c0e2e82b5bca5a6eef4e898ce07cb086c328eb9377effd4c8a8fea33edfe42d8196e7242bba9b5b5d0e27ea22325846c0e0bbd0ddd2dcbc41ad557e4f66dc059aad4bcf18edebbff2fa2807cb0b6c35d8d9d8341719990ad16670116833a250a55e989a0ec9f86df73235182069d90f0fad55d5ec53899ef8f1f183b5b9bcb0686b6d1d4328db7194b5c1972ed7406717a79339b45995acb056d5603f946d0901646df06d20b038335b98a37e2207170183859b83bb7b3cee4b6c8bb455f243be0663c3c1feee6e557129a76f343268742bf30bc1dbe003192c4f90badc1717bdaded48663c3e2fa2de6c39de3f90edf56486dc0d0e06022b0b0b466dae9083f949c3ca84447da672b4afdf27bebf48da1e59226b83c5e4f5f5cc6eb75655ab1569bc3cf94f98c325a6980b0d5f36b7eeeeeee45c4200591b0c90c06efcfed9f109835a9bc5490e660f4d43f173e972435f998fa0dc0d166ee9babf3f393ab656220df370978fd8424ba16173752d448f712783012408dcdccc4d4c96e8f41fbf1a4602cecbca1ee8e8f45e5e8ab7a3c97df8c860a91a769c9e755a9b74691fee73b8a243a8809353ea4c66dbf6f67d384c091890c10250e12e74b7b9b25a55529a9d2488f2449d0f11e28d10c5badc99b131fff5b550ffd0d891c10c740284b8f2faa686470d1addc7bc5b0d4dc204aea7b5cd717a8a06d3c031c86009f40372daf9c9496f5bbb74d3f0479218edd128d2ac9555b6ed9d30d50f5190c1ff002d20c7c1aeadb9a656fb910a62b424275951595cb2beb4243de48ff80e19fc039023180ceeac6fd4959b3fc803a9d00655b2c2945fb8382d7d2c380d593464f00f08b584f86cf7f5a5e5ea32d35f7f38b6a06f528a29af60767c42387d86f6d178fd0819fc144162e15d74d76b9f979089ffe2ddc3785d140fe50545d0d7e3727f15cffb49ad24be4306bf009338e0bfd95a5d6baeaed1676441a6ffd863bc1c8e005525a50b53d3c8be82bc34522f4106bf0cba45a8896f6ff77676ba9b5b0c6aed7f760fb1f82a4979594a6b55358e03d7beab07e134090dd3cb90c1af0189c3777767c7f6b181414b91919da0f8731eb395ab53524b72f5bdaded7b5fbe889f9c2cf77b775e870c7e0d9689d1413e8f0773bbd6ba7a2463f6981fc1e34f4f15fced602b449acf5766a3f89e9b98bc3877b0f3be3440af4306ff1a680442c1e099fd64666cbcaedc52a054618ef5689e60f33329df128fff0b77516d57188a877bfa0e766d37d7d76ce7915a40fc1c32f84d3c26e3ebabab439b6d6a64b4b1a20af958ad487b14f151c7d7237a616562724e8aa2205b55536a82bb5f36363dee4b4abdff0a32f805d0270ce9e7efe03711e4c570f8caeb3bdadf5f9c9ee96969ab2e292bcad16853d3999d598884c41f347d0cfc5e0c2c8962375fa932171ada1a1a91d7315ff4b8ddc21b2e449ebcb4d81601e967220a32f847c45c1b897c8b883c9709880b08b3abdb4000d5eaeee6d6fce4547f475773756d657149496e5ea14a9d97a9cc4dcf84d61a451abeead233f599ca82ec9c626d2e66848d15953d2dad48e49b2bab6776bb503388ef1679e5e522ec0bd515cf2083ff01f2c212a458efa5c77e7088aa97dd84f06217e1978f2a076f839ecbcb93a3a39df58da5b9b9e9d1b191befe81ceaeded6b6eee616e4e9818ecee1debec9e19185e919587bb4b7efbeb8080402588f60e52b2f1179b80b85b09fe05f5c0e276a717171f2f81fc86001a623be4199bbff6577b46fa0c152d956d7b0f679f9caeb657ffd5947b1bfc26569997038140a056e6eb02a9fc7e3bdbc445cfb7c377e3f440fdfdd612de2e22f675c09b64ef1cd23b6ed6dec0975e5e6beb6f6adb575ac8d0ae568c86051c1af5f43b7d2a9865a53396a0055528a46916e3114e337ceb37324c2d7a5c19f24b09818585a705084fd5efa260ae99fa3f92a1c0a0034f5b8dccbf30b0d968adc8c4cb4479796810a64a4b7efd0b617f0df08eba2642c738345070413902371706f6f68144ef78a9f30a74c104295ac40e58a32c0b6b58d24fab83c3cfb25582c1afc2cfde127b0c5447b850afb706f6fa8a7d7945fc8ee2e62219e3056c1694c225187b0371a092b9731f23598e982c3faf9c9e9e4f0302661986f3151a2cf7ce12b2661752633923192342ad1572ad7df46688c505844b07eb4676e72aab1a20a333f28fba43d08b522ad2caf0073c703dbde6d4078eeaa9c2596a9c14c5fb1cadce96e69356a744f5c790c264d4e8aa258978b24bd3437ef383d133c1679a73dac196c3dc8bbd8433ecfceb537584b72f3d48f57fe5e6a52f6a76424e3e6eadab5cf4b575e59df342c4783993718780cbfb5b23af70db79eb10534a9e9487e5dd6667876727884b91a8ee3f0efdb37e1ecdbdb35121a20fe17fe051536266707bbb6b9c949ec216579f9eccefa5fb7272109957a5571e9ecc484db79c1364a7a0139213b8399671eb77b616abaaaa4f4b1ca7ca2c88bc196c4bf94e8f42d35755323a33b1b1be7a7a728a39195d18f58b990997f051a80e5afbcded3e3e3f5e5654cceeacd1545399a57f2ee8b812571e828d5e78df60f384e4fe55916cbcb6036c098034d0d8f6092c4266d4fb4783d1e0dc3ffe665650b17d5ea1b26868621e281cd8632c0e57422a7c24effd5d5cdb51ff3bfc7f05f5de3f77875fbe1e1c6f2ca58ff4053754da93e3f375d38d5c0d6fc1bedc157835ad3dfd17972748cb25e6e12cbc8606168efef5d0ee768df0092e8cf0adfb784a49a980231afc254af589b5b61286eb05440e89ed6b6c1aeeeb181c1c9e111e4e9c7181f1c1aeaeaee68b4d69acad1007d461692eee3daded3187cc5b4afaba9e5f8e090492c6db30c908bc16266babf703870c8366a7f3a6ffbb7f1281f42b039251555ac2e2d03b5353234265bf9d9398f81dfe0f7dad4f498dcd7161d6c3df9caeccea6e6e38303767f854c46561606631b31a238bee3c00d7dd990471b1093601a652524fe32fedcab43629689e55313c7bfc14c5f4cdd701c2fc9d5b3c18e1efbb889ef12abfada3aceec76b6e1522fc42f716e301bc52b9f6f7e6ababca03056c5c3870d2671618e7ab8a71725931cd2709c1b8c21bcbd09ac2e7eae2a2efd8d330f3c06dbc6629d1ec71c9fc72b281cd7431ccf06435f4ccc6ddb3b8d1555c2870cc8405f16d8521c6d2c45c6e5b979f11ea078cec4716b303b809e9f9c743535b3073e3c19e6f80e6caf3a25b5de6cd9ddda6637d649fd1277c4a7c1d8288c198ea163038306b516c329378311ca8424ecba9dd6e6f393538c71bca6aaf83458287f0342f96b2e34c4fdeced6721eeb7c9456aedc4d030766676355beaa038220e0d168e98e1f0d1fe7e5375cd0779fee4df0a6cbb2a49613118375756c487a7c4612d116f066373906c3c6ef7687f7f7eb64acefab2400f6853d3dbeb1b4f8e8ed0397136dc20de0c469a090583eb4bcb72ae1fa243e8814f49852af5d4c8e895cff71089b7f312716530b605e3737a6cef68b4b2db269f0ca73c03fd809db9a6ccb4b3b1190e87c9e00f8aa0aff8c8d4b98949a346876123831f035d919b9135d0d97579e18ac4d785bab832185b7268dbb35656cbe4f2dbdb43e88d4fc9a8acd6163f87437175fb659c188cadc0a8f8afaec707870a551ad2f779a04f7469195d4d2d2e87137d1537992b4e0c16924af87effcb6e5db98526703f0b748b29bf706561319eaed2c583c1df13f0d558ff40814a9d253e5a8fe27928131291863bad4d2ea7336e6e9688078359023edadbaf2b375305fc5a88b35b537ec1dae7cfec8101520ff24c9c187c737d3d3d325a94a3a104fc7aa07f74e999ddcd2d97172ef45b1c8c3ef706a3fde0cc7e62adaa562b52d9d379297e16ec00652e2cdad9d88c8f6a987b837128bc0ddc2e2f2c1875b9c2f05009f1ab401ad6672a477afbbc979e382824e2c16097f3a2a7a51573142a21de12d8cf315ba8359563e6201ec0f816806f83c5395c78776bdb5264a493686f0df14855a4522f4ecf046e6e784fc37c1b8cdebfbeba9a1a19cbcbca267ddf1ee82b8d22adabb9c5797e8e3ee4da01ee73f0e9b1bdb5ae9e3d71ecc93851fc2cd057aac4e40a8311f339084006ff1dd0f2f0ddddc6f28a29af808d4af42051fc32f295aa99b1f1809fef42825783d16ca184f05d8d0f0ce933b29e8c0dc52f033b7c4e8aa2d3dae4383de3ba90e0d960b18468ab6bc849a2eb70ff3ad063884a63c9cefa063a93530d00c706a3dd5bab6b9622231b8c272344f1cb40a715aad4b36313d25be8f834814b83d166f47830703b3d3a56909d43fafe5ea0df348ab4ded6b64b97eb81db428257835142b82f2eba5b5a65f5309e3f11f5e68a03db1e3282908639844b8385cefefa7060b3d59b2d4a3a0bf18ec84a482ccb2b589e5f08df094f6b95fa972b7835f83e1c5e595834e517d0ad3cef096582f0b4d6b1fec1db1be1f377a5fee50a5e0dbe0d04c607878422980c7e47b052b8d3da74e9728bf75af227037f06a3c130d87b79d9dddca295f72379621528c6ec0787ac63a55ee607fe0c462fa3cda7c7f6c68a4a55520add4ef9ce40295c5e50b4b1b2c2e947c87069308ae02f1b9b1586624ac0ef0f186c506b67c6c7397db01a97068782c1c599d9629d9e6e087e7fa00ff559cac1ae6effd53519fc5f805e0edcdc8c0d0c621a4706bf3f84c95c6a7a5b7da3705d83c377ce7169f095d7dbd3da969b9e4906bf3f6030a61375e5164c2d200377699833835917bb1ccee6ea5ae17d9d5407bf3fc43eb4188c7b3b5fd8a7d0497dcd099c19cc0e73f6c3a39ab2727a5b51ac0287b292dcbcf5a5e5bb5088bb7b85793338120987c3b6ad1d73a181f48d55c0e0a21ccdc2d4f46d204006ff59d0bfc813c816c8195404c72ad093f9caecf1c121fff53519fc6741ff0603b78bd333c2e379fe8f0c8e4dc0e0dc8cacfe8e4e9f57f8c018a9af39813f836ffcfea9e19102a58a0c8e55c0606d5a7a47a3f5d2e52683ff2ce8df6b9f6fb8a7579fa9a42a2256819e542bd29aaa6b2e1c0ec1609e8ce0d0609fc7d3dbd6aea393c1b10bcc89739215f566cb99ddcedd1d6afc19ec71bb71bcd3a6a693c1b10ae1a246b2a2bab4ecf8e0003690c17f1018ec723a5b6aeb348ab42c3a9b16a3100c4e4ab1188c07bbbbf76118ccd3450dae0c162fc839cfcfa507ad92c1b10af1113ee50585b6eded306f97e57832184d45e79e9f9c34582a72e83153310cb127cbf2f2bf6c6c72f78639ce0c06a7c7c7b5a67255720aea60484c11ab306a73b756d7c27777c2e9087ee0cf60f1a60813ea363238b661d4e46e92c17f1434150738f7856ba0b3cb945f60d0688d5a1d450c42a32bd6e99b6b6a8577cb89b74f493dce035ccde4c4ce0d05432824d6969696e6e696e7e729de1fe8c99585c5fd2fbb01bf5f3c1dcc93129c19cc328498243069bea3885d84e12e3a962f1f00670633842c01842ea788517c47ea627ee0d2608278840c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f8860c26f846309820f8e57f9148e4812038e5e1e1ff01316fbcf451b347600000000049454e44ae426082',
        'type' => 'image/png'
    );
}

if ( !function_exists( 'hex2bin' ) ) {
        /**
         * @param $str
         * @return string
         */
        function hex2bin($str ) {
        $sbin = "";
        $len = strlen( $str );
        for ( $i = 0; $i < $len; $i += 2 ) {
            $sbin .= pack( "H*", substr( $str, $i, 2 ) );
        }

        return $sbin;
    }
}
        
        /**
         * @return string[]
         */
        function getStartletters(){
    return array(
        0 => 'A',
        1 => 'B',
        2 => 'C',
        3 => 'D',
        4 => 'E',
        5 => 'F',
        6 => 'G',
        7 => 'H',
        8 => 'I',
        9 => 'J',
        10 => 'K',
        11 => 'L',
        12 => 'M',
        13 => 'N',
        14 => 'O',
        15 => 'P',
        16 => 'Q',
        17 => 'R',
        18 => 'S',
        19 => 'T',
        20 => 'U',
        21 => 'V',
        22 => 'W',
        23 => 'X',
        24 => 'Y',
        25 => 'Z',
    );
}
        
        /**
         * @param $id
         * @return string
         */
        function getStartletter($id){
    $startletters = getStartletters();
    return $startletters[$id];
}
        
        /**
         * @param $dbo
         * @param int $min_weight
         * @param int $max_weight
         * @return array
         */
        function getUserroles($dbo, $min_weight = 0, $max_weight = 1000){
    
    $result = $dbo->select('rwPubgoldRoles', 'ur')
        ->fields('ur', [])
        ->condition('weight', $min_weight, '>=')
        ->condition('weight', $max_weight, '<=')
        ->execute();
    
    $userroles = array();
    foreach($result as $res){
        $userroles[$res->id] = $res->name;
    }
    
    return $userroles;
}
        
        /**
         * @param int $length
         * @return string
         */
        function genTempPassword($length = 8){
    
    $password = '';
    $pot = array(
        range('A', 'Z'),
        range('0', '9'),
        range('a', 'z')
    );
    
    while(strlen($password) < $length){
        
        $index = rand(0, count($pot) - 1);
        $password .= trim($pot[$index][rand(0, count($pot[$index]) - 1)]);
    }
    
    return $password;
}
        
        /**
         * @param array $role_id
         * @return array
         */
        function getUseridsByRole(array $role_id){
	
	$uids = [];
	
	foreach(  \Drupal::database()->select('rwPubgoldUserroles', 't')
			  ->fields('t', [])
			  ->condition('role_id', $role_id, 'IN')
			  ->execute()
			  ->fetchAll() as $res){
		
		$uids[] = $res->user_id;
	}
	
	return $uids;
}
        
        /**
         * @param $dbo
         * @param array $role_id
         * @return array
         */
        function getUsersByRole($dbo, $role_id = array()){
    
    $result = $dbo->select('rwpgvwUserRoles', 'ur')
                  ->fields('ur', ['user_id'])
                  ->condition('role_id', $role_id, 'IN')
                  ->execute()
                  ->fetchAll();
    
    $uids = '';
    
    foreach($result as $_){
        $uids .= (!empty($uids) ? ',' : '').$_->user_id;
    }
    
    if($uids != ''){
        
        $sql = "SELECT `up_uid`, `up_graduation`, `up_lastname`, `up_firstname` FROM {rwpgvwUserProfiles} WHERE `up_uid` IN($uids)";
        $result = $dbo->query($sql)->fetchAll();
        
        $return = array();
        
        foreach($result as $_){
            $return[$_->up_uid] = implode(' ', [$_->up_graduation, implode(', ', [$_->up_lastname, $_->up_firstname])]);
        }
        
        return $return;
    }
    else{
        return [];
    }
}
        
        /**
         * @param $dbo
         * @return array
         */
        function getBookCategories($dbo){
    
    $cats = $dbo
        ->select('rwPubgoldBookCategories', 'bcat')
        ->fields('bcat', [])
        ->execute()
        ->fetchAll();
    
    $categories = array();
    
    foreach($cats as $cat){
        $categories[$cat->bcat_id] = t($cat->bcat_name);
    }
    
    return $categories;
}
        
        /**
         * @param $bcat_id
         * @return mixed
         */
        function getCategoryName($bcat_id){
    
    $result = \Drupal::database()
        ->select('rwPubgoldBookCategories', 'bcat')
        ->fields('bcat', ['bcat_name'])
        ->condition('bcat_id', $bcat_id, '=')
        ->execute()
        ->fetchAssoc();
    
    return $result['bcat_name'];
}
        
        /**
         * @param $text
         * @return string|string[]
         */
        function normalizeInlineImageLinks($text){
    
    return $text;
    
    $replace = array();
    preg_match_all('/^(.*)<img.+?src="(.+?)".*?>(.*)$/m', $text, $all_matches);
    
    foreach($all_matches[2] as $match){
        
        if(preg_match('/^.*\/(.*)$/', $match, $matches)){
            $replace[$match] = '/publisso_gold/pic?pth='.$matches[1];
        }
    }
    return str_replace(array_keys($replace), array_values($replace), $text);
}
        
        /**
         * @param $dbo
         * @param null $aoe_id
         * @return array|mixed
         */
        function getAreasOfExpertise($dbo, $aoe_id = NULL){
    
    $aoe = $dbo->select('rwPubgoldAreasOfExpertise', 'aoe')
        ->fields('aoe', [])
        ->execute()
        ->fetchAll();
    
    $ret = array();
    
    foreach($aoe as $_){
        $ret[$_->aoe_id] = $_->aoe_name;
    }
    
    if($aoe_id === NULL)
        return $ret;
    else
        return $ret[$aoe_id];
}
        
        /**
         * @param $dbo
         * @param $uid
         * @return mixed
         */
        function getUserAreasOfExpertise($dbo, $uid){
    
    return $dbo->select('rwpgvwUserAreasOfExpertise', 'uaoe')
        ->fields('uaoe', [])
        ->condition('uaoe_uid', $uid, '=')
        ->execute()
        ->fetchAll();
}
        
        /**
         * @param false $lang_id
         * @return array|mixed|string
         */
        function getCorrespondenceLanguages($lang_id = false){
    
    $lang =  [
        'de' => t('german'),
        'en' => t('english')
    ];
    
    if($lang_id === false)
        return $lang;
    elseif($lang_id === null)
        return '';
    else
        return $lang[$lang_id];
}
        
        /**
         * @param $id
         * @return string
         */
        function getSalutation($id){
    
    $salutation = [
        
        0 => 'Mr',
        1 => 'Ms'
    ];
    return $salutation[$id] ?? '';
}
        
        /**
         * @param $dbo
         * @param $bk_id
         * @return array
         */
        function getBookEditors($dbo, $bk_id){
    
    $result = $dbo
        ->select('rwpgvwBookEditors', 'be')
        ->fields('be', [])
        ->condition('bk_id', $bk_id, '=')
        ->execute()
        ->fetchAll();
    
    $editors = [];
    
    foreach($result as $_)
        $editors[]['up_uid'] = $_->up_uid;
    
    return $editors;
}
        
        /**
         * @param $dbo
         * @param $bk_id
         * @return array
         */
        function getBookReviewers($dbo, $bk_id){
    
    $result = $dbo
        ->select('rwpgvwBookReviewers', 'br')
        ->fields('br', [])
        ->condition('bk_id', $bk_id, '=')
        ->execute()
        ->fetchAll();
    
    $reviewers = [];
    
    foreach($result as $_)
        $reviewers[]['up_uid'] = $_->up_uid;
    
    return $reviewers;
}
        
        /**
         * @param $dbo
         * @param $jrn_id
         * @return array
         */
        function getJournalEditors($dbo, $jrn_id){
    
    $result = $dbo
        ->select('rwpgvwJournalEditors', 'je')
        ->fields('je', [])
        ->condition('jrn_id', $jrn_id, '=')
        ->execute()
        ->fetchAll();
    
    $editors = [];
    
    foreach($result as $_)
        $editors[]['up_uid'] = $_->up_uid;
    
    return $editors;
}
        
        /**
         * @param $dbo
         * @param $bk_id
         * @return array
         */
        function getBookEditorsInChief($dbo, $bk_id){
    
    $result = $dbo
        ->select('rwpgvwBookEditorsInChief', 'eic')
        ->fields('eic', [])
        ->condition('bk_id', $bk_id, '=')
        ->execute()
        ->fetchAll();
    
    $editors_ic = [];
    
    foreach($result as $_)
        $editors_ic[]['up_uid'] = $_->up_uid;
    
    return $editors_ic;
}
        
        /**
         * @param $dbo
         * @param $jrn_id
         * @return array
         */
        function getJournalEditorsInChief($dbo, $jrn_id){
    
    $result = $dbo
        ->select('rwpgvwJournalsEditorsInChief', 'eic')
        ->fields('eic', [])
        ->condition('bk_id', $jrn_id, '=')
        ->execute()
        ->fetchAll();
    
    $editors_ic = [];
    
    foreach($result as $_)
        $editors_ic[]['up_uid'] = $_->up_uid;
    
    return $editors_ic;
}
        
        /**
         * @param $dbo
         * @param $bk_id
         * @return array
         */
        function getBookEditorialOffice($dbo, $bk_id){
    
    $result = $dbo
        ->select('rwpgvwBookEditorialOffice', 'beo')
        ->fields('beo', [])
        ->condition('bk_id', $bk_id, '=')
        ->execute()
        ->fetchAll();
    
    $ret = [];
    
    foreach($result as $_)
        $ret[]['up_uid'] = $_->up_uid;
    
    return $ret;
}
        
        /**
         * @param $dbo
         * @param $jrn_id
         * @return array
         */
        function getJournalEditorialOffice($dbo, $jrn_id){
    
    $result = $dbo
        ->select('rwpgvwJournalsEditorialOffice', 'jeo')
        ->fields('jeo', [])
        ->condition('jrn_id', $jrn_id, '=')
        ->execute()
        ->fetchAll();
    
    $ret = [];
    
    foreach($result as $_)
        $ret[]['up_uid'] = $_->up_uid;
    
    return $ret;
}
        
        /**
         * @param $dbo
         * @param $cf_id
         * @return array
         */
        function getConferenceEditorialOffice($dbo, $cf_id){
    
    $result = $dbo
        ->select('rwpgvwConferencesEditorialOffice', 'ceo')
        ->fields('ceo', [])
        ->condition('cf_id', $cf_id, '=')
        ->execute()
        ->fetchAll();
    
    $ret = [];
    
    foreach($result as $_)
        $ret[]['up_uid'] = $_->up_uid;
    
    return $ret;
}
        
        /**
         * @param $dbo
         * @param $bk_id
         * @return array
         */
        function getBookAuthors($dbo, $bk_id){
    
    $result = $dbo
        ->select('rwpgvwBookAuthors', 'ba')
        ->fields('ba', [])
        ->condition('bk_id', $bk_id, '=')
        ->execute()
        ->fetchAll();
    
    $authors = [];
    
    foreach($result as $_)
        $authors[]['up_uid'] = $_->up_uid;
    
    return $authors;
}
        
        /**
         * @param $dbo
         * @param $uid
         * @return mixed
         */
        function getAllUserData($dbo, $uid){
    
    return $dbo->select('rwpgvwAllUserdata', 'ud')
               ->fields('ud', [])
               ->condition('profile_userid', $uid, '=')
               ->execute()
               ->fetchAssoc();
}
        
        /**
         * @param $dbo
         * @return mixed
         */
        function getBooks($dbo){
    
    return $dbo->select('rwPubgoldBooks', 'b')
               ->fields('b', [])
			   ->orderBy('bk_cover_link', 'ASC')
			   ->orderBy('bk_title', 'ASC')
               ->execute()
               ->fetchAll();
}
        
        /**
         * @param $dbo
         * @param $bk_id
         * @return mixed
         */
        function getBook($dbo, $bk_id){
    
    return $dbo->select('rwPubgoldBooks', 'b')
               ->fields('b', [])
               ->condition('bk_id', $bk_id, '=')
               ->execute()
               ->fetchAssoc();
}
        
        /**
         * @param $dbo
         * @param null $wf_id
         * @return array
         */
        function getDashboard($dbo, $wf_id = null){
    
    $ret = array();
    
    //get workflows with direct association
    $where = "";
    
    $s = \Drupal::service('session');
    $user = $s->get('user');
    
    if($user['weight'] < 70){ //not Publisso or Administrator
      $where = "(
        FIND_IN_SET('u:".$user['id']."', `wf_assigned_to`)
        OR
        FIND_IN_SET('r:".$user['role_id']."', `wf_assigned_to`)
        OR
        FIND_IN_SET('".$user['id']."', `wf_assigned_to_eo`)
       )";
    }
    
    $sql = "
        SELECT
          *
        FROM
          {rwPubgoldWorkflow}
        WHERE
          ".(!empty($where) ? $where.' AND ' : '')."
          `wf_id` LIKE '".($wf_id ? $wf_id : '%')."'
    ";
    
    $result = $dbo
        ->query(
            $sql,
            [
                ':uid' => 'u:'.$user['id'],
                ':rid' => 'r:'.$user['role_id'],
                ':wf_id' => $wf_id ? $wf_id : "'%'"
            ]
        )
        ->fetchAll();
    
    $ret = array_merge($ret, $result);
    
    
    
    return $ret;
}
        
        /**
         * @return array
         */
        function getPublishedBookChapters_Workflow(){
        
        if($_SESSION['user']['role_id'] > 2) return []; //Only PUBLISSO and Administrators
        
        return \Drupal::database()->select('rwPubgoldBookChapters', 't')->fields('t', ['cp_wfid'])
                ->isNull('cp_next_version')->execute()->fetchCol();
}
        
        /**
         * @param $cp_wfid
         * @return mixed
         */
        function getBookchapterIDFromWorkflow($cp_wfid){
        return \Drupal::database()->select('rwPubgoldBookChapters', 't')->fields('t', ['cp_id'])
                ->condition('cp_wfid', $cp_wfid, '=')->execute()->fetchField();
}
        
        /**
         * @return mixed
         */
        function getMyWorkflowItems(){
  
  $dbo = \Drupal::database();
  $uid = $_SESSION['user']['id'];
  
  $result = $dbo->select('rwPubgoldWorkflow', 'wf')
                ->fields('wf', [])
                ->condition('wf_created_by_uid', $uid, '=')
                ->execute()
                ->fetchAll();
  return $result;
}
        
        /**
         * @param $dbo
         * @param $wf_id
         * @return mixed
         */
        function getWorkflowHistory($dbo, $wf_id){
    
    return $dbo->select('rwPubgoldWorkflowHistory', 'wfh')->fields('wfh', [])->condition('wfh_wfid', $wf_id, '=')->execute()->fetchAll();
}
        
        /**
         * @param $dbo
         * @param $wf_id
         * @return mixed
         */
        function getWorkflowComments($dbo, $wf_id){
    
    return $dbo->select('rwPubgoldWorkflowComments', 'wfc')->fields('wfc', [])->condition('wfc_wfid', $wf_id, '=')->execute()->fetchAll();
}
        
        /**
         * @param $dbo
         * @return mixed
         */
        function getJournals($dbo){
    $result = $dbo->select('rwPubgoldJournals', 'jrn')->fields('jrn', ['jrn_id', 'jrn_title', 'jrn_title_abbr']);
    
    $result->orderBy('jrn.jrn_id');
    $result->orderBy('jrn.jrn_title');
    $result->orderBy('jrn.jrn_title_abbr');
    
    $result = $result->execute()->fetchAll();
    
    return $result;
}
        
        /**
         * @param $dbo
         * @param $jrn_id
         * @return mixed
         */
        function getJournal($dbo, $jrn_id){
    
    return $dbo->select('rwPubgoldJournals', 'j')
               ->fields('j', [])
               ->condition('jrn_id', $jrn_id, '=')
               ->execute()
               ->fetchAssoc();
}
        
        /**
         * @param $dbo
         * @return mixed
         */
        function getConferences($dbo){
    $result = $dbo->select('rwPubgoldConferences', 'cf')->fields('cf', []);

    $result->orderBy('cf.cf_title');
    
    $result = $result->execute()->fetchAll();
    
    return $result;
}
        
        /**
         * @param $dbo
         * @param $cf_id
         * @return mixed
         */
        function getConference($dbo, $cf_id){
    
    return $dbo->select('rwPubgoldConferences', 'c')
               ->fields('c', [])
               ->condition('cf_id', $cf_id, '=')
               ->execute()
               ->fetchAssoc();
}
        
        /**
         * @param string $bk_id
         * @return array
         */
        function getBookSustainingMembers($bk_id = ''){
    
    $result = \Drupal::database()
        ->select('rwPubgoldBooksSustainingMembers', 'bsm')
        ->fields('bsm', ['bsm_id'])
        ->condition('bsm_bkid', $bk_id, '=')
        ->execute()
        ->fetchAll();
    
    $ret = array();
    
    foreach($result as $_){
        $ret[] = $_->bsm_id;
    }
    
    return $ret;
}
        
        /**
         * @param string $jrn_id
         * @return array
         */
        function getJournalSustainingMembers($jrn_id = ''){
    
    $result = \Drupal::database()
        ->select('rwPubgoldJournalsSustainingMembers', 'jsm')
        ->fields('jsm', ['jsm_id'])
        ->condition('jsm_jrnid', $jrn_id, '=')
        ->execute()
        ->fetchAll();
    
    $ret = array();
    
    foreach($result as $_){
        $ret[] = $_->jsm_id;
    }
    
    return $ret;
}
        
        /**
         * @param $rp_id
         * @return mixed
         */
        function getReviewPage($rp_id){
    
    $result = \Drupal::database()
        ->select('rwPubgoldReviewPage', 'rp')
        ->fields('rp', ['rp_schema'])
        ->condition('rp_id', $rp_id, '=')
        ->execute()
        ->fetchAssoc();
    
    return json_decode(base64_decode($result['rp_schema']));
}
        
        /**
         * @return mixed
         */
        function getReviewpages(){
    
    return \Drupal::database()
        ->select('rwPubgoldReviewPage', 'rp')
        ->fields('rp', [])
        ->execute()
        ->fetchAll();
}
        
        /**
         * @param $rpdata_encoded
         * @param null $show_only_uid
         * @return array
         */
        function renderReviewpageForm($rpdata_encoded, $show_only_uid = null){
        
        $rp_form = [];
        $rp = json_decode(base64_decode($rpdata_encoded), true);
        $c = 1;
        
        foreach($rp as $uid => $_){

              if($show_only_uid == null || $uid == $show_only_uid){
                
                foreach($_ as $__){
                    
                  $__ = json_decode(base64_decode($__), true);
                  $user = getAllUserData(\Drupal::database(), $uid);
                  
                  $vars = [
                    '@firstname' => $user['profile_firstname'],
                    '@lastname' => $user['profile_lastname']
                  ];
                  
                  $rp_form["rp_$c"] = [
                      '#type' => 'details',
                      '#open' => false,
                      '#title' => (string)t('Reviewpage by @lastname, @firstname', $vars),
                      'content' => []
                  ];
                  
                  foreach($__ as $rp_item){
                    
                    if($rp_item['type'] == 'checkbox'){
                      $_text = $rp_item['text'];
                      $_value = $rp_item['value'] = (string)t($rp_item['value'] == 1 ? 'Yes' : 'No');
                    }
                    elseif($rp_item['type'] == 'textarea'){
                      $_text = $rp_item['text'];
                      $_value = $rp_item['value'];
                    }
                    
                    $rp_form["rp_$c"]['content'][] = [
                        '#type' => 'markup',
                        '#markup' => "
                          $_text:<br>$_value<hr>
                        "
                    ];
                  }
                  
                  $c++;
                  
                }
              }
            }
        
        $rp_form = [
          '#type' => 'details',
          '#title' => (string)t('Reviewpages'),
          '#open' => false,
          'content' => $rp_form
        ];
        
        return $rp_form;
}
        
        /**
         * @param $wf_id
         * @param $uid
         * @return mixed
         */
        function getMyWorkflowComments($wf_id, $uid){
    
    $dbo = \Drupal::database();
    
    return $dbo->select('rwPubgoldWorkflowCommentsForAuthors', 'cfa')
                ->fields('cfa', [])
                ->condition('cfa_wfid', $wf_id, '=')
                ->condition('cfa_for_uid', $uid, '=')
                ->execute()
                ->fetchAll();
}
        
        /**
         * @param $to
         * @param $subject
         * @param $body
         * @param array $add_header
         * @param array $attachments
         */
        function sendMail($to, $subject, $body, $add_header = [], $attachments = []){
  
        $uuid = md5((int)uniqid() * time());
        $mail_system = \Drupal::service('plugin.manager.mail')->getInstance(array('module' => 'publisso_gold', 'key' => $uuid));
        
		$ctype = "text/plain; charset=UTF-8;";
		$boundary =  md5(uniqid(mt_rand(), 1));
		
		if(count($attachments)){
			
			$eol = PHP_EOL;
			
			$ctype = "multipart/mixed; boundary=\"$boundary\"";
			
			$body  = ""
					."--$boundary".$eol
					."Content-Type: text/plain; charset=UTF-8;$eol"
					."Content-Transfer-Encoding: 8BIT$eol$eol"
					.$body.$eol;

			foreach($attachments as $file){
        
        $path = $_SERVER['DOCUMENT_ROOT'].$file;

        if(!is_readable($path)){
          error_log("File \"$path\" is not readable!");
          continue;
        }
				
				$type = mime_content_type($path);
				$name = basename($path);
				$cont = chunk_split(base64_encode(file_get_contents($path)));
				$size = filesize($path);
				
				//$body .= "--$boundary--$eol";
				$body .= "--$boundary$eol";
				$body .= "Content-Type: $type; name=\"$name\"$eol";
				$body .= "Content-Disposition: attachment; filename=\"$name\";$eol";
				$body .= "Content-Length: $size$eol";
				$body .= "Content-Transfer-Encoding: base64$eol$eol";
				$body .= $cont;
			}

			$body .= "$eol--$boundary--";
		}
		
		
		
        $params = [
            'headers' => [
              'MIME-Version' => '1.0',
              'Content-Type' => $ctype,
              'Content-Transfer-Encoding' => '8BIT',
              'X-Mailer' => 'Publisso Mailer',
              'From' => 'PUBLISSO <livingbooks@zbmed.de>'
            ],
            'to' => $to,
            'body' => ($body),
            'subject' => $subject
        ];
		
		if(!empty($add_header['cc']))
			$params['cc'] = $add_header['cc'];
		
		if(!empty($add_header['bcc']))
			$params['bcc'] = $add_header['bcc'];
        
        foreach($add_header as $_k => $_v){
          $params['headers'][$_k] = $_v;
        }
		/*
		$headers = "";
		foreach($params['headers'] as $k => $v)
			$headers .= "$k: $v$eol";
		*/
        //mail($params['to'], $params['subject'], $params['body'], $headers);
        $mail_system->mail($params);
}
        
        /**
         * @param $workflow
         * @param $process
         * @param array $vars
         * @param bool $autosend
         * @param null $uid
         * @return false
         * @throws Exception
         */
        function sendWorkflowInfoMail(&$workflow, $process, $vars = [], $autosend = true, $uid = null){
	
	if(!$uid) $uid = \Drupal::service('session')->get('user')['id'];
	
	switch($workflow->getDataElement('type')){
		case 'bookchapter':
			$medium = new \Drupal\publisso_gold\Controller\Book($workflow->getDataElement('bk_id'));
			
			break;
		case 'journalarticle':
			$medium = new \Drupal\publisso_gold\Controller\Journal($workflow->getDataElement('jrn_id'));
			break;
		case 'conferencepaper':
			$medium = new \Drupal\publisso_gold\Controller\Conference($workflow->getDataElement('cf_id'));
			break;
		default:
			return false;
	}
	
	$mailout = $medium->getMailout($process);
	
	//echo '<pre>'.print_r($medium, 1).'</pre>'; exit();
	
	//error_log('MEDIUM: '.print_r($medium, 1));
	//error_log('MAILOUT: '.print_r($mailout, 1));
	
	if($mailout !== false){
		
		//error_log(print_r($mailout, 1), 0);
		$user = new \Drupal\publisso_gold\Controller\User($uid);
		
		$url_login = \Drupal\Core\Url::fromRoute('publisso_gold.login')->setAbsolute()->toString();
		
		$template_text = ['subject', 'template'];
		
		foreach($template_text as $template){
                        $mailout[$template] = str_replace('::submitted_by::', $user->profile->getElement('firstname').' '.$user->profile->getElement('lastname'), $mailout[$template]);
                        $mailout[$template] = str_replace('::submission_type::', t($workflow->getDataElement('type')), $mailout[$template]);
                        $mailout[$template] = str_replace('::Submission_type::', t(ucfirst($workflow->getDataElement('type'))), $mailout[$template]);
                        $mailout[$template] = str_replace('::submission_title::', $workflow->getDataElement('title'), $mailout[$template]);
                        $mailout[$template] = str_replace('::parent_title::', $medium->getElement('title'), $mailout[$template]);
                        $mailout[$template] = str_replace('::parent_signature::', $medium->getElement('mail_signature'), $mailout[$template]);
                        $mailout[$template] = str_replace('::link.login::', $url_login, $mailout[$template]);
                        $mailout[$template] = str_replace('::mail_signature_publisso::', \Drupal::service('publisso_gold.setup')->getValue('mail_signature_publisso'), $mailout[$template]);
                        
                        $mailout[$template] = str_replace(array_keys($vars), array_values($vars), $mailout[$template]);
                        
                        foreach($medium->getControlKeys() as $key){
                                $mailout[$template] = str_replace("::parent.control.$key::", $medium->getControlElement($key), $mailout[$template]);
                        }
                        
                        foreach($medium->getDataKeys() as $key){
                                $mailout[$template] = str_replace("::parent.data.$key::", $medium->getElement($key), $mailout[$template]);
                        }
                        
                        switch($workflow->getDataElement('type')){
                                case 'bookchapter':
                                        $mailout[$template] = str_replace('::media_type::', t('Book'), $mailout[$template]);
                                        break;
                                
                                case 'journalarticle':
                                        $mailout[$template] = str_replace('::media_type::', t('Journal'), $mailout[$template]);
                                        break;
                                
                                case 'conferencepaper':
                                        $mailout[$template] = str_replace('::media_type::', t('Conference'), $mailout[$template]);
                                        break;
                        }
		}
		$mailout['subject'] = t($mailout['subject']);

                $recipient = $cc = $bcc = [];
                
                error_log("Workflow #".$workflow->getElement('id'));
                error_log(print_r($mailout, 1));
                
                foreach(['recipient', 'cc', 'bcc'] as $mailTo){
                        
                        foreach(explode(',', $mailout[$mailTo]) as $_){

                                switch(substr($_, 0, 2)){

                                        case 'g:': //groups
                                                $group = explode(':', $_);
                                                $group = $group[1];

                                                switch($group){

                                                        case 'editorial_office':
                                                                $users = $medium->readEditorialOffice();
                                                                break;

                                                        case 'editors_in_chief':
                                                                foreach(explode(',', $workflow->getElement('assigned_to_eic')) as $uid){
                                                                        $users[] = new \Drupal\publisso_gold\Controller\User($uid);
                                                                }
                                                                $users = array_merge($users, $medium->readEditorsInChief());
                                                                break;

                                                        case 'reviewers':
                                                                foreach(explode(',', $workflow->getElement('assigned_to_reviewer')) as $uid){
                                                                        $users[] = new \Drupal\publisso_gold\Controller\User($uid);
                                                                }
                                                                break;

                                                        case 'editors':
                                                                foreach(explode(',', $workflow->getElement('assigned_to_editor')) as $uid){
                                                                        $users[] = new \Drupal\publisso_gold\Controller\User($uid);
                                                                }
                                                                break;
                                                }

                                                foreach($users as $_user){
                                                        
                                                        if(!in_array($_user->profile->getElement('email'), ${$mailTo}))
                                                                ${$mailTo}[] = $_user->profile->getElement('email');
                                                }

                                                break;

                                        case 'u:': //user
                                                $_user = explode(':', $_);
                                                
                                                if($_user[1] == 'submitting_author')
                                                        $_user[1] = $workflow->getElement('created_by_uid');

                                                $_user = new \Drupal\publisso_gold\Controller\User($_user[1]);

                                                if(!in_array($_user->profile->getElement('email'), ${$mailTo}))
                                                        ${$mailTo}[] = $_user->profile->getElement('email');
                                                break;

                                        case 'r:': //users by role
                                                $role = explode(':', $_);
                                                $role = $role[1];
                                                $users = getUsersByRole(\Drupal::database(), [$role]);

                                                foreach($users as $uid => $_user){
                                                        
                                                        $_user = new \Drupal\publisso_gold\Controller\User($uid);
                                                        
                                                        if(!in_array($_user->profile->getElement('email'), $$mailTo))
                                                                ${$mailTo}[] = $_user->profile->getElement('email');
                                                }
                                                break;

                                        default:
                                                foreach(array_filter(preg_split('/[;, ]/', $_)) as $___) ${$mailTo}[] = $___;
                                }
                        }
                }
                
                #error_log('Count recipients: '.count($recipient).' -- '.implode(', ', $recipient));
                #error_log('Fallback-recipient: '.\Drupal::service('publisso_gold.setup')->getValue('system.email.fallback.recipient'));

                if(!count($recipient) && \Drupal::service('publisso_gold.setup')->getValue('system.email.fallback.recipient')){
                        
                        error_log('No emailrecipient found! Using fallback!');
                        //set fallback emailrecipient if none found/set
                        $recipient[] = \Drupal::service('publisso_gold.setup')->getValue('system.email.fallback.recipient');

                        //remove fallback emailrecipient from (b)cc-lists
                        if(array_search($recipient[0], $cc )) array_splice($cc , array_search($recipient[0], $cc ), 1);
                        if(array_search($recipient[0], $bcc)) array_splice($bcc, array_search($recipient[0], $bcc), 1);

                        $mailout['template'] .= "\n\n######################################################\n".((string)t('You receive this email because no other recipient could be detected')."\n######################################################");
                }
		
		foreach($recipient as $_){
			
			//set recipient-depending variables
			
			$_mail = $mailout;
			
			if(\Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->condition('up_email', $_, '=')->countQuery()->execute()->fetchField()){
				$_user = new \Drupal\publisso_gold\Controller\User(\Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', ['up_uid'])->condition('up_email', $_, '=')->execute()->fetchField());
			}
			
			foreach($_user->getElementKeys() as $key){
				$_mail['template'] = str_replace("::recipient.$key::", $_user->getElement($key), $_mail['template']);
			}
			
			foreach($_user->profile->getElementKeys() as $key){
				$_mail['template'] = str_replace("::recipient.profile.$key::", $_user->profile->getElement($key), $_mail['template']);
			}
			
			$sender = new \Drupal\publisso_gold\Controller\User($_SESSION['user']['id']);
			
			foreach($sender->getElementKeys() as $key){
				$_mail['template'] = str_replace("::sender.$key::", $sender->getElement($key), $_mail['template']);
			}
			
			foreach($sender->profile->getElementKeys() as $key){
				$_mail['template'] = str_replace("::sender.profile.$key::", $sender->profile->getElement($key), $_mail['template']);
			}
			
			if($autosend == true)
				sendMail($_, $_mail['subject'], $_mail['template'], ['CC' => implode(',', $cc), 'BCC' => implode(',', $bcc)], $_mail['attachment']);
			else
				$spooler_id = saveMail2Spooler($_, $_mail, ['CC' => implode(',', $cc), 'BCC' => implode(',', $bcc)], json_encode($_mail['attachment']));
                        
                        //save Mail in getWorkflowHistory
                        $data = [
                                'wf_id'                 => $workflow->getElement('id'),
                                'wf_type'               => $workflow->getElement('type'),
                                'wf_state'              => $workflow->getDataElement('state'),
                                'wf_data'               => $workflow->getElement('data'),
                                'mail_from'             => 'PUBLISSO <livingbooks@zbmed.de>',
                                'mail_to'               => $_,
                                'mail_cc'               => serialize($cc),
                                'mail_bcc'              => serialize($bcc),
                                'mail_text'             => $_mail['template'],
                                'mail_attachment'       => serialize($_mail['attachment']),
                                'mail_autosend'         => $autosend ? 1 : 0,
                                'init_uid'              => $user->getElement('id')
                        ];
                        
                        $insert = \Drupal::database()->insert('rwPubgoldWorkflowMailHistory');
                        
                        if($spooler_id) $data['spooler_id'] = $spooler_id;
                        else $data['mail_sent'] = date('Y-m-d H:i:s');
                        
                        $insert->fields($data);
                        
                        $insert->execute();
		}
	}
	else{
		error_log('No mailtemplate for process "'.$process.'" found!');
	}
	
}
        
        /**
         * @param false $mailout
         * @param array $vars
         * @param bool $autosend
         * @throws Exception
         */
        function sendInfoMail($mailout = false, $vars = [], $autosend = true){
	
	if($mailout !== false){
		
		$current_user = new \Drupal\publisso_gold\Controller\User($_SESSION['user']['id']);
		$url_login = \Drupal\Core\Url::fromRoute('publisso_gold.login')->setAbsolute()->toString();
		
		foreach($current_user->getElementKeys() as $key)
			$mailout['template'] = str_replace("::initiator.$key::", $current_user->getElement($key), $mailout['template']);
		
		foreach($current_user->profile->getElementKeys() as $key)
			$mailout['template'] = str_replace("::initiator.profile.$key::", $current_user->profile->getElement($key), $mailout['template']);

		$mailout['template'] = str_replace('::link.login::', $url_login, $mailout['template']);
		$mailout['template'] = str_replace('::mail_signature_publisso::', \Drupal::service('publisso_gold.setup')->getValue('mail_signature_publisso'), $mailout['template']);
		$mailout['template'] = str_replace(array_keys($vars), array_values($vars), $mailout['template']);
		
		$mailout['subject'] = t($mailout['subject']);
		
		$recipients = $cc = $bcc = [];
		
		foreach(explode(',', $mailout['recipient']) as $_){
			
			switch(substr($_, 0, 2)){

				case 'u:': //user
					$_user = explode(':', $_);
					
					$_user = new \Drupal\publisso_gold\Controller\User($_user[1]);
					
					if(!in_array($_user->profile->getElement('email'), $recipients))
							$recipients[] = $_user->profile->getElement('email');
					break;
				
				case 'r:': //users by role
					$role = explode(':', $_);
					$role = $role[1];
					$users = getUsersByRole(\Drupal::database(), [$role]);
					
					foreach($users as $uid => $_user){
						$_user = new \Drupal\publisso_gold\Controller\User($uid);
						
						if(!in_array($_user->profile->getElement('email'), $recipients))
							$recipients[] = $_user->profile->getElement('email');
					}
					break;
				
				default:
					foreach(array_filter(preg_split('/[;, ]/', $_)) as $___) $recipients[] = $___;
			}
		}
		
		foreach(explode(',', $mailout['cc']) as $_){
			
			switch(substr($_, 0, 2)){
				
				case 'u:': //user
					$_user = explode(':', $_);
					
					$_user = new \Drupal\publisso_gold\Controller\User($_user[1]);
					
					if(!in_array($_user->profile->getElement('email'), $recipients))
							$cc[] = $_user->profile->getElement('email');
					break;
				
				case 'r:': //users by role
					$role = explode(':', $_);
					$role = $role[1];
					$users = getUsersByRole(\Drupal::database(), [$role]);
					
					foreach($users as $uid => $_user){
						$_user = new \Drupal\publisso_gold\Controller\User($uid);
						
						if(!in_array($_user->profile->getElement('email'), $cc))
							$cc[] = $_user->profile->getElement('email');
					}
					break;
				
				default:
					foreach(array_filter(preg_split('/[;, ]/', $_)) as $___) $cc[] = $___;
			}
		}
		
		foreach(explode(',', $mailout['bcc']) as $_){
			
			switch(substr($_, 0, 2)){
				
				case 'u:': //user
					$_user = explode(':', $_);
					
					$_user = new \Drupal\publisso_gold\Controller\User($_user[1]);
					
					if(!in_array($_user->profile->getElement('email'), $recipients))
							$bcc[] = $_user->profile->getElement('email');
					break;
				
				case 'r:': //users by role
					$role = explode(':', $_);
					$role = $role[1];
					$users = getUsersByRole(\Drupal::database(), [$role]);
					
					foreach($users as $uid => $_user){
						$_user = new \Drupal\publisso_gold\Controller\User($uid);
						
						if(!in_array($_user->profile->getElement('email'), $recipients))
							$bcc[] = $_user->profile->getElement('email');
					}
					break;
				
				default:
					foreach(array_filter(preg_split('/[;, ]/', $_)) as $___) $bcc[] = $___;
			}
		}
		
		#error_log('Count recipients: '.count($recipients));
		#error_log('Fallback-recipient: '.\Drupal::service('publisso_gold.setup')->getValue('system.email.fallback.recipient'));
		
		if(!count($recipients) && \Drupal::service('publisso_gold.setup')->getValue('system.email.fallback.recipient')){
                        error_log('No emailrecipient found! Using fallback!');
                        //set fallback emailrecipient if none found/set
                        $recipients[] = \Drupal::service('publisso_gold.setup')->getValue('system.email.fallback.recipient');
                        
                        //remove fallback emailrecipient from (b)cc-lists
                        if(array_search($recipients[0], $cc)) array_splice($cc, array_search($recipients[0], $cc), 1);
                        if(array_search($recipients[0], $bcc)) array_splice($bcc, array_search($recipients[0], $bcc), 1);
                        
                        $mailout['template'] .= "\n\n######################################################\n".((string)t('You receive this email because no other recipient could be detected')."\n######################################################");
		}
		
		foreach($recipients as $_){
			
			//set recipient-depending variables
			
			$_mail = $mailout;
			
			if(\Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', [])->condition('up_email', $_, '=')->countQuery()->execute()->fetchField()){
				$_user = new \Drupal\publisso_gold\Controller\User(\Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', ['up_uid'])->condition('up_email', $_, '=')->execute()->fetchField());
			}
			
			if($_user->getElement('id')){
				foreach($_user->getElementKeys() as $key){
					$_mail['template'] = str_replace("::recipient.$key::", $_user->getElement($key), $_mail['template']);
				}
				
				foreach($_user->profile->getElementKeys() as $key){
					$_mail['template'] = str_replace("::recipient.profile.$key::", $_user->profile->getElement($key), $_mail['template']);
				}
			}
			
			if($autosend == true)
				sendMail($_, $_mail['subject'], $_mail['template'], ['CC' => implode(',', $cc), 'BCC' => implode(',', $bcc)]);
			else
				saveMail2Spooler($_, $_mail, ['CC' => implode(',', $cc), 'BCC' => implode(',', $bcc)]);
		}
	}
	else{
	        $process = $process ?? 'system';
		error_log('No mailtemplate for process "'.$process.'" found!');
	}
	
}
        
        /**
         * @param $recipient
         * @param array $mailout
         * @param array $header
         * @param $attachment
         * @param null $autosend_parent
         * @return \Drupal\Core\Database\StatementInterface|int|string|null
         * @throws Exception
         */
        function saveMail2Spooler($recipient, $mailout = [], $header = [], $attachment, $autosend_parent = null){
	
	return \Drupal::database()->insert('rwPubgoldMailout')->fields([
		'recipient' 		=> $recipient,
		'subject'			=> $mailout['subject'],
		'body'				=> $mailout['template'],
		'header'			=> json_encode($header),
		'ready_to_send' 	=> 0,
		'created_by_uid'	=> $_SESSION['user']['id'],
		'attachment'		=> $attachment,
    'autosend_parent' => $autosend_parent
	])->execute();
}
        
        /**
         * @param $tmpl_name
         * @param array $vars
         */
        function sendRegistrationInfoMail($tmpl_name, $vars = []){

	$mailout = getMailtemplate($tmpl_name);
	
	if(count($mailout)){
		
		$mailout['template'] = t($mailout['template']);
			
		$delta = [
			'template',
			'recipient',
			'subject',
			'bcc',
			'cc'
		];
		
		foreach($delta as $key){
			$mailout[$key] = str_replace(array_keys($vars), array_values($vars), $mailout[$key]);
		}
	}
	
	sendMail(
		$mailout['recipient'],
		$mailout['subject'],
		$mailout['template'],
		[
			'cc'  => $mailout['cc'],
			'bcc' => $mailout['bcc']
		]
	);
}
        
        /**
         * @param $name
         * @return mixed
         */
        function getMailtemplate($name){
		
	return \Drupal::database()
		   ->select('rwPubgoldMailtemplates', 't')
		   ->fields('t', [])
		   ->condition('name', $name, '=')
		   ->execute()
		   ->fetchAssoc();
}
        
        /**
         * @return int
         */
        function isUserBookEditor(){
	
	$uid = false;
	
	if(array_key_exists('user', $_SESSION)){
		if(array_key_exists('id', $_SESSION['user'])){
			$uid = $_SESSION['user']['id'];
		}
	}
	
	if($uid === false) return 0;
	
	return \Drupal::database()->select('rwPubgoldBookEditors', 't')
							  ->fields('t', [])
							  ->condition('be_uid', $uid, '=')
							  ->countQuery()
							  ->execute()
							  ->fetchField();
}
        
        /**
         * @return int
         */
        function isUserBookEiC(){
	
	$uid = false;
	
	if(array_key_exists('user', $_SESSION)){
		if(array_key_exists('id', $_SESSION['user'])){
			$uid = $_SESSION['user']['id'];
		}
	}
	
	if($uid === false) return 0;
	
	return \Drupal::database()->select('rwPubgoldBookEditorsInChief', 't')
							  ->fields('t', [])
							  ->condition('eic_uid', $uid, '=')
							  ->countQuery()
							  ->execute()
							  ->fetchField();
}
        
        /**
         * @return int
         */
        function isUserBookEO(){
	
	$uid = false;
	
	if(array_key_exists('user', $_SESSION)){
		if(array_key_exists('id', $_SESSION['user'])){
			$uid = $_SESSION['user']['id'];
		}
	}
	
	if($uid === false) return 0;
	
	return \Drupal::database()->select('rwPubgoldBookEditorialOffice', 't')
							  ->fields('t', [])
							  ->condition('beo_uid', $uid, '=')
							  ->countQuery()
							  ->execute()
							  ->fetchField();
}
        
        /**
         * @return int
         */
        function isUserJournalEditor(){
	
	$uid = false;
	
	if(array_key_exists('user', $_SESSION)){
		if(array_key_exists('id', $_SESSION['user'])){
			$uid = $_SESSION['user']['id'];
		}
	}
	
	if($uid === false) return 0;
	
	return \Drupal::database()->select('rwPubgoldJournalEditors', 't')
							  ->fields('t', [])
							  ->condition('je_uid', $uid, '=')
							  ->countQuery()
							  ->execute()
							  ->fetchField();
}
        
        /**
         * @return int
         */
        function isUserJournalEiC(){
	
	$uid = false;
	
	if(array_key_exists('user', $_SESSION)){
		if(array_key_exists('id', $_SESSION['user'])){
			$uid = $_SESSION['user']['id'];
		}
	}
	
	if($uid === false) return 0;
	
	return \Drupal::database()->select('rwPubgoldJournalsEditorsInChief', 't')
							  ->fields('t', [])
							  ->condition('jeic_uid', $uid, '=')
							  ->countQuery()
							  ->execute()
							  ->fetchField();
}
        
        /**
         * @return int
         */
        function isUserJournalEO(){
	
	$uid = false;
	
	if(array_key_exists('user', $_SESSION)){
		if(array_key_exists('id', $_SESSION['user'])){
			$uid = $_SESSION['user']['id'];
		}
	}
	
	if($uid === false) return 0;
	
	return \Drupal::database()->select('rwPubgoldJournalsEditorialOffice', 't')
							  ->fields('t', [])
							  ->condition('jeo_uid', $uid, '=')
							  ->countQuery()
							  ->execute()
							  ->fetchField();
}
        
        /**
         * @return int
         */
        function isUserConferenceEditor(){
	
	return 0;
	
	/*
	$uid = false;
	
	if(array_key_exists('user', $_SESSION)){
		if(array_key_exists('id', $_SESSION['user'])){
			$uid = $_SESSION['user']['id'];
		}
	}
	
	if($uid === false) return 0;
	
	return \Drupal::database()->select('rwPubgoldBookEditors', 't')
							  ->fields('t', [])
							  ->condition('be_uid', $uid, '=')
							  ->countQuery()
							  ->execute()
							  ->fetchField();
	*/
}
        
        /**
         * @return int
         */
        function isUserConferenceEiC(){
	
	$uid = false;
	
	if(array_key_exists('user', $_SESSION)){
		if(array_key_exists('id', $_SESSION['user'])){
			$uid = $_SESSION['user']['id'];
		}
	}
	
	if($uid === false) return 0;
	
	return \Drupal::database()->select('rwPubgoldConferencesEditorsInChief', 't')
							  ->fields('t', [])
							  ->condition('ceic_uid', $uid, '=')
							  ->countQuery()
							  ->execute()
							  ->fetchField();
}
        
        /**
         * @return int
         */
        function isUserConferenceEO(){
	
	$uid = false;
	
	if(array_key_exists('user', $_SESSION)){
		if(array_key_exists('id', $_SESSION['user'])){
			$uid = $_SESSION['user']['id'];
		}
	}
	
	if($uid === false) return 0;
	
	return \Drupal::database()->select('rwPubgoldConferencesEditorialOffice', 't')
							  ->fields('t', [])
							  ->condition('ceo_uid', $uid, '=')
							  ->countQuery()
							  ->execute()
							  ->fetchField();
}
        
        /**
         * @param $string
         * @param array $vars
         * @return string|string[]|null
         */
        function replaceVars($string, array &$vars){

	//set Site-Vars
	$string = str_replace(array_keys($vars), array_values($vars), $string);
	
	//remove unused vars
	return preg_replace('(::([a-zA-Z-_1-9]+)?::)', '', $string);
}
        
        /**
         * @param $str
         * @return bool
         */
        function isB64($str){
	
    try{
		if(preg_match('/^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$/', $str))
			return true;
		
        $decoded = base64_decode($str, true);
		
		for($i = 0; $i < strlen($decoded); $i++){
			if(ord(substr($decoded, $i, 1)) <= 30)
				return false;
		}
		
		if(base64_encode($decoded) === $str)
			$ret = true;

    }
    catch(Exception $e){
		return false;
    }
}
        
        /**
         * @param $firstname
         * @param $lastname
         * @return int
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        function nameSearchAtZBMED($firstname, $lastname){
        
        if(strtolower(\Drupal::service('publisso_gold.setup')->getValue('system.sso.active')) != 'yes'){
                return -1;
        }
        
        $data = [
                'method' => 'name_search',
                'parameters' => [
                        'firstname' => $firstname,
                        'surname' => $lastname,
                        'completeness' => 'with_sysdata'
                ],
                'values' => []
        ];
        
        $response = \Drupal::httpClient()->request(
                'POST',
                'https://lvps46-163-114-198.dedicated.hosteurope.de/zbmedauth',
                [
                        'headers'       => [
                                'Content-Type' => 'application/json'
                        ],
                        'http_errors'   => false,
                        'verify'        => false,
                        'query'         => [
                                'data'  => base64_encode(json_encode($data))
                        ]
                ]
        );
        
        if($response->getStatusCode() != 200){
                //try to auth against local db
                error_log('nameSearchAtZBMED: '.$response->getStatusCode().': '.$response->getBody()->getContents());
                return -1;
        }
        else{
                $response->getBody()->rewind();
                $ret = json_decode($response->getBody()->getContents());
                
                error_log('Code: '.$ret->code);
                if($ret->code != 200){
                        
                        switch($ret->msg){
                                
                                default: //try to auth against local db
                                        error_log('nameSearchAtZBMED '.$ret->code.': '.$ret->msg);
                                        return -1;
                        }
                }
                else{
                        if(preg_match('/^JSONB64:\/\//', $ret->msg)){
                                
                                $list = json_decode(base64_decode(preg_replace('/^JSONB64:\/\//', '', $ret->msg)));
                                
                                if(json_last_error() == JSON_ERROR_NONE && property_exists($list, 'item')){
                                        return $list->item;
                                }
                                else{
                                        error_log('nameSearchAtZBMED '.$ret->code.': '.$ret->msg);
                                        return -1;
                                }
                        }
                }
        }
}
        
        /**
         * @param $string
         * @return int
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        function usedAtZBMED($string){
        
        if(strtolower(\Drupal::service('publisso_gold.setup')->getValue('system.sso.active')) != 'yes'){
                return -1;
        }
        $user = \Drupal::service('session')->get('user');
        //first auth against sso-zbmed
        $data = [
                'method' => 'get_user_data',
                'parameters' => [
                        'remote_id' => $string,
                        'pw' => '123456',
                        'with_sysdata' => 1
                ],
                'values' => []
        ];
        
        $response = \Drupal::httpClient()->request(
                'POST',
                'https://lvps46-163-114-198.dedicated.hosteurope.de/zbmedauth',
                [
                        'headers'       => [
                                'Content-Type' => 'application/json'
                        ],
                        'http_errors'   => false,
                        'verify'        => false,
                        'query'         => [
                                'data'  => base64_encode(json_encode($data))
                        ]
                ]
        );
        
        if($response->getStatusCode() != 200){
                //try to auth against local db
                error_log('usedAtZBMED: '.$response->getStatusCode().': '.$response->getBody()->getContents());
                return -1;
        }
        else{
                $response->getBody()->rewind();
                $ret = json_decode($response->getBody()->getContents());
                
                error_log('Code: '.$ret->code);
                if($ret->code != 200){
                        
                        switch($ret->msg){
                                
                                case 'BAD_PASSWORD':
                                        return 1;
                                        break;
                                
                                case 'BAD_USER_ID': //try to auth against local db.
                                        return 0;
                                        break;
                                
                                default: //try to auth against local db
                                        error_log('emailAtZBMED '.$ret->code.': '.$ret->msg);
                                        return -1;
                        }
                }
                else{
                         return 1;
                }
        }
}
        
        /**
         * @param $string
         * @param $excluded
         * @return bool
         */
        function usedLocal($string, $excluded){
        
        if(is_string($excluded)) $excluded = [$excluded];
        if(!is_array($excluded)) $excluded = [];
        $excluded = array_map('strtolower', $excluded);
        
        if(!in_array('pending', $excluded)){
                
                //String as username or email in pending registrations?
                $qry = \Drupal::database()->select('rwPubgoldRegisterPending', 't')->fields('t', []);
                $condition = $qry->orConditionGroup()->condition('rp_username', $string, '=')->condition('rp_email', $string, '=');
                if($qry->condition($condition)->countQuery()->execute()->fetchField()) return true;
        }
        
        if(!in_array('users', $excluded)){
                
                //String as username, zbmed_kennung or initial_email in user logins?
                $qry = \Drupal::database()->select('rwPubgoldUsers', 't')->fields('t', []);
                $condition = $qry->orConditionGroup()->condition('user', $string, '=')->condition('initial_email', $string, '=')->condition('zbmed_kennung', $string, '=');
                if($qry->condition($condition)->countQuery()->execute()->fetchField()) return true;
        }
        
        if(!in_array('profiles', $excluded)){
                
                //Email in user profiles?
                $qry = \Drupal::database()->select('rwPubgoldUserProfiles', 't')->fields('t', []);
                error_log($qry->condition('up_email', $string, '=')->countQuery()->execute()->fetchField());
                if($qry->condition('up_email', $string, '=')->countQuery()->execute()->fetchField()) return true;
        }
        
        if(!in_array('preview', $excluded)){
                
                //String as email or username in administrative created profiles?
                $qry = \Drupal::database()->select('rwPubgoldUserprofilePreview', 't')->fields('t', []);
                $condition = $qry->orConditionGroup()->condition('pv_username', $string, '=')->condition('pv_email', $string, '=');
                if($qry->condition($condition)->countQuery()->execute()->fetchField()) return true;
        }
        
        if(!in_array('invitations', $excluded)){
                
                //String as email or username in pending invitations?
                $qry = \Drupal::database()->select('rwPubgoldUserInvitations', 't')->fields('t', []);
                $condition = $qry->orConditionGroup()->condition('user', $string, '=')->condition('email', $string, '=');
                if($qry->condition($condition)->countQuery()->execute()->fetchField()) return true;
        }
        
        return false;
}
        
        /**
         * @param $uid
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        function exportUser($uid){
        
        $session = \Drupal::service('session');
        $user = $session->get('user');
        if(strtolower(\Drupal::service('publisso_gold.setup')->getValue('system.sso.active')) != 'yes'){
                return;
        }
        
        $user_local = new \Drupal\publisso_gold\Controller\User($uid);
        
        if(!$user_local->getElement('initial_email'))
          $user_local->setElement('initial_email', $user_local->profile->email);
        
        $data = [
                'method' => 'register_user',
                'parameters' => [
                        'remote_id' => $user_local->getElement('initial_email') ? $user_local->getElement('initial_email') : $user_local->getElement('user'),
                        'pw' => $user['password'],
                        'address' => [
                                'institut' => $user_local->profile->getElement('institute'),
                                'LKURZ' => preg_match('/^\d+$/', $user_local->profile->getElement('country')) ? array_search(getCountry($user_local->profile->getElement('country')), getCountrylist()) : $user_local->profile->getElement('country'),
                                'LORT' => $user_local->profile->getElement('city'),
                                'name' => implode(', ', [$user_local->profile->getElement('lastname'), $user_local->profile->getElement('firstname')]),
                                'LSTRAS' => $user_local->profile->getElement('street'),
                                'LPZAHL' => $user_local->profile->getElement('postal_code'),
                                'LTELNR' => $user_local->profile->getElement('telephone'),
                                'LSPRACHE' => $user_local->profile->getElement('correspondence_language'),
                                'LLAND' => getCountry($user_local->profile->getElement('country')),
                                'PRGRP' => 'z2',
                                'LEMAILADR' => $user_local->profile->getElement('email')
                        ]
                ],
                'values' => []
        ];
        
        foreach($data['parameters']['address'] as $k => $v)
                if(!$v) unset($data['parameters']['address'][$k]);
        
        $response = \Drupal::httpClient()->request('POST', 'https://lvps46-163-114-198.dedicated.hosteurope.de/zbmedauth', ['headers' => ['Content-Type' => 'application/json'], 'http_errors' => false, 'verify' => false, 'query' => ['data' => base64_encode(json_encode($data))]]);
        
        if($response->getStatusCode() == 200){
                
                $ret = json_decode($response->getBody()->getContents());
                //error_log($response->getBody());
                if($ret->code == 200){
                        
                        preg_match('/K\d+$/', $ret->msg, $matches);
                        $kennung_zbmed = $matches[0];
                        $user_local->setElement('zbmed_kennung', $kennung_zbmed);
                        error_log('Mapping local user '.$user_local->getElement('id').' to ZBMED '.$kennung_zbmed);
                }
                else{
                        error_log('exportUser: '.$ret->code.': '.$ret->msg);
                }
        }
        error_log('exportUser: '.$response->getStatusCode(). ': '.$response->getBody()->getContents());
        
}
        
        /**
         * @param $name
         * @return false|string
         */
        function getTemplate($name){
        
        $modpath = drupal_get_path('module', 'publisso_gold').'/inc';
        $file = getcwd().'/'.$modpath.'/'.$name.'.tmpl.inc.php';
        
        if(is_file($file) && is_readable($file)) return file_get_contents($file);
        else return (string)t('Error: Can\'t load template "'.$name.'"');
}
        
        /**
         * @param \Drupal\publisso_gold\Controller\User $user
         * @param string $sep
         * @return string
         */
        function getUserAffiliation(\Drupal\publisso_gold\Controller\User &$user, $sep = '; '){
        
        return implode($sep, array_filter([$user->profile->getElement('department'), $user->profile->getElement('institute'), $user->profile->getElement('City'), getCountry($user->profile->getElement('country'))]));
}
        
        /**
         * @return mixed
         */
        function getLicenses(){
        return \Drupal::database()->select('rwPubgoldLicenses', 't')->fields('t', ['id', 'name'])->execute()->fetchAllKeyed();
}
        
        /**
         * @param string $key
         * @return mixed
         */
        function getDDCs($key = 'id'){
        
        $qry = \Drupal::database()->select('rwPubgoldDDC', 't')->fields('t', ['id']);
        $qry->addExpression("CONCAT_WS(' ', `number`, `name`)", 'name');
        return $qry->execute()->fetchAllKeyed();
}
        
        /**
         * @param $ddc
         * @return mixed
         */
        function getDDC($ddc){
        
        $qry = \Drupal::database()->select('rwPubgoldDDC', 't')->fields('t', ['name']);
        $condgroup = $qry->orConditionGroup()->condition('id', $ddc, '=')->condition('name', $ddc, '=');
        $qry->condition($condgroup);
        
        return $qry->execute()->fetchField();
}
        
        /**
         * @param $index
         * @param $name
         * @return \Drupal\Core\Database\StatementInterface|int|string|null
         * @throws Exception
         */
        function createDDC($index, $name){
        if(\Drupal::database()->select('rwPubgoldDDC', 't')->fields('t', [])->condition('name', $name, '=')->countQuery()->execute()->fetchField() == 0)
        return \Drupal::database()->insert('rwPubgoldDDC')->fields(['number' => $index, 'name' => $name])->execute();
}
        
        /**
         * @return mixed
         */
        function getPublicationPlaces(){
        return \Drupal::database()->select('rwPubgoldPublicationPlaces', 't')->fields('t', ['id', 'name'])->execute()->fetchAllKeyed();
}
        
        /**
         * @param $pp
         * @return mixed
         */
        function getPublicationPlace($pp){
        
        $qry = \Drupal::database()->select('rwPubgoldPublicationPlaces', 't')->fields('t', ['name']);
        $condgroup = $qry->orConditionGroup()->condition('id', $pp, '=')->condition('name', $pp, '=');
        $qry->condition($condgroup);
        
        return $qry->execute()->fetchField();
}
        
        /**
         * @param $name
         * @return \Drupal\Core\Database\StatementInterface|int|string|null
         * @throws Exception
         */
        function createPublicationPlace($name){
        if(\Drupal::database()->select('rwPubgoldPublicationPlaces', 't')->fields('t', [])->condition('name', $name, '=')->countQuery()->execute()->fetchField() == 0)
                return \Drupal::database()->insert('rwPubgoldPublicationPlaces')->fields(['name' => $name])->execute();
}
        
        /**
         * @return mixed
         */
        function getPublishers(){
        return \Drupal::database()->select('rwPubgoldPublishers', 't')->fields('t', ['id', 'name'])->execute()->fetchAllKeyed();
}
        
        /**
         * @param $pp
         * @return mixed
         */
        function getPublisher($pp){
        
        $qry = \Drupal::database()->select('rwPubgoldPublishers', 't')->fields('t', ['name']);
        $condgroup = $qry->orConditionGroup()->condition('id', $pp, '=')->condition('name', $pp, '=');
        $qry->condition($condgroup);
        
        return $qry->execute()->fetchField();
}
        
        /**
         * @param $name
         * @return \Drupal\Core\Database\StatementInterface|int|string|null
         * @throws Exception
         */
        function createPublisher($name){
        if(\Drupal::database()->select('rwPubgoldPublishers', 't')->fields('t', [])->condition('name', $name, '=')->countQuery()->execute()->fetchField() == 0)
                return \Drupal::database()->insert('rwPubgoldPublishers')->fields(['name' => $name])->execute();
}
        
        /**
         * @return mixed
         */
        function getCategories(){
        return \Drupal::database()->select('rwPubgoldCategories', 't')->fields('t', ['id', 'name'])->execute()->fetchAllKeyed();
}
        
        /**
         * @param $pp
         * @return mixed
         */
        function getCategory($pp){
        
        $qry = \Drupal::database()->select('rwPubgoldCategories', 't')->fields('t', ['name']);
        $condgroup = $qry->orConditionGroup()->condition('id', $pp, '=')->condition('name', $pp, '=');
        $qry->condition($condgroup);
        
        return $qry->execute()->fetchField();
}
        
        /**
         * @param $name
         * @return \Drupal\Core\Database\StatementInterface|int|string|null
         * @throws Exception
         */
        function createCategory($name){
        if(\Drupal::database()->select('rwPubgoldCategories', 't')->fields('t', [])->condition('name', $name, '=')->countQuery()->execute()->fetchField() == 0)
                return \Drupal::database()->insert('rwPubgoldCategories')->fields(['name' => $name])->execute();
}
        
        /**
         * @return mixed
         */
        function getArticleTypes(){
        return \Drupal::database()->select('rwPubgoldArticleTypes', 't')->fields('t', ['id', 'name'])->execute()->fetchAllKeyed();
}
        
        /**
         * @param $pp
         * @return mixed
         */
        function getArticleType($pp){
        
        $qry = \Drupal::database()->select('rwPubgoldArticleTypes', 't')->fields('t', ['name']);
        $condgroup = $qry->orConditionGroup()->condition('id', $pp, '=')->condition('name', $pp, '=');
        $qry->condition($condgroup);
        
        return $qry->execute()->fetchField();
}

?>
