<?php
        
        /**
         * Class book
         */
        class book {
    
    private $id;
    private $bookelements = [];
        
        /**
         * @return string
         */
        public function __toString(){
        return "class book";
    }
        
        /**
         * book constructor.
         * @param null $id
         */
        public function __construct($id = null){
        
        $this->id = $id;
        $this->load($this->id);
    }
        
        /**
         * @param string $elemName
         * @param null $elemValue
         * @return bool
         */
        public function setElement(string $elemName, $elemValue = null){
        
        $oldValue = $this->bookelements[$elemName];
        $this->bookelements[$elemName] = $elemValue;
        
        if($this->save() === false){
            $this->bookelements[$elemName] = $oldValue;
            return false;
        }
        else{
            return true;
        }
    }
        
        /**
         * @return \Drupal\Core\Database\StatementInterface|int|string|null
         */
        private function save(){
        
        return \Drupal::database()->update('rwBooks')
        ->fields($this->bookelements)
        ->condition('id', $this->id)
        ->execute();
    }
    
    private function load(){
        
        if($this->id){
          
            $book = \Drupal::database()->select('rwBooks', 'b')
            ->fields('b', [])
            ->condition('id', $this->id, '=')
            ->execute()
            ->fetchall();
            
            foreach($book as $k => $v){
                
                if($k != 'id') //store book except id
                  $this->bookelements[$k] = $v;
            }
        }
    }
}

?>