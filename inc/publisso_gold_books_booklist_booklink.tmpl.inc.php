<li class="media">
  <div class="media-left">
      <a href="#">
        ::books_booklist_cover::
      </a>
  </div>
  <div class="media-body media-italics">
      <h3 class="media-heading">::books_booklist_link::</h3>
      <p>::books_booklist_description::</p>
      <p><a href="::lnk_view_book::">::txt_lnk_view_book::</a></p>
  </div>
  <div class="media-right">
      <a href="#">
        <img class="media-object" src="::books_booklist_logo::" alt="::books_booklist_logo_alt::">
      </a>
  </div>
</li>
