<div id="rwWrapperBook">
<div id="rwBookCover">
  ::cover::
</div>
<div id="rwBookMain">
  <h2>::bk_title::</h2>
  ::bk_editorial_board::<br>
  <br>
  ::txt_issn::: ::bk_issn::<br>
  ::txt_isbn::: ::bk_isbn::<br>
  <br>
  ::txt_funding_name:: ::bk_funding_name::<br>
  ::txt_funding_id:: ::bk_funding_id::
</div>
<div id="rwBookRight">
  ::logo::<br><br>
  ::lnk_edit_book::<br>
  <br><br>
  ::lnk_add_chapter::<br>
  <br>
  <br>
  ::lnk_edit_editorial_board::
</div>
<br clear="all">
</div>



