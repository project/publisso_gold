<strong>::strTitle::</strong>
<div class="rwTable">
        <div class="rwTablerow">
                <div class="rwTablecell">
                        ::strAuthorDesc:::
                </div>
                <div class="rwTablecell">
                        ::strAuthor::
                </div>
        </div>
        <div class="rwTablerow">
                <div class="rwTablecell">
                        ::strCreatedDesc:::
                </div>
                <div class="rwTablecell">
                        ::workflow_created::
                </div>
        </div>
        <div class="rwTablerow">
                <div class="rwTablecell">
                        ::strDOIDesc:::
                </div>
                <div class="rwTablecell">
                        ::chapter_doi::
                </div>
        </div>
        <div class="rwTablerow">
                <div class="rwTablecell">
                </div>
                <div class="rwTablecell">
                        <a href="::urlLnkSetErratum::" title="::strHelpSetErratum::">::strLnkSetErratum::</a>
                </div>
        </div>
</div>
