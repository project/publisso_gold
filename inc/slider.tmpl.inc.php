<div class="partnerslider">
        <div class="carousel-wrapper">
                <div class="slick  width-control">
                        
                        <div class="slide media">
                                <div class="media-left pull-left" data-id="{field:uid}">
                                        <a href="::datacite_url::" target="_blank" title="::datacite_title::">
                                        <img src="::datacite_logo::" data-smallRetina="::datacite_logo::" data-tablet="::datacite_logo::" data-tabletretina="::datacite_logo::" data-desktop="::datacite_logo::" alt="::datacite_alt::"></a>
                                </div>
                                <div class="media-body">
                                        <h2>ZB MED is a member of DataCite</h2>
                                        <p>DataCite is an international not-for-profit organisation that offers services and know-how relating to the management, referencing and citation of research data.</p>
                                </div>
                        </div>
                        <div class="slide media">
                                <div class="media-left pull-left" data-id="{field:uid}">
                                        <a href="::totalequality_url::" target="_blank" title="::totalequality_title::"><img src="::totalequality_logo::" data-smallRetina="::totalequality_logo::" data-tablet="::totalequality_logo::" data-tabletretina="::totalequality_logo::" data-desktop="::totalequality_logo::" alt="::totalequality_alt::"></a>
                                </div>
                                <div class="media-body">
                                        <h2>ZB MED advocates gender equality</h2>
                                        <p>ZB MED received the TOTAL E-QUALITY award in 2010 and 2013 for successfully implementing gender equality in its staffing structure and personnel policies.</p>
                                </div>
                        </div>
                        <div class="slide media">
                                <div class="media-left pull-left" data-id="{field:uid}">
                                        <a href="::landderideen_url::" target="_blank" title="::landderideen_title::"><img src="::landderideen_logo::" data-smallRetina="::landderideen_logo::" data-tablet="::landderideenlogo::" data-tabletretina="::landderideen_logo::" data-desktop="::landderideen_logo::" alt="::landderideen_alt::"></a>
                                </div>
                                <div class="media-body">
                                        <h2>Award for German Medical Science</h2>
                                        <p>German Medical Science (GMS) was awarded the distinction of &quot;Selected landmark 2011&quot; in the &quot;Germany - Land of Ideas&quot; initiative.</p>
                                </div>
                        </div>
                        <div class="slide media">
                                <div class="media-left pull-left" data-id="{field:uid}">
                                        <a href="::openaccess_url::" target="_blank" title="::openaccess_title::"><img src="::openaccess_logo::" data-smallRetina="::openaccess_logo::" data-tablet="::openaccess_logo::" data-tabletretina="::openaccess_logo::" data-desktop="::openaccess_logo::" alt="::openaccess_alt::"></a>
                                </div>
                                <div class="media-body">
                                        <h2>ZB MED advocates Open Access</h2>
                                        <p>ZB MED promotes and supports open access to scientific research through the PUBLISSO portal and other services.</p>
                                </div>
                        </div>
                </div>
        </div>
</div>
