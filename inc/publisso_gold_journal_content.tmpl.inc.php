<a id="main-content"></a>
<div class="row">
	<div class="col-xs-12">
		<!-- Tab navigation -->
		<ul class="nav nav-tabs pub-nav-tabs" id="tabs">
                        ::li_item_article_content::
			<li class="::tab_current_volume_class_active::">
				<a data-target="#current-volume" data-toggle="tab">
					::txt_current_volume::
				</a>
			</li>
			::li_item_archive_volume::
			<li class="::tab_about_class_active::">
				<a data-target="#about" data-toggle="tab">
					::txt_about::
				</a>
			</li>
                        <!--
			<li class="::tab_editorial_class_active::">
				<a data-target="#tabCntEditorial" data-toggle="tab">
					::txt_editorial_board::
				</a>
			</li>
			-->
                        ::li_item_editorial::
			<li class="::tab_guidelines_class_active::">
				<a data-target="#guidelines" data-toggle="tab">
					::txt_manuscript_guidelines::
				</a>
			</li>
			<li class="::tab_imprint_class_active::">
				<a data-target="#imprint" data-toggle="tab">
					::txt_imprint::
				</a>
			</li>
                        ::li_item_contact::
			::li_item_archive::
		</ul>
		<!-- /tab nav -->
	</div>
</div>
<div class="tab-content">
	::content_item_article_content::
	<div class="tab-pane ::content_current_volume_class_active::" id="current-volume">
        <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    <div class="table-of-content">
                        ::journal_current_volume::
                    </div>
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                    
                </div>
            </aside>
        </div>
	</div>
	::content_item_archive_volume::
	<div class="tab-pane ::content_about_class_active::" id="about">
        <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    ::journal_about::
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                    ::journal_about_sidebar_blocks::
                </div>
            </aside>
        </div>
	</div>
	<div class="tab-pane ::content_guidelines_class_active::" id="guidelines">
        <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    ::journal_manuscript_guidelines::
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                    
                </div>
            </aside>
        </div>
	</div>
	<div class="tab-pane article ::content_imprint_class_active::" id="imprint">
        <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    ::journal_imprint::
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                    
                </div>
            </aside>
        </div>
	</div>
        <!--
	<div class="tab-pane ::content_editorial_class_active::" id="tabCntEditorial">
        <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    ::content_eb::
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                    
                </div>
            </aside>
        </div>
	</div>
	-->
        ::content_item_editorial::
        ::content_item_contact::
	::content_item_archive::
</div>













