 
<a id="main-content"></a>
<div class="row">
	<div class="col-xs-12">
		<!-- Tab navigation -->
		<ul class="nav nav-tabs pub-nav-tabs" id="tabs">
			::li_item_chapter_content::
			<li class="::tab_about_class_active::">
				<a data-target="#about" data-toggle="tab">
					::txt_about::
				</a>
			</li>
			<li>
				<a data-target="#media" data-toggle="tab">
					::txt_media::
				</a>
			</li>
			<li>
				<a data-target="#editorial" data-toggle="tab">
					::txt_editorial_board::
				</a>
			</li>
			<li class="">
				<a data-target="#authors" data-toggle="tab">
					::txt_authors::
				</a>
			</li>
			<li>
				<a data-target="#guidelines" data-toggle="tab">
					::txt_manuscript_guidelines::
				</a>
			</li>
			<li class="">
				<a data-target="#imprint" data-toggle="tab">
					::txt_imprint::
				</a>
			</li>
			<li>
				<a data-target="#chapter-list" data-toggle="tab">
					::txt_overview_chapters::
				</a>
			</li>
		</ul>
		<!-- /tab nav -->
	</div>
</div>
<div class="tab-content">
	::content_item_chapter_content::
	<div class="tab-pane" id="chapter-list">
        <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    <div class="table-of-content">
                        ::book_chapter_list::
                    </div>
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                    
                </div>
            </aside>
        </div>
	</div>
	<div class="tab-pane ::content_about_class_active::" id="about">
        <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    ::book_about::
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                    ::book_about_sidebar_blocks::
                </div>
            </aside>
        </div>
	</div>
	<div class="tab-pane" id="guidelines">
        <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    ::book_manuscript_guidelines::
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                    
                </div>
            </aside>
        </div>
	</div>
	<div class="tab-pane article" id="imprint">
        <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    ::book_imprint::
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                    
                </div>
            </aside>
        </div>
	</div>
	<div class="tab-pane" id="media">
    <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    <h2>Media</h2>
                    <p>
						Here you will find audio-visual content integrated in the Living Handbook in alphabetical order. As soon as a chapter is published you will be able to browse the media database.
					</p>
					<br><br><br><br><br><br>
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                    
                </div>
            </aside>
        </div>
	</div>
	<div class="tab-pane" id="editorial">
        <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    ::content_eb::
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                    
                </div>
            </aside>
        </div>
	</div>
	<div class="tab-pane" id="authors">
        <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    ::content_authors::
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                    
                </div>
            </aside>
        </div>
	</div>
</div>
