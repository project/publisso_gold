<div class="tab-pane ::content_editorial_class_active::" id="tabCntEditorial">
        <div class="row">
                <div class="col-sm-9">
                        <div class="region region-content article-content">
                                <div class="table-of-content">
                                        ::journal_editorial_content::
                                </div>
                        </div>
                </div>
                <aside class="col-sm-3" role="complementary">
                        <div class="region region-sidebar-second article__suporting-information">
                        
                        </div>
                </aside>
        </div>
</div>
