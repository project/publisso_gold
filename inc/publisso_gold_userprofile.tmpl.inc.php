<h2>::text_headline:: ::up_salutation:: ::up_graduation:: ::up_lastname::, ::up_firstname:: ::up_graduation_suffix::</h2>
<img src="/publisso_gold/pic/?up=::up_uid::" class="rwUserpic" width="200">

<div class="rwTable">
    <div class="rwTablerow">
        <div class="rwTablecell head">
          ::text_institute::
        </div>
        <div class="rwTablecell">
          ::up_institute::
        </div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">
          ::text_country::
        </div>
        <div class="rwTablecell">
          ::up_country::
        </div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">
          ::text_city::
        </div>
        <div class="rwTablecell">
          ::up_postal_code:: ::up_city::
        </div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">
          ::text_street::
        </div>
        <div class="rwTablecell">
          ::up_street::
        </div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">
          ::text_email::
        </div>
        <div class="rwTablecell">
          ::up_email::
        </div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">
          ::text_department::
        </div>
        <div class="rwTablecell">
          ::up_department::
        </div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">
          ::text_telephone::
        </div>
        <div class="rwTablecell">
          ::up_telephone::
        </div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">
          ::text_area_of_expertise::
        </div>
        <div class="rwTablecell">
          ::up_area_of_expertise::
        </div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">
          ::text_orcid::
        </div>
        <div class="rwTablecell">
          ::up_orcid::
        </div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">
          ::text_correspondence_language::
        </div>
        <div class="rwTablecell">
          ::up_correspondence_language::
        </div>
    </div>
</div>
<hr>
<a href="/publisso_gold/userprofile_edit" class="btn btn-warning">::text_userprofile_edit::</a>&nbsp;&nbsp;&nbsp;
<a href="/publisso_gold/userprofile/password" class="btn btn-warning use-ajax" data-dialog-type="modal">::text_userprofile_chpw::</a><br>
<hr>
