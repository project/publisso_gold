<a id="main-content"></a>
<div class="row">
	<div class="col-xs-12">
		<ul class="nav nav-tabs pub-nav-tabs" id="tabs">
			::tabs::
		</ul>
	</div>
</div>
<div class="tab-content">
        <div class="tab-pane active" id="about">
                <div class="row">
                        <div class="col-sm-9">
                                <div class="region region-content article-content">
                                        ::content::
                                </div>
                        </div>
                        <aside class="col-sm-3" role="complementary">
                                <div class="region region-sidebar-second article__suporting-information">
                                        ::sidebar::
                                </div>
                        </aside>
                </div>
        </div>
</div>

