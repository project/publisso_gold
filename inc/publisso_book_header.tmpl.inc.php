<section class="article-header border-top">
        <div class="cover--pub col-md-2">
                <img class="img-responsive" src="::lnkCover::" alt="Cover: ::title::" title="::title::">
        </div>
        <div class="header-col-right col-md-3">
                <div class="logo--publisher">
                        <!--<a href="#">--><img class="img-responsive" src="/system/getPicture/bl/::bk_id::" alt=""
                                                title=""><!--</a>-->
                </div>
        </div>
        <div class="col-md-7">
                <h1 class="page-header">::title::</h1>
                
                <div class="meta--article">
                        <p>
                                ::public_editors::
                        </p>
                                ::issn_isbn::
                        <p>
                                ::funding_text::
                        </p>
                </div>
                <br>
                ::lnk_edit_book::
                ::lnk_edit_book_eb::
                ::lnk_publish_chapter::
        </div>
</section>
