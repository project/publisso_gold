<div class="rwTable">
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_article_type:::</div>
        <div>::cfp_article_type::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_doc_number:::</div>
        <div>::cfp_doc_number::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_license:::</div>
        <div>::cfp_license::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_publication_year:::</div>
        <div>::cfp_publication_year::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_publication_place:::</div>
        <div>::cfp_publication_place::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_publisher:::</div>
        <div>::cfp_publisher::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_doi:::</div>
        <div>::cfp_doi::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_ddc:::</div>
        <div>::cfp_ddc::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_corresponding_author:::</div>
        <div>::cfp_corresponding_author::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_presenting_author:::</div>
        <div>::cfp_presenting_author::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_urn:::</div>
        <div>::cfp_urn::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_keywords:::</div>
        <div>::cfp_keywords::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_funding_name:::</div>
        <div>::cfp_funding_name::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_funding_id:::</div>
        <div>::cfp_funding_id::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_publication_year:::</div>
        <div>::cfp_publication_year::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_version:::</div>
        <div>::cfp_version::</div>
    </div>
</div>