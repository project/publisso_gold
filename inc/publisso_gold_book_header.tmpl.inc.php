<section class="article-header border-top">
        <div class="cover--pub col-md-2">
                <!--<a href="::lnk_cover::">--><img class="img-responsive" src="/system/getPicture/bc/::bk_id::" alt=""
                                                    title=""><!--</a>-->
        </div>
        <div class="header-col-right col-md-3">
                <div class="logo--publisher">
                        <!--<a href="#">--><img class="img-responsive" src="/system/getPicture/bl/::bk_id::" alt=""
                                                title=""><!--</a>-->
                </div>
                ::lnk_publish_chapter::
        </div>
        <div class="col-md-7">
                <h1 class="page-header">::book_title::</h1>
                
                <div class="meta--article">
                        <p>
                                ::book_editors::
                        </p>
                        <ul class="">
                                ::li_item_book_issn::
                                ::li_item_book_isbn::
                        </ul>
                        <ul>
                                ::li_item_funding_text::
                        </ul>
                </div>
                ::lnk_edit_book::
                ::lnk_edit_book_eb::
        </div>
</section>
