<!-- side panel/ Inhaltsverzeichnis -->
<div class="panel-container">
	<button type="button" class="offcanvas-toggle" data-target="#offcanvas-panel"><span>::txt_table_of_contents::</span></button>
	<nav class="navbar navbar-offcanvas navbar-offcanvas-touch" role="navigation" id="offcanvas-panel" aria-label="Inhaltsverzeichnis">
		<button type="button" class="close offcanvas-close" aria-label="Close"><span class="icon-cancel" aria-hidden="true"></span></button>
		<h4 class="pub-title">::journal_title::</h4>
		
			::article_list::
		
	</nav>
</div>
<!-- .end/side panel -->
