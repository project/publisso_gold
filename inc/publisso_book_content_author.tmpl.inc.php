<div class="col col-sm-6">
        <div class="contact-tile media">
                <h3>::author_name::</h3>
                <div class="media-body">
                        <i>
                                ::author_notes::
                        </i>
                        <p>::author_institute:: ::author_department::
                        </p>
                        <a class="intern" href="#" data-toggle="modal" data-target="#::modal_id::"> ::txt_more::</a>
                
                </div>
                <div class="media-right">
                        <img class="" src="::author_pic_link::" alt="" title="">
                </div>
        </div>
        
        <div class="modal fade" id="::modal_id::" tabindex="-1" role="dialog" aria-labelledby="test label">
                <div class="modal-dialog" role="document">
                        <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="icon-cancel" aria-hidden="true"></span>
                                        </button>
                                </div>
                                <div class="modal-body media">
                                        <div class="media-body">
                                                <h3>::author_name::</h3>
                                                <p>
                                                        ::author_institute::
                                                        ::author_department::
                                                </p>
                                        </div>
                                        <div class="media-right">
                                                <img class="" src="::author_pic_link::" alt="" title="">
                                        </div>
                                        ::author_orcid::
                                </div>
                                <div class="modal-footer">
                                        <div class="row">
                                                <div class="col-sm-6">
                                                        <h4>::txt_address::</h4>
                                                        <p>::author_institute::</p>
                                                        <p>::author_street::</p>
                                                        <p>::author_address::</p>
                                                </div>
                                                <div class="col-sm-6">
                                                        <h4>::txt_contact::</h4>
                                                        <address>
                                                                <a class="contact" href="tel:::author_telephone::">::author_telephone::</a>
                                                                <a class="contact" href="mailto:::author_email::">::author_email::</a>
                                                        </address>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>
