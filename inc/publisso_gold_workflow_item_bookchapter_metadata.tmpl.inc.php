<div class="rwTable">
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_title_short::
        </div>
        <div class="rwTablecell">
          ::title_short::
        </div>
    </div>
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_authors::
        </div>
        <div class="rwTablecell">
          ::authors::
        </div>
    </div>
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_corporation::
        </div>
        <div class="rwTablecell">
          ::corporation::
        </div>
    </div>
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_conflict_of_interest::
        </div>
        <div class="rwTablecell">
          ::conflict_of_interest::
        </div>
    </div>
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_conflict_of_interest_text::
        </div>
        <div class="rwTablecell">
          ::conflict_of_interest_text::
        </div>
    </div>
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_keywords::
        </div>
        <div class="rwTablecell">
          ::keywords::
        </div>
    </div>
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_funding_name::
        </div>
        <div class="rwTablecell">
          ::funding_name::
        </div>
    </div>
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_funding_id::
        </div>
        <div class="rwTablecell">
          ::funding_id::
        </div>
    </div>
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_more_authors::
        </div>
        <div class="rwTablecell">
          ::more_authors::
        </div>
    </div>
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_ddc::
        </div>
        <div class="rwTablecell">
          ::ddc::
        </div>
    </div>
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_doi::
        </div>
        <div class="rwTablecell">
          ::doi::
        </div>
    </div>
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_license::
        </div>
        <div class="rwTablecell">
          ::license::
        </div>
    </div>
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_urn::
        </div>
        <div class="rwTablecell">
          ::urn::
        </div>
    </div>
    <div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_version::
        </div>
        <div class="rwTablecell">
          ::version::
        </div>
    </div>
	<div class="rwTablerow no_hover">
        <div class="rwTablecell head">
          ::txt_reviewer_suggestion::
        </div>
        <div class="rwTablecell">
          ::reviewer_suggestion::
        </div>
    </div>
</div>
