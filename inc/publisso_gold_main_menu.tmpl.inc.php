<ul class="menu nav navbar-nav">
    <li><a href="::lnk_publish::" target="_blank">::txt_publish::</a>
        <ul class="level2">
            <li><a href="::lnk_books::" target="_blank">::txt_books::</a>
                <ul class="level3" target="_blank">
                    <li><a href="::lnk_books_policy::" target="_blank">::txt_books_policy::</a></li>
                    <li><a href="::lnk_books_overview::">::txt_books_overview::</a></li>
                </ul>
            </li>
            <li><a href="::lnk_journals::" target="_blank">::txt_journals::</a>
                <ul class="level3">
                    <li><a href="/publisso_gold/journals">::txt_journals_overview::</a></li>
                </ul>
            </li>
            <li><a href="::lnk_conferences::" target="_blank">::txt_meetings::</a></li>
            <li>
                <a href="::lnk_research::" target="_blank">::txt_research::</a>
            </li>
            <li>
                <a href="::lnk_videos::" target="_blank">::txt_videos::</a>
            </li>
            <li>
                <a href="::lnk_repositories::" target="_blank">::txt_repositories::</a>
                <ul class="level3">
                    <li>
                        <a href="::lnk_repository_lifesciences::" target="_blank">
                            ::txt_repository_lifesciences::
                        </a>
                    </li>
                    <li>
                        <a href="::lnk_repository_theses_and_dissertations::" target="_blank">
                            ::txt_repository_theses_and_dissertations::
                        </a>
                    </li>
                    <li>
                        <a href="::lnk_leibnitzopen::" target="_blank">
                            ::txt_leibnitzopen::
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <a href="::lnk_advice::" target="_blank">
            ::txt_advice::
        </a>
        <ul class="level2">
            <li>
                <a href="::lnk_faq::" target="_blank">::txt_faq::</a>
            </li>
            <li>
                <a href="::lnk_workshops::" target="_blank">::txt_workshops::</a>
            </li>
            <li>
                <a href="::lnk_personal_consultations::" target="_blank">::txt_personal_consultations::</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="::lnk_rdm::" target="_blank">
            ::txt_rdm::
        </a>
        <ul class="level2">
            <li>
                <a href="::lnk_rdm_planning::" target="_blank">::txt_rdm_planning::</a>
            </li>
            <li>
                <a href="::lnk_rdm_documenting::" target="_blank">::txt_rdm_documenting::</a>
            </li>
            <li>
                <a href="::lnk_rdm_refs::" target="_blank">::txt_rdm_refs::</a>
            </li>
            <li>
                <a href="::lnk_rdm_publishing::" target="_blank">::txt_rdm_publishing::</a>
            </li>
            <li>
                <a href="::lnk_rdm_archiving::" target="_blank">::txt_rdm_archiving::</a>
            </li>
            <li>
                <a href="::lnk_rdm_searching::" target="_blank">::txt_rdm_searching::</a>
            </li>
            <li>
                <a href="::lnk_rdm_principles::" target="_blank">::txt_rdm_principles::</a>
            </li>
                <li>
                        <a href="::lnk_rdm_nfdi4life::" target="_blank">::txt_rdm_nfdi4life::</a>
                </li>
            <li>
                <a href="::lnk_rdm_projects::" target="_blank">::txt_rdm_projects::</a>
            </li>
            <li>
                <a href="::lnk_rdm_tips::" target="_blank">::txt_rdm_tips::</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="::lnk_dp::" target="_blank">
            ::txt_dp::
        </a>
        <ul class="level2">
            <li>
                <a href="::lnk_dp_what::" target="_blank">::txt_dp_what::</a>
            </li>
            <li>
                <a href="::lnk_dp_zbmed::" target="_blank">::txt_dp_zbmed::</a>
            </li>
            <li>
                <a href="::lnk_dp_digitres::" target="_blank">::txt_dp_digitres::</a>
            </li>
            <li>
                <a href="::lnk_dp_rosetta::" target="_blank">::txt_dp_rosetta::</a>
            </li>
            <li>
                <a href="::lnk_dp_partners::" target="_blank">::txt_dp_partners::</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="::lnk_working_for_you::" target="_blank">::txt_working_for_you::</a>
        <ul class="level2">
            <li>
                <a href="::lnk_about_us::" target="_blank">::txt_about_us::</a>
            </li>
            <li>
                <a href="::lnk_networking_and_policy_shaping::"
                   target="_blank">::txt_networking_and_policy_shaping::</a>
            </li>
            <li>
                <a href="::lnk_lectures_and_workshops::" target="_blank">::txt_lectures_and_workshops::</a>
            </li>
            <li>
                <a href="::lnk_leibnitz_association::" target="_blank">::txt_leibnitz_association::</a>
            </li>
            <li>
                <a href="::lnk_doi_service::" target="_blank">::txt_doi_service::</a>
            </li>
        </ul>
    </li>
    ::li_item_admin::
    ::li_item_siteadmin::
</ul>
