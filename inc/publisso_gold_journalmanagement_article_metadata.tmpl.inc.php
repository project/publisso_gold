<div class="rwTable">
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_article_type:::</div>
        <div>::jrna_article_type::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_volume:::</div>
        <div>::jrna_volume::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_volume_title:::</div>
        <div>::jrna_volume_title::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_doc_number:::</div>
        <div>::jrna_doc_number::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_title_translated:::</div>
        <div>::jrna_title_translated::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_issue:::</div>
        <div>::jrna_issue_title::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_license:::</div>
        <div>::jrna_license::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_publication_year:::</div>
        <div>::jrna_publication_year::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_publication_place:::</div>
        <div>::jrna_publication_place::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_publisher:::</div>
        <div>::jrna_publisher::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_doi:::</div>
        <div>::jrna_doi::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_ddc:::</div>
        <div>::jrna_ddc::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_corresponding_author:::</div>
        <div>::jrna_corresponding_author::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_urn:::</div>
        <div>::jrna_urn::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_keywords:::</div>
        <div>::jrna_keywords::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_funding_name:::</div>
        <div>::jrna_funding_name::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_funding_id:::</div>
        <div>::jrna_funding_id::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_publication_year:::</div>
        <div>::jrna_publication_year::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_version:::</div>
        <div>::jrna_version::</div>
    </div>
</div>