<div class="tab-pane ::content_contact_class_active::" id="contact">
        <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    <div class="table-of-content">
                        ::journal_contact_content::
                    </div>
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                    
                </div>
            </aside>
        </div>
	</div>
