<div class="width-control">
  <div class="region region-footer">
    <nav role="navigation" aria-labelledby="block-bootstrap-footer-menu" id="block-bootstrap-footer" class="contextual-region">
      <h4>::txt_footer_title::</h4>
      <ul class="nav social">
        <li><a href="::lnk_twitter::" target="_blank"><span class="icon-twitter">ZB MED auf Twitter</span></a></li>
        <li><a href="::lnk_youtube::" target="_blank"><span class="icon-youtube">ZB MED auf YouTube</span></a></li>
        <li><a href="::lnk_flickr::" target="_blank"><span class="icon-flickr">ZB MED auf Flickr</span></a></li>
        <li><a href="::lnk_googleplus::" target="_blank"><span class="icon-gplus">ZB MED auf Google+</span></a></li>
      </ul><br>
      <h2 class="visually-hidden">Footer menu</h2>
      <div data-contextual-id="block:block=bootstrap_footer:langcode=de|menu:menu=footer:langcode=de"></div>
      <ul class="menu nav">
        <li><a href="::lnk_sitemap::" target="_blank">::txt_sitemap::</a></li>
        <li><a href="::lnk_contact::" target="_blank">::txt_contact::</a></li>
        <li><a href="::lnk_legal_notice::" target="_blank">::txt_legal_notice::</a></li>
        <li>::lnk_privacy_protection::</li>
        <li>::login::</li>
      </ul>
      <a class="zbmed" href="::logo.zbmed.lnk::" target="_blank" title="::logo.zbmed.lnk.title::">
        <img class="img-responsive" src="::logo.zbmed.img.src::" alt="ZB MED: Logo">
      </a>
      <img class="img-responsive" src="::logo.gms.img.src::" alt="GMS: Logo" style="position: absolute; right: 20%; top: 35px; width: 100px;">
      <a class="zbmed" href="::logo.publisso.lnk::" style="right: 35% ! important;" target="_blank" title="::logo.publisso.lnk.title::">
        <img class="img-responsive" src="::logo.publisso.img.src::" alt="ZB MED: Logo">
      </a>
      <a class="toplink" href="#main">::txt_totop::<span class="icon-arrow-up" aria-hidden="true"></span></a>
    </nav>
  </div>
</div>
<div id="cookie_agree">
        <div>
                <span>::txt_cookie_hint::</span> 
        </div>
        <span id="cookie_agree_close" onclick="document.cookie = 'cookie_agreed=1;path=/';jQuery('#cookie_agree').slideUp()">::txt_close::</span>
</div>
