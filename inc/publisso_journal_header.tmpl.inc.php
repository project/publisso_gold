<section class="article-header border-top">
  <div class="cover--pub col-md-2">::cover::</div>
  <div class="header-col-right col-md-3">
    <div class="logo--publisher">
            ::logo::
    </div>
  </div>
  <div class="col-md-7">
          <h1 class="page-header">::title::</h1>
        
          <div class="meta--article">
                  <p>
                          ::editors::
                  </p>
                  ::issn_isbn::
                  <p>
                          ::funding::
                  </p>
          </div>
          <br>
        ::lnk_edit_journal::
        ::lnk_edit_journal_eb::
        ::lnk_publish_article::
  </div>
</section>
