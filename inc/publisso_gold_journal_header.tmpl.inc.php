<section class="article-header border-top">
  <div class="cover--pub col-md-2">::journal_cover::</div>
  <div class="header-col-right col-md-3">
    <div class="logo--publisher">
      ::journal_logo::
    </div>
  </div>
  <div class="col-md-7">
    <h1 class="page-header">::journal_title::</h1>

    <div class="meta--article">
      <p>
        ::journal_editors::
      </p>
      <ul class="">
        ::li_item_journal_issn::
        ::li_item_journal_isbn::
      </ul>
      <ul>
        ::li_item_funding_text::
      </ul>
    </div>
        ::lnk_edit_journal::
        ::lnk_edit_journal_eb::
        ::lnk_publish_article::
  </div>
</section>
