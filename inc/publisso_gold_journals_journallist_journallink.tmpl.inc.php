<li class="media">
  <div class="media-left">
      <a href="#">
        ::journals_journallist_cover::
      </a>
  </div>
  <div class="media-body media-italics">
      <h3 class="media-heading">::journals_journallist_link::</h3>
      <p>::journals_journallist_description::</p>
      <p>::lnk_view_journal::</p>
  </div>
  <div class="media-right">
      ::journals_journallist_logo::
  </div>
</li>
