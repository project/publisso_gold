<div class="tab-pane ::content_article_content_class_active::" id="article-content">
        <div class="row">
            <div class="col-sm-9">
                <div class="region region-content article-content">
                    <!--<div class="table-of-content">-->
                        <div id="articleMetaContainer">
                        ::containerArticleType::
                        ::containerSubCategory::
                        </div>
                        <br>
                        ::journal_article_content::
                    <!--</div>-->
                </div>
            </div>
            <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second article__suporting-information">
                        ::livivo_search::
                        ::pubmed_search::
                    ::citation_note::
                    ::dates::
                    ::license::
                    ::translations::
                    ::version::
                    ::downloads::
                    ::livivosearch::
                    ::sustaining_members::
                </div>
            </aside>
        </div>
	</div>
