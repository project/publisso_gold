<div class="tab-pane ::content_chapter_content_class_active::" id="chapter-content">
	<div class="row">
        <div class="col-sm-9">
            <div class="region region-content article-content">
                ::book_chapter_navigation::
                ::PDFPreview::
                <div id="chapter-text">
                ::book_chapter_content::
                ::book_chapter_erratum::
                </div>
            </div>
        </div>
        <aside class="col-sm-3" role="complementary">
            <div class="region region-sidebar-second article__suporting-information">
                ::booksearch::
                    
                ::citation_note::
                
                ::downloads::
                
                ::chapter_version::

                ::chapter_license::
                
                ::search::
        
                    ::search_livivo::
                
                ::chapter_downloads::

                ::chapter_keywords::
                
                ::chapter_other_publications::

                ::sustaining_members::
                
                ::dates::

            </div>
        </aside>
    </div>
	<hr>
	::lnk_set_erratum::
	<div id="socialshareprivacy"></div>
</div>
