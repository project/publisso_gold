<div class="rwTable">
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_license:::</div>
        <div>::cp_license::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_publication_year:::</div>
        <div>::cp_publication_year::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_publication_place:::</div>
        <div>::cp_publication_place::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_publisher:::</div>
        <div>::cp_publisher::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_doi:::</div>
        <div>::cp_doi::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_ddc:::</div>
        <div>::cp_ddc::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_corresponding_author:::</div>
        <div>::cp_corresponding_author::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_urn:::</div>
        <div>::cp_urn::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_keywords:::</div>
        <div>::cp_keywords::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_funding_number:::</div>
        <div>::cp_funding_number::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_funding_id:::</div>
        <div>::cp_funding_id::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_publication_year:::</div>
        <div>::cp_publication_year::</div>
    </div>
    <div class="rwTablerow">
        <div class="rwTablecell head">::txt_version:::</div>
        <div>::cp_version::</div>
    </div>
</div>